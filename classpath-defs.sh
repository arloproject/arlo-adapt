
#######################################
# Full ClassPath Definition for ADAPT #

# Build Java ClassPaths
CLASSPATH="${CLASSPATH}:${CWD}/adapt/"

# ARLO
CLASSPATH="${CLASSPATH}:${CWD}/adapt/bin:${CWD}/adapt/bin/arlo"

# DavidWebb
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/david-webb-1.1.0.jar"
# dependency hell - this needs to load before conflicting Java 8 version in arlosvm
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/json-20140107.jar"

# ARLO Weka
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/arlosvm.jar"

# Aparapi
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/aparapi.jar"

# Jetty
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/jetty/jetty-all-9.0.0.v20130308.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/jetty/org.eclipse.jetty.util.ajax_9.0.0.v20130308.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/jetty/javax.servlet-3.0.0.v201112011016.jar"

# Jython 
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/jython.jar"  # backward compatibility with existing Jython code in Java

# Javax
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/activation.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/mail.jar"

# JNA 
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/jna.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/platform.jar"

# ImageJ
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/ij.jar"

# Java Matrix
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/Jama-1.0.2.jar"

# JCuda
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/jcuda-0.4.1RC2b.jar"

# JavaZOOM
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/jl1.0.1.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/tritonus_share.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/mp3spi1.9.5.jar"

# MySQL
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/mysql-connector-java-5.1.18-bin.jar"

# Servlet API
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/servlet-api.jar"

# Google / Gdata
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-analytics-2.1.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-analytics-meta-2.1.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-appsforyourdomain-1.0.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-appsforyourdomain-meta-1.0.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-base-1.0.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-blogger-2.0.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-blogger-meta-2.0.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-books-1.0.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-books-meta-1.0.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-calendar-2.0.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-calendar-meta-2.0.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-client-1.0.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-client-meta-1.0.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-codesearch-2.0.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-codesearch-meta-2.0.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-contacts-3.0.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-contacts-meta-3.0.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-core-1.0.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-docs-3.0.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-docs-meta-3.0.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-finance-2.0.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-finance-meta-2.0.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-gtt-2.0.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-gtt-meta-2.0.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-health-2.0.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-health-meta-2.0.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-maps-2.0.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-maps-meta-2.0.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-media-1.0.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-photos-2.0.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-photos-meta-2.0.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-projecthosting-2.1.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-projecthosting-meta-2.1.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-sidewiki-2.0.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-sidewiki-meta-2.0.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-sites-2.0.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-sites-meta-2.0.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-spreadsheet-3.0.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-spreadsheet-meta-3.0.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-webmastertools-2.0.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-webmastertools-meta-2.0.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-youtube-2.0.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gbooks/gdata-youtube-meta-2.0.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/google-collect-1.0.jar"

# Weka
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/components-1.0.jar"
# NOTE - order of these dataflow jars matters - they have overlapping files and I suspect some of them are bad versions
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/dataflow-tonyb-build-20160618.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/dataflow-1.0.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/foundry-datatype-datamining.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/dom-2.3.0-jaxb-1.0.6.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/xml-apis-1.4.01.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/slf4j-api-1.7.21.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/weka.jar"

# APACHE HTTP Client
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/httpclient-4.5.2.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/httpcore-4.4.4.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/apache-logging-log4j.jar"

# Google Gson
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/gson-2.2.4.jar"

# Jena
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/jena-2.6.4.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/icu4j-3.4.4.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/iri-0.8.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/log4j-api-2.6.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/log4j-core-2.6.jar"

# Apache Commons
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/commons-logging-1.2.jar"
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/commons-codec-1.10.jar"

# Xerces
CLASSPATH="${CLASSPATH}:${CWD}/adapt/lib/xercesImpl.jar"

export CLASSPATH
