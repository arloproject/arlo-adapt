package types;

public class Tuple4<T1,T2,T3,T4> extends Tuple3<T1,T2,T3>{
	public T4 _4 = null;

	public Tuple4(T1 o1, T2 o2, T3 o3, T4 o4){
		super( o1, o2, o3);
		_4 = o4;
	}
}