package types;
import static literal.Collection.Map;
import static types.Operators.*;

public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println( o( 1, 2.0, 3, "four" ).getClass() );    // class types.Tuple4
		System.out.println( Map(o( "a", "1"), o("b", "2")));    // class types.Tuple4
	}

}