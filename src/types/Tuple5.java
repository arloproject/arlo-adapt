package types;

public class Tuple5<T1,T2,T3,T4,T5> extends Tuple4<T1,T2,T3,T4>{
	public T5 _5 = null;

	public Tuple5(T1 o1, T2 o2, T3 o3, T4 o4, T5 o5){
		super( o1, o2, o3, o4 );
		_5 = o5;
	}
}