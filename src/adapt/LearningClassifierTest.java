/**
 * University of Illinois/NCSA Open Source License
 * 
 * Copyright (c) 2010, Board of Trustees-University of Illinois. All rights reserved.
 * 
 * Developed by:
 * 
 * David Tcheng Automated Learning Group National Center for Supercomputing Applications
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal with the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to
 * do so, subject to the following conditions:
 * 
 * * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimers.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimers in the documentation and/or other materials provided with the
 * distribution.
 * 
 * * Neither the names of Automated Learning Group, The National Center for Supercomputing Applications, or University of Illinois, nor the names of its contributors may be used to endorse or promote
 * products derived from this Software without specific prior written permission.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 */
package adapt;

import java.util.LinkedHashMap;

public class LearningClassifierTest {

	public static void main(String[] args) {

		System.out.println("...loading Learning Classifier");

		LearningClassifier LearningClassifier = new LearningClassifier(new String[] { "/data/FruitVeg/", "/data/Starbucks/" });
		System.out.println("...done Creating Learning Classifier");

		LinkedHashMap<String, Double> dbKeysAndProbabilities = null;

		dbKeysAndProbabilities = LearningClassifier.getProbabilities("FruitVeg", "/data/live/images/Bananas/IMG_0965.JPG");
		System.out.println(dbKeysAndProbabilities);

		dbKeysAndProbabilities = LearningClassifier.getProbabilities("Starbucks", "/data/live/images/Blueberries/IMG_1093.JPG");
		System.out.println(dbKeysAndProbabilities);

		dbKeysAndProbabilities = LearningClassifier.getProbabilities("FruitVeg", "/data/live/images/Cantaloupe/IMG_1940.JPG");
		System.out.println(dbKeysAndProbabilities);
	}

}
