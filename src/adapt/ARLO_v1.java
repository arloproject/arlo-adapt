package adapt;

public class ARLO_v1 {

	static final int numSpaceDimensions = 3; // X, Y, Z
	static final int numSensors = 4;
	static final int numEvents = 3;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String[] sensorNames = new String[numSensors];
		double[][] sensorXYZs = new double[numSensors][numSpaceDimensions];
		double[][] eventTimesInMS = new double[numSensors][numEvents];
		{
			int i = 0;
			sensorNames[i] = "CenterDish";
			sensorXYZs[i] = new double[] { 697, 458, 0 };
			eventTimesInMS[i] = new double[] { 543, 722, 806 };
			i++;
			sensorNames[i] = "CenterGround";
			sensorXYZs[i] = new double[] { 700, 455, 0 };
			eventTimesInMS[i] = new double[] { 552, 730, 813 };
			i++;
			sensorNames[i] = "InnerNorthEast";
			sensorXYZs[i] = new double[] { 799, 559, 0 };
			eventTimesInMS[i] = new double[] { 225, 404, 488 };
			i++;
			sensorNames[i] = "OuterNorthEast";
			sensorXYZs[i] = new double[] { 899, 626, 0 };
			eventTimesInMS[i] = new double[] { 211, 393, 475 };
			i++;

		}

		int numTOADs = numSensors * (numSensors - 1) / 2;

		System.out.println("numSensors = " + numSensors);
		System.out.println("numEvents = " + numEvents);
		System.out.println("numTOADs = " + numTOADs);

		double[] eventTimeOfArrivalDifferences = new double[numTOADs];

		while (true)
			for (int eventIndex = 0; eventIndex < numEvents; eventIndex++) {

				double[] bestPoint = new double[3];
				double bestScore = Double.NEGATIVE_INFINITY;

				{

					// compute distance matrix

					int toadIndex = 0;

					for (int sensorIndex1 = 0; sensorIndex1 < numSensors - 1; sensorIndex1++) {

						for (int sensorIndex2 = sensorIndex1 + 1; sensorIndex2 < numSensors; sensorIndex2++) {

							double diff = eventTimesInMS[sensorIndex1][eventIndex] - eventTimesInMS[sensorIndex2][eventIndex];
							eventTimeOfArrivalDifferences[toadIndex++] = diff;

						}
					}

					// search space for solution

					// pick starting point for search

					double[] coordSums = new double[3];
					double[] startPoint = new double[3];

					for (int sensorIndex = 0; sensorIndex < numSensors; sensorIndex++) {
						for (int j = 0; j < numSpaceDimensions; j++) {
							coordSums[j] += sensorXYZs[sensorIndex][j];
						}
					}

					for (int j = 0; j < numSpaceDimensions; j++) {
						startPoint[j] = coordSums[j] / numSensors;
					}
					for (int j = 0; j < numSpaceDimensions; j++) {
						bestPoint[j] = startPoint[j];
					}

					// find search radius

					double maxDistance = Double.NEGATIVE_INFINITY;

					for (int sensorIndex = 0; sensorIndex < numSensors; sensorIndex++) {

						double distance = distanceBetweenPoints(startPoint, sensorXYZs[sensorIndex]);

						if (distance > maxDistance) {
							maxDistance = distance;
						}
					}

//					System.out.println("maxDistance = " + maxDistance);

					// double searchRadius = maxDistance * 4;
					double searchRadius = 1.0;
					double searchRadiusReductionFactor = 0.999999;
					double endSearchRadius = 0.001;

//					System.out.println("searchRadius = " + searchRadius);

					double[] candidatePoint = new double[numSpaceDimensions];
					double[] candidateTimeOfArrivals = new double[numSensors];
					double[] candidateTimeOfArrivalDifferences = new double[numTOADs];
					while (true) {

						// pick next candidate point
						if (true) {
							while (true) {
								for (int j = 0; j < numSpaceDimensions; j++) {
									candidatePoint[j] = bestPoint[j] + searchRadius * (Math.random() - 0.5);
								}
//								break;
								if (candidatePoint[2] >= 0.0) {
									break;
								}
							}
						}
						if (false) {
							candidatePoint[0] = 858.7250116215321 + 100 * Math.random();
							candidatePoint[1] = 596.317682957289 + 100 * Math.random();
							candidatePoint[2] = 117.48742050057461 + 100 * Math.random();

						}

						// calculate sensor time of arrivals

						for (int sensorIndex = 0; sensorIndex < numSensors; sensorIndex++) {

							double distance = distanceBetweenPoints(candidatePoint, sensorXYZs[sensorIndex]);

							double speedOfSoundInMetersPerSecond = 337.642;
							double timeInMS = distance / speedOfSoundInMetersPerSecond * 1000.0;

							candidateTimeOfArrivals[sensorIndex] = timeInMS;

						}

						// calculate difference matrix

						toadIndex = 0;
						for (int sensorIndex1 = 0; sensorIndex1 < numSensors - 1; sensorIndex1++) {

							for (int sensorIndex2 = sensorIndex1 + 1; sensorIndex2 < numSensors; sensorIndex2++) {

								double diff = candidateTimeOfArrivals[sensorIndex1] - candidateTimeOfArrivals[sensorIndex2];

								candidateTimeOfArrivalDifferences[toadIndex++] = diff;

							}
						}

						// calculate score

						double candidateScore = 0.0;
						for (int i = 0; i < numTOADs; i++) {
							double difference = Math.abs(candidateTimeOfArrivalDifferences[i] - eventTimeOfArrivalDifferences[i]);
							candidateScore -= difference;
						}

						candidateScore /= numTOADs;

						if (candidateScore > bestScore) {
							bestScore = candidateScore;
							for (int j = 0; j < numSpaceDimensions; j++) {
								bestPoint[j] = candidatePoint[j];
							}

						}

						// report progress

						// System.out.println();
						// System.out.println("bestScore = " + bestScore);
						// System.out.println("searchRadius = " + searchRadius);

						// update search radius

						searchRadius *= searchRadiusReductionFactor;

						if (searchRadius < endSearchRadius) {
							break;
						}

					}

				}
				System.out.println();
				System.out.println("eventIndex = " + eventIndex);
				System.out.println("bestScore = " + bestScore);
				for (int j = 0; j < numSpaceDimensions; j++) {
					System.out.println("bestPoint[" + j + "] = " + bestPoint[j]);
				}

			}

	}

	static double distanceBetweenPoints(double[] pointA, double[] pointB) {

		double sum = 0;

		for (int j = 0; j < numSpaceDimensions; j++) {

			double diff = pointA[j] - pointB[j];

			sum += diff * diff;

		}

		return Math.sqrt(sum);

	}
}
