package adapt;

/*
 * @(#)Dash.java	1.4 98/12/03
 *
 * Copyright 1998 by Sun Microsystems, Inc.,
 * 901 San Antonio Road, Palo Alto, California, 94303, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Sun Microsystems, Inc. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Sun.
 */

import ij.plugin.ScreenGrabber;

import java.awt.*;
import java.awt.geom.*;
import java.awt.event.*;
import java.awt.font.TextLayout;
import java.awt.font.FontRenderContext;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import javax.swing.*;

/**
 * Various shapes stroked with a dashing pattern.
 */
public class MovementClusterVis extends JApplet {

	int numDimensions = 3;

	static int screenWidth = 800;
	static int screenHeight = 800;

	public MovementClusterVis() {

		setBackground(Color.white);

		loadData();
		startBackgroundComputation();
	}

	private void startBackgroundComputation() {
		// TODO Auto-generated method stub

		MovementClusteringBackgroundComputation compute = new MovementClusteringBackgroundComputation(this);

		compute.start();

	}

	SimpleTable table = null;
	double[][][] examplesACC;
	double[] exampleEntropy = null;
	double[] workExampleEntropy = null;
	double exampleEntropyTopFraction = 0.10;
	double exampleEntropyTheshold = Double.NaN;

	int[] exampleCellIndices;
	int[] exampleCellIndiceExampleIndices;
	double[] exampleXs;
	double[] exampleYs;
	double[] examplePerformance;

	int numExamples = -1;
	int numFrames = -1;

	public void allocateExampleStructures() {

		exampleCellIndices = new int[numExamples];
		exampleCellIndiceExampleIndices = new int[numExamples];

		exampleXs = new double[numExamples];
		exampleYs = new double[numExamples];
		examplePerformance = new double[numExamples];
	}

	public void loadData() {

		// String fileRoot = "Galapagos Tortoise Programme EOBS with ACC";
		// String fileRoot = "Oilbirds EOBS with ACC";
		String fileRoot = "Capucins EOBS with ACC";

		String examplesFilePath = "/home/dtcheng/move/" + fileRoot + ".ser";

		// if (IO.fileExists(examplesFilePath)) {
		if (false) {
			examplesACC = (double[][][]) IO.readObject(examplesFilePath);
			numExamples = examplesACC.length;
			numFrames = examplesACC[0][0].length;
			allocateExampleStructures();
			return;
		}

		int dateIndex = 0;
		int xyzRowIndex = 3;
		int accRowIndex = 5;

		table = null;

		try {
			table = IO.readDelimitedTable("/home/dtcheng/move/" + fileRoot + ".csv", ",");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		int numDataRows = table.numDataRows;
		int numDataColumns = table.numDataColumns;

		System.out.println("numDataRows    = " + numDataRows);
		System.out.println("numDataColumns = " + numDataColumns);

		int numCandidateExamples = 0;
		int[] exampleTableRowIndices = new int[numDataRows];
		long[] exampleLongTime = new long[numDataRows];
		int[] exampleNumACCValueFrames = new int[numDataRows];

		int numFrameLengthBins = 1000;
		int[] frameLengthCounts = new int[numFrameLengthBins];

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		for (int rowIndex = 0; rowIndex < numDataRows; rowIndex++) {

			String xyzcValuesString = table.getString(rowIndex, xyzRowIndex);

			boolean containsACC = xyzcValuesString.contains("XYZ");

			if (containsACC) {

				String dateString = table.getString(rowIndex, dateIndex);
				Date date = null;
				try {
					date = simpleDateFormat.parse(dateString);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				long time = date.getTime();

				String accValuesString = table.getString(rowIndex, accRowIndex).replaceAll("\"", "");

				String[] parts = accValuesString.split(" ");

				if (parts.length % numDimensions != 0) {

					System.out.println("(parts.length % numDimensions != 0)");
					System.exit(1);
				}

				int numFrames = parts.length / numDimensions;

				exampleLongTime[numCandidateExamples] = time;
				exampleTableRowIndices[numCandidateExamples] = rowIndex;
				exampleNumACCValueFrames[numCandidateExamples] = numFrames;

				frameLengthCounts[numFrames]++;

				numCandidateExamples++;
			}
		}

		System.out.println("numCandidateExamples    = " + numCandidateExamples);

		int maxCount = -1;
		int maxCountIndex = -1;

		if (false) {
			for (int i = 0; i < frameLengthCounts.length; i++) {

				if (frameLengthCounts[i] > maxCount) {
					maxCount = frameLengthCounts[i];
					maxCountIndex = i;
				}

			}
		} else {
			maxCountIndex = 54;
		}

		System.out.println("maxCount    = " + maxCount);
		System.out.println("maxCountIndex    = " + maxCountIndex);

		int bestNumFrames = maxCountIndex;
		System.out.println("bestFrameLenth    = " + bestNumFrames);

		numExamples = 0;
		numFrames = bestNumFrames;

		for (int i = 0; i < numCandidateExamples; i++) {

			if (exampleNumACCValueFrames[i] == numFrames) {

				exampleTableRowIndices[numExamples] = exampleTableRowIndices[i];
				exampleLongTime[numExamples] = exampleLongTime[i];
				numExamples++;

			}

		}
		System.out.println("numExamples    = " + numExamples);

		examplesACC = new double[numExamples][numDimensions][numFrames];

		allocateExampleStructures();

		for (int exampleIndex = 0; exampleIndex < numExamples; exampleIndex++) {

			int rowIndex = exampleTableRowIndices[exampleIndex];

			// System.out.println("exampleIndex = " + exampleIndex);

			String valuesString = table.getString(rowIndex, 5).replaceAll("\"", "");

			// System.out.println("valuesString = " + valuesString);

			String[] values = valuesString.split(" ");

			int valueIndex = 0;
			for (int j = 0; j < numFrames; j++) {
				for (int i = 0; i < numDimensions; i++) {

					examplesACC[exampleIndex][i][j] = Double.parseDouble(values[valueIndex]);

					valueIndex++;
				}
			}

		}

		for (int exampleIndex = 0; exampleIndex < numExamples; exampleIndex++) {

			System.out.print(exampleLongTime[exampleIndex] + "\t" + exampleLongTime[exampleIndex] % (24 * 3600 * 1000));

			for (int j = 0; j < numFrames; j++) {
				for (int i = 0; i < numDimensions; i++) {
					System.out.print("\t" + (int) examplesACC[exampleIndex][i][j]);
				}
			}
			System.out.println();

		}

		System.exit(1);

		normalizeExamples();
		randomizeExamples();

		IO.writeObject(examplesFilePath, examplesACC);

	}

	void normalizeExamples() {

		exampleEntropy = new double[numExamples];
		workExampleEntropy = new double[numExamples];

		double[] sums = new double[numDimensions];

		for (int e = 0; e < numExamples; e++) {
			for (int d = 0; d < numDimensions; d++) {
				for (int i = 0; i < numFrames; i++) {
					sums[d] += examplesACC[e][d][i];
				}

			}
		}
		double[] means = new double[numDimensions];
		for (int d = 0; d < numDimensions; d++) {
			means[d] = sums[d] / (numExamples * numFrames);
		}

		double[] maxDeviations = new double[numDimensions];
		for (int d = 0; d < numDimensions; d++) {
			maxDeviations[d] = Double.NEGATIVE_INFINITY;
		}

		for (int e = 0; e < numExamples; e++) {
			for (int d = 0; d < numDimensions; d++) {
				for (int i = 0; i < numFrames; i++) {

					double deviation = Math.abs(examplesACC[e][d][i] - means[d]);

					if (deviation > maxDeviations[d]) {
						maxDeviations[d] = deviation;
					}

				}

			}
		}

		for (int e = 0; e < numExamples; e++) {
			for (int d = 0; d < numDimensions; d++) {

				double sum = 0.0;
				for (int i = 0; i < numFrames; i++) {
					sum += examplesACC[e][d][i];
				}
				double mean = sum / numFrames;

				for (int i = 0; i < numFrames; i++) {
					// examples[e][d][i] = ((examples[e][d][i] - means[d]) / (maxDeviations[d])) / 2.0 + 0.5;

					examplesACC[e][d][i] = ((examplesACC[e][d][i] - mean) / maxDeviations[d]) / 2.0 + 0.5;
					exampleEntropy[e] += Math.abs(examplesACC[e][d][i] - 0.5);
				}

			}
		}

		System.arraycopy(exampleEntropy, 0, workExampleEntropy, 0, numExamples);

		/************************************/
		/* filter examples based on entropy */
		/************************************/

		Arrays.sort(workExampleEntropy);
		exampleEntropyTheshold = workExampleEntropy[(int) (numExamples * (1.0 - exampleEntropyTopFraction))];

		System.out.println("exampleEntropyTheshold = " + exampleEntropyTheshold);

		int newNumExamples = 0;

		for (int e = 0; e < numExamples; e++) {
			if (exampleEntropy[e] > exampleEntropyTheshold) {
				newNumExamples++;
			}
		}
		System.out.println("newNumExamples = " + newNumExamples);

		double[][][] filteredExamples = new double[newNumExamples][][];

		int newExampleIndex = 0;
		for (int e = 0; e < numExamples; e++) {
			if (exampleEntropy[e] > exampleEntropyTheshold) {
				filteredExamples[newExampleIndex] = examplesACC[e];
				newExampleIndex++;
			}
		}
		examplesACC = filteredExamples;
		numExamples = newNumExamples;

	}

	void randomizeExamples() {

		double[][][] randomizedExamples = new double[numExamples][][];

		int[] randomIndices = Utility.randomIntArray(123, numExamples);

		for (int i = 0; i < numExamples; i++) {
			randomizedExamples[i] = examplesACC[randomIndices[i]];
		}

		examplesACC = randomizedExamples;

	}

	int cellWidth = 70;
	int cellHeight = 70;
	int xNumCells = -1;
	int yNumCells = -1;
	int numCells = -1;
	int[] cellNumExamples = null;
	int[] cellXs = null;
	int[] cellYs = null;
	int[][] cellExampleIndices = null;
	int maxNumExamplesPerCell = -1;

	public void drawImage(int screenWidth, int screenHeight, Graphics2D g2) {

		// computePerformance();

		System.out.println("drawImage");

		int borderWidth = 1;

		int rectWidth = screenWidth - borderWidth * 2;
		int rectHeight = screenHeight - borderWidth * 2;
		g2.drawRect(0, 0, rectWidth, rectHeight);

		g2.clearRect(0, 0, screenWidth, screenHeight);

		for (int exampleIndex = 0; exampleIndex < numExamples; exampleIndex++) {

			int cellIndex = exampleIndex % numCells;

			exampleCellIndices[exampleIndex] = cellIndex;
			exampleCellIndiceExampleIndices[exampleIndex] = cellNumExamples[cellIndex];
			exampleXs[exampleIndex] = cellXs[cellIndex];
			exampleYs[exampleIndex] = cellYs[cellIndex];

			cellExampleIndices[cellIndex][cellNumExamples[cellIndex]] = exampleIndex;

			cellNumExamples[cellIndex]++;

		}

		int cellIndex = 0;
		for (int x = 0; x < xNumCells; x++) {

			for (int y = 0; y < yNumCells; y++) {

				drawCell(g2, cellIndex, x * cellWidth, y * cellHeight, cellWidth, cellHeight);

				cellIndex++;

			}

		}

		try {
			Thread.sleep(1000 * 4);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	double correlation(double[] xValues, double[] yValues, int numObservations) {

		double correlation = Double.NaN;

		double N = numObservations;
		double sum_sq_x = 0;
		double sum_sq_y = 0;
		double sum_coproduct = 0;
		double mean_x = xValues[0];
		double mean_y = yValues[0];

		for (int i = 2; i <= N; i++) {

			double sweep = (i - 1.0) / i;
			double delta_x = xValues[i - 1] - mean_x;
			double delta_y = yValues[i - 1] - mean_y;
			sum_sq_x += delta_x * delta_x * sweep;
			sum_sq_y += delta_y * delta_y * sweep;
			sum_coproduct += delta_x * delta_y * sweep;
			mean_x += delta_x / i;
			mean_y += delta_y / i;
		}

		double pop_sd_x = Math.sqrt(sum_sq_x / N);
		double pop_sd_y = Math.sqrt(sum_sq_y / N);
		double cov_x_y = sum_coproduct / N;
		correlation = cov_x_y / (pop_sd_x * pop_sd_y);

		return correlation;

	}

	double[] xData = null;
	double[] yData = null;

	double computePerformance() {

		System.out.println("numExamples = " + numExamples);

		if (xData == null) {
			xData = new double[numExamples * (numExamples - 1) / 2];
			yData = new double[numExamples * (numExamples - 1) / 2];
		}

		int observationCount = 0;
		for (int i1 = 0; i1 < numExamples - 1; i1++) {

			for (int i2 = i1 + 1; i2 < numExamples; i2++) {

				double xDiff = exampleXs[i1] - exampleXs[i2];
				double yDiff = exampleYs[i1] - exampleYs[i2];

				double distance = Math.sqrt(xDiff * xDiff + yDiff * yDiff);

				xData[observationCount] = distance;

				double correlationSum = 0.0;
				for (int i = 0; i < numDimensions; i++) {
					correlationSum += correlation(examplesACC[i1][i], examplesACC[i2][i], numFrames);
				}
				yData[observationCount] = correlationSum / numDimensions;

				observationCount++;

				// System.out.println(distance);

			}

		}

		double performance = correlation(xData, yData, observationCount);

		return performance;

	}

	private void drawCell(Graphics2D g2, int cellIndex, int screenX, int screenY, int cellWidth, int cellHeight) {

		double[][] xyzACC = examplesACC[cellExampleIndices[cellIndex][0]];

		g2.setColor(Color.black);
		g2.drawRect(screenX, screenY, cellWidth, cellHeight);
		g2.drawRect(screenX + 1, screenY + 1, cellWidth - 2, cellHeight - 2);

		if (true) {
			g2.setColor(Color.green);
			for (int i = 0; i < numDimensions; i++) {

				switch (i) {
				case 0:
					g2.setColor(Color.red);
					break;
				case 1:
					g2.setColor(Color.green);
					break;
				case 2:
					g2.setColor(Color.blue);
					break;

				}

				int xOffset = (cellWidth - numFrames) / 2;

				for (int f = 0; f < numFrames; f++) {

					int yOffset = ((i + 1) * cellHeight) / 4;

					int y = (int) ((xyzACC[i][f] - 0.5) * cellHeight / 4);

					g2.drawRect(screenX + xOffset + f, screenY + yOffset + y, 1, 1);

				}

			}
		}

	}

	boolean firstTime = true;

	public void paint(Graphics g) {

		if (firstTime) {
			Graphics2D g2 = (Graphics2D) g;
			Dimension d = getSize();
			g2.setBackground(getBackground());
			g2.clearRect(0, 0, d.width, d.height);
			g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			drawImage(d.width, d.height, g2);
			firstTime = false;
		}

	}

	public static void main(String argv[]) {

		final MovementClusterVis demo = new MovementClusterVis();

		JFrame f = new JFrame("MovementClusterVis");

		// f.addWindowListener(new WindowAdapter() {
		// public void windowClosing(WindowEvent e) {
		// System.out.println("window closed... exiting!");
		// System.exit(0);
		// }
		// });

		// f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// f.setSize(800, 800);
		// f.setLocation(100,100);
		// f.getContentPane().add(BorderLayout.CENTER, new JTextArea(10, 40));
		// f.setVisible(true);
		// f.show();

		f.getContentPane().add("Center", demo);
		f.pack();
		f.setSize(new Dimension(screenWidth, screenHeight));
		f.setAlwaysOnTop(true);
		f.setVisible(true);
		f.show();
	}
}