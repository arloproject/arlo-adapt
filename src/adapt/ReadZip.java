package adapt;

import java.io.File;
import java.io.FileInputStream;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import com.kenai.jffi.Array;

import jtar.TarConstants;
import jtar.TarEntry;
import jtar.TarInputStream;

public class ReadZip {

	private static boolean useMLRules = false;
	private static boolean useExpertRules = true;

	static String dataDirectory = "/u/ncsa/dtcheng/scratch-global/text";
	static String ruleSourceFileName = "correction_rules.csv";

	private static int highFreqMinCount = (int) 1e3;
	private static int lowFreqMaxCount = (int) 2;
	private static int lowFreqMinCount = (int) 1;

	private static int maxNumEntriesToProcess = Integer.MAX_VALUE;
	// private static int maxNumEntriesToProcess = (int) 1e6;
	private static int maxNumNewNGrams = (int) 300e6;
	// private static int maxNumNGrams = (int) 850e6;
	private static int maxNumNGrams = (int) 10e6;
	private static boolean sequential = true;
	private static boolean trueRandom = false;
	// private static int reportInterval = (int) 1e5;
	private static int reportInterval = (int) 1e3;
	private static int numTopWordsToReport = (int) 1e2;
	private static int maxNumWords = 100000000;
	private static int maxPageNumBytes = 10000000;

	private static int nGramWindowSize = 8;

	private static boolean reportDistribution = false;

	// public static void main(String args[]) {
	// try {
	// String filePath = "/home/dtcheng/ngFlat.v2.tar.gz";
	//
	// // Since there are 4 constructor calls here, I wrote them out in full.
	// // In real life you would probably nest these constructor calls.
	// FileInputStream fin = new FileInputStream(filePath);
	// GZIPInputStream gzis = new GZIPInputStream(fin);
	// InputStreamReader xover = new InputStreamReader(gzis);
	// BufferedReader is = new BufferedReader(xover);
	//
	// String line;
	// // Now read lines of text: the BufferedReader puts them in lines,
	// // the InputStreamReader does Unicode conversion, and the
	// // GZipInputStream "gunzip"s the data from the FileInputStream.
	// while ((line = is.readLine()) != null) {
	// System.out.println("Read: " + line);
	//
	// }
	//
	// } catch (IOException e) {
	// e.printStackTrace();
	// }
	// }

	// public static void main(String args[]) {
	// try {
	// String filePath = "/data/text/ngFlat.v2.tar.gz";
	//
	// int bufferSize = 10000;
	//
	// // Since there are 4 constructor calls here, I wrote them out in full.
	// // In real life you would probably nest these constructor calls.
	// FileInputStream fin = new FileInputStream(filePath);
	// BufferedInputStream bfin = new BufferedInputStream(fin, bufferSize);
	// GZIPInputStream gzis = new GZIPInputStream(bfin);
	//
	// byte[] data = new byte[bufferSize];
	// int count = 0;
	// long totalBytesRead = 0;
	// long startTimeInMS = System.currentTimeMillis();
	// while (gzis.available() != 0) {
	//
	// int bytesRead = gzis.read(data);
	//
	// totalBytesRead += bytesRead;
	//
	// count++;
	//
	// if (count % 100000 == 0) {
	// double durationInSeconds = (System.currentTimeMillis() - startTimeInMS) / 1000.0;
	//
	// System.out.println();
	// System.out.println("durationInSeconds = " + durationInSeconds);
	// System.out.println("totalBytesRead (GB) = " + (double) totalBytesRead / 1000000000);
	// System.out.println("bytesPerSecond (MB) = " + (double) totalBytesRead / durationInSeconds / 1000000);
	//
	// System.out.println("bytesRead = " + bytesRead);
	// System.out.println("count = " + count);
	// System.out.println("fin.available() = " + fin.available());
	// }
	//
	// }
	//
	// } catch (IOException e) {
	// e.printStackTrace();
	// }
	// }

	// public static void main(String[] args) throws Exception {
	//
	// long length = new File("/data/text/ngFlat.v2.tar").length();
	// MappedByteBuffer in = new FileInputStream(args[0]).getChannel().map(FileChannel.MapMode.READ_ONLY, 0, length);
	// int i = 0;
	// while (i < length) {
	// in.g
	// }
	// }

	// public static void main(String args[]) {
	// try {
	// String filePath = "/data/text/ngFlat.v2.tar";
	//
	// int bufferSize = 100000;
	//
	// // Since there are 4 constructor calls here, I wrote them out in full.
	// // In real life you would probably nest these constructor calls.
	// FileInputStream fin = new FileInputStream(filePath);
	// BufferedInputStream bfin = new BufferedInputStream(fin, bufferSize);
	//
	// byte[] data = new byte[bufferSize];
	// int count = 0;
	// long totalBytesRead = 0;
	// long startTimeInMS = System.currentTimeMillis();
	// int lastNumGigaBytesRead = -1;
	// int numGigBytesRead = 0;
	// while (bfin.available() != 0) {
	//
	// int bytesRead = bfin.read(data);
	//
	// totalBytesRead += bytesRead;
	//
	// numGigBytesRead = (int) (totalBytesRead / 1000000000);
	//
	// count++;
	//
	// if (numGigBytesRead != lastNumGigaBytesRead) {
	//
	// lastNumGigaBytesRead = numGigBytesRead;
	//
	// double durationInSeconds = (System.currentTimeMillis() - startTimeInMS) / 1000.0;
	//
	// System.out.println();
	// System.out.println("durationInSeconds = " + durationInSeconds);
	// System.out.println("totalBytesRead (GB) = " + (double) totalBytesRead / 1000000000);
	// System.out.println("bytesPerSecond (MB) = " + (double) totalBytesRead / durationInSeconds / 1000000);
	//
	// System.out.println("bytesRead = " + bytesRead);
	// System.out.println("count = " + count);
	// System.out.println("fin.available() = " + fin.available());
	// }
	//
	// }
	//
	// } catch (IOException e) {
	// e.printStackTrace();
	// }
	// }

	// public static void main(String args[]) {
	//
	// String tarFile = "/data/text/ngFlat.v2.tar";
	// String destFolder = "/data/text/ngFlat.v2";
	//
	// // File dir = new File(destFolder);
	// // dir.mkdir();
	//
	// try {
	// // Create a TarInputStream
	// TarInputStream tis = new TarInputStream(new BufferedInputStream(new FileInputStream(tarFile)));
	// TarEntry entry;
	// int entryCount = 0;
	// long totalNumBytesRead = 0;
	// while ((entry = tis.getNextEntry()) != null) {
	// byte data[] = new byte[2048];
	//
	// String entryName = entry.getName();
	//
	// String fileName = destFolder + "/" + entryName;
	//
	//
	// if (false) {
	// if (fileName.endsWith("/")) {
	// new File(fileName).mkdir();
	// } else {
	//
	// FileOutputStream fos = new FileOutputStream(fileName);
	// BufferedOutputStream dest = new BufferedOutputStream(fos);
	//
	// int numBytesRead;
	// while ((numBytesRead = tis.read(data)) != -1) {
	// dest.write(data, 0, numBytesRead);
	// }
	//
	// dest.flush();
	// dest.close();
	// }
	// }
	//
	//
	// totalNumBytesRead += entry.getSize() + entry.getHeader().size;
	//
	// if (entryCount % 1 == 0) {
	// System.out.println();
	// System.out.println(entryCount);
	// System.out.println(entry.getHeader().size);
	// System.out.println(totalNumBytesRead);
	// System.out.println(entry.getHeader().size);
	// System.out.println(fileName);
	// }
	//
	//
	// entryCount++;
	//
	// }
	// System.out.println();
	// System.out.println(entryCount);
	// System.out.println(totalNumBytesRead);
	//
	// tis.close();
	// } catch (FileNotFoundException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// } catch (IOException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// }

	// public static void main(String args[]) {
	// try {
	// String filePath = "/data/text/ngFlat.v2.tar";
	//
	// int maxBufferSize = (int) 10e6;
	// int bufferSize = -1;
	//
	// // Since there are 4 constructor calls here, I wrote them out in full.
	// // In real life you would probably nest these constructor calls.
	// // FileInputStream fin = new FileInputStream(filePath);
	// // BufferedInputStream bfin = new BufferedInputStream(fin, bufferSize);
	//
	// long length = new File(filePath).length();
	//
	// byte[] headerData = new byte[TarConstants.HEADER_BLOCK];
	// byte[] data = new byte[maxBufferSize];
	// int count = 0;
	// long totalBytesRead = 0;
	// long lastTotalBytesRead = 0;
	// long startTimeInMS = System.currentTimeMillis();
	// long lastBufferTimeInMS = System.currentTimeMillis();
	// int lastNumGigaBytesRead = -1;
	// int numGigBytesRead = 0;
	// RandomAccessFile raf = new RandomAccessFile(filePath, "r");
	// // FileChannel rwCh = new RandomAccessFile(allDataSpectraFilePath, "r").getChannel();
	//
	// ArrayList<String> entries = new ArrayList<String>();
	// ArrayList<Long> positions = new ArrayList<Long>();
	// while (totalBytesRead < length) {
	//
	// long bytesLeft = length - totalBytesRead;
	//
	// if (bytesLeft < maxBufferSize) {
	// bufferSize = (int) bytesLeft;
	// } else {
	// bufferSize = maxBufferSize;
	// }
	//
	// // MappedByteBuffer in = raf.getChannel().map(FileChannel.MapMode.READ_ONLY, totalBytesRead, bufferSize);
	// MappedByteBuffer in = raf.getChannel().map(FileChannel.MapMode.READ_ONLY, totalBytesRead, TarConstants.HEADER_BLOCK);
	//
	// in.get(headerData);
	//
	// TarEntry tarEntry = new TarEntry(headerData);
	// totalBytesRead += headerData.length;
	//
	// entries.add(tarEntry.getName());
	// positions.add(totalBytesRead);
	//
	// long dataSize = tarEntry.getSize();
	// long tarSize = -1;
	//
	// if (dataSize % TarConstants.HEADER_BLOCK == 0) {
	// tarSize = dataSize;
	// } else {
	// tarSize = (dataSize / TarConstants.HEADER_BLOCK + 1) * TarConstants.HEADER_BLOCK;
	// }
	//
	// // System.out.println("dataSize = " + dataSize);
	// // System.out.println("tarSize = " + tarSize);
	//
	// // in.get(data, 0, (int) tarSize);
	// // in.get(data, 0, (int) tarSize);
	// // in.clear();
	//
	// totalBytesRead += tarSize;
	//
	// numGigBytesRead = (int) (totalBytesRead / 1000000000);
	//
	// count++;
	//
	// if (numGigBytesRead != lastNumGigaBytesRead) {
	//
	// long bytesRead = totalBytesRead - lastTotalBytesRead;
	// lastNumGigaBytesRead = numGigBytesRead;
	// lastTotalBytesRead = totalBytesRead;
	//
	// double durationInSeconds = (System.currentTimeMillis() - startTimeInMS) / 1000.0;
	// double bufferDurationInSeconds = (System.currentTimeMillis() - lastBufferTimeInMS) / 1000.0;
	// lastBufferTimeInMS = System.currentTimeMillis();
	//
	// System.out.println();
	// System.out.println("durationInSeconds = " + durationInSeconds);
	// System.out.println("totalBytesRead (GB) = " + (double) totalBytesRead / 1000000000);
	// System.out.println("overall bytesPerSecond (MB) = " + (double) totalBytesRead / durationInSeconds / 1000000);
	// System.out.println("buffer  bytesPerSecond (MB) = " + (double) bytesRead / bufferDurationInSeconds / 1000000);
	//
	// System.out.println("bufferSize = " + bufferSize);
	// System.out.println("bytesRead  = " + bytesRead);
	// System.out.println("count = " + count);
	// }
	//
	// }
	//
	// raf.close();
	//
	// System.out.println("entries.size()   = " + entries.size());
	// System.out.println("positions.size() = " + positions.size());
	//
	// IO.writeObject("/data/entries.ser", entries);
	// IO.writeObject("/data/positions.ser", positions);
	//
	// } catch (IOException e) {
	// e.printStackTrace();
	// }
	// }

	public static void createIndex() {
		try {
			int maxNumEntries = (int) 69810694;

			HashMap<String, Integer> volumeNameToIndex = new HashMap<String, Integer>();
			HashMap<String, Integer> pageNameToIndex = new HashMap<String, Integer>();
			String[] volumeNames = new String[maxNumEntries];
			String[] pageNames = new String[maxNumEntries];

			// int nameMemorySize = (int) 2e9;
			// byte[] nameMemory = new byte[nameMemorySize];
			// int[] nameMemoryPtrs = new int[maxNumEntries];
			int[] volumeNameIndices = new int[maxNumEntries];
			int[] pageNameIndices = new int[maxNumEntries];
			long[] positions = new long[maxNumEntries];
			long[] lengths = new long[maxNumEntries];
			int nameMemoryIndex = -1;

			String filePath = "/data/text/ngFlat.v2.tar";

			// int maxBufferSize = (int) 10e6;
			int bufferSize = -1;

			long length = new File(filePath).length();

			byte[] headerData = new byte[TarConstants.HEADER_BLOCK];

			int count = 0;
			long totalBytesRead = 0;
			long lastTotalBytesRead = 0;
			long startTimeInMS = System.currentTimeMillis();
			long lastBufferTimeInMS = System.currentTimeMillis();
			int lastNumGigaBytesRead = -1;
			int numGigBytesRead = 0;
			RandomAccessFile raf = new RandomAccessFile(filePath, "r");
			// FileChannel rwCh = new RandomAccessFile(allDataSpectraFilePath, "r").getChannel();

			// ArrayList<String> entries = new ArrayList<String>();
			// ArrayList<Long> positions = new ArrayList<Long>();
			nameMemoryIndex = 0;
			int pageCount = 0;
			while (totalBytesRead < length) {

				raf.seek(totalBytesRead);

				// MappedByteBuffer in = raf.getChannel().map(FileChannel.MapMode.READ_ONLY, totalBytesRead, bufferSize);
				// MappedByteBuffer in = raf.getChannel().map(FileChannel.MapMode.READ_ONLY, totalBytesRead, TarConstants.HEADER_BLOCK);

				raf.readFully(headerData);
				// in.get(headerData);

				TarEntry tarEntry = new TarEntry(headerData);

				String name = tarEntry.getName();

				String parts[] = name.split("/");

				int numParts = parts.length;

				boolean valid = false;
				String volumeName = null;
				String pageName = null;
				if (numParts == 3) {
					volumeName = parts[1];
					pageName = parts[2];
					valid = true;
				}
				if (numParts == 2) {
					volumeName = parts[1];
					if (volumeName.endsWith(".mets.xml")) {
						int index = volumeName.indexOf(".mets.xml");
						volumeName = volumeName.substring(0, index);
						pageName = "mets.xml";
						valid = true;
					}
				}

				if (valid) {

					Integer volumeNameIndex = volumeNameToIndex.get(volumeName);
					if (volumeNameIndex == null) {
						volumeNameIndex = volumeNameToIndex.size();
						volumeNames[volumeNameIndex] = volumeName;
						volumeNameToIndex.put(volumeName, volumeNameIndex);
					}
					volumeNameIndices[pageCount] = volumeNameIndex;

					String[] pageNameParts = pageName.split("_");

					if (pageNameParts.length == 2) {
						pageName = pageNameParts[1];
					}
					Integer pageNameIndex = pageNameToIndex.get(pageName);
					if (pageNameIndex == null) {
						pageNameIndex = pageNameToIndex.size();
						pageNames[pageNameIndex] = pageName;
						pageNameToIndex.put(pageName, pageNameToIndex.size());
					}
					pageNameIndices[pageCount] = pageNameIndex;

					if (pageCount % 100000 == 0) {

						System.out.println("name = " + name);
						System.out.println("parts.length = " + parts.length);
						System.out.println("volumeName = " + volumeName);
						System.out.println("pageName = " + pageName);
						System.out.println("volumeNameToIndex.size() = " + volumeNameToIndex.size());
						System.out.println("pageNameToIndex.size() = " + pageNameToIndex.size());

					}

					pageCount++;
				}

				// System.arraycopy(nameBytes, 0, nameMemory, nameMemoryIndex, nameLength);
				// nameMemoryPtrs[count] = nameMemoryIndex;
				// nameMemoryIndex += nameLength;

				totalBytesRead += headerData.length;
				positions[pageCount] = totalBytesRead;

				// entries.add(tarEntry.getName());

				// positions.add(totalBytesRead);

				long dataSize = tarEntry.getSize();

				lengths[pageCount] = dataSize;

				long tarSize = -1;

				if (dataSize % TarConstants.HEADER_BLOCK == 0) {
					tarSize = dataSize;
				} else {
					tarSize = (dataSize / TarConstants.HEADER_BLOCK + 1) * TarConstants.HEADER_BLOCK;
				}

				// System.out.println("dataSize = " + dataSize);
				// System.out.println("tarSize = " + tarSize);

				// in.get(data, 0, (int) tarSize);
				// in.get(data, 0, (int) tarSize);
				// in.clear();

				totalBytesRead += tarSize;

				numGigBytesRead = (int) (totalBytesRead / 1000000000);

				count++;

				if (numGigBytesRead != lastNumGigaBytesRead) {

					long bytesRead = totalBytesRead - lastTotalBytesRead;
					lastNumGigaBytesRead = numGigBytesRead;
					lastTotalBytesRead = totalBytesRead;

					double durationInSeconds = (System.currentTimeMillis() - startTimeInMS) / 1000.0;
					double bufferDurationInSeconds = (System.currentTimeMillis() - lastBufferTimeInMS) / 1000.0;
					lastBufferTimeInMS = System.currentTimeMillis();

					System.out.println();
					System.out.println("durationInSeconds = " + durationInSeconds);
					System.out.println("totalBytesRead (GB) = " + (double) totalBytesRead / 1000000000);
					System.out.println("overall bytesPerSecond (MB) = " + (double) totalBytesRead / durationInSeconds / 1000000);
					System.out.println("buffer  bytesPerSecond (MB) = " + (double) bytesRead / bufferDurationInSeconds / 1000000);

					System.out.println("bufferSize = " + bufferSize);
					System.out.println("bytesRead  = " + bytesRead);
					System.out.println("count = " + count);
				}

			}

			raf.close();

			{
				int[] newVolumeNameIndices = new int[pageCount];
				int[] newPageNameIndices = new int[pageCount];

				System.arraycopy(volumeNameIndices, 0, newVolumeNameIndices, 0, pageCount);
				System.arraycopy(pageNameIndices, 0, newPageNameIndices, 0, pageCount);

				volumeNameIndices = newVolumeNameIndices;
				pageNameIndices = newPageNameIndices;
			}
			IO.writeObject("/data/volumeNameIndices.ser", volumeNameIndices);
			IO.writeObject("/data/pageNameIndices.ser", pageNameIndices);

			{
				long[] newPositions = new long[pageCount];
				long[] newLengths = new long[pageCount];

				System.arraycopy(positions, 0, newPositions, 0, pageCount);
				System.arraycopy(lengths, 0, newLengths, 0, pageCount);

				positions = newPositions;
				lengths = newLengths;
			}
			IO.writeObject("/data/positions.ser", positions);
			IO.writeObject("/data/lengths.ser", lengths);

			int numVolumeNames = volumeNameToIndex.size();
			int numPageNames = pageNameToIndex.size();
			{

				int volumeNameSize = 0;
				for (int i = 0; i < numVolumeNames; i++) {
					volumeNameSize += volumeNames[i].length();
				}
				System.out.println("numVolumeNames = " + numVolumeNames);
				System.out.println("volumeNameSize = " + volumeNameSize);

				byte[] volumeNameMemory = new byte[volumeNameSize];
				int volumeNameMemoryPtr = 0;
				int[] volumeNameMemoryPtrs = new int[numVolumeNames];
				int[] volumeNameMemorySizes = new int[numVolumeNames];
				for (int i = 0; i < numVolumeNames; i++) {

					String volumeName = volumeNames[i];
					byte[] data = volumeName.getBytes();
					int size = data.length;

					System.arraycopy(data, 0, volumeNameMemory, volumeNameMemoryPtr, size);

					volumeNameMemoryPtrs[i] = volumeNameMemoryPtr;
					volumeNameMemorySizes[i] = size;

					volumeNameMemoryPtr += data.length;

				}
				IO.writeObject("/data/volumeNameMemory.ser", volumeNameMemory);
				IO.writeObject("/data/volumeNameMemoryPtrs.ser", volumeNameMemoryPtrs);
				IO.writeObject("/data/volumeNameMemorySizes.ser", volumeNameMemorySizes);
			}

			{
				int pageNameSize = 0;
				for (int i = 0; i < numPageNames; i++) {
					pageNameSize += pageNames[i].length();
				}
				System.out.println("numPageNames = " + numPageNames);
				System.out.println("pageNameSize  = " + pageNameSize);

				byte[] pageNameMemory = new byte[pageNameSize];
				int pageNameMemoryPtr = 0;
				int[] pageNameMemoryPtrs = new int[numPageNames];
				int[] pageNameMemorySizes = new int[numPageNames];
				for (int i = 0; i < numPageNames; i++) {

					String pageName = pageNames[i];
					byte[] data = pageName.getBytes();
					int size = data.length;

					System.arraycopy(data, 0, pageNameMemory, pageNameMemoryPtr, size);

					pageNameMemoryPtrs[i] = pageNameMemoryPtr;
					pageNameMemorySizes[i] = size;

					pageNameMemoryPtr += data.length;

				}
				IO.writeObject("/data/pageNameMemory.ser", pageNameMemory);
				IO.writeObject("/data/pageNameMemoryPtrs.ser", pageNameMemoryPtrs);
				IO.writeObject("/data/pageNameMemorySizes.ser", pageNameMemorySizes);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	static int numNewNGrams = 0;
	static int numTotalNGrams = 0;
	static long[] newNGrams = null;
	static long[] totalNGrams = null;
	static long[] newTotalNGrams = null;

	/*
	 * 
	 * minCount = (int) 1e1; windowSize = 3; numFrequentWords = 1360143 substringCounts.size() = 124828 numFrequentWords = 7661534 substringCounts.size() = 391488 accuracy = 0.9258559147353074
	 * 
	 * minCount = (int) 1e2; windowSize = 3; numFrequentWords = 1360143 substringCounts.size() = 124828 numFrequentWords = substringCounts.size() = accuracy = 0.9286511487607461
	 * 
	 * 
	 * 
	 * best
	 * 
	 * minCount = (int) 1e3; windowSize = 3; numFrequentWords = 286645 substringCounts.size() = 35524 accuracy = 0.9315972047659745
	 * 
	 * 
	 * 
	 * 
	 * 
	 * minCount = (int) 1e4; windowSize = 3; numFrequentWords = 64071 substringCounts.size() = 13905 accuracy = 0.9310844100346891
	 * 
	 * minCount = (int) 1e5; windowSize = 3; numFrequentWords = 13339 substringCounts.size() = 5965 accuracy = 0.8664521642954099
	 * 
	 * minCount = (int) 1e6; windowSize = 3; numFrequentWords = 1682 substringCounts.size() = 1837 accuracy = 0.47736162083354283
	 * 
	 * minCount = (int) 1e7; windowSize = 3; accuracy = 0.011110086667896035
	 */

	public static void applyRules() {

		try {

			HashMap<String, String> ruleHashMap = null;
			// int windowSize = 3;
			boolean useCache = false;
			if (!useCache) {
				/***********************/
				/* read rules 1st pass */
				/***********************/

				SimpleTable table = IO.readDelimitedTable(dataDirectory + File.separatorChar + ruleSourceFileName, "\t");
				ruleHashMap = new HashMap<String, String>();
				int numRules = 0;
				int numRows = table.numDataRows;
				for (int i = 0; i < numRows; i++) {
					String ngram = table.stringMatrix[i][0];
					String ngram_spelling = table.stringMatrix[i][1];

					numRules++;
					ruleHashMap.put(ngram, ngram_spelling);
				}
				System.out.println("numRules = " + numRules);
				IO.writeObject(dataDirectory + "rules.ser", ruleHashMap);
			} else {
				/***********************/

				ruleHashMap = null;
				useCache = false;

				int tryCount = 0;
				int errorCount = 0;
				ruleHashMap = (HashMap<String, String>) IO.readObject(dataDirectory + File.separatorChar + "rules.ser");
				int numRules1 = ruleHashMap.size();
				System.out.println("numRules = " + numRules1);
			}

			HashMap<String, CountAndString> wordCounts = new HashMap<String, CountAndString>();
			int chuckSize = (int) 1e6;

			int[] volumeNameIndices = null;
			int[] pageNameIndices = null;
			long[] positions = null;
			long[] lengths = null;

			String filePath = dataDirectory + File.separatorChar + "ngFlat.v2.tar";

			int bufferSize = -1;

			RandomAccessFile raf = new RandomAccessFile(filePath, "r");

			int numEntries;

			byte[] volumeNameMemory = (byte[]) IO.readObject(dataDirectory + File.separatorChar + "volumeNameMemory.ser");
			int[] volumeNameMemoryPtrs = (int[]) IO.readObject(dataDirectory + File.separatorChar + "volumeNameMemoryPtrs.ser");
			int[] volumeNameMemorySizes = (int[]) IO.readObject(dataDirectory + File.separatorChar + "volumeNameMemorySizes.ser");

			byte[] pageNameMemory = (byte[]) IO.readObject(dataDirectory + File.separatorChar + "pageNameMemory.ser");
			int[] pageNameMemoryPtrs = (int[]) IO.readObject(dataDirectory + File.separatorChar + "pageNameMemoryPtrs.ser");
			int[] pageNameMemorySizes = (int[]) IO.readObject(dataDirectory + File.separatorChar + "pageNameMemorySizes.ser");

			volumeNameIndices = (int[]) IO.readObject(dataDirectory + File.separatorChar + "volumeNameIndices.ser");
			pageNameIndices = (int[]) IO.readObject(dataDirectory + File.separatorChar + "pageNameIndices.ser");
			positions = (long[]) IO.readObject(dataDirectory + File.separatorChar + "positions.ser");
			lengths = (long[]) IO.readObject(dataDirectory + File.separatorChar + "lengths.ser");

			numEntries = volumeNameIndices.length;

			System.out.println("numEntries = " + numEntries);

			byte[] readBuffer = new byte[maxPageNumBytes];
			long totalBytesRead = 0;
			long lastTotalBytesRead = -1;
			int fileCount = 0;

			int[] randomIndices = null;

			if (!sequential) {
				if (trueRandom) {
					randomIndices = Utility.randomIntArray((int) System.currentTimeMillis(), numEntries);
				} else {
					randomIndices = Utility.randomIntArray(123, numEntries);
				}
			}

			long overallNumPages = 0;
			long overallNumWords = 0;
			long overallNumRulesApplied = 0;
			int maxCharacterIndex = 127;
			// long[] characterCounts = new long[maxCharacterIndex];
			// long[][] characterCounts2 = new long[maxCharacterIndex][maxCharacterIndex];
			// long[][][] characterCounts3 = new long[maxCharacterIndex][maxCharacterIndex][maxCharacterIndex];
			// long[][][][] characterCounts4 = new long[maxCharacterIndex][maxCharacterIndex][maxCharacterIndex][maxCharacterIndex];

			long startTimeInMS = System.currentTimeMillis();
			long lastBufferTimeInMS = System.currentTimeMillis();
			numNewNGrams = 0;
			numTotalNGrams = 0;

			double bestNumCorrectionsPerWordRate = 0.0;
			int numEntriesToProcess = Math.min(maxNumEntriesToProcess, numEntries);

			String lastVolumeName = "";
			int numRulesAppliedOnVolume = 0;
			int volumeCount = 0;
			int volumeWithErrorCount = 0;

			int pageCount = 0;
			int pageWithErrorCount = 0;
			for (int i = 0; i < numEntriesToProcess; i++) {

				int randomIndex = -1;

				if (sequential) {
					randomIndex = i;
				} else {
					randomIndex = randomIndices[i];
				}
				// int randomIndex = i;

				String volumeName = new String(volumeNameMemory, volumeNameMemoryPtrs[volumeNameIndices[randomIndex]], volumeNameMemorySizes[volumeNameIndices[randomIndex]]);
				String pageName = new String(pageNameMemory, pageNameMemoryPtrs[pageNameIndices[randomIndex]], pageNameMemorySizes[pageNameIndices[randomIndex]]);

				// System.out.println("volumeName = " + volumeName);
				// System.out.println("pageName = " + pageName);

				if (pageName.endsWith(".txt")) {

					if (!volumeName.equals(lastVolumeName)) {
						lastVolumeName = volumeName;
						if (numRulesAppliedOnVolume != 0) {
							volumeWithErrorCount++;
						}
						numRulesAppliedOnVolume = 0;
						volumeCount++;
					}

					raf.seek(positions[randomIndex]);

					int bufferLength = (int) lengths[randomIndex];
					raf.readFully(readBuffer, 0, bufferLength);

					if (true) {

						String dataString = new String(readBuffer, 0, (int) bufferLength, "UTF-8");

						// fix hyphenation
						dataString = dataString.replaceAll("- \\n ", "");
						dataString = dataString.replaceAll("-\\n ", "");
						dataString = dataString.replaceAll("-\\n", "");

						// dataString = dataString.toLowerCase();

						// System.out.println("dataString: " + dataString);

						int stringLength = dataString.length();
						// System.out.println("bufferLength = " + bufferLength);
						// System.out.println("stringLength = " + stringLength);

						boolean[] breakCharacter = new boolean[65536];
						breakCharacter[10] = true; //
						breakCharacter[13] = true; //
						breakCharacter[(byte) ' '] = true;
						breakCharacter[(byte) '.'] = true;
						breakCharacter[(byte) ','] = true;
						breakCharacter[(byte) '!'] = true;
						breakCharacter[(byte) '?'] = true;
						breakCharacter[(byte) '~'] = true;
						breakCharacter[(byte) '`'] = true;
						breakCharacter[(byte) '@'] = true;
						breakCharacter[(byte) '#'] = true;
						breakCharacter[(byte) '$'] = true;
						breakCharacter[(byte) '%'] = true;
						breakCharacter[(byte) '^'] = true;
						breakCharacter[(byte) '&'] = true;
						breakCharacter[(byte) '*'] = true;
						breakCharacter[(byte) '('] = true;
						breakCharacter[(byte) ')'] = true;
						breakCharacter[(byte) '_'] = true;
						breakCharacter[(byte) '-'] = true;
						breakCharacter[(byte) '+'] = true;
						breakCharacter[(byte) '='] = true;
						breakCharacter[(byte) '{'] = true;
						breakCharacter[(byte) '['] = true;
						breakCharacter[(byte) '}'] = true;
						breakCharacter[(byte) ']'] = true;
						breakCharacter[(byte) '|'] = true;
						breakCharacter[(byte) '\\'] = true;
						breakCharacter[(byte) ':'] = true;
						breakCharacter[(byte) ';'] = true;
						breakCharacter[(byte) '"'] = true;
						breakCharacter[(byte) '\''] = true;
						breakCharacter[(byte) '<'] = true;
						breakCharacter[(byte) '>'] = true;
						breakCharacter[(byte) '/'] = true;

						boolean lastCharacterWasABreak = true;
						String lastString = "";
						int wordCount = 0;
						int numRulesAppliedOnPage = 0;
						ArrayList<String> words = new ArrayList<String>();
						for (int j = 0; j < stringLength; j++) {

							char value = dataString.charAt(j);

							// boolean isLetterOrDigit = Character.isJavaLetterOrDigit(value);
							// System.out.println("CHAR" + "\t" + (int) value + "\t" + value);

							boolean isBreak = breakCharacter[value];

							if (!isBreak) {
								lastString += value;
								lastCharacterWasABreak = false;
							} else if (!lastCharacterWasABreak) {

								if (false) {
									// System.out.println("WORD" + "\t" + wordCount + "\t" + lastString);
									System.out.print(lastString + " ");

									if (wordCount % 20 == 0)
										System.out.println();
								}

								String word = lastString;

								words.add(word);

								lastString = "";
								wordCount++;
							}

							lastCharacterWasABreak = isBreak;
						}

						int numWords = words.size();
						for (int j = 0; j < numWords; j++) {

							String word = words.get(j);

							if (true) {
								String translation = ruleHashMap.get(word);

								if (translation != null) {

									// System.out.println("TRANS" + "\t" + word + "\t" + translation);
									numRulesAppliedOnPage++;
									numRulesAppliedOnVolume++;

									word = translation;
								}
							}

						}

						if (numRulesAppliedOnPage > 0) {
							pageWithErrorCount++;
						}
						pageCount++;

						// System.out.println("numRulesAppliedOnPage = " + numRulesAppliedOnPage);

						double rate = (double) numRulesAppliedOnPage / (double) wordCount;

						overallNumRulesApplied += numRulesAppliedOnPage;

						overallNumWords += wordCount;
						overallNumPages++;

						if (overallNumPages % reportInterval == 0) {

							double volumeWithErrorProbability = (double) volumeWithErrorCount / (double) volumeCount;

							double pageWithErrorProbability = (double) pageWithErrorCount / (double) pageCount;

							double volumeRate = (double) overallNumRulesApplied / (double) volumeCount;

							double pageRate = (double) overallNumRulesApplied / (double) overallNumPages;

							double wordRate = (double) overallNumRulesApplied / (double) overallNumWords;

							System.out.println("overallNumPages              = " + overallNumPages);
							System.out.println("overallNumWords              = " + overallNumWords);
							System.out.println("overallNumRulesApplied       = " + overallNumRulesApplied);
							System.out.println("average hit rate per volume  = " + volumeRate);
							System.out.println("average hit rate per page    = " + pageRate);
							System.out.println("average hit rate per word    = " + wordRate);
							System.out.println("volumeWithErrorProbability   = " + volumeWithErrorProbability);
							System.out.println("pageWithErrorProbability     = " + pageWithErrorProbability);
							System.out.println("volumeCount                  = " + volumeCount);
							System.out.println("volumeWithErrorCount         = " + volumeWithErrorCount);
							System.out.println("pageCount                    = " + pageCount);
							System.out.println("pageWithErrorCount           = " + pageWithErrorCount);

							System.out.println("=====================================================================================================");
						}

					}
					totalBytesRead += lengths[randomIndex];

				}
				fileCount++;

				// if (numChunksRead != lastNumChunksRead) {
				if (overallNumPages % reportInterval == 0) {

					long bytesRead = totalBytesRead - lastTotalBytesRead;
					lastTotalBytesRead = totalBytesRead;

					double durationInSeconds = (System.currentTimeMillis() - startTimeInMS) / 1000.0;
					double bufferDurationInSeconds = (System.currentTimeMillis() - lastBufferTimeInMS) / 1000.0;
					lastBufferTimeInMS = System.currentTimeMillis();

					System.out.println();

					System.out.println("randomIndex = " + randomIndex);
					System.out.println("volumeName = " + volumeName);
					System.out.println("pageName = " + pageName);

					System.out.println("wordCounts.size() = " + wordCounts.size());

					System.out.println("durationInSeconds = " + durationInSeconds);
					System.out.println("totalBytesRead (GB) = " + (double) totalBytesRead / 1000000000);
					System.out.println("overall bytesPerSecond (MB) = " + (double) totalBytesRead / durationInSeconds / 1000000);
					System.out.println("buffer  bytesPerSecond (MB) = " + (double) bytesRead / bufferDurationInSeconds / 1000000);

					System.out.println("bufferSize = " + bufferSize);
					System.out.println("bytesRead  = " + bytesRead);
					System.out.println("fileCount = " + fileCount);
					System.out.println("numEntriesToProcess = " + numEntriesToProcess);

					double fractionComplete = (double) fileCount / numEntriesToProcess;
					double rate = fractionComplete / durationInSeconds;
					double fractionLeft = 1.0 - fractionComplete;
					double timeLeftEstimate = fractionLeft / rate;
					double totalTimeEstimate = 1.0 / rate;
					System.out.println("fractionComplete = " + fractionComplete);
					System.out.println("fractionLeft     = " + fractionLeft);
					System.out.println("timeLeftEstimate = " + timeLeftEstimate + "s");
					System.out.println("timeLeftEstimate = " + timeLeftEstimate / 3600 + "h");
					System.out.println("timeLeftEstimate = " + timeLeftEstimate / 3600 / 24 + "d");
					System.out.println("totalTimeEstimate = " + totalTimeEstimate + "s");
					System.out.println("totalTimeEstimate = " + totalTimeEstimate / 3600 + "h");
					System.out.println("totalTimeEstimate = " + totalTimeEstimate / 3600 / 24 + "d");
				}

			}

			raf.close();
			System.out.println("Finished!");
			System.exit(0);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void doit() {
		try {

			HashMap<String, String> ruleHashMap = (HashMap<String, String>) IO.readObject(dataDirectory + File.separatorChar + "rules.ser");

			// int windowSize = 3;

			for (int windowSize = 4; windowSize <= 4; windowSize++) {

				System.out.println("##################################################################");
				System.out.println("##################################################################");
				System.out.println("##################################################################");
				System.out.println("##################################################################");
				System.out.println("##################################################################");
				System.out.println("##################################################################");
				System.out.println("windowSize = " + windowSize);

				/**************/
				/* read words */
				/**************/

				HashMap<String, CountAndString> highFreqSubstringCounts = new HashMap<String, CountAndString>();
				long highFreqNumSubstrings = 0;
				HashMap<String, CountAndString> lowFreqSubstringCounts = new HashMap<String, CountAndString>();
				long lowFreqNumSubstrings = 0;
				HashMap<String, CountAndString> errorSubstringCounts = new HashMap<String, CountAndString>();
				long errorNumSubstrings = 0;
				HashMap<String, CountAndString> correctSubstringCounts = new HashMap<String, CountAndString>();
				long correctNumSubstrings = 0;

				int minNumChars = windowSize;

				if (useMLRules) {

					BufferedReader bufferedReader = new BufferedReader(new FileReader(new File(dataDirectory + File.separatorChar + "words.sorted.csv")));

					int numFrequentWords = 0;
					while (true) {
						String line = bufferedReader.readLine();
						if (line == null) {
							break;
						}
						String[] parts = line.split("\\t");

						if (parts.length < 2)
							continue;

						int count = Integer.parseInt(parts[0]);

						if ((count < highFreqMinCount && count > lowFreqMaxCount) || (count < lowFreqMinCount)) {

							continue;

						}

						String word = parts[1];

						int numChars = word.length();

						if (numChars < minNumChars) {
							continue;
						}
						if (word.contains("?"))
							continue;

						// System.out.println(count + "\t" + numChars + "\t" + word);

						for (int i = 0; i < numChars - (windowSize - 1); i++) {
							String substring = word.substring(i, i + windowSize);
							// System.out.println(i + "\t" + substring);

							if (count >= highFreqMinCount) {

								CountAndString countAndString = highFreqSubstringCounts.get(substring);

								if (countAndString == null) {

									countAndString = new CountAndString(1, substring);
									highFreqSubstringCounts.put(substring, countAndString);

								} else {
									countAndString.count++;
									highFreqNumSubstrings++;
								}
							}
							if (count >= lowFreqMinCount && count <= lowFreqMaxCount) {

								CountAndString countAndString = lowFreqSubstringCounts.get(substring);

								if (countAndString == null) {

									countAndString = new CountAndString(1, substring);
									lowFreqSubstringCounts.put(substring, countAndString);

								} else {
									countAndString.count++;
									lowFreqNumSubstrings++;
								}
							}

						}

						numFrequentWords++;

						if (numFrequentWords % 1000000 == 0) {
							System.out.println("numFrequentWords = " + numFrequentWords);

						}
					}
					System.out.println("numFrequentWords = " + numFrequentWords);
					System.out.println("substringCounts.size() = " + highFreqSubstringCounts.size());

					bufferedReader.close();

					/*************************************/
					/* report relative ngram frequencies */
					/*************************************/

					{
						Set<String> set = lowFreqSubstringCounts.keySet();
						for (String substring : set) {

							CountAndString lowFreqCountAndString = lowFreqSubstringCounts.get(substring);
							CountAndString highFreqCountAndString = highFreqSubstringCounts.get(substring);

							int lowFreqCount = 0;
							int highFreqCount = 0;

							if (lowFreqCountAndString != null)
								lowFreqCount = lowFreqCountAndString.count;
							if (highFreqCountAndString != null)
								highFreqCount = highFreqCountAndString.count;

							double lowFreqProbability = (double) lowFreqCount / lowFreqNumSubstrings;
							double highFreqProbability = (double) highFreqCount / highFreqNumSubstrings;
							double probabilityDeltaNormalized = (lowFreqProbability - highFreqProbability) * (lowFreqNumSubstrings + highFreqNumSubstrings) / 2.0;

							double ratio = lowFreqProbability / highFreqProbability;
							double score = ratio;

							if (Double.isInfinite(ratio))
								score = 1000000 + lowFreqProbability;

							int countDelta = lowFreqCount - highFreqCount;

							System.out.println(lowFreqCount + "\t" + highFreqCount + "\t" + lowFreqProbability + "\t" + highFreqProbability + "\t" + countDelta + "\t" + probabilityDeltaNormalized + "\t" + ratio + "\t" + substring);
						}
					}
				}

				if (true) {
					HashMap<String, CountAndString> wordCounts = new HashMap<String, CountAndString>();
					int chuckSize = (int) 1e6;

					int[] volumeNameIndices = null;
					int[] pageNameIndices = null;
					long[] positions = null;
					long[] lengths = null;

					String filePath = dataDirectory + File.separatorChar + "ngFlat.v2.tar";

					int bufferSize = -1;

					RandomAccessFile raf = new RandomAccessFile(filePath, "r");

					int numEntries;

					byte[] volumeNameMemory = (byte[]) IO.readObject(dataDirectory + File.separatorChar + "volumeNameMemory.ser");
					int[] volumeNameMemoryPtrs = (int[]) IO.readObject(dataDirectory + File.separatorChar + "volumeNameMemoryPtrs.ser");
					int[] volumeNameMemorySizes = (int[]) IO.readObject(dataDirectory + File.separatorChar + "volumeNameMemorySizes.ser");

					byte[] pageNameMemory = (byte[]) IO.readObject(dataDirectory + File.separatorChar + "pageNameMemory.ser");
					int[] pageNameMemoryPtrs = (int[]) IO.readObject(dataDirectory + File.separatorChar + "pageNameMemoryPtrs.ser");
					int[] pageNameMemorySizes = (int[]) IO.readObject(dataDirectory + File.separatorChar + "pageNameMemorySizes.ser");

					volumeNameIndices = (int[]) IO.readObject(dataDirectory + File.separatorChar + "volumeNameIndices.ser");
					pageNameIndices = (int[]) IO.readObject(dataDirectory + File.separatorChar + "pageNameIndices.ser");
					positions = (long[]) IO.readObject(dataDirectory + File.separatorChar + "positions.ser");
					lengths = (long[]) IO.readObject(dataDirectory + File.separatorChar + "lengths.ser");

					numEntries = volumeNameIndices.length;

					System.out.println("numEntries = " + numEntries);

					byte[] readBuffer = new byte[maxPageNumBytes];
					long totalBytesRead = 0;
					long lastTotalBytesRead = -1;
					int fileCount = 0;

					int[] randomIndices = null;

					if (!sequential) {
						if (trueRandom) {
							randomIndices = Utility.randomIntArray((int) System.currentTimeMillis(), numEntries);
						} else {
							randomIndices = Utility.randomIntArray(123, numEntries);
						}
					}

					long overallNumPages = 0;
					long overallNumWords = 0;
					long overallNumRulesApplied = 0;
					int maxCharacterIndex = 127;
					// long[] characterCounts = new long[maxCharacterIndex];
					// long[][] characterCounts2 = new long[maxCharacterIndex][maxCharacterIndex];
					// long[][][] characterCounts3 = new long[maxCharacterIndex][maxCharacterIndex][maxCharacterIndex];
					// long[][][][] characterCounts4 = new long[maxCharacterIndex][maxCharacterIndex][maxCharacterIndex][maxCharacterIndex];

					long startTimeInMS = System.currentTimeMillis();
					long lastBufferTimeInMS = System.currentTimeMillis();
					numNewNGrams = 0;
					numTotalNGrams = 0;

					long collectionNumErrorDetections = 0;
					long collectionNumPageErrorDetections = 0;
					long collectionNumWords = 0;
					long collectionNumPages = 0;
					double collectionMaxErrorDetectionRate = 0.0;
					int numEntriesToProcess = Math.min(maxNumEntriesToProcess, numEntries);
					for (int i = 0; i < numEntriesToProcess; i++) {

						int randomIndex = -1;

						if (sequential) {
							randomIndex = i;
						} else {
							randomIndex = randomIndices[i];
						}
						// int randomIndex = i;

						String volumeName = new String(volumeNameMemory, volumeNameMemoryPtrs[volumeNameIndices[randomIndex]], volumeNameMemorySizes[volumeNameIndices[randomIndex]]);
						String pageName = new String(pageNameMemory, pageNameMemoryPtrs[pageNameIndices[randomIndex]], pageNameMemorySizes[pageNameIndices[randomIndex]]);

						if (pageName.endsWith(".txt")) {

							raf.seek(positions[randomIndex]);

							int bufferLength = (int) lengths[randomIndex];
							raf.readFully(readBuffer, 0, bufferLength);

							if (true) {

								String dataString = new String(readBuffer, 0, (int) bufferLength, "UTF-8");

								// fix hyphenation
								dataString = dataString.replaceAll("- \\n ", "");
								dataString = dataString.replaceAll("-\\n ", "");
								dataString = dataString.replaceAll("-\\n", "");

								// dataString = dataString.toLowerCase();

								// System.out.println("dataString: " + dataString);

								int stringLength = dataString.length();
								// System.out.println("bufferLength = " + bufferLength);
								// System.out.println("stringLength = " + stringLength);

								boolean[] breakCharacter = new boolean[65536];
								breakCharacter[10] = true; //
								breakCharacter[13] = true; //
								breakCharacter[(byte) ' '] = true;
								breakCharacter[(byte) '.'] = true;
								breakCharacter[(byte) ','] = true;
								breakCharacter[(byte) '!'] = true;
								breakCharacter[(byte) '?'] = true;
								breakCharacter[(byte) '~'] = true;
								breakCharacter[(byte) '`'] = true;
								breakCharacter[(byte) '@'] = true;
								breakCharacter[(byte) '#'] = true;
								breakCharacter[(byte) '$'] = true;
								breakCharacter[(byte) '%'] = true;
								breakCharacter[(byte) '^'] = true;
								breakCharacter[(byte) '&'] = true;
								breakCharacter[(byte) '*'] = true;
								breakCharacter[(byte) '('] = true;
								breakCharacter[(byte) ')'] = true;
								breakCharacter[(byte) '_'] = true;
								breakCharacter[(byte) '-'] = true;
								breakCharacter[(byte) '+'] = true;
								breakCharacter[(byte) '='] = true;
								breakCharacter[(byte) '{'] = true;
								breakCharacter[(byte) '['] = true;
								breakCharacter[(byte) '}'] = true;
								breakCharacter[(byte) ']'] = true;
								breakCharacter[(byte) '|'] = true;
								breakCharacter[(byte) '\\'] = true;
								breakCharacter[(byte) ':'] = true;
								breakCharacter[(byte) ';'] = true;
								breakCharacter[(byte) '"'] = true;
								breakCharacter[(byte) '\''] = true;
								breakCharacter[(byte) '<'] = true;
								breakCharacter[(byte) '>'] = true;
								breakCharacter[(byte) '/'] = true;

								boolean lastCharacterWasABreak = true;
								String lastString = "";
								int wordCount = 0;
								int numRulesAppliedOnPage = 0;
								ArrayList<String> words = new ArrayList<String>();
								for (int j = 0; j < stringLength; j++) {

									char value = dataString.charAt(j);

									// boolean isLetterOrDigit = Character.isJavaLetterOrDigit(value);
									// System.out.println("CHAR" + "\t" + (int) value + "\t" + value);

									boolean isBreak = breakCharacter[value];

									if (!isBreak) {
										lastString += value;
										lastCharacterWasABreak = false;
									} else if (!lastCharacterWasABreak) {

										if (false) {
											// System.out.println("WORD" + "\t" + wordCount + "\t" + lastString);
											System.out.print(lastString + " ");

											if (wordCount % 20 == 0)
												System.out.println();
										}

										String word = lastString;

										words.add(word);

										lastString = "";
										wordCount++;
									}

									lastCharacterWasABreak = isBreak;
								}

								int numWords = words.size();
								int numErrorDetections = 0;
								boolean pageErrorDetection = false;
								for (int wordIndex = 0; wordIndex < numWords; wordIndex++) {

									String word = words.get(wordIndex);

									if (useExpertRules) {

										String translation = ruleHashMap.get(word);

										if (translation != null) {

											// System.out.println("TRANS" + "\t" + word + "\t" + translation);
											numRulesAppliedOnPage++;

											collectionNumErrorDetections++;
											pageErrorDetection = true;
										}
									}

									if (useMLRules) {

										int numChars = word.length();

										if (numChars < minNumChars) {
											continue;
										}
										if (word.contains("?"))
											continue;

										// System.out.println(count + "\t" + numChars + "\t" + word);
										double score = 0;

										double maxDelta = Double.NEGATIVE_INFINITY;
										double zeroCountMaxDelta = 999999;
										double zeroCountDelta = 999999;
										double minErrorScore = 800000;

										int numSubstrings = numChars - (windowSize - 1);
										for (int substringIndex = 0; substringIndex < numSubstrings; substringIndex++) {
											String substring = word.substring(substringIndex, substringIndex + windowSize);
											// System.out.println(i + "\t" + substring);

											{
												CountAndString correctCountAndString = highFreqSubstringCounts.get(substring);

												CountAndString errorCountAndString = lowFreqSubstringCounts.get(substring);

												if (correctCountAndString != null && errorCountAndString != null) {
													int highFreqCount = correctCountAndString.count;
													int lowFreqCount = errorCountAndString.count;

													double lowFreqProbability = (double) lowFreqCount / lowFreqNumSubstrings;
													double highFreqProbability = (double) highFreqCount / highFreqNumSubstrings;
													double probabilityDeltaNormalized = (lowFreqProbability - highFreqProbability) * (lowFreqNumSubstrings + highFreqNumSubstrings) / 2.0;

													if (probabilityDeltaNormalized > maxDelta) {
														maxDelta = probabilityDeltaNormalized;
													}

													score += probabilityDeltaNormalized;
													// score = maxDelta;
												} else {
													score += zeroCountDelta;
													// score = zeroCountMaxDelta;
												}
											}

										}

										score /= numSubstrings;

										if (score > minErrorScore) {
											System.out.print("#");
										}
										System.out.print(word);
										if (score > minErrorScore) {
											System.out.print("#");
										}
										if (wordIndex % 20 == 0)
											System.out.println();
										else
											System.out.print(" ");

										if (score > minErrorScore) {
											numErrorDetections++;
											collectionNumErrorDetections++;
											pageErrorDetection = true;
										}
										if (false)
											System.out.println("EXAMPLE" + "\t" + score + "\t" + word + "");

									}

									collectionNumWords++;

								}

								// System.out.println();

								if (pageErrorDetection)
									collectionNumPageErrorDetections++;
								collectionNumPages++;

								overallNumPages++;

								double errorDetectionRate = (double) numErrorDetections / numWords;
								// System.out.println("errorDetectionRate = " + errorDetectionRate);

								if (errorDetectionRate > collectionMaxErrorDetectionRate) {
									collectionMaxErrorDetectionRate = errorDetectionRate;
									// System.out.println("collectionMaxErrorDetectionRate = " + collectionMaxErrorDetectionRate);
								}

								totalBytesRead += lengths[randomIndex];

							}
							fileCount++;

							// if (numChunksRead != lastNumChunksRead) {
							if (overallNumPages % reportInterval == 0) {

								long bytesRead = totalBytesRead - lastTotalBytesRead;
								lastTotalBytesRead = totalBytesRead;

								double durationInSeconds = (System.currentTimeMillis() - startTimeInMS) / 1000.0;
								double bufferDurationInSeconds = (System.currentTimeMillis() - lastBufferTimeInMS) / 1000.0;
								lastBufferTimeInMS = System.currentTimeMillis();

								System.out.println();

								System.out.println("randomIndex = " + randomIndex);
								System.out.println("volumeName = " + volumeName);
								System.out.println("pageName = " + pageName);

								double collectionErrorDetectionWordRate = (double) collectionNumErrorDetections / collectionNumWords;

								double collectionErrorDetectionPageRate = (double) collectionNumPageErrorDetections / collectionNumPages;

								System.out.println("collectionErrorDetectionWordRate = " + collectionErrorDetectionWordRate);
								System.out.println("collectionErrorDetectionPageRate = " + collectionErrorDetectionPageRate);
								System.out.println("collectionNumErrorDetections     = " + collectionNumErrorDetections);
								System.out.println("collectionNumWords               = " + collectionNumWords);
								System.out.println("collectionNumPageErrorDetections = " + collectionNumPageErrorDetections);
								System.out.println("collectionNumPages               = " + collectionNumPages);

								System.out.println("wordCounts.size() = " + wordCounts.size());

								System.out.println("durationInSeconds = " + durationInSeconds);
								System.out.println("totalBytesRead (GB) = " + (double) totalBytesRead / 1000000000);
								System.out.println("overall bytesPerSecond (MB) = " + (double) totalBytesRead / durationInSeconds / 1000000);
								System.out.println("buffer  bytesPerSecond (MB) = " + (double) bytesRead / bufferDurationInSeconds / 1000000);

								System.out.println("bufferSize = " + bufferSize);
								System.out.println("bytesRead  = " + bytesRead);
								System.out.println("fileCount = " + fileCount);
								System.out.println("numEntriesToProcess = " + numEntriesToProcess);

								double fractionComplete = (double) fileCount / numEntriesToProcess;
								double rate = fractionComplete / durationInSeconds;
								double fractionLeft = 1.0 - fractionComplete;
								double timeLeftEstimate = fractionLeft / rate;
								double totalTimeEstimate = 1.0 / rate;
								System.out.println("fractionComplete = " + fractionComplete);
								System.out.println("fractionLeft     = " + fractionLeft);
								System.out.println("timeLeftEstimate = " + timeLeftEstimate + "s");
								System.out.println("timeLeftEstimate = " + timeLeftEstimate / 3600 + "h");
								System.out.println("timeLeftEstimate = " + timeLeftEstimate / 3600 / 24 + "d");
								System.out.println("totalTimeEstimate = " + totalTimeEstimate + "s");
								System.out.println("totalTimeEstimate = " + totalTimeEstimate / 3600 + "h");
								System.out.println("totalTimeEstimate = " + totalTimeEstimate / 3600 / 24 + "d");
							}

						}

					}

					raf.close();
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void doitOld() {

		try {

			// int windowSize = 3;

			for (int windowSize = 3; windowSize <= 3; windowSize++) {

				System.out.println("##################################################################");
				System.out.println("##################################################################");
				System.out.println("##################################################################");
				System.out.println("##################################################################");
				System.out.println("##################################################################");
				System.out.println("##################################################################");
				System.out.println("windowSize = " + windowSize);

				/**************/
				/* read words */
				/**************/

				HashMap<String, CountAndString> highFreqSubstringCounts = new HashMap<String, CountAndString>();
				long highFreqNumSubstrings = 0;
				HashMap<String, CountAndString> lowFreqSubstringCounts = new HashMap<String, CountAndString>();
				long lowFreqNumSubstrings = 0;
				HashMap<String, CountAndString> errorSubstringCounts = new HashMap<String, CountAndString>();
				long errorNumSubstrings = 0;
				HashMap<String, CountAndString> correctSubstringCounts = new HashMap<String, CountAndString>();
				long correctNumSubstrings = 0;

				BufferedReader bufferedReader = new BufferedReader(new FileReader(new File(dataDirectory + File.separatorChar + "words.sorted.csv")));

				int highFreqMinCount = (int) 1e3;
				int lowFreqMaxCount = (int) 2;
				int lowFreqMinCount = (int) 2;
				int minNumChars = windowSize;
				int numFrequentWords = 0;
				while (true) {
					String line = bufferedReader.readLine();
					if (line == null) {
						break;
					}
					String[] parts = line.split("\\t");

					if (parts.length < 2)
						continue;

					int count = Integer.parseInt(parts[0]);

					if ((count < highFreqMinCount && count > lowFreqMaxCount) || (count < lowFreqMinCount)) {

						continue;

					}

					String word = parts[1];

					int numChars = word.length();

					if (numChars < minNumChars) {
						continue;
					}

					// System.out.println(count + "\t" + numChars + "\t" + word);

					for (int i = 0; i < numChars - (windowSize - 1); i++) {
						String substring = word.substring(i, i + windowSize);
						// System.out.println(i + "\t" + substring);

						if (count >= highFreqMinCount) {

							CountAndString countAndString = highFreqSubstringCounts.get(substring);

							if (countAndString == null) {

								countAndString = new CountAndString(1, substring);
								highFreqSubstringCounts.put(substring, countAndString);

							} else {
								countAndString.count++;
								highFreqNumSubstrings++;
							}
						}
						if (count >= lowFreqMinCount && count <= lowFreqMaxCount) {

							CountAndString countAndString = lowFreqSubstringCounts.get(substring);

							if (countAndString == null) {

								countAndString = new CountAndString(1, substring);
								lowFreqSubstringCounts.put(substring, countAndString);

							} else {
								countAndString.count++;
								lowFreqNumSubstrings++;
							}
						}

					}

					numFrequentWords++;

					if (numFrequentWords % 1000000 == 0) {
						System.out.println("numFrequentWords = " + numFrequentWords);

					}
				}
				System.out.println("numFrequentWords = " + numFrequentWords);
				System.out.println("substringCounts.size() = " + highFreqSubstringCounts.size());

				bufferedReader.close();

				/*************************************/
				/* report relative ngram frequencies */
				/*************************************/

				{
					Set<String> set = lowFreqSubstringCounts.keySet();
					for (String substring : set) {

						CountAndString lowFreqCountAndString = lowFreqSubstringCounts.get(substring);
						CountAndString highFreqCountAndString = highFreqSubstringCounts.get(substring);

						int lowFreqCount = 0;
						int highFreqCount = 0;

						if (lowFreqCountAndString != null)
							lowFreqCount = lowFreqCountAndString.count;
						if (highFreqCountAndString != null)
							highFreqCount = highFreqCountAndString.count;

						double lowFreqProbability = (double) lowFreqCount / lowFreqNumSubstrings;
						double highFreqProbability = (double) highFreqCount / highFreqNumSubstrings;
						double probabilityDeltaNormalized = (lowFreqProbability - highFreqProbability) * (lowFreqNumSubstrings + highFreqNumSubstrings) / 2.0;

						double ratio = lowFreqProbability / highFreqProbability;
						double score = ratio;

						if (Double.isInfinite(ratio))
							score = 1000000 + lowFreqProbability;

						int countDelta = lowFreqCount - highFreqCount;

						System.out.println(lowFreqCount + "\t" + highFreqCount + "\t" + lowFreqProbability + "\t" + highFreqProbability + "\t" + countDelta + "\t" + probabilityDeltaNormalized + "\t" + ratio + "\t" + substring);
					}
				}
				// System.exit(0);

				if (false) {
					/***********************/
					/* read rules 1st pass */
					/***********************/

					HashMap<String, String> ruleHashMap = null;
					boolean useCache = false;

					SimpleTable table = IO.readDelimitedTable(dataDirectory + File.separatorChar + "rules.out", "\t");
					ruleHashMap = new HashMap<String, String>();
					int numRules = 0;
					int numRows = table.numDataRows;
					for (int i = 0; i < numRows; i++) {
						String ngram_id = table.stringMatrix[i][0];
						String ngram_spelling_id = table.stringMatrix[i][1];
						String ngram = table.stringMatrix[i][2];
						String ngram_spelling = table.stringMatrix[i][3];
						String checked = table.stringMatrix[i][4];
						String analysis_include_flag = table.stringMatrix[i][5];

						// System.out.println(checked + "\t" + analysis_include_flag);

						// if (checked.equals("1") && !ngram.equalsIgnoreCase(ngram_spelling)) {
						if (checked.equals("1")) {
							// if (analysis_include_flag.equals("0")) {
							numRules++;
							ruleHashMap.put(ngram, ngram_spelling);

							String errorWord = ngram;
							String correctWord = ngram_spelling;

							int errorWordSize = errorWord.length();

							int correctWordSize = correctWord.length();

							for (int j = 0; j < errorWordSize - (windowSize - 1); j++) {

								String substring = errorWord.substring(j, j + windowSize);

								CountAndString countAndString = errorSubstringCounts.get(substring);

								if (countAndString == null) {

									countAndString = new CountAndString(1, substring);
									errorSubstringCounts.put(substring, countAndString);

								} else {
									countAndString.count++;
								}

								errorNumSubstrings++;

							}

							for (int j = 0; j < correctWordSize - (windowSize - 1); j++) {

								String substring = correctWord.substring(j, j + windowSize);

								CountAndString countAndString = correctSubstringCounts.get(substring);
								if (countAndString == null) {

									countAndString = new CountAndString(1, substring);
									correctSubstringCounts.put(substring, countAndString);

								} else {
									countAndString.count++;
								}

								correctNumSubstrings++;
							}

						}

					}
					System.out.println("numRules = " + numRules);
					System.out.println("errorNumSubstrings = " + errorNumSubstrings);
					System.out.println("correctNumSubstrings = " + correctNumSubstrings);
					IO.writeObject(dataDirectory + File.separatorChar + "rules.ser", ruleHashMap);

					Set<String> set = errorSubstringCounts.keySet();
					for (String substring : set) {

						CountAndString errorCountAndString = errorSubstringCounts.get(substring);
						CountAndString correctCountAndString = correctSubstringCounts.get(substring);

						int errorCount = 0;
						int correctCount = 0;

						if (errorCountAndString != null)
							errorCount = errorCountAndString.count;
						if (correctCountAndString != null)
							correctCount = correctCountAndString.count;

						double errorProbability = (double) errorCount / errorNumSubstrings;
						double correctProbability = (double) correctCount / correctNumSubstrings;
						double delta = (errorProbability - correctProbability) * (errorNumSubstrings + correctNumSubstrings) / 2.0;

						double ratio = errorProbability / correctProbability;

						if (Double.isInfinite(ratio))
							ratio = 999999;

						int countDelta = errorCount - correctCount;

						System.out.println(countDelta + "\t" + delta + "\t" + ratio + "\t" + substring);
					}

					/***********************/
					/* read rules 2nd pass */
					/***********************/

					if (true) {
						ruleHashMap = null;
						useCache = false;

						int tryCount = 0;
						int errorCount = 0;
						if (!useCache) {
							SimpleTable table1 = IO.readDelimitedTable(dataDirectory + File.separatorChar + "rules.out", "\t");
							ruleHashMap = new HashMap<String, String>();
							int numRules1 = 0;
							int numRows1 = table1.numDataRows;
							for (int i = 0; i < numRows1; i++) {
								String ngram_id = table1.stringMatrix[i][0];
								String ngram_spelling_id = table1.stringMatrix[i][1];
								String ngram = table1.stringMatrix[i][2];
								String ngram_spelling = table1.stringMatrix[i][3];
								String checked = table1.stringMatrix[i][4];
								String analysis_include_flag = table1.stringMatrix[i][5];

								// System.out.println(checked + "\t" + analysis_include_flag);

								// if (checked.equals("1") && !ngram.equalsIgnoreCase(ngram_spelling)) {
								if (checked.equals("1")) {
									// if (analysis_include_flag.equals("0")) {
									numRules1++;
									ruleHashMap.put(ngram, ngram_spelling);

									if (true) {
										System.out.println(numRules1 + "\t" + ngram + "\t" + ngram_spelling);
									}

									String errorWord = ngram;
									String correctWord = ngram_spelling;

									int errorWordSize = errorWord.length();

									int correctWordSize = correctWord.length();

									System.out.println(numRules1 + "\t" + errorWordSize + "\t" + errorWord);

									double errorFrequencyProduct = -1;
									double correctFrequencyProduct = -1;

									for (int j = 0; j < errorWordSize - (windowSize - 1); j++) {

										String substring = errorWord.substring(j, j + windowSize);

										CountAndString countAndString = highFreqSubstringCounts.get(substring);

										int frequency = 0;
										if (countAndString != null) {
											frequency = countAndString.count;
										}
										if (true) {
											System.out.println("ERROR\t" + j + "\t" + frequency + "\t" + substring);
										}

										if (j == 0) {
											errorFrequencyProduct = frequency;
										} else {
											errorFrequencyProduct *= frequency;
										}

									}

									for (int j = 0; j < correctWordSize - (windowSize - 1); j++) {

										String substring = correctWord.substring(j, j + windowSize);

										CountAndString countAndString = highFreqSubstringCounts.get(substring);

										int frequency = 0;
										if (countAndString != null) {
											frequency = countAndString.count;
										}
										if (true) {
											System.out.println("CORRECT\t" + j + "\t" + frequency + "\t" + substring);
										}

										if (j == 0) {
											correctFrequencyProduct = frequency;
										} else {
											correctFrequencyProduct *= frequency;
										}

									}

									if (true) {
										System.out.println("errorFrequencyProduct   = " + errorFrequencyProduct);
										System.out.println("correctFrequencyProduct = " + correctFrequencyProduct);
									}

									tryCount++;
									if (errorFrequencyProduct >= correctFrequencyProduct) {
										errorCount++;
									}
									double accuracy = 1.0 - (double) errorCount / (double) tryCount;
									System.out.println("accuracy = " + accuracy);

								}

							}
							System.out.println("numRules = " + numRules1);
							IO.writeObject(dataDirectory + File.separatorChar + "rules.ser", ruleHashMap);
						} else {
							ruleHashMap = (HashMap<String, String>) IO.readObject(dataDirectory + File.separatorChar + "rules.ser");
							int numRules1 = ruleHashMap.size();
							System.out.println("numRules = " + numRules1);
						}

						System.out.println("Finished!");
						System.exit(0);

						HashMap<String, CountAndString> wordCounts = new HashMap<String, CountAndString>();
						int chuckSize = (int) 1e6;

						int[] volumeNameIndices = null;
						int[] pageNameIndices = null;
						long[] positions = null;
						long[] lengths = null;

						String filePath = dataDirectory + File.separatorChar + "ngFlat.v2.tar";

						int bufferSize = -1;

						RandomAccessFile raf = new RandomAccessFile(filePath, "r");

						int numEntries;

						byte[] volumeNameMemory = (byte[]) IO.readObject(dataDirectory + File.separatorChar + "volumeNameMemory.ser");
						int[] volumeNameMemoryPtrs = (int[]) IO.readObject(dataDirectory + File.separatorChar + "volumeNameMemoryPtrs.ser");
						int[] volumeNameMemorySizes = (int[]) IO.readObject(dataDirectory + File.separatorChar + "volumeNameMemorySizes.ser");

						byte[] pageNameMemory = (byte[]) IO.readObject(dataDirectory + File.separatorChar + "pageNameMemory.ser");
						int[] pageNameMemoryPtrs = (int[]) IO.readObject(dataDirectory + File.separatorChar + "pageNameMemoryPtrs.ser");
						int[] pageNameMemorySizes = (int[]) IO.readObject(dataDirectory + File.separatorChar + "pageNameMemorySizes.ser");

						volumeNameIndices = (int[]) IO.readObject(dataDirectory + File.separatorChar + "volumeNameIndices.ser");
						pageNameIndices = (int[]) IO.readObject(dataDirectory + File.separatorChar + "pageNameIndices.ser");
						positions = (long[]) IO.readObject(dataDirectory + File.separatorChar + "positions.ser");
						lengths = (long[]) IO.readObject(dataDirectory + File.separatorChar + "lengths.ser");

						numEntries = volumeNameIndices.length;

						System.out.println("numEntries = " + numEntries);

						byte[] readBuffer = new byte[maxPageNumBytes];
						long totalBytesRead = 0;
						long lastTotalBytesRead = -1;
						int fileCount = 0;

						int[] randomIndices = null;

						if (!sequential) {
							if (trueRandom) {
								randomIndices = Utility.randomIntArray((int) System.currentTimeMillis(), numEntries);
							} else {
								randomIndices = Utility.randomIntArray(123, numEntries);
							}
						}

						long overallNumPages = 0;
						long overallNumWords = 0;
						long overallNumRulesApplied = 0;
						int maxCharacterIndex = 127;
						// long[] characterCounts = new long[maxCharacterIndex];
						// long[][] characterCounts2 = new long[maxCharacterIndex][maxCharacterIndex];
						// long[][][] characterCounts3 = new long[maxCharacterIndex][maxCharacterIndex][maxCharacterIndex];
						// long[][][][] characterCounts4 = new long[maxCharacterIndex][maxCharacterIndex][maxCharacterIndex][maxCharacterIndex];

						long startTimeInMS = System.currentTimeMillis();
						long lastBufferTimeInMS = System.currentTimeMillis();
						numNewNGrams = 0;
						numTotalNGrams = 0;

						double bestNumCorrectionsPerWordRate = 0.0;
						int numEntriesToProcess = Math.min(maxNumEntriesToProcess, numEntries);
						for (int i = 0; i < numEntriesToProcess; i++) {

							int randomIndex = -1;

							if (sequential) {
								randomIndex = i;
							} else {
								randomIndex = randomIndices[i];
							}
							// int randomIndex = i;

							String volumeName = new String(volumeNameMemory, volumeNameMemoryPtrs[volumeNameIndices[randomIndex]], volumeNameMemorySizes[volumeNameIndices[randomIndex]]);
							String pageName = new String(pageNameMemory, pageNameMemoryPtrs[pageNameIndices[randomIndex]], pageNameMemorySizes[pageNameIndices[randomIndex]]);

							if (pageName.endsWith(".txt")) {

								raf.seek(positions[randomIndex]);

								int bufferLength = (int) lengths[randomIndex];
								raf.readFully(readBuffer, 0, bufferLength);

								if (true) {

									String dataString = new String(readBuffer, 0, (int) bufferLength, "UTF-8");

									// fix hyphenation
									dataString = dataString.replaceAll("- \\n ", "");
									dataString = dataString.replaceAll("-\\n ", "");
									dataString = dataString.replaceAll("-\\n", "");

									// dataString = dataString.toLowerCase();

									// System.out.println("dataString: " + dataString);

									int stringLength = dataString.length();
									// System.out.println("bufferLength = " + bufferLength);
									// System.out.println("stringLength = " + stringLength);

									boolean[] breakCharacter = new boolean[65536];
									breakCharacter[10] = true; //
									breakCharacter[13] = true; //
									breakCharacter[(byte) ' '] = true;
									breakCharacter[(byte) '.'] = true;
									breakCharacter[(byte) ','] = true;
									breakCharacter[(byte) '!'] = true;
									breakCharacter[(byte) '?'] = true;
									breakCharacter[(byte) '~'] = true;
									breakCharacter[(byte) '`'] = true;
									breakCharacter[(byte) '@'] = true;
									breakCharacter[(byte) '#'] = true;
									breakCharacter[(byte) '$'] = true;
									breakCharacter[(byte) '%'] = true;
									breakCharacter[(byte) '^'] = true;
									breakCharacter[(byte) '&'] = true;
									breakCharacter[(byte) '*'] = true;
									breakCharacter[(byte) '('] = true;
									breakCharacter[(byte) ')'] = true;
									breakCharacter[(byte) '_'] = true;
									breakCharacter[(byte) '-'] = true;
									breakCharacter[(byte) '+'] = true;
									breakCharacter[(byte) '='] = true;
									breakCharacter[(byte) '{'] = true;
									breakCharacter[(byte) '['] = true;
									breakCharacter[(byte) '}'] = true;
									breakCharacter[(byte) ']'] = true;
									breakCharacter[(byte) '|'] = true;
									breakCharacter[(byte) '\\'] = true;
									breakCharacter[(byte) ':'] = true;
									breakCharacter[(byte) ';'] = true;
									breakCharacter[(byte) '"'] = true;
									breakCharacter[(byte) '\''] = true;
									breakCharacter[(byte) '<'] = true;
									breakCharacter[(byte) '>'] = true;
									breakCharacter[(byte) '/'] = true;

									boolean lastCharacterWasABreak = true;
									String lastString = "";
									int wordCount = 0;
									int numRulesAppliedOnPage = 0;
									ArrayList<String> words = new ArrayList<String>();
									for (int j = 0; j < stringLength; j++) {

										char value = dataString.charAt(j);

										// boolean isLetterOrDigit = Character.isJavaLetterOrDigit(value);
										// System.out.println("CHAR" + "\t" + (int) value + "\t" + value);

										boolean isBreak = breakCharacter[value];

										if (!isBreak) {
											lastString += value;
											lastCharacterWasABreak = false;
										} else if (!lastCharacterWasABreak) {

											if (false) {
												// System.out.println("WORD" + "\t" + wordCount + "\t" + lastString);
												System.out.print(lastString + " ");

												if (wordCount % 20 == 0)
													System.out.println();
											}

											String word = lastString;

											words.add(word);

											lastString = "";
											wordCount++;
										}

										lastCharacterWasABreak = isBreak;
									}

									int numWords = words.size();
									for (int j = 0; j < numWords; j++) {

										String word = words.get(j);

										if (true) {
											String translation = ruleHashMap.get(word);

											if (translation != null) {

												// System.out.println("TRANS" + "\t" + word + "\t" + translation);
												numRulesAppliedOnPage++;

												word = translation;
											}
										}

										if (false) {

											CountAndString countAndString = wordCounts.get(word);

											if (countAndString == null) {

												countAndString = new CountAndString(1, word);
												wordCounts.put(word, countAndString);

												if (false) {
													String wordp3 = null;
													String wordp2 = null;
													String wordp1 = null;
													String words1 = null;
													String words2 = null;
													String words3 = null;

													if (j > 0)
														wordp1 = words.get(j - 1);
													if (j > 1)
														wordp2 = words.get(j - 2);
													if (j > 2)
														wordp3 = words.get(j - 3);
													if (j < numWords - 1)
														words1 = words.get(j + 1);
													if (j < numWords - 2)
														words2 = words.get(j + 2);
													if (j < numWords - 3)
														words3 = words.get(j + 3);

													System.out.println("WORD" + "\t" + wordp3 + " " + wordp2 + " " + wordp1 + " #" + word + "# " + words1 + " " + words2 + " " + words3);
												}

											} else {
												countAndString.count++;
											}
										}

									}

									// System.out.println("numRulesAppliedOnPage = " + numRulesAppliedOnPage);

									double rate = (double) numRulesAppliedOnPage / (double) wordCount;

									if (rate > bestNumCorrectionsPerWordRate) {
										int wordIndex;
										bestNumCorrectionsPerWordRate = rate;

										System.out.println("bestNumCorrectionsPerWordRate: " + bestNumCorrectionsPerWordRate);
										System.out.println("ORIGINAL: ");
										wordIndex = 0;
										for (int j = 0; j < stringLength; j++) {

											char value = dataString.charAt(j);
											boolean isBreak = breakCharacter[value];

											if (!isBreak) {
												lastString += value;
												lastCharacterWasABreak = false;
											} else if (!lastCharacterWasABreak) {

												if (true) {
													System.out.print(lastString + " ");

													wordIndex++;

													if (wordIndex % 20 == 0)
														System.out.println();
												}

												lastString = "";
											}
										}
										System.out.println();
										System.out.println("TRANSLATED: ");

										wordIndex = 0;
										for (int j = 0; j < stringLength; j++) {

											char value = dataString.charAt(j);
											boolean isBreak = breakCharacter[value];

											if (!isBreak) {
												lastString += value;
												lastCharacterWasABreak = false;
											} else if (!lastCharacterWasABreak) {

												String word = lastString;
												String tranlation = ruleHashMap.get(word);

												if (tranlation == null) {
													tranlation = word;
												} else {
													tranlation = "#" + tranlation + "#";
												}

												if (true) {
													System.out.print(tranlation + " ");

													wordIndex++;
													if (wordIndex % 20 == 0)
														System.out.println();
												}

												lastString = "";
											}
										}

									}

									overallNumRulesApplied += numRulesAppliedOnPage;

									overallNumWords += wordCount;
									overallNumPages++;

									if (overallNumPages % reportInterval == 0) {

										double pageRate = (double) overallNumRulesApplied / (double) overallNumPages;

										double wordRate = (double) overallNumRulesApplied / (double) overallNumWords;

										System.out.println("overallNumPages = " + overallNumPages);
										System.out.println("overallNumWords = " + overallNumWords);
										System.out.println("overallNumRulesApplied = " + overallNumRulesApplied);
										System.out.println("average hit rate per page  = " + pageRate);
										System.out.println("average hit rate per word  = " + wordRate);

										System.out.println("=====================================================================================================");
									}

								}
								totalBytesRead += lengths[randomIndex];

							}
							fileCount++;

							// if (numChunksRead != lastNumChunksRead) {
							if (overallNumPages % reportInterval == 0) {

								long bytesRead = totalBytesRead - lastTotalBytesRead;
								lastTotalBytesRead = totalBytesRead;

								double durationInSeconds = (System.currentTimeMillis() - startTimeInMS) / 1000.0;
								double bufferDurationInSeconds = (System.currentTimeMillis() - lastBufferTimeInMS) / 1000.0;
								lastBufferTimeInMS = System.currentTimeMillis();

								System.out.println();

								System.out.println("randomIndex = " + randomIndex);
								System.out.println("volumeName = " + volumeName);
								System.out.println("pageName = " + pageName);

								System.out.println("wordCounts.size() = " + wordCounts.size());

								System.out.println("durationInSeconds = " + durationInSeconds);
								System.out.println("totalBytesRead (GB) = " + (double) totalBytesRead / 1000000000);
								System.out.println("overall bytesPerSecond (MB) = " + (double) totalBytesRead / durationInSeconds / 1000000);
								System.out.println("buffer  bytesPerSecond (MB) = " + (double) bytesRead / bufferDurationInSeconds / 1000000);

								System.out.println("bufferSize = " + bufferSize);
								System.out.println("bytesRead  = " + bytesRead);
								System.out.println("fileCount = " + fileCount);
								System.out.println("numEntriesToProcess = " + numEntriesToProcess);

								double fractionComplete = (double) fileCount / numEntriesToProcess;
								double rate = fractionComplete / durationInSeconds;
								double fractionLeft = 1.0 - fractionComplete;
								double timeLeftEstimate = fractionLeft / rate;
								double totalTimeEstimate = 1.0 / rate;
								System.out.println("fractionComplete = " + fractionComplete);
								System.out.println("fractionLeft     = " + fractionLeft);
								System.out.println("timeLeftEstimate = " + timeLeftEstimate + "s");
								System.out.println("timeLeftEstimate = " + timeLeftEstimate / 3600 + "h");
								System.out.println("timeLeftEstimate = " + timeLeftEstimate / 3600 / 24 + "d");
								System.out.println("totalTimeEstimate = " + totalTimeEstimate + "s");
								System.out.println("totalTimeEstimate = " + totalTimeEstimate / 3600 + "h");
								System.out.println("totalTimeEstimate = " + totalTimeEstimate / 3600 / 24 + "d");
							}

						}

						raf.close();

						{
							System.out.println("wordCounts.size() = " + wordCounts.size());
							Set<Entry<String, CountAndString>> entrySet = wordCounts.entrySet();

							// compute max count
							int maxCount = 0;
							{
								for (Iterator<Entry<String, CountAndString>> iterator = entrySet.iterator(); iterator.hasNext();) {
									Entry<String, CountAndString> entry = (Entry<String, CountAndString>) iterator.next();

									CountAndString countAndString = entry.getValue();

									if (countAndString.count > maxCount) {
										maxCount = countAndString.count;
									}

								}
							}
							int distributionMaxCount = Math.min(maxCount, 100000);
							long[] distribution = new long[distributionMaxCount];
							{
								for (Iterator<Entry<String, CountAndString>> iterator = entrySet.iterator(); iterator.hasNext();) {
									Entry<String, CountAndString> entry = (Entry<String, CountAndString>) iterator.next();

									CountAndString countAndString = entry.getValue();

									if (countAndString.count < distributionMaxCount) {
										distribution[countAndString.count]++;
									}

								}

							}
							System.out.println("DISTRIBUTION");
							for (int index = 0; index < distributionMaxCount; index++) {
								System.out.println(index + "\t" + distribution[index]);
							}

							{
								System.out.println("ENTRIES");
								for (Iterator<Entry<String, CountAndString>> iterator = entrySet.iterator(); iterator.hasNext();) {
									Entry<String, CountAndString> entry = (Entry<String, CountAndString>) iterator.next();
									CountAndString countAndString = entry.getValue();
									System.out.println(countAndString.count + "\t" + countAndString.string);
								}
							}
						}

						// update();

						// IO.writeObject(dataDirectory + File.separatorChar + "wordCounts.ser", wordCounts);
						// } else {
						// wordCounts = (HashMap<String, CountAndString>) IO.readObject(dataDirectory + File.separatorChar + "wordCounts.ser");

					}
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void compileRules() {

		try {

			HashMap<String, String> ruleHashMap = null;
			boolean useCache = false;

			ruleHashMap = null;
			useCache = false;

			int tryCount = 0;
			int errorCount = 0;
			if (!useCache) {
				SimpleTable table1 = IO.readDelimitedTable(dataDirectory + File.separatorChar + ruleSourceFileName, "\t");
				ruleHashMap = new HashMap<String, String>();
				int numRules = 0;
				int numRows1 = table1.numDataRows;
				for (int i = 0; i < numRows1; i++) {
					String ngram = table1.stringMatrix[i][0];
					String ngram_spelling = table1.stringMatrix[i][1];

					numRules++;
					ruleHashMap.put(ngram, ngram_spelling);

					if (true) {
						System.out.println(numRules + "\t" + ngram + "\t" + ngram_spelling);
					}

				}
				System.out.println("numRules = " + numRules);
				IO.writeObject(dataDirectory + File.separatorChar + "rules.ser", ruleHashMap);
			} else {
				ruleHashMap = (HashMap<String, String>) IO.readObject(dataDirectory + File.separatorChar + "rules.ser");
				int numRules1 = ruleHashMap.size();
				System.out.println("numRules = " + numRules1);
			}

			System.out.println("Finished!");

		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void update() {

		// sort new elements
		{
			long startTime = System.currentTimeMillis();
			Arrays.sort(newNGrams, 0, numNewNGrams);
			System.out.println("sort Time = " + (System.currentTimeMillis() - startTime) / 1000.0);
		}

		// merge old and new sorted nGrams //

		{
			long startTime = System.currentTimeMillis();
			int totalNGramIndex = 0;
			int newNGramIndex = 0;
			int newTotalNGramIndex = 0;
			long lastValue = -1L;
			while (true) {
				if ((totalNGramIndex == numTotalNGrams) && (newNGramIndex == numNewNGrams)) {
					break;
				}
				long nextValue = -1;
				if (totalNGramIndex == numTotalNGrams) {
					nextValue = newNGrams[newNGramIndex++];
				} else if (newNGramIndex == numNewNGrams) {
					nextValue = totalNGrams[totalNGramIndex++];
				} else if (newNGrams[newNGramIndex] < totalNGrams[totalNGramIndex]) {
					nextValue = newNGrams[newNGramIndex++];
				} else {
					nextValue = totalNGrams[totalNGramIndex++];
				}
				if (nextValue != lastValue) {
					newTotalNGrams[newTotalNGramIndex++] = nextValue;
					lastValue = nextValue;
				}

			}

			{
				long[] swapTemp = totalNGrams;
				totalNGrams = newTotalNGrams;
				numTotalNGrams = newTotalNGramIndex;

				newTotalNGrams = swapTemp;

				numNewNGrams = 0;
			}
			System.out.println("merge Time = " + (System.currentTimeMillis() - startTime) / 1000.0);
			System.out.println("numTotalNGrams = " + numTotalNGrams);
		}

	}

	public static void testIndex() {
		try {
			// int chuckSize = (int) 1e9;
			int chuckSize = (int) 1e6;

			HashMap<String, CountAndString> wordCounts = new HashMap<String, CountAndString>();
			// HashMap<String, Integer> pageNameToIndex = null;
			// String[] volumeNames = null;
			// String[] pageNames = null;

			int[] volumeNameIndices = null;
			int[] pageNameIndices = null;
			long[] positions = null;
			long[] lengths = null;

			String filePath = "/data/text/ngFlat.v2.tar";

			int bufferSize = -1;
			long startTimeInMS = System.currentTimeMillis();
			long lastBufferTimeInMS = System.currentTimeMillis();

			RandomAccessFile raf = new RandomAccessFile(filePath, "r");

			// volumeNameToIndex = (HashMap<String, Integer>) IO.readObject("/data/volumeNameToIndex.ser");
			// pageNameToIndex = (HashMap<String, Integer>) IO.readObject("/data/pageNameToIndex.ser");

			int numEntries;

			byte[] volumeNameMemory = (byte[]) IO.readObject("/data/volumeNameMemory.ser");
			int[] volumeNameMemoryPtrs = (int[]) IO.readObject("/data/volumeNameMemoryPtrs.ser");
			int[] volumeNameMemorySizes = (int[]) IO.readObject("/data/volumeNameMemorySizes.ser");

			byte[] pageNameMemory = (byte[]) IO.readObject("/data/pageNameMemory.ser");
			int[] pageNameMemoryPtrs = (int[]) IO.readObject("/data/pageNameMemoryPtrs.ser");
			int[] pageNameMemorySizes = (int[]) IO.readObject("/data/pageNameMemorySizes.ser");

			volumeNameIndices = (int[]) IO.readObject("/data/volumeNameIndices.ser");
			pageNameIndices = (int[]) IO.readObject("/data/pageNameIndices.ser");
			positions = (long[]) IO.readObject("/data/positions.ser");
			lengths = (long[]) IO.readObject("/data/lengths.ser");

			numEntries = volumeNameIndices.length;

			System.out.println("numEntries = " + numEntries);

			byte[] data = new byte[10000000];
			long totalBytesRead = 0;
			long lastTotalBytesRead = -1;
			int numChunksRead = 0;
			int lastNumChunksRead = -1;
			int fileCount = 0;

			int[] randomIndices = null;

			if (!sequential) {
				if (trueRandom) {
					randomIndices = Utility.randomIntArray((int) System.currentTimeMillis(), numEntries);
				} else {
					randomIndices = Utility.randomIntArray(123, numEntries);
				}
			}

			long overallCharacterCount = 0;
			long overallWordCount = 0;
			long overallNumPages = 0;
			CountAndString[] countAndStrings = new CountAndString[maxNumWords];
			for (int i = 0; i < numEntries; i++) {

				int randomIndex = -1;

				if (sequential) {
					randomIndex = i;
				} else {
					randomIndex = randomIndices[i];
				}
				// int randomIndex = i;

				String volumeName = new String(volumeNameMemory, volumeNameMemoryPtrs[volumeNameIndices[randomIndex]], volumeNameMemorySizes[volumeNameIndices[randomIndex]]);
				String pageName = new String(pageNameMemory, pageNameMemoryPtrs[pageNameIndices[randomIndex]], pageNameMemorySizes[pageNameIndices[randomIndex]]);

				if (!pageName.endsWith(".txt"))
					continue;
				// if (!pageName.endsWith(".xml"))
				// continue;

				raf.seek(positions[randomIndex]);

				raf.readFully(data, 0, (int) lengths[randomIndex]);
				// in.get(headerData);

				String dataString = new String(data, 0, (int) lengths[randomIndex]);

				dataString = dataString.toLowerCase();
				dataString = dataString.replaceAll("\\\"", " ");
				dataString = dataString.replaceAll("\\:", " ");
				dataString = dataString.replaceAll("\\!", " ");
				dataString = dataString.replaceAll("\\?", " ");
				dataString = dataString.replaceAll("\\;", " ");
				dataString = dataString.replaceAll("\\,", " ");
				dataString = dataString.replaceAll("\\.", " ");
				dataString = dataString.replaceAll("- \\n ", "");
				dataString = dataString.replaceAll("-\\n ", "");
				dataString = dataString.replaceAll("-\\n", "");
				dataString = dataString.replaceAll("\\n", " ");
				dataString = dataString.replaceAll("\\-", " ");
				dataString = dataString.replaceAll("\\_", " ");  // replaced _ from a UTF '-' character... assuming this should have been a '_' --TonyB
				dataString = dataString.replaceAll("\\(", " ");
				dataString = dataString.replaceAll("\\)", " ");

				String[] parts = dataString.split(" ");

				int pageWordCount = 0;
				int pageCharacterCount = 0;

				for (int j = 0; j < parts.length; j++) {
					String word = parts[j];
					if (word.length() > 0) {
						pageWordCount++;
						pageCharacterCount += word.length();

						CountAndString countAndString = wordCounts.get(word);
						if (countAndString == null) {
							countAndString = new CountAndString(0, word);
							countAndStrings[wordCounts.size()] = countAndString;
							wordCounts.put(word, countAndString);
						}
						countAndString.count++;

					}
				}

				if (false) {
					// System.out.println(dataString);
					int wordIndex = 0;
					for (int j = 0; j < parts.length; j++) {
						String word = parts[j];

						if (word.length() > 0) {

							System.out.print(word + " ");

							wordIndex++;
							if (wordIndex % 20 == 0)
								System.out.println();

						}
					}
					System.out.println();
				}

				overallWordCount += pageWordCount;
				overallCharacterCount += pageCharacterCount;
				overallNumPages++;

				if (overallNumPages % reportInterval == 0) {
					double pageCharactersPerWord = (double) pageCharacterCount / pageWordCount;

					double averagePageWordCount = (double) overallWordCount / overallNumPages;
					double averageCharactersPerWord = (double) overallCharacterCount / overallWordCount;

					System.out.println("pageWordCount            = " + pageWordCount);
					System.out.println("pageCharactersPerWord    = " + pageCharactersPerWord);
					System.out.println("overallWordCount         = " + overallWordCount);
					System.out.println("overallNumPages          = " + overallNumPages);
					System.out.println("averagePageWordCount     = " + averagePageWordCount);
					System.out.println("averageCharactersPerWord = " + averageCharactersPerWord);
					System.out.println("wordCounts.size()        = " + wordCounts.size());

					Arrays.sort(countAndStrings, 0, wordCounts.size());

					for (int wordIndex = 0; wordIndex < Math.min(numTopWordsToReport, wordCounts.size()); wordIndex++) {

						double rate = (double) countAndStrings[wordIndex].count / overallWordCount;

						System.out.println(wordIndex + "\t" + countAndStrings[wordIndex].count + "\t" + rate + "\t" + countAndStrings[wordIndex].string);
					}

					System.out.println("=====================================================================================================");
				}

				totalBytesRead += lengths[randomIndex];

				numChunksRead = (int) (totalBytesRead / chuckSize);

				fileCount++;

				// if (numChunksRead != lastNumChunksRead) {
				if (overallNumPages % reportInterval == 0) {

					long bytesRead = totalBytesRead - lastTotalBytesRead;
					lastNumChunksRead = numChunksRead;
					lastTotalBytesRead = totalBytesRead;

					double durationInSeconds = (System.currentTimeMillis() - startTimeInMS) / 1000.0;
					double bufferDurationInSeconds = (System.currentTimeMillis() - lastBufferTimeInMS) / 1000.0;
					lastBufferTimeInMS = System.currentTimeMillis();

					System.out.println();
					System.out.println("randomIndex = " + randomIndex);
					System.out.println("volumeName = " + volumeName);
					System.out.println("pageName = " + pageName);
					System.out.println("durationInSeconds = " + durationInSeconds);
					System.out.println("totalBytesRead (GB) = " + (double) totalBytesRead / 1000000000);
					System.out.println("overall bytesPerSecond (MB) = " + (double) totalBytesRead / durationInSeconds / 1000000);
					System.out.println("buffer  bytesPerSecond (MB) = " + (double) bytesRead / bufferDurationInSeconds / 1000000);

					System.out.println("bufferSize = " + bufferSize);
					System.out.println("bytesRead  = " + bytesRead);
					System.out.println("fileCount = " + fileCount);
				}

			}

			raf.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void createUnsortedNGramIndex() {
		try {

			// final int maxNumEntries = 1000;
			final int maxNumEntries = 999999999;

			int[] volumeNameIndices = null;
			int[] pageNameIndices = null;
			long[] positions = null;
			long[] lengths = null;

			String filePath = "/data/text/ngFlat.v2.tar";

			String unsortedIndexfilePath = "/data/text/unsortedIndexfilePath.bin";

			int bufferSize = (int) 1e6;

			RandomAccessFile raf = new RandomAccessFile(filePath, "r");
			int numEntries;

			RandomAccessFile out = new RandomAccessFile(unsortedIndexfilePath, "rw");

			byte[] volumeNameMemory = (byte[]) IO.readObject("/data/volumeNameMemory.ser");
			int[] volumeNameMemoryPtrs = (int[]) IO.readObject("/data/volumeNameMemoryPtrs.ser");
			int[] volumeNameMemorySizes = (int[]) IO.readObject("/data/volumeNameMemorySizes.ser");

			byte[] pageNameMemory = (byte[]) IO.readObject("/data/pageNameMemory.ser");
			int[] pageNameMemoryPtrs = (int[]) IO.readObject("/data/pageNameMemoryPtrs.ser");
			int[] pageNameMemorySizes = (int[]) IO.readObject("/data/pageNameMemorySizes.ser");

			volumeNameIndices = (int[]) IO.readObject("/data/volumeNameIndices.ser");
			pageNameIndices = (int[]) IO.readObject("/data/pageNameIndices.ser");
			positions = (long[]) IO.readObject("/data/positions.ser");
			lengths = (long[]) IO.readObject("/data/lengths.ser");

			numEntries = volumeNameIndices.length;

			System.out.println("numEntries = " + numEntries);

			byte[] data = new byte[maxPageNumBytes];
			long totalBytesRead = 0;
			long lastTotalBytesRead = -1;
			int fileCount = 0;

			int[] randomIndices = null;

			if (!sequential) {
				if (trueRandom) {
					randomIndices = Utility.randomIntArray((int) System.currentTimeMillis(), numEntries);
				} else {
					randomIndices = Utility.randomIntArray(123, numEntries);
				}
			}

			long overallNumPages = 0;

			long startTimeInMS = System.currentTimeMillis();
			long lastBufferTimeInMS = System.currentTimeMillis();
			numTotalNGrams = 0;
			byte[] outBuffer = new byte[maxPageNumBytes * 2 * 8];

			int numEntriesToProcess = Math.min(numEntries, maxNumEntries);

			long posCount = 0;
			long negCount = 0;
			for (int i = 0; i < numEntriesToProcess; i++) {

				int randomIndex = -1;

				if (sequential) {
					randomIndex = i;
				} else {
					randomIndex = randomIndices[i];
				}
				// int randomIndex = i;

				String volumeName = new String(volumeNameMemory, volumeNameMemoryPtrs[volumeNameIndices[randomIndex]], volumeNameMemorySizes[volumeNameIndices[randomIndex]]);
				String pageName = new String(pageNameMemory, pageNameMemoryPtrs[pageNameIndices[randomIndex]], pageNameMemorySizes[pageNameIndices[randomIndex]]);

				long fileTarPosition = positions[randomIndex];
				raf.seek(fileTarPosition);

				int fileLength = (int) lengths[randomIndex];
				raf.readFully(data, 0, fileLength);

				for (int j = 0; j < fileLength; j++) {
					if (data[j] < 0) {
						System.out.println(data[j]);
						negCount++;
					} else {
						posCount++;
					}
				}
				double ratio = (double) posCount / (posCount + negCount);
				System.out.println(posCount + "\t" + negCount + "\t" + ratio);

				// String string = new String(data, 0, fileLength);
				// int stringLength = string.length();
				//
				// char [] chars = new char[stringLength];
				//
				// string.getChars(0, stringLength, chars, 0);
				//
				// for (int stringIndex = 0; stringIndex < stringLength; stringIndex++) {
				// System.out.print(chars[stringIndex]);
				// }
				//
				// System.out.println(fileLength + "\t" + string.length());

				int outBufferIndex = 0;
				int endNgramFilePosition = (fileLength - nGramWindowSize) + 2;

				for (int filePosition = -1; filePosition < endNgramFilePosition; filePosition++) {

					for (int ngramIndex = 0; ngramIndex < nGramWindowSize; ngramIndex++) {

						if (filePosition == -1 && ngramIndex == 0) {
							outBuffer[outBufferIndex++] = 1; // start of page marker
						} else if (filePosition == endNgramFilePosition - 1 && ngramIndex == nGramWindowSize - 1) {
							outBuffer[outBufferIndex++] = 2; // end of page marker
						} else
							outBuffer[outBufferIndex++] = data[filePosition + ngramIndex];
					}

					long ngramTarPosition = fileTarPosition + filePosition;

					// outBuffer[outBufferIndex++] = (byte) ' ';
					// outBuffer[outBufferIndex++] = (byte) ' ';
					// outBuffer[outBufferIndex++] = (byte) ' ';
					// outBuffer[outBufferIndex++] = (byte) ' ';
					// outBuffer[outBufferIndex++] = (byte) ' ';
					// outBuffer[outBufferIndex++] = (byte) ' ';
					// outBuffer[outBufferIndex++] = (byte) ' ';
					// outBuffer[outBufferIndex++] = (byte) '\n';

					outBuffer[outBufferIndex++] = (byte) (0xff & (ngramTarPosition >> 56));
					outBuffer[outBufferIndex++] = (byte) (0xff & (ngramTarPosition >> 48));
					outBuffer[outBufferIndex++] = (byte) (0xff & (ngramTarPosition >> 40));
					outBuffer[outBufferIndex++] = (byte) (0xff & (ngramTarPosition >> 32));
					outBuffer[outBufferIndex++] = (byte) (0xff & (ngramTarPosition >> 24));
					outBuffer[outBufferIndex++] = (byte) (0xff & (ngramTarPosition >> 16));
					outBuffer[outBufferIndex++] = (byte) (0xff & (ngramTarPosition >> 8));
					outBuffer[outBufferIndex++] = (byte) (0xff & ngramTarPosition);

					// String key = new String(outBuffer, keyIndex, 8);
					// System.out.println("key = " + key);
					// String pos = new String(outBuffer, positionIndex, 8);
					// System.out.println("pos = " + pos);
					// System.out.println("position = " + position);

					fileTarPosition++;
				}

				out.write(outBuffer, 0, outBufferIndex);

				overallNumPages++;

				if (overallNumPages % reportInterval == 0) {

					System.out.println("overallNumPages = " + overallNumPages);
					System.out.println("numNewNGrams = " + numNewNGrams);

					System.out.println("=====================================================================================================");
				}

				totalBytesRead += lengths[randomIndex];

				fileCount++;

				// if (numChunksRead != lastNumChunksRead) {
				if (overallNumPages % reportInterval == 0) {

					long bytesRead = totalBytesRead - lastTotalBytesRead;
					lastTotalBytesRead = totalBytesRead;

					double durationInSeconds = (System.currentTimeMillis() - startTimeInMS) / 1000.0;
					double bufferDurationInSeconds = (System.currentTimeMillis() - lastBufferTimeInMS) / 1000.0;
					lastBufferTimeInMS = System.currentTimeMillis();

					System.out.println();
					System.out.println("randomIndex = " + randomIndex);
					System.out.println("volumeName = " + volumeName);
					System.out.println("pageName = " + pageName);
					System.out.println("durationInSeconds = " + durationInSeconds);
					System.out.println("totalBytesRead (GB) = " + (double) totalBytesRead / 1000000000);
					System.out.println("overall bytesPerSecond (MB) = " + (double) totalBytesRead / durationInSeconds / 1000000);
					System.out.println("buffer  bytesPerSecond (MB) = " + (double) bytesRead / bufferDurationInSeconds / 1000000);

					System.out.println("bufferSize = " + bufferSize);
					System.out.println("bytesRead  = " + bytesRead);
					System.out.println("fileCount = " + fileCount);

					double fractionComplete = (double) fileCount / numEntries;
					double rate = fractionComplete / durationInSeconds;
					double fractionLeft = 1.0 - fractionComplete;
					double timeLeftEstimate = fractionLeft / rate;
					double totalTimeEstimate = 1.0 / rate;
					System.out.println("fractionComplete = " + fractionComplete);
					System.out.println("fractionLeft     = " + fractionLeft);
					System.out.println("timeLeftEstimate = " + timeLeftEstimate);
					System.out.println("totalTimeEstimate = " + totalTimeEstimate);
				}

			}

			raf.close();

			out.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void sortNGramIndex() {
		try {

			String unsortedIndexfilePath = "/data/text/unsortedIndexfilePath.bin";

			RandomAccessFile raf = new RandomAccessFile(unsortedIndexfilePath, "r");

			long fileLength = raf.length();

			int fullBufferSize = (int) Integer.MAX_VALUE;
			// int fullBufferSize = (int) 500e6;
			// int fullBufferSize = (int) 80e6; //
			// int fullBufferSize = (int) 160e6; // 16 // 255595.464
			// int fullBufferSize = (int) 320e6; // 35 // 273439.40400000004
			// int fullBufferSize = (int) 640e6; // 77
			// int fullBufferSize = (int) 1280e6; // 168

			int fullBufferNumNGrams = fullBufferSize / 16;

			int numBuffers = (int) (fileLength / fullBufferSize);

			if (fileLength % fullBufferSize == 0) {
				numBuffers++;
			}

			byte[] data = new byte[fullBufferSize];

			System.out.println("fileLength = " + fileLength);
			System.out.println("fullBufferSize = " + fullBufferSize);
			System.out.println("fullBufferNumNGrams = " + fullBufferNumNGrams);
			System.out.println("numBuffers = " + numBuffers);

			long startTimeInMS = System.currentTimeMillis();
			long fileByteIndex = 0;
			long bytesLeftToRead = fileLength;
			int bufferSize = -1;

			NGramAndPosition[] nGramAndPositions = new NGramAndPosition[fullBufferNumNGrams];
			NGramAndPosition.data = data;
			for (int j = 0; j < fullBufferNumNGrams; j++) {
				nGramAndPositions[j] = new NGramAndPosition(-1);
			}

			for (int i = 0; i < numBuffers; i++) {
				long bufferStartTimeInMS = System.currentTimeMillis();

				if (bytesLeftToRead >= fullBufferSize) {
					bufferSize = fullBufferSize;
				} else {
					bufferSize = (int) bytesLeftToRead;

				}
				System.out.println("bufferSize = " + bufferSize);

				raf.seek(fileByteIndex);

				raf.readFully(data, 0, bufferSize);

				// create sort records
				int bufferNumNGrams = bufferSize / 16;
				System.out.println("bufferNumNGrams = " + bufferNumNGrams);
				for (int j = 0; j < bufferNumNGrams; j++) {
					nGramAndPositions[j].byteIndex = j * 16;
				}

				long sortStartTimeInMS = System.currentTimeMillis();
				Arrays.sort(nGramAndPositions, 0, bufferNumNGrams);
				long sortEndTimeInMS = System.currentTimeMillis();

				double sortTime = (sortEndTimeInMS - sortStartTimeInMS) / 1000.0;
				System.out.println("sortTime = " + sortTime);

				if (false)
					for (int j = 0; j < bufferNumNGrams; j++) {
						String string = new String(data, nGramAndPositions[j].byteIndex, 8);
						System.out.println("string = " + string);
					}

				long bufferEndTimeInMS = System.currentTimeMillis();
				double bufferTime = (bufferEndTimeInMS - bufferStartTimeInMS) / 1000.0;

				double totalTime = numBuffers * bufferTime;

				System.out.println("bufferTime = " + bufferTime);
				System.out.println("totalTime = " + totalTime);

				bytesLeftToRead -= bufferSize;

			}

			raf.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String args[]) {

		// createIndex();

		// compileRules();

		applyRules();

		// doit();

		if (false)
			createUnsortedNGramIndex();

		if (false)
			sortNGramIndex();

	}
}

class CountAndString implements Serializable, Comparable<CountAndString> {
	private static final long serialVersionUID = 1L;
	int count;
	String string;

	CountAndString(int count, String string) {
		this.count = count;
		this.string = string;
	}

	public int compareTo(CountAndString o) {

		if (this.count < o.count)
			return 1;
		if (this.count > o.count)
			return -1;

		return 0;
	}

}

class NGramAndPosition implements Comparable<NGramAndPosition> {

	static byte[] data;

	int byteIndex;

	NGramAndPosition(int byteIndex) {
		this.byteIndex = byteIndex;
	}

	public int compareTo(NGramAndPosition o) {

		for (int i = 0; i < 16; i++) {
			if (data[byteIndex + i] < data[o.byteIndex + i]) {
				return -1;
			}
			if (data[byteIndex + i] > data[o.byteIndex + i]) {
				return 1;
			}
		}
		return 0;
	}
}

/*
 * 
 * 
 * default jvm durationInSeconds = 1140.646 totalBytesRead (GB) = 250.0 bytesPerSecond (MB) = 219.1740469874089 bytesRead = 100000000 count = 2500 fin.available() = 402027520
 * 
 * 
 * 
 * 
 * durationInSeconds = 1033.832 totalBytesRead (GB) = 250.0 bytesPerSecond (MB) = 241.81878680481933 bytesRead = 1000000 count = 250000
 * 
 * 
 * durationInSeconds = 993.126 totalBytesRead (GB) = 250.0 bytesPerSecond (MB) = 251.7303947333974 bytesRead = 10000000 count = 25000
 */

