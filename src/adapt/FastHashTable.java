package adapt;

import java.util.Random;

public class FastHashTable {

	int maxSymbolLength;
	int maxNumSymbols;

	Random random = new Random(123L);
	long hashRandomNumbers[][] = null;

	public FastHashTable(int maxSymbolLength, int maxNumSymbols) {

		this.maxSymbolLength = maxSymbolLength;
		this.maxNumSymbols = maxNumSymbols;

		hashRandomNumbers = new long[maxSymbolLength][maxNumSymbols];
		for (int i = 0; i < maxSymbolLength; i++) {
			for (int j = 0; j < maxNumSymbols; j++) {
				hashRandomNumbers[i][j] = random.nextLong();
			}
		}
	}

	public long computeHashCode(byte bytes[]) {

		int size = bytes.length;

		long hashCode = 0;

		for (int i = 0; i < size; i++) {

			hashCode += hashRandomNumbers[i][bytes[i] + 127];

		}

		return hashCode;
	}


	public long computeHashCode(String string) {

		return computeHashCode(string.getBytes());
	}

}
