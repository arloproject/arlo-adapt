/**
 * University of Illinois/NCSA Open Source License
 * 
 * Copyright (c) 2010, Board of Trustees-University of Illinois. All rights reserved.
 * 
 * Developed by:
 * 
 * David Tcheng Automated Learning Group National Center for Supercomputing Applications
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal with the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to
 * do so, subject to the following conditions:
 * 
 * * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimers.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimers in the documentation and/or other materials provided with the
 * distribution.
 * 
 * * Neither the names of Automated Learning Group, The National Center for Supercomputing Applications, or University of Illinois, nor the names of its contributors may be used to endorse or promote
 * products derived from this Software without specific prior written permission.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 */
package adapt;

public class FoodBiasOptimization {
	public static void main(String[] args) {

		 if (true) {
//		 for (double biasValue = 7f; biasValue < 999999999.0; biasValue += 1f) {
//		for (double biasValue = 16; biasValue <= 99999999; biasValue *= 2) {
//			for (double biasValue = 2; biasValue <= 99999999; biasValue *= 2) {
//			 for (double biasValue = 512f; biasValue < 999999999.0; biasValue *= 2f) {
//			System.out.println("######### biasValue = " + biasValue + "#########");

			/********************/
			/* initialize ADAPT */
			/********************/

			ADAPT adapt = new ADAPT();

			adapt.useExampleSetCache = true;
			adapt.useExampleInputCache = true;

			adapt.numThreads = 8;

			adapt.reportClassificationMatrix = false;

			 adapt.numLearningProblemsToCreate = 1000;
//			adapt.numLearningProblemsToCreate = adapt.numThreads;
			adapt.numTestingGroups = 100;
			// adapt.reportIntervalInProblems = adapt.numThreads;
			adapt.reportIntervalInProblems = adapt.numThreads;

			// adapt.groupIndexIsDirectory = false; // pollen
			adapt.groupIndexIsFile = true; // food

			adapt.dataDirectoryPath = "/data/test/";
			// adapt.imageDirectoryPath = "/data/sbucks/";
			// adapt.imageDirectoryPath = "/data/pollen/";
			adapt.imageDirectoryPath = adapt.dataDirectoryPath + "images/";

			// adapt.subwindowSizeFactor = 1.0f;
			adapt.doInitialImageChecks = false;
			adapt.deleteBadFiles = true;
			// adapt.textureXYResolution = 285; // pollen
			// adapt.minXYResolution = 285; // pollen
			adapt.textureXYResolution = 512;
			adapt.minShapeXYResolution = 512;
			adapt.maxXResolution = 2048;
			adapt.maxYResolution = 2048;
			adapt.maxHeight = 2048;
			adapt.maxWidth = 2048;
			// adapt.maxXResolution = 2592; // android?
			// adapt.maxYResolution = 2592; // android?
			// adapt.maxHeight = 2592; // android?
			// adapt.maxWidth = 2592; // android?
			adapt.useGCForScan = false;
			adapt.useGCForExampleCreation = false;

			adapt.reportGoodClasses = false;
			adapt.minNumExamplesForActiveClass = 1;
			adapt.readClassNamesFromCSVFile();

			/*****************/
			/* PROBLEM SPACE */
			/*****************/

			adapt.numColorsToUse = 3; // food
			// adapt.numColorsToUse = 1; // pollen

			adapt.minNumClassesToUse = -1; // food
			adapt.maxNumClassesToUse = -1; // food
			// adapt.minNumClassesToUse = 2; // pollen
			// adapt.maxNumClassesToUse = 4; // pollen
			adapt.numNumClassesToUse = adapt.maxNumClassesToUse - adapt.minNumClassesToUse + 1;

			adapt.useNaturalTrainingSets = true;
			adapt.minNumTrainingGroupsLog2 = -1;
			adapt.maxNumTrainingGroupsLog2 = -1;
//			adapt.minNumTrainingExamplesLog2 = 0;
//			adapt.maxNumTrainingExamplesLog2 = 13;
			// adapt.minNumTrainingExamplesLog2 = 14;
			// adapt.maxNumTrainingExamplesLog2 = 14;
			// adapt.minNumTrainingExamplesLog2 = 14; // food
			// adapt.maxNumTrainingExamplesLog2 = 14; // food
			// adapt.minNumTrainingExamplesLog2 = 0; // pollen
			// adapt.maxNumTrainingExamplesLog2 = 8; // pollen
			// adapt.numNumTrainingExamplesLog2 = adapt.maxNumTrainingExamplesLog2 - adapt.minNumTrainingExamplesLog2 + 1;
			// adapt.numTestingExamples = 2469; // pollen?
			// adapt.numTestingExamples = 2469;

			// adapt.minNumTrainingExamplesLog2 = (int) (Math.log(adapt.numExamples - adapt.numTestingExamples) / Math.log(2.0));
			// adapt.maxNumTrainingExamplesLog2 = (int) (Math.log(adapt.numExamples - adapt.numTestingExamples) / Math.log(2.0)); // testing
			adapt.numNumTrainingGroupsLog2 = adapt.maxNumTrainingGroupsLog2 - adapt.minNumTrainingGroupsLog2 + 1;

			// adapt.minNumExamplesPerClass = (int) Math.pow(2, adapt.maxNumTrainingExamplesPerClassLog2) + adapt.numTestingExamplesPerClass;
			adapt.minNumExamplesForActiveClass = 1;

//			System.out.println("adapt.minNumTrainingExamplesLog2 = " + adapt.minNumTrainingExamplesLog2);
//			System.out.println("adapt.maxNumTrainingExamplesLog2 = " + adapt.maxNumTrainingExamplesLog2);
//			System.out.println("adapt.numNumTrainingExamplesLog2 = " + adapt.numNumTrainingExamplesLog2);
//			System.out.println("adapt.minNumExamplesPerClass = " + adapt.minNumExamplesForActiveClass);

			adapt.problemGenerationRandomSeed = 123;

			/**************/
			/* BIAS SPACE */
			/**************/

//			 adapt.distanceWeightingPower = biasValue;
				adapt.distanceWeightingPower = (double) 12; // starbucks
//			adapt.distanceWeightingPower = (double) 17; // all fruitveg
			// adapt.distanceWeightingPower = (double) 10; // pollen

//			 adapt.overallIntensityDistributionWeight = biasValue;
			adapt.overallIntensityDistributionWeight = 64f; // starbucks
//			adapt.overallIntensityDistributionWeight = 64f; // fruitveg
			// adapt.overallIntensityDistributionWeight = 16f; // pollen
			adapt.minNumIntensityBins = 2;
			adapt.maxNumIntensityBins = 15;
			adapt.numNumIntensityBins = adapt.maxNumIntensityBins - adapt.minNumIntensityBins + 1;
			adapt.intensityDistributionFeatureIndices = new int[adapt.numNumIntensityBins];
			adapt.intensityDistributionFeatureWeights = new double[adapt.numNumIntensityBins];
			// adapt.intensityDistributionFeatureWeights[10] = (double) 1.0;
			for (int i = 0; i < adapt.numNumIntensityBins; i++) {
				adapt.intensityDistributionFeatureWeights[i] = 1.0f;
			}

//			adapt.overallGrossShapeWeight = biasValue;
			 adapt.overallGrossShapeWeight = 8f; // food
//			 adapt.overallGrossShapeWeight = 8f; // fruitveg
			// adapt.overallGrossShapeWeight = 1f; // pollen
			adapt.minGrossShapeResolution = 1;
			adapt.maxGrossShapeResolution = 11;
			adapt.numGrossShapeResolutions = adapt.maxGrossShapeResolution - adapt.minGrossShapeResolution + 1;
			adapt.grossShapeFeatureIndices = new int[adapt.numGrossShapeResolutions];
			adapt.grossShapeFeatureWeights = new double[adapt.numGrossShapeResolutions];
			// adapt.grossShapeFeatureWeights[10] = (double) 1.0;
			for (int i = 0; i < adapt.numGrossShapeResolutions; i++) {
				adapt.grossShapeFeatureWeights[i] = 1.0f;
			}

//			 adapt.overallTextureWeight = biasValue;
			adapt.overallTextureWeight = 2048; // starbucks
//			adapt.overallTextureWeight = 4096; // fruitveg
			// adapt.overallTextureWeight = 4096; // pollen
			adapt.minTextureLineNumPixels = 1;
			adapt.maxTextureLineNumPixels = 11;
			adapt.numTextureLineNumPixels = adapt.maxTextureLineNumPixels - adapt.minTextureLineNumPixels + 1;
			adapt.textureLineFeatureIndices = new int[adapt.numTextureLineNumPixels];
			adapt.textureLineFeatureWeights = new double[adapt.numTextureLineNumPixels];
			// adapt.textureLineFeatureWeights[(int) 7] = (double) 1.0;
			for (int i = 0; i < adapt.numTextureLineNumPixels; i++) {
				adapt.textureLineFeatureWeights[i] = 1.0f;
			}

			/*******************/
			/* create examples */
			/*******************/

			// int code = Math.abs(adaptParameterMap.toString().hashCode());

			String examplesPath = adapt.dataDirectoryPath + "examples.ser";

			if (adapt.useExampleSetCache && IO.fileExists(examplesPath)) {
				adapt.loadExamples(examplesPath);
			} else {
				adapt.createExamples();
				adapt.saveExamples(examplesPath);
			}

			adapt.setVerbose(true);
			adapt.createLearningProblems();

			int numProblems = adapt.getNumProblems();
			System.out.println("numProblems:" + numProblems);

			//

			//

			long startTime = System.currentTimeMillis();

			adapt.solveSupervisedLearningProblems();

			long endTime = System.currentTimeMillis();

			//

			//

			double durationInSeconds = (endTime - startTime) / (double) 1000.0;
			double problemsPerSecond = numProblems / durationInSeconds;

			System.out.println("numProblems:" + numProblems);
			System.out.println("problemsPerSecond:" + problemsPerSecond);

		}

	}

}
