package adapt;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import com.google.gdata.client.books.BooksService;
import com.google.gdata.client.books.VolumeQuery;
import com.google.gdata.client.calendar.CalendarService;
import com.google.gdata.data.books.VolumeEntry;
import com.google.gdata.data.books.VolumeFeed;
import com.google.gdata.data.calendar.CalendarEntry;
import com.google.gdata.data.calendar.CalendarFeed;
import com.google.gdata.data.dublincore.Title;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;

public class GoogleBooksAPIMain {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		String nGramFilePath = args[0];
		int startAttempNumber = Integer.parseInt(args[1]);
		int sleepTimeInMS = Integer.parseInt(args[2]);
		int maxNumResults = Integer.parseInt(args[3]);

		BufferedReader bufferedReader = null;
		try {
			bufferedReader = new BufferedReader(new FileReader(new File(nGramFilePath)));
		} catch (FileNotFoundException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}

		int attemptNumber = 0;
		// int startAttempNumber = 0;
//		int startAttempNumber = 1467;
//		int startAttempNumber = 1688;
//		int maxNumResults = 100;
		while (true) {
			
			
			String line = null;
			try {
				line = bufferedReader.readLine();
			} catch (IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			if (line == null) {
				break;
			}
			attemptNumber++;

			if (attemptNumber < startAttempNumber) {
				continue;
			}
			


			// exclude no-preview books (by default, they are included)
			// query.setMinViewability(VolumeQuery.MinViewability.PARTIAL);

			BooksService service = new BooksService("exampleCo-exampleApp-1");
			try {
				service.setUserCredentials("", "");
			} catch (AuthenticationException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
			}

			// TODO Auto-generated method stub
			VolumeQuery query = null;
			try {
				query = new VolumeQuery(new URL("http://www.google.com/books/feeds/volumes"));
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


			int startIndex = 1;
			int batchSize = 20;
			int count = 0;
			while (true) {

				try {
					Thread.sleep((long) (sleepTimeInMS));
//					Thread.sleep((long) (400));
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				query.setFullTextQuery(line);
				query.setStartIndex(startIndex);
				query.setMaxResults(batchSize);

				VolumeFeed volumeFeed = null;
				try {
					volumeFeed = service.query(query, VolumeFeed.class);

					int numFound = 0;
					// print title(s) and viewability value if available
					for (VolumeEntry entry : volumeFeed.getEntries()) {

						numFound++;
						count++;

						int numTitles = 0;
						for (Title t : entry.getTitles()) {
							numTitles++;
						}

						System.out.print(entry.getId().replaceAll("http://www.google.com/books/feeds/volumes/", "") + "\t");
						System.out.print(attemptNumber + "\t");
						System.out.print(count + "\t");
						System.out.print(numTitles + "\t");
						System.out.print(System.currentTimeMillis() + "\t");
						System.out.print(line + "\t");
						for (Title t : entry.getTitles()) {
							System.out.print(t.getValue() + "\t");
						}
						System.out.println();

						if (count >= maxNumResults) {
							break;
						}
					}

					if ((count >= maxNumResults) || (numFound != batchSize)) {
						break;
					}
					// System.out.println("count = " + count);
					startIndex += batchSize;

				} catch (Exception e) {
					 System.out.println("Exception = " + e);
					break;
				}

			}
		}
		try {
			bufferedReader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
