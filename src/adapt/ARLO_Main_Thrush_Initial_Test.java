package adapt;

public class ARLO_Main_Thrush_Initial_Test {

	public static void main(String[] args) {

		ARLO arlo = new ARLO();

		// ///////////////////
		// DEFINE SENSORS //
		// ///////////////////

		int numSensors = 6;

		Sensor[] sensors = new Sensor[numSensors];

		for (int i = 0; i < numSensors; i++) {

			switch (i) {
			case 0:
				sensors[i] = new Sensor("CenterDish", "", 697, 458, 0.5);
				break;

			case 1:
				sensors[i] = new Sensor("CenterGround", "", 700, 455, 0);
				break;

			case 2:
				sensors[i] = new Sensor("InnerNorthEast", "", 799, 559, 0);
				break;

			case 3:
				sensors[i] = new Sensor("InnerSouthEast", "", 796, 358, 0);
				break;

			case 4:
				sensors[i] = new Sensor("OuterSouthEast", "", 900, 256, 0);
				break;

			case 5:
				sensors[i] = new Sensor("InnerNorthWest", "", 604, 560, 0);
				break;
			}
		}

		arlo.setSensors(sensors);

		/*****************/
		/* DEFINE EVENTS */
		/*****************/

		// parabola 87 110.5 23.5 ms
		// center ground 84 106.7
		// NE inner 140 117.9 30.9
		// SE inner 116 147.4 60.4
		// SE outer 249 316.3 162
		// NW inner 159 202.0

		int numEvents = 1;

		Event[] events = new Event[numEvents];

		for (int i = 0; i < numEvents; i++) {

			switch (i) {
			case 0:
//				events[i] = new Event(new double[] { 0.1105, 0.1067, 0.1179, 0.1474, 0.3163, 0.2020 });  // fixlater !!!
				break;

			}

		}

		arlo.setEvents(events);

		arlo.setInitialSearchRadius(10.0);
//		arlo.setSearchRadiusReductionFactor(0.9999999);
		arlo.setSearchRadiusReductionFactor(0.99999);
		arlo.setEndSearchRadius(0.01);
		arlo.setSpeedOfSoundInMetersPerSecond(337.642);

		arlo.run();

	}
}

// arlo.setInitialSearchRadius(10.0);
// arlo.setSearchRadiusReductionFactor(0.999999);
// arlo.setEndSearchRadius(0.01);
// arlo.setSpeedOfSoundInMetersPerSecond(337.642);
// eventIndex = 0
// bestScore = -1.3097084236499843
// bestPoint[0] = 767.9233724992735
// bestPoint[1] = 725.2776450286552
// bestPoint[2] = 0.5229589624811366
//
// eventIndex = 1
// bestScore = -0.8202463788532649
// bestPoint[0] = 767.8327572394779
// bestPoint[1] = 722.7842288441545
// bestPoint[2] = 0.0583796195854962
//
// eventIndex = 2
// bestScore = -0.3133080892290631
// bestPoint[0] = 767.8957762706311
// bestPoint[1] = 724.4354680639794
// bestPoint[2] = 0.5821331433796206
