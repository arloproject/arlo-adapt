package adapt;

public class Sensor {

	String name;
	String matchString;
	double[] position;

	Sensor(String name, String matchString, double x, double y, double z) {
		this.name = name;
		this.matchString = matchString;
		this.position = new double[] { x, y, z };
	}
}
