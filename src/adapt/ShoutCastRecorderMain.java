package adapt;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Date;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

public class ShoutCastRecorderMain {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

		int i = 0;
		String dataDirectoryPath = args[i++];
		String station = args[i++];
		double maxDurationInSeconds = Double.parseDouble(args[i++]);
		double maxThreadWaitTimeInSeconds = Double.parseDouble(args[i++]);
		double maxThreadAliveTimeInSeconds = Double.parseDouble(args[i++]);
		boolean play = Boolean.parseBoolean(args[i++]);

		System.out.println(dataDirectoryPath + "\t" + System.currentTimeMillis() + "\t" + new Date() + "\t" + station);

		ShoutCastRecorder scr = new ShoutCastRecorder(dataDirectoryPath, station, maxDurationInSeconds, play);

		long startTime = System.currentTimeMillis();
		scr.start();

		while (true) {

			if (!scr.isAlive()) {
				break;
			}

			long currentTime = System.currentTimeMillis();

			long duration = currentTime - startTime;

			if (!scr.playing && duration > 1000 * maxThreadWaitTimeInSeconds) {
				System.exit(0);
			}

			if (duration > 1000 * maxThreadAliveTimeInSeconds) {
				System.exit(0);
			}

			Thread.sleep(1000);

			System.gc();
		}

	}

}
