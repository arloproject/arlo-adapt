package adapt;

import java.io.*;
import java.util.Iterator;
import java.util.Vector;

// This appears in Core Web Programming from
// Prentice Hall Publishers, and may be freely used
// or adapted. 1997 Marty Hall, hall@apl.jhu.edu.

/**
 * A class that eases the pain of running external processes from applications. Lets you run a program three ways:
 * <OL>
 * <LI><B>exec</B>: Execute the command, returning immediately even if the command is still running. This would be appropriate for printing a file.
 * <LI><B>execWait</B>: Execute the command, but don't return until the command finishes. This would be appropriate for sequential commands where the first depends on the second having finished (e.g.
 * <CODE>javac</CODE> followed by <CODE>java</CODE>).
 * <LI><B>execPrint</B>: Execute the command and print the output. This would be appropriate for the UNIX command <CODE>ls</CODE>.
 * </OL>
 * Note that the PATH is not taken into account, so you must specify the <B>full</B> pathname to the command, and shell builtin commands will not work. For instance, on Unix the above three examples
 * might look like:
 * <OL>
 * <LI>
 * 
 * <PRE>
 * Exec.exec(&quot;/usr/ucb/lpr Some-File&quot;);
 * </PRE>
 * 
 * <LI>
 * 
 * <PRE>
 * Exec.execWait(&quot;/usr/local/bin/javac Foo.java&quot;);
 * Exec.execWait(&quot;/usr/local/bin/java Foo&quot;);
 * </PRE>
 * 
 * <LI>
 * 
 * <PRE>
 * Exec.execPrint(&quot;/usr/bin/ls -al&quot;);
 * </PRE>
 * 
 * </OL>
 * 
 * @author Marty Hall (<A HREF="mailto:hall@apl.jhu.edu"> hall@apl.jhu.edu</A>)
 * @version 1.0 1997
 */

public class ShoutCastRecorderExec {
	// ----------------------------------------------------

	private static boolean verbose = true;

	/**
	 * Determines if the Exec class should print which commands are being executed, and print error messages if a problem is found. Default is true.
	 * 
	 * @param verboseFlag
	 *            true: print messages. false: don't.
	 */

	public static void setVerbose(boolean verboseFlag) {
		verbose = verboseFlag;
	}

	/** Will Exec print status messages? */

	public static boolean getVerbose() {
		return (verbose);
	}

	// ----------------------------------------------------
	/**
	 * Starts a process to execute the command. Returns immediately, even if the new process is still running.
	 * 
	 * @param command
	 *            The <B>full</B> pathname of the command to be executed. No shell builtins (e.g. "cd") or shell meta-chars (e.g. ">") allowed.
	 * @return false if a problem is known to occur, but since this returns immediately, problems aren't usually found in time. Returns true otherwise.
	 */

	public static boolean exec(String command) {
		return (exec(command, false, false));
	}

	// ----------------------------------------------------
	/**
	 * Starts a process to execute the command. Waits for the process to finish before returning.
	 * 
	 * @param command
	 *            The <B>full</B> pathname of the command to be executed. No shell builtins or shell meta-chars allowed.
	 * @return false if a problem is known to occur, either due to an exception or from the subprocess returning a non-zero value. Returns true otherwise.
	 */

	public static boolean execWait(String command) {
		return (exec(command, false, true));
	}

	// ----------------------------------------------------
	/**
	 * Starts a process to execute the command. Prints all output the command gives.
	 * 
	 * @param command
	 *            The <B>full</B> pathname of the command to be executed. No shell builtins or shell meta-chars allowed.
	 * @return false if a problem is known to occur, either due to an exception or from the subprocess returning a non-zero value. Returns true otherwise.
	 */

	public static boolean execPrint(String command) {
		return (exec(command, true, false));
	}

	// ----------------------------------------------------
	// This creates a Process object via
	// Runtime.getRuntime.exec(). Depending on the
	// flags, it may call waitFor on the process
	// to avoid continuing until the process terminates,
	// or open an input stream from the process to read
	// the results.

	static Vector<Process> procs = null;

	private static boolean exec(String command, boolean printResults, boolean wait) {
		if (verbose) {
			System.out.println("Executing '" + command + "'.");
		}
		try {
			// Start running command, returning immediately.
			Process process = Runtime.getRuntime().exec(command);

			procs.add(process);

			// Print the output. Since we read until
			// there is no more input, this causes us
			// to wait until the process is completed
			if (printResults) {
				BufferedInputStream buffer = new BufferedInputStream(process.getInputStream());
				DataInputStream commandResult = new DataInputStream(buffer);
				String s = null;
				try {
					while ((s = commandResult.readLine()) != null)
						System.out.println("Output: " + s);
					commandResult.close();
					if (process.exitValue() != 0) {
						if (verbose)
							printError(command + " -- p.exitValue() != 0");
						return (false);
					}
					// Ignore read errors; they mean process is done
				} catch (Exception e) {
				}

				// If you don't print the results, then you
				// need to call waitFor to stop until the process
				// is completed
			} else if (wait) {
				try {
					System.out.println(" ");
					int returnVal = process.waitFor();
					if (returnVal != 0) {
						if (verbose)
							printError(command);
						return (false);
					}
				} catch (Exception e) {
					if (verbose)
						printError(command, e);
					return (false);
				}
			}
		} catch (Exception e) {
			if (verbose)
				printError(command, e);
			return (false);
		}
		return (true);
	}

	// ----------------------------------------------------

	private static void printError(String command, Exception e) {
		System.out.println("Error doing exec(" + command + "): " + e.getMessage());
		System.out.println("Did you specify the full " + "pathname?");
	}

	private static void printError(String command) {
		System.out.println("Error executing '" + command + "'.");
	}

	// ----------------------------------------------------

	public static void main(String[] args) {

		procs = new Vector<Process>();
		double execFailSleepDuration = 1.0;
		double mandatorySleepDuration = 0.25;
		double minimumWaitDuration = 0.25;
		boolean stagger = true;
		int maxNumAlive = 128; // target 90% memory use by programs
		// String dataDirectoryPath[] = {"/media/My3TBB/data/audio/clips"};
		// String dataDirectoryPath[] = {"/media/My1TB/data/audio/clips"};
		// String dataDirectoryPaths[] = {"/media/My500GB/data/audio/clips"};

		String dataDirectoryPaths[] = { "/media/My3TBA/data/audio/clips11", "/media/My3TBC/data/audio/clips12", "/media/My2TB/data/audio/clips13",
				"/media/My500GBA/data/audio/clips14", "/media/My500GBB/data/audio/clips15", "/media/My500GBC/data/audio/clips16", "/media/My500GBD/data/audio/clips17",
				"/data/audio/clips18", };

		double targetSpeedFactor = maxNumAlive * 5.00;
		// double targetSpeedFactor = 1000.0;
		double maxDurationInSeconds = 1024.0;
		double maxThreadWaitTimeInSeconds = 4.0;
		double maxThreadAliveTimeInSeconds = maxDurationInSeconds + maxThreadWaitTimeInSeconds + 4.0;
		boolean play = false;

		SimpleTable table = null;
		try {
			table = IO.readDelimitedTable("/data/audio/stations/stations.tsv");
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		int numRows = table.getNumRows();

		int numAlive = -1;

		long lastLoopTime = System.currentTimeMillis();
		long startTime = System.currentTimeMillis();
		long initialAllDriveSpace = -1;
		int count = 0;
		while (true) {

			if (stagger) {
				long nextLoopTime = lastLoopTime + (long) (maxDurationInSeconds * 1000 / targetSpeedFactor);
				long sleepTime = nextLoopTime - System.currentTimeMillis();
				if (sleepTime > 0) {
					try {
						Thread.sleep(sleepTime);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				lastLoopTime = System.currentTimeMillis();
			} else {
				try {
					Thread.sleep((long) (minimumWaitDuration * 1000));
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			int randomRow = (int) (numRows * Math.random());

			String station = table.getString(randomRow, 0);

			// execPrint("java -jar /data/audio/stations/rp.jar");
			// exec("java -jar /data/audio/stations/rp.jar", true, true);

			while (true) {
				try {

					long maxDriveSpace = 0;
					int maxDriveIndex = -1;
					File maxSpaceFile = null;
					long allDriveSpace = 0;
					for (int i = 0; i < dataDirectoryPaths.length; i++) {

						File file = new File(dataDirectoryPaths[i]);

						long driveSpace = file.getUsableSpace();

						allDriveSpace += driveSpace;

						if (driveSpace > maxDriveSpace) {
							maxDriveSpace = driveSpace;
							maxDriveIndex = i;
							maxSpaceFile = file;
						}
					}

					if (initialAllDriveSpace == -1)
						initialAllDriveSpace = allDriveSpace;

					double duration = (System.currentTimeMillis() - startTime) / 1000.0;

					double rate = (initialAllDriveSpace - allDriveSpace) / duration;
					double secondsLeft = allDriveSpace / rate;
					double daysLeft = secondsLeft / 3600 / 24;

					System.out.println("maxDriveSpace = " + maxDriveSpace);
					System.out.println("maxDriveIndex = " + maxDriveIndex);
					System.out.println("maxSpaceFile  = " + maxSpaceFile);
					System.out.println("allDriveSpace = " + allDriveSpace);
					System.out.println("rate          = " + rate + "/s");
					System.out.println("secondsLeft   = " + secondsLeft);
					System.out.println("daysLeft      = " + daysLeft);

					boolean result = exec("/usr/lib/jvm/java-6-openjdk/bin/java -Xms11M -Xmx11M -Dfile.encoding=UTF-8 -classpath "
							+ "/data/workspace/adapt/bin:/data/workspace/adapt/lib/ij.jar:" + "/data/workspace/adapt/lib/Jama-1.0.2.jar:"
							+ "/data/workspace/adapt/lib/jl1.0.1.jar:" + "/data/workspace/adapt/lib/tritonus_share.jar:"
							+ "/data/workspace/adapt/lib/mp3spi1.9.5.jar adapt.ShoutCastRecorderMain " + dataDirectoryPaths[maxDriveIndex] + " " + station + " "
							+ maxDurationInSeconds + " " + maxThreadWaitTimeInSeconds + " " + maxThreadAliveTimeInSeconds + " " + play, false, false);
					if (result == true)
						break;
				} catch (Exception e1) {
					try {
						Thread.sleep((long) (execFailSleepDuration * 1000));
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}

			try {
				Thread.sleep((long) (mandatorySleepDuration * 1000));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			System.out.println("==============================================");
			System.out.flush();

			while (true) {

				System.gc();

				numAlive = 0;

				boolean activityFlag = false;
				for (int i = 0; i < procs.size(); i++) {

					Process process = procs.elementAt(i);

					if (process != null) {

						try {
							int exitValue = process.exitValue();
							System.out.print("##" + i + "##" + "\t");
							procs.set(i, null);
						} catch (Exception e) {
							System.out.print(i + "\t");
							numAlive++;
						}

						activityFlag = true;

					}
					if (activityFlag && (i + 1) % 20 == 0) {
						System.out.println();
						activityFlag = false;
					}
				}
				System.out.println();

				System.out.println("numAlive = " + numAlive);

				if (numAlive <= maxNumAlive)
					break;

				try {
					Thread.sleep(4 * 1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				stagger = false;
			}

			System.out.println("==============================================");
			System.out.flush();

			count++;
		}

	}
}
