/*
 Copyright (c) 2012 University of Illinois Board of Trustees
 All rights reserved.
 
 Developed by: David Tcheng
               Illinois Informatics Institute
               University of Illinois at Urbana-Champaign
               www.informatics.illinois.edu/icubed/
               
Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the
"Software"), to deal with the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:
 
 Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimers.
 Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimers in the
documentation and/or other materials provided with the distribution.
 Neither the names of <Name of Development Group, Name of Institution>,
nor the names of its contributors may be used to endorse or promote
products derived from this Software without specific prior written
permission.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 */

package adapt;

import ij.io.ImageWriter;

import java.awt.Graphics;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.HeadlessException;
import java.awt.Rectangle;
import java.awt.SystemColor;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferedImage;
import java.awt.image.ComponentSampleModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.Raster;
import java.awt.image.SampleModel;
import java.awt.image.WritableRaster;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.Vector;

import javax.imageio.ImageIO;

import arlo.Correlation;

public class RawNanoReader implements Serializable {

	private static final long serialVersionUID = 1L;

	final static int numZPlanes = 41;
	final static int minZPlane = -20;

	final static boolean modelGenerationSplitTypeIsMedian = true;
	final static boolean modelGenerationSplitModeIsMean = !modelGenerationSplitTypeIsMedian;

	boolean computeAndShowProbabilityField;

	int modelApplicationStartZ;
	int modelApplicationEndZ;

	boolean useBuffering;
	String modelFilePath;
	String defaultDataPath;
	int numExamplesPerTrial;
	int exampleGenerationheightAndWidthRadiusInPixels;
	int exampleGenerationDepthRadiusInPixels;
	int exampleGenerationNumZPlanesToSearch;
	int exampleGenerationStartZPlaneIndexToSearch;

	int treeGenerationSplitEvaluationSampleSize;
	int treeGenerationSplitEvaluationHalfSampleSize;

	String exampleFileName;

	public String samplePathDirectory = null;
	int numRoundsOfWindowsAvailableOnDisk;

	int firstTrialAvailableOnDisk;

	int exampleGenerationStartTrialIndex;
	int exampleGenerationEndTrialIndex;

	boolean useMaxProbabilityIntegration;
	boolean writeImagesToDiskAfterGeneration;

	int sampleImageSizeX;
	int sampleImageSizeY;
	int actualScreenSizeX;
	int actualScreenSizeY;

	int displayPadX;
	int displayPadY;

	public void setParameters(boolean resetRoundAndSlide, int newStartRound, int newStartSlide, String samplePathDirectory) {

		createRandomSampleFiles = false;

		// modelFilePath = "/media/Patriot/data/pollen/forest.size1.ser"; //
		// testing
		// modelFilePath = "/media/Patriot/data/pollen/forest.size1.ser"; //
		// testing
		// modelFilePath = "/media/Patriot/data/pollen/forest.size4.ser"; //
		// testing
		// modelFilePath = "/media/Patriot/data/pollen/forest.size8.ser"; //
		// testing
		// modelFilePath = "/media/Patriot/data/pollen/forest.size16.ser"; //
		// testing

		computeAndShowProbabilityField = false;

		sampleImageSizeX = 2560;
		sampleImageSizeY = 1600;
		actualScreenSizeX = 1366;
		actualScreenSizeY = 768;

		displayPadX = (sampleImageSizeX - actualScreenSizeX) / 2;
		displayPadY = (sampleImageSizeY - actualScreenSizeY) / 2;

		initialZPlane = 0;

		useMaxProbabilityIntegration = false;

		writeImagesToDiskAfterGeneration = false;

		showStatusLine = true;
		showTaggingRegionBox = true;
		/*
		 * 
		 * 
		 * 
		 * 
		 * duration = 15.618000 numPixelsPerSecond = 255379.433986
		 * 
		 * duration = 21.773000 numPixelsPerSecond = 183186.331695
		 * 
		 * 
		 * duration = 37.060000 numPixelsPerSecond = 107623.205613
		 * 
		 * 
		 * 
		 * duration = 68.180000 numPixelsPerSecond = 58499.794661
		 * 
		 * 
		 * duration = 153.902000 numPixelsPerSecond = 25915.946511
		 * 
		 * 
		 * 
		 * duration = 360.799000 numPixelsPerSecond = 11054.675872
		 * 
		 * duration = 751.903000 numPixelsPerSecond = 5304.561892
		 */
		defaultDataPath = ".";

		exampleGenerationStartTrialIndex = 0;
		// exampleGenerationEndTrialIndex = maxNumSlides * 39; // !!! for all
		// examples
		exampleGenerationEndTrialIndex = maxNumSlides * 10; // !!! for image
															// dump

		treeGenerationSplitEvaluationSampleSize = 1;
		treeGenerationSplitEvaluationHalfSampleSize = Math.max(treeGenerationSplitEvaluationSampleSize / 2, 1);

		/* override existing state parameters if any exist */

		// example generation

		numRoundsOfWindowsAvailableOnDisk = 1000;

		// samplePathDirectory = "/media/Patriot/data/s2560x1600/";
		// samplePathDirectory = "./s2560x1600/";

		this.samplePathDirectory = samplePathDirectory;

		exampleGenerationheightAndWidthRadiusInPixels = 14;
		exampleGenerationDepthRadiusInPixels = 1;

		// exampleGenerationNumZPlanesToSearch = 41;
		// exampleGenerationStartZPlaneIndexToSearch = 0;
		exampleGenerationNumZPlanesToSearch = 33;
		exampleGenerationStartZPlaneIndexToSearch = 10;
		exampleFileName = "results.e40.33z." + numExamplesPerTrial + "x.ser";

		numExamplesPerTrial = 10000;

		// model generation

		// model application
		computeAndShowProbabilityField = false;

		int zAnalysisRadius = 1; // 0 = single Z plane, 20 max
		modelApplicationStartZ = 20 - zAnalysisRadius;
		modelApplicationEndZ = 21 + zAnalysisRadius;

		// int zAnalysisRadius = 0; // 0 = single Z plane, 20 max
		// modelApplicationStartZ = 4;
		// modelApplicationEndZ = 9;

		// modelApplicationStartZ = 0;
		// modelApplicationEndZ = numZplanes;

		// interactive interface
		numBuffersLookAhead = 3;
		numBuffersLookBehind = 2;

		// for machine learning (no interface)
		// numBuffersLookAhead = 0;
		// numBuffersLookBehind = 0;

		if (resetRoundAndSlide) {
			int roundIndex = newStartRound - 1;
			int slideIndex = newStartSlide - 1;
			trialIndex = roundIndex * maxNumSlides + slideIndex;
		}

		useBuffering = true; // !!! controls full screen

		if (answerShownForTrial == null) {
			answerShownForTrial = new boolean[maxNumTrials];
		}
	}

	//

	// decision tree related global memory //

	int numInputFeatures;

	// tree data //

	int maxNumNodes;

	transient int[] nodeChild1Indices;
	transient int[] nodeChild2Indices;

	transient int[] nodeExamplesStartIndices;
	transient int[] nodeExamplesEndIndices;

	transient float[] nodePurities;
	transient float[] nodeClass1Probs;
	// transient float[] nodeClass2Probs;

	transient short[] nodeSplitFeatureIndices;
	transient short[] nodeSplitFeatureValues;

	// forest data //
	transient int numTreesInForest;
	transient int forestTreeNumNodes[];
	transient int[][] forestTreeNodeChild1Indices;
	transient int[][] forestTreeNodeChild2Indices;

	transient int[][] forestTreeNodeExamplesStartIndices;
	transient int[][] forestTreeNodeExamplesEndIndices;

	transient float[][] forestTreeNodePurities;

	transient float[][] forestTreeNodeClass1Probs;
	// transient float[][] forestTreeNodeClass2Probs;

	transient short[][] forestTreeNodeSplitFeatureIndices;
	transient short[][] forestTreeNodeSplitFeatureValues;

	//

	int maxNumSlides = 76;
	int maxNumRounds = 100;
	String[][] viewDataPaths = new String[maxNumRounds][maxNumSlides];

	int trialIndex = 0;
	int maxNumTrials = maxNumSlides * maxNumRounds;
	String[] trialViewPaths = new String[maxNumTrials];
	int[] trialViewRounds = new int[maxNumTrials];
	int[] trialViewSlides = new int[maxNumTrials];
	// int[] trialViewLastZ = new int[maxNumTrials];

	int maxClassificationsPerView = 100;
	int minTagWidth = 10;
	int[] trialNumTags = new int[maxNumTrials];
	int[][] trialClassificationX = new int[maxNumTrials][maxClassificationsPerView];
	int[][] trialClassificationY = new int[maxNumTrials][maxClassificationsPerView];
	int[][] trialClassificationZ = new int[maxNumTrials][maxClassificationsPerView];
	int[][] trialClassificationW = new int[maxNumTrials][maxClassificationsPerView];
	String[][] trialClassificationText = new String[maxNumTrials][maxClassificationsPerView];

	boolean[] answerShownForTrial = new boolean[maxNumTrials];

	int numBuffersLookAhead = -1;
	int numBuffersLookBehind = -1;
	transient byte[][][] trialBuffers = null;

	String stateFileDirectory;
	int stateIndex = 0;

	int betweenFrameSleepTime = 10;

	// int taggableImageWidth = 1024;
	// int taggableImageHeight = 1024;
	int taggableImageWidth = 512;
	int taggableImageHeight = 512;

	// int taggableImageWidth = 1366;
	// int taggableImageHeight = 768;

	int targetImageWidth = 2560;
	int targetImageHeight = 1600;
	// int targetImageWidth = 1366;
	// int targetImageHeight = 768;

	int nanozoomerTileWidth = (int) 3968;
	int nanozoomerTileHeight = (int) 256;

	boolean createRandomSampleFiles = false;
	int numRounds = 100;

	public boolean showStatusLine;
	public boolean showTaggingRegionBox;

	String label = "";
	boolean showWindow = false;
	boolean change = true;
	long bestPathIntegralZPlaneValue;
	int bestPathIntegralZPlaneIndex;
	int pathLengthSampleInterval = 1;

	boolean useCache = false;
	boolean verbose = true;

	public static String imageFilePathDirectory = "/media/Patriot/data/s2560x1600/";
	// public static String imageFilePathDirectory = "/data/image/nanozoomer";
	int initialZPlane;

	// memory allocation
	int maxNumDirectories;
	int maxNumDirectoryEntries;

	// file

	RandomAccessFile raf = null;

	// data format
	boolean bigFormat = false;
	boolean isLittleEndian = true;

	// data

	// constants

	final int numBands = 3;

	// boolean loadCacheNow = false;

	double randomDouble1;
	double randomDouble2;
	double lastRandomDouble1;
	double lastRandomDouble2;

	public int rasterIndex;
	public int zPlaneIndex;
	public int lastZPlaneIndex;
	int minZIndex = -20;
	int maxZIndex = 20;

	int mousePressX;
	int mousePressY;
	int mouseDraggedX;
	int mouseDraggedY;
	boolean markSpot;

	int numberBuffers = 2;
	transient GraphicsDevice device;

	public GraphicsDevice getDevice() {
		return device;
	}

	public void setDevice(GraphicsDevice device) {
		this.device = device;
	}

	transient public NanozoomerImageViewer viewer;
	public boolean changePositionForward;
	public boolean changePositionBackward;
	public boolean deleteLastTag;
	public boolean createExamplePhase1;
	public boolean updateWheelMotion;
	public int wheelRotation;

	public RawNanoReader() {
	}

	transient MyMouseListener mouseListener;
	transient MyMouseWheelListener mouseWheelListener;
	transient MyMouseMotionListener mouseMotionListener;
	transient MyKeyListener keyListener;

	// MyMouseMotionListener mouseMotionListener;

	transient Raster[] rasters;
	transient byte[][] currentBuffer = new byte[numZPlanes][];
	transient byte[][][] allBuffers;
	transient boolean[] allBuffersFilled;

	public boolean exitNow;

	public boolean masterMode;
	public boolean testerMode;

	public boolean showingAnswer;

	RawNanoReader masterRNR;

	public void runExperiment(String stateFileDirectory, String imageFilePathDirectory, boolean masterMode, boolean testerMode, RawNanoReader masterRNR) throws Exception {

		createTrialViewPaths(true);

		System.out.println("reading forrest");

		Object[] treeParts = null;

		if (computeAndShowProbabilityField) {
			treeParts = (Object[]) IO.readObject(modelFilePath);

			System.out.println("done reading forrest");

			int index = 0;
			numTreesInForest = (Integer) treeParts[index++];
			forestTreeNumNodes = (int[]) treeParts[index++];
			maxNumNodes = (Integer) treeParts[index++];
			forestTreeNodeChild1Indices = (int[][]) treeParts[index++];
			forestTreeNodeChild2Indices = (int[][]) treeParts[index++];
			forestTreeNodeExamplesStartIndices = (int[][]) treeParts[index++];
			forestTreeNodeExamplesEndIndices = (int[][]) treeParts[index++];
			forestTreeNodePurities = (float[][]) treeParts[index++];
			forestTreeNodeSplitFeatureIndices = (short[][]) treeParts[index++];
			forestTreeNodeSplitFeatureValues = (short[][]) treeParts[index++];
			forestTreeNodeClass1Probs = (float[][]) treeParts[index++];
			// forestTreeNodeClass2Probs = (float[][]) treeParts[index++];

			IO.writeObject(defaultDataPath + File.separatorChar + "forest.size" + numTreesInForest + ".ser", treeParts);

		}

		this.masterRNR = masterRNR;

		this.stateFileDirectory = stateFileDirectory;
		this.masterMode = masterMode;
		this.testerMode = testerMode;

		long startTime = System.currentTimeMillis();

		trialBuffers = new byte[maxNumTrials][][];
		// currentBuffer = trialBuffers[trialIndex];

		// numBuffersLookAhead = 10; !!!
		// numBuffersLookBehind = 3;

		// create random samples //

		rasters = new Raster[numZPlanes];
		// currentBuffer = new byte[numZPlanes][];
		// pathLengths = new long[numZPlanes];
		// int numSamplePaths = 0;
		// {
		//
		// createTrialViewPaths(true);
		//
		// }

		createRandomSampleFiles = false;
		if (createRandomSampleFiles) {

			currentBuffer = new byte[numZPlanes][];

			int grandOffset = 2; // !!! 0, 1, or 2

			boolean recreateFirstRun = false;
			boolean randomSampling = !recreateFirstRun;

			// int creationStartRoundIndex = 150;
			// int creationEndRoundIndex = 300;

			int creationStartRoundIndex = 254;
			int creationEndRoundIndex = 512;

			// int creationStartSlideIndex = -1;
			int creationStartSlideIndex = 0;

			String slideDirectryListFile = "/u/ncsa/dtcheng/scratch-global/pollen/slideRootNames.xls";

			SimpleTable slideRootNamesTable = IO.readDelimitedTable(slideDirectryListFile, "\t", true);

			maxNumSlides = slideRootNamesTable.numDataRows;
			HashMap<String, Integer> rootNameToSlideIndex = new HashMap<String, Integer>();
			HashMap<Integer, String> slideIndexToRootName = new HashMap<Integer, String>();
			for (int i = 0; i < maxNumSlides; i++) {
				rootNameToSlideIndex.put(slideRootNamesTable.getString(i, 0), i);
				slideIndexToRootName.put(i, slideRootNamesTable.getString(i, 0));
			}

			String rawNanoZoomerImageDirectory = "/u/ncsa/dtcheng/scratch-global/pollen/PollenImages/";
			String existingWindowsImageDirectory = "/u/ncsa/dtcheng/scratch-global/pollen/existingWindows/";

			String newWindowsImageDirectory = "/u/ncsa/dtcheng/scratch-global/pollen/newWindows/";

			/************************************/
			/* find all image files to consider */
			/************************************/

			String[] paths = IO.getPathStrings(rawNanoZoomerImageDirectory);

			int numImagePaths = 0;
			for (int i = 0; i < paths.length; i++) {
				if (!paths[i].endsWith(").ngr"))
					continue;
				numImagePaths++;
			}

			String[] imagePaths = new String[numImagePaths];
			int imagePathIndex = 0;
			for (int i = 0; i < paths.length; i++) {
				if (!paths[i].endsWith(").ngr"))
					continue;
				imagePaths[imagePathIndex++] = paths[i];
			}

			// HashMap<String, Integer> slideNameToIndex = new HashMap<String,
			// Integer>();

			int slideCount = 0;

			for (int i = 0; i < numImagePaths; i++) {

				String imagePath = imagePaths[i];

				int i1 = imagePath.indexOf('(', 0);
				int i2 = imagePath.indexOf(')', i1);
				int i3 = imagePath.indexOf('.', i2);

				String root = imagePath.substring(0, i1);
				int zIndex = Integer.parseInt(imagePath.substring(i1 + 1, i2));

				// System.out.println("imagePath = " + imagePath);
				// System.out.println("root = " + root);
				// System.out.println("zIndex = " + zIndex);

				Integer slideIndex = rootNameToSlideIndex.get(root);

				if (slideIndex == null) {
					slideIndex = slideCount++;
					rootNameToSlideIndex.put(root, slideIndex);
					slideIndexToRootName.put(slideIndex, root);
					System.out.println("Adding new rootName: root       =" + root);
					System.out.println("Adding new rootName: slideIndex =" + slideIndex);
				}
				// System.out.println("slideIndex = " + slideIndex);

			}

			int numSlides = maxNumSlides;

			System.out.println("numSlides = " + numSlides);
			System.out.println("minZIndex = " + minZIndex);
			System.out.println("maxZIndex = " + maxZIndex);
			System.out.println("numZPlanes = " + numZPlanes);

			String[] slideNames = new String[numSlides];

			System.out.println("New " + slideDirectryListFile + " should contain:");
			for (int i = 0; i < numSlides; i++) {
				slideNames[i] = slideIndexToRootName.get(i);
				System.out.println("slideNames[" + i + "]  = " + slideNames[i]);
			}

			@SuppressWarnings("unchecked")
			HashMap<Integer, Boolean>[] cellsTaken = new HashMap[numSlides];
			for (int slideIndex = 0; slideIndex < numSlides; slideIndex++) {
				cellsTaken[slideIndex] = new HashMap<Integer, Boolean>();
			}

			/********************************/
			/* mark windows already created */
			/********************************/

			String[] existingWindowsPaths = IO.getPathStrings(existingWindowsImageDirectory);

			int[][] existingSlideTileIndices = new int[maxNumSlides][creationEndRoundIndex];
			// int[] slideTileCount = new int[maxNumSlides];

			for (int i = 0; i < existingWindowsPaths.length; i++) {

				String path = existingWindowsPaths[i];
				String fileName = IO.getFileNameFromPath(path);

				if (!fileName.endsWith("_buffer.ser")) {
					continue;
				}

				int roundIndex = Integer.parseInt(fileName.substring(1, fileName.indexOf("_s")));

				int index1 = fileName.indexOf("_s") + 2;
				int index2 = fileName.indexOf("_t");
				int slideIndex = Integer.parseInt(fileName.substring(index1, index2));

				int index3 = fileName.indexOf("_t") + 2;
				int index4 = fileName.indexOf("_w");
				int tileIndex = Integer.parseInt(fileName.substring(index3, index4));

				cellsTaken[slideIndex].put(tileIndex, true);

				System.out.printf("slideIndex %d tileIndex %d taken\n", slideIndex, tileIndex);

				existingSlideTileIndices[slideIndex][roundIndex] = tileIndex;

				// slideTileCount[slideIndex]++;

			}
			// System.exit(0);

			//

			boolean firstTime = true;

			for (int roundIndex = creationStartRoundIndex; roundIndex < creationEndRoundIndex; roundIndex++) {

				int startSlideIndex = 0;
				if (firstTime && creationStartSlideIndex != -1) {
					startSlideIndex = creationStartSlideIndex;
				}
				firstTime = false;

				for (int slideIndex = startSlideIndex; slideIndex < numSlides; slideIndex++) {

					String testFilePathString = slideNames[slideIndex] + "(0).ngr";

					int tileIndex = -1;

					if (randomSampling) {
						tileIndex = selectSampleIndex(testFilePathString, cellsTaken[slideIndex], grandOffset);
					} else if (recreateFirstRun) {
						tileIndex = existingSlideTileIndices[slideIndex][roundIndex];
					}

					for (int i = minZIndex; i <= maxZIndex; i++) {
						String filePathString = null;

						if (randomSampling) {
							filePathString = slideNames[slideIndex] + "(" + i + ").ngr";
						}
						if (recreateFirstRun) {
							filePathString = slideNames[slideIndex] + "(" + i + ").ngr";
						}

						rasterIndex = i - minZIndex;
						loadImageData(rasterIndex, filePathString, tileIndex, grandOffset);
					}

					String outputFile = samplePathDirectory + File.separatorChar + "r" + roundIndex + "_s" + slideIndex + "_t" + tileIndex + "_w" + targetImageWidth + "_h" + targetImageHeight + "_buffer.ser";
					IO.writeObject(outputFile, currentBuffer);

				}
			}

			trialIndex = creationStartRoundIndex * numSlides;

			numRounds = creationEndRoundIndex;

			saveStateToFileWithTimestamp();

			System.exit(0);

		}

		createTrialViewPaths(true);

		boolean readForrest = false;

		if (readForrest) {

			System.out.println("reading forrest");

			treeParts = null;

			if (computeAndShowProbabilityField) {
				treeParts = (Object[]) IO.readObject(modelFilePath);

				System.out.println("done reading forrest");

				int index = 0;
				numTreesInForest = (Integer) treeParts[index++];
				forestTreeNumNodes = (int[]) treeParts[index++];
				maxNumNodes = (Integer) treeParts[index++];
				forestTreeNodeChild1Indices = (int[][]) treeParts[index++];
				forestTreeNodeChild2Indices = (int[][]) treeParts[index++];
				forestTreeNodeExamplesStartIndices = (int[][]) treeParts[index++];
				forestTreeNodeExamplesEndIndices = (int[][]) treeParts[index++];
				forestTreeNodePurities = (float[][]) treeParts[index++];
				forestTreeNodeSplitFeatureIndices = (short[][]) treeParts[index++];
				forestTreeNodeSplitFeatureValues = (short[][]) treeParts[index++];
				forestTreeNodeClass1Probs = (float[][]) treeParts[index++];
				// forestTreeNodeClass2Probs = (float[][]) treeParts[index++];

				IO.writeObject(defaultDataPath + File.separatorChar + "forest.size" + numTreesInForest + ".ser", treeParts);

			}
		}

		mouseListener = new MyMouseListener(this);
		mouseWheelListener = new MyMouseWheelListener(this);
		mouseMotionListener = new MyMouseMotionListener(this);
		keyListener = new MyKeyListener(this);

		BufferedImage image = new BufferedImage((int) targetImageWidth, (int) targetImageHeight, BufferedImage.TYPE_3BYTE_BGR);

		viewer = new NanozoomerImageViewer(image, this);

		viewer.taggableWidth = taggableImageWidth;
		viewer.taggableHeight = taggableImageHeight;

		viewer.createFrame((int) targetImageWidth, (int) targetImageHeight, null);

		zPlaneIndex = initialZPlane;
		rasterIndex = zPlaneIndex - minZIndex;
		lastZPlaneIndex = Integer.MIN_VALUE;

		// loadCacheNow = false;

		mousePressX = -1;
		mousePressY = -1;
		mouseDraggedX = -1;
		mouseDraggedY = -1;

		// load up initial

		// start thread to load buffers in background

		BackgroundBufferLoader bbl = new BackgroundBufferLoader(this);
		bbl.start();
		BackgroundSaveState bss = new BackgroundSaveState(this);
		bss.start();

		loadCurrentBuffer();

		bestPathIntegralZPlaneValue = Long.MIN_VALUE;

		changePositionForward = false;
		changePositionBackward = false;
		// loadCacheNow = true;

		change = true;

		exitNow = false;

		showingAnswer = false;

		while (true) {

			if (exitNow) {

				bbl.enable = false;
				bss.enable = false;

				stateIndex++;

				saveStateToFileWithTimestamp();

				// System.exit(0);

				viewer.mainFrame.dispose();

				while (bbl.isAlive()) {
					System.out.println("waiting for background buffer loader to exit");
					Thread.sleep(1000);
				}
				while (bss.isAlive()) {
					System.out.println("waiting for background state saver to exit");
					Thread.sleep(1000);
				}
				break;

			}

			if (changePositionForward || changePositionBackward) {

				if (changePositionForward) {

					if (testerMode) {

						if (showingAnswer) {

							trialIndex++;
							if (trialIndex == numRoundsOfWindowsAvailableOnDisk) {
								trialIndex = numRoundsOfWindowsAvailableOnDisk - 1;
							}

							showingAnswer = false;

						} else {
							showingAnswer = true;
							answerShownForTrial[trialIndex] = true;
						}

					}

					else {

						while (true) {
							trialIndex++;
							if (countPollenPerSlide(trialViewSlides[trialIndex]) < 300)
								break;
						}

						if (trialIndex == numRoundsOfWindowsAvailableOnDisk) {
							trialIndex = numRoundsOfWindowsAvailableOnDisk - 1;
						}
					}
				}

				if (changePositionBackward) {

					if (testerMode) {
						if (answerShownForTrial[trialIndex])
							if (showingAnswer) {
								showingAnswer = false;
							} else {
								showingAnswer = true;
							}
					} else {

						while (true) {
							trialIndex--;
							if (countPollenPerSlide(trialViewSlides[trialIndex]) < 300)
								break;
						}

						if (trialIndex < firstTrialAvailableOnDisk) {
							trialIndex = firstTrialAvailableOnDisk;
						}
					}
				}

				loadCurrentBuffer();

				for (int i = 0; i < numZPlanes; i++) {
					rasters[i] = null;
				}
				changePositionForward = false;
				changePositionBackward = false;
				markSpot = false;
				zPlaneIndex = 0;
				rasterIndex = zPlaneIndex - minZIndex;
				mousePressX = -1;
				mousePressY = -1;
				mouseDraggedX = -1;
				mouseDraggedY = -1;
				label = "";
			}

			if (updateWheelMotion) {

				// System.out.println("updateWheelMotion");

				zPlaneIndex += wheelRotation;
				wheelRotation = 0;

				if (zPlaneIndex < minZIndex)
					zPlaneIndex = minZIndex;

				if (zPlaneIndex > maxZIndex)
					zPlaneIndex = maxZIndex;

				rasterIndex = zPlaneIndex - minZIndex;

				displayImage("sample_" + trialIndex, viewer, image, rasters, currentBuffer, rasterIndex);

				updateWheelMotion = false;
			}

			if (!answerShownForTrial[trialIndex] && deleteLastTag) {

				if (trialNumTags[trialIndex] > 0) {
					trialNumTags[trialIndex]--;
				}

				rasterIndex = zPlaneIndex - minZIndex;

				displayImage("sample_" + trialIndex, viewer, image, rasters, currentBuffer, rasterIndex);

				deleteLastTag = false;
			}

			else {

				if (change) {

					rasterIndex = zPlaneIndex - minZIndex;

					displayImage("sample_" + trialIndex, viewer, image, rasters, currentBuffer, rasterIndex);
					change = false;
				}
			}

			Thread.sleep(betweenFrameSleepTime);

		}

		long endTime = System.currentTimeMillis();

		double elapsedTime = (endTime - startTime) / 1000.0;

		System.out.println("elapsedTime = " + elapsedTime);

		System.out.println("finished!!!");
	}

	void createTrialViewPaths(boolean verbose) {

		String[] filePaths = IO.getPathStrings(samplePathDirectory);

		String[][] viewDataPaths = new String[maxNumRounds][maxNumSlides];

		for (int i = 0; i < filePaths.length; i++) {

			String filePath = filePaths[i];

			if (!filePath.endsWith("buffer.ser"))
				continue;

			String fileName = filePath.substring(filePath.lastIndexOf("/") + 1);
			if (verbose)
				System.out.println("fileName = " + fileName);

			String[] parts = fileName.split("_");

			int roundIndex = Integer.parseInt(parts[0].substring(1));
			int slideIndex = Integer.parseInt(parts[1].substring(1));
			int tileIndex = Integer.parseInt(parts[2].substring(1));
			int viewWidth = Integer.parseInt(parts[3].substring(1));
			int viewHeight = Integer.parseInt(parts[4].substring(1));

			if (verbose) {
				System.out.println("roundIndex = " + roundIndex);
				System.out.println("slideIndex = " + slideIndex);
				System.out.println("tileIndex  = " + tileIndex);
				System.out.println("viewWidth  = " + viewWidth);
				System.out.println("viewHeight = " + viewHeight);
			}

			viewDataPaths[roundIndex][slideIndex] = filePath;
		}

		{
			int index = 0;
			for (int roundIndex = 0; roundIndex < numRoundsOfWindowsAvailableOnDisk; roundIndex++) {

				for (int slideIndex = 0; slideIndex < maxNumSlides; slideIndex++) {

					trialViewPaths[index] = viewDataPaths[roundIndex][slideIndex];
					trialViewRounds[index] = roundIndex;
					trialViewSlides[index] = slideIndex;
					// System.out.println("############trialViewPaths[index] = " + trialViewPaths[index]);

					index++;

				}

			}
		}
	}

	void saveStateToFileWithTimestamp() {

		String fileName = "";

		if (masterMode)
			fileName += "master";
		if (testerMode)
			fileName += "tester";

		fileName += "_state_" + stateIndex + "_" + System.currentTimeMillis() + ".ser";

		String pathString = stateFileDirectory + File.separatorChar + fileName;
		IO.writeObject(pathString, this);

		System.out.println("state saved to filename: " + fileName);
	}

	void saveLastStateToFile() {

		String fileName = "";

		if (masterMode)
			fileName += "master";
		if (testerMode)
			fileName += "tester";

		fileName += "_state_" + stateIndex + "_lastState.ser";

		String pathString = stateFileDirectory + File.separatorChar + fileName;
		IO.writeObject(pathString, this);

		System.out.println("state saved to filename: " + fileName);
	}

	void savesStateToFile(String pathString) {

		IO.writeObject(pathString, this);

		System.out.println("state saved to pathString: " + pathString);
	}

	private void loadCurrentBuffer() throws InterruptedException {
		while (trialBuffers[trialIndex] == null) {

			System.out.println("waiting for current buffer to load...");

			Thread.sleep(1000);

		}

		currentBuffer = trialBuffers[trialIndex];
	}

	private void displayImage(String imageText, NanozoomerImageViewer viewer, BufferedImage image, Raster[] rasters, byte[][] buffers, int rasterIndex) throws FileNotFoundException, IOException {
		// note: assumes all byte ordering is LittleEndian!! (low order bytes
		// first)

		if (rasters[rasterIndex] == null) {

			SampleModel sampleModel = new ComponentSampleModel(DataBuffer.TYPE_BYTE, targetImageWidth, targetImageHeight, numBands, targetImageWidth * numBands, new int[] { 0, 1, 2 });

			byte[] activeRasterBytes = buffers[rasterIndex];

			DataBuffer buffer = new DataBufferByte(activeRasterBytes, activeRasterBytes.length);
			Raster raster = Raster.createRaster(sampleModel, buffer, null);

			viewer.image = image;

			rasters[rasterIndex] = raster;

			if (computeAndShowProbabilityField) {

				computeProbabilityField(buffers, activeRasterBytes);

			}
		}

		viewer.imageText = imageText;

		image.setData(rasters[rasterIndex]);

		viewer.repaint();

	}

	private void computeProbabilityField(byte[][] buffers, byte[] activeRasterBytes) {

		double[][] probFieldSums = new double[targetImageWidth][targetImageHeight];

		int skipFactor = 1;

		int windowWidth = 27;
		int halfWindowWidth = windowWidth / 2;
		numInputFeatures = windowWidth * windowWidth * 3;
		byte[] workInputFeatureValues = new byte[numInputFeatures];
		double[] classProbabilities = new double[2]; // !!! fix later

		// int zRadius = 8;
		// int startZ = 20 - zRadius;
		// int endZ = 21 + zRadius;

		int numZplanesUsed = modelApplicationEndZ - modelApplicationStartZ;

		// int startX = 922;
		int startX = halfWindowWidth;
		int endX = targetImageWidth - halfWindowWidth;
		int startY = halfWindowWidth;
		int endY = targetImageHeight - halfWindowWidth;

		long lastReportTimeInMS = System.currentTimeMillis();
		int reportTimeIntervalInSeconds = 10;

		int numProblemsSolved = 0;
		int numProblems = (modelApplicationEndZ - modelApplicationStartZ) * (endX - startX) / skipFactor;
		System.out.printf("numProblems = %d\n", numProblems);
		long startTime = System.currentTimeMillis();
		for (int z = modelApplicationStartZ; z < modelApplicationEndZ; z++) {
			System.out.printf("analyzing z plane index %d\n", z);
			long zPlaneStartTime = System.currentTimeMillis();

			byte[] bytes = buffers[z];

			for (int x = startX; x < endX; x += skipFactor) {

				long time = System.currentTimeMillis();

				if (time - lastReportTimeInMS > 1000 * reportTimeIntervalInSeconds) {

					double duration = (time - startTime) / 1000.0;

					double fractionDone = (double) numProblemsSolved / numProblems;
					double fractionLeft = 1.0 - fractionDone;
					double timeLeft = fractionLeft / (fractionDone / duration);

					System.out.printf("duration     = %f\n", duration);
					System.out.printf("fractionDone = %f\n", fractionDone);
					System.out.printf("fractionLeft = %f\n", fractionLeft);
					System.out.printf("timeLeft     = %f\n", timeLeft);

					Date eta = new Date((long) (time + timeLeft * 1000));

					System.out.printf("eta = %s\n", eta);

					lastReportTimeInMS = time;

				}

				// System.out.printf("x = %d\n", x);

				for (int y = startY; y < endY; y += skipFactor) {

					int inputFeatureIndex = 0;

					int startWindowX = x - halfWindowWidth;
					int endWindowX = startWindowX + windowWidth;
					int startWindowY = y - halfWindowWidth;
					int endWindowY = startWindowY + windowWidth;

					for (int windowX = startWindowX; windowX < endWindowX; windowX++) {

						for (int windowY = startWindowY; windowY < endWindowY; windowY++) {

							int pixelBytePtr = windowY * targetImageWidth * numBands + windowX * numBands;

							workInputFeatureValues[inputFeatureIndex++] = bytes[pixelBytePtr++];
							workInputFeatureValues[inputFeatureIndex++] = bytes[pixelBytePtr++];
							workInputFeatureValues[inputFeatureIndex++] = bytes[pixelBytePtr++];

						}
					}

					// evaluate model
					evaluateForrest(workInputFeatureValues, classProbabilities);

					if (useMaxProbabilityIntegration) {

						if (classProbabilities[1] > probFieldSums[x][y]) {
							probFieldSums[x][y] = classProbabilities[1];
						}

					} else {

						probFieldSums[x][y] += classProbabilities[1];
					}

				}

				numProblemsSolved++;

			}

			System.out.printf("done analyzing z plane index %d\n", z);
			long endTime = System.currentTimeMillis();
			double duration = (endTime - zPlaneStartTime) / 1000.0;
			System.out.printf("duration = %f\n", duration);

			double numPixelsPerSecond = (endX - startX) * (endY - startY) / duration;
			System.out.printf("numPixelsPerSecond = %f\n", numPixelsPerSecond);

		}

		for (int x = halfWindowWidth; x < targetImageWidth - halfWindowWidth; x += skipFactor) {

			for (int y = halfWindowWidth; y < targetImageHeight - halfWindowWidth; y += skipFactor) {

				byte intensityValue;

				if (useMaxProbabilityIntegration) {
					intensityValue = (byte) (probFieldSums[x][y] * 255);
				} else {
					intensityValue = (byte) (probFieldSums[x][y] / numZplanesUsed * 255);
				}

				int pixelBytePtr = y * targetImageWidth * numBands + x * numBands;

				activeRasterBytes[pixelBytePtr++] = intensityValue;
				activeRasterBytes[pixelBytePtr++] = intensityValue;
				activeRasterBytes[pixelBytePtr++] = intensityValue;

			}
		}
	}

	private int selectSampleIndex(String filePathString, HashMap<Integer, Boolean> cellsTaken, int grandOffset) throws FileNotFoundException, IOException {
		// note: assumes all byte ordering is LittleEndian!! (low order bytes
		// first)

		raf = new RandomAccessFile(filePathString, "r");

		long length = raf.length();

		if (verbose) {
			System.out.println("length = " + length);
		}

		// determine byte order
		{
			// read unsigned integer low order byte first
			int formatSpecifier2 = raf.read();
			int formatSpecifier1 = raf.read();

			if (verbose) {
				System.out.println("formatSpecifier1 = " + (char) formatSpecifier1);
				System.out.println("formatSpecifier2 = " + (char) formatSpecifier2);
			}

			if (formatSpecifier1 != 'N' || formatSpecifier2 != 'G') {
				Thread.dumpStack();
				System.exit(1);
			}
		}

		int majorVersion = 0;
		int minorVersion = 0;
		{
			minorVersion = raf.read();
			majorVersion = raf.read();
		}
		if (verbose) {
			System.out.println("majorVersion = " + majorVersion);
			System.out.println("minorVersion = " + minorVersion);
		}

		long totalImageWidth = readUnsignedInt();
		long totalImageHeight = readUnsignedInt();

		if (verbose) {
			System.out.println("totalImageWidth  = " + totalImageWidth);
			System.out.println("totalImageHeight = " + totalImageHeight);
		}

		int imageTileImageWidth = (int) readUnsignedInt();
		int imageTileImageHeight = (int) readUnsignedInt();

		if (verbose) {
			System.out.println("imageTileImageHeight  = " + imageTileImageHeight);
			System.out.println("imageTileImageWidth = " + imageTileImageWidth);
		}

		if ((imageTileImageWidth != nanozoomerTileWidth) || (imageTileImageHeight != nanozoomerTileHeight)) {

			System.out.println("Error! (imageTileImageWidth != tileImageWidth)  || (imageTileImageHeight != tileImageHeight");
			System.out.println("imageTileImageHeight  = " + imageTileImageHeight);
			System.out.println("imageTileImageWidth = " + imageTileImageWidth);
			System.exit(1);
		}

		long commentSectionOffset = readUnsignedInt();
		if (verbose) {
			System.out.println("commentSectionOffset = " + commentSectionOffset);
		}

		long dataSectionOffset = readUnsignedInt();
		if (verbose) {
			System.out.println("dataSectionOffset = " + dataSectionOffset);
		}

		long reserved1 = readUnsignedInt();
		if (verbose) {
			System.out.println("reserved1 = " + reserved1);
		}
		long reserved2 = readUnsignedInt();
		if (verbose) {
			System.out.println("reserved2 = " + reserved2);
		}

		int colorType = readUnsignedShort();

		if (verbose) {
			System.out.println("colorType = " + colorType);
		}

		int bitsPerPixel = readUnsignedShort();

		if (verbose) {
			System.out.println("bitsPerPixel = " + bitsPerPixel);
		}

		// skip byte index 40-63
		for (int i = 0; i < 24; i++) {
			raf.read();
		}

		int numCommentBytes = (int) (dataSectionOffset - commentSectionOffset);

		byte[] byteArray = new byte[numCommentBytes];
		raf.read(byteArray);

		String comment = new String(byteArray);

		if (verbose) {
			System.out.println("comment = " + comment);
		}

		raf.close();

		long sizeOfImageData1 = length - dataSectionOffset;
		long sizeOfImageData2 = totalImageWidth * totalImageHeight * 3;

		if (verbose) {
			System.out.println("length = " + length);
			System.out.println("sizeOfImageData1 = " + sizeOfImageData1);
			System.out.println("sizeOfImageData2 = " + sizeOfImageData2);
		}

		int numTilesToMerge = targetImageHeight / nanozoomerTileHeight;
		if (targetImageHeight % nanozoomerTileHeight != 0) {
			numTilesToMerge++;
		}
		System.out.println("numTilesToMerge = " + numTilesToMerge);

		// int tileImageSizeInBytes = nanozoomerTileWidth * nanozoomerTileHeight
		// * numBands * numTilesToMerge;

		// byte[] bytes = new byte[tileImageSizeInBytes];

		int numTiles = (int) (totalImageWidth * totalImageHeight / (nanozoomerTileWidth * nanozoomerTileHeight));
		if (verbose) {
			System.out.println("numTiles = " + numTiles);
		}

		int tileIndex = -1;
		while (true) {

			double randomDouble = Math.random();
			tileIndex = (int) (randomDouble * numTiles) / numTilesToMerge * numTilesToMerge;
			tileIndex += 1000000 * grandOffset;

			if (cellsTaken.get(tileIndex) == null) {
				cellsTaken.put(tileIndex, true);
				break;
			}
		}

		return tileIndex;

	}

	private void loadImageData(int rasterIndex, String filePathString, int tileIndex, int grandOffset) throws FileNotFoundException, IOException {
		// note: assumes all byte ordering is LittleEndian!! (low order bytes
		// first)

		raf = new RandomAccessFile(filePathString, "r");

		long length = raf.length();

		if (verbose) {
			System.out.println("length = " + length);
		}

		// determine byte order
		{
			// read unsigned integer low order byte first
			int formatSpecifier2 = raf.read();
			int formatSpecifier1 = raf.read();

			if (verbose) {
				System.out.println("formatSpecifier1 = " + (char) formatSpecifier1);
				System.out.println("formatSpecifier2 = " + (char) formatSpecifier2);
			}

			if (formatSpecifier1 != 'N' || formatSpecifier2 != 'G') {
				Thread.dumpStack();
				System.exit(1);
			}
		}

		int majorVersion = 0;
		int minorVersion = 0;
		{
			minorVersion = raf.read();
			majorVersion = raf.read();
		}
		if (verbose) {
			System.out.println("majorVersion = " + majorVersion);
			System.out.println("minorVersion = " + minorVersion);
		}

		long totalImageWidth = readUnsignedInt();
		long totalImageHeight = readUnsignedInt();

		if (verbose) {
			System.out.println("totalImageWidth  = " + totalImageWidth);
			System.out.println("totalImageHeight = " + totalImageHeight);
		}

		int imageTileImageWidth = (int) readUnsignedInt();
		int imageTileImageHeight = (int) readUnsignedInt();

		if (verbose) {
			System.out.println("imageTileImageHeight  = " + imageTileImageHeight);
			System.out.println("imageTileImageWidth = " + imageTileImageWidth);
		}

		if ((imageTileImageWidth != nanozoomerTileWidth) || (imageTileImageHeight != nanozoomerTileHeight)) {

			System.out.println("Error! (imageTileImageWidth != tileImageWidth)  || (imageTileImageHeight != tileImageHeight");
			System.out.println("imageTileImageHeight  = " + imageTileImageHeight);
			System.out.println("imageTileImageWidth = " + imageTileImageWidth);
			System.exit(1);
		}

		long commentSectionOffset = readUnsignedInt();
		if (verbose) {
			System.out.println("commentSectionOffset = " + commentSectionOffset);
		}

		long dataSectionOffset = readUnsignedInt();
		if (verbose) {
			System.out.println("dataSectionOffset = " + dataSectionOffset);
		}

		long reserved1 = readUnsignedInt();
		if (verbose) {
			System.out.println("reserved1 = " + reserved1);
		}
		long reserved2 = readUnsignedInt();
		if (verbose) {
			System.out.println("reserved2 = " + reserved2);
		}

		int colorType = readUnsignedShort();

		if (verbose) {
			System.out.println("colorType = " + colorType);
		}

		int bitsPerPixel = readUnsignedShort();

		if (verbose) {
			System.out.println("bitsPerPixel = " + bitsPerPixel);
		}

		// skip byte index 40-63
		for (int i = 0; i < 24; i++) {
			raf.read();
		}

		int numCommentBytes = (int) (dataSectionOffset - commentSectionOffset);

		byte[] byteArray = new byte[numCommentBytes];
		raf.read(byteArray);

		String comment = new String(byteArray);

		if (verbose) {
			System.out.println("comment = " + comment);
		}

		int numTilesToMerge = targetImageHeight / nanozoomerTileHeight;
		if (targetImageHeight % nanozoomerTileHeight != 0) {
			numTilesToMerge++;
		}

		int tileImageSizeInBytes = nanozoomerTileWidth * nanozoomerTileHeight * numBands * numTilesToMerge;

		byte[] bytes = new byte[tileImageSizeInBytes];

		int numTiles = (int) (totalImageWidth * totalImageHeight / nanozoomerTileWidth / nanozoomerTileHeight);
		if (verbose) {
			System.out.println("numTiles = " + numTiles);
		}

		long tileSize = nanozoomerTileWidth * nanozoomerTileHeight * numBands;

		raf.seek(dataSectionOffset + tileSize * (tileIndex - 1000000 * grandOffset));

		raf.read(bytes);

		raf.close();

		// File outputfile = new File(tileImageFilePathPrefix + tileIndex +
		// ".png");
		// ImageIO.write(image, "png", outputfile);

		// trim bytes to sample window size

		int sampleImageSizeInBytes = targetImageWidth * targetImageHeight * numBands;
		byte[] newBytes = new byte[sampleImageSizeInBytes];

		int newByteIndex = 0;

		int byteIndex = -1;
		switch (grandOffset) {
		case 0:
			byteIndex = 0;
			break;
		case 1:
			byteIndex = (1024 + 299) * numBands;
			break;
		case 2:
			byteIndex = (1024 + 299 + 1024 + 298) * numBands;
			break;
		}

		// int byteIndex = 0;
		for (int rowIndex = 0; rowIndex < targetImageHeight; rowIndex++) {
			System.arraycopy(bytes, byteIndex, newBytes, newByteIndex, targetImageWidth * numBands);
			newByteIndex += targetImageWidth * numBands;
			byteIndex += nanozoomerTileWidth * numBands;
		}

		System.out.println("rasterIndex = " + rasterIndex);
		currentBuffer[rasterIndex] = newBytes;

	}

	private int readUnsignedShort() throws IOException {
		int unsignedShort;
		int value1 = raf.read();
		int value2 = raf.read();

		if (isLittleEndian) {
			unsignedShort = value1 + (value2 << 8);
		} else {
			unsignedShort = value2 + (value1 << 8);
		}

		return unsignedShort;
	}

	private long readUnsignedInt() throws IOException {
		long unsignedInt;
		long value1 = raf.read();
		long value2 = raf.read();
		long value3 = raf.read();
		long value4 = raf.read();

		if (verbose) {
			System.out.println("value1 = " + value1);
			System.out.println("value2 = " + value2);
			System.out.println("value3 = " + value3);
			System.out.println("value4 = " + value4);
		}

		if (isLittleEndian) {
			unsignedInt = value1 + (value2 << 8) + (value3 << 16) + (value4 << 24);
		} else {
			unsignedInt = value4 + (value3 << 8) + (value2 << 16) + (value1 << 24);
		}

		return unsignedInt;
	}

	private long readLittleOrBigIFDOffset() throws IOException {
		long ifdOffset;

		long value1 = raf.read();
		long value2 = raf.read();
		long value3 = raf.read();
		long value4 = raf.read();
		if (verbose) {
			System.out.println("value1 = " + value1);
			System.out.println("value2 = " + value2);
			System.out.println("value3 = " + value3);
			System.out.println("value4 = " + value4);
		}

		long value5 = 0;
		long value6 = 0;
		long value7 = 0;
		long value8 = 0;

		if (bigFormat) {
			value5 = raf.read();
			value6 = raf.read();
			value7 = raf.read();
			value8 = raf.read();
			if (verbose) {
				System.out.println("value5 = " + value5);
				System.out.println("value6 = " + value6);
				System.out.println("value7 = " + value7);
				System.out.println("value8 = " + value8);
			}
		}

		if (bigFormat) {
			if (isLittleEndian) {
				ifdOffset = value1 + (value2 << 8) + (value3 << 16) + (value4 << 24) + (value5 << 32) + (value6 << 40) + (value7 << 48) + (value8 << 56);
			} else {
				ifdOffset = value8 + (value7 << 8) + (value6 << 16) + (value5 << 24) + (value4 << 32) + (value3 << 40) + (value2 << 48) + (value1 << 56);
			}
		} else {
			if (isLittleEndian) {
				ifdOffset = value1 + (value2 << 8) + (value3 << 16) + (value4 << 24);
			} else {
				ifdOffset = value4 + (value3 << 8) + (value2 << 16) + (value1 << 24);
			}
		}

		return ifdOffset;
	}

	private void analyzeTags() {

		boolean createExamples = true;
		//

		// exampleGenerationheightAndWidthRadiusInPixels = 32;

		int numBands = 3;
		// int heightAndWidthWindowSizeInPixels = 1 + (exampleGenerationheightAndWidthRadiusInPixels - 1) * 2;
		// int depthWindowSizeInPixels = 1 + (exampleGenerationDepthRadiusInPixels - 1) * 2;

		// System.out.printf("depthRadiusInPixels              = %d\n", exampleGenerationDepthRadiusInPixels);
		// System.out.printf("depthWindowSizeInPixels          = %d\n", depthWindowSizeInPixels);
		// System.out.printf("heightAndWidthRadiusInPixels     = %d\n", exampleGenerationheightAndWidthRadiusInPixels);
		// System.out.printf("heightAndWidthWindowSizeInPixels = %d\n", heightAndWidthWindowSizeInPixels);

		// int numInputFeatures = depthWindowSizeInPixels * heightAndWidthWindowSizeInPixels * heightAndWidthWindowSizeInPixels * numBands;
		// int numOutputFeatures = 1;
		// System.out.printf("numInputFeatures  = %d\n", numInputFeatures);
		// System.out.printf("numOutputFeatures = %d\n", numOutputFeatures);

		// int widthPadInPixels = (targetImageWidth - taggableImageWidth) / 2;
		// int heightPadInPixels = (targetImageHeight - taggableImageHeight) / 2;

		int[] pixelValues = new int[numBands];

		//

		int numTrials = trialIndex;

		// for (int i = 0; i < trialViewPaths.length; i++) {
		// trialViewPaths[i] = null;
		// }

		String pathString = "cacheState.ser";

		boolean buildCache = IO.fileDoesNotExists(pathString);

		if (buildCache) {
			createTrialViewPaths(true);
			savesStateToFile(pathString);
			System.exit(0);
		}

		int totalNumTags = 0;

		HashMap<String, Integer> classNameToIndex = new HashMap<String, Integer>();
		HashMap<Integer, String> classIndexToName = new HashMap<Integer, String>();

		SimpleTable table = null;
		try {
			table = IO.readDelimitedTable("classNames.tab", "\t", true);
		} catch (Exception e) {
			e.printStackTrace();
		}

		int maxNumClasses = table.numDataRows;

		for (int c = 0; c < maxNumClasses; c++) {

			int index = table.getInt(c, 0) - 1;
			String className = table.getString(c, 1);

			classNameToIndex.put(className, index);
			classIndexToName.put(index, className);
		}

		HashMap<String, Integer> classCounts = new HashMap<String, Integer>();

		int[] slidePollenCounts = new int[maxNumSlides];
		int[] slideLycPollenCounts = new int[maxNumSlides];
		int[][] slidePollenClassCounts = new int[maxNumSlides][maxNumClasses];

		int numErrors = 0;
		int numUnknowns = 0;
		int newClassIndex = 0;

		int maxNumTrialsToProcess = 0;

		boolean useSample = false;

		if (!useSample)
			maxNumTrialsToProcess = numTrials;
		else
			maxNumTrialsToProcess = 25;

		int numDistanceLevels = 6; /* !!! */
		// int numDistanceLevels = -1; /* !!! */
		int numDistanceFeatures = (int) (Math.pow(2, numDistanceLevels) - 1);
		System.out.printf("numDistanceLevels   = %d\n", numDistanceLevels);
		System.out.printf("numDistanceFeatures = %d\n", numDistanceFeatures);

		int minTextureLineNumPixels = 2;
		int maxTextureLineNumPixels = 12;
		// int minTextureLineNumPixels = -1;
		// int maxTextureLineNumPixels = -1;

		System.out.printf("minTextureLineNumPixels = %d\n", minTextureLineNumPixels);
		System.out.printf("maxTextureLineNumPixels = %d\n", maxTextureLineNumPixels);

		double[][] bandIntensitySums = new double[numDistanceFeatures][numBands];
		double[][] bandIntensityCounts = new double[numDistanceFeatures][numBands];
		double[][] bandIntensities = new double[numDistanceFeatures][numBands];
		double[][] bandIntensityVarianceSums = new double[numDistanceFeatures][numBands];
		double[][] bandIntensitySTDs = new double[numDistanceFeatures][numBands];

		for (int trialIndex = 0; trialIndex < maxNumTrialsToProcess; trialIndex++) {

			if (trialIndex % numThreads != threadIndex)
				continue;

			if (trialViewPaths[trialIndex] == null) {
				continue;
			}

			// if (createExamples && (trialViewPaths[trialIndex] == null || IO.fileDoesNotExists(trialViewPaths[trialIndex]))) {
			// continue;
			// }

			// System.out.printf("trialViewPaths[trialIndex] = %s\n", trialViewPaths[trialIndex]);
			int currentRoundIndex = trialViewRounds[trialIndex];
			int currentSlideIndex = trialViewSlides[trialIndex];
			int currentTrialNumTags = trialNumTags[trialIndex];

			if (false) {
				System.out.println();
				System.out.printf("trialIndex = %d\n", trialIndex);
				System.out.printf("roundIndex = %d\n", currentRoundIndex);
				System.out.printf("slideIndex = %d\n", currentSlideIndex);
				System.out.printf("numTags    = %d\n", currentTrialNumTags);
			}

			byte[][] byteBuffers = null;
			DataBuffer[] dataBuffers = null;

			if (currentTrialNumTags > 0) {

				if (true)
					if (trialViewPaths[trialIndex] != null && IO.fileExists(trialViewPaths[trialIndex])) {
						System.out.printf("load data buffers for %s\n", trialViewPaths[trialIndex]);
						byteBuffers = (byte[][]) IO.readObject(trialViewPaths[trialIndex]);

						dataBuffers = new DataBuffer[numZPlanes];
						for (int j = 0; j < numZPlanes; j++) {
							dataBuffers[j] = new DataBufferByte(byteBuffers[j], byteBuffers[j].length);
						}
					}

				for (int tagIndex = 0; tagIndex < currentTrialNumTags; tagIndex++) {

					int tagX = trialClassificationX[trialIndex][tagIndex];
					int tagY = trialClassificationY[trialIndex][tagIndex];
					int tagZ = trialClassificationZ[trialIndex][tagIndex];
					int tagW = trialClassificationW[trialIndex][tagIndex];
					int tagR = tagW / 2 + 1;

					// System.out.printf("tagX = %d\n", tagX);
					// System.out.printf("tagY = %d\n", tagY);

					if (trialClassificationText[trialIndex][tagIndex].length() != 4) {
						// System.out.printf("Error! length != 4 for label: %s\n", trialClassificationText[i][j].length());
						numErrors++;
						continue;
					}

					// System.out.printf("label %s\n", trialClassificationText[i][j]);

					String className = trialClassificationText[trialIndex][tagIndex].substring(0, 3);
					// System.out.printf("className %s\n", className);

					if (className.equalsIgnoreCase("unk") || className.equalsIgnoreCase("ind")) {
						numUnknowns++;
						continue;
					}

					int confidence = Integer.parseInt(trialClassificationText[trialIndex][tagIndex].substring(3, 4));
					// System.out.printf("confidence %d\n", confidence);

					Integer count = classCounts.get(className);
					if (count == null) {
						count = 0;
					}
					classCounts.put(className, (count + 1));

					slidePollenCounts[currentSlideIndex]++;

					if (className.equalsIgnoreCase("lyc")) {
						slideLycPollenCounts[currentSlideIndex]++;
					}

					Integer classIndex = classNameToIndex.get(className);
					// if (classIndex == null) {
					// classIndex = newClassIndex++;
					// classNameToIndex.put(className, classIndex);
					// classIndexToName.put(classIndex, className);
					// System.out.printf("new className  = %s\n", className);
					// System.out.printf("new classIndex = %d\n", classIndex);
					//
					// }

					// System.out.printf("classIndex = %d\n", classIndex);
					slidePollenClassCounts[currentSlideIndex][classIndex]++;

					if (createExamples) {
						// extract image features //

						// set input feature values

						SampleModel sampleModel = new ComponentSampleModel(DataBuffer.TYPE_BYTE, targetImageWidth, targetImageHeight, numBands, targetImageWidth * numBands, new int[] { 0, 1, 2 });

						int rasterIndex = tagZ - minZPlane;

						int zRadius = 1;
						int startRasterIndex = rasterIndex - zRadius;
						if (startRasterIndex < 0)
							startRasterIndex = 0;
						int numRasters = zRadius * 2 + 1;
						int endRasterIndex = startRasterIndex + numRasters;

						Raster[] rasters = new Raster[numRasters];

						if (dataBuffers != null) {
							for (int i = 0; i < numRasters; i++) {
								rasters[i] = Raster.createRaster(sampleModel, dataBuffers[rasterIndex], null);
							}
						}

						int startX = tagX - tagR;
						int endX = startX + tagW;
						int startY = tagY - tagR;
						int endY = startY + tagW;

						if (startX < 0)
							startX = 0;
						if (endX > targetImageWidth)
							endX = targetImageWidth;

						if (startY < 0)
							startY = 0;
						if (endY > targetImageHeight)
							endY = targetImageHeight;

						/*************************/
						/* radial color features */
						/*************************/

						if (numDistanceLevels > 0) {
							int pixelCount = 0;

							{
								int distanceFeatureIndex = 0;

								for (int distanceLevelIndex = 0; distanceLevelIndex < numDistanceLevels; distanceLevelIndex++) {

									int numFeaturesAtThisLevel = (int) Math.pow(2, distanceLevelIndex);

									for (int i = 0; i < numFeaturesAtThisLevel; i++) {

										for (int b = 0; b < numBands; b++) {
											bandIntensitySums[distanceFeatureIndex][b] = 0;
											bandIntensityCounts[distanceFeatureIndex][b] = 0;
										}

										distanceFeatureIndex++;
									}
								}
							}

							for (int rIndex = 0; rIndex < numRasters; rIndex++) {

								for (int xTest = startX; xTest < endX; xTest++) {

									for (int yTest = startY; yTest < endY; yTest++) {

										double xDiff = xTest - tagX;
										double yDiff = yTest - tagY;
										double distance = Math.sqrt(xDiff * xDiff + yDiff * yDiff);

										// System.out.printf("angle = %6.3f\n", angle);

										if (distance > tagR)
											continue;

										// System.out.printf("distance = %6.3f\n", distance);

										rasters[rIndex].getPixel(xTest, yTest, pixelValues);

										{
											int distanceFeatureIndexOffset = 0;

											for (int distanceLevelIndex = 0; distanceLevelIndex < numDistanceLevels; distanceLevelIndex++) {

												int numFeaturesAtThisLevel = (int) Math.pow(2, distanceLevelIndex);

												int distanceFeatureIndex = (int) (distance / (tagR + 1) * numFeaturesAtThisLevel);
												// System.out.printf("distance = %f\n", distance);
												// System.out.printf("tagR = %d\n", tagR);
												// System.out.printf("distanceFeatureIndex = %d\n", distanceFeatureIndex);

												for (int b = 0; b < numBands; b++) {
													bandIntensitySums[distanceFeatureIndexOffset + distanceFeatureIndex][b] += pixelValues[b];
													bandIntensityCounts[distanceFeatureIndexOffset + distanceFeatureIndex][b]++;
												}

												distanceFeatureIndexOffset += numFeaturesAtThisLevel;

											}
										}

										pixelCount++;
									}
								}
							}
							// System.out.printf("tagW = %d\n", tagW);
							// System.out.printf("tagR = %d\n", tagR);
							// System.out.printf("pixelCount = %d\n", pixelCount);

							{
								int distanceFeatureIndex = 0;

								for (int distanceLevelIndex = 0; distanceLevelIndex < numDistanceLevels; distanceLevelIndex++) {

									int numFeaturesAtThisLevel = (int) Math.pow(2, distanceLevelIndex);

									for (int i = 0; i < numFeaturesAtThisLevel; i++) {

										for (int b = 0; b < numBands; b++) {
											bandIntensities[distanceFeatureIndex][b] = bandIntensitySums[distanceFeatureIndex][b] / bandIntensityCounts[distanceFeatureIndex][b];
											bandIntensityVarianceSums[distanceFeatureIndex][b] = 0;
										}

										distanceFeatureIndex++;
									}
								}
							}

							for (int rIndex = 0; rIndex < numRasters; rIndex++) {

								for (int xTest = startX; xTest < endX; xTest++) {

									for (int yTest = startY; yTest < endY; yTest++) {

										double xDiff = xTest - tagX;
										double yDiff = yTest - tagY;
										double distance = Math.sqrt(xDiff * xDiff + yDiff * yDiff);

										if (distance > tagR)
											continue;

										// System.out.printf("distance = %6.3f\n", distance);

										rasters[rIndex].getPixel(xTest, yTest, pixelValues);

										{
											int distanceFeatureIndexOffset = 0;

											for (int distanceLevelIndex = 0; distanceLevelIndex < numDistanceLevels; distanceLevelIndex++) {

												int numFeaturesAtThisLevel = (int) Math.pow(2, distanceLevelIndex);

												int distanceFeatureIndex = (int) (distance / (tagR + 1) * numFeaturesAtThisLevel);
												// System.out.printf("distance = %f\n", distance);
												// System.out.printf("tagR = %d\n", tagR);
												// System.out.printf("distanceFeatureIndex = %d\n", distanceFeatureIndex);

												for (int b = 0; b < numBands; b++) {

													double diff = pixelValues[b] - bandIntensities[distanceFeatureIndexOffset + distanceFeatureIndex][b];
													bandIntensityVarianceSums[distanceFeatureIndexOffset + distanceFeatureIndex][b] += diff * diff;

												}

												distanceFeatureIndexOffset += numFeaturesAtThisLevel;

											}
										}

										pixelCount++;
									}
								}
							}

							{
								int distanceFeatureIndex = 0;

								for (int distanceLevelIndex = 0; distanceLevelIndex < numDistanceLevels; distanceLevelIndex++) {

									int numFeaturesAtThisLevel = (int) Math.pow(2, distanceLevelIndex);

									for (int i = 0; i < numFeaturesAtThisLevel; i++) {

										for (int b = 0; b < numBands; b++) {
											bandIntensitySTDs[distanceFeatureIndex][b] = Math.sqrt(bandIntensityVarianceSums[distanceFeatureIndex][b] / bandIntensityCounts[distanceFeatureIndex][b]);
										}

										distanceFeatureIndex++;
									}
								}
							}
						}

						/********************/
						/* texture features */
						/********************/

						double[] textureLineValues = null;
						int numTextureLineFeatures = 0;

						if (minTextureLineNumPixels > 1) {

							int maxNumBins = 1 << (maxTextureLineNumPixels - 1);
							int[] pixelLineBinCounts = new int[maxNumBins];

							int textureLineFeatureIndex = 0;

							for (int textureLineNumPixels = minTextureLineNumPixels; textureLineNumPixels <= maxTextureLineNumPixels; textureLineNumPixels++) {

								for (int c = 0; c < numBands; c++) {

									int textureLineNumPixelsMinusOne = textureLineNumPixels - 1;
									int textureLineNumValueBins = (1 << textureLineNumPixelsMinusOne);

									textureLineFeatureIndex += textureLineNumValueBins;
								}
							}

							numTextureLineFeatures = textureLineFeatureIndex;

							System.out.printf("numTextureLineFeatures = %d\n", numTextureLineFeatures);

							textureLineValues = new double[numTextureLineFeatures];
							textureLineFeatureIndex = 0;

							for (int textureLineNumPixels = minTextureLineNumPixels; textureLineNumPixels <= maxTextureLineNumPixels; textureLineNumPixels++) {

								for (int c = 0; c < numBands; c++) {

									int textureLineNumPixelsMinusOne = textureLineNumPixels - 1;
									int textureLineNumValueBins = (1 << textureLineNumPixelsMinusOne);

									for (int k = 0; k < textureLineNumValueBins; k++) {
										pixelLineBinCounts[k] = 0;
									}

									int numTextureLines = 0;

									for (int rIndex = 0; rIndex < numRasters; rIndex++) {

										for (int xTest = startX; xTest < endX; xTest++) {

											for (int yTest = startY; yTest < endY - textureLineNumPixelsMinusOne; yTest++) {

												{
													int binIndex = 0;
													int yStart = yTest;
													int yEnd = yTest + textureLineNumPixelsMinusOne;
													for (int pixelY = yStart; pixelY < yEnd; pixelY++) {

														rasters[rIndex].getPixel(xTest, pixelY + 1, pixelValues);
														int value1 = pixelValues[c];

														rasters[rIndex].getPixel(xTest, pixelY, pixelValues);
														int value2 = pixelValues[c];

														if (value1 - value2 < 0) {
															binIndex = binIndex * 2;
														} else {
															binIndex = binIndex * 2 + 1;
														}
													}
													pixelLineBinCounts[binIndex]++;
													numTextureLines++;
												}

												{
													int binIndex = 0;
													int yStart = yTest + textureLineNumPixelsMinusOne;
													int yEnd = yTest;
													for (int pixelY = yStart; pixelY > yEnd; pixelY--) {

														rasters[rIndex].getPixel(xTest, pixelY - 1, pixelValues);
														int value1 = pixelValues[c];

														rasters[rIndex].getPixel(xTest, pixelY, pixelValues);
														int value2 = pixelValues[c];

														if (value1 - value2 < 0) {
															binIndex = binIndex * 2;
														} else {
															binIndex = binIndex * 2 + 1;
														}
													}
													pixelLineBinCounts[binIndex]++;
													numTextureLines++;
												}
											}
										}
									}

									for (int rIndex = 0; rIndex < numRasters; rIndex++) {

										for (int xTest = startX; xTest < endX - textureLineNumPixelsMinusOne; xTest++) {

											for (int yTest = 0; yTest < endY; yTest++) {

												{
													int binIndex = 0;
													int xStart = xTest;
													int xEnd = xTest + textureLineNumPixelsMinusOne;
													for (int pixelX = xStart; pixelX < xEnd; pixelX++) {

														rasters[rIndex].getPixel(pixelX + 1, yTest, pixelValues);
														int value1 = pixelValues[c];

														rasters[rIndex].getPixel(pixelX, yTest, pixelValues);
														int value2 = pixelValues[c];

														if (value1 - value2 < 0) {
															binIndex = binIndex * 2;
														} else {
															binIndex = binIndex * 2 + 1;
														}
													}
													pixelLineBinCounts[binIndex]++;
													numTextureLines++;
												}
												{
													int binIndex = 0;
													int xStart = xTest + textureLineNumPixelsMinusOne;
													int xEnd = xTest;
													for (int pixelX = xStart; pixelX > xEnd; pixelX--) {

														rasters[rIndex].getPixel(pixelX - 1, yTest, pixelValues);
														int value1 = pixelValues[c];

														rasters[rIndex].getPixel(pixelX, yTest, pixelValues);
														int value2 = pixelValues[c];

														if (value1 - value2 < 0) {
															binIndex = binIndex * 2;
														} else {
															binIndex = binIndex * 2 + 1;
														}
													}
													pixelLineBinCounts[binIndex]++;
													numTextureLines++;
												}

											}
										}
									}

									for (int pixelIndex = 0; pixelIndex < textureLineNumValueBins; pixelIndex++) {
										textureLineValues[textureLineFeatureIndex++] = (double) pixelLineBinCounts[pixelIndex] / numTextureLines;
									}

								}

							}
						}

						//

						//

						//

						// System.out.println("sum = " + sum);

						// set output feature value

						if (true) {
							System.out.printf("EXAMPLE\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%f\t", //
									trialIndex, currentRoundIndex, currentSlideIndex, currentTrialNumTags, tagIndex, tagX, tagY, tagZ, (tagW - 42.0) / 600.0);

							{
								int distanceFeatureIndex = 0;

								for (int distanceLevelIndex = 0; distanceLevelIndex < numDistanceLevels; distanceLevelIndex++) {

									int numFeaturesAtThisLevel = (int) Math.pow(2, distanceLevelIndex);

									for (int i = 0; i < numFeaturesAtThisLevel; i++) {

										for (int b = 0; b < numBands; b++) {

											System.out.printf("%f\t", bandIntensities[distanceFeatureIndex][b] / 256.0);

										}
										for (int b = 0; b < numBands; b++) {

											System.out.printf("%f\t", bandIntensitySTDs[distanceFeatureIndex][b] / 256.0);

										}

										distanceFeatureIndex++;
									}
								}
							}

							for (int i = 0; i < numTextureLineFeatures; i++) {
								System.out.printf("%f\t", textureLineValues[i]);
							}

							System.out.printf("%d\t%d\n", //
									confidence, classIndex);
						}

					}
					totalNumTags++;

				}
			}
		}
		System.out.printf("numUnknowns = %d\n", numUnknowns);
		System.out.printf("numErrors = %d\n", numErrors);

		int numClasses = classCounts.size();

		System.out.printf("totalNumTags = %d\n", totalNumTags);
		System.out.printf("numClasses = %d\n", numClasses);

		CountAndLabel[] countAndLabels = new CountAndLabel[numClasses];

		Set<Entry<String, Integer>> set = classCounts.entrySet();
		System.out.println();

		{
			int i = 0;
			for (Iterator<Entry<String, Integer>> iterator = set.iterator(); iterator.hasNext();) {
				Entry<String, Integer> entry = iterator.next();
				System.out.printf("%d\t", entry.getValue());
				System.out.printf("%s\n", entry.getKey());

				countAndLabels[i++] = new CountAndLabel(entry.getValue(), entry.getKey());

			}

			Arrays.sort(countAndLabels);

			double probabilitySum = 0;
			int countSum = 0;

			for (int k = 0; k < numClasses; k++) {
				double probability = (double) countAndLabels[k].count / totalNumTags;
				countSum += countAndLabels[k].count;

				probabilitySum += probability;
				System.out.printf("%d\t%d\t%f\t%f\t%s\n", k + 1, countAndLabels[k].count, probability, probabilitySum, countAndLabels[k].label);

			}
			System.out.printf("probabilitySum = %f\n", probabilitySum);
			System.out.printf("countSum = %d\n", countSum);
		}
		System.out.println();

		double averageNumTagsPerTrial = (double) totalNumTags / (double) trialIndex;
		System.out.printf("averageNumTagsPerTrial = %f\n", averageNumTagsPerTrial);

		int numLycopodium = classCounts.get("lyc");
		System.out.printf("numLycopodium = %d\n", numLycopodium);

		// int numUnkown = classCounts.get("unk");
		// System.out.printf("numUnkown = %d\n", numUnkown);

		// int numKnownTags = totalNumTags - numUnkown;
		//
		double fractionLycopodium = (double) numLycopodium / totalNumTags;
		System.out.printf("fractionLycopodium (of known tags) = %f\n", fractionLycopodium);

		int numSlidesWithPollen = 0;
		for (int j = 0; j < maxNumSlides; j++) {
			if (slidePollenCounts[j] > 0) {
				numSlidesWithPollen++;
			}
		}
		System.out.printf("numSlidesWithPollen = %d\n", numSlidesWithPollen);
		double fractionOfSlidesWithPollen = (double) numSlidesWithPollen / maxNumSlides;
		System.out.printf("fractionOfSlidesWithPollen = %f\n", fractionOfSlidesWithPollen);

		int numSlidesWithLycPollen = 0;
		for (int j = 0; j < maxNumSlides; j++) {
			if (slideLycPollenCounts[j] > 0) {
				numSlidesWithLycPollen++;
			}
		}
		System.out.printf("numSlidesWithLycPollen = %d\n", numSlidesWithLycPollen);
		double fractionOfSlidesWithLycPollen = (double) numSlidesWithLycPollen / maxNumSlides;
		System.out.printf("fractionOfSlidesWithLycPollen = %f\n", fractionOfSlidesWithLycPollen);

		double lycProbabilitySum = 0.0;

		for (int j = 0; j < maxNumSlides; j++) {
			if (slidePollenCounts[j] > 0) {
				lycProbabilitySum += (double) slideLycPollenCounts[j] / (double) slidePollenCounts[j];
			}
		}
		double lycProbability = lycProbabilitySum / numSlidesWithPollen;
		// System.out.printf("lycProbabilitySum = %f\n", lycProbabilitySum);
		System.out.printf("lycProbability = %f\n\n", lycProbability);

		if (false) {
			SimpleTable slideMetaDataTable = null;
			try {
				slideMetaDataTable = IO.readDelimitedTable("/media/Patriot/pollen/slides.xls", "\t", true);
			} catch (Exception e) {
				e.printStackTrace();
			}
			for (int slideIndex = 0; slideIndex < maxNumSlides; slideIndex++) {

				String fileName = slideMetaDataTable.getString(slideIndex, 0);

				for (int classIndex = 0; classIndex < numClasses; classIndex++) {

					if (slidePollenClassCounts[slideIndex][classIndex] > 0)

						System.out.println(slideIndex + "\t" + fileName + "\t" + slidePollenCounts[slideIndex] + "\t" + classIndexToName.get(classIndex) + "\t" + slidePollenClassCounts[slideIndex][classIndex]);
				}
			}
		}

	}

	int countPollenPerSlide(int slideIndexToCount) {

		int[] slidePollenCounts = new int[maxNumSlides];

		for (int i = 0; i < trialIndex; i++) {
			int slideIndex = trialViewSlides[i];
			int numTags = trialNumTags[i];

			for (int j = 0; j < numTags; j++) {

				if (trialClassificationText[i][j].length() != 4) {
					continue;
				}

				String className = trialClassificationText[i][j].substring(0, 3);

				if (className.equalsIgnoreCase("lyc") || className.equalsIgnoreCase("ind")) {
					continue;
				}

				slidePollenCounts[slideIndex]++;
			}
		}

		return slidePollenCounts[slideIndexToCount];

	}

	private double scoreTrial(RawNanoReader master, RawNanoReader tester, int trialIndex) {

		System.out.println();
		System.out.printf("trial %d\n", trialIndex);
		int roundIndex = master.trialViewRounds[trialIndex];
		int slideIndex = master.trialViewSlides[trialIndex];
		int numTags = master.trialNumTags[trialIndex];
		System.out.printf("roundIndex %d\n", roundIndex);
		System.out.printf("slideIndex %d\n", slideIndex);
		System.out.printf("numTags %d\n", numTags);

		int masterNumTags = master.trialNumTags[trialIndex];
		int testerNumTags = tester.trialNumTags[trialIndex];

		int numMasterTagsConsidered = 0;
		double errorSum = 0;
		boolean[] testerTagUsed = new boolean[testerNumTags];
		for (int masterTagIndex = 0; masterTagIndex < masterNumTags; masterTagIndex++) {

			String masterLabel = master.trialClassificationText[trialIndex][masterTagIndex];

			System.out.printf("masterLabel %s\n", masterLabel);

			String masterClassName = masterLabel.substring(0, 3);
			System.out.printf("masterClassName %s\n", masterClassName);

			int masterXPos = master.trialClassificationX[trialIndex][masterTagIndex] - (targetImageWidth - taggableImageWidth) / 2;
			int masterYPos = master.trialClassificationY[trialIndex][masterTagIndex] - (targetImageHeight - taggableImageHeight) / 2;
			int masterZPos = master.trialClassificationZ[trialIndex][masterTagIndex];

			System.out.printf("masterXPos %s\n", masterXPos);
			System.out.printf("masterYPos %s\n", masterYPos);
			System.out.printf("masterZPos %s\n", masterZPos);

			// only consider tags in the tagging region
			if (masterXPos < 0 || masterXPos >= taggableImageWidth) {
				continue;
			}
			if (masterYPos < 0 || masterYPos >= taggableImageHeight) {
				continue;
			}

			// try to find matching tag in tester tags

			double bestMasterTagDistance = Double.POSITIVE_INFINITY;
			int bestTesterTagIndex = -1;
			for (int testerTagIndex = 0; testerTagIndex < testerNumTags; testerTagIndex++) {

				if (testerTagUsed[testerTagIndex]) {
					continue;
				}

				String testerLabel = tester.trialClassificationText[trialIndex][testerTagIndex];

				System.out.printf("testerLabel %s\n", testerLabel);

				String testerClassName = "";
				if (testerLabel.length() >= 3)
					testerClassName = testerLabel.substring(0, 3);

				// !!! cludge correction for Surangi //
				if (testerClassName.equals("cer"))
					testerClassName = "cec";

				System.out.printf("testerClassName %s\n", testerClassName);

				int testerXPos = tester.trialClassificationX[trialIndex][testerTagIndex] - (targetImageWidth - taggableImageWidth) / 2;
				int testerYPos = tester.trialClassificationY[trialIndex][testerTagIndex] - (targetImageHeight - taggableImageHeight) / 2;
				int testerZPos = tester.trialClassificationZ[trialIndex][testerTagIndex];

				System.out.printf("testerXPos %s\n", testerXPos);
				System.out.printf("testerYPos %s\n", testerYPos);
				System.out.printf("testerZPos %s\n", testerZPos);

				// look for best match to current master tag

				double xDiff = masterXPos - testerXPos;
				double yDiff = masterYPos - testerYPos;
				double zDiff = masterZPos - testerZPos;
				double zScaleFactor = 1.0;
				double masterTagDistance = Math.sqrt(xDiff * xDiff + yDiff * yDiff + (zDiff * zScaleFactor) * (zDiff * zScaleFactor));

				System.out.printf("masterTagDistance %f\n", masterTagDistance);
				if (masterTagDistance < bestMasterTagDistance) {
					bestMasterTagDistance = masterTagDistance;
					bestTesterTagIndex = testerTagIndex;
				}

			}

			System.out.printf("bestMasterTagDistance %f\n", bestMasterTagDistance);
			System.out.printf("bestTesterTagIndex %d\n", bestTesterTagIndex);

			if (bestTesterTagIndex == -1) {
				errorSum += 1.0;
			} else {
				String bestTesterLabel = tester.trialClassificationText[trialIndex][bestTesterTagIndex];

				System.out.printf("testerLabel %s\n", bestTesterLabel);

				String bestTesterClassName = "";
				if (bestTesterLabel.length() >= 3)
					bestTesterClassName = bestTesterLabel.substring(0, 3);

				// !!! cludge correction for Surangi //
				if (bestTesterClassName.equals("cer"))
					bestTesterClassName = "cec";
				System.out.printf("bestTesterClassName %s\n", bestTesterClassName);

				double minMatchDistance = 20.0;
				if (bestMasterTagDistance > minMatchDistance || !bestTesterClassName.equalsIgnoreCase(masterClassName)) {
					errorSum += 1.0;
				}

				if (bestMasterTagDistance <= minMatchDistance) {
					testerTagUsed[bestTesterTagIndex] = true;
				}
			}

			numMasterTagsConsidered++;

		}

		for (int testerTagIndex = 0; testerTagIndex < testerNumTags; testerTagIndex++) {
			if (!testerTagUsed[testerTagIndex]) {
				errorSum += 1.0;
			}
		}
		return errorSum;
	}

	private int getPollenCountInBox(RawNanoReader rnr, int trialIndex) {

		int numTags = rnr.trialNumTags[trialIndex];

		int pollenCount = 0;
		for (int tagIndex = 0; tagIndex < numTags; tagIndex++) {

			int masterXPos = rnr.trialClassificationX[trialIndex][tagIndex] - (targetImageWidth - taggableImageWidth) / 2;
			int masterYPos = rnr.trialClassificationY[trialIndex][tagIndex] - (targetImageHeight - taggableImageHeight) / 2;

			// only consider tags in the tagging region
			if (masterXPos < 0 || masterXPos >= taggableImageWidth) {
				continue;
			}
			if (masterYPos < 0 || masterYPos >= taggableImageHeight) {
				continue;
			}

			pollenCount++;
		}

		return pollenCount;
	}

	public void scoreTags(RawNanoReader masterRNR) {

		RawNanoReader testerRNR = this;

		int numSubjects = 2; // master and tester
		int masterSubjectIndex = 0;
		int testerSubjectIndex = 1;
		int[][] allSlidePollenCounts = new int[numSubjects][maxNumSlides];

		int startRoundIndex = 1;
		int endRoundIndex = 4;

		int startTrial = maxNumSlides * startRoundIndex;
		int endTrial = maxNumSlides * endRoundIndex;
		int numTrials = endTrial - startTrial;

		double errorSum = 0;
		int exactMatchErrorSum = 0;
		for (int i = startTrial; i < endTrial; i++) {

			double error = scoreTrial(masterRNR, testerRNR, i);

			if (error > 0) {
				exactMatchErrorSum++;
			}

			errorSum += error;
		}

		System.out.printf("errorSum           = %f\n", errorSum);
		System.out.printf("exactMatchErrorSum = %d\n", exactMatchErrorSum);

		double[] masterPollenCounts = new double[numTrials];
		double[] testerPollenCounts = new double[numTrials];

		for (int i = startTrial, observationIndex = 0; i < endTrial; i++, observationIndex++) {

			masterPollenCounts[observationIndex] = getPollenCountInBox(masterRNR, i);
			testerPollenCounts[observationIndex] = getPollenCountInBox(testerRNR, i);

		}

		double masterTesterCorrelation = Correlation.correlation(masterPollenCounts, testerPollenCounts, numTrials);

		System.out.printf("masterTesterCorrelation = %f\n", masterTesterCorrelation);

		double errorRate = errorSum / numTrials;
		double exactMatchErrorRate = (double) exactMatchErrorSum / numTrials;
		double exactMatchAccuracy = 1.0 - exactMatchErrorRate;
		System.out.printf("errorRate           = %f\n", errorRate);
		System.out.printf("exactMatchErrorRate = %f\n", exactMatchErrorRate);
		System.out.printf("exactMatchAccuracy  = %f\n", exactMatchAccuracy);

		System.exit(0);

		for (int subjectIndex = 0; subjectIndex < numSubjects; subjectIndex++) {

			RawNanoReader activeRNR = null;
			switch (subjectIndex) {
			case 0:
				activeRNR = masterRNR;
				break;
			case 1:
				activeRNR = testerRNR;
				break;
			}
			int totalNumTags = 0;

			HashMap<String, Integer> classCounts = new HashMap<String, Integer>();

			int[] slidePollenCounts = allSlidePollenCounts[subjectIndex];

			int numErrors = 0;
			int numUnknowns = 0;

			int pollenCountErrorSum = 0;
			int exactPollenCountErrorSum = 0;
			int numTrialsAnalyzed = 0;

			int numConfidenceJudgements = 0;
			double confidenceSum = 0.0;
			for (int i = startTrial; i < endTrial; i++) {
				System.out.println();
				System.out.printf("trial %d\n", i);
				int roundIndex = activeRNR.trialViewRounds[i];
				int slideIndex = activeRNR.trialViewSlides[i];
				int numTags = activeRNR.trialNumTags[i];
				System.out.printf("roundIndex %d\n", roundIndex);
				System.out.printf("slideIndex %d\n", slideIndex);
				System.out.printf("numTags %d\n", numTags);

				int masterNumTags = masterRNR.trialNumTags[i];

				pollenCountErrorSum += Math.abs(masterNumTags - numTags);

				if (masterNumTags != numTags) {
					exactPollenCountErrorSum++;
				}

				for (int j = 0; j < numTags; j++) {

					int xPos = trialClassificationX[i][j] - (targetImageWidth - taggableImageWidth) / 2;
					int yPos = trialClassificationY[i][j] - (targetImageHeight - taggableImageHeight) / 2;
					int zPos = trialClassificationZ[i][j];

					System.out.printf("xPos %s\n", xPos);
					System.out.printf("yPos %s\n", yPos);
					System.out.printf("zPos %s\n", zPos);

					String label = activeRNR.trialClassificationText[i][j];

					System.out.printf("label %s\n", label);

					String className = label.substring(0, 3);

					// !!! cludge correction //

					// if (className.equals("cer"))
					// className = "cec";

					// System.out.printf("className %s\n", className);

					if (xPos < 0 || xPos >= taggableImageWidth) {
						continue;
					}

					if (yPos < 0 || yPos >= taggableImageHeight) {
						continue;
					}

					if (className.equalsIgnoreCase("unk") || className.equalsIgnoreCase("ind")) {
						numUnknowns++;
					} else {
						if (label.length() != 4) {
							System.out.printf("Error! length != 4 for label: %s\n", label.length());
							numErrors++;
							continue;
						}

						int confidence = Integer.parseInt(label.substring(3, 4));
						System.out.printf("confidence %d\n", confidence);

						confidenceSum += confidence;
						numConfidenceJudgements++;

					}

					Integer count = classCounts.get(className);
					if (count == null) {
						count = 0;
					}
					classCounts.put(className, (count + 1));

					slidePollenCounts[slideIndex]++;

					totalNumTags++;
				}

				numTrialsAnalyzed++;
			}

			double averageConfidence = confidenceSum / numConfidenceJudgements;

			System.out.printf("confidenceSum     = %f\n", confidenceSum);
			System.out.printf("averageConfidence = %f\n", averageConfidence);

			System.out.printf("numUnknowns = %d\n", numUnknowns);
			System.out.printf("numErrors   = %d\n", numErrors);

			int numClasses = classCounts.size();

			System.out.printf("totalNumTags = %d\n", totalNumTags);
			System.out.printf("numClasses   = %d\n", numClasses);

			CountAndLabel[] countAndLabels = new CountAndLabel[numClasses];

			Set<Entry<String, Integer>> set = classCounts.entrySet();
			System.out.println();

			{
				int i = 0;
				for (Iterator<Entry<String, Integer>> iterator = set.iterator(); iterator.hasNext();) {
					Entry<String, Integer> entry = iterator.next();
					System.out.printf("%d\t", entry.getValue());
					System.out.printf("%s\n", entry.getKey());

					countAndLabels[i++] = new CountAndLabel(entry.getValue(), entry.getKey());

				}

				Arrays.sort(countAndLabels);

				double probabilitySum = 0;

				for (int k = 0; k < numClasses; k++) {
					double probability = (double) countAndLabels[k].count / totalNumTags;
					probabilitySum += probability;
					System.out.printf("%d\t%d\t%f\t%f\t%s\n", k + 1, countAndLabels[k].count, probability, probabilitySum, countAndLabels[k].label);

				}
				System.out.printf("probabilitySum = %f\n", probabilitySum);
			}
			System.out.println();

			double averageNumTagsPerTrial = (double) totalNumTags / (double) trialIndex;
			System.out.printf("averageNumTagsPerTrial = %f\n", averageNumTagsPerTrial);

			Integer numLycopodium = classCounts.get("lyc");
			if (numLycopodium == null)
				numLycopodium = 0;
			System.out.printf("numLycopodium = %d\n", numLycopodium);

			// int numUnkown = classCounts.get("unk");
			// System.out.printf("numUnkown = %d\n", numUnkown);

			// int numKnownTags = totalNumTags - numUnkown;
			//
			double fractionLycopodium = (double) numLycopodium / totalNumTags;
			System.out.printf("fractionLycopodium (of known tags) = %f\n", fractionLycopodium);

			int numSlidesWithPollen = 0;
			for (int j = 0; j < maxNumSlides; j++) {
				if (slidePollenCounts[j] > 0) {
					numSlidesWithPollen++;
				}
			}
			System.out.printf("numSlidesWithPollen = %d\n", numSlidesWithPollen);
			double fractionOfSlidesWithPollen = (double) numSlidesWithPollen / maxNumSlides;
			System.out.printf("fractionOfSlidesWithPollen = %f\n", fractionOfSlidesWithPollen);

			double pollenCountErrorRate = (double) pollenCountErrorSum / (double) numTrialsAnalyzed;
			double exactPollenCountErrorRate = (double) exactPollenCountErrorSum / (double) numTrialsAnalyzed;
			System.out.printf("pollenCountErrorSum = %d\n", pollenCountErrorSum);
			System.out.printf("pollenCountErrorRate = %f\n", pollenCountErrorRate);
			System.out.printf("exactPollenCountErrorSum = %d\n", exactPollenCountErrorSum);
			System.out.printf("exactPollenCountErrorRate = %f\n", exactPollenCountErrorRate);

		}

		int countErrorSum = 0;
		int exactCountErrorSum = 0;
		for (int slideIndex = 0; slideIndex < maxNumSlides; slideIndex++) {
			System.out.printf("allSlidePollenCounts[MASTER][i] = %d\n", allSlidePollenCounts[masterSubjectIndex][slideIndex]);
			System.out.printf("allSlidePollenCounts[TESTER][i] = %d\n", allSlidePollenCounts[testerSubjectIndex][slideIndex]);

			countErrorSum += Math.abs(allSlidePollenCounts[masterSubjectIndex][slideIndex] - allSlidePollenCounts[testerSubjectIndex][slideIndex]);
			if (allSlidePollenCounts[masterSubjectIndex][slideIndex] != allSlidePollenCounts[testerSubjectIndex][slideIndex]) {
				exactCountErrorSum++;
			}
		}

		System.out.printf("countErrorSum = %d\n", countErrorSum);
		System.out.printf("exactCountErrorSum = %d\n", exactCountErrorSum);

		double averageCountErrorPerSlide = (double) countErrorSum / maxNumSlides;
		double exactCountErrorProbability = (double) exactCountErrorSum / maxNumSlides;

		System.out.printf("averageCountErrorPerSlide = %f\n", averageCountErrorPerSlide);
		System.out.printf("exactCountErrorProbability = %f\n", exactCountErrorProbability);
	}

	int unsignedByteToInt(byte byteValue) {
		if (byteValue < 0)
			return 256 + byteValue;
		else
			return (int) byteValue;
	}

	public void createPollenNoPollenExamples() {

		createTrialViewPaths(true);

		boolean exportImagesAsBMP = false;
		if (exportImagesAsBMP) {

			// export images a bmp

			for (int i = exampleGenerationStartTrialIndex; i < exampleGenerationEndTrialIndex; i++) {

				System.out.printf("trialIndex = %d\n", i);

				int roundIndex = trialViewRounds[i];
				int slideIndex = trialViewSlides[i];
				int numTags = trialNumTags[i];

				if (false) {
					System.out.println();
					System.out.printf("trial %d\n", i);
					System.out.printf("roundIndex %d\n", roundIndex);
					System.out.printf("slideIndex %d\n", slideIndex);
					System.out.printf("numTags %d\n", numTags);
				}

				int masterNumTags = trialNumTags[i];

				byte[][] byteBuffers = null;
				DataBuffer[] dataBuffers = null;

				System.out.printf("load data buffers for %s\n", trialViewPaths[i]);
				byteBuffers = (byte[][]) IO.readObject(trialViewPaths[i]);

				dataBuffers = new DataBuffer[numZPlanes];
				for (int j = 0; j < numZPlanes; j++) {
					dataBuffers[j] = new DataBufferByte(byteBuffers[j], byteBuffers[j].length);
				}

				BufferedImage image = new BufferedImage((int) targetImageWidth, (int) targetImageHeight, BufferedImage.TYPE_3BYTE_BGR);

				SampleModel sampleModel = new ComponentSampleModel(DataBuffer.TYPE_BYTE, targetImageWidth, targetImageHeight, numBands, targetImageWidth * numBands, new int[] { 0, 1, 2 });

				// ImagePlus ip = new ImagePlus();

				// // find an appropriate writer
				// Iterator it = ImageIO.getImageWritersByFormatName("jpg");
				// ij.io.ImageWriter writer;
				// if (it.hasNext()) {
				// writer = (ImageWriter) it.next();
				// } else {
				// System.exit(0);
				// }

				for (int rasterIndex = 0; rasterIndex < numZPlanes; rasterIndex++) {

					Raster raster = Raster.createRaster(sampleModel, dataBuffers[rasterIndex], null);
					image.setData(raster);

					try {

						String fileName = "/media/Pollen1/dump/pollen.Round-" + (roundIndex + 1) + ".Slide-" + (slideIndex + 1) + ".Z-" + (rasterIndex + 1) + ".bmp";

						// ImageJ ij = new ImageJ();

						// Image image2 = ij.createImage(targetImageWidth,
						// targetImageHeight);

						ImageIO.write(image, "bmp", new File(fileName));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			}

			System.exit(0);
		}

		double cropFactor = 1.0;

		// int numZPlanesToSearch = 1;
		// int startZPlaneIndexToSearch = 20;

		// boolean createExamples = true;

		double retryProbability = 0.0;

		String examplePathString = defaultDataPath + File.separatorChar + exampleFileName;

		// createTrialViewPaths(false);

		// int startRoundIndex = 1;
		// int endRoundIndex = 2;
		//
		// int startTrial = maxNumSlides * startRoundIndex;
		// int endTrial = maxNumSlides * endRoundIndex;

		// int endTrialIndex = 998;
		// int endTrialIndex = 922;
		// int endTrialIndex = 16;

		int numTrials = exampleGenerationEndTrialIndex - exampleGenerationStartTrialIndex;

		// int[] zDist = new int[numZPlanes];
		// int maxRadius = 500;
		// int[] rDist = new int[maxRadius];

		// find all class names and count classes //

		HashMap<String, Integer> classNameToIndex = new HashMap<String, Integer>();

		// int maxNumClasses = 100;
		int maxNumClasses = 2;
		String[] classNames = new String[maxNumClasses];

		classNames[0] = "notPollen";
		classNames[1] = "Pollen";
		int numClasses = 2; // classIndex 0 is for "not pollen"

		// for (int trialIndex = startTrialIndex; trialIndex < endTrialIndex;
		// trialIndex++) {
		//
		// int numTags = trialNumTags[trialIndex];
		//
		// for (int j = 0; j < numTags; j++) {
		//
		// int xPos = trialClassificationX[trialIndex][j] - (targetImageWidth -
		// taggableImageWidth) / 2;
		// int yPos = trialClassificationY[trialIndex][j] - (targetImageHeight -
		// taggableImageHeight) / 2;
		//
		// // if (xPos < 0 || xPos >= taggableImageWidth) {
		// // continue;
		// // }
		// //
		// // if (yPos < 0 || yPos >= taggableImageHeight) {
		// // continue;
		// // }
		//
		// String label = trialClassificationText[trialIndex][j];
		//
		// // System.out.printf("label %s\n", label);
		//
		// String className = label.substring(0, 3);
		//
		// Integer index = classNameToIndex.get(className);
		//
		// if (index == null) {
		// classNames[numClasses] = className;
		// classNameToIndex.put(className, numClasses++);
		// }
		// }
		//
		// }

		if (true) {
			System.out.printf("numClasses = %d\n", numClasses);
			for (int i = 0; i < numClasses; i++) {
				System.out.printf("classNames[%d] = %s\n", i, classNames[i]);

			}
		}

		int numExamples = numTrials * numExamplesPerTrial;
		int numBands = 3;
		int heightAndWidthWindowSizeInPixels = 1 + (exampleGenerationheightAndWidthRadiusInPixels - 1) * 2;
		int depthWindowSizeInPixels = 1 + (exampleGenerationDepthRadiusInPixels - 1) * 2;

		System.out.printf("depthRadiusInPixels              = %d\n", exampleGenerationDepthRadiusInPixels);
		System.out.printf("depthWindowSizeInPixels          = %d\n", depthWindowSizeInPixels);
		System.out.printf("heightAndWidthRadiusInPixels     = %d\n", exampleGenerationheightAndWidthRadiusInPixels);
		System.out.printf("heightAndWidthWindowSizeInPixels = %d\n", heightAndWidthWindowSizeInPixels);

		int numInputFeatures = depthWindowSizeInPixels * heightAndWidthWindowSizeInPixels * heightAndWidthWindowSizeInPixels * numBands;
		int numOutputFeatures = 1;
		System.out.printf("numExamples       = %d\n", numExamples);
		System.out.printf("numInputFeatures  = %d\n", numInputFeatures);
		System.out.printf("numOutputFeatures = %d\n", numOutputFeatures);

		byte[][] inputFeatureValues = new byte[numExamples][numInputFeatures];
		int[] outputFeatureValues = new int[numExamples];
		int[] exampleGroups = new int[numExamples];

		int[] hitDistByClass = new int[numClasses];

		long numPollenHits = 0;
		long numRandomPoints = 0;
		int exampleIndex = 0;
		int groupIndex = 0;

		int widthPadInPixels = (targetImageWidth - taggableImageWidth) / 2;
		int heightPadInPixels = (targetImageHeight - taggableImageHeight) / 2;

		int[] pixelValues = new int[numBands];

		for (int i = exampleGenerationStartTrialIndex; i < exampleGenerationEndTrialIndex; i++) {

			System.out.printf("trialIndex = %d\n", i);

			int roundIndex = trialViewRounds[i];
			int slideIndex = trialViewSlides[i];
			int numTags = trialNumTags[i];

			if (false) {
				System.out.println();
				System.out.printf("trial %d\n", i);
				System.out.printf("roundIndex %d\n", roundIndex);
				System.out.printf("slideIndex %d\n", slideIndex);
				System.out.printf("numTags %d\n", numTags);
			}

			int masterNumTags = trialNumTags[i];

			byte[][] byteBuffers = null;
			DataBuffer[] dataBuffers = null;

			System.out.printf("load data buffers for %s\n", trialViewPaths[i]);
			byteBuffers = (byte[][]) IO.readObject(trialViewPaths[i]);

			dataBuffers = new DataBuffer[numZPlanes];
			for (int j = 0; j < numZPlanes; j++) {
				dataBuffers[j] = new DataBufferByte(byteBuffers[j], byteBuffers[j].length);
			}

			// int numZPlanesToSearch = 31;
			// int startZPlaneIndexToSearch = 5;
			for (int trialExampleGenerationIndex = 0; trialExampleGenerationIndex < numExamplesPerTrial; trialExampleGenerationIndex++) {

				int exampleX = (int) (Math.random() * taggableImageWidth) + widthPadInPixels;
				int exampleY = (int) (Math.random() * taggableImageHeight) + heightPadInPixels;
				int exampleZ = (int) (Math.random() * exampleGenerationNumZPlanesToSearch) + exampleGenerationStartZPlaneIndexToSearch;

				// classify point

				boolean pollenHit = false;
				int classIndex = 0; // assume not pollen
				for (int j = 0; j < numTags; j++) {

					int xPos = trialClassificationX[i][j];
					int yPos = trialClassificationY[i][j];
					int zPos = trialClassificationZ[i][j];
					int width = trialClassificationW[i][j];

					int radius = width / 2;
					// zDist[zPos - minZPlane]++;
					// rDist[radius]++;

					// System.out.printf("xPos %s\n", xPos);
					// System.out.printf("yPos %s\n", yPos);
					// System.out.printf("zPos %s\n", zPos);

					// String label = trialClassificationText[i][j];

					// System.out.printf("label %s\n", label);

					// String className = label.substring(0, 3);
					// System.out.printf("className %s\n", className);

					// if (xPos < 0 || xPos >= taggableImageWidth) {
					// continue;
					// }
					//
					// if (yPos < 0 || yPos >= taggableImageHeight) {
					// continue;
					// }

					{
						int xDiff = exampleX - xPos;
						int yDiff = exampleY - yPos;
						int distance = xDiff * xDiff + yDiff * yDiff;
						int matchWidth = (int) (width * cropFactor);
						if (distance < matchWidth * matchWidth) {

							// classIndex = classNameToIndex.get(className);

							classIndex = 1;

							// System.out.printf("match with tag %s\n",
							// className);
							pollenHit = true;
							// if (focusOnExpertZplane)
							// exampleZ = zPos - minZPlane; // set example to
							// expert selected z-plane
							break;

						}
					}

				}

				if (!pollenHit && Math.random() < retryProbability) {
					trialExampleGenerationIndex--;
					continue;
				}

				if (pollenHit) {
					numPollenHits++;
				}
				numRandomPoints++;

				hitDistByClass[classIndex]++;

				// set input feature values

				int inputFeatureIndex = 0;

				SampleModel sampleModel = new ComponentSampleModel(DataBuffer.TYPE_BYTE, targetImageWidth, targetImageHeight, numBands, targetImageWidth * numBands, new int[] { 0, 1, 2 });

				int startZ = 1 - exampleGenerationDepthRadiusInPixels + exampleZ;
				int endZ = startZ + depthWindowSizeInPixels;

				for (int rasterIndex = startZ; rasterIndex < endZ; rasterIndex++) {

					// byte[] bytes = byteBuffers[rasterIndex];

					// DataBuffer buffer = new DataBufferByte(bytes,
					// bytes.length);
					Raster raster = Raster.createRaster(sampleModel, dataBuffers[rasterIndex], null);

					int startX = 1 - exampleGenerationheightAndWidthRadiusInPixels + exampleX;
					int endX = 1 - exampleGenerationheightAndWidthRadiusInPixels + exampleX + heightAndWidthWindowSizeInPixels;
					int startY = 1 - exampleGenerationheightAndWidthRadiusInPixels + exampleY;
					int endY = 1 - exampleGenerationheightAndWidthRadiusInPixels + exampleY + heightAndWidthWindowSizeInPixels;

					for (int x = startX; x < endX; x++) {

						for (int y = startY; y < endY; y++) {

							raster.getPixel(x, y, pixelValues);

							for (int b = 0; b < numBands; b++) {

								inputFeatureValues[exampleIndex][inputFeatureIndex++] = (byte) pixelValues[b];
								if (false)
									System.out.printf("pixelValues[%d] = %d (%d)\n", b, pixelValues[b], unsignedByteToInt((byte) pixelValues[b]));
							}
						}
					}
					// System.out.println("sum = " + sum);

				}

				// set output feature value

				if (classIndex == 0) {
					outputFeatureValues[exampleIndex] = (byte) 0;
				} else {
					outputFeatureValues[exampleIndex] = (byte) 1;
				}

				// System.out.printf("classIndex = %d\n", classIndex);
				exampleGroups[exampleIndex] = groupIndex;

				exampleIndex++;
			}

			groupIndex++;

		}

		int numGroups = groupIndex;
		System.out.printf("numGroups = %d\n", numGroups);

		double pollenHitRate = (double) numPollenHits / (double) numRandomPoints;
		System.out.printf("pollenHitRate = %f\n", pollenHitRate);

		System.out.printf("numClasses = %d\n", numClasses);
		System.out.printf("Example Class Distribution:\n");
		for (int i = 0; i < numClasses; i++) {
			System.out.printf("%d\t%s\t%d\n", i, classNames[i], hitDistByClass[i]);

		}

		// if (false) {
		// for (int j = 0; j < numZPlanes; j++) {
		// System.out.printf("zDist[%d] = %d\n", j, zDist[j]);
		// }
		// for (int j = 0; j < maxRadius; j++) {
		// System.out.printf("rDist[%d] = %d\n", j, rDist[j]);
		// }
		// }

		Object[] results = new Object[] { numExamples, heightAndWidthWindowSizeInPixels, classNames, inputFeatureValues, outputFeatureValues, exampleGroups };

		IO.writeObject(examplePathString, results);
	}

	public void extractPollenFeatures() {

		createTrialViewPaths(true);

		double cropFactor = 1.0;

		double retryProbability = 0.0;

		String examplePathString = defaultDataPath + File.separatorChar + exampleFileName;

		// createTrialViewPaths(false);

		// int startRoundIndex = 1;
		// int endRoundIndex = 2;
		//
		// int startTrial = maxNumSlides * startRoundIndex;
		// int endTrial = maxNumSlides * endRoundIndex;

		// int endTrialIndex = 998;
		// int endTrialIndex = 922;
		// int endTrialIndex = 16;

		int numTrials = exampleGenerationEndTrialIndex - exampleGenerationStartTrialIndex;

		// int[] zDist = new int[numZPlanes];
		// int maxRadius = 500;
		// int[] rDist = new int[maxRadius];

		// find all class names and count classes //

		HashMap<String, Integer> classNameToIndex = new HashMap<String, Integer>();

		// int maxNumClasses = 100;
		int maxNumClasses = 2;
		String[] classNames = new String[maxNumClasses];

		classNames[0] = "notPollen";
		classNames[1] = "Pollen";
		int numClasses = 2; // classIndex 0 is for "not pollen"

		// for (int trialIndex = startTrialIndex; trialIndex < endTrialIndex;
		// trialIndex++) {
		//
		// int numTags = trialNumTags[trialIndex];
		//
		// for (int j = 0; j < numTags; j++) {
		//
		// int xPos = trialClassificationX[trialIndex][j] - (targetImageWidth -
		// taggableImageWidth) / 2;
		// int yPos = trialClassificationY[trialIndex][j] - (targetImageHeight -
		// taggableImageHeight) / 2;
		//
		// // if (xPos < 0 || xPos >= taggableImageWidth) {
		// // continue;
		// // }
		// //
		// // if (yPos < 0 || yPos >= taggableImageHeight) {
		// // continue;
		// // }
		//
		// String label = trialClassificationText[trialIndex][j];
		//
		// // System.out.printf("label %s\n", label);
		//
		// String className = label.substring(0, 3);
		//
		// Integer index = classNameToIndex.get(className);
		//
		// if (index == null) {
		// classNames[numClasses] = className;
		// classNameToIndex.put(className, numClasses++);
		// }
		// }
		//
		// }

		if (true) {
			System.out.printf("numClasses = %d\n", numClasses);
			for (int i = 0; i < numClasses; i++) {
				System.out.printf("classNames[%d] = %s\n", i, classNames[i]);

			}
		}

		int numExamples = numTrials * numExamplesPerTrial;
		int numBands = 3;
		int heightAndWidthWindowSizeInPixels = 1 + (exampleGenerationheightAndWidthRadiusInPixels - 1) * 2;
		int depthWindowSizeInPixels = 1 + (exampleGenerationDepthRadiusInPixels - 1) * 2;

		System.out.printf("depthRadiusInPixels              = %d\n", exampleGenerationDepthRadiusInPixels);
		System.out.printf("depthWindowSizeInPixels          = %d\n", depthWindowSizeInPixels);
		System.out.printf("heightAndWidthRadiusInPixels     = %d\n", exampleGenerationheightAndWidthRadiusInPixels);
		System.out.printf("heightAndWidthWindowSizeInPixels = %d\n", heightAndWidthWindowSizeInPixels);

		int numInputFeatures = depthWindowSizeInPixels * heightAndWidthWindowSizeInPixels * heightAndWidthWindowSizeInPixels * numBands;
		int numOutputFeatures = 1;
		System.out.printf("numExamples       = %d\n", numExamples);
		System.out.printf("numInputFeatures  = %d\n", numInputFeatures);
		System.out.printf("numOutputFeatures = %d\n", numOutputFeatures);

		byte[][] inputFeatureValues = new byte[numExamples][numInputFeatures];
		int[] outputFeatureValues = new int[numExamples];
		int[] exampleGroups = new int[numExamples];

		int[] hitDistByClass = new int[numClasses];

		long numPollenHits = 0;
		long numRandomPoints = 0;
		int exampleIndex = 0;
		int groupIndex = 0;

		int widthPadInPixels = (targetImageWidth - taggableImageWidth) / 2;
		int heightPadInPixels = (targetImageHeight - taggableImageHeight) / 2;

		int[] pixelValues = new int[numBands];

		for (int i = exampleGenerationStartTrialIndex; i < exampleGenerationEndTrialIndex; i++) {

			System.out.printf("trialIndex = %d\n", i);

			int roundIndex = trialViewRounds[i];
			int slideIndex = trialViewSlides[i];
			int numTags = trialNumTags[i];

			if (false) {
				System.out.println();
				System.out.printf("trial %d\n", i);
				System.out.printf("roundIndex %d\n", roundIndex);
				System.out.printf("slideIndex %d\n", slideIndex);
				System.out.printf("numTags %d\n", numTags);
			}

			int masterNumTags = trialNumTags[i];

			byte[][] byteBuffers = null;
			DataBuffer[] dataBuffers = null;

			System.out.printf("load data buffers for %s\n", trialViewPaths[i]);
			byteBuffers = (byte[][]) IO.readObject(trialViewPaths[i]);

			dataBuffers = new DataBuffer[numZPlanes];
			for (int j = 0; j < numZPlanes; j++) {
				dataBuffers[j] = new DataBufferByte(byteBuffers[j], byteBuffers[j].length);
			}

			// int numZPlanesToSearch = 31;
			// int startZPlaneIndexToSearch = 5;
			for (int trialExampleGenerationIndex = 0; trialExampleGenerationIndex < numExamplesPerTrial; trialExampleGenerationIndex++) {

				int exampleX = (int) (Math.random() * taggableImageWidth) + widthPadInPixels;
				int exampleY = (int) (Math.random() * taggableImageHeight) + heightPadInPixels;
				int exampleZ = (int) (Math.random() * exampleGenerationNumZPlanesToSearch) + exampleGenerationStartZPlaneIndexToSearch;

				// classify point

				boolean pollenHit = false;
				int classIndex = 0; // assume not pollen
				for (int j = 0; j < numTags; j++) {

					int xPos = trialClassificationX[i][j];
					int yPos = trialClassificationY[i][j];
					int zPos = trialClassificationZ[i][j];
					int width = trialClassificationW[i][j];

					int radius = width / 2;
					// zDist[zPos - minZPlane]++;
					// rDist[radius]++;

					// System.out.printf("xPos %s\n", xPos);
					// System.out.printf("yPos %s\n", yPos);
					// System.out.printf("zPos %s\n", zPos);

					// String label = trialClassificationText[i][j];

					// System.out.printf("label %s\n", label);

					// String className = label.substring(0, 3);
					// System.out.printf("className %s\n", className);

					// if (xPos < 0 || xPos >= taggableImageWidth) {
					// continue;
					// }
					//
					// if (yPos < 0 || yPos >= taggableImageHeight) {
					// continue;
					// }

					{
						int xDiff = exampleX - xPos;
						int yDiff = exampleY - yPos;
						int distance = xDiff * xDiff + yDiff * yDiff;
						int matchWidth = (int) (width * cropFactor);
						if (distance < matchWidth * matchWidth) {

							// classIndex = classNameToIndex.get(className);

							classIndex = 1;

							// System.out.printf("match with tag %s\n",
							// className);
							pollenHit = true;
							// if (focusOnExpertZplane)
							// exampleZ = zPos - minZPlane; // set example to
							// expert selected z-plane
							break;

						}
					}

				}

				if (!pollenHit && Math.random() < retryProbability) {
					trialExampleGenerationIndex--;
					continue;
				}

				if (pollenHit) {
					numPollenHits++;
				}
				numRandomPoints++;

				hitDistByClass[classIndex]++;

				// set input feature values

				int inputFeatureIndex = 0;

				SampleModel sampleModel = new ComponentSampleModel(DataBuffer.TYPE_BYTE, targetImageWidth, targetImageHeight, numBands, targetImageWidth * numBands, new int[] { 0, 1, 2 });

				int startZ = 1 - exampleGenerationDepthRadiusInPixels + exampleZ;
				int endZ = startZ + depthWindowSizeInPixels;

				for (int rasterIndex = startZ; rasterIndex < endZ; rasterIndex++) {

					// byte[] bytes = byteBuffers[rasterIndex];

					// DataBuffer buffer = new DataBufferByte(bytes,
					// bytes.length);
					Raster raster = Raster.createRaster(sampleModel, dataBuffers[rasterIndex], null);

					int startX = 1 - exampleGenerationheightAndWidthRadiusInPixels + exampleX;
					int endX = 1 - exampleGenerationheightAndWidthRadiusInPixels + exampleX + heightAndWidthWindowSizeInPixels;
					int startY = 1 - exampleGenerationheightAndWidthRadiusInPixels + exampleY;
					int endY = 1 - exampleGenerationheightAndWidthRadiusInPixels + exampleY + heightAndWidthWindowSizeInPixels;

					for (int x = startX; x < endX; x++) {

						for (int y = startY; y < endY; y++) {

							raster.getPixel(x, y, pixelValues);

							for (int b = 0; b < numBands; b++) {

								inputFeatureValues[exampleIndex][inputFeatureIndex++] = (byte) pixelValues[b];
								if (false)
									System.out.printf("pixelValues[%d] = %d (%d)\n", b, pixelValues[b], unsignedByteToInt((byte) pixelValues[b]));
							}
						}
					}
					// System.out.println("sum = " + sum);

				}

				// set output feature value

				if (classIndex == 0) {
					outputFeatureValues[exampleIndex] = (byte) 0;
				} else {
					outputFeatureValues[exampleIndex] = (byte) 1;
				}

				// System.out.printf("classIndex = %d\n", classIndex);
				exampleGroups[exampleIndex] = groupIndex;

				exampleIndex++;
			}

			groupIndex++;

		}

		int numGroups = groupIndex;
		System.out.printf("numGroups = %d\n", numGroups);

		double pollenHitRate = (double) numPollenHits / (double) numRandomPoints;
		System.out.printf("pollenHitRate = %f\n", pollenHitRate);

		System.out.printf("numClasses = %d\n", numClasses);
		System.out.printf("Example Class Distribution:\n");
		for (int i = 0; i < numClasses; i++) {
			System.out.printf("%d\t%s\t%d\n", i, classNames[i], hitDistByClass[i]);

		}

		// if (false) {
		// for (int j = 0; j < numZPlanes; j++) {
		// System.out.printf("zDist[%d] = %d\n", j, zDist[j]);
		// }
		// for (int j = 0; j < maxRadius; j++) {
		// System.out.printf("rDist[%d] = %d\n", j, rDist[j]);
		// }
		// }

		Object[] results = new Object[] { numExamples, heightAndWidthWindowSizeInPixels, classNames, inputFeatureValues, outputFeatureValues, exampleGroups };

		IO.writeObject(examplePathString, results);
	}

	public double computeLikelihood(int classDistCounts[], int numClasses, String[] classNames, int numExamples, double[] classPDF, boolean verbose) {

		int numZeros = 0;
		for (int i = 0; i < numClasses; i++) {
			if (classDistCounts[i] == 0) {
				numZeros++;
			}
		}
		if (false)
			System.out.printf("numZeros = %d\n", numZeros);

		// double[] classPDF = new double[numClasses];

		if (numZeros > 0) {
			double zeroCountProbability = 1.0 / numExamples / numZeros;
			double classProbabilityDecrement = zeroCountProbability * numZeros / (numClasses - numZeros);
			for (int i = 0; i < numClasses; i++) {
				if (classDistCounts[i] == 0) {
					classPDF[i] = zeroCountProbability;
				} else {
					classPDF[i] = (double) classDistCounts[i] / numExamples - classProbabilityDecrement;
				}
			}
		} else {
			for (int i = 0; i < numClasses; i++) {
				classPDF[i] = (double) classDistCounts[i] / numExamples;
			}
		}

		double probabilitySum = 0.0;

		if (verbose) {
			for (int i = 0; i < numClasses; i++) {
				if (classPDF[i] > 0.01)
					System.out.printf("%d\t%s\t%12.9f\n", i, classNames[i], classPDF[i]);
			}
			for (int i = 0; i < numClasses; i++) {
				probabilitySum += classPDF[i];
			}
			System.out.printf("probabilitySum = %f\n", probabilitySum);
		}

		// compute likelihood

		double likelkihoodSum = 0.0;
		for (int i = 0; i < numClasses; i++) {
			likelkihoodSum += classPDF[i] * Math.log10(classPDF[i]);
		}

		return likelkihoodSum;

	}

	public void computePDF(int classDistCounts[], int numClasses, int numExamples, double[] classPDF) {

		int numZeros = 0;
		for (int i = 0; i < numClasses; i++) {
			if (classDistCounts[i] == 0) {
				numZeros++;
			}
		}
		if (false)
			System.out.printf("numZeros = %d\n", numZeros);

		double zeroCountProbability = 1.0 / numExamples / numZeros;
		double classProbabilityDecrement = zeroCountProbability * numZeros / (numClasses - numZeros);
		for (int i = 0; i < numClasses; i++) {
			if (classDistCounts[i] == 0) {
				classPDF[i] = zeroCountProbability;
			} else {
				classPDF[i] = (double) classDistCounts[i] / numExamples - classProbabilityDecrement;
			}
		}

	}

	byte[] decompositionInputFeatureValues = null;
	int decompositionSplitFeatureIndex = (short) -1;
	short decompositionSplitFeatureValueInteger = -1;
	byte[] decompositionInstanceFeatureValues1 = null;
	byte[] decompositionInstanceFeatureValues2 = null;

	// public int testDecomposition() {
	//
	// // int exampleChildeNodeIndex = -1;
	//
	// if
	// (unsignedByteToInt(decompositionInputFeatureValues[decompositionSplitFeatureIndex])
	// < decompositionSplitFeatureValueInteger) {
	// // exampleChildeNodeIndex = 0;
	// return 0;
	// } else {
	// // exampleChildeNodeIndex = 1;
	// return 1;
	// }
	//
	// // return exampleChildeNodeIndex;
	// }

	public void computeTree(String[] args) {

		boolean verbose = false;

		int exampleTrainTestPartitionRandomSeed = 123;

		Random exampleTrainTestPartitionRandom = new Random(exampleTrainTestPartitionRandomSeed);

		int numArgs = args.length;

		String exampleSetPath = args[numArgs - 7];
		int numValidationTrials = Integer.parseInt(args[numArgs - 6]);
		int numTrainGroups = Integer.parseInt(args[numArgs - 5]);
		int minNumExamplesPerNode = Integer.parseInt(args[numArgs - 4]);
		int numTriesPerSplit = Integer.parseInt(args[numArgs - 3]);
		numTreesInForest = Integer.parseInt(args[numArgs - 2]);
		boolean createFinalModel = Boolean.parseBoolean(args[numArgs - 1]);

		System.out.printf("dataPath = %s\n", exampleSetPath);
		System.out.printf("numValidationTrials = %d\n", numValidationTrials);
		System.out.printf("numTrainGroups = %d\n", numTrainGroups);
		System.out.printf("minNumExamplesPerNode = %d\n", minNumExamplesPerNode);
		System.out.printf("numTriesPerSplit = %d\n", numTriesPerSplit);
		System.out.printf("numTreesInForrest = %d\n", numTreesInForest);
		System.out.printf("createFinalModel = %b\n", createFinalModel);

		double minDeltaPerformanceForSplit = Double.NEGATIVE_INFINITY;

		long startReadTime = System.currentTimeMillis();

		Object[] exampleSet = (Object[]) IO.readObject(exampleSetPath);

		long endReadTime = System.currentTimeMillis();
		double readDuration = (endReadTime - startReadTime) / 1000.0;
		System.out.printf("readDuration = %f\n", readDuration);

		long startTime = System.currentTimeMillis();

		int partIndex = 0;

		int numAllExamples = (Integer) exampleSet[partIndex++];
		int windowWidthInPixels = (Integer) exampleSet[partIndex++];
		String[] classNames = (String[]) exampleSet[partIndex++];
		byte[][] allInputFeatureValues = (byte[][]) exampleSet[partIndex++];
		int[] allOutputFeatureValues = (int[]) exampleSet[partIndex++];
		int[] allExampleGroupIDs = (int[]) exampleSet[partIndex++];

		numInputFeatures = allInputFeatureValues[0].length;
		int numAllGroups = allExampleGroupIDs[numAllExamples - 1] + 1;

		// {
		//
		// // transpose array save and exit
		//
		// byte[][] transposedAllInputFeatureValues = new
		// byte[numInputFeatures][numAllExamples];
		//
		// for (int e = 0; e < numAllExamples; e++) {
		// for (int f = 0; f < numInputFeatures; f++) {
		// transposedAllInputFeatureValues[f][e] = allInputFeatureValues[e][f];
		// }
		// }
		//
		// long startWriteTime = System.currentTimeMillis();
		//
		// exampleSet[3] = transposedAllInputFeatureValues;
		//
		// IO.writeObject(exampleSetPath + ".trans", exampleSet);
		//
		// long endWriteTime = System.currentTimeMillis();
		// double writeDuration = (endWriteTime - startWriteTime) / 1000.0;
		// System.out.printf("writeDuration = %f\n", writeDuration);
		//
		// System.exit(0);
		//
		// }

		boolean twoClassMode = true;
		boolean allClassMode = !twoClassMode;

		int numClasses = -1;

		if (twoClassMode) {
			numClasses = 2;
			classNames[0] = "not pollen";
			classNames[1] = "pollen";

			for (int exampleIndex = 0; exampleIndex < numAllExamples; exampleIndex++) {
				if (allOutputFeatureValues[exampleIndex] > 0) {
					allOutputFeatureValues[exampleIndex] = 1;
				}
			}
		}
		if (allClassMode) {
			numClasses = 0;
			for (int i = 0; i < classNames.length; i++) {
				if (classNames[i] == null)
					break;
				numClasses++;
			}
		}

		System.out.printf("numClasses = %d\n", numClasses);
		System.out.printf("numAllExamples = %d\n", numAllExamples);
		System.out.printf("numInputFeatures = %d\n", numInputFeatures);
		System.out.printf("numAllGroups = %d\n", numAllGroups);

		int numExamplesPerGroup = numAllExamples / numAllGroups;

		// double[] rootClassPDF = new double[numClasses];

		float rootClass1Prob = Float.NaN;
		float rootClass2Prob = Float.NaN;

		float class1ProbChild1 = Float.NaN;
		float class2ProbChild1 = Float.NaN;
		float class1ProbChild2 = Float.NaN;
		float class2ProbChild2 = Float.NaN;

		float bestClass1ProbChild1 = Float.NaN;
		float bestClass2ProbChild1 = Float.NaN;
		float bestClass1ProbChild2 = Float.NaN;
		float bestClass2ProbChild2 = Float.NaN;

		double averagePerformanceSum = 0;

		int maxPollenCountPerTrial = 20;
		double[] pollenCountProbabilitySums = new double[maxPollenCountPerTrial];
		int[] pollenCountNumObservations = new int[maxPollenCountPerTrial];

		// int numTrainGroups = (int) (numAllGroups * trainingFraction);
		int numTestGroups;

		if (createFinalModel) {
			numTrainGroups = numAllGroups;
			numTestGroups = 0;
			numValidationTrials = 1;
		} else {
			numTestGroups = numAllGroups - numTrainGroups;
		}

		int numTrainExamples = numExamplesPerGroup * numTrainGroups;
		int numTestExamples = numExamplesPerGroup * numTestGroups;

		System.out.printf("numAllGroups     = %d\n", numAllGroups);
		System.out.printf("numTrainGroups   = %d\n", numTrainGroups);
		System.out.printf("numTestGroups    = %d\n", numTestGroups);
		System.out.printf("numTrainExamples = %d\n", numTrainExamples);
		System.out.printf("numTestExamples  = %d\n", numTestExamples);

		int maxNumObservations = numAllGroups * numValidationTrials;
		int[] masterGroupPollenCounts = new int[numAllGroups];
		double[] masterPollenCounts = new double[maxNumObservations];
		double[] machinePollenProbabilitySums = new double[maxNumObservations];
		int observationIndex = 0;

		for (int i = 0; i < numAllGroups; i++) {
			masterGroupPollenCounts[i] = getPollenCountInBox(this, i);
			// System.out.printf("masterGroupPollenCounts[i]     = %d\n",
			// masterGroupPollenCounts[i]);
		}

		for (int validationTrialIndex = 0; validationTrialIndex < numValidationTrials; validationTrialIndex++) {

			/************************************************/
			/* PARTITION EXAMPLES IN TO TRAIN AND TEST SETS */
			/************************************************/

			byte[][] trainInputFeatureValues = new byte[numTrainExamples][];
			int[] trainOutputFeatureValues = new int[numTrainExamples];
			int[] trainGroupIndices = new int[numTrainExamples];

			byte[][] testInputFeatureValues = new byte[numTestExamples][];
			int[] testOutputFeatureValues = new int[numTestExamples];
			int[] testGroupIDs = new int[numTestExamples];

			int[] randomGroupIDs = Utility.randomIntArray(exampleTrainTestPartitionRandom, numAllGroups);

			{
				int trainExampleIndex = 0;
				int testExampleIndex = 0;
				for (int exampleIndex = 0; exampleIndex < numAllExamples; exampleIndex++) {

					int exampleGroupID = allExampleGroupIDs[exampleIndex];

					boolean exampleIsTrainingExample = false;

					for (int j = 0; j < numTrainGroups; j++) {
						if (randomGroupIDs[j] == exampleGroupID) {
							exampleIsTrainingExample = true;
							break;
						}
					}

					if (exampleIsTrainingExample) {

						trainInputFeatureValues[trainExampleIndex] = allInputFeatureValues[exampleIndex];
						trainOutputFeatureValues[trainExampleIndex] = allOutputFeatureValues[exampleIndex];

						trainGroupIndices[trainExampleIndex] = allExampleGroupIDs[exampleIndex];

						trainExampleIndex++;
					} else {

						testInputFeatureValues[testExampleIndex] = allInputFeatureValues[exampleIndex];
						testOutputFeatureValues[testExampleIndex] = allOutputFeatureValues[exampleIndex];

						testGroupIDs[testExampleIndex] = allExampleGroupIDs[exampleIndex];

						testExampleIndex++;
					}

				}
				System.out.printf("trainExampleIndex = %d\n", trainExampleIndex);
				System.out.printf("testExampleIndex  = %d\n", testExampleIndex);
			}

			/**********************************************/
			/* USE TRAIN EXAMPLES TO CREATE DECISION TREE */
			/**********************************************/

			double performanceSum = 0.0;
			long evaluationStartTime = System.currentTimeMillis();

			double[][] groupClassProbabilitySums = new double[numAllGroups][numClasses];

			forestTreeNumNodes = new int[numTreesInForest];

			// node memory //

			maxNumNodes = (int) numTrainExamples;

			forestTreeNodeChild1Indices = new int[numTreesInForest][];
			forestTreeNodeChild2Indices = new int[numTreesInForest][];

			forestTreeNodeExamplesStartIndices = new int[numTreesInForest][];
			forestTreeNodeExamplesEndIndices = new int[numTreesInForest][];

			forestTreeNodePurities = new float[numTreesInForest][];
			// forestTreeNodeClassPDFs = new
			// double[numTreesInForest][numClasses][];
			forestTreeNodeClass1Probs = new float[numTreesInForest][];
			// forestTreeNodeClass2Probs = new float[numTreesInForest][];

			forestTreeNodeSplitFeatureIndices = new short[numTreesInForest][];
			forestTreeNodeSplitFeatureValues = new short[numTreesInForest][];

			for (int treeIndex = 0; treeIndex < numTreesInForest; treeIndex++) {

				// allocate all memory for tree */

				System.out.printf("allocating all memory for tree\n");
				nodeChild1Indices = new int[maxNumNodes];
				nodeChild2Indices = new int[maxNumNodes];

				nodeExamplesStartIndices = new int[maxNumNodes];
				nodeExamplesEndIndices = new int[maxNumNodes];

				nodePurities = new float[maxNumNodes];
				// nodeClassPDFs = new double[maxNumNodes][numClasses];
				nodeClass1Probs = new float[maxNumNodes];
				// nodeClass2Probs = new float[maxNumNodes];

				nodeSplitFeatureIndices = new short[maxNumNodes];
				nodeSplitFeatureValues = new short[maxNumNodes];

				forestTreeNodeChild1Indices[treeIndex] = nodeChild1Indices;
				forestTreeNodeChild2Indices[treeIndex] = nodeChild2Indices;

				forestTreeNodeExamplesStartIndices[treeIndex] = nodeExamplesStartIndices;
				forestTreeNodeExamplesEndIndices[treeIndex] = nodeExamplesEndIndices;

				forestTreeNodePurities[treeIndex] = nodePurities;
				// forestTreeNodeClassPDFs[treeIndex] = nodeClassPDFs;
				forestTreeNodeClass1Probs[treeIndex] = nodeClass1Probs;
				// forestTreeNodeClass2Probs[treeIndex] = nodeClass2Probs;

				forestTreeNodeSplitFeatureIndices[treeIndex] = nodeSplitFeatureIndices;
				forestTreeNodeSplitFeatureValues[treeIndex] = nodeSplitFeatureValues;
				System.out.printf("done allocating all memory for tree\n");

				int[] rootClassDistCounts = new int[numClasses];
				for (int exampleIndex = 0; exampleIndex < numTrainExamples; exampleIndex++) {
					rootClassDistCounts[trainOutputFeatureValues[exampleIndex]]++;
				}

				if (true) {
					System.out.printf("Example Class Distribution:\n");
					for (int i = 0; i < numClasses; i++) {
						System.out.printf("%d\t%s\t%d\n", i, classNames[i], rootClassDistCounts[i]);
					}
				}

				rootClass1Prob = (float) rootClassDistCounts[0] / numTrainExamples;
				rootClass2Prob = (float) rootClassDistCounts[1] / numTrainExamples;

				float rootPurity;
				if (rootClass1Prob > rootClass2Prob) {
					rootPurity = rootClass1Prob;
				} else {
					rootPurity = rootClass2Prob;
				}

				// generate and test splits

				// int[] classDistCountsChild1 = new int[numClasses];
				// int[] classDistCountsChild2 = new int[numClasses];

				int class1DistCountsChild1 = 0;
				int class2DistCountsChild1 = 0;

				int class1DistCountsChild2 = 0;
				int class2DistCountsChild2 = 0;

				nodeChild1Indices = forestTreeNodeChild1Indices[treeIndex];
				nodeChild2Indices = forestTreeNodeChild2Indices[treeIndex];

				nodeExamplesStartIndices = forestTreeNodeExamplesStartIndices[treeIndex];
				nodeExamplesEndIndices = forestTreeNodeExamplesEndIndices[treeIndex];

				nodePurities = forestTreeNodePurities[treeIndex];

				nodeClass1Probs = forestTreeNodeClass1Probs[treeIndex];
				// nodeClass2Probs = forestTreeNodeClass2Probs[treeIndex];

				nodeSplitFeatureIndices = forestTreeNodeSplitFeatureIndices[treeIndex];
				nodeSplitFeatureValues = forestTreeNodeSplitFeatureValues[treeIndex];

				int nodeIndex = 0;

				// set up root node

				nodePurities[nodeIndex] = rootPurity;
				nodeExamplesStartIndices[nodeIndex] = 0;
				nodeExamplesEndIndices[nodeIndex] = numTrainExamples;
				nodeChild1Indices[nodeIndex] = -1;
				nodeChild2Indices[nodeIndex] = -1;
				nodeClass1Probs[nodeIndex] = rootClass1Prob;
				// nodeClass2Probs[nodeIndex] = rootClass2Prob;

				int newNodeIndex = 1;

				// int[] zeroByteValueCounts = new int[256];
				int[] byteValueCounts = new int[256];

				float bestPurity1 = Float.NaN;
				float bestPurity2 = Float.NaN;
				short bestSplitFeatureIndex = -1;
				short bestSplitFeatureValue = -1;

				while (nodeIndex < newNodeIndex) {

					if (nodePurities[nodeIndex] != 1.0f) {

						int exampleStartIndex = nodeExamplesStartIndices[nodeIndex];
						int exampleEndIndex = nodeExamplesEndIndices[nodeIndex];
						int nodeNumExamples = exampleEndIndex - exampleStartIndex;
						int halfNodeNumExamples = nodeNumExamples / 2;

						if (false) {
							System.out.printf("\n");
							System.out.printf("\n");
							System.out.printf("nodeIndex           = %d\n", nodeIndex);
							System.out.printf("newNodeIndex        = %d\n", newNodeIndex);
							int numNodesLeftToSplit = newNodeIndex - nodeIndex;
							// System.out.printf("numNodesLeftToSplit = %d\n",
							// numNodesLeftToSplit);
							System.out.printf("exampleStartIndex   = %d\n", exampleStartIndex);
							System.out.printf("exampleEndIndex     = %d\n", exampleEndIndex);
							System.out.printf("nodeNumExamples     = %d\n", nodeNumExamples);
						}

						float bestDeltaPerformance = Float.NEGATIVE_INFINITY;
						int bestNumExamplesChild1 = -1;
						int bestNumExamplesChild2 = -1;

						for (int tryIndex = 0; tryIndex < numTriesPerSplit; tryIndex++) {

							decompositionSplitFeatureIndex = (short) (Math.random() * numInputFeatures);

							if (treeGenerationSplitEvaluationSampleSize == 1) {

								// same result for median or mean splitting in
								// this case

								int exampleIndex = (int) (Math.random() * nodeNumExamples) + exampleStartIndex;
								decompositionSplitFeatureValueInteger = (short) unsignedByteToInt(trainInputFeatureValues[exampleIndex][decompositionSplitFeatureIndex]);

							} else {

								if (modelGenerationSplitTypeIsMedian) {

									for (int i = 0; i < 256; i++) {
										byteValueCounts[i] = 0;
									}

									if (nodeNumExamples <= treeGenerationSplitEvaluationSampleSize) {
										for (int exampleIndex = exampleStartIndex; exampleIndex < exampleEndIndex; exampleIndex++) {
											byteValueCounts[(int) trainInputFeatureValues[exampleIndex][decompositionSplitFeatureIndex] + 128]++;
										}
									} else {
										for (int i = 0; i < treeGenerationSplitEvaluationSampleSize; i++) {
											int exampleIndex = (int) (Math.random() * nodeNumExamples) + exampleStartIndex;

											int index = (int) trainInputFeatureValues[exampleIndex][decompositionSplitFeatureIndex] + 128;

											byteValueCounts[index]++;
										}
									}

									int sum = 0;
									int threshold;
									if (nodeNumExamples <= treeGenerationSplitEvaluationSampleSize) {
										threshold = halfNodeNumExamples;
									} else {
										threshold = treeGenerationSplitEvaluationHalfSampleSize;
									}
									boolean found = false;
									for (int i = 128; i < 256; i++) {
										sum += byteValueCounts[i];
										if (sum >= threshold) {
											decompositionSplitFeatureValueInteger = (short) (i - 128);
											found = true;
											break;
										}
									}
									if (!found) {
										for (int i = 0; i < 127; i++) {
											sum += byteValueCounts[i];
											if (sum >= threshold) {
												decompositionSplitFeatureValueInteger = (short) (i + 128);
												break;
											}
										}
									}
								}

								if (modelGenerationSplitModeIsMean) {
									long valueSum = 0;
									for (int exampleIndex = exampleStartIndex; exampleIndex < exampleEndIndex; exampleIndex++) {
										valueSum += unsignedByteToInt(trainInputFeatureValues[exampleIndex][decompositionSplitFeatureIndex]);
									}

									decompositionSplitFeatureValueInteger = (short) (valueSum / nodeNumExamples);
								}
							}

							class1DistCountsChild1 = 0;
							class2DistCountsChild1 = 0;
							class1DistCountsChild2 = 0;
							class2DistCountsChild2 = 0;

							for (int exampleIndex = exampleStartIndex; exampleIndex < exampleEndIndex; exampleIndex++) {

								// decompositionInputFeatureValues =
								// trainInputFeatureValues[exampleIndex];

								// int exampleChildeNodeIndex =
								// testDecomposition();

								if (unsignedByteToInt(trainInputFeatureValues[exampleIndex][decompositionSplitFeatureIndex]) < decompositionSplitFeatureValueInteger) {

									switch (trainOutputFeatureValues[exampleIndex]) {
									case 0:
										class1DistCountsChild1++;
										break;
									case 1:
										class2DistCountsChild1++;
										break;
									}

									// numExamplesChild1++;
								} else {
									// classDistCountsChild2[trainOutputFeatureValues[exampleIndex]]++;
									// numExamplesChild2++;
									switch (trainOutputFeatureValues[exampleIndex]) {
									case 0:
										class1DistCountsChild2++;
										break;
									case 1:
										class2DistCountsChild2++;
										break;
									}
								}

							}

							int numExamplesChild1 = class1DistCountsChild1 + class2DistCountsChild1;
							int numExamplesChild2 = class1DistCountsChild2 + class2DistCountsChild2;

							float purity1;
							float purity2;
							if (numExamplesChild1 > minNumExamplesPerNode && numExamplesChild2 > minNumExamplesPerNode) {

								class1ProbChild1 = (float) class1DistCountsChild1 / numExamplesChild1;
								class2ProbChild1 = (float) class2DistCountsChild1 / numExamplesChild1;
								if (class1ProbChild1 > class2ProbChild1) {
									purity1 = class1ProbChild1;
								} else {
									purity1 = class2ProbChild1;
								}

								class1ProbChild2 = (float) class1DistCountsChild2 / numExamplesChild2;
								class2ProbChild2 = (float) class2DistCountsChild2 / numExamplesChild2;
								if (class1ProbChild2 > class2ProbChild2) {
									purity2 = class1ProbChild2;
								} else {
									purity2 = class2ProbChild2;
								}

								float fraction1 = (float) numExamplesChild1 / nodeNumExamples;
								float fraction2 = (float) numExamplesChild2 / nodeNumExamples;

								float weightedPerformance = fraction1 * purity1 + fraction2 * purity2;
								float deltaPerformance = weightedPerformance - nodePurities[nodeIndex];

								if (deltaPerformance > bestDeltaPerformance) {

									bestDeltaPerformance = deltaPerformance;

									bestPurity1 = purity1;
									bestPurity2 = purity2;

									bestSplitFeatureIndex = (short) decompositionSplitFeatureIndex;
									bestSplitFeatureValue = decompositionSplitFeatureValueInteger;

									bestNumExamplesChild1 = numExamplesChild1;
									bestNumExamplesChild2 = numExamplesChild2;

									bestClass1ProbChild1 = class1ProbChild1;
									bestClass2ProbChild1 = class2ProbChild1;

									bestClass1ProbChild2 = class1ProbChild2;
									bestClass2ProbChild2 = class2ProbChild2;
								}
							}

						}

						if (/*
							 * bestDeltaPerformance > minDeltaPerformanceForSplit &&
							 */bestNumExamplesChild1 > minNumExamplesPerNode && bestNumExamplesChild2 > minNumExamplesPerNode) {

							nodeSplitFeatureIndices[nodeIndex] = bestSplitFeatureIndex;
							nodeSplitFeatureValues[nodeIndex] = bestSplitFeatureValue;

							// partitionExamples

							int childNode1FillPtr = exampleStartIndex;
							int childNode2FillPtr = exampleEndIndex - 1;

							while (childNode1FillPtr <= childNode2FillPtr) {

								if (unsignedByteToInt(trainInputFeatureValues[childNode1FillPtr][bestSplitFeatureIndex]) < bestSplitFeatureValue) {
									childNode1FillPtr++;
								} else {
									// swap for unknown example

									byte[] workInputFeatureValues = trainInputFeatureValues[childNode2FillPtr];
									int workOutputFeatureValue = trainOutputFeatureValues[childNode2FillPtr];
									int workExampleGroup = trainGroupIndices[childNode2FillPtr];

									trainInputFeatureValues[childNode2FillPtr] = trainInputFeatureValues[childNode1FillPtr];
									trainOutputFeatureValues[childNode2FillPtr] = trainOutputFeatureValues[childNode1FillPtr];
									trainGroupIndices[childNode2FillPtr] = trainGroupIndices[childNode1FillPtr];

									trainInputFeatureValues[childNode1FillPtr] = workInputFeatureValues;
									trainOutputFeatureValues[childNode1FillPtr] = workOutputFeatureValue;
									trainGroupIndices[childNode1FillPtr] = workExampleGroup;

									childNode2FillPtr--;
								}

							}

							int newNodeIndex1 = newNodeIndex++;
							int newNodeIndex2 = newNodeIndex++;

							nodeChild1Indices[nodeIndex] = newNodeIndex1;
							nodeChild2Indices[nodeIndex] = newNodeIndex2;

							nodeExamplesStartIndices[newNodeIndex1] = exampleStartIndex;
							nodeExamplesEndIndices[newNodeIndex1] = exampleStartIndex + bestNumExamplesChild1;

							nodeExamplesStartIndices[newNodeIndex2] = nodeExamplesEndIndices[newNodeIndex1];
							nodeExamplesEndIndices[newNodeIndex2] = exampleEndIndex;

							nodePurities[newNodeIndex1] = bestPurity1;
							nodePurities[newNodeIndex2] = bestPurity2;

							nodeClass1Probs[newNodeIndex1] = bestClass1ProbChild1;
							// nodeClass2Probs[newNodeIndex1] =
							// bestClass2ProbChild1;

							nodeClass1Probs[newNodeIndex2] = bestClass1ProbChild2;
							// nodeClass2Probs[newNodeIndex2] =
							// bestClass2ProbChild2;

						} else {

							nodeChild1Indices[nodeIndex] = -1;
							nodeChild2Indices[nodeIndex] = -1;
						}
					} else {
						nodeChild1Indices[nodeIndex] = -1;
						nodeChild2Indices[nodeIndex] = -1;
					}

					nodeIndex++;

				}

				int numNodes = nodeIndex;
				System.out.printf("numNodes = %d\n", numNodes);

				forestTreeNumNodes[treeIndex] = numNodes;

				/*********************************/
				/* UPDATE GROUP PROBABILITY SUMS */
				/*********************************/

				for (int testExampleIndex = 0; testExampleIndex < numTestExamples; testExampleIndex++) {

					nodeIndex = 0;

					int groupID = testGroupIDs[testExampleIndex];

					while (true) {

						if (nodeChild1Indices[nodeIndex] == -1) {

							groupClassProbabilitySums[groupID][0] += nodeClass1Probs[nodeIndex];
							groupClassProbabilitySums[groupID][1] += (1.0f - nodeClass1Probs[nodeIndex]);
							// groupClassProbabilitySums[groupID][1] +=
							// nodeClass2Probs[nodeIndex];

							break;
						}
						//
						// decompositionSplitFeatureIndex =
						// nodeSplitFeatureIndices[nodeIndex];
						// decompositionSplitFeatureValueInteger =
						// nodeSplitFeatureValues[nodeIndex];

						// int exampleChildNodeIndex = testDecomposition();
						// System.out.printf("exampleChildNodeIndex = %d\n",
						// exampleChildNodeIndex);

						if (unsignedByteToInt(testInputFeatureValues[testExampleIndex][nodeSplitFeatureIndices[nodeIndex]]) < nodeSplitFeatureValues[nodeIndex]) {
							nodeIndex = nodeChild1Indices[nodeIndex];
						} else {
							nodeIndex = nodeChild2Indices[nodeIndex];
						}

					}

				}

				// trim unused memory from data structures

				{
					int[] temp_nodeChild1Indices = new int[numNodes];
					System.arraycopy(nodeChild1Indices, 0, temp_nodeChild1Indices, 0, numNodes);
					forestTreeNodeChild1Indices[treeIndex] = temp_nodeChild1Indices;
					temp_nodeChild1Indices = null;

					int[] temp_nodeChild2Indices = new int[numNodes];
					System.arraycopy(nodeChild2Indices, 0, temp_nodeChild2Indices, 0, numNodes);
					forestTreeNodeChild2Indices[treeIndex] = temp_nodeChild2Indices;
					temp_nodeChild2Indices = null;

					int[] temp_nodeExamplesStartIndices = new int[numNodes];
					System.arraycopy(nodeExamplesStartIndices, 0, temp_nodeExamplesStartIndices, 0, numNodes);
					forestTreeNodeExamplesStartIndices[treeIndex] = temp_nodeExamplesStartIndices;
					temp_nodeExamplesStartIndices = null;

					int[] temp_nodeExamplesEndIndices = new int[numNodes];
					System.arraycopy(nodeExamplesEndIndices, 0, temp_nodeExamplesEndIndices, 0, numNodes);
					forestTreeNodeExamplesEndIndices[treeIndex] = temp_nodeExamplesEndIndices;
					temp_nodeExamplesEndIndices = null;

					float[] temp_nodePurities = new float[numNodes];
					System.arraycopy(nodePurities, 0, temp_nodePurities, 0, numNodes);
					forestTreeNodePurities[treeIndex] = temp_nodePurities;
					temp_nodePurities = null;

					float[] temp_nodeClass1Probs = new float[numNodes];
					System.arraycopy(nodeClass1Probs, 0, temp_nodeClass1Probs, 0, numNodes);
					forestTreeNodeClass1Probs[treeIndex] = temp_nodeClass1Probs;
					temp_nodeClass1Probs = null;

					short[] temp_nodeSplitFeatureIndices = new short[numNodes];
					System.arraycopy(nodeSplitFeatureIndices, 0, temp_nodeSplitFeatureIndices, 0, numNodes);
					forestTreeNodeSplitFeatureIndices[treeIndex] = temp_nodeSplitFeatureIndices;
					temp_nodeSplitFeatureIndices = null;

					short[] temp_nodeSplitFeatureValues = new short[numNodes];
					System.arraycopy(nodeSplitFeatureValues, 0, temp_nodeSplitFeatureValues, 0, numNodes);
					forestTreeNodeSplitFeatureValues[treeIndex] = temp_nodeSplitFeatureValues;
					temp_nodeSplitFeatureValues = null;

				}

			}

			if (createFinalModel) {

				Object[] treeParts = new Object[] { numTreesInForest, forestTreeNumNodes, maxNumNodes, forestTreeNodeChild1Indices, forestTreeNodeChild2Indices, null, null, null, forestTreeNodeSplitFeatureIndices, forestTreeNodeSplitFeatureValues,
						forestTreeNodeClass1Probs };

				IO.writeObject(defaultDataPath + File.separatorChar + "forest.size" + numTreesInForest + ".ser", treeParts);
			}

			double[] classPDF = new double[numClasses];

			for (int testGroupIndex = 0; testGroupIndex < numTestGroups; testGroupIndex++) {

				int testGroupID = testGroupIDs[testGroupIndex * numExamplesPerGroup];

				int pollenCount = masterGroupPollenCounts[testGroupID];

				for (int i = 0; i < numClasses; i++) {
					// classPDF[i] = groupClassProbabilitySums[testGroupID][i] /
					// groupClassProbabilityWeights[testGroupID] /
					// numTreesInForrest;
					classPDF[i] = groupClassProbabilitySums[testGroupID][i] / numExamplesPerGroup / numTreesInForest;
					// System.out.printf("%d\t%7.4f\n", i, classPDF[i]);
				}

				masterPollenCounts[observationIndex] = masterGroupPollenCounts[testGroupID];
				machinePollenProbabilitySums[observationIndex] = 1.0 - classPDF[0]; // index
																					// 0
																					// is
																					// not
																					// pollen
				observationIndex++;

				pollenCountNumObservations[pollenCount]++;
				pollenCountProbabilitySums[pollenCount] += 1.0 - classPDF[0]; // index
																				// 0
																				// is
																				// not
																				// pollen
			}

			if (false) {
				System.out.printf("running pollen count vs. probability\n");
				for (int i = 0; i < maxPollenCountPerTrial; i++) {
					double averageProbabiltiy = pollenCountProbabilitySums[i] / pollenCountNumObservations[i];
					System.out.printf("%d\t%7.4f\t%d\n", i, averageProbabiltiy, pollenCountNumObservations[i]);
				}

				double averagePerformance = performanceSum / numTestExamples;
				System.out.printf("averagePerformance = %f\n", averagePerformance);
				averagePerformanceSum += averagePerformance;

				//

				int numObservations = observationIndex;
				double correlation = Correlation.correlation(masterPollenCounts, machinePollenProbabilitySums, numObservations);
				System.out.printf("correlation = %f\n", correlation);
				System.out.printf("numObservations = %d\n", numObservations);
			}

			//

			long evaluationEndTime = System.currentTimeMillis();

			double duration = (evaluationEndTime - evaluationStartTime) / 1000.0;

			double classficationsPerSecond = numTestExamples / duration;

			System.out.printf("duration = %f\n", duration);
			System.out.printf("classficationsPerSecond = %e\n", classficationsPerSecond);
		}

		if (!createFinalModel) {
			double overallAverageLogLikelihood = averagePerformanceSum / numValidationTrials;
			System.out.printf("overallAverageLogLikelihood = %f\n", overallAverageLogLikelihood);
		}

		long endTime = System.currentTimeMillis();
		double duration = (endTime - startTime) / 1000.0;
		System.out.printf("duration = %f\n", duration);
	}

	public void evaluateForrestWrapper(byte[] testInputFeatures, double probabilities[]) {

	}

	public void evaluateForrest(byte[] testInputFeatures, double probabilities[]) {

		double class1Probability = 0;
		double class2Probability = 0;

		for (int treeIndex = 0; treeIndex < numTreesInForest; treeIndex++) {

			nodeChild1Indices = forestTreeNodeChild1Indices[treeIndex];
			nodeChild2Indices = forestTreeNodeChild2Indices[treeIndex];

			nodeClass1Probs = forestTreeNodeClass1Probs[treeIndex];
			// nodeClass2Probs = forestTreeNodeClass2Probs[treeIndex];

			nodeSplitFeatureIndices = forestTreeNodeSplitFeatureIndices[treeIndex];
			nodeSplitFeatureValues = forestTreeNodeSplitFeatureValues[treeIndex];

			decompositionInputFeatureValues = testInputFeatures;

			int nodeIndex = 0;

			int strangeErrorType1Count = 0;
			int strangeErrorType2Count = 0;
			while (true) {

				if (nodeIndex < 0) { // !!! I should not have to test for
										// (nodeIndex < 0) !!!

					System.out.printf("strangeErrorType1Count = %d\n", strangeErrorType1Count++);

					class1Probability += 0.5f;
					class2Probability += 0.5f;
					// class2Probability += nodeClass2Probs[nodeIndex];

					break;
				}

				if (nodeChild1Indices[nodeIndex] == -1) { // !!! I should not
															// have to test for
															// (nodeIndex > 0)
															// !!!

					class1Probability += nodeClass1Probs[nodeIndex];
					class2Probability += (1.0f - nodeClass1Probs[nodeIndex]);
					// class2Probability += nodeClass2Probs[nodeIndex];

					break;
				}

				decompositionSplitFeatureIndex = nodeSplitFeatureIndices[nodeIndex];
				decompositionSplitFeatureValueInteger = nodeSplitFeatureValues[nodeIndex];

				if (decompositionSplitFeatureIndex < 0) { // !!! I should not
															// have to test for
															// (decompositionSplitFeatureIndex
															// < 0) !!!

					System.out.printf("strangeErrorType2Count = %d\n", strangeErrorType2Count++);

					class1Probability += 0.5f;
					class2Probability += 0.5f;
					// class2Probability += nodeClass2Probs[nodeIndex];

					break;
				}

				// int exampleChildNodeIndex = testDecomposition();

				if (unsignedByteToInt(decompositionInputFeatureValues[decompositionSplitFeatureIndex]) < decompositionSplitFeatureValueInteger) {
					nodeIndex = nodeChild1Indices[nodeIndex];
				} else {
					nodeIndex = nodeChild2Indices[nodeIndex];
				}

			}

		}

		probabilities[0] = class1Probability / numTreesInForest;
		probabilities[1] = class2Probability / numTreesInForest;

	}

	/**
	 * @param args
	 */

	static int numThreads = -1;
	static int threadIndex = -1;

	public static void main(String[] args) {

		String modeString = args[0];
		String stateFileDirectory = args[1];
		String masterStateFileName = args[2];
		String testerStateFileName = args[3];
		String samplePathDirectory = args[4];
		String resetRoundAndSlideString = args[5];
		String newStartRoundString = args[6];
		String newStartSlideString = args[7];
		numThreads = Integer.parseInt(args[8]);
		threadIndex = Integer.parseInt(args[9]);

		System.out.printf("modeString = %s\n", modeString);
		System.out.printf("stateFileDirectory = %s\n", stateFileDirectory);
		System.out.printf("masterStateFileName = %s\n", masterStateFileName);
		System.out.printf("testerStateFileName = %s\n", testerStateFileName);
		System.out.printf("samplePathDirectory = %s\n", samplePathDirectory);
		System.out.printf("resetRoundAndSlideString = %s\n", resetRoundAndSlideString);
		System.out.printf("numThreads = %d\n", numThreads);
		System.out.printf("threadIndex = %d\n", threadIndex);

		boolean analyzeMode = false;
		boolean createExamplesMode = false;
		boolean computeTree = false;
		boolean scoreMode = false;
		boolean masterMode = false;
		boolean testerMode = false;

		if (modeString.equalsIgnoreCase("analyze")) {
			analyzeMode = true;
		} else if (modeString.equalsIgnoreCase("createExamples")) {
			createExamplesMode = true;
		} else if (modeString.equalsIgnoreCase("computeTree")) {
			computeTree = true;
		} else if (modeString.equalsIgnoreCase("score")) {
			scoreMode = true;
		} else if (modeString.equalsIgnoreCase("master")) {
			masterMode = true;
		} else if (modeString.equalsIgnoreCase("tester")) {
			testerMode = true;
		} else {
			System.out.printf("invalid mode!");
			System.exit(1);
		}

		String masterStateFilePath = stateFileDirectory + File.separatorChar + masterStateFileName;
		String testerStateFilePath = stateFileDirectory + File.separatorChar + testerStateFileName;

		boolean resetRoundAndSlide = Boolean.parseBoolean(resetRoundAndSlideString);
		int newStartRound = -1;
		int newStartSlide = -1;

		if (resetRoundAndSlide) {
			newStartRound = Integer.parseInt(newStartRoundString);
			newStartSlide = Integer.parseInt(newStartSlideString);
		}

		if (analyzeMode) {
			RawNanoReader ngr = (RawNanoReader) IO.readObject(masterStateFilePath);
			ngr.setParameters(resetRoundAndSlide, newStartRound, newStartSlide, samplePathDirectory);
			ngr.analyzeTags();
		} else //
		if (createExamplesMode) {
			RawNanoReader masterRNR = (RawNanoReader) IO.readObject(masterStateFilePath);
			masterRNR.setParameters(resetRoundAndSlide, newStartRound, newStartSlide, samplePathDirectory);
			masterRNR.createPollenNoPollenExamples();
		} else //
		if (computeTree) {
			RawNanoReader masterRNR = (RawNanoReader) IO.readObject(masterStateFilePath);
			masterRNR.setParameters(resetRoundAndSlide, newStartRound, newStartSlide, samplePathDirectory);
			masterRNR.computeTree(args);
		} else //
		if (scoreMode) {
			RawNanoReader masterRNR = (RawNanoReader) IO.readObject(masterStateFilePath);
			RawNanoReader testerRNR = (RawNanoReader) IO.readObject(testerStateFilePath);
			masterRNR.setParameters(resetRoundAndSlide, newStartRound, newStartSlide, samplePathDirectory);
			testerRNR.setParameters(resetRoundAndSlide, newStartRound, newStartSlide, samplePathDirectory);
			testerRNR.scoreTags(masterRNR);
		} else //
		if (masterMode) {

			GraphicsEnvironment env = null;
			GraphicsDevice device = null;

			try {
				env = GraphicsEnvironment.getLocalGraphicsEnvironment();
				device = env.getDefaultScreenDevice();
			} catch (HeadlessException e1) {
				// TODO Auto-generated catch block
				// e1.printStackTrace();
			}

			RawNanoReader masterRNR;
			boolean loadMasterState;
			if (masterStateFileName.equalsIgnoreCase("null")) {
				loadMasterState = false;
			} else {
				loadMasterState = true;
			}
			if (loadMasterState) {
				masterRNR = (RawNanoReader) IO.readObject(masterStateFilePath);
			} else {
				masterRNR = new RawNanoReader();
			}

			// increase size of arrays and copy old over

			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			// temp code....
			//
			//
			//
			//
			//
			//

			if (false) {

				masterRNR.trialIndex = masterRNR.maxNumSlides * 100;

				masterRNR.firstTrialAvailableOnDisk = masterRNR.trialIndex;

				masterRNR.maxNumRounds = 1000;

				masterRNR.numRoundsOfWindowsAvailableOnDisk = masterRNR.maxNumRounds;

				masterRNR.viewDataPaths = new String[masterRNR.maxNumRounds][masterRNR.maxNumSlides];

				masterRNR.maxNumTrials = masterRNR.maxNumSlides * masterRNR.maxNumRounds;

				masterRNR.trialViewPaths = new String[masterRNR.maxNumTrials];
				masterRNR.trialViewRounds = new int[masterRNR.maxNumTrials];
				masterRNR.trialViewSlides = new int[masterRNR.maxNumTrials];
				// int[] trialViewLastZ = new int[maxNumTrials];

				masterRNR.answerShownForTrial = new boolean[masterRNR.maxNumTrials];

				{
					int[] trialNumTags = masterRNR.trialNumTags;
					int[][] trialClassificationX = masterRNR.trialClassificationX;
					int[][] trialClassificationY = masterRNR.trialClassificationY;
					int[][] trialClassificationZ = masterRNR.trialClassificationZ;
					int[][] trialClassificationW = masterRNR.trialClassificationW;
					String[][] trialClassificationText = masterRNR.trialClassificationText;

					masterRNR.trialNumTags = new int[masterRNR.maxNumTrials];
					masterRNR.trialClassificationX = new int[masterRNR.maxNumTrials][masterRNR.maxClassificationsPerView];
					masterRNR.trialClassificationY = new int[masterRNR.maxNumTrials][masterRNR.maxClassificationsPerView];
					masterRNR.trialClassificationZ = new int[masterRNR.maxNumTrials][masterRNR.maxClassificationsPerView];
					masterRNR.trialClassificationW = new int[masterRNR.maxNumTrials][masterRNR.maxClassificationsPerView];
					masterRNR.trialClassificationText = new String[masterRNR.maxNumTrials][masterRNR.maxClassificationsPerView];

					System.arraycopy(trialNumTags, 0, masterRNR.trialNumTags, 0, trialNumTags.length);
					System.arraycopy(trialClassificationX, 0, masterRNR.trialClassificationX, 0, trialClassificationX.length);
					System.arraycopy(trialClassificationY, 0, masterRNR.trialClassificationY, 0, trialClassificationY.length);
					System.arraycopy(trialClassificationZ, 0, masterRNR.trialClassificationZ, 0, trialClassificationZ.length);
					System.arraycopy(trialClassificationW, 0, masterRNR.trialClassificationW, 0, trialClassificationW.length);

					System.arraycopy(trialClassificationText, 0, masterRNR.trialClassificationText, 0, trialClassificationText.length);

				}

				// masterRNR.saveStateToFileWithTimestamp();

				// System.exit(0);
			}

			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//

			masterRNR.setParameters(resetRoundAndSlide, newStartRound, newStartSlide, samplePathDirectory);
			masterRNR.setDevice(device);

			try {
				masterRNR.runExperiment(stateFileDirectory, imageFilePathDirectory, true, false, null);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else //
		if (testerMode) {

			GraphicsEnvironment env = null;
			GraphicsDevice device = null;

			try {
				env = GraphicsEnvironment.getLocalGraphicsEnvironment();
				device = env.getDefaultScreenDevice();
			} catch (HeadlessException e1) {
				// TODO Auto-generated catch block
				// e1.printStackTrace();
			}

			RawNanoReader masterRNR;
			masterRNR = (RawNanoReader) IO.readObject(masterStateFilePath);
			// masterRNR.setParameters();

			RawNanoReader testerRNR;
			boolean loadTesterState;
			if (testerStateFileName.equalsIgnoreCase("null")) {
				loadTesterState = false;
			} else {
				loadTesterState = true;
			}
			if (loadTesterState) {
				testerRNR = (RawNanoReader) IO.readObject(testerStateFilePath);
			} else {
				testerRNR = new RawNanoReader();
			}
			testerRNR.setParameters(resetRoundAndSlide, newStartRound, newStartSlide, samplePathDirectory);

			testerRNR.setDevice(device);

			try {
				testerRNR.runExperiment(stateFileDirectory, imageFilePathDirectory, false, true, masterRNR);

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}

class BackgroundSaveState extends Thread {
	RawNanoReader rnr;
	public boolean enable;

	BackgroundSaveState(RawNanoReader rnr) {
		this.rnr = rnr;
		this.enable = true;
	}

	public void run() {

		long lastSaveTime = System.currentTimeMillis();

		while (enable) {

			long currentTime = System.currentTimeMillis();
			long elapsedTime = (currentTime - lastSaveTime);

			// save state every 10 minutes
			if (elapsedTime > 1000 * 10 * 60) {
				rnr.saveLastStateToFile();
				lastSaveTime = currentTime;
			}
			try {
				sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

}

class BackgroundBufferLoader extends Thread {
	RawNanoReader rnr;
	public boolean enable;

	BackgroundBufferLoader(RawNanoReader rnr) {
		this.rnr = rnr;
		this.enable = true;
	}

	public void run() {

		rnr.createTrialViewPaths(true);

		int count = 0;

		while (enable) {

			count++;

			if (false)
				System.out.println("background loader iteration #" + count);

			int currentTrialIndex = rnr.trialIndex;

			// free unused memory

			boolean memoryFreed = false;
			for (int i = 0; i < rnr.maxNumTrials; i++) {

				if (!enable) {
					break;
				}

				int delta = i - currentTrialIndex;

				if ((delta < -rnr.numBuffersLookBehind) || (delta > rnr.numBuffersLookAhead)) {
					if (rnr.trialBuffers[i] != null) {
						System.out.println("background loader: freing " + rnr.trialViewPaths[i]);
						rnr.trialBuffers[i] = null;
						memoryFreed = true;
					}
				}

			}
			if (memoryFreed) {
				continue;
			}

			boolean bufferLoaded = false;

			// load current buffer
			if (rnr.trialBuffers[currentTrialIndex] == null) {
				System.out.println("background loader: loading current " + rnr.trialViewPaths[currentTrialIndex]);
				rnr.trialBuffers[currentTrialIndex] = (byte[][]) IO.readObject(rnr.trialViewPaths[currentTrialIndex]);
				bufferLoaded = true;
			}
			if (bufferLoaded) {
				continue;
			}

			// load forward buffers
			for (int i = 0; i < rnr.numBuffersLookAhead; i++) {

				if (!enable) {
					break;
				}

				int trialIndexToLoad = currentTrialIndex + i + 1;
				if (trialIndexToLoad == rnr.maxNumTrials) {
					break;
				}

				if (rnr.trialBuffers[trialIndexToLoad] == null) {
					System.out.println("background loader: loading forward " + rnr.trialViewPaths[trialIndexToLoad]);
					rnr.trialBuffers[trialIndexToLoad] = (byte[][]) IO.readObject(rnr.trialViewPaths[trialIndexToLoad]);
					bufferLoaded = true;
					break;
				}

			}
			if (bufferLoaded) {
				continue;
			}

			// load backwards buffers

			int actaualnumBuffersLookBehind = rnr.numBuffersLookBehind;

			int numAvailableBackward = currentTrialIndex - rnr.firstTrialAvailableOnDisk;

			if (numAvailableBackward < rnr.numBuffersLookBehind) {
				actaualnumBuffersLookBehind = numAvailableBackward;
			}

			for (int i = 0; i < actaualnumBuffersLookBehind; i++) {

				if (!enable) {
					break;
				}

				int trialIndexToLoad = currentTrialIndex - i - 1;
				if (trialIndexToLoad < 0) {
					break;
				}

				if (rnr.trialBuffers[trialIndexToLoad] == null) {
					System.out.println("background loader: loading backwards " + rnr.trialViewPaths[trialIndexToLoad]);
					rnr.trialBuffers[trialIndexToLoad] = (byte[][]) IO.readObject(rnr.trialViewPaths[trialIndexToLoad]);
					bufferLoaded = true;
					break;
				}

			}
			if (bufferLoaded) {
				continue;
			}

			try {
				sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

}

class MyMouseListener implements MouseListener {

	private RawNanoReader rnr;

	MyMouseListener(RawNanoReader rawNanoReader) {
		this.rnr = rawNanoReader;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		// System.out.println("MouseEvent = " + e);

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

		// System.out.println("MouseEvent = " + e);
		// System.out.println("getButton = " + e.getButton());

		if (e.getButton() == MouseEvent.BUTTON1) {
			rnr.mousePressX = e.getX();
			rnr.mousePressY = e.getY();
			rnr.mouseDraggedX = rnr.mousePressX;
			rnr.mouseDraggedY = rnr.mousePressY;
			rnr.markSpot = true;
			rnr.change = true;
			// rawNanoReader.viewer.paint(rawNanoReader.viewer.getGraphics());
		}
		// if (e.getButton() == MouseEvent.BUTTON2) {
		// rawNanoReader.loadCacheNow = true;
		// }
		if (e.getButton() == MouseEvent.BUTTON3) {
			rnr.changePositionForward = true;

			rnr.change = true;
		}

	}

	@Override
	public void mouseReleased(MouseEvent e) {

		if (e.getButton() == MouseEvent.BUTTON1) {
			rnr.mouseDraggedX = e.getX();
			rnr.mouseDraggedY = e.getY();
			rnr.createExamplePhase1 = true;
			rnr.label = "";
			rnr.change = true;
		}

		// System.out.println("mouseReleased = " + e);
		// if (e.getButton() == MouseEvent.BUTTON1) {
		// rawNanoReader.mouseReleasedX = e.getX();
		// rawNanoReader.mouseReleasedY = e.getY();
		// rawNanoReader.markSpot = true;
		// // rawNanoReader.viewer.paint(rawNanoReader.viewer.getGraphics());
		// }
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}
}

class MyMouseWheelListener implements MouseWheelListener {

	private RawNanoReader rawNanoReader;

	MyMouseWheelListener(RawNanoReader rawNanoReader) {
		this.rawNanoReader = rawNanoReader;
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		// System.out.println("MouseWheelEvent = " + e);

		rawNanoReader.updateWheelMotion = true;
		rawNanoReader.wheelRotation += e.getWheelRotation();

		rawNanoReader.change = true;

	}

}

class MyMouseMotionListener implements MouseMotionListener {

	private RawNanoReader rawNanoReader;

	MyMouseMotionListener(RawNanoReader rawNanoReader) {
		this.rawNanoReader = rawNanoReader;
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// System.out.println("MouseWheelEvent = " + e);
		rawNanoReader.mouseDraggedX = e.getX();
		rawNanoReader.mouseDraggedY = e.getY();
		rawNanoReader.change = true;
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub

	}

}

class MyKeyListener implements KeyListener {

	private RawNanoReader rnr;

	MyKeyListener(RawNanoReader rawNanoReader) {
		this.rnr = rawNanoReader;
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

		int character = e.getKeyChar();
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub

		if (true) {
			System.out.println("KeyEvent = " + e);
			System.out.println("char int = " + (int) e.getKeyChar());
			System.out.println("code int = " + (int) e.getKeyCode());
		}

		int code = e.getKeyCode();

		if (code == 8) {

			if (rnr.label.length() == 0)
				return;

			rnr.label = rnr.label.substring(0, rnr.label.length() - 1);

			if (rnr.trialNumTags[rnr.trialIndex] > 0) {
				rnr.trialClassificationText[rnr.trialIndex][rnr.trialNumTags[rnr.trialIndex] - 1] = rnr.label;
			}

			rnr.change = true;

		} else if (code == 10) {
			// enter

			return;

		} else if (code == 27) {
			// escape

			rnr.exitNow = true;

		} else if (code == 40) {

			rnr.updateWheelMotion = true;
			rnr.wheelRotation++;
			rnr.change = true;

		} else if (code == 38) {

			rnr.updateWheelMotion = true;
			rnr.wheelRotation--;
			rnr.change = true;

		} else if (code == 39) {
			rnr.changePositionForward = true;
			rnr.change = true;

		} else if (code == 37) {
			rnr.changePositionBackward = true;
			rnr.change = true;
		} else if (code == 127) {

			rnr.deleteLastTag = true;
			rnr.change = true;

		} else {

			if (!rnr.answerShownForTrial[rnr.trialIndex] && !rnr.markSpot) {
				rnr.label += e.getKeyChar();
				System.out.println("rawNanoReader.label = " + rnr.label);

				if (rnr.trialNumTags[rnr.trialIndex] > 0) {
					rnr.trialClassificationText[rnr.trialIndex][rnr.trialNumTags[rnr.trialIndex] - 1] = rnr.label;
				}
				rnr.change = true;
			}
		}

	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

}

class CountAndLabel implements Comparable<Object> {

	int count;
	String label;

	public CountAndLabel(int count, String label) {
		this.count = count;
		this.label = label;
	}

	public int compareTo(Object o) {

		if (this.count == ((CountAndLabel) o).count)
			return 0;

		if (this.count < ((CountAndLabel) o).count)
			return 1;
		else
			return -1;

	}

}

class ComputeProbabilityFieldThread extends Thread {
	RawNanoReader rnr;
	int numThreads;
	int trheadIndex;

	ComputeProbabilityFieldThread(RawNanoReader rnr, int numThreads, int trheadIndex) {
		this.rnr = rnr;
		this.rnr = rnr;
		this.rnr = rnr;
	}

	public void run() {

		long lastSaveTime = System.currentTimeMillis();

		long currentTime = System.currentTimeMillis();
		long elapsedTime = (currentTime - lastSaveTime);

		// save state every 10 minutes
		if (elapsedTime > 1000 * 10 * 60) {
			rnr.saveStateToFileWithTimestamp();
			lastSaveTime = currentTime;
		}
		try {
			sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}

/**
 * @author dtcheng
 * 
 * 
 *         random splitting
 * 
 *         any sample size 1
 * 
 * 
 *         numNodes = 528433 duration = 75.714000 classficationsPerSecond = 0.000000e+00 duration = 79.492000
 * 
 *         numNodes = 532685 duration = 80.467000 classficationsPerSecond = 0.000000e+00 duration = 84.227000
 * 
 *         numNodes = 529253 duration = 81.238000 classficationsPerSecond = 0.000000e+00 duration = 85.032000
 * 
 *         numNodes = 533165 duration = 81.507000 classficationsPerSecond = 0.000000e+00 duration = 85.296000
 * 
 * 
 *         numNodes = 529095 duration = 82.940000 classficationsPerSecond = 0.000000e+00 duration = 86.784000
 * 
 *         median sample size 1
 * 
 *         numNodes = 523213 duration = 83.289000 classficationsPerSecond = 0.000000e+00 duration = 87.104000
 * 
 * 
 * 
 *         mean splitting
 * 
 * 
 *         duration = 105.235000 classficationsPerSecond = 0.000000e+00 duration = 109.002000
 * 
 *         duration = 103.275000 classficationsPerSecond = 0.000000e+00 duration = 107.382000
 * 
 *         duration = 103.064000 classficationsPerSecond = 0.000000e+00 duration = 106.858000
 * 
 * 
 *         duration = 104.016000 classficationsPerSecond = 0.000000e+00 duration = 107.907000
 * 
 *         duration = 104.252000 classficationsPerSecond = 0.000000e+00 duration = 108.178000
 * 
 * 
 * 
 *         median splitting
 * 
 * 
 *         numNodes = 735519 duration = 164.298000 classficationsPerSecond = 0.000000e+00 duration = 168.069000
 * 
 *         numNodes = 734775 duration = 167.520000 classficationsPerSecond = 0.000000e+00 duration = 171.291000
 * 
 *         numNodes = 734903 duration = 165.206000 classficationsPerSecond = 0.000000e+00 duration = 168.973000
 * 
 *         numNodes = 733175 duration = 166.555000 classficationsPerSecond = 0.000000e+00 duration = 170.351000
 * 
 * 
 *         numNodes = 735325 duration = 166.364000 classficationsPerSecond = 0.000000e+00 duration = 170.140000
 * 
 *         numNodes = 734971 duration = 164.298000 classficationsPerSecond = 0.000000e+00 duration = 168.088000
 * 
 *         numNodes = 734655 duration = 166.712000 classficationsPerSecond = 0.000000e+00 duration = 170.490000
 * 
 * 
 *         numNodes = 735699 duration = 166.798000 classficationsPerSecond = 0.000000e+00 duration = 170.604000
 * 
 *         numNodes = 736157 duration = 166.681000 classficationsPerSecond = 0.000000e+00 duration = 170.495000
 * 
 *         numNodes = 733869 duration = 166.562000 classficationsPerSecond = 0.000000e+00 duration = 170.369000
 * 
 * 
 *         numNodes = 751171 duration = 77.769000 classficationsPerSecond = 0.000000e+00 duration = 81.525000 (10x splits)
 * 
 *         numNodes = 1667119 (old no purity check)
 * 
 * 
 *         Example Class Distribution: 0 not pollen 2924450 1 pollen 169550 numNodes = 1666331
 * 
 * 
 *         duration = 190.219000 classficationsPerSecond = 0.000000e+00 duration = 194.196000
 * 
 *         duration = 194.071000 classficationsPerSecond = 0.000000e+00 duration = 197.827000
 * 
 *         duration = 153.434000 classficationsPerSecond = 0.000000e+00 duration = 157.298000 ???
 * 
 *         duration = 196.550000 classficationsPerSecond = 0.000000e+00 duration = 200.431000
 * 
 *         duration = 198.298000 classficationsPerSecond = 0.000000e+00 duration = 202.089000
 * 
 * 
 *         readDuration = 1.70 // transposed
 * 
 *         readDuration = 3.52 // original
 * 
 * 
 * 
 * 
 * 
 * 
 *         readDuration = 11.561000 writeDuration = 20.634000
 * 
 * 
 * 
 *         readDuration = 18.947000 writeDuration = 29.626000
 * 
 *         readDuration = 67.239000
 * 
 *         readDuration = 65.707000
 * 
 * 
 * 
 *         Using original matrix form [e][f];
 * 
 *         numObservations = 0 duration = 96.263000 classficationsPerSecond = 0.000000e+00 duration = 100.091000
 * 
 * 
 *         averageLogLikelihood = NaN correlation = NaN numObservations = 0 duration = 13.192000 classficationsPerSecond = 0.000000e+00 duration = 17.059000
 * 
 * 
 *         duration = 10.907000 classficationsPerSecond = 0.000000e+00 duration = 14.705000
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 *         surangi surangi_master_state_3_1330651136782.ser
 * 
 *         errorSum = 128.000000 exactMatchErrorSum = 66 masterTesterCorrelation = 0.943316 errorRate = 0.561404 exactMatchErrorRate = 0.289474 exactMatchAccuracy = 0.710526
 * 
 *         luke luke_master_state_3_1331217592760.ser
 * 
 *         masterTesterCorrelation = 0.953707
 * 
 *         david david_master_state_3_1331223958203.ser
 * 
 *         masterTesterCorrelation = 0.930669
 * 
 * 
 * 
 * 
 * 
 * 
 *         eta = Fri Mar 09 00:44:28 CST 2012 done analyzing z plane index 20 duration = 16.305000
 * 
 * 
 *         duration = 15.618000 numPixelsPerSecond = 255379.433986
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */
// double angle = Double.NaN;
//
// if (yDiff == 0) {
// angle = 0;
// }
// if (xDiff > 0 && yDiff > 0) {
// // 1st Quadrant
// angle = Math.toDegrees(Math.atan(xDiff / yDiff));
// }
// if (xDiff < 0 && yDiff > 0) {
// // 2nd Quadrant
// angle = Math.toDegrees(Math.atan(xDiff / yDiff)) + 180;
// }
// if (xDiff < 0 && yDiff < 0) {
// // 3rd Quadrant
// angle = Math.toDegrees(Math.atan(xDiff / yDiff)) + 180;
// }
// if (xDiff > 0 && yDiff < 0) {
// // 4th Quadrant
// angle = Math.toDegrees(Math.atan(xDiff / yDiff)) + 360;
// }
