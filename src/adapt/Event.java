package adapt;

public class Event {

	double[] times;
	double[][] positions;
	String[] fileNames;
	double[] fileTimes;
	double[] frequencies;
	int[] tagIDs;

	public Event(double[] times, double[][] positions, String[] fileNames, double[] fileTimes, double[] frequencies, int[] tagIDs) {
		this.times = times;
		this.positions = positions;
		this.fileNames = fileNames;
		this.fileTimes = fileTimes;
		this.frequencies = frequencies;
		this.tagIDs = tagIDs;
	}

}
