package adapt;

public class ARLO_Main {

	public static void main(String[] args) {

		ARLO arlo = new ARLO();

		// ///////////////////
		// DEFINE SENSORS //
		// ///////////////////

		int numSensors = 4;

		Sensor[] sensors = new Sensor[numSensors];

		for (int i = 0; i < numSensors; i++) {

			switch (i) {
			case 0:
				sensors[i] = new Sensor("CenterDish", "", 697, 458, 0.5);
				break;

			case 1:
				sensors[i] = new Sensor("CenterGround", "", 700, 455, 0);
				break;

			case 2:
				sensors[i] = new Sensor("InnerNorthEast", "", 799, 559, 0);
				break;

			case 3:
				sensors[i] = new Sensor("OuterNorthEast", "", 899, 626, 0);
				break;
			}
		}

		arlo.setSensors(sensors);

		/*****************/
		/* DEFINE EVENTS */
		/*****************/

		int numEvents = 3;

		Event[] events = new Event[numEvents];

		for (int i = 0; i < numEvents; i++) {

			switch (i) {
			case 0:
//				events[i] = new Event(new double[] { 543, 552, 225, 211 });  //!!!fixlater!!!//
				break;

			case 1:
//				events[i] = new Event(new double[] { 722, 730, 404, 393 });
				break;

			case 2:
//				events[i] = new Event(new double[] { 806, 813, 488, 475 });
				break;

			}

		}

		arlo.setEvents(events);

		arlo.setInitialSearchRadius(10.0);
		arlo.setSearchRadiusReductionFactor(0.9999999);
		arlo.setEndSearchRadius(0.01);
		arlo.setSpeedOfSoundInMetersPerSecond(337.642);

		arlo.run();

	}
}
// arlo.setInitialSearchRadius(10.0);
// arlo.setSearchRadiusReductionFactor(0.999999);
// arlo.setEndSearchRadius(0.01);
// arlo.setSpeedOfSoundInMetersPerSecond(337.642);
// eventIndex = 0
// bestScore = -1.3097084236499843
// bestPoint[0] = 767.9233724992735
// bestPoint[1] = 725.2776450286552
// bestPoint[2] = 0.5229589624811366
//
// eventIndex = 1
// bestScore = -0.8202463788532649
// bestPoint[0] = 767.8327572394779
// bestPoint[1] = 722.7842288441545
// bestPoint[2] = 0.0583796195854962
//
// eventIndex = 2
// bestScore = -0.3133080892290631
// bestPoint[0] = 767.8957762706311
// bestPoint[1] = 724.4354680639794
// bestPoint[2] = 0.5821331433796206
