package adapt;

import java.util.ArrayList;

public class ARLO_Tracker_Main {

	public static void main(String[] args) {

		ARLO arlo = new ARLO();

		/******************/
		/* DEFINE SENSORS */
		/******************/

		/*
		 * wav/Audio 01 SW_12.wav wav/Audio 02 NW_12.wav wav/Audio 03 NE_12.wav wav/Audio 04 SE_12.wav wav/Audio 05 CG_12.wav wav/Audio 06 N_13.wav wav/Audio 07 W_06.wav
		 */

		int numSensors = 7;

		Sensor[] sensors = new Sensor[numSensors];

		double inchesPerMeter = 39.3700787;

		for (int i = 0; i < numSensors; i++) {

			switch (i) {

			case 0:
				sensors[i] = new Sensor("SW", "wav/Audio 01 SW_12.wav", 15 / inchesPerMeter, 11 / inchesPerMeter, 36 / inchesPerMeter);
				break;

			case 1:
				sensors[i] = new Sensor("NW", "wav/Audio 02 NW_12.wav", 18 / inchesPerMeter, 169 / inchesPerMeter, 65 / inchesPerMeter);
				break;

			case 2:
				sensors[i] = new Sensor("NE", "wav/Audio 03 NE_12.wav", 152 / inchesPerMeter, 146 / inchesPerMeter, 93 / inchesPerMeter);
				break;

			case 3:
				sensors[i] = new Sensor("SE", "wav/Audio 04 SE_12.wav", 145 / inchesPerMeter, 24 / inchesPerMeter, 53 / inchesPerMeter);
				break;

			case 4:
				sensors[i] = new Sensor("CG", "wav/Audio 05 CG_12.wav", 77 / inchesPerMeter, 99 / inchesPerMeter, 21 / inchesPerMeter);
				break;

			case 5:
				sensors[i] = new Sensor("N", "wav/Audio 06 N_13.wav", 73 / inchesPerMeter, 149 / inchesPerMeter, 78 / inchesPerMeter);
				break;

			case 6:
				sensors[i] = new Sensor("W", "wav/Audio 07 W_06.wav", 15 / inchesPerMeter, 102 / inchesPerMeter, 55 / inchesPerMeter);
				break;
			}
		}

		arlo.setSensors(sensors);

		//

		//

		//

		/*****************/
		/* DEFINE EVENTS */
		/*****************/

		//

		SimpleTable table = null;
		try {
			table = IO.readDelimitedTable("/home/dtcheng/d1", "\t");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		int numDataRows = table.numDataRows;

		// System.out.println("numDataRows = " + numDataRows);

		double lastEventTime = 0.0;
		int numEventSensors = 0;

		ArrayList<Event> eventList = new ArrayList<Event>();

		for (int dataRowIndex = 0; dataRowIndex < numDataRows; dataRowIndex++) {

			double time = table.getDouble(dataRowIndex, 3);

			double timeSinceLastEvent = time - lastEventTime;

			if (timeSinceLastEvent > 0.015) {

				// process last event cluster

				if (numEventSensors == numSensors) {

					// System.out.println("numEventSensors = " + numEventSensors);

					double[] eventTimes = new double[numEventSensors];

					int startIndex = dataRowIndex - numEventSensors;
					int endIndex = startIndex + numEventSensors;

					int numUniqueSensors = 0;

					for (int j = startIndex; j < endIndex; j++) {

						double eventTime = table.getDouble(j, 3);
						String matchString = table.getString(j, 13);

						int matchingSensorIndex = -1;
						boolean[] hasSensor = new boolean[numSensors];
						for (int sensorIndex = 0; sensorIndex < numSensors; sensorIndex++) {

							if (sensors[sensorIndex].matchString.equals(matchString)) {
								matchingSensorIndex = sensorIndex;
								break;
							}

						}

						if (!hasSensor[matchingSensorIndex]) {
							numUniqueSensors++;
						}

						hasSensor[matchingSensorIndex] = true;

						eventTimes[matchingSensorIndex] = eventTime;

					}

					// if (lastEventTime >= 35 * 60 && lastEventTime < 38 * 60) {
					//
					// eventList.add(new Event(eventTimes));
					//
					// }

					// eventList.add(new Event(eventTimes)); // !!! fix later !!!

				}

				// start new cluster
				numEventSensors = 1;
			} else {
				numEventSensors++;
			}

			lastEventTime = time;
		}

		int numEvents = eventList.size();

		Event[] events = new Event[numEvents];

		eventList.toArray(events);

		arlo.setEvents(events);

		// arlo.setInitialSearchRadius(0.01);

		// arlo.setSearchRadiusReductionFactor(0.999999); // fine grain
		// arlo.setSearchRadiusReductionFactor(0.999); // fast
		// arlo.setEndSearchRadius(0.001);

		// arlo.numIterations = (int) 1e8;

		double biasSearchRadius = 0.01;
		System.out.println("biasSearchRadius = " + biasSearchRadius + ";");

		//
		double bestSpeedOfSound = 338;
		System.out.println("bestSpeedOfSound = " + bestSpeedOfSound + ";");
		arlo.setSpeedOfSoundInMetersPerSecond(bestSpeedOfSound);

		double searchRadiusReductionFactor = 0.9999;
		System.out.println("searchRadiusReductionFactor = " + searchRadiusReductionFactor + ";");
		arlo.setSearchRadiusReductionFactor(searchRadiusReductionFactor);

		double initialSearchRadius = 0.1;
		System.out.println("initialSearchRadius = " + initialSearchRadius + ";");
		arlo.setInitialSearchRadius(initialSearchRadius);

		double endSearchRadius = 0.01;
		System.out.println("endSearchRadius = " + endSearchRadius + ";");
		arlo.setEndSearchRadius(endSearchRadius);

		double bestError = Double.POSITIVE_INFINITY;
		int iteration = 0;
		while (true) {
			iteration++;

			int candidateSensorIndex = (int) (Math.random() * numSensors);
			int candidateDimensionIndex = (int) (Math.random() * 3);

			double bestBiasValue = sensors[candidateSensorIndex].position[candidateDimensionIndex];

			double candidateBiasValue = bestBiasValue + 2 * Math.random() * biasSearchRadius - biasSearchRadius;

			// arlo.setSpeedOfSoundInMetersPerSecond(biasValue);
			sensors[candidateSensorIndex].position[candidateDimensionIndex] = candidateBiasValue;

			arlo.run();

			double candidateError = arlo.averageAbsDiffScore;

			System.out.print("STATUS");

			System.out.print("\titeration = \t" + iteration);
			System.out.print("\tcandidateSensorIndex = \t" + candidateSensorIndex);
			System.out.print("\tcandidateDimensionIndex = \t" + candidateDimensionIndex);
			System.out.print("\tbestBiasValue = \t" + bestBiasValue);
			System.out.print("\tcandidateBiasValue = \t" + candidateBiasValue);
			System.out.print("\tcandidateError = \t" + candidateError);
			System.out.print("\tbiasSearchRadius = " + biasSearchRadius);
			System.out.print("\tbestError = " + bestError);
			System.out.println();

			if (candidateError < bestError) {
				bestBiasValue = candidateBiasValue;
				bestError = candidateError;
			} else {
				sensors[candidateSensorIndex].position[candidateDimensionIndex] = bestBiasValue;
			}

			for (int sensorIndex = 0; sensorIndex < numSensors; sensorIndex++) {
				for (int dimensionIndex = 0; dimensionIndex < 3; dimensionIndex++) {
					System.out.print("sensors[" + sensorIndex + "].point[" + dimensionIndex + "] = " + sensors[sensorIndex].position[dimensionIndex] + "; ");
				}
				System.out.println();
			}
			System.out.println("bestError = " + bestError + ";");

			// biasSearchRadius *= 0.999;
		}
	}
}

// averageAbsDiffScore = -10801393727130923