package adapt;

import com.sun.jna.Library;
import com.sun.jna.Native;
public class Environment {
  public interface LibC extends Library {
    public int setenv(String name, String value, int overwrite);
    public int unsetenv(String name);
  }
  static LibC libc = (LibC) Native.loadLibrary("c", LibC.class);
}