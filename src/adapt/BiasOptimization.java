/**
 * University of Illinois/NCSA Open Source License
 * 
 * Copyright (c) 2010, Board of Trustees-University of Illinois. All rights reserved.
 * 
 * Developed by:
 * 
 * David Tcheng Automated Learning Group National Center for Supercomputing Applications
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal with the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to
 * do so, subject to the following conditions:
 * 
 * * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimers.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimers in the documentation and/or other materials provided with the
 * distribution.
 * 
 * * Neither the names of Automated Learning Group, The National Center for Supercomputing Applications, or University of Illinois, nor the names of its contributors may be used to endorse or promote
 * products derived from this Software without specific prior written permission.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 */

package adapt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class BiasOptimization {
	public static void main(String[] args) {

		// if (false) {
		//
		// SimpleTable table = null;
		// try {
		// table = IO.readDelimitedTable("/d250/data/pollen/expertClassifications.csv", "\t");
		// } catch (Exception e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		//
		// HashMap<String, String> hashmap = new HashMap<String, String>();
		//
		// ArrayList<String[]> glaucaSlidesAndGrains = new ArrayList<String[]>();
		// ArrayList<String[]> marianaSlidesAndGrains = new ArrayList<String[]>();
		//
		// for (int i = 0; i < table.numDataRows; i++) {
		//
		// String slideID = table.getString(i, 0);
		// String grainID = table.getString(i, 2);
		// String className = table.getString(i, 3);
		// Double certainty = Double.parseDouble(table.getString(i, 4));
		//
		// className = className.replaceAll(",", " ");
		//
		// int index = className.indexOf(" ");
		// if (index != -1)
		// className = className.substring(0, index);
		// className = className.toLowerCase();
		// //
		// // if (certainty < 90.0) {
		// // continue;
		// // }
		//
		// hashmap.put(className, className);
		//
		// System.out.println();
		// System.out.println("slideID = " + slideID);
		// System.out.println("grainID = " + grainID);
		// System.out.println("className = " + className);
		// System.out.println("certainty = " + certainty);
		//
		// if (className.equals("glacua") || className.equals("glauca")) {
		//
		// glaucaSlidesAndGrains.add(new String[] { slideID, grainID, certainty.toString() });
		//
		// }
		//
		// if (className.equals("mariana") || className.equals("marina")) {
		//
		// marianaSlidesAndGrains.add(new String[] { slideID, grainID, certainty.toString() });
		//
		// }
		//
		// }
		//
		// System.out.println("hashmap.size() = " + hashmap.size());
		//
		// Set keySet = hashmap.keySet();
		//
		// for (Iterator iterator = keySet.iterator(); iterator.hasNext();) {
		// String key = (String) iterator.next();
		// System.out.println("key = " + key);
		//
		// }
		//
		// // rename files //
		//
		// String[] imagePathStringsInDirectory = IO.getPathStrings("/d250/data/pollen/images/fossil");
		//
		// ArrayList<String> imagePathStrings = new ArrayList<String>();
		//
		//
		// for (int i = 0; i < imagePathStringsInDirectory.length; i++) {
		//
		// String filePath = imagePathStringsInDirectory[i];
		//
		// int index = filePath.indexOf(".TIF");
		//
		// String filePathPart1 = filePath.substring(0, index);
		// String filePathPart2 = filePath.substring(index);
		//
		// // if (!filePath.endsWith(".TIF.inputs.example.ser"))
		// // continue;
		//
		// boolean isGlauca = false;
		// boolean isMariana = false;
		//
		// if (true) {
		//					
		// for (int j = 0; j < glaucaSlidesAndGrains.size(); j++) {
		//
		// String[] strings = glaucaSlidesAndGrains.get(j);
		// String slideID = strings[0];
		// String grainID = strings[1];
		// String certainty = strings[2];
		//
		// String newFilePath = filePathPart1 + ".glauca." + certainty + filePathPart2;
		//
		// if (filePath.contains(slideID) && filePath.contains("Pos " + grainID + "/")) {
		// isGlauca = true;
		// // System.out.println("renaming " + filePath + " to " + newFilePath);
		// // IO.rename(filePath, newFilePath);
		// // IO.rename(newFilePath, filePath);
		// // System.out.println("renaming " + newFilePath + " to " + filePath);
		//
		// }
		//
		// }
		//
		// for (int j = 0; j < marianaSlidesAndGrains.size(); j++) {
		//
		// String[] strings = marianaSlidesAndGrains.get(j);
		// String slideID = strings[0];
		// String grainID = strings[1];
		// String certainty = strings[2];
		//
		// String newFilePath = filePathPart1 + ".mariana." + certainty + filePathPart2;
		//
		// if (filePath.contains(slideID) && filePath.contains("Pos " + grainID + "/")) {
		// isMariana = true;
		// // System.out.println("renaming " + filePath + " to " + newFilePath);
		// // IO.rename(filePath, newFilePath);
		// // IO.rename(newFilePath, filePath);
		// // System.out.println("renaming " + newFilePath + " to " + filePath);
		// }
		//
		// }
		// }
		//
		// if (!isGlauca && !isMariana) {
		// System.out.println("Error! (!isGlauca && !isMariana) filePath = " + filePath);
		// // System.exit(1);
		// }
		//
		// }
		//
		// }
		//
		// System.exit(0);

		if (true) {

			/********************/
			/* initialize ADAPT */
			/********************/

			ADAPT adapt = new ADAPT();

			adapt.useExampleSetCache = false;
			adapt.useExampleInputCache = true;

			adapt.numThreads = 8;

			adapt.reportClassificationMatrix = true;

			adapt.numLearningProblemsToCreate = 1000;
			// adapt.reportIntervalInProblems = adapt.numThreads;
			adapt.reportIntervalInProblems = 1;

			adapt.useFileSeparatorForClassNamePathKeys = false;
			adapt.groupIndexIsDirectory = true;
			adapt.groupIndexIsFile = !adapt.groupIndexIsDirectory;

			adapt.dataDirectoryPath = "/d250/data/pollen/";
			adapt.imageDirectoryPath = "/d250/data/pollen/";
			adapt.imageDirectoryPath = adapt.dataDirectoryPath + "images/";

			adapt.doInitialImageChecks = false; // 1st time only //
			adapt.deleteBadFiles = false;
			adapt.textureXYResolution = 200; // pollen
			adapt.minShapeXYResolution = 200; // pollen
			adapt.maxXResolution = 2048;
			adapt.maxYResolution = 2048;
			adapt.maxHeight = 2048;
			adapt.maxWidth = 2048;
			adapt.useGCForScan = false;
			adapt.useGCForExampleCreation = false;
			adapt.normalizeSpectralPowerOfImage = false;

			adapt.reportGoodClasses = true;
			adapt.minNumExamplesForActiveClass = 1;
			adapt.readClassNamesFromCSVFile();

			/*****************/
			/* PROBLEM SPACE */
			/*****************/

			adapt.numColorsToUse = 1; // pollen

			adapt.minNumClassesToUse = 2; // pollen
			adapt.maxNumClassesToUse = 2; // pollen
			adapt.numNumClassesToUse = adapt.maxNumClassesToUse - adapt.minNumClassesToUse + 1;

			adapt.useNaturalTrainingSets = true;
//			adapt.numTestingGroups = 1;
//			adapt.minNumTrainingGroupsLog2 = -1;
//			adapt.maxNumTrainingGroupsLog2 = -1;
			// adapt.numTestingGroups = 8;
			// adapt.numTestingGroups = 69;
//			adapt.numTestingGroups = 11;
			adapt.numTestingGroups = 36;
			adapt.minNumTrainingGroupsLog2 = 1; // pollen
			adapt.maxNumTrainingGroupsLog2 = 7; // pollen
			// adapt.numNumTrainingExamplesLog2 = adapt.maxNumTrainingExamplesLog2 - adapt.minNumTrainingExamplesLog2 + 1;
			// adapt.numTestingExamples = 2469; // pollen?
			// adapt.numTestingExamples = 2469;

			// adapt.minNumTrainingExamplesLog2 = (int) (Math.log(adapt.numExamples - adapt.numTestingExamples) / Math.log(2.0));
			// adapt.maxNumTrainingExamplesLog2 = (int) (Math.log(adapt.numExamples - adapt.numTestingExamples) / Math.log(2.0)); // testing
			adapt.numNumTrainingGroupsLog2 = adapt.maxNumTrainingGroupsLog2 - adapt.minNumTrainingGroupsLog2 + 1;

			// adapt.minNumExamplesPerClass = (int) Math.pow(2, adapt.maxNumTrainingExamplesPerClassLog2) + adapt.numTestingExamplesPerClass;
			adapt.minNumExamplesForActiveClass = 1;

			// System.out.println("adapt.minNumTrainingExamplesLog2 = " + adapt.minNumTrainingExamplesLog2);
			// System.out.println("adapt.maxNumTrainingExamplesLog2 = " + adapt.maxNumTrainingExamplesLog2);
			// System.out.println("adapt.numNumTrainingExamplesLog2 = " + adapt.numNumTrainingExamplesLog2);
			// System.out.println("adapt.minNumExamplesPerClass = " + adapt.minNumExamplesForActiveClass);

			adapt.problemGenerationRandomSeed = 123;

			/**************/
			/* BIAS SPACE */
			/**************/

			// adapt.distanceWeightingPower = biasValue;
			adapt.distanceWeightingPower = (double) 12; // pollen (10)

			// adapt.overallIntensityDistributionWeight = biasValue;
			adapt.overallIntensityDistributionWeight = 16f; // pollen 16 before
			adapt.minNumIntensityBins = 2;
			adapt.maxNumIntensityBins = 15;
			adapt.numNumIntensityBins = adapt.maxNumIntensityBins - adapt.minNumIntensityBins + 1;
			adapt.intensityDistributionFeatureIndices = new int[adapt.numNumIntensityBins];
			adapt.intensityDistributionFeatureWeights = new double[adapt.numNumIntensityBins];
			// adapt.intensityDistributionFeatureWeights[10] = (double) 1.0;
			for (int i = 0; i < adapt.numNumIntensityBins; i++) {
				adapt.intensityDistributionFeatureWeights[i] = 1.0f;
			}

			// adapt.overallGrossShapeWeight = biasValue;
			adapt.overallGrossShapeWeight = 1f; // pollen
			adapt.minGrossShapeResolution = 1;
			adapt.maxGrossShapeResolution = 11;
			adapt.numGrossShapeResolutions = adapt.maxGrossShapeResolution - adapt.minGrossShapeResolution + 1;
			adapt.grossShapeFeatureIndices = new int[adapt.numGrossShapeResolutions];
			adapt.grossShapeFeatureWeights = new double[adapt.numGrossShapeResolutions];
			// adapt.grossShapeFeatureWeights[10] = (double) 1.0;
			for (int i = 0; i < adapt.numGrossShapeResolutions; i++) {
				adapt.grossShapeFeatureWeights[i] = 1.0f;
			}

			// adapt.overallTextureWeight = biasValue;
			adapt.overallTextureWeight = 4096; // pollen
			adapt.minTextureLineNumPixels = 1;
			adapt.maxTextureLineNumPixels = 11;
			adapt.numTextureLineNumPixels = adapt.maxTextureLineNumPixels - adapt.minTextureLineNumPixels + 1;
			adapt.textureLineFeatureIndices = new int[adapt.numTextureLineNumPixels];
			adapt.textureLineFeatureWeights = new double[adapt.numTextureLineNumPixels];
			// adapt.textureLineFeatureWeights[(int) 7] = (double) 1.0;
			for (int i = 0; i < adapt.numTextureLineNumPixels; i++) {
				adapt.textureLineFeatureWeights[i] = 1.0f;
			}

			/*******************/
			/* create examples */
			/*******************/

			// int code = Math.abs(adaptParameterMap.toString().hashCode());

			String examplesPath = adapt.dataDirectoryPath + "examples.ser";

			if (adapt.useExampleSetCache && IO.fileExists(examplesPath)) {
				adapt.loadExamples(examplesPath);
			} else {
				adapt.createExamples();
				adapt.saveExamples(examplesPath);
			}

			adapt.setVerbose(true);
			adapt.createLearningProblems();

			int numProblems = adapt.getNumProblems();
			System.out.println("numProblems:" + numProblems);

			//

			//

			long startTime = System.currentTimeMillis();

			adapt.solveSupervisedLearningProblems();

			long endTime = System.currentTimeMillis();

			//

			//

			double durationInSeconds = (endTime - startTime) / (double) 1000.0;
			double problemsPerSecond = numProblems / durationInSeconds;

			System.out.println("numProblems:" + numProblems);
			System.out.println("problemsPerSecond:" + problemsPerSecond);

		}

	}
}
