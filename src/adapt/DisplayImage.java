package adapt;

import java.awt.*;
import java.io.*;
import java.awt.event.*;
import java.awt.image.*;
import java.awt.geom.*;
import java.awt.font.*;

import javax.swing.*;
import javax.imageio.*;

public class DisplayImage extends JApplet {

	private BufferedImage bi[];

	public static final float[] BLUR3x3 = { 0.1f, 0.1f, 0.1f, 0.1f, 0.2f, 0.1f, 0.1f, 0.1f, 0.1f };

	public static final float[] SHARPEN3x3 = { 0.f, -1.f, 0.f, -1.f, 5.f, -1.f, 0.f, -1.f, 0.f };

	public void init() {

		setBackground(Color.white);

		// Load two images that we can use as examples for the
		// image operations.

		bi = new BufferedImage[4];
		String s[] = { "bld.jpg", "bld.jpg", "boat.gif", "boat.gif" };

		for (int i = 0; i < bi.length; i++) {
			File f = new File("C:/" + s[i]);
			try {

				// Read in a BufferedImage from a file.
				BufferedImage bufferedImage = ImageIO.read(f);

				// Convert the image to an RGB style normalized image.
				bi[i] = new BufferedImage(bufferedImage.getWidth(), bufferedImage.getHeight(), BufferedImage.TYPE_INT_RGB);
				bi[i].getGraphics().drawImage(bufferedImage, 0, 0, this);

			} catch (IOException e) {
				System.err.println("Error reading file: " + f);
				System.exit(1);
			}
		}
	}

	public void paint(Graphics g) {

		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		int w = getSize().width;
		int h = getSize().height;

		// Set the color to black.

		g2.setColor(Color.black);

		// Create a low-pass filter and a sharpen filter.

		float[][] data = { BLUR3x3, SHARPEN3x3 };

		String theDesc[] = { "Convolve LowPass", "Convolve Sharpen", "LookupOp", "RescaleOp" };

		// Cycle through each of the four BufferedImage objects.

		for (int i = 0; i < bi.length; i++) {

			int iw = bi[i].getWidth(this);
			int ih = bi[i].getHeight(this);
			int x = 0, y = 0;

			// Create a scaled transformation for the image.

			AffineTransform at = new AffineTransform();
			at.scale((w - 14) / 2.0 / iw, (h - 34) / 2.0 / ih);

			BufferedImageOp biop = null;
			BufferedImage bimg = new BufferedImage(iw, ih, BufferedImage.TYPE_INT_RGB);

			switch (i) {

			// IMAGE 1 and 2: Create a convolution
			// kernel that consists of either the low-pass filter
			// or the sharpen filter. Set the x and y of the image
			// so that it appears in the correct quadrant and has
			// enough room for the descriptive text above.

			case 0:
			case 1:
				x = i == 0 ? 5 : w / 2 + 3;
				y = 15;

				Kernel kernel = new Kernel(3, 3, data[i]);
				ConvolveOp cop = new ConvolveOp(kernel, ConvolveOp.EDGE_NO_OP, null);

				// Apply the convolution operation, placing the
				// result in bimg.

				cop.filter(bi[i], bimg);

				// Create the appropriate AffineTransformation that
				// will be used while drawing IMAGES 1 and 2

				biop = new AffineTransformOp(at, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
				break;

			case 2:
				x = 5;
				y = h / 2 + 15;

				// IMAGE 3:
				// Create the parameters needed for a LookupOp, which
				// process the color channels of an image using a
				// look-up table. This will create a reverse brightness
				// of the image, similar to a photographic negative.

				byte chlut[] = new byte[256];
				for (int j = 0; j < 200; j++)
					chlut[j] = (byte) (256 - j);
				ByteLookupTable blut = new ByteLookupTable(0, chlut);
				LookupOp lop = new LookupOp(blut, null);

				lop.filter(bi[i], bimg);

				// Create the appropriate AffineTransformation, which
				// will be used while drawing the IMAGE 3.

				biop = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
				break;

			case 3:
				x = w / 2 + 3;
				y = h / 2 + 15;

				// IMAGE 4:
				// Perform a rescaling operation, multiplying each
				// pixel by a scaling factor (1.1), then adding an
				// offset (20.0). Note that this has nothing to do
				// with a geometric scaling of an image.

				RescaleOp rop = new RescaleOp(1.1f, 20.0f, null);
				rop.filter(bi[i], bimg);
				biop = new AffineTransformOp(at,

				AffineTransformOp.TYPE_BILINEAR);
			}

			// Draw the image with the appropriate AffineTransform
			// operation, as well as the text above it.

			g2.drawImage(bimg, biop, x, y);
			TextLayout tl = new TextLayout(theDesc[i], g2.getFont(), g2.getFontRenderContext());
			tl.draw(g2, (float) x, (float) y - 4);
		}
	}

	public static void main(String s[]) {
		JFrame f = new JFrame("ImageOps");
		f.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		JApplet applet = new DisplayImage();
		f.getContentPane().add("Center", applet);
		applet.init();
		f.pack();
		f.setSize(new Dimension(550, 550));
		f.setVisible(true);
	}

}