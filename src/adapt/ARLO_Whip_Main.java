package adapt;

import java.util.ArrayList;
import java.util.Arrays;

public class ARLO_Whip_Main {

	public static void main(String[] args) {
		String dataFilePath = "/home/dtcheng/tags.csv";
		int minNumSensors = 3;
		int maxNumSensors = 9;
		double clusterTimeGap = 0.8;

		ARLO arlo = new ARLO();

		arlo.reportEvents = true;
		arlo.minAverageTOADErrorInMS = 1.0;
//		 arlo.minAverageTOADErrorInMS = Double.POSITIVE_INFINITY;
		arlo.minZ = 0.0;

		/******************/
		/* DEFINE SENSORS */
		/******************/

		/*
		 * wav/Audio 01 SW_12.wav wav/Audio 02 NW_12.wav wav/Audio 03 NE_12.wav wav/Audio 04 SE_12.wav wav/Audio 05 CG_12.wav wav/Audio 06 N_13.wav wav/Audio 07 W_06.wav
		 */

		int numSensors = 8;

		Sensor[] sensors = new Sensor[numSensors];

		for (int i = 0; i < numSensors; i++) {

			switch (i) {

			case 0:
				sensors[i] = new Sensor("50moon", "50moon", 773.8, 536.8, 0);
				break;

			case 1:
				sensors[i] = new Sensor("51moon", "51moon", 723.9, 538.8, 0);
				break;

			case 2:
				sensors[i] = new Sensor("52moon", "52moon", 773.3, 613.1, 0);
				break;

			case 3:
				sensors[i] = new Sensor("53moon", "53moon", 793.3, 719.8, 0);
				break;

			case 4:
				sensors[i] = new Sensor("54moon", "54moon", 738.9, 615.6, 0);
				break;

			case 5:
				sensors[i] = new Sensor("55moon", "55moon", 810.7, 701.3, 0);
				break;

			case 6:
				sensors[i] = new Sensor("56moon", "56moon", 740.4, 619.1, 0);
				break;

			case 7:
				sensors[i] = new Sensor("57moon", "57moon", 753.4, 668.9, 0);
				break;

			}
		}

		arlo.setSensors(sensors);

		//

		//

		//

		/*****************/
		/* DEFINE EVENTS */
		/*****************/

		//

		SimpleTable table = null;
		try {
			table = IO.readDelimitedTable(dataFilePath, ",");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		int numDataRows = table.numDataRows;

		System.out.println("numDataRows = " + numDataRows);

		int numClusterEvents = 0;

		ArrayList<SensorEvent> sensorEventList = new ArrayList<SensorEvent>();

		double recordingAbsoluteStartTime = 0.0 /*0524.0 * 24.0 * 3600.0 + 20.0 * 3600.0*/;

		for (int dataRowIndex = 0; dataRowIndex < numDataRows; dataRowIndex++) {

			String fileName = table.getString(dataRowIndex, 13);



			int sensorIndex = -1;
			String remains = null;
			for (int i = 0; i < numSensors; i++) {
				if (fileName.contains(sensors[i].matchString)) {
					sensorIndex = i;
					remains = fileName.replaceAll(sensors[i].matchString, "");
				}
			}
			
			if (sensorIndex == -1) {
				continue;
			}
			
			fileName = fileName.replaceAll("brown","");
			fileName = fileName.replaceAll("eh","");
			fileName = fileName.replaceAll("fc","");
			fileName = fileName.replaceAll("ap","");
			fileName = fileName.replaceAll("hing","");
			fileName = fileName.replaceAll("lg","");
			fileName = fileName.replaceAll("lm","");
			fileName = fileName.replaceAll("ken","");
			fileName = fileName.replaceAll("morgan","");
			fileName = fileName.replaceAll("sd","");
			
			if (!fileName.subSequence(2,6).equals("moon")) {
				fileName = fileName.substring(0,2) + "moon" + fileName.substring(3);
			}
			
			
//			System.out.println(fileName);
			
			String micName = fileName.substring(0, 6);
			int yearNumber = Integer.parseInt(fileName.substring(6, 10));
			String dateNumber = fileName.substring(10, 14);
			String hourNumber =  fileName.substring(14,18);

			double timeOffset = Double.parseDouble(dateNumber) * 24 * 3600 + Double.parseDouble(hourNumber) / 100 * 3600;

			double fileRelativeStartTime = table.getDouble(dataRowIndex, 3);
			double absoluteStartTime = fileRelativeStartTime + timeOffset;

			double recordingRelativeStartTime = absoluteStartTime - recordingAbsoluteStartTime;

			// System.out.println(fileRelativeStartTime + "\t" + timeOffset + "\t" + absoluteStartTime + "\t"+ recordingRelativeStartTime + "\t" + fileName + "\t" + sensorIndex + "\t" + remains + "\t"
			// + micName + "\t" + dateNumber + "\t" + hourNumber);

			int tagID = table.getInt(dataRowIndex, 2);

			sensorEventList.add(new SensorEvent(recordingRelativeStartTime, sensorIndex, fileName, fileRelativeStartTime, Double.NaN, tagID));

		}

		int numSensorEvents = sensorEventList.size();

		SensorEvent[] sensorEvents = new SensorEvent[numSensorEvents];

		sensorEvents = sensorEventList.toArray(sensorEvents);

		Arrays.sort(sensorEvents, 0, sensorEventList.size());

		if (false)
			for (int i = 0; i < numSensorEvents; i++) {
				System.out.println(sensorEvents[i].time + "\t" + sensorEvents[i].sensorIndex);
			}

		ArrayList<Event> candidateGlobalEventList = new ArrayList<Event>();
		double lastEventTime = sensorEvents[0].time;

		boolean[] hasSensor = new boolean[numSensors];
		for (int sensorEventIndex = 0; sensorEventIndex < numSensorEvents; sensorEventIndex++) {

			double time = sensorEvents[sensorEventIndex].time;

			double timeSinceLastEvent = time - lastEventTime;

			if (timeSinceLastEvent > clusterTimeGap) {

				// process last event cluster

				ArrayList<Double> eventTimes = new ArrayList();
				ArrayList<double[]> eventPositions = new ArrayList();
				ArrayList<String> eventFileNames = new ArrayList();
				ArrayList<Double> eventFileTimes = new ArrayList();
				ArrayList<Integer> eventTagIDs = new ArrayList();

				int startIndex = sensorEventIndex - numClusterEvents;
				int endIndex = startIndex + numClusterEvents;

				int numUniqueSensors = 0;
				boolean sensorIndexRepeat = false;
				for (int clusterSensorEventIndex = startIndex; clusterSensorEventIndex < endIndex; clusterSensorEventIndex++) {

					double eventTime = sensorEvents[clusterSensorEventIndex].time;
					int sensorIndex = sensorEvents[clusterSensorEventIndex].sensorIndex;

					if (!hasSensor[sensorIndex]) {
						hasSensor[sensorIndex] = true;
						numUniqueSensors++;
					} else {
						sensorIndexRepeat = true;
					}

					eventTimes.add(eventTime);
					eventPositions.add(sensors[sensorIndex].position);
					eventFileNames.add(sensorEvents[clusterSensorEventIndex].fileName);
					eventFileTimes.add(sensorEvents[clusterSensorEventIndex].fileTime);
					eventTagIDs.add(sensorEvents[clusterSensorEventIndex].tagID);

				}

				if ((!sensorIndexRepeat) && (numClusterEvents == numUniqueSensors) && (numUniqueSensors >= minNumSensors) && (numUniqueSensors <= maxNumSensors)) {

					// System.out.println("numUniqueSensors = " + numUniqueSensors);

					double[] times = new double[eventTimes.size()];
					double[][] positions = new double[eventPositions.size()][3];
					String[] fileNames = new String[eventFileNames.size()];
					double[] fileTimes = new double[eventFileTimes.size()];
					int[] tagIDs = new int[eventTagIDs.size()];

					for (int j = 0; j < numUniqueSensors; j++) {
						times[j] = eventTimes.get(j);
						positions[j] = eventPositions.get(j);
						fileNames[j] = eventFileNames.get(j);
						fileTimes[j] = eventFileTimes.get(j);
						tagIDs[j] = eventTagIDs.get(j);
					}

					candidateGlobalEventList.add(new Event(times, positions, fileNames, fileTimes, null, tagIDs));
				}

				// start new cluster
				numClusterEvents = 1;

				hasSensor = new boolean[numSensors];

			} else {
				numClusterEvents++;
			}

			lastEventTime = time;
		}

		int numGlobalEvents = candidateGlobalEventList.size();

		System.out.println("numGlobalEvents = " + numGlobalEvents);
		Event[] events = new Event[numGlobalEvents];

		candidateGlobalEventList.toArray(events);

		arlo.setEvents(events);

		// arlo.setInitialSearchRadius(0.01);

		// arlo.setSearchRadiusReductionFactor(0.999999); // fine grain
		// arlo.setSearchRadiusReductionFactor(0.999); // fast
		// arlo.setEndSearchRadius(0.001);

		// double biasSearchRadius = 0.01; // for skywriting
		double biasSearchRadius = 0.00; // for thrush
		System.out.println("biasSearchRadius = " + biasSearchRadius + ";");

		//
		// TASK LEVEL BIASES //
		//

		double bestSpeedOfSound = 337.311; // for thrush
		System.out.println("bestSpeedOfSound = " + bestSpeedOfSound + ";");
		arlo.setSpeedOfSoundInMetersPerSecond(bestSpeedOfSound);

		arlo.maxNumIterations = (int) 1e6;

		double searchRadiusReductionFactor = 0.9999999;
		System.out.println("searchRadiusReductionFactor = " + searchRadiusReductionFactor + ";");
		arlo.setSearchRadiusReductionFactor(searchRadiusReductionFactor);

		// double initialSearchRadius = 0.1; // for skywriting
		double initialSearchRadius = 10.0; // for thrush
		System.out.println("initialSearchRadius = " + initialSearchRadius + ";");
		arlo.setInitialSearchRadius(initialSearchRadius);

		double bestError = Double.POSITIVE_INFINITY;
		int iteration = 0;

		if (true) {
			iteration++;

			int candidateSensorIndex = (int) (Math.random() * numSensors);
			int candidateDimensionIndex = (int) (Math.random() * 3);

			double bestBiasValue = sensors[candidateSensorIndex].position[candidateDimensionIndex];

			double candidateBiasValue = bestBiasValue + 2 * Math.random() * biasSearchRadius - biasSearchRadius;

			// arlo.setSpeedOfSoundInMetersPerSecond(biasValue);
			sensors[candidateSensorIndex].position[candidateDimensionIndex] = candidateBiasValue;

			arlo.run();

			System.out.println("arlo.numGoodEvents = " + arlo.numGoodEvents);

			double candidateError = arlo.averageAbsDiffScore;

			if (true) {
				System.out.print("STATUS");
				System.out.print("\titeration = \t" + iteration);
				System.out.print("\tcandidateSensorIndex = \t" + candidateSensorIndex);
				System.out.print("\tcandidateDimensionIndex = \t" + candidateDimensionIndex);
				System.out.print("\tbestBiasValue = \t" + bestBiasValue);
				System.out.print("\tcandidateBiasValue = \t" + candidateBiasValue);
				System.out.print("\tcandidateError = \t" + candidateError);
				System.out.print("\tbiasSearchRadius = " + biasSearchRadius);
				System.out.print("\tbestError = " + bestError);
				System.out.println();
			}

			if (candidateError < bestError) {
				bestBiasValue = candidateBiasValue;
				bestError = candidateError;
			} else {
				sensors[candidateSensorIndex].position[candidateDimensionIndex] = bestBiasValue;
			}

			for (int sensorIndex = 0; sensorIndex < numSensors; sensorIndex++) {
				for (int dimensionIndex = 0; dimensionIndex < 3; dimensionIndex++) {
					System.out.print("sensors[" + sensorIndex + "].point[" + dimensionIndex + "] = " + sensors[sensorIndex].position[dimensionIndex] + "; ");
				}
				System.out.println();
			}
			System.out.println("bestError = " + bestError + ";");

			// biasSearchRadius *= 0.999;
		}
	}
}

// 16.461465308838694 100.0;
// 16.368630528529387 1000
