/**
 * 
 */
package adapt;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.Raster;
import java.awt.image.SampleModel;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;

/**
 * @author dtcheng
 * 
 */
public class TIFFReader {
	
	
	
	// aborted attempt at building a tiff reader!
	
	
	

//	// memory allocation
//	int maxNumDirectories = 100000;
//	int maxNumDirectoryEntries = 100000;
//
//	// file
//
//	// data format
//	boolean isLittleEndian = false;
//
//	// output control
//	boolean verbose = false;
//
//	// data
//
//	int[][] directoryEntryTags;
//	int[][] directoryEntryFieldTypes;
//	long[][] directoryEntryFieldCounts;
//	long[][] directoryEntryFieldValueOrOffsets;
//	boolean[] imageCompressed;
//
//	TIFFReader() {
//	}
//
//	byte[] tiffFileDataBytes;
//	int tiffFileLength;
//	int tiffFileDataIndex;
//	
//
//	public boolean parseTiffFileData(byte[] tiffFileBytes, int length) throws Exception {
//
//		this.tiffFileDataBytes = tiffFileBytes;
//		this.tiffFileLength = length;
//
//		tiffFileDataIndex = 0;
//
//		// determine byte order
//		{
//			int byteOrder1 = readUnsignedByte();
//			int byteOrder2 = readUnsignedByte();
//
//			if (byteOrder1 == 'I' && byteOrder2 == 'I') {
//				isLittleEndian = true;
//			} else if (byteOrder1 == 'M' && byteOrder2 == 'M') {
//				isLittleEndian = false;
//			} else {
//				return false;
//			}
//		}
//		System.out.println("isLittleEndian = " + isLittleEndian);
//
//		// look for special tiff format indicator '42' (and meaning of life the universe and everything)
//
//		int meaningOfLife = 0;
//		{
//			int value1 = readUnsignedByte();
//			int value2 = readUnsignedByte();
//
//			System.out.println("value1 = " + value1);
//			System.out.println("value2 = " + value2);
//
//			if (isLittleEndian) {
//				meaningOfLife = value1 + (value2 << 8);
//			} else {
//				meaningOfLife = value2 + (value1 << 8);
//			}
//
//		}
//		System.out.println("meaningOfLife = " + meaningOfLife);
//
//		if (meaningOfLife != 42) {
//			return false;
//		}
//
//		// position file to first IFD
//
//		long ifdOffset = readLittleOrBigIFDOffset();
//
//		System.out.println("ifdOffset = " + ifdOffset);
//
//		tiffFileDataIndex = (int) ifdOffset;
//
//		// read all IFDs an create objects
//
//		int[][] directoryEntryTags = new int[maxNumDirectories][];
//		int[][] directoryEntryFieldTypes = new int[maxNumDirectories][];
//		long[][] directoryEntryFieldCounts = new long[maxNumDirectories][];
//		long[][] directoryEntryFieldValueOrOffsets = new long[maxNumDirectories][];
//		boolean[] imageCompressed = new boolean[maxNumDirectories];
//		long[] imageWidths = new long[maxNumDirectories];
//		long[] imageLengths = new long[maxNumDirectories];
//		long[] imageStripOffsets = new long[maxNumDirectories];
//		long[] imageSamplesPerPixels = new long[maxNumDirectories];
//		long[] imageRowsPerStrips = new long[maxNumDirectories];
//		long[] imageSampleByteCounts = new long[maxNumDirectories];
//
//		int totalNumEntriesDirectories = 0;
//		int totalNumDirectories = 0;
//		while (true) {
//
//			int numDirectorieEntries = readUnsignedShort();
//			 System.out.println("numDirectorieEntries = " + numDirectorieEntries);
//
//			int[] entryTags = new int[numDirectorieEntries];
//			int[] entryFieldTypes = new int[numDirectorieEntries];
//			long[] entryFieldCounts = new long[numDirectorieEntries];
//			long[] entryFieldValueOrOffsets = new long[numDirectorieEntries];
//
//			// read entries
//
//			for (int i = 0; i < numDirectorieEntries; i++) {
//
//				int tag = readUnsignedShort();
//				entryTags[i] = tag;
//
//				int fieldType = readUnsignedShort();
//				entryFieldTypes[i] = fieldType;
//
//				long fieldCount = readUnsignedInt();
//				entryFieldCounts[i] = fieldCount;
//
//				long valueOrOffset = readUnsignedInt();
//				entryFieldValueOrOffsets[i] = valueOrOffset;
//
//			}
//			long nextIFDOffset = readLittleOrBigIFDOffset();
//
//			directoryEntryTags[totalNumDirectories] = entryTags;
//			directoryEntryFieldTypes[totalNumDirectories] = entryFieldTypes;
//			directoryEntryFieldCounts[totalNumDirectories] = entryFieldCounts;
//			directoryEntryFieldValueOrOffsets[totalNumDirectories] = entryFieldValueOrOffsets;
//
//			// update counts
//
//			totalNumDirectories++;
//			totalNumEntriesDirectories += numDirectorieEntries;
//
//			// check for end condition
//
//			if (nextIFDOffset == 0) {
//				break;
//			}
//
//			// position file to next IFD
//
//			tiffFileDataIndex = (int) (nextIFDOffset);
//
//		}
//
//		System.out.println("totalNumDirectories        = " + totalNumDirectories);
//		System.out.println("totalNumEntriesDirectories = " + totalNumEntriesDirectories);
//
//		long stripOffset = -1;
//		long stripByteCounts = -1;
//		long imageWidth = -1;
//		long imageLength = -1;
//
//		// read meta data
//
//		for (int directoryIndex = 0; directoryIndex < totalNumDirectories; directoryIndex++) {
//			int numEntries = directoryEntryTags[directoryIndex].length;
//			for (int entryIndex = 0; entryIndex < numEntries; entryIndex++) {
//
//				int tag = directoryEntryTags[directoryIndex][entryIndex];
//				int fieldType = directoryEntryFieldTypes[directoryIndex][entryIndex];
//				long count = directoryEntryFieldCounts[directoryIndex][entryIndex];
//				long valueOrOffset = directoryEntryFieldValueOrOffsets[directoryIndex][entryIndex];
//
//				String typeString = null;
//
//				switch (fieldType) {
//				case 1:
//					typeString = "BYTE";
//					break;
//				case 2:
//					typeString = "ASCII";
//					break;
//				case 3:
//					typeString = "SHORT";
//					break;
//				case 4:
//					typeString = "LONG";
//					break;
//				case 5:
//					typeString = "RATIONAL";
//					break;
//				case 6:
//					typeString = "SBYTE";
//					break;
//				case 7:
//					typeString = "UNDEFINED";
//					break;
//				case 8:
//					typeString = "SSHORT";
//					break;
//				case 9:
//					typeString = "SLONG";
//					break;
//				case 10:
//					typeString = "SRATIONAL";
//					break;
//				case 11:
//					typeString = "FLOAT";
//					break;
//				case 12:
//					typeString = "DOUBLE";
//					break;
//				default:
//					typeString = "?????????";
//					break;
//				}
//
//				String tagString = null;
//
//				switch (tag) {
//				case 256:
//					tagString = "ImageWidth";
//					break;
//				case 257:
//					tagString = "ImageLength";
//					break;
//				case 258:
//					tagString = "BitsPerSample";
//					break;
//				case 259:
//					tagString = "Compression";
//					break;
//				case 262:
//					tagString = "PhotometricInterpretation";
//					break;
//				case 271:
//					tagString = "Make";
//					break;
//				case 272:
//					tagString = "Model";
//					break;
//				case 273:
//					tagString = "StripOffsets";
//					break;
//				case 277:
//					tagString = "SamplesPerPixel";
//					break;
//				case 278:
//					tagString = "RowsPerStrip";
//					break;
//				case 279:
//					tagString = "StripByteCounts";
//					break;
//				case 282:
//					tagString = "XResolution";
//					break;
//				case 283:
//					tagString = "YResolution";
//					break;
//				case 296:
//					tagString = "ResolutionUnit";
//					break;
//				case 305:
//					tagString = "Software";
//					break;
//				case 306:
//					tagString = "DateTime";
//					break;
//				case 530:
//					tagString = "YCbCrSubSampling";
//					break;
//				case 532:
//					tagString = "ReferenceBlackWhite";
//					break;
//
//				case 65420:
//					tagString = "Version";
//					break;
//				case 65421:
//					tagString = "Lens/Image Type";
//					break;
//				case 65422:
//					tagString = "Physical X";
//					break;
//				case 65423:
//					tagString = "Physical Y";
//					break;
//				case 65424:
//					tagString = "Physical Z";
//					break;
//				case 65425:
//					tagString = "Tissue Index";
//					break;
//				case 65426:
//					tagString = "NanoZommer?";
//					break;
//				case 65427:
//					tagString = "Reference";
//					break;
//				case 65428:
//					tagString = "NanoZommer?";
//					break;
//				case 65433:
//					tagString = "NanoZommer?";
//					break;
//				case 65439:
//					tagString = "NanoZommer?";
//					break;
//				case 65440:
//					tagString = "NanoZommer?";
//					break;
//				case 65441:
//					tagString = "NanoZommer?";
//					break;
//				case 65442:
//					tagString = "NanoZommer?";
//					break;
//				case 65443:
//					tagString = "NanoZommer?";
//					break;
//				case 65444:
//					tagString = "NanoZommer?";
//					break;
//				case 65445:
//					tagString = "NanoZommer?";
//					break;
//				case 65446:
//					tagString = "NanoZommer?";
//					break;
//				case 65449:
//					tagString = "NanoZommer?";
//					break;
//
//				default:
//					tagString = "?????????";
//					break;
//				}
//
//				if (true)
//					System.out.println(directoryIndex + "\t" + entryIndex + "\t" + tag + "\t" + tagString + "\t" + typeString + "\t" + count + "\t" + valueOrOffset);
//
//				if (tag == 256) {
//					imageWidths[directoryIndex] = valueOrOffset;
//				}
//				if (tag == 257) {
//					imageLengths[directoryIndex] = valueOrOffset;
//				}
//				if (tag == 259) {
//					if (valueOrOffset == 1)
//						imageCompressed[directoryIndex] = false;
//					else
//						imageCompressed[directoryIndex] = true;
//				}
//				if (tag == 273) {
//					imageStripOffsets[directoryIndex] = valueOrOffset;
//				}
//				if (tag == 277) {
//					imageSamplesPerPixels[directoryIndex] = valueOrOffset;
//				}
//				if (tag == 278) {
//					imageRowsPerStrips[directoryIndex] = valueOrOffset;
//				}
//				if (tag == 279) {
//					imageSampleByteCounts[directoryIndex] = valueOrOffset;
//				}
//			}
//			// System.out.println("imageCompressed = " + imageCompressed[directoryIndex]);
//		}
//
//		for (int directoryIndex = 0; directoryIndex < totalNumDirectories; directoryIndex++) {
//
//			// if (imageWidths[directoryIndex] > 65535 || imageLengths[directoryIndex] > 65535) {
//			// System.out.println("skipping image size: " + imageWidths[directoryIndex] + "x" + imageLengths[directoryIndex]);
//			// } else
//
////			if (imageCompressed[directoryIndex]) {
////
////				System.out.println("seek to: " + imageStripOffsets[directoryIndex]);
////				raf.seek(imageStripOffsets[directoryIndex]);
////
////				byte[] data = new byte[(int) imageSampleByteCounts[directoryIndex]];
////
////				raf.readFully(data);
////
////				// if (imageWidths[directoryIndex] > 65535) {
////
////				String filePathString1 = jpgOutputDirectory + "/test." + directoryIndex + ".jpg";
////
////				RandomAccessFile out = new RandomAccessFile(filePathString1, "rw");
////				out.write(data);
////				out.close();
////
////				// }
////
////			}
//		}
//		
//		return true;
//
//	}
//
//	private int readUnsignedShort() throws IOException {
//		int unsignedShort;
//		int value1 = readUnsignedByte();
//		int value2 =  readUnsignedByte();
//
//		if (isLittleEndian) {
//			unsignedShort = value1 + (value2 << 8);
//		} else {
//			unsignedShort = value2 + (value1 << 8);
//		}
//
//		return unsignedShort;
//	}
//
//	private long readUnsignedInt() throws IOException {
//		long unsignedInt;
//		long value1 =  readUnsignedByte();
//		long value2 = readUnsignedByte();
//		long value3 = readUnsignedByte();
//		long value4 = readUnsignedByte();
//
//		if (verbose) {
//			System.out.println("value1 = " + value1);
//			System.out.println("value2 = " + value2);
//			System.out.println("value3 = " + value3);
//			System.out.println("value4 = " + value4);
//		}
//
//		if (isLittleEndian) {
//			unsignedInt = value1 + (value2 << 8) + (value3 << 16) + (value4 << 24);
//		} else {
//			unsignedInt = value4 + (value3 << 8) + (value2 << 16) + (value1 << 24);
//		}
//
//		return unsignedInt;
//	}
//
//	private long readLittleOrBigIFDOffset() throws IOException {
//		long ifdOffset;
//		long value1 =  readUnsignedByte();
//		long value2 =  readUnsignedByte();
//		long value3 =  readUnsignedByte();
//		long value4 = readUnsignedByte();
//		if (verbose) {
//			System.out.println("value1 = " + value1);
//			System.out.println("value2 = " + value2);
//			System.out.println("value3 = " + value3);
//			System.out.println("value4 = " + value4);
//		}
//
//		if (isLittleEndian) {
//			ifdOffset = value1 + (value2 << 8) + (value3 << 16) + (value4 << 24);
//		} else {
//			ifdOffset = value4 + (value3 << 8) + (value2 << 16) + (value1 << 24);
//		}
//
//		return ifdOffset;
//	}
//	
//	int readUnsignedByte() {
//		
////		int value = tiffFileDataBytes[tiffFileDataIndex++];
////		
////		if (value < 0) {
////			value = 127 - value;
////		}
////		
////		return value;
//		
//		return 0xFF & tiffFileDataBytes[tiffFileDataIndex++];
//		
//	}
//
//	/**
//	 * @param args
//	 */
////	public static void main(String[] args) {
////	}

}
