/**
 * University of Illinois/NCSA Open Source License
 * 
 * Copyright (c) 2010, Board of Trustees-University of Illinois. All rights reserved.
 * 
 * Developed by:
 * 
 * David Tcheng Automated Learning Group National Center for Supercomputing Applications
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal with the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to
 * do so, subject to the following conditions:
 * 
 * * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimers.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimers in the documentation and/or other materials provided with the
 * distribution.
 * 
 * * Neither the names of Automated Learning Group, The National Center for Supercomputing Applications, or University of Illinois, nor the names of its contributors may be used to endorse or promote
 * products derived from this Software without specific prior written permission.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 */
package adapt;

import ij.IJ;
import ij.ImagePlus;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.Serializable;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;

public class ADAPT extends Thread {

	String version = "1.38";

	boolean normalizeSpectralPowerOfImage = false;
	boolean useFileSeparatorForClassNamePathKeys = false;

	double zeroDistanceSubstituteDistance = 0.000001;

	boolean testForNaNs = true;

	boolean verboseCreateExample = true;

	boolean useExampleSetCache = false;
	boolean useExampleInputCache = false;

	String floatTypeString = "double";

	String author = "David Tcheng";
	String bestVM = "ibm-java-x86_64-60";
	String lastModified = "5/31/2010";

	boolean groupIndexIsDirectory = false;
	boolean groupIndexIsFile = true;

	boolean computeMinNumGroupsPerClass = false;

	boolean useNaturalTrainingSets = true;
	boolean useEqualSizedTrainingSets = false;

	boolean useRankNormalization = false;

	boolean reportGoodClasses = false;

	boolean solvingExampleCreationProblems = false;
	boolean solvingSupervisedLearningProblems = false;

	boolean useGCForScan = true;
	boolean useGCForExampleCreation = false;

	boolean reportBadExampleAccuracies = false;
	double badExampleAccuracyThreshold = 0.0;
	boolean reportExampleAccuracies = false;
	boolean reportExampleAveragePredictionPDF = true;
	boolean reportClassificationMatrix = true;
	boolean doInitialImageChecks = true;
	boolean deleteBadFiles = false;

	public int numProblemsToCreate = -1;
	public int numLearningProblemsToCreate = -1;
	public int reportIntervalInProblems = 1;

	public double subwindowSizeFactor = 1.0;

	// PROBLEM SPACE //

	public int textureXYResolution = 64;
	public int minShapeXYResolution = -1;
	public int maxXResolution = -1;
	public int maxYResolution = -1;
	int sampleInterval = 32;

	// public int exampleNumClasses = -1;

	public int minNumClassesToUse;
	public int maxNumClassesToUse;
	public int numNumClassesToUse;

	public int minNumTrainingGroupsLog2;
	public int maxNumTrainingGroupsLog2;
	public int numNumTrainingGroupsLog2;

	public int minNumTrainingExamplesPerClassLog2;
	public int maxNumTrainingExamplesPerClassLog2;
	public int numNumTrainingExamplesPerClassLog2;

	public int numTestingGroups;
	public int numTestingExamplesPerClass;

	boolean exampleGenerationUseMaxNumExamples = false;
	int exampleGenerationMaxNumExamples = 1;

	boolean exampleGenerationUseMaxNumExamplesPerClass;
	int exampleGenerationMaxNumExamplesPerClass = 1;

	int minNumExamplesForActiveClass = 1;

	public int problemGenerationRandomSeed;

	//

	//

	public int numColorsToUse = -1;
	public int[] colorIndicesToUse = new int[] { 0, 1, 2 };
	public int maxWidth = -1;
	public int maxHeight = -1;

	public final int sleepTimeInMS = 10;
	public final int monitorSleepTimeInMS = 10;

	private boolean verbose = true;

	Monitor monitor = null;
	long startTimeInMS = -1;
	long stopTimeInMS = -1;
	long runTimeInMS = -1;

	// example set

	ArrayList<String> classNames = new ArrayList<String>();
	ArrayList<ArrayList<String>> classNamePathKeys = new ArrayList<ArrayList<String>>();

	String dataDirectoryPath = null;
	String imageDirectoryPath = null;

	int maxNumSamples = -1;

	Example examples[] = null;
	int numExamples;
	int maxNumExamples;
	int numGroups;
	String[] groupNames;

	double[][] exampleValues;
	// double[] exampleInputValues;
	// double[] exampleOutputValues;

	ArrayList<Experience> experiences = null;
	int numProblems;
	ProblemSolver[] problemSolvers = null;
	int nextProblemIndex = -1;
	int numThreads = 1;

	// BIAS SPACE //

	double distanceWeightingPower;

	double overallIntensityDistributionWeight = (double) 0.0;
	int numIntensityDistributionFeatures = -1; // !!! not used ???
	int minNumIntensityBins = -1;
	int maxNumIntensityBins = -1;
	int numNumIntensityBins = -1;
	int[] intensityDistributionFeatureIndices = null;
	double[] intensityDistributionFeatureWeights = null;

	double overallGrossShapeWeight = (double) 0.0;
	// int numGrossShapeFeatures = -1;
	int minGrossShapeResolution = -1;
	int maxGrossShapeResolution = -1;
	int numGrossShapeResolutions = -1;
	int[] grossShapeFeatureIndices = null;
	double[] grossShapeFeatureWeights = null;

	double overallTextureWeight = (double) 0.0;
	int minTextureLineNumPixels = -1;
	int maxTextureLineNumPixels = -1;
	int numTextureLineNumPixels = -1;
	int[] textureLineFeatureIndices = null;
	double[] textureLineFeatureWeights = null;

	int numInputs = -1;
	int numOutputs = -1;
	String inputNames[] = null;
	String outputNames[] = null;
	String hostname;

	ADAPT() {

		System.out.println("Initializing ADAPT");

		if (false) {
			System.out.println(splashString());
		}
		try {
			hostname = java.net.InetAddress.getLocalHost().getHostName();
			System.out.println("hostname = " + hostname);
		} catch (UnknownHostException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		if (hostname.equals("dtcheng-laptop"))
			numThreads = 1;
		if (hostname.equals("trips"))
			numThreads = 2;
		if (hostname.equals("adapt"))
			numThreads = 8;
		if (hostname.equals("zipongo"))
			numThreads = 8;

	}

	public String splashString() {

		String string = "";

		string += "ADAPT (Advanced Discovery Analysis and Prediction Tool)\n";
		string += "Version: " + version + "\n";
		string += "Last Modified: " + lastModified + "\n";
		string += "Contributors: " + author + "\n";

		return string;
	}

	public void run() {

		startTimeInMS = System.currentTimeMillis();

		System.out.println(splashString());

		monitor = new Monitor(this);

		monitor.start();

	}

	public void terminate() {

		monitor.stopNow = true;

		while (true) {

			if (monitor.terminated)
				break;

			try {
				Thread.sleep(monitorSleepTimeInMS);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		stopTimeInMS = System.currentTimeMillis();

		runTimeInMS = stopTimeInMS - startTimeInMS;

		System.out.println("runTimeInMS: " + runTimeInMS);
		System.out.println("ADAPT terminated");

	}

	void readClassNamesFromCSVFile() {
		classNames = new ArrayList<String>();
		classNamePathKeys = new ArrayList<ArrayList<String>>();
		try {
			SimpleTable simpleTable = IO.readDelimitedTable(dataDirectoryPath + "TargetClasses.xls", "\t");
			String[][] matrix = simpleTable.stringMatrix;
			for (int rowIndex = 0; rowIndex < matrix.length; rowIndex++) {

				String className = matrix[rowIndex][0];
				String classNamePathKey = matrix[rowIndex][1];

				if (!classNames.contains(className)) {
					classNames.add(className);
					classNamePathKeys.add(new ArrayList<String>());
				}

				int classIndex = classNames.indexOf(className);
				classNamePathKeys.get(classIndex).add(classNamePathKey);

			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void createExamples() {

		solvingExampleCreationProblems = true;
		createImageExamples();
		solvingExampleCreationProblems = false;

	}

	int[] intBufferCache = null;

	void createExample(Experience experience, double[][] samples, double[][][] xycValues, double[][][] shapeValueSums, int[][][] shapeValueCounts, double[][][] standardXYCValues,
			double[][][] grossShapeValueSums, int[][][] grossShapeValueCounts, int[] pixelLineBinCounts) {

		if (useGCForExampleCreation)
			System.gc();

		int experienceIndex = experience.overallIndex;

		if (verboseCreateExample)
			System.out.println("creating example for exampleIndex:" + experienceIndex);

		Example example = (Example) examples[experienceIndex];
		double[] inputValues = example.inputValues;

		String imageFilePath = example.filePath;

		// String exampleImagePath = imageFilePath + ".image.ser";
		String exampleInputPath = imageFilePath + ".inputs.example.ser";

		if (!useExampleInputCache || !IO.fileExists(exampleInputPath)) {

			int[] intBuffer = null;

			// if (!useImageCache || !IO.fileExists(exampleImagePath)) {

			ImagePlus imagePlus = IJ.openImage(imageFilePath); // !!! todo
			// PlanarImage image = JAI.create("fileload", imageFilePath);

			BufferedImage bufferedImage = null;
			try {
				bufferedImage = imagePlus.getBufferedImage();

				// bufferedImage = image.getAsBufferedImage();
				// image.dispose();

				example.width = bufferedImage.getWidth();
				example.height = bufferedImage.getHeight();
				if (verboseCreateExample) {
					System.out.println("example.width: " + example.width + "  example.height: " + example.height);
				}

				int numPixels = example.width * example.height;

				if (intBufferCache == null || intBufferCache.length < numPixels) {
					intBufferCache = new int[numPixels];
				}

				intBuffer = intBufferCache;

				intBuffer = bufferedImage.getRGB(0, 0, example.width, example.height, intBuffer, 0, bufferedImage.getWidth());

				// if (useImageCache) {
				// IO.writeObject(exampleImagePath + ".incomplete.ser", intBuffer);
				// IO.rename(exampleImagePath + ".incomplete.ser", exampleImagePath);
				// }

			} catch (Exception e) {
				e.printStackTrace();
				System.exit(1);
			}
			// } else {
			// intBuffer = (int[]) IO.readObject(exampleImagePath);
			// }

			int index = 0;

			for (int y = 0; y < example.height; y++) {
				for (int x = 0; x < example.width; x++) {

					// int value = bufferedImage.getRGB(x + xOffset, y +
					// yOffset);

					int value = intBuffer[index++];

					for (int c = 0; c < numColorsToUse; c++) {

						switch (c) {
						case 0:
							xycValues[x][y][c] = ((value & 0x00ff0000) >> 16);
							break;
						case 1:
							xycValues[x][y][c] = ((value & 0x0000ff00) >> 8);
							break;
						case 2:
							xycValues[x][y][c] = ((value & 0x000000ff) >> 0);
							break;
						}

					}

				}
			}

			// System.out.println("###dump start###");
			// System.out.println(imageFilePath);
			// System.out.println(example.height);
			// System.out.println(example.width);
			// System.out.println(numColorsToUse);
			// for (int y = 0; y < example.height; y++) {
			// for (int x = 0; x < example.width; x++) {
			//
			// for (int c = 0; c < numColorsToUse; c++) {
			// System.out.println(xycValues[x][y][c]);
			//
			// }
			//
			// }
			// }
			// System.out.println("###dump end###");

			/*************************************/
			/* Normalize Spectral Power of Image */
			/*************************************/

			if (normalizeSpectralPowerOfImage) {
				double sum = (double) 0.0;
				long count = 0;
				for (int x = 0; x < example.width; x++) {
					for (int y = 0; y < example.height; y++) {
						for (int c = 0; c < numColorsToUse; c++) {
							sum += xycValues[x][y][c];
							count++;
						}
					}
				}

				Double factor = (sum / count);
				for (int x = 0; x < example.width; x++) {
					for (int y = 0; y < example.height; y++) {
						for (int c = 0; c < numColorsToUse; c++) {
							xycValues[x][y][c] /= factor;
						}
					}
				}

			}

			for (int x = 0; x < minShapeXYResolution; x++) {
				for (int y = 0; y < minShapeXYResolution; y++) {
					for (int c = 0; c < numColorsToUse; c++) {
						shapeValueSums[x][y][c] = 0;
						shapeValueCounts[x][y][c] = 0;
					}
				}
			}

			for (int x = 0; x < example.width; x++) {
				for (int y = 0; y < example.height; y++) {
					int xBin = minShapeXYResolution * x / example.width;
					int yBin = minShapeXYResolution * y / example.height;
					for (int c = 0; c < numColorsToUse; c++) {
						shapeValueSums[xBin][yBin][c] += xycValues[x][y][c];
						shapeValueCounts[xBin][yBin][c]++;
					}
				}
			}

			int sampleIndex = 0;
			int sampleCount = 0;

			for (int x = 0; x < textureXYResolution; x++) {
				for (int y = 0; y < textureXYResolution; y++) {

					for (int c = 0; c < numColorsToUse; c++) {

						double value = shapeValueSums[x][y][c] / shapeValueCounts[x][y][c];

						standardXYCValues[x][y][c] = value;

						if (sampleCount % sampleInterval == 0)
							samples[c][sampleIndex] = value;
					}

					if (sampleCount % sampleInterval == 0)
						sampleIndex++;

					sampleCount++;
				}
			}

			int inputFeatureIndex = 0;

			for (int numIntensityBins = minNumIntensityBins; numIntensityBins <= maxNumIntensityBins; numIntensityBins++) {
				for (int colorIndex = 0; colorIndex < numColorsToUse; colorIndex++) {
					Arrays.sort(samples[colorIndex], 0, sampleIndex);
					for (int j = 0; j < numIntensityBins - 1; j++) {

						double value = samples[colorIndex][(int) (((double) (j + 1) / (double) numIntensityBins) * sampleIndex)];

						if (!Double.isNaN(value)) {
							inputValues[inputFeatureIndex++] = value;
						} else {
							System.exit(1);
						}
					}
				}
			}

			for (int grossShapeResolution = minGrossShapeResolution; grossShapeResolution <= maxGrossShapeResolution; grossShapeResolution++) {

				for (int x = 0; x < grossShapeResolution; x++) {
					for (int y = 0; y < grossShapeResolution; y++) {
						for (int c = 0; c < numColorsToUse; c++) {
							grossShapeValueSums[x][y][c] = 0;
							grossShapeValueCounts[x][y][c] = 0;
						}
					}
				}
				for (int x = 0; x < example.width; x++) {
					for (int y = 0; y < example.height; y++) {
						for (int c = 0; c < numColorsToUse; c++) {

							int xBin = grossShapeResolution * x / example.width;
							int yBin = grossShapeResolution * y / example.height;

							grossShapeValueSums[xBin][yBin][c] += xycValues[x][y][c];
							grossShapeValueCounts[xBin][yBin][c]++;

						}
					}

				}

				for (int x = 0; x < grossShapeResolution; x++) {
					for (int y = 0; y < grossShapeResolution; y++) {
						for (int c = 0; c < numColorsToUse; c++) {
							inputValues[inputFeatureIndex++] = grossShapeValueSums[x][y][c] / grossShapeValueCounts[x][y][c];
						}
					}
				}
			}

			for (int textureLineNumPixels = minTextureLineNumPixels; textureLineNumPixels <= maxTextureLineNumPixels; textureLineNumPixels++) {

				for (int c = 0; c < numColorsToUse; c++) {

					int textureLineNumPixelsMinusOne = textureLineNumPixels - 1;
					int textureLineNumValueBins = (2 << textureLineNumPixelsMinusOne);

					for (int k = 0; k < textureLineNumValueBins; k++) {
						pixelLineBinCounts[k] = 0;
					}

					int numTextureLines = 0;
					for (int x = 0; x < textureXYResolution; x++) {

						for (int y = 0; y < textureXYResolution - textureLineNumPixelsMinusOne; y++) {

							{
								int binIndex = 0;
								int yStart = y;
								int yEnd = y + textureLineNumPixelsMinusOne;
								for (int pixelY = yStart; pixelY < yEnd; pixelY++) {
									if (standardXYCValues[x][pixelY + 1][c] - standardXYCValues[x][pixelY][c] < 0) {
										binIndex = binIndex * 2;
									} else {
										binIndex = binIndex * 2 + 1;
									}
								}
								pixelLineBinCounts[binIndex]++;
								numTextureLines++;
							}

							{
								int binIndex = 0;
								int yStart = y + textureLineNumPixelsMinusOne;
								int yEnd = y;
								for (int pixelY = yStart; pixelY > yEnd; pixelY--) {
									if (standardXYCValues[x][pixelY - 1][c] - standardXYCValues[x][pixelY][c] < 0) {
										binIndex = binIndex * 2;
									} else {
										binIndex = binIndex * 2 + 1;
									}
								}
								pixelLineBinCounts[binIndex]++;
								numTextureLines++;
							}
						}
					}

					for (int x = 0; x < textureXYResolution - textureLineNumPixelsMinusOne; x++) {

						for (int y = 0; y < textureXYResolution; y++) {

							{
								int binIndex = 0;
								int xStart = x;
								int xEnd = x + textureLineNumPixelsMinusOne;
								for (int pixelX = xStart; pixelX < xEnd; pixelX++) {
									if (standardXYCValues[pixelX + 1][y][c] - standardXYCValues[pixelX][y][c] < 0) {
										binIndex = binIndex * 2;
									} else {
										binIndex = binIndex * 2 + 1;
									}
								}
								pixelLineBinCounts[binIndex]++;
								numTextureLines++;
							}
							{
								int binIndex = 0;
								int xStart = x + textureLineNumPixelsMinusOne;
								int xEnd = x;
								for (int pixelX = xStart; pixelX > xEnd; pixelX--) {
									if (standardXYCValues[pixelX - 1][y][c] - standardXYCValues[pixelX][y][c] < 0) {
										binIndex = binIndex * 2;
									} else {
										binIndex = binIndex * 2 + 1;
									}
								}
								pixelLineBinCounts[binIndex]++;
								numTextureLines++;
							}

						}
					}
					for (int pixelIndex = 0; pixelIndex < textureLineNumValueBins; pixelIndex++) {
						inputValues[inputFeatureIndex++] = (double) pixelLineBinCounts[pixelIndex] / numTextureLines;
					}
				}

			}

			boolean nanInputValues = false;
			if (testForNaNs) {
				for (int j = 0; j < inputValues.length; j++) {
					if (Double.isNaN(inputValues[j]))
						nanInputValues = true;
				}
			}

			if (nanInputValues) {
				fail("nanInputValues experienceIndex = " + experienceIndex + "  example.filePath = " + example.filePath);
			} else {
				IO.writeObject(exampleInputPath + ".incomplete.ser", example.inputValues);
				IO.rename(exampleInputPath + ".incomplete.ser", exampleInputPath);
			}

		} else {
			inputValues = (double[]) IO.readObject(exampleInputPath);

			boolean nanInputValues = false;
			if (testForNaNs) {
				for (int j = 0; j < inputValues.length; j++) {
					if (Double.isNaN(inputValues[j]))
						nanInputValues = true;
				}
			}

			if (nanInputValues) {
				fail("nanInputValues experienceIndex = " + experienceIndex + "  example.filePath = " + example.filePath);
			} else {
				examples[experienceIndex].inputValues = inputValues;
			}
		}

		// IO.writeObject(exampleObjectPath + ".incomplete.ser", example);
		// IO.rename(exampleObjectPath + ".incomplete.ser", exampleObjectPath);
		// }
		//
		// else {
		// examples[i] = (Example) IO.readObject(exampleObjectPath);
		// }

		experience.result.complete = true;
	}

	public void warn(String message) {

		System.out.println("Warning!  " + message);
		// new Exception().printStackTrace();

	}

	public void fail(String message) {

		System.out.println("Error!" + message);
		new Exception().printStackTrace();
		System.exit(1);

	}

	public void createImageExamples() {

		/************************/
		/* DEFINE FEATURE SPACE */
		/************************/

		ArrayList<String> inputFeatureNamesArrayList = new ArrayList<String>();

		int inputFeatureIndex = 0;
		int startInputFeatureIndex = 0;

		startInputFeatureIndex = inputFeatureIndex;
		for (int numIntensityBins = minNumIntensityBins, i = 0; numIntensityBins <= maxNumIntensityBins; numIntensityBins++, i++) {
			intensityDistributionFeatureIndices[i] = inputFeatureIndex;
			for (int c = 0; c < numColorsToUse; c++) {
				for (int j = 0; j < numIntensityBins - 1; j++) {
					inputFeatureNamesArrayList.add("intensity:" + numIntensityBins + ":" + c + ":" + j);
					inputFeatureIndex++;
				}
			}
			inputFeatureNamesArrayList.add("intensity:" + numIntensityBins);
		}
		numIntensityDistributionFeatures = inputFeatureIndex - startInputFeatureIndex;

		startInputFeatureIndex = inputFeatureIndex;
		for (int grossShapeResolution = minGrossShapeResolution, i = 0; grossShapeResolution <= maxGrossShapeResolution; grossShapeResolution++, i++) {
			grossShapeFeatureIndices[i] = inputFeatureIndex;
			for (int x = 0; x < grossShapeResolution; x++) {
				for (int y = 0; y < grossShapeResolution; y++) {
					for (int c = 0; c < numColorsToUse; c++) {
						inputFeatureNamesArrayList.add("shape:" + grossShapeResolution + ":" + x + ":" + y + ":" + c);
						inputFeatureIndex++;
					}
				}
			}
			inputFeatureNamesArrayList.add("shape:" + grossShapeResolution);
		}

		startInputFeatureIndex = inputFeatureIndex;
		for (int textureLineNumPixels = minTextureLineNumPixels, i = 0; textureLineNumPixels <= maxTextureLineNumPixels; textureLineNumPixels++, i++) {
			textureLineFeatureIndices[i] = inputFeatureIndex;

			for (int c = 0; c < numColorsToUse; c++) {

				int textureLineNumPixelsMinusOne = textureLineNumPixels - 1;
				int textureLineNumValueBins = (2 << textureLineNumPixelsMinusOne);
				for (int pixelIndex = 0; pixelIndex < textureLineNumValueBins; pixelIndex++) {
					inputFeatureNamesArrayList.add("texture:" + textureLineNumPixels + ":" + c + ":" + pixelIndex);
					inputFeatureIndex++;
				}
			}
			inputFeatureNamesArrayList.add("texture:" + textureLineNumPixels);
		}

		numInputs = inputFeatureIndex;
		numOutputs = classNames.size();

		System.out.println("numInputs = " + numInputs);
		System.out.println("numOutputs = " + numOutputs);

		/*************************************************/
		/* analyze image directory and allocate examples */
		/*************************************************/

		//

		String[] imagePathStringsInDirectory = IO.getPathStrings(imageDirectoryPath);

		ArrayList<String> imagePathStrings = new ArrayList<String>();

		for (int i = 0; i < imagePathStringsInDirectory.length; i++) {

			String filePath = imagePathStringsInDirectory[i];

			if (!filePath.endsWith(".tif") && !filePath.endsWith(".tiff") && !filePath.endsWith(".TIF") && !filePath.endsWith(".jpg") && !filePath.endsWith(".JPG") && !filePath.endsWith(".jpeg")
					&& !filePath.endsWith(".JPEG"))
				continue;

			imagePathStrings.add(filePath);
		}

		int numImagePaths = imagePathStrings.size();
		System.out.println("numImagePaths = " + numImagePaths);

		String[] randomizedImagePathStringsInDirectory = new String[numImagePaths];
		int[] randomIndicies = Utility.randomIntArray(123, numImagePaths);
		for (int i = 0; i < numImagePaths; i++) {
			randomizedImagePathStringsInDirectory[i] = imagePathStrings.get(randomIndicies[i]);
		}
		imagePathStringsInDirectory = randomizedImagePathStringsInDirectory;

		System.out.println("exampleGenerationUseMaxNumExamples = " + exampleGenerationUseMaxNumExamples);
		if (exampleGenerationUseMaxNumExamples) {

			String[] randomSubsetPathStringsInDirectory = new String[exampleGenerationMaxNumExamples];
			for (int i = 0; i < exampleGenerationMaxNumExamples; i++) {
				randomSubsetPathStringsInDirectory[i] = imagePathStringsInDirectory[i];
			}

			imagePathStringsInDirectory = randomSubsetPathStringsInDirectory;
			numImagePaths = exampleGenerationMaxNumExamples;
		}

		HashMap<String, Example> nameToExample = new HashMap<String, Example>();
		HashMap<Integer, Example> indexToExample = new HashMap<Integer, Example>();

		HashMap<String, Integer> directoryPathToGroupIndex = new HashMap<String, Integer>();
		HashMap<String, Integer> filePathToGroupIndex = new HashMap<String, Integer>();

		System.out.println("groupIndexIsDirectory = " + groupIndexIsDirectory);
		System.out.println("groupIndexIsFile = " + groupIndexIsFile);
		if (groupIndexIsDirectory && groupIndexIsFile) {

			System.out.println("Error!  (groupIndexIsDirectory && groupIndexIsFile)");

			System.exit(1);
		}

		int exampleIndex = 0;
		int exampleGroupIndex = 0;
		int[] classExampleCount = new int[numOutputs];
		for (int i = 0; i < imagePathStringsInDirectory.length; i++) {

			String filePath = imagePathStringsInDirectory[i];

			String exampleFilePath = filePath;

			String imageDirectoryPath = null;
			String[] parts = filePath.split(File.separator);
			imageDirectoryPath = parts[parts.length - 2];

			Integer groupID = null;

			if (groupIndexIsDirectory) {
				groupID = directoryPathToGroupIndex.get(imageDirectoryPath);
				if (groupID == null) {
					groupID = exampleGroupIndex;
					directoryPathToGroupIndex.put(imageDirectoryPath, exampleGroupIndex);
					exampleGroupIndex++;
				}
			} else if (groupIndexIsFile) {
				groupID = filePathToGroupIndex.get(filePath);
				if (groupID == null) {
					groupID = exampleGroupIndex;
					directoryPathToGroupIndex.put(imageDirectoryPath, exampleGroupIndex);
					exampleGroupIndex++;
				}
			}

			Example example = null;

			double[] exampleInputs = new double[numInputs];
			double[] exampleOutputs = new double[numOutputs];

			int firstHitOutputIndex = -1;

			int outputHits[] = new int[numOutputs];
			for (int outputIndex = 0; outputIndex < numOutputs; outputIndex++) {

				for (int outputNameAlternativeIndex = 0; outputNameAlternativeIndex < classNamePathKeys.get(outputIndex).size(); outputNameAlternativeIndex++) {

					String key = null;

					if (useFileSeparatorForClassNamePathKeys) {
						key = File.separator + classNamePathKeys.get(outputIndex).get(outputNameAlternativeIndex) + File.separator;
					} else {
						key = classNamePathKeys.get(outputIndex).get(outputNameAlternativeIndex);
					}

					// System.out.println("filePath = " + filePath);
					// System.out.println("key = " + key);

					if (exampleFilePath.contains(key)) {

						exampleOutputs[outputIndex] = (double) 1.0;

						if (outputHits[outputIndex] == 0) {
							firstHitOutputIndex = outputIndex;
						}
						outputHits[outputIndex]++;
					}
				}
			}

			int numOutputsHit = 0;
			for (int outputIndex = 0; outputIndex < numOutputs; outputIndex++) {

				if (outputHits[outputIndex] > 0)
					numOutputsHit++;
			}

			if (numOutputsHit != 1) {
				warn("(numOutputsHit != 1) filePath = " + filePath + "numOutputsHit = " + numOutputsHit);
				continue;
			}

			if (exampleGenerationUseMaxNumExamplesPerClass) {

				if (classExampleCount[firstHitOutputIndex] >= exampleGenerationMaxNumExamplesPerClass)
					continue;

				classExampleCount[firstHitOutputIndex]++;

			}

			example = new Example(exampleFilePath, exampleIndex, groupID, exampleInputs, exampleOutputs);
			example.filePath = filePath;

			nameToExample.put(exampleFilePath, example);
			indexToExample.put(exampleIndex, example);

			exampleIndex++;

		}

		numExamples = nameToExample.size();
		examples = new Example[numExamples];
		for (int i = 0; i < numExamples; i++) {
			examples[i] = indexToExample.get(i);
		}
		maxNumExamples = numExamples;

		Clock clock = new Clock();

		/**********************************/
		/* MAKE 1ST PASS SCAN OF EXAMPLES */
		/**********************************/

		if (doInitialImageChecks) {

			createImageScanProblems();
			solveProblems();

			int minSize = Integer.MAX_VALUE;

			ArrayList<Example> goodExamplesArrayList = new ArrayList<Example>();

			for (int i = 0; i < numExamples; i++) {

				Experience experience = experiences.get(i);
				Result result = experience.result;
				Example example = result.example;

				if (!result.failed) {
					int height = example.height;
					int width = example.width;
					minSize = Math.min(height, width);

					goodExamplesArrayList.add(example);
				}

			}
			System.out.println("minSize = " + minSize);

			int numGoodExamples = goodExamplesArrayList.size();

			Example[] goodExamples = new Example[numGoodExamples];
			goodExamplesArrayList.toArray(goodExamples);

			numExamples = numGoodExamples;
			examples = goodExamples;
		}

		System.out.println("numExamples = " + numExamples);
		System.out.println("minXYResolution = " + minShapeXYResolution);

		/*******************/
		/* CREATE EXAMPLES */
		/*******************/

		maxNumSamples = maxXResolution * maxYResolution / sampleInterval;

		createExampleGenerationProblems();
		solveProblems();

		clock.report("final real time: ");

		// define outputs (classes)
		int numClasses = classNames.size();
		numOutputs = numClasses;
		outputNames = new String[numOutputs];
		for (int i = 0; i < outputNames.length; i++) {

			outputNames[i] = classNames.get(i);

		}

		clock.report("final real time: ");
	}

	void scanImageFile(Experience experience) {

		int i = experience.overallIndex;

		boolean goodExample = true; // assumption

		if (useGCForScan)
			System.gc();

		System.out.println("scanning example index:" + i);

		Example example = (Example) examples[i];

		experience.result.example = example;

		String imageFilePath = example.filePath;
		String imageFileIntBufferPath = example.filePath + ".image.ser";

		if (IO.fileExists(imageFileIntBufferPath)) {

			experience.result.complete = true;

			return;
		}

		example.width = -1;
		example.height = -1;

		ImagePlus imagePlus = IJ.openImage(imageFilePath); // !!! todo
		// PlanarImage image = JAI.create("fileload", imageFilePath);

		BufferedImage bufferedImage = null;

		try {
			// image.getHeight();
			// image.getWidth();
			// example.width = (int) (image.getWidth() *
			// subwindowSizeFactor);
			// example.height = (int) (image.getHeight() *
			// subwindowSizeFactor);

			bufferedImage = imagePlus.getBufferedImage();
			// bufferedImage = image.getAsBufferedImage();

			example.width = (int) (bufferedImage.getWidth() * subwindowSizeFactor);
			example.height = (int) (bufferedImage.getHeight() * subwindowSizeFactor);

			System.out.println("example.width = " + example.width + "  example.height = " + example.height);

			if ((example.width < minShapeXYResolution) || (example.height < minShapeXYResolution)) {
				System.out.println("image too small:" + imageFilePath);
				if (deleteBadFiles)
					IO.delete(imageFilePath);
				goodExample = false;
			}
			if ((example.width > maxXResolution) || (example.height > maxYResolution)) {
				System.out.println("image too big:" + imageFilePath);
				if (deleteBadFiles)
					IO.delete(imageFilePath);
				goodExample = false;
			}

			// if (goodExample) {
			// if (example.width < minSize) {
			// minSize = example.width;
			// }
			// if (example.height < minSize) {
			// minSize = example.height;
			// }
			// }

		} catch (Exception e) {

			System.out.println("parsing error file:" + imageFilePath);
			if (deleteBadFiles) {
				System.out.println("deleting file:" + imageFilePath);
				IO.delete(imageFilePath);
			}

			// e.printStackTrace();
			// System.exit(1);
		}

		if (!goodExample) {
			// System.out.println("minSize = " + minSize);
			experience.result.failed = true;
		}

		experience.result.complete = true;

		// return minSize;
	}

	public void printExamples() {

		System.out.println("numExamples = " + numExamples);

		for (int i = 0; i < numExamples; i++) {

			Example example = examples[i];

			System.out.print(i);

			for (int j = 0; j < numInputs; j++) {

				System.out.print("\tI_" + inputNames[j] + ":" + example.inputValues[j]);
			}
			for (int j = 0; j < numOutputs; j++) {

				System.out.print("\tO_" + outputNames[j] + ":" + example.outputValues[j]);
			}

			System.out.print("\t" + example.name);

			System.out.println();
		}
	}

	public void saveExamples(String path) {
		IO.writeObject(path, new ExampleState(this));
	}

	public void loadExamples(String path) {

		ExampleState.clearExampleState(this);

		System.gc();

		ExampleState exampleState = (ExampleState) IO.readObject(path);

		exampleState.restoreExampleState(this);

		System.out.println("numExamples = " + numExamples);

	}

	public void createImageScanProblems() {

		numProblemsToCreate = numExamples;

		experiences = new ArrayList<Experience>(numProblemsToCreate);
		for (int overallProblemIndex = 0; overallProblemIndex < numProblemsToCreate; overallProblemIndex++) {

			Experience experience = new Experience(overallProblemIndex, true, false, false);

			experiences.add(experience);
		}

		numProblems = experiences.size();

	}

	public void createExampleGenerationProblems() {

		numProblemsToCreate = numExamples;

		experiences = new ArrayList<Experience>(numProblemsToCreate);

		for (int overallProblemIndex = 0; overallProblemIndex < numProblemsToCreate; overallProblemIndex++) {

			Experience experience = new Experience(overallProblemIndex, false, true, false);

			experiences.add(experience);
		}

		numProblems = experiences.size();

	}

	public void createLearningProblems() {

		numProblemsToCreate = numLearningProblemsToCreate;

		int numClasses = numOutputs;

		/******************************************/
		/* create list of examples for each group */
		/******************************************/

		ArrayList<Integer> groupIDs = new ArrayList<Integer>();

		int maxGroupID = Integer.MIN_VALUE;
		for (int exampleIndex = 0; exampleIndex < numExamples; exampleIndex++) {

			Example example = examples[exampleIndex];

			int groupID = example.groupID;

			if (!groupIDs.contains(groupID)) {
				groupIDs.add(groupID);
			}

			if (groupID > maxGroupID) {
				maxGroupID = groupID;
			}

		}

		numGroups = groupIDs.size();
		System.out.println("numGroups = " + numGroups);
		System.out.println("maxGroupID = " + maxGroupID);

		ArrayList<Integer>[] groupExampleIndices = new ArrayList[numGroups];
		for (int groupIndex = 0; groupIndex < numGroups; groupIndex++) {
			groupExampleIndices[groupIndex] = new ArrayList<Integer>();
		}

		groupNames = new String[numGroups];
		for (int exampleIndex = 0; exampleIndex < numExamples; exampleIndex++) {

			Example example = examples[exampleIndex];

			int groupIndex = groupIDs.indexOf(example.groupID);

			example.groupIndex = groupIndex;

			groupExampleIndices[groupIndex].add(exampleIndex);

			if (groupNames[groupIndex] == null) {
				groupNames[groupIndex] = example.name;
			}

		}

		if (false)
			for (int groupIndex = 0; groupIndex < numGroups; groupIndex++) {
				System.out.println("groupExampleIndices[" + groupIndex + "].size() = " + groupExampleIndices[groupIndex].size());
			}

		/***************************************/
		/* create list of groups in each class */
		/***************************************/

		int[] numGroupsInClass = new int[numClasses];
		ArrayList<Integer>[] classGroupIndices = new ArrayList[numClasses];
		for (int i = 0; i < numClasses; i++) {
			classGroupIndices[i] = new ArrayList<Integer>();
		}

		for (int groupIndex = 0; groupIndex < numGroups; groupIndex++) {

			// get 1st example in group
			Example example = examples[groupExampleIndices[groupIndex].get(0)];

			int classIndex = example.getClassIndex();

			numGroupsInClass[classIndex]++;

			classGroupIndices[classIndex].add(groupIndex);

		}

		if (computeMinNumGroupsPerClass) {
			minNumExamplesForActiveClass = Integer.MAX_VALUE;
			for (int i = 0; i < numClasses; i++) {
				if (numGroupsInClass[i] < minNumExamplesForActiveClass) {
					minNumExamplesForActiveClass = numGroupsInClass[i];
				}
			}
		}

		if (reportGoodClasses) {
			System.out.println("minNumGroupsPerClass = " + minNumExamplesForActiveClass);

			System.out.println("Good Classes:");
		}

		ArrayList<Integer> activeClassIndices = new ArrayList<Integer>();
		for (int i = 0; i < numClasses; i++) {
			if (numGroupsInClass[i] >= minNumExamplesForActiveClass) {

				if (reportGoodClasses) {
					System.out.println(outputNames[i] + " n = " + numGroupsInClass[i]);
				}
				activeClassIndices.add(i);
			}
		}

		int numActiveClassIndices = activeClassIndices.size();
		System.out.println("numActiveClassIndices = " + numActiveClassIndices);

		/*******************/
		/* create problems */
		/*******************/

		experiences = new ArrayList<Experience>(numLearningProblemsToCreate);

		Random randomGenerator = new Random(problemGenerationRandomSeed);

		int[] randomClassIndices = new int[numActiveClassIndices];

		int[] classExampleRandomIndices = new int[numExamples];

		int[] randomGroupIndices = new int[numGroups];

		if (minNumClassesToUse == -1) {
			minNumClassesToUse = numOutputs;
			maxNumClassesToUse = numOutputs;
			numNumClassesToUse = 1;
		}

		for (int overallProblemIndex = 0; overallProblemIndex < numProblemsToCreate; overallProblemIndex++) {

			Experience experience = new Experience(overallProblemIndex, false, false, true);

			/*******************************************************/
			/* decide how many classes to use (2 to maxNumClasses) */
			/*******************************************************/

			int numClassesToUse = minNumClassesToUse + (int) (randomGenerator.nextDouble() * numNumClassesToUse);

			experience.problem.numClassesToUse = numClassesToUse;

			/* select classes to use */

			ArrayList<Integer> classIndicesToUse = new ArrayList<Integer>(numClassesToUse);

			Utility.randomIntArray(randomGenerator, numActiveClassIndices, randomClassIndices);

			for (int i = 0; i < numClassesToUse; i++) {
				classIndicesToUse.add(activeClassIndices.get(randomClassIndices[i]));
			}

			experience.problem.classIndicesToUse = classIndicesToUse;

			/***********************************************/
			/* select training and testing examples to use */
			/***********************************************/

			if (useEqualSizedTrainingSets) {

				if (false) {
					int numTrainingExamplesPerClassLog2 = minNumTrainingExamplesPerClassLog2 + (int) (randomGenerator.nextDouble() * numNumTrainingExamplesPerClassLog2);
					int numTrainingExamplesPerClass = (1 << numTrainingExamplesPerClassLog2);

					experience.problem.numTrainingExamplesPerClassLog2 = numTrainingExamplesPerClassLog2;
					experience.problem.numTrainingExamplesPerClass = numTrainingExamplesPerClass;
					experience.problem.numTestingExamplesPerClass = numTestingExamplesPerClass;

					if (numTrainingExamplesPerClass + numTestingExamplesPerClass > minNumExamplesForActiveClass) {
						new Exception().printStackTrace();
						System.exit(1);
					}

					// select examples to use
					ArrayList<Integer> trainExampleIndices = new ArrayList<Integer>();
					ArrayList<Integer> testExampleIndices = new ArrayList<Integer>();

					for (int classToUseIndex = 0; classToUseIndex < numClassesToUse; classToUseIndex++) {

						int classIndex = classIndicesToUse.get(classToUseIndex);

						int classNumExamples = numGroupsInClass[classIndex];

						Utility.randomIntArray(randomGenerator, classNumExamples, classExampleRandomIndices);

						for (int classExampleIndex = 0; classExampleIndex < classNumExamples; classExampleIndex++) {

							if (classExampleIndex < numTrainingExamplesPerClass) {
								trainExampleIndices.add(classGroupIndices[classIndex].get(classExampleRandomIndices[classExampleIndex]));
							} else if (classExampleIndex < numTrainingExamplesPerClass + numTestingExamplesPerClass) {
								testExampleIndices.add(classGroupIndices[classIndex].get(classExampleRandomIndices[classExampleIndex]));
							}

						}

					}

					experience.problem.trainExampleIndices = trainExampleIndices;
					experience.problem.testExampleIndices = testExampleIndices;
					experience.problem.numTrainExamples = trainExampleIndices.size();
					experience.problem.numTestExamples = testExampleIndices.size();
				}
			}
			if (useNaturalTrainingSets) {

				int numTrainingGroupsLog2 = minNumTrainingGroupsLog2 + (int) (randomGenerator.nextDouble() * numNumTrainingGroupsLog2);

				int numTrainingGroups = -1;
				
				if ((minNumTrainingGroupsLog2 == -1) || ((1 << numTrainingGroupsLog2) + numTestingGroups > numGroups)) {
					numTrainingGroups = numGroups - numTestingGroups;
				} else {
					numTrainingGroups = (1 << numTrainingGroupsLog2);
				}

				experience.problem.numTrainingGroupsLog2 = numTrainingGroupsLog2;
				experience.problem.numTrainingGroups = numTrainingGroups;
				experience.problem.numTestingGroups = numTestingGroups;

				// select examples to use
				ArrayList<Integer> groupIndices = new ArrayList<Integer>();
				ArrayList<Integer> trainGroupIndices = new ArrayList<Integer>();
				ArrayList<Integer> testGroupIndices = new ArrayList<Integer>();
				ArrayList<Integer> trainExampleIndices = new ArrayList<Integer>();
				ArrayList<Integer> testExampleIndices = new ArrayList<Integer>();

				int totalNumExamplesNeeded = numTrainingGroups + numTestingGroups;

				/************************************************/
				/* compute number of examples in active classes */
				/************************************************/

				for (int classToUseIndex = 0; classToUseIndex < numClassesToUse; classToUseIndex++) {

					int classIndex = classIndicesToUse.get(classToUseIndex);

					int classNumGroups = numGroupsInClass[classIndex];

					for (int classGroupIndex = 0; classGroupIndex < classNumGroups; classGroupIndex++) {
						groupIndices.add(classGroupIndices[classIndex].get(classGroupIndex));
					}

				}

				int numGroupsInActiveClasses = groupIndices.size();

				if (numGroupsInActiveClasses < totalNumExamplesNeeded) {
					System.out.println("failed to create problem, trying again, overallProblemIndex = " + overallProblemIndex + "  numClassesToUse = " + numClassesToUse + "  numTrainingExamples = "
							+ numTrainingGroups);
					overallProblemIndex--;
					continue;
				}
				// System.out.println("succeeded creating problem");

				Utility.randomIntArray(randomGenerator, numGroupsInActiveClasses, randomGroupIndices);

				for (int exampleIndex = 0; exampleIndex < totalNumExamplesNeeded; exampleIndex++) {

					if (exampleIndex < numTrainingGroups) {
						trainGroupIndices.add(groupIndices.get(randomGroupIndices[exampleIndex]));
					}

					else {
						testGroupIndices.add(groupIndices.get(randomGroupIndices[exampleIndex]));
					}

				}

				for (Iterator iterator = trainGroupIndices.iterator(); iterator.hasNext();) {
					Integer groupIndex = (Integer) iterator.next();
					for (Iterator iterator2 = groupExampleIndices[groupIndex].iterator(); iterator2.hasNext();) {
						Integer exampleIndex = (Integer) iterator2.next();
						trainExampleIndices.add(exampleIndex);
					}
				}
				for (Iterator iterator = testGroupIndices.iterator(); iterator.hasNext();) {
					Integer groupIndex = (Integer) iterator.next();
					for (Iterator iterator2 = groupExampleIndices[groupIndex].iterator(); iterator2.hasNext();) {
						Integer exampleIndex = (Integer) iterator2.next();
						testExampleIndices.add(exampleIndex);
					}
				}

				experience.problem.trainGroupIndices = trainGroupIndices;
				experience.problem.testGroupIndices = testGroupIndices;

				experience.problem.trainExampleIndices = trainExampleIndices;
				experience.problem.testExampleIndices = testExampleIndices;
				experience.problem.numTrainExamples = trainExampleIndices.size();
				experience.problem.numTestExamples = testExampleIndices.size();

				// System.out.println("experience.problem.numTrainExamples = " +
				// experience.problem.numTrainExamples);
				// System.out.println("experience.problem.numTestExamples = " +
				// experience.problem.numTestExamples);
			}

			experiences.add(experience);
		}

		numProblems = experiences.size();

	}

	public void solveSupervisedLearningProblems() {
		solvingSupervisedLearningProblems = true;
		solveProblems();
		solvingSupervisedLearningProblems = false;
	}

	public void solveProblems() {

		nextProblemIndex = 0;

		/************************/
		/* start worker threads */
		/************************/

		problemSolvers = new ProblemSolver[getNumThreads()];
		for (int i = 0; i < getNumThreads(); i++) {
			problemSolvers[i] = new ProblemSolver(this, i);
			problemSolvers[i].start();
		}

		/*************************************************/
		/* wait until all worker threads have terminated */
		/*************************************************/
		int nextReportNumProblemsSolved = reportIntervalInProblems;

		Clock clock = new Clock();
		while (true) {

			// boolean allDone = true;
			//
			// for (int i = 0; i < getNumThreads(); i++) {
			// if (!problemSolvers[i].terminated) {
			// allDone = false;
			// break;
			// }
			// }
			//
			// if (allDone)
			// break;

			int numProblemsSolved = countNumContiguousProblemsSolved();

			if (numProblemsSolved == numProblemsToCreate) {
				break;
			}

			if (solvingSupervisedLearningProblems) {
				if (numProblemsSolved >= nextReportNumProblemsSolved) {
					analyzeCurrentPerformance(nextReportNumProblemsSolved);

					double problemsPerSecond = nextReportNumProblemsSolved / clock.getTime();
					double examplesClassifiedPerSecond = problemsPerSecond * numTestingGroups;

					System.out.println("problemsPerSecond = " + problemsPerSecond);
					System.out.println("examplesClassifiedPerSecond = " + examplesClassifiedPerSecond);

					nextReportNumProblemsSolved += reportIntervalInProblems;
				}
			}

			try {
				Thread.sleep(sleepTimeInMS);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (solvingSupervisedLearningProblems)
			analyzeCurrentPerformance(numProblemsToCreate);
	}

	int countNumContiguousProblemsSolved() {

		int numProblemsSolved = 0;
		for (Iterator<Experience> iterator = experiences.iterator(); iterator.hasNext();) {

			Experience experience = (Experience) iterator.next();

			if (experience.result.complete) {
				numProblemsSolved++;
			} else {
				break;
			}
		}

		return numProblemsSolved;
	}

	void analyzeCurrentPerformance(int numFinishedProblems) {

		int[][] accuracyByNumClassesToUseByTrainingSetSizeCounts = null;
		double[][] accuracyByNumClassesToUseByTrainingSetSizeSums = null;
		int[][] menuDistanceByNumClassesToUseByTrainingSetSizeCounts = null;
		double[][] menuDistanceByNumClassesToUseByTrainingSetSizeSums = null;

		// int[][] classificationMatrix = new
		// int[exampleNumClasses][exampleNumClasses];
		int[][] classificationMatrix = new int[numOutputs][numOutputs];
		int[] exampleCorrectClassificationCounts = new int[numExamples];
		int[] exampleTotalClassificationCounts = new int[numExamples];

		int[][][] menuDistributionCounts = null;

		if (useEqualSizedTrainingSets) {

			accuracyByNumClassesToUseByTrainingSetSizeCounts = new int[numNumClassesToUse][numNumTrainingExamplesPerClassLog2];
			accuracyByNumClassesToUseByTrainingSetSizeSums = new double[numNumClassesToUse][numNumTrainingExamplesPerClassLog2];
			menuDistanceByNumClassesToUseByTrainingSetSizeCounts = new int[numNumClassesToUse][numNumTrainingExamplesPerClassLog2];
			menuDistanceByNumClassesToUseByTrainingSetSizeSums = new double[numNumClassesToUse][numNumTrainingExamplesPerClassLog2];

			menuDistributionCounts = new int[numOutputs][numNumClassesToUse][numNumTrainingExamplesPerClassLog2];
		}

		if (useNaturalTrainingSets) {

			accuracyByNumClassesToUseByTrainingSetSizeCounts = new int[numNumClassesToUse][numNumTrainingGroupsLog2];
			accuracyByNumClassesToUseByTrainingSetSizeSums = new double[numNumClassesToUse][numNumTrainingGroupsLog2];
			menuDistanceByNumClassesToUseByTrainingSetSizeCounts = new int[numNumClassesToUse][numNumTrainingGroupsLog2];
			menuDistanceByNumClassesToUseByTrainingSetSizeSums = new double[numNumClassesToUse][numNumTrainingGroupsLog2];

			menuDistributionCounts = new int[numOutputs][numNumClassesToUse][numNumTrainingGroupsLog2];
		}

		double[] menuDistribution = new double[numOutputs];

		for (int i = 0; i < numFinishedProblems; i++) {

			Experience experience = experiences.get(i);
			Problem problem = experience.problem;
			Result result = experience.result;
			Objective objective = experience.objective;

			int numClassesToUse = problem.numClassesToUse;
			double accuracy = objective.accuracy;
			double menuDistance = objective.menuDistance;
			int index1 = numClassesToUse - minNumClassesToUse;

			int index2 = -1;

			if (useEqualSizedTrainingSets) {
				int numTrainingExamplesPerClassLog2 = problem.numTrainingExamplesPerClassLog2;
				index2 = numTrainingExamplesPerClassLog2 - minNumTrainingExamplesPerClassLog2;
			}
			if (useNaturalTrainingSets) {
				int numTrainingExamplesLog2 = problem.numTrainingGroupsLog2;
				index2 = numTrainingExamplesLog2 - minNumTrainingGroupsLog2;
			}

			for (int j = 0; j < numOutputs; j++) {
				menuDistributionCounts[j][index1][index2] += objective.menuDistributionCounts[j];
			}

			accuracyByNumClassesToUseByTrainingSetSizeSums[index1][index2] += accuracy;
			accuracyByNumClassesToUseByTrainingSetSizeCounts[index1][index2]++;

			menuDistanceByNumClassesToUseByTrainingSetSizeSums[index1][index2] += menuDistance;
			menuDistanceByNumClassesToUseByTrainingSetSizeCounts[index1][index2]++;

			if (false) {
				for (int j = 0; j < problem.numTestExamples; j++) {
					int predictedClassIndex = result.testExamplePredictedClass.get(j);
					int actualClassIndex = result.testExampleActualClass.get(j);

					classificationMatrix[predictedClassIndex][actualClassIndex]++;

					int testingExampleIndex = problem.testExampleIndices.get(j);
					if (predictedClassIndex == actualClassIndex) {
						exampleCorrectClassificationCounts[testingExampleIndex]++;
					}
					exampleTotalClassificationCounts[testingExampleIndex]++;
				}
			}

			for (int proglemTestGroupIndex = 0; proglemTestGroupIndex < problem.testGroupIndices.size(); proglemTestGroupIndex++) {

				int predictedClassIndex = result.testGroupPredictedClass.get(proglemTestGroupIndex);
				int actualClassIndex = result.testGroupActualClass.get(proglemTestGroupIndex);

				classificationMatrix[predictedClassIndex][actualClassIndex]++;

				// int testingExampleIndex =
				// problem.testExampleIndices.get(proglemTestGroupIndex);
				// if (predictedClassIndex == actualClassIndex) {
				// exampleCorrectClassificationCounts[testingExampleIndex]++;
				// }
				// exampleTotalClassificationCounts[testingExampleIndex]++;
			}

		}

		if (reportBadExampleAccuracies) {

			for (int j = 0; j < numExamples; j++) {
				if (exampleTotalClassificationCounts[j] > 0) {
					double exampleAccuracy = (double) exampleCorrectClassificationCounts[j] / exampleTotalClassificationCounts[j];

					if (exampleAccuracy < badExampleAccuracyThreshold) {
						System.out.println(exampleTotalClassificationCounts[j] + "\t" + exampleAccuracy + "\t" + "firefox \"" + examples[j].name + "\"");

					}
				}
			}

		}

		if (reportExampleAveragePredictionPDF) {

			double[][] groupPDFSums = new double[numGroups][numOutputs];
			int[] groupPDFCounts = new int[numGroups];
			double[][] fossilClasses = new double[numGroups][2];
			double[] fossilConfidence = new double[numGroups];

			SimpleTable table = null;
			try {
				table = IO.readDelimitedTable("/d250/data/pollen/expertClassifications.csv", "\t");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			for (int groupIndex = 0; groupIndex < numGroups; groupIndex++) {

				String groupName = groupNames[groupIndex];

				boolean groupFossilClassFound = false;
				for (int i = 0; i < table.numDataRows; i++) {

					if (groupName.contains(table.getString(i, 0))) {

						if (groupName.contains("Pos " + table.getString(i, 2) + "/") || groupName.contains("Pos " + table.getString(i, 2) + " ")
								|| groupName.contains("Pos " + table.getString(i, 2) + "_")) {

							fossilConfidence[groupIndex] = table.getDouble(i, 4);

							if (table.getString(i, 3).equals("mariana")) {
								fossilClasses[groupIndex][0] = 1.0;
								fossilClasses[groupIndex][1] = 0.0;
								groupFossilClassFound = true;
								break;
							}

							if (table.getString(i, 3).equals("Mariana")) {
								fossilClasses[groupIndex][0] = 1.0;
								fossilClasses[groupIndex][1] = 0.0;
								groupFossilClassFound = true;
								break;
							}

							if (table.getString(i, 3).equals("glauca")) {
								fossilClasses[groupIndex][0] = 0.0;
								fossilClasses[groupIndex][1] = 1.0;
								groupFossilClassFound = true;
								break;
							}
							if (table.getString(i, 3).equals("Glauca")) {
								fossilClasses[groupIndex][0] = 0.0;
								fossilClasses[groupIndex][1] = 1.0;
								groupFossilClassFound = true;
								break;
							}

						}

					}

				}

			}

			for (int j = 0; j < numExamples; j++) {

				if (examples[j].numObservations > 0) {

					int groupIndex = examples[j].groupIndex;
					for (int k = 0; k < numOutputs; k++) {

						groupPDFSums[groupIndex][k] += (examples[j].outputValueSums[k] / examples[j].numObservations);

					}
					groupPDFCounts[groupIndex]++;

				}
			}

			double[][] fossilClassScores = new double[2][numOutputs];
			int[] fossilClassCounts = new int[2];
			for (int groupIndex = 0; groupIndex < numGroups; groupIndex++) {

				if (groupPDFCounts[groupIndex] > 0 && (fossilClasses[groupIndex][0] == 1.0 || fossilClasses[groupIndex][1] == 1.0)) {

					String groupName = groupNames[groupIndex];

					System.out.print(groupIndex + 1);
					for (int outputIndex = 0; outputIndex < numOutputs; outputIndex++) {
						System.out.print("\t" + groupPDFSums[groupIndex][outputIndex] / groupPDFCounts[groupIndex]);
					}
					for (int fossilClassIndex = 0; fossilClassIndex < 2; fossilClassIndex++) {

						if (fossilClasses[groupIndex][fossilClassIndex] == 1.0) {
							for (int outputClassIndex = 0; outputClassIndex < numOutputs; outputClassIndex++) {
								fossilClassScores[fossilClassIndex][outputClassIndex] += groupPDFSums[groupIndex][outputClassIndex] / groupPDFCounts[groupIndex];
							}
							fossilClassCounts[fossilClassIndex]++;
						}
					}
					for (int fossilClassIndex = 0; fossilClassIndex < 2; fossilClassIndex++) {
						System.out.print("\t" + fossilClasses[groupIndex][fossilClassIndex]);
					}

					System.out.print("\t" + fossilConfidence[groupIndex]);

					System.out.print("\t" + groupName);
					System.out.println();

				}
			}

			System.out.println();

			for (int fossilClassIndex = 0; fossilClassIndex < 2; fossilClassIndex++) {
				System.out.print(fossilClassIndex);
				for (int outputIndex = 0; outputIndex < numOutputs; outputIndex++) {
					System.out.print("\t" + fossilClassScores[fossilClassIndex][outputIndex] / fossilClassCounts[fossilClassIndex]);
				}
				System.out.println();
			}

		}

		if (reportExampleAccuracies) {

			for (int j = 0; j < numExamples; j++) {
				if (exampleTotalClassificationCounts[j] > 0) {

					double exampleAccuracy = (double) exampleCorrectClassificationCounts[j] / exampleTotalClassificationCounts[j];

					System.out.println(exampleTotalClassificationCounts[j] + "\t" + exampleAccuracy + "\t" + "\"" + examples[j].name + "\"");

				}
			}

			if (false) {
				double worstExampleAccuracy = Double.POSITIVE_INFINITY;
				int worstExampleIndex = -1;
				for (int j = 0; j < numExamples; j++) {
					if (exampleTotalClassificationCounts[j] > 0) {
						double exampleAccuracy = (double) exampleCorrectClassificationCounts[j] / exampleTotalClassificationCounts[j];

						if (exampleAccuracy < worstExampleAccuracy) {
							worstExampleAccuracy = exampleAccuracy;
							worstExampleIndex = j;
						}
					}
				}

				System.out.println("worstExampleIndex: " + worstExampleIndex);
				System.out.println("correct count: " + exampleCorrectClassificationCounts[worstExampleIndex]);
				System.out.println("total count: " + exampleTotalClassificationCounts[worstExampleIndex]);
				System.out.println("worst accuracy: " + worstExampleAccuracy);
				System.out.println("worst example: " + examples[worstExampleIndex].name);
			}
		}

		int numTrainingExampleLevels = -1;
		int minTrainingExampleLevel = -1;
		if (useEqualSizedTrainingSets) {
			numTrainingExampleLevels = numNumTrainingExamplesPerClassLog2;
			minTrainingExampleLevel = minNumTrainingExamplesPerClassLog2;
		}

		if (useNaturalTrainingSets) {
			numTrainingExampleLevels = numNumTrainingGroupsLog2;
			minTrainingExampleLevel = minNumTrainingGroupsLog2;
		}

		System.out.print("numClassesToUse" + "\t" + "numTrainingExamplesLog2" + "\t" + "numTrainingExamples" + "\t" + "accuracy" + "\t" + "menuDistance");
		for (int j = 0; j < numOutputs; j++) {
			System.out.print("\t" + "InTop" + (j + 1));
		}
		System.out.println();

		for (int index1 = 0; index1 < numNumClassesToUse; index1++) {

			int numClassesToUse = index1 + minNumClassesToUse;

			for (int index2 = 0; index2 < numTrainingExampleLevels; index2++) {

				int numTrainingExamplesLog2 = index2 + minTrainingExampleLevel;

				if (accuracyByNumClassesToUseByTrainingSetSizeCounts[index1][index2] > 0) {

					double accuracy = accuracyByNumClassesToUseByTrainingSetSizeSums[index1][index2] / accuracyByNumClassesToUseByTrainingSetSizeCounts[index1][index2];
					double menuDistance = menuDistanceByNumClassesToUseByTrainingSetSizeSums[index1][index2] / menuDistanceByNumClassesToUseByTrainingSetSizeCounts[index1][index2];

					int totalCount = 0;
					for (int j = 0; j < numOutputs; j++) {
						totalCount += menuDistributionCounts[j][index1][index2];
					}

					double probSum = 0.0;
					for (int j = 0; j < numOutputs; j++) {
						probSum += (double) menuDistributionCounts[j][index1][index2] / totalCount;
						menuDistribution[j] = probSum;
					}

					System.out.print(numClassesToUse + "\t" + numTrainingExamplesLog2 + "\t" + (1 << numTrainingExamplesLog2) + "\t" + accuracy + "\t" + menuDistance);

					for (int j = 0; j < numOutputs; j++) {
						System.out.print("\t" + menuDistribution[j]);
					}
					System.out.println();
				}
			}
			System.out.println();
		}

		if (reportClassificationMatrix) {

			int maxClassNameLength = Integer.MIN_VALUE;
			for (int i = 0; i < maxNumClassesToUse; i++) {
				if (outputNames[i].length() > maxClassNameLength)
					maxClassNameLength = outputNames[i].length();
			}

			maxClassNameLength += 1;

			System.out.println(String.format("%" + maxClassNameLength * (maxNumClassesToUse - 1) + "s", "actual vs predicted,"));

			String stringFormat = "%-" + maxClassNameLength + "s,";
			String numberFormat = " %-" + (maxClassNameLength - 1) + ".3f,";

			System.out.print(String.format(stringFormat, "predicted:"));
			for (int i = 0; i < maxNumClassesToUse; i++) {
				System.out.print(String.format(stringFormat, outputNames[i]));
			}
			System.out.println();

			for (int i = 0; i < maxNumClassesToUse; i++) {
				System.out.print(String.format(stringFormat, outputNames[i]));
				int classificationCount = 0;
				for (int j = 0; j < maxNumClassesToUse; j++) {
					classificationCount += classificationMatrix[i][j];
				}
				for (int j = 0; j < maxNumClassesToUse; j++) {
					System.out.print(String.format(numberFormat, (double) classificationMatrix[i][j] / classificationCount));
				}
				System.out.println();
			}

		}

		if (false) {
			IO.writeObject("/data/experience.ser", experiences);
		}
	}

	void setNumThreads(int numThreads) {
		this.numThreads = numThreads;
	}

	int getNumThreads() {
		return numThreads;
	}

	void setNextProblemIndex(int nextProblemIndex) {
		this.nextProblemIndex = nextProblemIndex;
	}

	int getNextProblemIndexAndIncrement() {

		synchronized (this) {

			if (nextProblemIndex < numProblems) {

				int problemIndex = nextProblemIndex;

				nextProblemIndex++;

				return problemIndex;
			} else {
				return -1;
			}
		}

	}

	void setVerbose(boolean verbose) {
		this.verbose = verbose;
	}

	boolean isVerbose() {
		return verbose;
	}

	int getNumProblems() {
		return experiences.size();
	}

}

class Monitor extends Thread {

	boolean verbose = false;
	boolean stopNow = false;
	boolean terminated = false;

	ADAPT adapt;

	Monitor(ADAPT adapt) {
		this.adapt = adapt;
	}

	public void run() {

		int count = 0;

		while (true) {

			if (stopNow)
				break;

			if (verbose)
				System.out.println("Monitor Sleep Count: " + count);
			count++;
			try {
				Thread.sleep(adapt.monitorSleepTimeInMS);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		terminated = true;

		return;
	}

}

class ExampleCreationBias implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -748872772740778101L;
	HashMap<String, Object> parameterMap;

	ExampleCreationBias(HashMap<String, Object> parameterMap) {
		this.parameterMap = parameterMap;
	}
}

class Example implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3416904877701228532L;

	//

	//

	int width = -1;
	int height = -1;

	String name;
	int groupID = -1;
	int groupIndex = -1;
	int exampleIndex = -1;

	double[] inputValues;
	double[] outputValues;

	int numObservations;
	double[] outputValueSums;

	HashMap<String, Object> metaFeatures;
	HashMap<String, Object> primitiveFeatures;
	HashMap<String, Object> constructedFeatures;

	ArrayList<String> filePaths;
	String filePath;

	Example(String name, int exampleIndex, int groupID, double[] inputValues, double[] outputValues) {

		this.name = name;
		this.exampleIndex = exampleIndex;
		this.groupID = groupID;
		this.inputValues = inputValues;
		this.outputValues = outputValues;
		this.filePaths = new ArrayList<String>();

		this.metaFeatures = new HashMap<String, Object>();
		this.primitiveFeatures = new HashMap<String, Object>();
		this.constructedFeatures = new HashMap<String, Object>();

	}

	public int getClassIndex() {

		for (int outputIndex = 0; outputIndex < this.outputValues.length; outputIndex++) {

			if (this.outputValues[outputIndex] == 1.0) {
				return outputIndex;
			}

		}
		// TODO Auto-generated method stub
		return -1;
	}

	void setFilePath(String pathName) {
		this.filePath = pathName;
	}

	String getFilePath() {
		return filePath;
	}

}

class ExampleState implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5689841256131485054L;
	Example examples[] = null;
	int numExamples;
	int maxNumExamples;
	int numInputs;
	int numOutputs;
	String inputNames[] = null;
	String outputNames[] = null;
	double[][] exampleDoubleValues;
	double[] exampleInputDoubleValues;
	double[] exampleOutputDoubleValues;

	double[] inputWeights;

	int numIntensityDistributionFeatures;
	int minNumIntensityBins;
	int maxNumIntensityBins;
	int numNumIntensityBins;

	int numGrossShapeFeatures;
	int minGrossShapeResolution;
	int maxGrossShapeResolution;
	int numGrossShapeResolutions;

	int numTextureFeatures = -1;
	int minTextureLineNumPixels;
	int maxTextureLineNumPixels;
	int numTextureLineNumPixels;

	int[] intensityDistributionFeatureIndices;
	int[] grossShapeFeatureIndices;
	int[] textureLineFeatureIndices;

	ExampleState(ADAPT adapt) {

		this.examples = adapt.examples;
		this.numExamples = adapt.numExamples;
		this.maxNumExamples = adapt.maxNumExamples;
		this.numInputs = adapt.numInputs;
		this.numOutputs = adapt.numOutputs;
		this.inputNames = adapt.inputNames;
		this.outputNames = adapt.outputNames;
		this.exampleDoubleValues = adapt.exampleValues;
		// this.exampleInputDoubleValues = adapt.exampleInputValues;
		// this.exampleOutputDoubleValues = adapt.exampleOutputValues;

		this.numIntensityDistributionFeatures = adapt.numIntensityDistributionFeatures;
		this.minNumIntensityBins = adapt.minNumIntensityBins;
		this.maxNumIntensityBins = adapt.maxNumIntensityBins;
		this.numNumIntensityBins = adapt.numNumIntensityBins;

		this.minGrossShapeResolution = adapt.minGrossShapeResolution;
		this.maxGrossShapeResolution = adapt.maxGrossShapeResolution;
		this.numGrossShapeResolutions = adapt.numGrossShapeResolutions;

		this.minTextureLineNumPixels = adapt.minTextureLineNumPixels;
		this.maxTextureLineNumPixels = adapt.maxTextureLineNumPixels;
		this.numTextureLineNumPixels = adapt.numTextureLineNumPixels;

		this.intensityDistributionFeatureIndices = adapt.intensityDistributionFeatureIndices;
		this.grossShapeFeatureIndices = adapt.grossShapeFeatureIndices;
		this.textureLineFeatureIndices = adapt.textureLineFeatureIndices;
	}

	void restoreExampleState(ADAPT adapt) {
		adapt.examples = this.examples;
		adapt.numExamples = this.numExamples;
		adapt.maxNumExamples = this.maxNumExamples;
		adapt.numInputs = this.numInputs;
		adapt.numOutputs = this.numOutputs;
		adapt.inputNames = this.inputNames;
		adapt.outputNames = this.outputNames;
		adapt.exampleValues = this.exampleDoubleValues;
		// adapt.exampleInputValues = this.exampleInputDoubleValues;
		// adapt.exampleOutputValues = this.exampleOutputDoubleValues;
		adapt.numIntensityDistributionFeatures = this.numIntensityDistributionFeatures;
		adapt.minNumIntensityBins = this.minNumIntensityBins;
		adapt.maxNumIntensityBins = this.maxNumIntensityBins;
		adapt.numNumIntensityBins = this.numNumIntensityBins;
		adapt.minGrossShapeResolution = this.minGrossShapeResolution;
		adapt.maxGrossShapeResolution = this.maxGrossShapeResolution;
		adapt.numGrossShapeResolutions = this.numGrossShapeResolutions;
		adapt.minTextureLineNumPixels = this.minTextureLineNumPixels;
		adapt.maxTextureLineNumPixels = this.maxTextureLineNumPixels;
		adapt.numTextureLineNumPixels = this.numTextureLineNumPixels;
		adapt.intensityDistributionFeatureIndices = this.intensityDistributionFeatureIndices;
		adapt.grossShapeFeatureIndices = this.grossShapeFeatureIndices;
		adapt.textureLineFeatureIndices = this.textureLineFeatureIndices;

	}

	static void clearExampleState(ADAPT adapt) {
		adapt.examples = null;
		adapt.numExamples = -1;
		adapt.maxNumExamples = -1;
		adapt.numInputs = -1;
		adapt.numOutputs = -1;
		adapt.inputNames = null;
		adapt.outputNames = null;
		adapt.exampleValues = null;
		adapt.numIntensityDistributionFeatures = -1;
		adapt.minNumIntensityBins = -1;
		adapt.maxNumIntensityBins = -1;
		adapt.numNumIntensityBins = -1;
		adapt.minGrossShapeResolution = -1;
		adapt.maxGrossShapeResolution = -1;
		adapt.numGrossShapeResolutions = -1;
		adapt.minTextureLineNumPixels = -1;
		adapt.maxTextureLineNumPixels = -1;
		adapt.numTextureLineNumPixels = -1;
		adapt.intensityDistributionFeatureIndices = null;
		adapt.grossShapeFeatureIndices = null;
		adapt.textureLineFeatureIndices = null;

	}

}

class ExperienceSet implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8329347637921019218L;
	int numExperiences;
	Experience[] experiences;

	ExperienceSet(Experience[] experiences) {

		this.experiences = experiences;
		this.numExperiences = experiences.length;

	}

}

class Experience implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -194669560912285511L;

	boolean isImageScan;
	boolean isExampleGeneration;
	boolean isSupervisedLearning;

	int overallIndex;

	// PROBLEM

	Problem problem;

	// BIAS

	Bias bias;

	// RESULTS

	Result result;

	// OBJECTIVE

	Objective objective;

	Experience(int overallIndex, boolean isImageScan, boolean isExampleGeneration, boolean isSupervisedLearning) {

		this.isImageScan = isImageScan;
		this.isExampleGeneration = isExampleGeneration;
		this.isSupervisedLearning = isSupervisedLearning;
		this.overallIndex = overallIndex;
		this.problem = new Problem();
		this.bias = new Bias();
		this.result = new Result();
		this.objective = new Objective();

	}

}

class Problem implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4059624185184935250L;

	int numClassesToUse;

	int numTrainingExamplesPerClassLog2;
	int numTrainingExamplesPerClass;
	int numTestingExamplesPerClass;

	int numTrainingGroupsLog2;
	int numTrainingGroups;
	int numTestingGroups;

	ArrayList<Integer> classIndicesToUse;
	int numTrainExamples;
	int numTestExamples;
	transient ArrayList<Integer> testGroupIndices;
	transient ArrayList<Integer> trainGroupIndices;
	transient ArrayList<Integer> testExampleIndices;
	transient ArrayList<Integer> trainExampleIndices;

}

class Bias implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1513729867346594058L;

}

class Result implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3197567519959354844L;

	transient ArrayList<Integer> testExamplePredictedClass;
	transient ArrayList<Integer> testExampleActualClass;
	transient ArrayList<Double> testExampleMenuDistance;

	transient ArrayList<Integer> testGroupPredictedClass;
	transient ArrayList<Integer> testGroupActualClass;
	transient ArrayList<Double> testGroupMenuDistance;

	double[][] testExamplePredictedOutputValues;

	boolean complete;
	boolean failed;

	Example example;

}

class Objective implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -794535785132825190L;

	double accuracy;
	double menuDistance;
	int[] menuDistributionCounts;

}

class ProblemSolver extends Thread {

	ADAPT adapt;

	int numThreads = -1;
	int threadIndex = -1;

	boolean stopNow = false;
	boolean terminated = false;

	// int[] trainExampleIndices;
	double[][] trainExampleInputValues;
	double[][] trainExampleOutputValues;
	double[][] trainExampleInputValuesSorted;

	// int[] testExampleIndices;
	double[][] testExampleInputValues;
	double[][] testExampleOutputValues;
	double[][] testExampleInputValuesSorted;

	long[] distanceAndIndexLongs;
	double[] averageOutputValues;

	//

	//

	//

	double[][] samples = null;
	double[][][] xycValues = null;
	double[][][] shapeValueSums = null;
	int[][][] shapeValueCounts = null;
	double[][][] textureXYCValues = null;
	double[][][] grossShapeValueSums = null;
	int[][][] grossShapeValueCounts = null;
	int[] pixelLineBinCounts = null;

	Random predictionTieBreakerRandomGenerator = null;

	//

	ProblemSolver(ADAPT adapt, int threadIndex) {
		this.adapt = adapt;
		this.numThreads = adapt.getNumThreads();
		this.threadIndex = threadIndex;
	}

	public void run() {

		while (true) {

			if (stopNow) {
				break;
			}

			int problemIndex = adapt.getNextProblemIndexAndIncrement();

			if (problemIndex == -1) {
				break;
			}

			/*****************/
			/* SOLVE PROBLEM */
			/*****************/

			solveProblem(adapt.experiences.get(problemIndex));

		}

		terminated = true;

		return;
	}

	void solveProblem(Experience experience) {

		if (experience.isImageScan) {

			adapt.scanImageFile(experience);

		}

		if (experience.isExampleGeneration) {

			if (samples == null) {
				samples = new double[adapt.numColorsToUse][adapt.maxNumSamples];
			}
			if (xycValues == null) {
				xycValues = new double[adapt.maxWidth][adapt.maxHeight][adapt.numColorsToUse];
			}

			if (shapeValueSums == null) {
				shapeValueSums = new double[adapt.minShapeXYResolution][adapt.minShapeXYResolution][adapt.numColorsToUse];
				shapeValueCounts = new int[adapt.minShapeXYResolution][adapt.minShapeXYResolution][adapt.numColorsToUse];
				textureXYCValues = new double[adapt.textureXYResolution][adapt.textureXYResolution][adapt.numColorsToUse];
			}
			if (grossShapeValueSums == null) {
				grossShapeValueSums = new double[adapt.maxGrossShapeResolution][adapt.maxGrossShapeResolution][adapt.numColorsToUse];
				grossShapeValueCounts = new int[adapt.maxGrossShapeResolution][adapt.maxGrossShapeResolution][adapt.numColorsToUse];
			}

			if (pixelLineBinCounts == null) {
				int textureLineNumPixelsMinusOne = adapt.maxTextureLineNumPixels - 1;
				int textureLineNumValueBins = (2 << textureLineNumPixelsMinusOne);
				pixelLineBinCounts = new int[textureLineNumValueBins];
			}

			adapt.createExample(experience, samples, xycValues, shapeValueSums, shapeValueCounts, textureXYCValues, grossShapeValueSums, grossShapeValueCounts, pixelLineBinCounts);

		}

		if (experience.isSupervisedLearning) {

			predictionTieBreakerRandomGenerator = new Random(experience.overallIndex);

			// System.out.println("solveProblem(" + experience.overallIndex +
			// ")");

			// create examples

			createTrainAndTestExamples(experience);

			// create model

			createModel(experience);

			// apply model

			applyModelToTestExamples(experience);
		}

	}

	// int[] trainExampleIndices;
	double[][] trainExampleInputValuesCache;
	double[][] trainExampleOutputValuesCache;

	// int[] testExampleIndices;
	double[][] testExampleInputValuesCache;
	double[][] testExampleOutputValuesCache;

	void createTrainAndTestExamples(Experience experience) {

		Problem problem = experience.problem;

		ArrayList<Integer> trainExampleIndexArrayList = problem.trainExampleIndices;

		if (trainExampleInputValuesCache == null || trainExampleInputValuesCache.length < problem.numTrainExamples) {
			trainExampleInputValuesCache = new double[problem.numTrainExamples][];
			trainExampleOutputValuesCache = new double[problem.numTrainExamples][];
		}

		if (testExampleInputValuesCache == null || testExampleInputValuesCache.length < problem.numTestExamples) {
			testExampleInputValuesCache = new double[problem.numTestExamples][];
			testExampleOutputValuesCache = new double[problem.numTestExamples][];
		}

		trainExampleInputValues = trainExampleInputValuesCache;
		trainExampleOutputValues = trainExampleOutputValuesCache;
		testExampleInputValues = testExampleInputValuesCache;
		testExampleOutputValues = testExampleInputValuesCache;

		for (int trainExampleIndex = 0; trainExampleIndex < problem.numTrainExamples; trainExampleIndex++) {

			int exampleIndex = trainExampleIndexArrayList.get(trainExampleIndex);

			trainExampleInputValues[trainExampleIndex] = adapt.examples[exampleIndex].inputValues;
			trainExampleOutputValues[trainExampleIndex] = adapt.examples[exampleIndex].outputValues;

			// for (int inputIndex = 0; inputIndex < adapt.numInputs; inputIndex++) {
			//
			// // trainExampleInputValues[trainExampleIndex][inputIndex] = adapt.exampleInputValues[exampleIndex * adapt.numInputs + inputIndex];
			// trainExampleInputValues[trainExampleIndex][inputIndex] = adapt.examples[exampleIndex].inputValues[inputIndex];
			// }
			// for (int outputIndex = 0; outputIndex < adapt.numOutputs; outputIndex++) {
			//
			// trainExampleOutputValues[trainExampleIndex][outputIndex] = adapt.exampleOutputValues[exampleIndex * adapt.numOutputs + outputIndex];
			// }

		}

		ArrayList<Integer> testExampleIndexArrayList = problem.testExampleIndices;

		testExampleInputValues = new double[problem.numTestExamples][];
		testExampleOutputValues = new double[problem.numTestExamples][];

		for (int testExampleIndex = 0; testExampleIndex < problem.numTestExamples; testExampleIndex++) {

			int exampleIndex = testExampleIndexArrayList.get(testExampleIndex);

			testExampleInputValues[testExampleIndex] = adapt.examples[exampleIndex].inputValues; // !!!
			testExampleOutputValues[testExampleIndex] = adapt.examples[exampleIndex].outputValues; // !!!

			// for (int inputIndex = 0; inputIndex < adapt.numInputs; inputIndex++) {
			//
			// testExampleInputValues[testExampleIndex][inputIndex] = adapt.exampleInputValues[exampleIndex * adapt.numInputs + inputIndex];
			// }
			// for (int outputIndex = 0; outputIndex < adapt.numOutputs; outputIndex++) {
			//
			// testExampleOutputValues[testExampleIndex][outputIndex] = adapt.exampleOutputValues[exampleIndex * adapt.numOutputs + outputIndex];
			// }

		}

	}

	void createModel(Experience experience) {

		// System.out.println("createModel(" + experience.overallIndex + ")");

		if (adapt.useRankNormalization) {
			computeNormalizationsFromTrainExamples(experience);

			applyNormalizationsToTrainExamples(experience);
		}

		// optimize distance function & feature weights

	}

	void computeNormalizationsFromTrainExamples(Experience experience) {

		int numTrainExamples = experience.problem.numTrainExamples;

		trainExampleInputValuesSorted = new double[adapt.numInputs][numTrainExamples];

		for (int inputIndex = 0; inputIndex < adapt.numInputs; inputIndex++) {

			for (int trainExampleIndex = 0; trainExampleIndex < numTrainExamples; trainExampleIndex++) {
				trainExampleInputValuesSorted[inputIndex][trainExampleIndex] = trainExampleInputValues[trainExampleIndex][inputIndex];
			}
			Arrays.sort(trainExampleInputValuesSorted[inputIndex]);
		}

	}

	void applyNormalizationsToTrainExamples(Experience experience) {
		// build model: normalize examples

		Problem problem = experience.problem;

		for (int inputIndex = 0; inputIndex < adapt.numInputs; inputIndex++) {

			for (int trainExampleIndex = 0; trainExampleIndex < problem.numTrainExamples; trainExampleIndex++) {

				int result = Arrays.binarySearch(trainExampleInputValuesSorted[inputIndex], trainExampleInputValues[trainExampleIndex][inputIndex]);
				// System.out.println("result:" + result);

				boolean keyFound = (result >= 0);

				if (!keyFound) {
					System.out.println("Error in applyNormalizationsToTrainExamples! (!keyFound) : value = " + trainExampleInputValues[trainExampleIndex][inputIndex]);
					System.out.println("Error in applyNormalizationsToTrainExamples! (!keyFound) : result = " + result);
					System.out.println("Error in applyNormalizationsToTrainExamples! (!keyFound) : key-2 = " + trainExampleInputValuesSorted[inputIndex][-result - 2]);
					System.out.println("Error in applyNormalizationsToTrainExamples! (!keyFound) : key-1 = " + trainExampleInputValuesSorted[inputIndex][-result - 1]);
					System.out.println("Error in applyNormalizationsToTrainExamples! (!keyFound) : key   = " + trainExampleInputValuesSorted[inputIndex][-result]);
					System.out.println("Error in applyNormalizationsToTrainExamples! (!keyFound) : key+1 = " + trainExampleInputValuesSorted[inputIndex][-result + 1]);
					System.out.println("Error in applyNormalizationsToTrainExamples! (!keyFound) : key+2 = " + trainExampleInputValuesSorted[inputIndex][-result + 2]);
					result = -result;
					if (result < 0)
						result = 0;
					if (result >= problem.numTrainExamples)
						result = (problem.numTrainExamples - 1);
				}

				double normalizedValue = (double) result / (problem.numTrainExamples - 1);

				if (normalizedValue < (double) 0.0 || normalizedValue > (double) 1.0) {
					System.out.println("Error in applyNormalizationsToTrainExamples! (normalizedValue < 0.0 || normalizedValue > 1.0)");

					System.out.println("result = " + result);
					System.out.println("experience.problem.numTrainExamples = " + problem.numTrainExamples);
					System.out.println("normalizedValue = " + normalizedValue);
					System.out.println("experience.overallIndex = " + experience.overallIndex);

					if (normalizedValue < 0.0) {
						normalizedValue = 0.0;
					}
					if (normalizedValue > 1.0) {
						normalizedValue = 1.0;
					}
				}

				trainExampleInputValues[trainExampleIndex][inputIndex] = normalizedValue;
				// System.out.println("value:" +
				// trainExampleInputValues[trainExampleIndex][inputIndex]);

			}
		}
	}

	void applyNormalizationsToTestExamples(Experience experience) {
		// build model: normalize examples
		Problem problem = experience.problem;

		for (int inputIndex = 0; inputIndex < adapt.numInputs; inputIndex++) {

			for (int testExampleIndex = 0; testExampleIndex < problem.numTestExamples; testExampleIndex++) {

				int result = Arrays.binarySearch(trainExampleInputValuesSorted[inputIndex], testExampleInputValues[testExampleIndex][inputIndex]);
				// System.out.println("result:" + result);

				boolean keyFound = (result >= 0);

				if (!keyFound) {
					result = -result;
					if (result < 0)
						result = 0;
					if (result >= problem.numTrainExamples)
						result = (problem.numTrainExamples - 1);
				}

				double normalizedValue = (double) result / (problem.numTrainExamples - 1);

				if (normalizedValue < (double) 0.0 || normalizedValue > (double) 1.0) {
					System.out.println("Error in applyNormalizationsToTestExamples! (normalizedValue < 0.0 || normalizedValue > 1.0)");
					System.out.println("keyFound = " + keyFound);
					System.out.println("result = " + result);
					System.out.println("problem.numTrainExamples = " + problem.numTrainExamples);
					System.out.println("normalizedValue = " + normalizedValue);
					System.out.println("experience.overallIndex = " + experience.overallIndex);
					if (normalizedValue < (double) 0.0) {
						normalizedValue = (double) 0.0;
					}
					if (normalizedValue > (double) 1.0) {
						normalizedValue = (double) 1.0;
					}
				}

				testExampleInputValues[testExampleIndex][inputIndex] = normalizedValue;
				// System.out.println("value:" +
				// TestExampleInputValues[TestExampleIndex][inputIndex]);

			}
		}
	}

	void applyModelToTestExamples(Experience experience) {

		// System.out.println("applyModel(" + experience.overallIndex + ")");
		if (adapt.useRankNormalization) {
			applyNormalizationsToTestExamples(experience);
		}

		// measure performance on test examples

		measureIBLAccuracy(experience);

		experience.result.complete = true;

	}

	int[] tiedForBestClassIndices = null;

	void measureIBLAccuracy(Experience experience) {

		if (tiedForBestClassIndices == null)
			tiedForBestClassIndices = new int[adapt.numOutputs];

		double distanceWeightingPower = adapt.distanceWeightingPower;

		// double individualDistancePower = adapt.individualDistancePower;

		Problem problem = experience.problem;
		Result result = experience.result;
		Objective objective = experience.objective;

		// distanceAndIndexLongs = new long[problem.numTrainExamples];

		if (averageOutputValues == null) {
			averageOutputValues = new double[adapt.numOutputs];
		}

		double accuracySum = 0.0;
		double menuDistanceSum = 0.0;

		result.testExampleActualClass = new ArrayList<Integer>(problem.numTestExamples);
		result.testExamplePredictedClass = new ArrayList<Integer>(problem.numTestExamples);
		result.testGroupActualClass = new ArrayList<Integer>(problem.testGroupIndices.size());
		result.testGroupPredictedClass = new ArrayList<Integer>(problem.testGroupIndices.size());
		result.testExampleMenuDistance = new ArrayList<Double>(problem.numTestExamples);

		objective.menuDistributionCounts = new int[adapt.numOutputs];

		int numGroupsPresent = problem.testGroupIndices.size();
		int[][] groupActualDistributions = new int[numGroupsPresent][adapt.numOutputs];
		double[][] groupPredictionDistributions = new double[numGroupsPresent][adapt.numOutputs];

		result.testExamplePredictedOutputValues = new double[problem.numTestExamples][adapt.numOutputs];

		for (int testExampleIndex = 0; testExampleIndex < problem.numTestExamples; testExampleIndex++) {

			// System.out.println("testExampleIndex = " + testExampleIndex);

			Example testExample = adapt.examples[problem.testExampleIndices.get(testExampleIndex)];

			double[] testInputValues = testExampleInputValues[testExampleIndex]; // !!!
			double[] testOutputValues = testExampleOutputValues[testExampleIndex]; // !!!
			// double[] testInputValues = testExample.inputValues; //!!!
			// double[] testOutputValues = testExample.outputValues; //!!!

			int actualClassIndex = -1;
			double maxActualOutputValue = Double.NEGATIVE_INFINITY;
			for (int outputIndex = 0; outputIndex < adapt.numOutputs; outputIndex++) {
				double outputValue = testOutputValues[outputIndex];
				if (outputValue > maxActualOutputValue) {
					actualClassIndex = outputIndex;
					maxActualOutputValue = outputValue;
				}
			}

			// System.out.println("maxActualOutputValueIndex = " +
			// maxActualOutputValueIndex);

			for (int outputIndex = 0; outputIndex < adapt.numOutputs; outputIndex++) {
				averageOutputValues[outputIndex] = 0.0;
			}

			for (int trainExampleIndex = 0; trainExampleIndex < problem.numTrainExamples; trainExampleIndex++) {

				// Example trainExample = adapt.examples[problem.trainExampleIndices.get(trainExampleIndex)];

				double[] trainInputValues = trainExampleInputValues[trainExampleIndex];

				// double[] trainOutputValues =
				// trainExampleOutputValues[trainExampleIndex];

				// compute distance components

				double overallDistanceSum = 0.0;

				/***************************/
				/* INTENSITY DISTRIBUTIONS */
				/***************************/

				if (adapt.overallIntensityDistributionWeight > 0.0) {

					for (int numIntensityBins = adapt.minNumIntensityBins, i = 0; numIntensityBins <= adapt.maxNumIntensityBins; numIntensityBins++, i++) {
						int numIntensityBinsMinusOne = numIntensityBins - 1;
						int index = adapt.intensityDistributionFeatureIndices[i];

						double weight = adapt.intensityDistributionFeatureWeights[i] * adapt.overallIntensityDistributionWeight;
						if (weight > 0.0)
							for (int c = 0; c < adapt.numColorsToUse; c++) {
								for (int b = 0; b < numIntensityBinsMinusOne; b++) {
									double difference = (Math.abs(testInputValues[index] - trainInputValues[index]));
									overallDistanceSum += difference * weight;
									index++;
								}
							}
					}

				}

				/***************/
				/* GROSS SHAPE */
				/***************/

				if (adapt.overallGrossShapeWeight > 0.0) {

					for (int grossShapeResolution = adapt.minGrossShapeResolution, i = 0; grossShapeResolution <= adapt.maxGrossShapeResolution; grossShapeResolution++, i++) {

						int index = adapt.grossShapeFeatureIndices[i];
						double weight = adapt.grossShapeFeatureWeights[i] * adapt.overallGrossShapeWeight;

						if (weight > 0.0)
							for (int x = 0; x < grossShapeResolution; x++) {
								for (int y = 0; y < grossShapeResolution; y++) {
									for (int z = 0; z < adapt.numColorsToUse; z++) {

										int testOffset = x * grossShapeResolution * adapt.numColorsToUse + y * adapt.numColorsToUse + z;
										int trainOffset = x * grossShapeResolution * adapt.numColorsToUse + y * adapt.numColorsToUse + z;

										double difference = (Math.abs(testInputValues[index + testOffset] - trainInputValues[index + trainOffset]));

										overallDistanceSum += difference * weight;

										index++;
									}
								}
							}
					}

				}

				/*****************/
				/* TEXTURE LINES */
				/*****************/

				if (adapt.overallTextureWeight > 0.0) {

					for (int textureLineNumPixels = adapt.minTextureLineNumPixels, i = 0; textureLineNumPixels <= adapt.maxTextureLineNumPixels; textureLineNumPixels++, i++) {

						int textureLineNumPixelsMinusOne = textureLineNumPixels - 1;

						int index = adapt.textureLineFeatureIndices[i];

						double weight = adapt.textureLineFeatureWeights[i] * adapt.overallTextureWeight;

						if (weight > 0.0)
							for (int c = 0; c < adapt.numColorsToUse; c++) {
								for (int pixelIndex = 0; pixelIndex < textureLineNumPixelsMinusOne; pixelIndex++) {

									double difference = (Math.abs(testInputValues[index] - trainInputValues[index]));
									overallDistanceSum += difference * weight;

									index++;
								}
							}
					}
				}

				double distance = overallDistanceSum;
				double weight = Double.NaN;

				// System.out.println("distance = " + distance);

				if (distance < adapt.zeroDistanceSubstituteDistance)
					distance = adapt.zeroDistanceSubstituteDistance;

				weight = (double) (1.0 / Math.pow(distance, distanceWeightingPower));

				if (adapt.testForNaNs) {
					if (Double.isNaN(weight)) {

						adapt.fail("Double.isNaN(weight)");

						new Exception().printStackTrace();
						System.exit(1);
					}
				}

				for (int outputIndex = 0; outputIndex < adapt.numOutputs; outputIndex++) {
					averageOutputValues[outputIndex] += trainExampleOutputValues[trainExampleIndex][outputIndex] * weight;
				}

			}

			for (int outputIndex = 0; outputIndex < adapt.numOutputs; outputIndex++) {
				result.testExamplePredictedOutputValues[testExampleIndex][outputIndex] = averageOutputValues[outputIndex];
			}

			// System.out.print("META" + "\t" + testExample.name);
			double sum = 0.0;
			for (int outputIndex = 0; outputIndex < adapt.numOutputs; outputIndex++) {
				sum += averageOutputValues[outputIndex];
			}
			// for (int outputIndex = 0; outputIndex < adapt.numOutputs; outputIndex++) {
			// System.out.print("\t" + averageOutputValues[outputIndex] / sum);
			// }
			// System.out.println();

			if (testExample.outputValueSums == null) {
				testExample.outputValueSums = new double[adapt.numOutputs];
				testExample.numObservations = 0;
			}
			for (int outputIndex = 0; outputIndex < adapt.numOutputs; outputIndex++) {
				testExample.outputValueSums[outputIndex] += averageOutputValues[outputIndex] / sum;
			}
			testExample.numObservations++;

			// int maxPredictedOutputValueIndex = -1;
			double maxPredictedOutputValue = Double.NEGATIVE_INFINITY;
			double outputSum = 0.0;
			for (int outputIndex = 0; outputIndex < adapt.numOutputs; outputIndex++) {
				if (averageOutputValues[outputIndex] > maxPredictedOutputValue) {
					// maxPredictedOutputValueIndex = outputIndex;
					maxPredictedOutputValue = averageOutputValues[outputIndex];
				}
				outputSum += averageOutputValues[outputIndex];
			}

			if (outputSum != 0.0) {

				for (int outputIndex = 0; outputIndex < adapt.numOutputs; outputIndex++) {

					groupPredictionDistributions[problem.testGroupIndices.indexOf(testExample.groupIndex)][outputIndex] += averageOutputValues[outputIndex] / outputSum;

					if (adapt.testForNaNs) {
						if (groupPredictionDistributions[problem.testGroupIndices.indexOf(testExample.groupIndex)][outputIndex] == Double.NaN) {

							adapt.fail("groupPredictionDistributions[problem.testGroupIndices.indexOf(testExample.groupIndex)][outputIndex] == Double.NaN");
						}
					}
				}
			} else {
				adapt.fail("(outputSum == 0.0)");
			}

			int numTopOutputIndices = 0;
			for (int outputIndex = 0; outputIndex < adapt.numOutputs; outputIndex++) {
				if (averageOutputValues[outputIndex] == maxPredictedOutputValue) {
					tiedForBestClassIndices[numTopOutputIndices++] = outputIndex;
				}
			}

			int predictedOutputValueIndex = tiedForBestClassIndices[(int) (predictionTieBreakerRandomGenerator.nextFloat() * numTopOutputIndices)];

			// groupPredictionDistributions[problem.testGroupIndices.indexOf(testExample.groupIndex)][predictedOutputValueIndex]++;
			groupActualDistributions[problem.testGroupIndices.indexOf(testExample.groupIndex)][actualClassIndex]++;

			double accuracy;
			if (actualClassIndex == predictedOutputValueIndex) {
				accuracy = 1.0;
			} else {
				accuracy = 0.0;
			}

			double actualClassPredictedProbability = averageOutputValues[actualClassIndex];
			int numBetter = 0;
			int numSame = 0;
			for (int j = 0; j < adapt.numOutputs; j++) {

				if (problem.classIndicesToUse.contains(j)) { // !!! slow !!!
					if (averageOutputValues[j] > actualClassPredictedProbability)
						numBetter++;
					else if (averageOutputValues[j] == actualClassPredictedProbability)
						numSame++;
				}
			}

			double menuDistance = numBetter + numSame / 2;

			result.testExampleMenuDistance.add(menuDistance);
			objective.menuDistributionCounts[(int) menuDistance]++;

			menuDistanceSum += menuDistance;

			if (actualClassIndex == -1 || predictedOutputValueIndex == -1) {

				System.out.println();
				System.out.println("maxActualOutputValueIndex = " + actualClassIndex);
				System.out.println("maxPredictedOutputValueIndex = " + predictedOutputValueIndex);
				predictedOutputValueIndex = 0; // !!!
			}
			result.testExampleActualClass.add(actualClassIndex);
			result.testExamplePredictedClass.add(predictedOutputValueIndex);

			accuracySum += accuracy;
		}

		double averageAccuracy = accuracySum / problem.numTestExamples;
		double averageMenuDistance = menuDistanceSum / problem.numTestExamples;

		// objective.accuracy = averageAccuracy;
		objective.menuDistance = averageMenuDistance; // !!! needs change to
		// group perspective

		accuracySum = 0.0;
		for (int problemGroupIndex = 0; problemGroupIndex < numGroupsPresent; problemGroupIndex++) {

			double groupPredictedClassMaxWeight = Double.NEGATIVE_INFINITY;
			int groupActualClassMaxCount = Integer.MIN_VALUE;
			int groupPredictedClassIndex = -1;
			int groupActualClassIndex = -1;

			for (int classIndex = 0; classIndex < adapt.numOutputs; classIndex++) {

				if (false) {
					System.out.println("groupPredictionDistributions[" + problemGroupIndex + "][" + classIndex + "] = " + groupPredictionDistributions[problemGroupIndex][classIndex]);
					System.out.println("groupActualDistributions    [" + problemGroupIndex + "][" + classIndex + "] = " + groupActualDistributions[problemGroupIndex][classIndex]);
				}

				if (groupPredictionDistributions[problemGroupIndex][classIndex] > groupPredictedClassMaxWeight) {
					groupPredictedClassMaxWeight = groupPredictionDistributions[problemGroupIndex][classIndex];
					groupPredictedClassIndex = classIndex;
				}
				if (groupActualDistributions[problemGroupIndex][classIndex] > groupActualClassMaxCount) {
					groupActualClassMaxCount = groupActualDistributions[problemGroupIndex][classIndex];
					groupActualClassIndex = classIndex;
				}

			}

			if (groupPredictedClassIndex == -1 || groupPredictedClassIndex == -1) {
				System.out.println("groupPredictedClassIndex = " + groupPredictedClassIndex);
				System.out.println("groupActualClassIndex = " + groupActualClassIndex);
				new Exception().printStackTrace();
				System.exit(1);
			}

			if (false) {
				System.out.println("groupPredictedClassIndex = " + groupPredictedClassIndex);
				System.out.println("groupActualClassIndex = " + groupActualClassIndex);
			}

			if (groupPredictedClassIndex == groupActualClassIndex)
				accuracySum += 1.0;

			result.testGroupPredictedClass.add(groupPredictedClassIndex);
			result.testGroupActualClass.add(groupActualClassIndex);
		}

		double accuracy = accuracySum / numGroupsPresent;

		if (false) {
			System.out.println("accuracy = " + accuracy);
		}

		objective.accuracy = accuracy;

	}

	public void terminate() {
		stopNow = true;
	}

}
