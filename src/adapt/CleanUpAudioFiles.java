package adapt;

import java.io.File;

public class CleanUpAudioFiles {

	static boolean deleteFilesARMED = true; // !!! if true, files WILL BE DELETED!

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String dataDirectoryPaths[] = { "/media/My3TBA/data/audio/clips11", "/media/My3TBC/data/audio/clips12", "/media/My2TB/data/audio/clips13",
				"/media/My500GBA/data/audio/clips14", "/media/My500GBB/data/audio/clips15", "/media/My500GBC/data/audio/clips16", "/media/My500GBD/data/audio/clips17",
				"/data/audio/clips18", };

		int numFiles = 0;
		int numBadFiles = 0;
		for (int j = 0; j < dataDirectoryPaths.length; j++) {

			
			// String[] strings = IO.getPathStrings("/media/My1TB/data/audio");
			// String[] strings = IO.getPathStrings("/media/My3TBA/data/audio");
			// String[] strings = IO.getPathStrings("/media/My3TBB/data/audio");
			// String[] strings = IO.getPathStrings("/media/My3TBC/data/audio");
			String[] strings = IO.getPathStrings(dataDirectoryPaths[j]);

			numFiles += strings.length;
			
			for (int i = 0; i < strings.length; i++) {

				String fileName = strings[i];
				// System.out.println(fileName);

				String[] parts = fileName.split("_");

				// System.out.println(fileName);
				// System.out.println(parts[0]);
				// System.out.println(parts[1]);
				// System.out.println(parts[2]);
				// System.out.println(parts[3]);

				int sampleRate = -1;
				int numChannels = -1;
				try {
					sampleRate = Integer.parseInt(parts[2]);
					numChannels = Integer.parseInt(parts[3].replaceAll(".raw", ""));
				} catch (Exception e) {
				}

				File file = new File(fileName);
				long fileLength = file.length();

				long minValidFileLength = 1024 * sampleRate * numChannels * 2;

				if (sampleRate != 44100 || numChannels != 2 || fileLength < minValidFileLength) {
					numBadFiles++;

					System.out.println(sampleRate + "\t" + numChannels + "\t" + fileLength + "\t" + minValidFileLength + "\t" + fileName);

					System.out.println("deleting file: " + fileName);

					if (deleteFilesARMED)
						file.delete();
				}

			}

		}
		System.out.println("numFiles = " + numFiles);
		System.out.println("numBadFiles = " + numBadFiles);
	}
}
