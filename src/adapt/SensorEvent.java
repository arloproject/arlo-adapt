package adapt;

public class SensorEvent implements Comparable {

	double time;
	int sensorIndex;
	String fileName;
	double fileTime;
	double frequency;
	int tagID;

	public SensorEvent(double time, int sensorIndex, String filePath, double fileTime,  double frequency, int tagID) {
		this.time = time;
		this.sensorIndex = sensorIndex;
		this.fileName = filePath;
		this.fileTime = fileTime;
		this.frequency = frequency;
		this.tagID = tagID;
	}

	public int compareTo(Object o) {

		if (this.time == ((SensorEvent) o).time)
			return 0;
		
		if (this.time < ((SensorEvent) o).time)
			return -1;
		else
			return 1;
				
	}

}
