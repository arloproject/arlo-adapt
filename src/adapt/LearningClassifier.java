/**
 * University of Illinois/NCSA Open Source License
 * 
 * Copyright (c) 2010, Board of Trustees-University of Illinois. All rights reserved.
 * 
 * Developed by:
 * 
 * David Tcheng Automated Learning Group National Center for Supercomputing Applications
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal with the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to
 * do so, subject to the following conditions:
 * 
 * * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimers.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimers in the documentation and/or other materials provided with the
 * distribution.
 * 
 * * Neither the names of Automated Learning Group, The National Center for Supercomputing Applications, or University of Illinois, nor the names of its contributors may be used to endorse or promote
 * products derived from this Software without specific prior written permission.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT
 * OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 */
package adapt;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class LearningClassifier {

	int numModels = -1;
	ADAPT adapts[];

	public LearningClassifier(String[] dataDirectoryPaths) {

		numModels = dataDirectoryPaths.length;

		adapts = new ADAPT[numModels];

		for (int i = 0; i < numModels; i++) {

			adapts[i] = createADAPT(dataDirectoryPaths[i]);
		}

	}

	ADAPT createADAPT(String dataDirectoryPath) {

		ADAPT adapt = new ADAPT();

		adapt.numThreads = 1;

		adapt.dataDirectoryPath = dataDirectoryPath;
		adapt.imageDirectoryPath = adapt.dataDirectoryPath + "images/";

		adapt.readClassNamesFromCSVFile();

		adapt.numColorsToUse = 3;

		adapt.maxXResolution = 2592;
		adapt.maxYResolution = 2592;
		adapt.maxWidth = adapt.maxXResolution;
		adapt.maxHeight = adapt.maxYResolution;
		adapt.textureXYResolution = 512;
		adapt.minShapeXYResolution = 512;
		adapt.sampleInterval = 32;
		adapt.subwindowSizeFactor = 1.0;

		adapt.distanceWeightingPower = (double) 13.0;

		adapt.overallIntensityDistributionWeight = 64; // food
		adapt.minNumIntensityBins = 2;
		adapt.maxNumIntensityBins = 15;
		adapt.numNumIntensityBins = adapt.maxNumIntensityBins - adapt.minNumIntensityBins + 1;
		adapt.intensityDistributionFeatureIndices = new int[adapt.numNumIntensityBins];
		adapt.intensityDistributionFeatureWeights = new double[adapt.numNumIntensityBins];
		adapt.intensityDistributionFeatureWeights[10] = (double) 1.0;
		for (int i = 0; i < adapt.numNumIntensityBins; i++) {
			adapt.intensityDistributionFeatureWeights[i] = 1.0;
		}

		adapt.overallGrossShapeWeight = 8; // food
		adapt.minGrossShapeResolution = 1;
		adapt.maxGrossShapeResolution = 11;
		adapt.numGrossShapeResolutions = adapt.maxGrossShapeResolution - adapt.minGrossShapeResolution + 1;
		adapt.grossShapeFeatureIndices = new int[adapt.numGrossShapeResolutions];
		adapt.grossShapeFeatureWeights = new double[adapt.numGrossShapeResolutions];
		adapt.grossShapeFeatureWeights[10] = (double) 1.0;
		for (int i = 0; i < adapt.numGrossShapeResolutions; i++) {
			adapt.grossShapeFeatureWeights[i] = 1.0;
		}

		adapt.overallTextureWeight = 4096; // food
		adapt.minTextureLineNumPixels = 1;
		adapt.maxTextureLineNumPixels = 11;
		adapt.numTextureLineNumPixels = adapt.maxTextureLineNumPixels - adapt.minTextureLineNumPixels + 1;
		adapt.textureLineFeatureIndices = new int[adapt.numTextureLineNumPixels];
		adapt.textureLineFeatureWeights = new double[adapt.numTextureLineNumPixels];
		adapt.textureLineFeatureWeights[(int) 7] = (double) 1.0;
		for (int i = 0; i < adapt.numTextureLineNumPixels; i++) {
			adapt.textureLineFeatureWeights[i] = 1.0;
		}

		/*******************/
		/* create examples */
		/*******************/

		String examplesPath = adapt.dataDirectoryPath + "examples.ser";

		adapt.loadExamples(examplesPath);

		/******************************************/
		/* change class names to dbIDs (temp fix) */
		/******************************************/

		String foodDBPath = adapt.dataDirectoryPath + "foodDB.xls";
		HashMap<String, String> classNameToFoodDBKey = new HashMap<String, String>();
		try {
			String[][] foodDB = (String[][]) IO.readDelimitedTable(foodDBPath, "\t").stringMatrix;

			for (int i = 0; i < foodDB.length; i++) {

				String foodDBKey = foodDB[i][0];
				String className = foodDB[i][1];

				System.out.println(className + " -> " + foodDBKey);

				classNameToFoodDBKey.put(className, foodDBKey);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (int outputIndex = 0; outputIndex < adapt.numOutputs; outputIndex++) {

			String oldName = adapt.outputNames[outputIndex];
			String newName = classNameToFoodDBKey.get(oldName);
			adapt.outputNames[outputIndex] = newName;

			if (newName == null) {
				adapt.fail(oldName + " not found");
			}
		}

		/* create space for one extra examples for classification and learning */

		int numExtra = 1000;
		Example[] newExamples = new Example[adapt.examples.length + numExtra];
		for (int i = 0; i < adapt.examples.length; i++) {
			newExamples[i] = adapt.examples[i];
		}
		adapt.examples = newExamples;
		adapt.maxNumExamples = newExamples.length;

		return adapt;
	}

	double[][] samples = null;
	double[][][] xycValues = null;
	double[][][] shapeValueSums = null;
	int[][][] shapeValueCounts = null;
	double[][][] textureXYCValues = null;
	double[][][] grossShapeValueSums = null;
	int[][][] grossShapeValueCounts = null;
	int[] pixelLineBinCounts = null;

	public LinkedHashMap<String, Double> getProbabilities(String modelName, String filePath) {

		int modelIndex = -1;
		
		if (modelName.equalsIgnoreCase("FruitVeg")) {
			modelIndex = 0;
		}
		if (modelName.equalsIgnoreCase("Starbucks")) {
			modelIndex = 1;
		}

		if (modelIndex == -1) {
			modelIndex = 0;
		}

		ADAPT adapt = adapts[modelIndex];

		synchronized (this) {

			// create example from image //

			adapt.maxNumSamples = adapt.maxXResolution * adapt.maxYResolution / adapt.sampleInterval;

			if (samples == null) {
				samples = new double[adapt.numColorsToUse][adapt.maxNumSamples];
			}
			if (xycValues == null) {
				xycValues = new double[adapt.maxWidth][adapt.maxHeight][adapt.numColorsToUse];
			}

			if (shapeValueSums == null) {
				shapeValueSums = new double[adapt.minShapeXYResolution][adapt.minShapeXYResolution][adapt.numColorsToUse];
				shapeValueCounts = new int[adapt.minShapeXYResolution][adapt.minShapeXYResolution][adapt.numColorsToUse];
				textureXYCValues = new double[adapt.textureXYResolution][adapt.textureXYResolution][adapt.numColorsToUse];
			}
			if (grossShapeValueSums == null) {
				grossShapeValueSums = new double[adapt.maxGrossShapeResolution][adapt.maxGrossShapeResolution][adapt.numColorsToUse];
				grossShapeValueCounts = new int[adapt.maxGrossShapeResolution][adapt.maxGrossShapeResolution][adapt.numColorsToUse];
			}

			if (pixelLineBinCounts == null) {
				int textureLineNumPixelsMinusOne = adapt.maxTextureLineNumPixels - 1;
				int textureLineNumValueBins = 2 << textureLineNumPixelsMinusOne;
				pixelLineBinCounts = new int[textureLineNumValueBins];
			}

			Experience experience = new Experience(adapt.numExamples, false, false, true);
			Example example = new Example(filePath, adapt.numExamples, adapt.numExamples, null, null);
			example.inputValues = new double[adapt.numInputs];
			example.outputValues = new double[adapt.numOutputs];
			example.filePath = filePath;
			example.groupIndex = adapt.numExamples;
			adapt.examples[adapt.numExamples] = example;

			adapt.createExample(experience, samples, xycValues, shapeValueSums, shapeValueCounts, textureXYCValues, grossShapeValueSums, grossShapeValueCounts, pixelLineBinCounts);

			ProblemSolver problemSovler = new ProblemSolver(adapt, 0);

			Problem problem = new Problem();

			problem.numClassesToUse = adapt.numOutputs;

			problem.classIndicesToUse = new ArrayList<Integer>();

			for (int i = 0; i < adapt.numOutputs; i++) {
				problem.classIndicesToUse.add(i);
			}

			problem.numTrainExamples = adapt.numExamples;
			problem.numTestExamples = 1;

			problem.trainGroupIndices = new ArrayList<Integer>();
			problem.trainExampleIndices = new ArrayList<Integer>();
			problem.testGroupIndices = new ArrayList<Integer>();
			problem.testExampleIndices = new ArrayList<Integer>();

			for (int i = 0; i < adapt.numExamples; i++) {
				problem.trainGroupIndices.add(i);
				problem.trainExampleIndices.add(i);
			}
			problem.testGroupIndices.add(adapt.numExamples);
			problem.testExampleIndices.add(adapt.numExamples);

			experience.problem = problem;
			problemSovler.solveProblem(experience);

			Result result = experience.result;

			double[] probabilities = result.testExamplePredictedOutputValues[0];

			double sum = (double) 0.0;
			for (int i = 0; i < adapt.numOutputs; i++) {
				sum += probabilities[i];
			}
			for (int i = 0; i < adapt.numOutputs; i++) {
				probabilities[i] /= sum;
			}

			// experience.result

			ProbabilityAndKey[] probabilitiesAndKeys = new ProbabilityAndKey[adapt.numOutputs];

			for (int i = 0; i < adapt.numOutputs; i++) {
				probabilitiesAndKeys[i] = new ProbabilityAndKey((double) probabilities[i], adapt.outputNames[i]);
			}

			Arrays.sort(probabilitiesAndKeys);

			LinkedHashMap<String, Double> hashMap = new LinkedHashMap();

			for (int i = 0; i < adapt.numOutputs; i++) {
				hashMap.put(probabilitiesAndKeys[i].key, probabilitiesAndKeys[i].probability);
			}

			return hashMap;
		}
	}

	public LinkedHashMap<String, Double> getProbabilities(String modelName, File file) {
		return getProbabilities(modelName, file.getAbsolutePath());
	}

}

class ProbabilityAndKey implements Comparable<ProbabilityAndKey> {

	double probability;
	String key;

	ProbabilityAndKey(double probability, String index1) {
		this.probability = probability;
		this.key = index1;
	}

	public int compareTo(ProbabilityAndKey o) {

		if (this.probability < o.probability)
			return 1;
		if (this.probability > o.probability)
			return -1;

		return 0;
	}

}
