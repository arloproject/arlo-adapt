package adapt;

import java.io.*;

public class JavaRunCommand {

	public static void main(String args[]) {

		String s = null;

		try {

			// run the Unix "ps -ef" command
			// using the Runtime exec method:

			String imagePath = "/media/Patriot/cline/sample/FBIS#FICHE#DONEFROMBEFORE#6-27-2007#Scan_18195-014.png";
			String textPath = "/media/Patriot/cline/scan";

			Runtime runtime = Runtime.getRuntime();
			String[] envp = new String[] { "LD_LIBRARY_PATH=/lib:/home/dtcheng/cuda:/usr/local/cuda/lib64:/home/dtcheng/aparapi:/home/dtcheng/tesseract-3.01/api/.libs:/usr/local/lib/" };
			Process p = Runtime.getRuntime().exec("/usr/local/bin/tesseract " + imagePath + " " + textPath, envp);

			BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));

			BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));

			// read the output from the command
			System.out.println("Here is the standard output of the command:\n");
			while ((s = stdInput.readLine()) != null) {
				System.out.println(s);
			}

			// read any errors from the attempted command
			System.out.println("Here is the standard error of the command (if any):\n");
			while ((s = stdError.readLine()) != null) {
				System.out.println(s);
			}

			SimpleTable table = IO.readDelimitedTable(textPath + ".txt", "\t");

			for (int i = 0; i < table.numDataRows; i++) {
				System.out.println(table.getString(i, 0));
			}

			System.exit(0);
		} catch (IOException e) {
			System.out.println("exception happened - here's what I know: ");
			e.printStackTrace();
			System.exit(-1);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}