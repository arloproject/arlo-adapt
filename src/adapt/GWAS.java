package adapt;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Random;

public class GWAS {

	static double weightPower = 0.0;
	static boolean similarityScoreIsD2Z = true;
	static boolean similarityScoreIsMatchCount = !similarityScoreIsD2Z;
	static boolean similarityScoreIsMatchCountMinusExpectation = !similarityScoreIsD2Z;

	static boolean predictGeneActivity = true;

	//
	// define range of genes to group together //
	static int bestPickStartIndex = 0; // 0 is best pick, 1 is 2nd best, etc.
	static int bestPickEndIndex = 1; // numGenes = bestPickEndIndex - bestPickStartIndex
	//

	static boolean useCache = true;
	// static int maxNumFilesToProcess = 13000;
	static int maxNumFilesToProcess = 10448;

	static int targetNumRandomSelectionEvaluations = 1000;
	static int minNumObservations = 100;

	static boolean rejectPromotersWithAmbigousBPs = true;
	static boolean reportETA = true;

	static int numThreads = 1;
	static int threadIndex = 0;

	static int tssdbIDColumnIndex = 0;
	static int entrezIDColumnIndex = 1;
	static int chromosomeColumnIndex = 5;
	static int strandColumnIndex = 6;
	static int positionColumnIndex = 7;
	static int sequenceColumnIndex = 13;

//	static String dbtssPath = "hg19promoter.tab";
	 static String dbtssPath = "mm9promoter.tab";

	static String[] symbolStrings = new String[] { "A", "C", "G", "T" };

	static boolean useGeneratedData = false;
	static boolean useProbeTestData = false;
	static boolean useGenomeData = true;

	static boolean reportTopScores = false;
	static int numTopScoresToReport = 30;
	static boolean useCrabgrass = true;
	static boolean useAccordion = true;

	// static int generationRandomSeed = 123456;
	// static Random generationRandom = new Random(generationRandomSeed);
	static Random generationRandom = new Random();

	static boolean simulate = false;
	static int numSimulationTrials = 100000;
	static double[] simulatedExpectedValues = new double[numSimulationTrials];

	static int sequenceLength = 1201;
	// static int sequenceLength = 1201;
	// static int sampleNumGenes = 32122;
	// static int sampleNumGenes = 96807;
	// static int sampleNumGenes = 48940;
	// static int sampleNumGenes = 48938;
	// static int sampleNumGenes = 96807;
//	static int sampleNumGenes = 34170; // human
	 static int sampleNumGenes = 21978; // mouse

	// static int sampleNumGenes = (numRealProbeSequences + numRealTestSequences);
	static int targetNumGenes = sampleNumGenes;
	// static int targetNumGenes = 48940;
	static int kSize = 7;
	static int numKmers;
	static int numWindowsToGenerate;

	static int[] sequence;
	static int[] sequenceA;
	static int[] sequenceB;
	static short[] wordCounts;
	static short[] wordCountsA;
	static short[] wordCountsB;

	static long generationStartTime;

	static short[][] geneKmerCounts;

	static double[] similarityScores;

	static int numSymbols;
	static double[] symbolProbababilities;
	static int[][] geneSymbolCounts;
	static double[][] geneSymbolProbababilities;
	static String[] geneTSSDBIDs;
	static String[] geneEntrezIDs;
	static String[] geneChromosomeIDs;
	static String[] genePositions;

	static ScoreAndIndex[] similaritiesAndKeys;

	public static void main(String[] args) throws Exception {

		if (args.length >= 1) {
			similarityScoreIsD2Z = Boolean.parseBoolean(args[0]);
			similarityScoreIsMatchCount = Boolean.parseBoolean(args[1]);
			similarityScoreIsMatchCountMinusExpectation = Boolean.parseBoolean(args[2]);
		}

		if (args.length >= 4) {
			kSize = Integer.parseInt(args[3]);
		}

		int bestPickRankStart = -1;
		int bestPickRankEnd = -1;
		if (args.length >= 5) {
			bestPickRankStart = Integer.parseInt(args[4]);
			bestPickRankEnd = Integer.parseInt(args[5]);
			bestPickStartIndex = bestPickRankStart - 1;
			bestPickEndIndex = bestPickRankEnd;
		}

		if (args.length >= 7) {
			weightPower = Double.parseDouble(args[6]);
		}
	
		if (args.length >= 8) {
			reportTopScores = Boolean.parseBoolean(args[7]);
		}

		if (args.length >= 10) {
			numThreads = Integer.parseInt(args[8]);
			threadIndex = Integer.parseInt(args[9]);
		}

		 predictGeneActivity = true;
		if (bestPickRankStart <= 0 | bestPickRankEnd <= 0) {
			predictGeneActivity = false;
		}
		
		System.out.println("similarityScoreIsD2Z                        = " + similarityScoreIsD2Z);
		System.out.println("similarityScoreIsMatchCount                 = " + similarityScoreIsMatchCount);
		System.out.println("similarityScoreIsMatchCountMinusExpectation = " + similarityScoreIsMatchCountMinusExpectation);
		System.out.println("kSize                                       = " + kSize);
		System.out.println("bestPickRankStart                           = " + bestPickRankStart);
		System.out.println("bestPickRankEnd                             = " + bestPickRankEnd);
		System.out.println("bestPickStartIndex                          = " + bestPickStartIndex);
		System.out.println("bestPickEndIndex                            = " + bestPickEndIndex);
		System.out.println("predictGeneActivity                         = " + predictGeneActivity);

		numKmers = (int) Math.pow(4, kSize);
		// int numWindowsToGenerate = sequenceLength - kSize + 1;
		numWindowsToGenerate = sequenceLength - kSize + 1 - 3;

		sequence = new int[sequenceLength];
		sequenceA = new int[sequenceLength];
		sequenceB = new int[sequenceLength];
		wordCounts = new short[numKmers];
		wordCountsA = new short[numKmers];
		wordCountsB = new short[numKmers];

		generationStartTime = System.currentTimeMillis();

		geneKmerCounts = new short[sampleNumGenes][numKmers];

		similarityScores = new double[sampleNumGenes];

		numSymbols = 4;
		symbolProbababilities = new double[numSymbols];
		geneSymbolCounts = new int[sampleNumGenes][numSymbols];
		geneSymbolProbababilities = new double[sampleNumGenes][numSymbols];
		geneTSSDBIDs = new String[sampleNumGenes];
		geneEntrezIDs = new String[sampleNumGenes];
		geneChromosomeIDs = new String[sampleNumGenes];
		genePositions = new String[sampleNumGenes];

		similaritiesAndKeys = new ScoreAndIndex[sampleNumGenes];

		int maxNumExperiments = maxNumFilesToProcess;
		int numExperiments = 0;
		int maxNumGenes = 25000;
		int numGenes = 0;

		float[][] geneActivities = new float[maxNumExperiments][maxNumGenes];

		for (int i = 0; i < geneActivities.length; i++) {
			Arrays.fill(geneActivities[i], Float.NaN);
		}

		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//

		HashMap<Integer, Integer> entrezNumberToEntrezGeneSymbolIndex = new HashMap<Integer, Integer>();
		int[] entrezGeneSymbolIndexToEntrezNumber = new int[maxNumGenes];
		String[] entrezGeneSymbolIndexToGeneSymbol = new String[maxNumGenes];
		int numGoodFiles = 0;

		if (!useCache) {
			// read table matching entrez ids to gene symbols

			SimpleTable entrezToGeneSymbolTable = null;
			HashMap<String, Integer> geneSymbolToEntrezNumber = new HashMap<String, Integer>();
			try {
				entrezToGeneSymbolTable = IO.readDelimitedTable("entrezToGeneSymbol.tab", "\t", true);
			} catch (Exception e) {
				System.exit(1);
			}

			for (int r = 0; r < entrezToGeneSymbolTable.numDataRows; r++) {
				int entrezNumber = entrezToGeneSymbolTable.getInt(r, 0);
				String geneSymbol = entrezToGeneSymbolTable.getString(r, 1).toUpperCase();
				geneSymbolToEntrezNumber.put(geneSymbol, entrezNumber);
			}

			// read RNA expression data for machine learning //

			String[] pathStrings = IO.getPathStrings("gwas/");
			int numFiles = pathStrings.length;

			// String[] pathStrings = IO.getPathStrings("marked");

			// Random random = new Random(111111111);
			Random random = new Random();
			Utility.randomizeStringArrayInPlace(random, pathStrings, numFiles);

			numGoodFiles = 0;

			entrezNumberToEntrezGeneSymbolIndex = new HashMap<Integer, Integer>();
			entrezGeneSymbolIndexToEntrezNumber = new int[maxNumGenes];
			entrezGeneSymbolIndexToGeneSymbol = new String[maxNumGenes];

			int geneLookupCount = 0;
			int geneNotFoundCount = 0;

			numFiles = Math.min(numFiles, maxNumFilesToProcess);

			for (int f = 0; f < numFiles; f++) {

				String path = pathStrings[f];
				System.out.println();
				System.out.println("working on file #" + (f + 1) + " of " + numFiles + " path = " + path);

				SimpleTable table = null;

				try {
					table = IO.readDelimitedTable(path, "\t", false);
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("skipping (could not read table) file #" + (f + 1) + " of " + numFiles + " path = " + path);
					continue;
				}

				if (table.numDataRows < 1000) {
					System.out.println("skipping (too few lines) file #" + (f + 1) + " of " + numFiles + " path = " + path);
					continue;
				}

				if (table.getString(0, 0).indexOf("!") != 0) {
					System.out.println("skipping (wrong format) file #" + (f + 1) + " of " + numFiles + " path = " + path);
					continue;
				}

				int headerRowIndex = -1;
				for (int r = 0; r < table.numDataRows; r++) {

					if (table.getString(r, 0).indexOf("!") != 0) {
						headerRowIndex = r;
						break;
					}
				}
				if (headerRowIndex == -1) {
					System.out.println("skipping (no header) file #" + (f + 1) + " of " + numFiles + " path = " + path);
					continue;
				}

				if (!table.getString(headerRowIndex, 0).equalsIgnoreCase("spot")) {
					System.out.println("skipping (no 'spot' in 1st column) file #" + (f + 1) + " of " + numFiles + " path = " + path);
					continue;
				}

				int numHeaderColumns = table.getNumColumns(headerRowIndex);

				// find gene bank accession number

				int accessionNumberColumnIndex = -1;

				for (int c = 0; c < numHeaderColumns; c++) {

					String string = table.getString(headerRowIndex, c);

					if (string.equals("Accession")) {
						accessionNumberColumnIndex = c;
						break;
					}
					if (string.equals("GenBank Accession")) {
						accessionNumberColumnIndex = c;
						break;
					}
				}

				if (accessionNumberColumnIndex == -1) {
					System.out.println("skipping ( no GenBank Accession Column) file #" + (f + 1) + " of " + numFiles + " path = " + path);

					for (int c = 0; c < numHeaderColumns; c++) {

						String string = table.getString(headerRowIndex, c);
						System.out.println(c + "\t" + string);

						for (int i = 0; i < string.length(); i++) {
							System.out.println(i + "\t" + string.charAt(i));
						}

					}

					continue;
				}

				int geneSymbolColumnIndex = -1;

				for (int c = 0; c < numHeaderColumns; c++) {

					String string = table.getString(headerRowIndex, c);

					if (string.equals("Gene Symbol")) {
						geneSymbolColumnIndex = c;
						break;
					}
				}

				// System.out.println("geneNameColumnIndex = " + geneNameColumnIndex);

				if (geneSymbolColumnIndex == -1) {
					System.out.println("skipping ( no Gene Symbol Column) file #" + (f + 1) + " of " + numFiles + " path = " + path);

					for (int c = 0; c < numHeaderColumns; c++) {

						String string = table.getString(headerRowIndex, c);
						System.out.println(c + "\t" + string);

						for (int i = 0; i < string.length(); i++) {
							System.out.println(i + "\t" + string.charAt(i));
						}

					}

					continue;
				}

				int activityLevelColumnIndex = -1;

				for (int c = 0; c < numHeaderColumns; c++) {

					String string = table.getString(headerRowIndex, c);

					if (string.equals("Log(base2) of R/G Normalized Ratio (Mean)")) {
						activityLevelColumnIndex = c;
						break;
					}
					if (string.equals("LOG_RAT2N_MEAN")) {
						activityLevelColumnIndex = c;
						break;
					}
				}

				// System.out.println("activityLevelColumn = " + activityLevelColumnIndex);

				if (activityLevelColumnIndex == -1) {

					System.out.println("skipping ( no GenBank Accession Column) file #" + (f + 1) + " of " + numFiles + " path = " + path);

					for (int c = 0; c < numHeaderColumns; c++) {

						String string = table.getString(headerRowIndex, c);
						System.out.println(c + "\t" + string);

						// for (int i = 0; i < string.length(); i++) {
						// System.out.println(i + "\t" + string.charAt(i));
						// }

					}

					continue;
				}

				//
				//
				//
				//
				//
				//

				// record activity data

				if (true) {

					for (int r = headerRowIndex + 1; r < table.numDataRows; r++) {

						String geneSymbol = table.getString(r, geneSymbolColumnIndex);
						geneSymbol = geneSymbol.toUpperCase();
						if (geneSymbol.indexOf(":") != -1) {
							geneSymbol = geneSymbol.substring(0, geneSymbol.indexOf(":"));
						}

						geneLookupCount++;
						Integer entrezNumber = geneSymbolToEntrezNumber.get(geneSymbol);
						if (entrezNumber == null) {
							geneNotFoundCount++;
							continue;
						}

						String activityLevelString = table.getString(r, activityLevelColumnIndex);

						if (activityLevelString == null || activityLevelString.length() == 0) {
							continue;
						}

						float activityLevel = table.getFloat(r, activityLevelColumnIndex);

						// System.out.printf("%s\t%f\n", geneName, activityLevel);

						Integer geneIndex = entrezNumberToEntrezGeneSymbolIndex.get(entrezNumber);
						if (geneIndex == null) {
							geneIndex = numGenes;
							entrezNumberToEntrezGeneSymbolIndex.put(entrezNumber, numGenes);
							entrezGeneSymbolIndexToEntrezNumber[numGenes] = entrezNumber;
							entrezGeneSymbolIndexToGeneSymbol[numGenes] = geneSymbol;
							numGenes++;
						}

						geneActivities[numGoodFiles][geneIndex] = activityLevel;

					}
				}

				//
				//
				//
				//
				//
				//

				numGoodFiles++;
				System.out.println("numGoodFiles = " + numGoodFiles);
				float fractionGoodFiles = (float) numGoodFiles / (f + 1);
				System.out.println("fractionGoodFiles = " + fractionGoodFiles);

				double fractionNotFound = (double) geneNotFoundCount / geneLookupCount;
				System.out.println("fractionNotFound = " + fractionNotFound);

				System.out.println("numGenes = " + numGenes);

			}

			Object[] objects = new Object[] { numGoodFiles, numGenes, geneActivities, entrezNumberToEntrezGeneSymbolIndex, entrezGeneSymbolIndexToEntrezNumber, entrezGeneSymbolIndexToGeneSymbol };

			IO.writeObject("gwasActivityData.ser", objects);
		} else {

			Object[] objects = (Object[]) IO.readObject("gwasActivityData.ser");

			int index = 0;

			numGoodFiles = (Integer) objects[index++];
			numGenes = (Integer) objects[index++];
			geneActivities = (float[][]) objects[index++];
			entrezNumberToEntrezGeneSymbolIndex = (HashMap<Integer, Integer>) objects[index++];
			entrezGeneSymbolIndexToEntrezNumber = (int[]) objects[index++];
			entrezGeneSymbolIndexToGeneSymbol = (String[]) objects[index++];

		}

		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//

		if (useGeneratedData) {
			for (int geneIndex = 0; geneIndex < sampleNumGenes; geneIndex++) {
				double probabilitySum = 0.0f;
				for (int symbolIndex = 0; symbolIndex < numSymbols; symbolIndex++) {
					symbolProbababilities[symbolIndex] = generationRandom.nextDouble();
					probabilitySum += symbolProbababilities[symbolIndex];
				}
				for (int symbolIndex = 0; symbolIndex < numSymbols; symbolIndex++) {
					geneSymbolProbababilities[geneIndex][symbolIndex] = symbolProbababilities[symbolIndex] / probabilitySum;
				}
			}

			for (int geneIndex = 0; geneIndex < sampleNumGenes; geneIndex++) {

				for (int i = 0; i < sequenceLength; i++) {
					double randomValue = Math.random();
					for (int symbolIndex = 0; symbolIndex < numSymbols; symbolIndex++) {
						randomValue -= geneSymbolProbababilities[geneIndex][symbolIndex];
						if (randomValue < 0.0) {
							sequence[i] = symbolIndex;
							break;
						}
					}
				}

				for (int i = 0; i < numKmers; i++) {
					wordCounts[i] = 0;
				}

				for (int i = 0; i < numWindowsToGenerate; i++) {
					int kmerIndex = 0;
					for (int j = 0; j < kSize; j++) {
						kmerIndex *= numSymbols;
						kmerIndex += sequence[j + i];
					}
					wordCounts[kmerIndex]++;
				}

				for (int i = 0; i < numKmers; i++) {
					geneKmerCounts[geneIndex][i] = wordCounts[i];
				}
			}

			long generationEndTime = System.currentTimeMillis();
			double generationDuration = (generationEndTime - generationStartTime) / 1000.0f;
		}

		else

		if (useProbeTestData) {

			for (int geneIndex = 0; geneIndex < sampleNumGenes; geneIndex++) {

				GwasTestSequences.getSequenceData(sequence, geneIndex);

				for (int i = 0; i < sequenceLength; i++) {
					geneSymbolCounts[geneIndex][sequence[i]]++;
				}

				for (int symbolIndex = 0; symbolIndex < numSymbols; symbolIndex++) {
					geneSymbolProbababilities[geneIndex][symbolIndex] = (double) geneSymbolCounts[geneIndex][symbolIndex] / sequenceLength;
				}

				for (int i = 0; i < numKmers; i++) {
					wordCounts[i] = 0;
				}

				for (int i = 0; i < numWindowsToGenerate; i++) {
					int kmerIndex = 0;
					for (int j = 0; j < kSize; j++) {
						kmerIndex *= numSymbols;
						kmerIndex += sequence[j + i];
					}
					wordCounts[kmerIndex]++;
				}

				for (int i = 0; i < numKmers; i++) {
					geneKmerCounts[geneIndex][i] = wordCounts[i];
				}
			}

			int[] testCounts = GwasTestSequences.getTestCounts();
			int testCount = 0;
			int gwasCount = 0;
			for (int i = 0; i < numKmers; i++) {

				String kmerString = "";
				int index = i;
				for (int j = 0; j < kSize; j++) {

					int charIndex = index % numSymbols;
					index /= numSymbols;

					kmerString = symbolStrings[charIndex] + kmerString;
				}

				System.out.println(kmerString + "\t" + testCounts[i] + "\t" + geneKmerCounts[0][i]);
				if (testCounts[i] != geneKmerCounts[0][i]) {
					System.out.println("######### Error! testCounts do not match GWAS counts");
					// System.exit(1);
				}
				testCount += testCounts[i];
				gwasCount += geneKmerCounts[0][i];

			}
			System.out.println("testCount   = " + testCount);
			System.out.println("gwasCount   = " + gwasCount);
		}

		else

		if (useGenomeData) {

			SimpleTable table = null;
			try {
				table = IO.readDelimitedTable(dbtssPath, "\t", false);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			int numDataRows = table.numDataRows;
			System.out.println("numDataRows   = " + numDataRows);

			int geneIndex = 0;
			int numN = 0;
			int numBP = 0;
			int numStrands = 0;
			for (int rowIndex = 0; rowIndex < numDataRows; rowIndex++) {

				String tssdbID = table.getString(rowIndex, tssdbIDColumnIndex);
				String entrezID = table.getString(rowIndex, entrezIDColumnIndex);
				// System.out.println("tssDBID = " + tssdbID);
				// System.out.println("entrezID = " + entrezID);

				String chromosomeID = table.getString(rowIndex, chromosomeColumnIndex);
				int strandIndex = table.getInt(rowIndex, strandColumnIndex);
				String genePositionString = table.getString(rowIndex, positionColumnIndex);
				String bpString = table.getString(rowIndex, sequenceColumnIndex);

				// if (strandIndex == 0) {
				// continue;
				// }
				if (entrezID.length() == 0 || (rejectPromotersWithAmbigousBPs && ((bpString.indexOf("N") != -1) || (bpString.indexOf("n") != -1)))) {
					continue;
				}

				numStrands++;

				// if (genePositionString.length() == 0)
				// continue;

				// System.out.println("genePositionString = " + genePositionString);
				// System.out.println("chromosomeID = " + chromosomeID);

				geneTSSDBIDs[geneIndex] = tssdbID;
				geneEntrezIDs[geneIndex] = entrezID;

				geneChromosomeIDs[geneIndex] = chromosomeID;

				genePositions[geneIndex] = genePositionString;

				// System.out.println("genePosition = " + genePosition);

				byte[] stringBytes = bpString.getBytes();
				// System.out.println("stringBytes.length   = " + stringBytes.length);

				int stringLength = bpString.length();
				int sequenceLength = sequence.length;

				// if (stringLength != sequenceLength) {
				//
				// System.out.println("######### Error! stringLength != sequenceLength");
				// System.out.println("######### Error! stringLength  = " + stringLength);
				// System.out.println("######### Error! sequenceLength  = " + sequenceLength);
				// System.exit(1);
				// }

				for (int i = 0; i < sequence.length; i++) {
					switch (stringBytes[i]) {

					case (byte) 'A':
						sequence[i] = 0;
						break;
					case (byte) 'C':
						sequence[i] = 1;
						break;
					case (byte) 'G':
						sequence[i] = 2;
						break;
					case (byte) 'T':
						sequence[i] = 3;
						break;

					case (byte) 'a':
						sequence[i] = 0;
						break;
					case (byte) 'c':
						sequence[i] = 1;
						break;
					case (byte) 'g':
						sequence[i] = 2;
						break;
					case (byte) 't':
						sequence[i] = 3;
						break;

					case (byte) 'N':
						sequence[i] = (int) (Math.random() * numSymbols);
						numN++;
						break;
					case (byte) 'n':
						sequence[i] = (int) (Math.random() * numSymbols);
						numN++;
						break;

					default:

						System.out.println("bad bp character = " + (char) stringBytes[i]);

					}
					numBP++;

				}

				for (int i = 0; i < sequenceLength; i++) {
					geneSymbolCounts[geneIndex][sequence[i]]++;
				}

				for (int symbolIndex = 0; symbolIndex < numSymbols; symbolIndex++) {
					geneSymbolProbababilities[geneIndex][symbolIndex] = (double) geneSymbolCounts[geneIndex][symbolIndex] / sequenceLength;
				}

				for (int i = 0; i < numKmers; i++) {
					wordCounts[i] = 0;
				}

				for (int i = 0; i < numWindowsToGenerate; i++) {
					int kmerIndex = 0;
					for (int j = 0; j < kSize; j++) {
						kmerIndex *= numSymbols;
						kmerIndex += sequence[j + i];
					}
					wordCounts[kmerIndex]++;
				}

				for (int i = 0; i < numKmers; i++) {
					geneKmerCounts[geneIndex][i] = wordCounts[i];
				}

				geneIndex++;
			}
			System.out.println("numStrands   = " + numStrands);

			// System.exit(0);
			double fractionN = (double) numN / numBP;
			System.out.println("numN   = " + numN);
			System.out.println("numBP   = " + numBP);
		}

		long generationEndTime = System.currentTimeMillis();
		double generationDuration = (generationEndTime - generationStartTime) / 1000.0f;

		long comparisonStartTime = System.currentTimeMillis();

		int numGenesAnalyzed = 0;
		int comparisonCount = 0;

		for (int geneIndex = 0; geneIndex < sampleNumGenes; geneIndex++) {
			similaritiesAndKeys[geneIndex] = new ScoreAndIndex(Float.NaN, -1);
		}

		int[] randomizedGene1Indices = Utility.randomIntArray(-1, sampleNumGenes);

		int correlationComparisonCount = 0;

		double deltaCorrelationSum = 0;
		double deltaCorrelationMagnitudeSum = 0;
		double seedVSBestCorrelationSum = 0;
		double seedVSRandomCorrelationSum = 0;

		double[] deltaCorrelationObservations = new double[sampleNumGenes];
		int numDeltaCorrelationObservations = 0;
		double[] deltaCorrelationMagnitudeObservations = new double[sampleNumGenes];
		int numDeltaCorrelationMagnitudeObservations = 0;

		double[] probeGeneActivities = new double[maxNumFilesToProcess];
		double[] bestGeneActivities = new double[maxNumFilesToProcess];
		double[] randomGeneActivities = new double[maxNumFilesToProcess];

		double[] seedVSRandomGeneActivitySeedObservations = new double[maxNumFilesToProcess];
		double[] seedVSRandomGeneActivityRandomObservations = new double[maxNumFilesToProcess];

		/*********************************************************************/
		/* main loop doing d2z computation and comparison with gene activity */
		/* gene1 is the probe gene which is compared to all other genes */
		/*********************************************************************/

		for (int gene1IndexIndex = 0; gene1IndexIndex < sampleNumGenes; gene1IndexIndex++) {

			int gene1Index = randomizedGene1Indices[gene1IndexIndex];

			if (gene1IndexIndex % numThreads != threadIndex)
				continue;

			// similarityScores[gene1Index][gene1Index] = Float.POSITIVE_INFINITY;

			for (int gene2Index = 0; gene2Index < sampleNumGenes; gene2Index++) {

				double similarityScore = Double.NEGATIVE_INFINITY;

				// if (gene1Index != gene2Index) {

				int matchCount = 0;
				for (int i = 0; i < numKmers; i++) {
					matchCount += geneKmerCounts[gene1Index][i] * geneKmerCounts[gene2Index][i];
				}

				// compute indices

				double[] q1 = geneSymbolProbababilities[gene1Index];
				double[] q2 = geneSymbolProbababilities[gene2Index];

				// double originalComputedExpectation = 1.0;
				// double originalComputedVariance = 1.0;
				// double originalComputedStandardDeviation = 1.0;

				double originalComputedExpectation = computeExpectation(sequenceLength, sequenceLength, kSize, q1, q2);
				double originalComputedVariance = computeVariance(sequenceLength, sequenceLength, kSize, q1, q2);
				double originalComputedStandardDeviation = Math.sqrt(originalComputedVariance);

				// simulate to measure expected value and std //
				if (simulate) {

					for (int simulationTrialIndex = 0; simulationTrialIndex < numSimulationTrials; simulationTrialIndex++) {

						// initialize window A //
						for (int i = 0; i < sequenceLength; i++) {
							double randomValue = Math.random();
							for (int symbolIndex = 0; symbolIndex < numSymbols; symbolIndex++) {
								randomValue -= geneSymbolProbababilities[gene1Index][symbolIndex];
								if (randomValue < 0.0) {
									sequenceA[i] = symbolIndex;
									break;
								}
							}
						}
						// initialize window B //
						for (int i = 0; i < sequenceLength; i++) {
							double randomValue = Math.random();
							for (int symbolIndex = 0; symbolIndex < numSymbols; symbolIndex++) {
								randomValue -= geneSymbolProbababilities[gene2Index][symbolIndex];
								if (randomValue < 0.0) {
									sequenceB[i] = symbolIndex;
									break;
								}
							}
						}

						int numWindowsToGenerate = sequenceLength - kSize + 1;

						for (int i = 0; i < numKmers; i++) {
							wordCountsA[i] = 0;
							wordCountsB[i] = 0;
						}

						// compute word frequencies for A
						for (int i = 0; i < numWindowsToGenerate; i++) {
							int kmerIndex = 0;
							for (int j = 0; j < kSize; j++) {
								kmerIndex *= numSymbols;
								kmerIndex += sequenceA[j + i];
							}
							wordCountsA[kmerIndex]++;
						}

						// compute word frequencies for B
						for (int i = 0; i < numWindowsToGenerate; i++) {
							int kmerIndex = 0;
							for (int j = 0; j < kSize; j++) {
								kmerIndex *= numSymbols;
								kmerIndex += sequenceB[j + i];
							}
							wordCountsB[kmerIndex]++;
						}

						int simulatedMatchCount = 0;
						for (int i = 0; i < numKmers; i++) {
							simulatedMatchCount += wordCountsA[i] * wordCountsB[i];
						}

						simulatedExpectedValues[simulationTrialIndex] = simulatedMatchCount;
					}

					double expectedValueSum = 0.0;
					for (int simulationTrialIndex = 0; simulationTrialIndex < numSimulationTrials; simulationTrialIndex++) {
						expectedValueSum += simulatedExpectedValues[simulationTrialIndex];
					}

					double simulatedExpectedValue = expectedValueSum / numSimulationTrials;

					double varianceSum = 0.0;
					for (int simulationTrialIndex = 0; simulationTrialIndex < numSimulationTrials; simulationTrialIndex++) {
						double difference = simulatedExpectedValue - simulatedExpectedValues[simulationTrialIndex];
						varianceSum += difference * difference;
					}
					double simulatedStandardDeviation = Math.sqrt(varianceSum / numSimulationTrials);

					System.out.println("DATA\t" + originalComputedExpectation + "\t" + simulatedExpectedValue + "\t" + originalComputedStandardDeviation + "\t" + simulatedStandardDeviation);
				}

				if (similarityScoreIsD2Z)
					similarityScore = (float) ((matchCount - originalComputedExpectation) / originalComputedStandardDeviation);

				if (similarityScoreIsMatchCount)
					similarityScore = (float) (matchCount);

				if (similarityScoreIsMatchCountMinusExpectation)
					similarityScore = (float) (matchCount - originalComputedExpectation);

				// } else {
				// similarityScore = Double.NEGATIVE_INFINITY;
				// }

				similarityScores[gene2Index] = similarityScore;

				comparisonCount++;

			}

			numGenesAnalyzed++;

			long comparisonEndTime = System.currentTimeMillis();

			double comparisonDuration = (comparisonEndTime - comparisonStartTime) / 1000.0f;
			double numGenesAnalyzedPerSecond = numGenesAnalyzed / comparisonDuration;
			double estimatedAnalysisDuration = sampleNumGenes / numThreads / numGenesAnalyzedPerSecond;

			if (reportETA) {
				System.out.println("numGenesAnalyzedPerSecond     = " + numGenesAnalyzedPerSecond);
				System.out.println("estimatedAnalysisDuration (s) = " + estimatedAnalysisDuration);
				System.out.println("estimatedAnalysisDuration (h) = " + estimatedAnalysisDuration / 3600.0);
				System.out.println("estimatedAnalysisDuration (d) = " + estimatedAnalysisDuration / 3600.0 / 24.0);
			}
			// int numGenesToReport = -1;
			// if (useProbeTestData) {
			// numGenesToReport = 1;
			// } else if (useGeneratedData) {
			// numGenesToReport = sampleNumGenes;
			// } else if (useGenomeData) {
			// numGenesToReport = sampleNumGenes;
			// }
			// sort and report top 30 D2z scores //

			double selfSimilarityScore = similarityScores[gene1Index];
			// System.out.println("selfSimilarityScore = " + selfSimilarityScore);

			for (int gene2Index = 0; gene2Index < sampleNumGenes; gene2Index++) {
				similaritiesAndKeys[gene2Index].score = similarityScores[gene2Index];
				similaritiesAndKeys[gene2Index].index = gene2Index;
			}

			if (useGeneratedData || useGenomeData)
				Arrays.sort(similaritiesAndKeys);

			// boolean reportT

			if (reportTopScores) {
				for (int i = 0; i <= numTopScoresToReport; i++) {
					int gene2Index = similaritiesAndKeys[i].index;
					double baseScore = similaritiesAndKeys[i].score;
					double scoreMinusSelfSimilarityScore = (baseScore - selfSimilarityScore);

					// if (scoreMinusSelfSimilarityScore <= 0.0) {
					// continue;
					// }
					// System.out.println("TOP" + "\t" + gene1Index + "\t" + i + "\t" + gene2Index + "\t" + baseScore + "\t" + scoreMinusSelfSimilarityScore + "\t" + geneTSSDBIDs[gene1Index] + "\t" + geneTSSDBIDs[gene2Index] + "\t" +
					// geneEntrezIDs[gene1Index]
					// + "\t" + geneEntrezIDs[gene2Index] + "\t" + geneChromosomeIDs[gene1Index] + "\t" + geneChromosomeIDs[gene2Index] + "\t" + genePositions[gene1Index] + "\t" + genePositions[gene2Index]);

					System.out.printf(
							"TOP\t%d\t%d\t%d\t%f\t%f\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n", //
							gene1Index, i, gene2Index, baseScore, scoreMinusSelfSimilarityScore, geneTSSDBIDs[gene1Index], geneTSSDBIDs[gene2Index], geneEntrezIDs[gene1Index], geneEntrezIDs[gene2Index], geneChromosomeIDs[gene1Index],
							geneChromosomeIDs[gene2Index], genePositions[gene1Index], genePositions[gene2Index]);

				}
			}

			if (predictGeneActivity == false)
				continue;

			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//

			//
			
			
			if (!predictGeneActivity) {
				continue;
			}

			/**********************************************************************************************************************************************/
			/* test hypothesis that high d2z scores infer gene co-regulation by measuring gene activity correlation looking for up and up|down regulation */
			/**********************************************************************************************************************************************/

			int probeGWASGeneIndex = gene1Index;
			int probeGeneEntrezNumber = Integer.parseInt(geneEntrezIDs[probeGWASGeneIndex]);
			String probeGeneSymbol = null;
			int probeEntrezGeneSymbolIndex = -1;

			Integer probleSymbolIndex = entrezNumberToEntrezGeneSymbolIndex.get(probeGeneEntrezNumber);

			// only consider genes with gene symbols
			if (probleSymbolIndex == null) {
				continue;
			}

			probeEntrezGeneSymbolIndex = entrezNumberToEntrezGeneSymbolIndex.get(probeGeneEntrezNumber);
			probeGeneSymbol = entrezGeneSymbolIndexToGeneSymbol[probeEntrezGeneSymbolIndex];

			int numGenesToMerge = bestPickEndIndex - bestPickStartIndex;

			int[] bestGWASGeneIndices = new int[numGenesToMerge];
			int[] bestGeneEntrezNumbers = new int[numGenesToMerge];
			String[] bestGeneSymbols = new String[numGenesToMerge];
			int[] bestEntrezGeneSymbolIndices = new int[numGenesToMerge];
			double[] bestGeneSimilarities = new double[numGenesToMerge];

			int[] randomGWASGeneIndices = new int[numGenesToMerge];
			int[] randomGeneEntrezNumbers = new int[numGenesToMerge];
			String[] randomGeneSymbols = new String[numGenesToMerge];
			int[] randomEntrezGeneSymbolIndices = new int[numGenesToMerge];
			double[] randomGeneSimilarities = new double[numGenesToMerge];

			double randomTrialBestCorrelationSum = 0;
			double randomTrialRandomCorrelationSum = 0;

			double randomTrialDeltaCorrelationSum = 0;
			double randomTrialDeltaCorrelationMagnitudeSum = 0;

			int numRandomSelectionEvaluations = 0;
			for (int randomTrialIndex = 0; randomTrialIndex < targetNumRandomSelectionEvaluations; randomTrialIndex++) {

				int mergeGeneIndex = 0;
				int tryIndex = 0;
				for (int sortedGeneIndex = 0; sortedGeneIndex < sampleNumGenes; sortedGeneIndex++) {

					{
						int gene2Index = similaritiesAndKeys[sortedGeneIndex].index;

						// prevent self matches
						if (gene2Index == gene1Index)
							continue;

						Integer entrezNumber = Integer.parseInt(geneEntrezIDs[gene2Index]);

						// only consider genes with entrez numbers
						if (entrezNumberToEntrezGeneSymbolIndex.get(entrezNumber) == null)
							continue;

						if (tryIndex < bestPickStartIndex) {
							tryIndex++;
							continue;
						}

						bestGWASGeneIndices[mergeGeneIndex] = gene2Index;
						bestGeneEntrezNumbers[mergeGeneIndex] = entrezNumber;
						bestEntrezGeneSymbolIndices[mergeGeneIndex] = entrezNumberToEntrezGeneSymbolIndex.get(bestGeneEntrezNumbers[mergeGeneIndex]);
						bestGeneSymbols[mergeGeneIndex] = entrezGeneSymbolIndexToGeneSymbol[bestEntrezGeneSymbolIndices[mergeGeneIndex]];
						bestGeneSimilarities[mergeGeneIndex] = similaritiesAndKeys[sortedGeneIndex].score;

						// prevent any self matched symbol names
						if (probeEntrezGeneSymbolIndex == bestEntrezGeneSymbolIndices[mergeGeneIndex]) {
							// System.out.println("######### Error! probeEntrezGeneSymbolIndex == bestEntrezGeneSymbolIndex");
							continue;
						}
					}

					// pick random set of genes to merge

					while (true) {

						int randomIndex = (int) (sampleNumGenes * Math.random());

						int gene2Index = similaritiesAndKeys[randomIndex].index;

						// prevent self matches
						if (gene2Index == gene1Index)
							continue;

						Integer entrezNumber = Integer.parseInt(geneEntrezIDs[gene2Index]);

						// only consider genes with entrez numbers
						if (entrezNumberToEntrezGeneSymbolIndex.get(entrezNumber) == null)
							continue;

						randomGWASGeneIndices[mergeGeneIndex] = gene2Index;
						randomGeneEntrezNumbers[mergeGeneIndex] = entrezNumber;
						randomEntrezGeneSymbolIndices[mergeGeneIndex] = entrezNumberToEntrezGeneSymbolIndex.get(randomGeneEntrezNumbers[mergeGeneIndex]);
						randomGeneSymbols[mergeGeneIndex] = entrezGeneSymbolIndexToGeneSymbol[randomEntrezGeneSymbolIndices[mergeGeneIndex]];
						randomGeneSimilarities[mergeGeneIndex] = similaritiesAndKeys[randomIndex].score;

						// prevent any self matched symbol names
						if (probeEntrezGeneSymbolIndex == randomEntrezGeneSymbolIndices[mergeGeneIndex]) {
							// System.out.println("######### Error! probeEntrezGeneSymbolIndex == randomEntrezGeneSymbolIndex");
							continue;
						}

						break;

					}

					mergeGeneIndex++;

					if (mergeGeneIndex == numGenesToMerge) {

						break;

					}
				}

				if (mergeGeneIndex != numGenesToMerge) {
					continue;
				}

				// only consider genes with gene symbols
				// if (bestEntrezGeneSymbolIndex == -1) {
				// continue;
				// }

				// prevent any self matched symbol names
				// if (probeEntrezGeneSymbolIndex == bestEntrezGeneSymbolIndex) {
				// System.out.println("######### Error! probeEntrezGeneSymbolIndex == bestEntrezGeneSymbolIndex");
				// continue;
				// }

				if (false) {
					System.out.println("seedGWASGeneIndex           = " + probeGWASGeneIndex);
					System.out.println("seedEntrezGeneSymbolIndex   = " + probeEntrezGeneSymbolIndex);
					System.out.println("seedGeneEntrezNumber        = " + probeGeneEntrezNumber);
					System.out.println("seedGeneSymbol              = " + probeGeneSymbol);

					System.out.println("bestGWASGeneIndex           = " + bestGWASGeneIndices);
					System.out.println("bestEntrezGeneSymbolIndex   = " + bestEntrezGeneSymbolIndices);
					System.out.println("bestGeneEntrezNumber        = " + bestGeneEntrezNumbers);
					System.out.println("bestGeneSymbol              = " + bestGeneSymbols);

				}

				// compute correlations between best (according to d2z) and best picks and random picks

				int numSeedVSBestObservations = 0;

				for (int activityFileIndex = 0; activityFileIndex < numGoodFiles; activityFileIndex++) {

					if (Float.isNaN(geneActivities[activityFileIndex][probeEntrezGeneSymbolIndex])) {
						continue;
					}

					boolean allMergeGenesFound = true;

					for (int j = 0; j < numGenesToMerge; j++) {
						if (Float.isNaN(geneActivities[activityFileIndex][bestEntrezGeneSymbolIndices[j]])) {
							allMergeGenesFound = false;
							break;
						}
						if (Float.isNaN(geneActivities[activityFileIndex][randomEntrezGeneSymbolIndices[j]])) {
							allMergeGenesFound = false;
							break;
						}
					}

					if (!allMergeGenesFound) {
						continue;
					}

					// sum best merged gene activity levels //

					double weightSum = 0.0;
					for (int j = 0; j < numGenesToMerge; j++) {

						double weight = Math.pow(bestGeneSimilarities[j], weightPower);

						weightSum += weight;

					}

					double bestMergedGenesActivitySum = 0.0;
					for (int j = 0; j < numGenesToMerge; j++) {

						// System.out.println("################## bestGeneSimilarities[j] = " + bestGeneSimilarities[j]);

						double weight = Math.pow(bestGeneSimilarities[j], weightPower) / weightSum;
						// System.out.println("################## weight = " + weight);

						bestMergedGenesActivitySum += weight * geneActivities[activityFileIndex][bestEntrezGeneSymbolIndices[j]];
					}

					// sum random merge gene activity levels //
					double randomMergedGenesActivitySum = 0.0;
					for (int j = 0; j < numGenesToMerge; j++) {
						randomMergedGenesActivitySum += geneActivities[activityFileIndex][randomEntrezGeneSymbolIndices[j]];
					}

					// create example for correlation
					probeGeneActivities[numSeedVSBestObservations] = geneActivities[activityFileIndex][probeEntrezGeneSymbolIndex];
					bestGeneActivities[numSeedVSBestObservations] = bestMergedGenesActivitySum;
					randomGeneActivities[numSeedVSBestObservations] = randomMergedGenesActivitySum;
					numSeedVSBestObservations++;
				}

				// System.out.println("##################numSeedVSBestObservations = " + numSeedVSBestObservations);

				// only consider cases that have the minimum required number of observations //
				if (numSeedVSBestObservations < minNumObservations) {
					// System.out.println("######### Warning! (numSeedVSBestObservations < minNumObservations)");
					continue;
				}

				// catch any data errors that lead to self matching //
				double seedVSBestCorrelation = Utility.correlation(probeGeneActivities, bestGeneActivities, numSeedVSBestObservations);
				double seedVSRandomCorrelation = Utility.correlation(probeGeneActivities, randomGeneActivities, numSeedVSBestObservations);
				if (seedVSBestCorrelation > 0.999999999) {
					System.out.println("######### Error! seedVSBestCorrelation > 0.999999999");
					continue;
				}
				if (seedVSRandomCorrelation > 0.999999999) {
					System.out.println("######### Error! seedVSRandomCorrelation > 0.999999999");
					continue;
				}

				randomTrialBestCorrelationSum += seedVSBestCorrelation;
				randomTrialRandomCorrelationSum += seedVSRandomCorrelation;

				double deltaCorrelation = seedVSBestCorrelation - seedVSRandomCorrelation;
				// double deltaCorrelationMagnitude = Math.abs(seedVSBestCorrelation) - Math.abs(seedVSRandomCorrelation);

				double deltaCorrelationMagnitude = Math.abs(seedVSBestCorrelation) - Math.abs(seedVSRandomCorrelation);
				// double deltaCorrelationMagnitude = seedVSRandomCorrelationMagnitudesSum / seedVSRandomCorrelationMagnitudesCount;

				randomTrialDeltaCorrelationSum += deltaCorrelation;
				randomTrialDeltaCorrelationMagnitudeSum += deltaCorrelationMagnitude;

				numRandomSelectionEvaluations++;

			}

			if (numRandomSelectionEvaluations == 0) {
				continue;
			}

			double deltaCorrelation = randomTrialDeltaCorrelationSum / numRandomSelectionEvaluations;
			// double deltaCorrelationMagnitude = Math.abs(seedVSBestCorrelation) - Math.abs(seedVSRandomCorrelation);

			double deltaCorrelationMagnitude = randomTrialDeltaCorrelationMagnitudeSum / numRandomSelectionEvaluations;
			// double deltaCorrelationMagnitude = Math.abs(seedVSBestCorrelation) - Math.abs(seedVSRandomCorrelation);
			// double deltaCorrelationMagnitude = seedVSRandomCorrelationMagnitudesSum / seedVSRandomCorrelationMagnitudesCount;

			deltaCorrelationSum += deltaCorrelation;
			double averageDeltaCorrelation = deltaCorrelationSum / (correlationComparisonCount + 1);

			deltaCorrelationMagnitudeSum += deltaCorrelationMagnitude;
			double averageDeltaCorrelationMagnitude = deltaCorrelationMagnitudeSum / (correlationComparisonCount + 1);

			double seedVSBestCorrelation = randomTrialBestCorrelationSum / numRandomSelectionEvaluations;
			double seedVSRandomCorrelation = randomTrialRandomCorrelationSum / numRandomSelectionEvaluations;

			seedVSBestCorrelationSum += seedVSBestCorrelation;
			double averageSeedVSBestCorrelation = seedVSBestCorrelationSum / (correlationComparisonCount + 1);

			seedVSRandomCorrelationSum += seedVSRandomCorrelation;
			double averageSeedVSRandomCorrelation = seedVSRandomCorrelationSum / (correlationComparisonCount + 1);

			deltaCorrelationObservations[correlationComparisonCount] = deltaCorrelation;
			deltaCorrelationMagnitudeObservations[correlationComparisonCount] = deltaCorrelationMagnitude;

			correlationComparisonCount++;

			double deltaCorelationMean = Utility.computeSampleMean(deltaCorrelationObservations, correlationComparisonCount);
			double deltaCorelationSTD = Utility.computeSampleSTD(deltaCorrelationObservations, deltaCorelationMean, correlationComparisonCount);
			double deltaCorelationSTDE = Utility.computeStandardErrorOfMean(deltaCorelationSTD, correlationComparisonCount);
			double deltaCorelationZ = deltaCorelationMean / deltaCorelationSTDE;

			double deltaCorelationMagnitudeMean = Utility.computeSampleMean(deltaCorrelationMagnitudeObservations, correlationComparisonCount);
			double deltaCorelationMagnitudeSTD = Utility.computeSampleSTD(deltaCorrelationMagnitudeObservations, deltaCorelationMagnitudeMean, correlationComparisonCount);
			double deltaCorelationMagnitudeSTDE = Utility.computeStandardErrorOfMean(deltaCorelationMagnitudeSTD, correlationComparisonCount);
			double deltaCorelationMagnitudeZ = deltaCorelationMagnitudeMean / deltaCorelationMagnitudeSTDE;

			System.out.println("correlationComparisonCount       = " + correlationComparisonCount);
			// System.out.println("numSeedVSBestObservations        = " + numSeedVSBestObservations);
			// System.out.println("numSeedVSRandomObservations      = " + numSeedVSRandomObservations);
			System.out.println("seedVSBestCorrelation            = " + seedVSBestCorrelation);
			System.out.println("seedVSRandomCorrelation          = " + seedVSRandomCorrelation);
			System.out.println("averageSeedVSBestCorrelation     = " + averageSeedVSBestCorrelation);
			System.out.println("averageSeedVSRandomCorrelation   = " + averageSeedVSRandomCorrelation);
			System.out.println("averageDeltaCorrelation          = " + averageDeltaCorrelation);
			System.out.println("averageDeltaCorrelationMagnitude = " + averageDeltaCorrelationMagnitude);

			System.out.println("deltaCorelationMean = " + deltaCorelationMean);
			System.out.println("deltaCorelationSTD  = " + deltaCorelationSTD);
			System.out.println("deltaCorelationSTDE = " + deltaCorelationSTDE);
			System.out.println("deltaCorelationZ    = " + deltaCorelationZ);
			System.out.println("deltaCorelationMagnitudeMean = " + deltaCorelationMagnitudeMean);
			System.out.println("deltaCorelationMagnitudeSTD  = " + deltaCorelationMagnitudeSTD);
			System.out.println("deltaCorelationMagnitudeSTDE = " + deltaCorelationMagnitudeSTDE);
			System.out.println("deltaCorelationMagnitudeZ    = " + deltaCorelationMagnitudeZ);

			System.out.println("seedGeneSymbol   = " + probeGeneSymbol);
			System.out.println("bestGeneSymbol   = " + bestGeneSymbols);
			// System.out.println("randomGeneSymbol = " + randomGeneSymbol);
			System.out.println();

			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//
			//

		}

		int numComparisons = sampleNumGenes * (sampleNumGenes - 1) / 2;

		long comparisonEndTime = System.currentTimeMillis();

		double comparisonDuration = (comparisonEndTime - comparisonStartTime) / 1000.0f;
		double comparisonsPerSecond = comparisonCount / comparisonDuration;

		int targetNumComparisons = targetNumGenes * (targetNumGenes - 1) / 2;
		double targetDuration = targetNumComparisons / comparisonsPerSecond;

		System.out.println("numGenes             = " + sampleNumGenes);
		System.out.println("numComparisons       = " + numComparisons);
		System.out.println("comparisonCount      = " + comparisonCount);
		System.out.println("kSize                = " + kSize);
		System.out.println("numKmers             = " + numKmers);
		System.out.println("comparisonDuration   = " + comparisonDuration);
		System.out.println("comparisonsPerSecond = " + comparisonsPerSecond);
		System.out.println("targetNumGenes       = " + targetNumGenes);
		System.out.println("targetNumComparisons = " + targetNumComparisons);
		System.out.println("targetDuration       = " + targetDuration);

	}

	public static double computeExpectation(int len1, int len2, int k, double[] q1, double[] q2) {
		double p2 = 0;
		for (int i = 0; i < 4; i++) {
			p2 += q1[i] * q2[i];
		}
		int nbar1 = len1 - k + 1;
		int nbar2 = len2 - k + 1;
		double retval = nbar1;
		retval *= nbar2;
		retval *= Math.pow(p2, k);
		return retval;
	}

	public static double computeVariance(int len1, int len2, int k, double[] q1, double[] q2) {

		double nbar1 = len1 - k + 1;
		double nbar2 = len2 - k + 1;
		double qbar1 = len1 - 2 * k + 2;
		double qbar2 = len2 - 2 * k + 2;

		double p2 = 0, p31 = 0, p32 = 0;
		for (int i = 0; i < 4; i++) {
			p2 += q1[i] * q2[i];
			p31 += q1[i] * q2[i] * q1[i];
			p32 += q1[i] * q2[i] * q2[i];
		}

		double variance = 0;

		double t1 = (qbar2) * (qbar2 - 1);
		double t2 = 2 * (qbar1) * (qbar1 - 1);
		double t3 = 2 * k;

		if (useCrabgrass) {
			// crabgrass with l=0 (= complete overlap):
			double power1 = Math.pow(p32, k) - Math.pow(p2, 2 * k);
			power1 *= (nbar1) * (qbar2) * (qbar2 - 1);
			double power2 = Math.pow(p31, k) - Math.pow(p2, 2 * k);
			power2 *= (nbar2) * (qbar1) * (qbar1 - 1);
			variance += power1 + power2;
			// crabgrasses with l>0:
			for (int l = 1; l <= k - 1; l++) {
				variance += 2 * (nbar1 - l) * t1 * (Math.pow(p2, 2 * l) * Math.pow(p32, k - l) - Math.pow(p2, t3)) + (nbar2 - l) * t2 * (Math.pow(p2, 2 * l) * Math.pow(p31, k - l) - Math.pow(p2, t3));
			}
		}

		if (useAccordion) {
			// // accordion main diagonal:
			variance += (nbar1) * (nbar2) * (Math.pow(p2, k) - Math.pow(p2, t3)); // l=0 term
			for (int l = 1; l <= k - 1; l++) {
				variance += 2 * (nbar1 - l) * (nbar2 - l) * (Math.pow(p2, k + l) - Math.pow(p2, t3)); // l > 0 terms
			}
		}
		// INCLUDDED EDGE EFFECTS
		// IGNORED NON-DIAGONAL ACCORDION TERMS

		// if (variance <= 0) { fprintf(stderr,"Error: negative variance, p2, p32, p31 are:\t");
		// fprintf(stderr,"%g %g %g\n",p2,p32,p31);
		// }
		return variance;
	}
}

class ScoreAndIndex implements Comparable<ScoreAndIndex> {

	double score;
	int index;

	ScoreAndIndex(float score, int index) {
		this.score = score;
		this.index = index;
	}

	public int compareTo(ScoreAndIndex o) {

		// forward sort is normal operation for GWAS
		if (this.score < o.score)
			return 1;
		if (this.score > o.score)
			return -1;

		// reverse sort for testing
		// if (this.score < o.score)
		// return -1;
		// if (this.score > o.score)
		// return 1;

		return 0;
	}

}

class GwasTestSequences {

	static int[] testCounts = new int[] { 0, 1, 1, 0, 0, 2, 0, 1, 3, 1, 2, 1, 0, 0, 0, 0, 0, 0, 1, 0, 2, 3, 0, 2, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 2, 0, 2, 2, 2, 1, 3, 0, 0, 1, 1, 1, 2, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0,
			0, 0, 1, 5, 1, 0, 0, 1, 2, 1, 2, 0, 1, 2, 2, 0, 1, 0, 1, 0, 0, 0, 4, 1, 2, 0, 0, 0, 0, 0, 2, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 2, 1, 1, 0, 0, 3, 4, 0, 2, 0, 4, 1, 1, 2, 2, 1, 1, 0, 0, 1, 3, 2, 0, 0, 2, 1, 1, 0, 1, 3, 0, 2, 10, 2, 3,
			1, 1, 2, 0, 3, 1, 2, 0, 3, 3, 7, 2, 2, 0, 2, 2, 0, 0, 6, 2, 1, 2, 4, 0, 0, 0, 2, 0, 0, 2, 1, 3, 1, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 4, 0, 2, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1,
			0, 1, 0, 0, 0, 1, 1, 1, 4, 0, 0, 2, 1, 1, 1, 0, 0, 1, 0, 0, 1, 2, 1, 1, 1, 3, 0, 0, 1, 0, 1, 0, 3, 1, 2, 1, 0, 0, 1, 2, 0, 1, 2, 2, 1, 0, 2, 0, 3, 2, 0, 0, 0, 2, 3, 3, 0, 1, 2, 1, 10, 1, 0, 6, 4, 6, 3, 0, 2, 1, 0, 0, 1, 0, 0, 1, 3, 0, 0, 0, 0,
			0, 2, 0, 2, 0, 2, 2, 0, 1, 0, 2, 4, 2, 4, 5, 6, 8, 0, 0, 2, 1, 1, 2, 7, 8, 3, 7, 4, 0, 8, 0, 2, 1, 0, 4, 5, 7, 2, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 2, 2, 2, 1, 11, 2, 5, 1, 2, 8, 2, 2, 4, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1,
			0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 5, 0, 3, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 2, 1, 1, 4, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 1, 0, 3, 1, 2, 1, 0, 1, 1, 1, 0, 2, 1, 4, 1, 4, 9, 1, 3, 1, 0, 2, 1,
			2, 1, 2, 1, 0, 1, 1, 0, 0, 2, 0, 1, 3, 5, 11, 1, 2, 2, 6, 1, 2, 0, 0, 0, 1, 4, 0, 2, 1, 1, 2, 0, 0, 3, 0, 4, 0, 1, 3, 0, 1, 1, 0, 0, 0, 3, 1, 0, 0, 2, 1, 0, 1, 0, 3, 0, 0, 1, 0, 2, 0, 0, 0, 1, 0, 2, 2, 1, 2, 1, 2, 2, 0, 3, 1, 4, 5, 2, 3, 3, 0,
			2, 1, 0, 0, 0, 0, 0, 0, 3, 0, 1, 1, 0, 1, 0, 4, 3, 0, 0, 1, 0, 1, 1, 1, 0, 2, 0, 1, 3, 3, 2, 0, 1, 0, 0, 0, 1, 3, 0, 3, 9, 2, 5, 0, 1, 1, 0, 1, 8, 2, 0, 0, 0, 2, 1, 1, 1, 0, 0, 1, 1, 2, 0, 0, 0, 1, 0, 1, 2, 1, 0, 3, 1, 0, 0, 1, 0, 6, 2, 0, 1,
			2, 0, 3, 1, 1, 1, 3, 1, 1, 2, 3, 3, 8, 2, 0, 2, 1, 3, 2, 2, 4, 0, 1, 2, 0, 3, 2, 1, 2, 1, 1, 2, 3, 2, 1, 3, 6, 4, 2, 1, 3, 3, 8, 6, 11, 4, 0, 2, 3, 2, 0, 0, 1, 0, 3, 1, 1, 1, 3, 2, 1, 2, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0,
			2, 1, 1, 1, 0, 3, 1, 0, 3, 0, 0, 2, 0, 0, 3, 2, 0, 1, 1, 3, 2, 2, 1, 0, 3, 0, 3, 2, 1, 0, 2, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 3, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 2, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 2, 2, 1,
			1, 0, 1, 0, 1, 2, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 2, 0, 3, 0, 2, 1, 1, 1, 0, 2, 3, 0, 1, 1, 0, 1, 0, 2, 8, 0, 8, 4, 1, 4, 0, 1, 0, 0, 2, 2, 3, 3, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 3, 0, 0, 0, 1, 0, 0,
			0, 1, 1, 3, 3, 1, 0, 0, 1, 3, 3, 0, 0, 1, 3, 0, 0, 1, 0, 0, 1, 0, 2, 2, 2, 2, 1, 0, 0, 0, 2, 1, 0, 1, 1, 0, 2, 0, 2, 0, 0, 0, 0, 0, 1, 4, 0, 2, 1, 2, 0, 3, 5, 1, 1, 5, 2, 8, 0, 0, 2, 0, 0, 1, 0, 0, 2, 0, 3, 0, 1, 1, 2, 4, 0, 0, 1, 1, 0, 1, 1,
			1, 0, 3, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 2, 3, 0, 1, 1, 3, 0, 2, 0, 1, 0, 0, 0, 1, 3, 3, 0, 1, 2, 0, 1, 0, 0, 1, 1, 2, 1, 0, 1, 0, 0, 0, 0, 0, 2, 0, 2, 0, 0, 4, 2, 1, 0, 1, 2, 1, 2, 8 };

	static public int[] getTestCounts() {
		return testCounts;
	}

	static String[] testSequenceStrings = new String[] {
			"CAACTGGCAGAGCCAGGATTCACGCCCTGGCAATTTGACTCCAGAATCCTAACCTTAACCCAGAAGCACGGCTTCAAGCCCCTGGAAACCACAATACCTGTGGCAGCCAGGGGGAGGTGCTGGAATCTCATTTCACATGTGGGGAGGGGGCTCCCCTGTGCTCAAGGTCACAACCAAAGAGGAAGCTGTGATTAAAACCCAGGTCCCATTTGCAAAGCCTCGACTTTTAGCAGGTGCATCATACTGTTCCCACCCCTCCCATCCCACTTCTGTCCAGCCGCCTAGCCCCACTTTCTTTTTTTTCTTTTTTTGAGACAGTCTCCCTCTTGCTGAGGCTGGAGTGCAGTGGCGAGATCTCGGCTCACTGTAACCTCCGCCTCCCGGGTTCAAGCGATTCTCCTGCCTCAGCCTCCCAAGTAGCTAGGATTACAGGCGCCCGCCACCACGCCTGGCTAACTTTTGTATTTTTAGTAGAGATGGGGTTTCACCATGTTGGCCAGGCTGGTCTCAAACTCCTGACCTTAAGTGATTCGCCCACTGTGGCCTCCCAAAGTGCTGGGATTACAGGCGTGAGCTACCGCCCCCAGCCCCTCCCATCCCACTTCTGTCCAGCCCCCTAGCCCTACTTTCTTTCTGGGATCCAGGAGTCCAGATCCCCAGCCCCCTCTCCAGATTACATTCATCCAGGCACAGGAAAGGACAGGGTCAGGAAAGGAGGACTCTGGGCGGCAGCCTCCACATTCCCCTTCCACGCTTGGCCCCCAGAATGGAGGAGGGTGTCTGTATTACTGGGCGAGGTGTCCTCCCTTCCTGGGGACTGTGGGGGGTGGTCAAAAGACCTCTATGCCCCACCTCCTTCCTCCCTCTGCCCTGCTGTGCCTGGGGCAGGGGGAGAACAGCCCACCTCGTGACTGGGGGCTGGCCCAGCCCGCCCTATCCCTGGGGGAGGGGGCGGGACAGGGGGAGCCCTATAATTGGACAAGTCTGGGATCCTTGAGTCCTACTCAGCCCCAGCGGAGGTGAAGGACGTCCTTCCCCAGGAGCCGGTGAGAAGCGCAGTCGGGGGCACGGGGATGAGCTCAGGGGCCTCTAGAAAGAGCTGGGACCCTGGGAACCCCTGGCCTCCAGGTAGTCTCAGGAGAGCTACTCGGGGTCGGGCTTGGGGAGAGGAGGAGCGGGGGTGAGGCAAGCAGCAGGGGAC", //
			"AACTGACCTCCACTTATCTTGCTGCCTGGATTGGCATACATATTCCTCTGCCTGAAAAATTGCTCAATACTTTTGAAAGGTTGTTTCTCACTGTTACACCCAGGCATTTCTGCTAACACCAGCTGAGTTGTAAGGGGCCCTCCAGGAGACCTAGGTTGTTCTCCTTATCACATACATTATTTTCATTTATCAATTTACCCAGAACATCTAATGAAATATTATGGACACCAATAACATGAATACAAATATAGCTATAGACCCTAAAAAGTAAATTGATGTCTCTTGAAAGGCTGTCATGTCTTAGATGCTAAATTTGGAGTATGTGTTTGTAATAAATAAATAGCTTGCTTTAATAATTCTGATTTATGAATGTAATATAGGGGAGCAGCTTTTATTTCGCCCTTAATCTTAAAAAACAATTAGCTGGGTGTGGTGGTGGGCACCTGTAGTCCCAGCTACTCAGGAGGCTGAAGCAGGAGGATGGTGTGCACCTGGGAGGCGGAGCTTGCAGTAAGCCAAAATTGTGCCACTGCACTCCAGCCTGAGTGACAGTGTGAGACTCTGTCTCAAAAAAAAAAAAAAAAAAAAAGATCATACATGGAGTAAATGATGATTTAGCATAAAACACAACTTTAGCCTAAAATACAGATCATAGGGCTCAAACTTTGTTATTTTATGGGATTATACTAATCAGTTAATATTCAAAATTTACCATTTCAAAAGAAGAAATATTGGTCAGGGAAAATTGCCATTGCTTATACAGAAGAATGGAGGAATCTTTTTGACAACAACAATTCTAGTGATCTTCCTCATCACTGGTTCCAAAACCAAGCCCATGGAAGGTCTGCATAGGGATAATGTGGAGAACATGCAAGAGAAATACACATCACCAGCTCCTCAACTCAGAGCTCCAAGGGAATGGCCAAGGAAGCTGTATTGTCCTAAGAAATTTGAGACTGTTTTGACTTACAATAAATCCTGCTCAATCATATGTATGTATACACACACACACAACCACACACACAGACGGTGTTGTCAGTGAAGCCTCTAACAATGCATCTTGACTGGACAGATTTGACAATTAAAATGGCTGGAAGAAAATAGGCTTCATAAGTGTCACTTACTATTCACATTACACATTAAAAGGACATCTCCTAAATTAAATCTTGTCTATGATAAAAAGGCAGAAAATCATGACCCA", //
			"TGGCAGAAATCAGGGAAGGAAAGGCAAAGTTCAGGTATTTCGCACAATAAATAAATAAAGATAGGTAGATTTGATTGATGGATGTATGGATGAAACGTGGGAGTTTATGGGCAAATGTTCATCAGACACTGGAAGTGTAAGTTGTCACAAAGATTATGGAGTGCACTTGTCTTATGACCCTGTTATTTTATCCTAGTATATACACTAGAGCATTTTTTTCTAACTGTGTAAATTGAAAGCTCACAAATTAGTTTCGTGAGAGAAAAGATAACAGATTGGAAGAGAATTACCATATTCATTAGTTGTGTTTTTAAAAATTTAAAGTAAAATAGAGACATGATTTTTTTCATGCTTTCGAATGCATCTATAAAAAATAGACTTGAGGGCTGGGCGCGGTGGCTCACGCCTGTAATCCCAGCACTTTGGGAGGCCTAGGAGGGCAGATCACGAGGTTAGGAGTTAGAGACCAGCCTGACCAACATGGTGAAACCCCGTTTCTACTAAAAATACAAAAATTAGGCGAGTGTGGTGGCGCCCGCCTGTAATCCCAGATACTCAGGAGGCTGAGGCAGGAGAATCTCTTGAACCTGGGAGGCAGAGTTTGCAGTGAGCCAAGGTCGCACCATTGCACTCCAGCCTGGGTGACAGAGCGAGACTCCATCTCAAAAAAAAAAAAAGTTACTCATTAACAGCATAGACCAATTGTTTTCTATTGGAATTTCTCCATTATTTTCACAATGTCCCAGGCTGTGAAACCAGGATTTAATAACGAACCAGAATGCCACATCTGTGTCACGTGGGTAGGGACCAGTCCTGATCCATTAAGTCCGGGTCTCCGGGTAACTGGACTCACTGCTGGGCAAAACAGAATGTCCGGCGTGCGTTCCTAACGGGGGACCGCAGAGCCTCATAGGAAATGTAGTGTCACCTTCCAATGATGTTACCATCAAGGACCTTGGGAACCAGCTTTTCTCTCTGCGCATGCGCCGCCCCGCCCACTTCGCCATTTTCCTCCGGAAGTGCGGATCCCAGCGGCGGTCGTGTAGCTGAGCAGGCCTGGGGCTTGGTTCTATGTCCCTGTGGGTAGGTGCGAGGGCAAAGAGGAACCTGTGGGCCTCGGGGGATCCCGGGGGGCCAGACCAGTGTTCCCCATTTGTGGGGGCAGACGCTTGGGCGCATCGCGGGCAGGAGGGGCTTGACG", //
			"TTTATTAAGTAAGTGGTTACTCTTTCAAAGACAAAAAAAATGCAAATTGTTACAAAACTGGCAGTATTTGTAAGTGCAAGCACTACACGCTGCCTTGTTCTTTTACCAATTGCATTTGCATTTTAAGGTACTACTTGTACAGCCATGGTGGAGAACAGTTTGGAGGTTCCTCTAAACACTGAAAATAGAGGTGCCACATGATCCAGCAATCCCACTGTTGGATATATACCCCAGAAATAAGAAATGAGTATATCGAAGAAATTATCTGCACTCCCATGTTGGTTGCACCACTGTTGACAATAGCTAAGATTTGGAAGCAACCTAAGTGTCCATCAACAGATTAATGTATTAAAGAAAATGTGGTAGATACACACAGTGGAGTATTATTCAGCCCTAAAAAAGAATGAGATTCAGTCATTTGCAACAACATGGAAGGAACTGGATATCATTATGTTAAGGGAAATAAGCCAAGCACGGAAAGGCAGACATTGCATGTTCTCACTTATTTGTGGGATCTAAAAATCAAAACAATTGAACTCATGGACATAGTAAGTACTAGGGGGCTGGGGGGGGAGACAGGGCACGGGTAATGGGTACAAAAATAGGCAGAAGGAATGAATAAGACATACTATTTGATAGCACAACAGGGGGACTCTAGTCAATAATTGTACATTTAAAAATAACTAAAAGAATCTAATTGGATTGTAACACAAAGGAAACATGCTTAAAGGGATGGATACCCACTCTCCATGATGTGATTAGTTCATGCTGCATGCCTGTATCAAAACATCTCATGCACCCCATAAATATATATGCTTATTATATACTCACAAAAATGCTTGAAAATAAAAATAAAGGAACTACTGAAGGTCAGGTCAGAGTGGAAATGTAAAAATACTAATTAGAGAATAATGTGAATACAACAGGAATCCTGTTGGTATTCTATTTATATTGTAAGCAGCAGTTCAATTGTTTTGAAAAAGTAATTTCAATTTTAATCACTGAACTAAAGAAATGGGCAAGGCTGACTTCCGTAATATAGGTTCTACCTAACCATCTCTAACACCGCTGTCAAGGAGGACCAGTGTTAAGGTACATTACTAACAACCACACAAATTTTTAAAAGAAAAGAACACTCTTAGCAGCCTATGGTACTTTGAAATGAAATATTGCCTCTCATTCTCACTTGTGTTGCCATTCC", //
			"CAGGTTTGATTTCCTCATTTCTAAAACAGGGATAGTACTAGTACCTACCCCATGGGATTACTGGGCTAATGAAAAGAGATAATGTATGCAGAGTCCCTGGCTCATAAAAGTTATTTTCTCCGATTAATTAAATAAGTGAATATGTGTAAAGTGCTGAAGACAGCGTTTGACACTTAGCGTTTTATCAGTCTTAGCTATCATGATTTTCTTATTCTAGTACATAGAAAGTCCTCAATAAGTAACAGTAGTTATTATTATTTTTGGTCACATGTCCCTGTGTTTCCCTCCAAAACCCTAACCATCCCCAATTCTGGTTTTCTCCCCGCATGCAGCCCAAGGGTTTTCACGTCCCAGTGCGATTGTCTGGGCTGTTTCCTCCGCCTGATGTTCCATTTCTCCTCCTCCCGATCTCAGATGGCTTCTCCTCTGGGATTCCTCTGGTCTTCATCACTCCCTCCTCCCAGGCAGAAGGAATGGCTCCTGGATTTGTGTCCTGCTTAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGAAGTCCATATAGCACTTAGTTCTTTGTGCCTGGCGGTTGGGTTAACTCTTTCTGAGCCTGTCTCCGAGAGTGGCGGCCCCCGGGGGACGGCGCTGGGTGAATCACCTCTGAGTGTCACCCAAGGCGTCTGGCACAGTGCGGGACAGTAAACGGGCCGCTGTGCGGAGAGGGGTCCAGTTTAACTCCTAGAAGCCCGAGTCCCCGACCCTCTACAGGCCGCGTCCACCACAAATCCCTCCTCCTGGGGGGCTCGGATTCTCCCTTCCCCGCGGTCTCCCTCACCTGAGCCAGCCCCGCTCCCCCGAGTCTCGCCAGCTCCTCCCCGGCCCCTCCCTCCAGCCCCCTCCGGGCTGGAGCCTCCCCTCCTCCATCCCCTGGCGGAGGCCCGCGGGGCTGGAGGCGGAGCGTCTCGGCAGAGGGCTGGGAGCCGGGTGGGGAGGCGCGGGGCGAGCCGGGGGGTTCCAGACGCGCCTCCACCGCCGGGCAGTGGGCAGGTATGGCTGAGGGCGTGTGAGCGCCGAGCGCTAAGGGCCGCCGCCACCATGCCAGGGGGCGCAGGCGCCGCCCGGCTCTGCTTGCTGGCGTTTGCCCTGCAGCCCCTCCGGCCGCGGGCGGCGCGGGAGCCTGGATGGACAAGTAAGTGGCCAGGCGCGGGGGGATGGAG", //
			"AATTGACAAATGGGATCTGATTGAACTAAAGAGCTTCTGCACAGCAAAAGAAACTATCATCAGAGTGAACAGGCAACCTACAGAATGGGAGAAGATTTTTGCAATCTATCCATCTGACAGAGGGCTAATATCCCCAATCTACAAAGAACTTAAACAGATTTACAAGAAAAAATCAAACAACCCTATCAAAAAGTGGGTGAAAGATATGAACAGACACTTCTCAAAAGAAGACATTTATGCAGCCAACAAACATATGAAAAAAAGATCATCTTCACTGGTTATTTGTAATTTTAATAGATACTGCCAGCTTGCTCTCCTGACTTCCTCTTGAGAATATAAACACATTATACATACAGTTTCTTTCTAAATGGGACCATACAGTATATAACTTTTATCTATCTGATTTTAACCTGTAAGATATCAGGGATATTTTCTTTCAAACAGGCCTCTGGCCTTCAAGTGAGGCTTATTGCTTTCTCTCCAGGACTTCTCCAGGCCTCGGCCGCCCCAGAAGGCACTCCCGGCAAACCCAGTGCCAGGCCGCAGGAGCCTCCCCAGGCCAGGAGGTGCATCCCCACTGCGGCCCCCTGGTGCTGGCCCTCAGCAGTCCCGGCCTCTGGCAGCACTTGCCCCAAAGGTGAGTCCACGGGAAGCCCTCAAGGTGAAAGCTGGTACCAGAGGGCTCCAGGGGGGCAGGTGTAGAGTTGAGAAAACAAAGCAATTCATGCTTCTTGTGGTCTGGACTGAACTTCCAGAACAAAAGCCAAGGGCAAAACATTCATGTTTCTTGGTGCCCGCTTGACTGTGGAGTTTTGGCTTCATGTGAAAGGTGATTCTTAGAATCCTGAGCTGTGGTGGCTTCAGTCCTGCCCCTGCACCTGACCTGGGGAGGGACCCTGAGCAAGTCCCTCTTGAGTCTGTTTCCTCATTTGTACAGAGGCTATGATGAGCAGTAGTACCAGCCCCATCAGGTTGTTGTGAAGAGCAAGGAAGTGCATTAGTAGAAGCCACCTGGCCTGCGGGAGGCTGTGTACACTTGGCCCCTCCTCGTACTTCTTCTGTGTGGCTGACAGTCTCAGTTTGCCAGTCTTGGCAGTCAGGTCAGAAAGGGATGAGGTGAAGAAGCTGGCTGACCATGCTGTATTCCCACGAGGCATTGAGAAAACTATGGACTGTGCTGCTGCCTTAGCAGGGAAGGGAA", //
			"CGGCCTGGGCGTCCCCATTCCAGCTTTAATTTCCCTGAGTTTATCACCCTGAAGGAGCGGACGCAGGAAGCAGGGAAAAACCAAGGACTGGGCTGCCGGCGGGGCAGAAGGCCAGAAGCTAATGACCTAAAACGGAACAGGCTGTGAGCAATTCGATCAAACTCGAAATGCAACTTTCCAACAATGTTCTTAAACCGCTGTAGCCCCAGCTTGCTTGGCTTGGGGTGAAAAGTGTGGGAGAAAGTCTTCACGGCCTGCAGACTTGCTTGCCGACTGAGGTTGATCCGGCAGTTTTCCGTTTTGTTTTGAGCGCGAAAGCGTGGCTGGCTGGTGGAGCTCCAATCTATTCCGCAGCACAAACGGCCCTTCTGAGGCTCTGCTCCCCGAGGCTGGCGGAAGGAAGGGCACTGGCTACTCCGGTACCCACCGAAACAAAACCGACAAACGCAACCGAATTAACACTATTGCTTGTTTGGGTCACTGAAGAAAACTTTTCCCGTGTCGACAGAATTCTAGCCCGACTTTCGTTTGGAACTCAGGATCAGACCTTTACAAAATTACCGACCTGGGTTAGAGGTGCCCGCACGTGTCGAGACCATCTGGCCAAGGACAGAGAGATGCCCCCTGGGAGAATGTTTTAACCGGCGCAGCGGAGCCGCTTGGCTGCGCCCGCCGCAGCCCTACTCCCACTCCGTGCACCGGGCTCCGCAGTGACCCGGGCTCCGCAGTGACCCGGGCTCCCGCAGCGACCCGGGCTCCGGTTGCCCAGGTAACGCAGGACGCAGGCGCGAAAGGCGGGTGCCTGGCGCGCGCTGCAGCTCCCCCTCCCCCCGGCGGGCCGCGGAGGCGACGCGCTGCCTGCGCCCTGAGGCGAGTGGTTCTCCAGCCGGAAAATCGCGGCTCCGGTGCCGCTCTGGGCTCAGCCAGTGAGAGGCCGGGGGTGTGGTAGAGAGAAGGAGCCGGCCGGGTACTTAGGACCGCGCCAGCCTCTGAGCCCGGCCTCCTTCGCACTTTCGAGTGCGCGTTTGCTGCCCGCCGCAGCCCGGGCGTTGAAGGTAGTAAAGCGGCCACCCGGCCGCATCATGGTGACCCCCTGTCCCACCAGCCCCTCGAGCCCCGCCGCCCGAGCGGGGAGGCGGGACAACGACCAGAACCTTCGCGCCCCGGTGAAGAAGAGCAGGCGTCCGCGCCTCCGGAGGAA", //
			"GTGAGAGGCCGGGGGTGTGGTAGAGAGAAGGAGCCGGCCGGGTACTTAGGACCGCGCCAGCCTCTGAGCCCGGCCTCCTTCGCACTTTCGAGTGCGCGTTTGCTGCCCGCCGCAGCCCGGGCGTTGAAGGTAGTAAAGCGGCCACCCGGCCGCATCATGGTGACCCCCTGTCCCACCAGCCCCTCGAGCCCCGCCGCCCGAGCGGGGAGGCGGGACAACGACCAGAACCTTCGCGCCCCGGTGAAGAAGAGCAGGCGTCCGCGCCTCCGGAGGAAGCAGCCGCTGCATCCCCTGAACCCGTGCCCGCTCCCGGGAGACTCCGGCATTTGCGACCTGTTCGAGTCCCCCAGCTCCGGCTCAGACGGCGCAGAGAGCCCCTCTGCGGCGCGGGGTGGTAGCCCCCTGCCCGGCCCGGCCCAGCCCGTGGCGCAGCTAGATCTACAGACCTTCCGCGACTACGGCCAGAGCTGCTACGCCTTCCGCAAGGCGCAGGAGAGCCACTTCCACCCGCGGGAGGCGCTGGCACGGCAGCCACAAGTGAGGTGCTGGTAGAGCCAGCTCCACGCCGCCGCTCTTCCTCTCCTCCCAGGCTCTCTCCTCTCCTGCCGGCTCGGCCCTCCGAGCGCCCGGGCCCGCGACAGCCCAGGTCCCGCCCCGAGTCGAAGCACAAGGGGCCGTGGAGGGGCGTCACCGGCCTCATTTTACAGATGGAGAAAAGTGGGGCGTAGACGGCTTGAGGGACTTGCCCCGTCCCAAGCTGGCTACTGGGGCACGGTAGAGAGGAATTCGTTTCCGGGTTGAGTCTGGACTCCTACCGAACTCGGGCAAGTTATTTAACACCCCTGAGCTTCTTTCTTCATCTGCAAAATGGAACCCCTCCCCTTCCTTTGAATTGCTGAGCCCATTACGCGAGGTAACGTGTACGACGCTCTCTGCACAGCGCCAGGTCCCAATCCCGGTGTTCCGGCTCAGCACGGCCCACGCGTTTCCCAGTTTCCATTGCGTTTCTCTTTCTGAAATCTTCTTGCCCCTCTCCTGGGAAGCTCCCCGATTCTTCTTGGCCCTTCCTTCCCTTCCACAGTCCCTCTCCACCCTAAGCCCAGCGGGCCCGCCTCCCCCCTCCTTCCCGGCAGGTGACGGCGGAATCCCGCTGTAAGCTGCTCAGCTGGCTGATCCCGGTGCACCGCCAATTCGGCCTCTC", //
			"CTAAGTCACTCATCCTTGGACCAGTTTCCTGTGGAGCCCAGTGCGGTATAAAATTCCCCAAGAGCTCACTGGCATTCCTGAGCTCCACATCCTTCAAGGTTAAAAGCTGGGTGTTGAAAACTCCCTGGACCCTCATTGATAGGCTAATGGATTATGGACCTGTGTGTGTGTGTGTGTGTGTGTGTGTGTGTTTTAAATCAGATGCTGGCTCCAGTGCAGCAAACTCTTCTGCCTCTTGCAAGCATCAGCCTTGGAAACAAAACAAAACAAAACAAAAAAAACCTTTTTTATGGAGCAGCCTCTGGCCCTGACAGGCTCTGGCTCTCTGTTGGCTCCCAAGAAGCCCCCCACCTTTCTTAGAGATCAGATTTTTAAAAGATGAGGTTGTACAGTGCTCAGCCAGATCCCCACGTGGCCGCTGAAGAAAGCCCAGAGTGTTGGGGACTGGGCCCTTTCTCAGCTACCACCTCTGCTTGGGTGTCCTCAATGCTAAGACAAGACACCTTGTGGCCCAACTCTTTCCAAAATGACGGGGCTGGGATTCTCCCCCACCTCAAGTCGAATATCCAGCGTTCACCAGGCGGTGGCAGGACCGAGCACGTGGTATCATTTAGGGGCTGGGCTCCAGGGTCCTCCTAGGAAAGAGGACAGCCCAAGAGGCGAGAGGCTGGCCGGCCACCCTGGCCATGGCAACCTCTCTGGCTCCTACAGCTGCTGGCCAAAGGCAGGGGGCACGCCCTGCGTCCCATTACCACGTTCATAGGATGGTGCCAGGAGCCCGTTATCTCTTCTTCGCCTTCTCCTCACGCACCACCCCCACTGTCATGCCGATAAATCCCGGGAGCAAAGGGTGGCACGATTCCTCCCGCGCCCCTGCGGGCTGAAGGCGGGCACTAAAGCAGTGCAGTCCCAAGCTTCCCCTGCAGAAGCCGCGCTCCCTACTCGGTCCGGGGGCAGAGGGGGCGCGAGAGAGCAAGTGGGCGGGCGTCCCATCCTCCGCATCCTCCTCCAGGTCCTGGCGCACAGGGTGGGAGCGCTGCGCTGCGCCGCGCTGCGCATCGCGGCCCGCTTGCCGCCTGCCCCCTGCCCTAGCTGGGCCACCTCCCCGGGCTGCCGGTGGAGGGCTAAGAGGCGCTAACGTTACGCTGTTTCCGGTTTTCCAGCGGGCTCTGTTTCCCCTCCCAAGGCGGCGGCGGCTGAG", //
			"GAGCTGTGCAGAGTTAAGGCCATAGTGCTCTTGCTTGAATAGTATTTTTATAACCAAACAAGACAGATGAAAGCAATGGCTCCAAAATACCCATGAAAATGGGAGGAGAAAATTTGATATAAATGCATGTGTTTTATATTAAGTTTCCTGTAAGACTTGTTTTGACATTTTCTTCTTCAAAAGGAATAATAAATATCTGAATGAAGGCTCATTGTATTTTAATTGTCTTCACCAGTTGTTTTCAATGGCAGGTTCTGCTTCAAGAACAGAATTTGGGAATTAGGTATAAGTTCAATGTTCCCATCACTCGAACTGGCAGTGGAGATAATGAAGTTGGCTTTACATGGAATCATCAGCCTTGGTCAGAATGCTCAGCTACTTGTGCTGGAGGTAAGATGCCCACTAGGCAGCCCACCCAGAGGGCAAGATGGAGAACAAAACACATTCTGAGCTATGCTTTGTGTTTGTTAAAAAAGCTAATTGGAAACATTTCTTGCAGGTTTGCTTCAAGCTGTAATTTAGCAAAAGAAACTTTGCTTTAATTATATTATATTCCATTTGTTTTCAACCTCATGTAATTTGTGCAGATTTGTTGGTAAAATACATCTTGGCACAATGAGTGTCTCTGCTGGTGCTTCTCCCAAGACTATCTTGAAGGTGGGCTGTTTGCCTTTCGTGAACACATTCTTGGTAAAGAACATCAAAAGTTTTAAAAAAGAAAATGAGCAAGAATCAGACATCACAGATGCAACTTCTTGTAATGGGAGATGAGAATGTACGGCTGTGTGCTTGTTGTGTGTGTTTGTGTGCCTGTGTGTTTGCCACAATCCTATTCAAACTCCCTTCTCCTGCCATCAAAGTTAAGGGGCTGTATACTGGGATGCTACAATAATTACTGGTATCTGGGTTCTGGGTTAATGGTGTATACTGACCCCATTACAGTCCCTCAGAGGTAGCTGCTAGGCGGTGGTTGGTGATGTGTTGGTTGTCCCATGTGCGTTTTTCATGGGTGCCTTTTCCCTACAATTTACTCTATTCCTAATCCTCCTCCCTCTAGTGGCCACCAATGCAATTCTTTACCTCTGGCTGGGAGAGCAGGACAGTTGGCTTCCTTCAAAGCCAACACCCTTGCTGCTGCATGGACTGGTGAGTAGAAGTGCTGTGCAAGCAGCATCTTCCTTCTCTCTGGAGACTAAATGCA", //
			"CCCGTCCAAAGGGTAGCTCCTCATAGCTCTCACCCTGCACGGCAGGGAGCTCAGTCCCCAAAGCCTTCCCTCCGCTGTTCCCGGCTTCGCTGGAGCCGTCCTTCCCTCAGCCCCGGCTGGCTTCTCGGCTAGGCCCTCTGCATGTGTAGCGTCAGGCAATGGTAAGAACTGCGGGAAAATAGCGGGTGAAGGGATGCTGGTTTGGAGAGAGAGGGGACGGCAATTTTAGATAAAATCACCAGGGAAACCTCTCCCATGGGAAACACTGAAACGGAACCCTGCGGGATGAGAGTGAGCCACACGCTATCTGTGGAAAAGCGGTGCAGACGCAGGTCAGTGGGGTCATCCCTTGCTCTGTATCTTTCACACCTGCGCAGCCGGGTCCAGGGGTCAGAAAACAAAGAAAAATTATGCAAGAAAGGAGGCAGCCTTAGGCCGGGCGCGGTGGCTCACGCCTGTAATCCCAGCACTTTGGGAGGCCGAGGCGGGCGGATCGCCTGAGGTCAGGAGTTCAAGACCAGCCTGACCAACATGGAGAACCCCTGTCTCTAGTAAAAATACAAAATTATCTGGGCGTGGTGGCGCATGCCTGCAATCCCAGCTACTCGGGAGGCTGAAGTAGGAGAATCGCTTGAACCCGGGAGGCGGAGGTTGCAGTGAGTCGAGATTGCGCCACTGTACCCCAGCCTGGGCAATAAGAGCGAAACTCTGCCTCAAAAAAGAAAAAAAACGAGGCAGCCTTTCCAGCACCCAGAGCCCCGCCCTCTCCGGCCCCGATTCCCAGGGCTCCGCCCCTTGCCTCACATTCCCTCCGCTCCGCCCTAATCGGTTCCGCCCAGTCCCTTGGTCCGGGGATGGCCCCGCCTTAAGGACCATGACTCCGCCCCTCTCCGCTCTCCGTAGTTACGAAGCTGGGCGTCAACGATTGGGCTGAGTCGGAGACTTCCTTTCCTGATTGGCCGTGTTGTACGGCGGCTTCTCGCGCAGCTGATGACCTGGAAGTGATGCCTAAAGCTGTGGACCGCGTGGGCTCGCCTCCCTGGGACTAGGTTTCAGCGGCCGCTGCGATGACCAAAATAAAGGCAGATCCCGACGGGCCCGAGGCTCAGGCGGAGGCGTGTTCCGGGGAGCGCACCTACCAGGAGCTGCTGGTCAACCAGAACCCCATCGCGCAGCCCCTGGCTTCTCGCCGCCTCACGCG", //
			"CACTGACATAATTAACTAAACTGTTACCCAGGTTTAAATTAAGCAAGGTGTCCGGGGACTGCAGCTGTTGTATTTATTTTGGTTGCATGTGATGGGAGCTGTTACTTCCCGGCTTGACAAACAGCCTCAAGATGCAAGCTGGGAGGCTGTGGTCAGCCCAAGGCTATACAGAGGAAAGGCAGGCCTCCTCTGAACCCCGCCCCAGCAGAATAAGAAGCGAGGGGCAAGGGTCAGCGCGGAGAGGGAGGGTGGAAAGCGCCATATAGTGCCCAGGGTCAGGGTGGGTAAACAAGCCTTTCTTCGTGTCATTTTTTTTTTTTGTCTCACCGATATCTGCATTGTAACTGAACTTTATGCCAGGAGACCTTAACCTTGTTAATCAGGTTTCCAGTCGAATCAGGATCGATTTTAATGCCTCCATTTTGGTTGTTGCTGTTGGAATTTCCGAAAAAGCCTAACTGCGCCAGTCTTTCTGTTAGTTCTCCACACCCCCTTCCCTTTCCGAGACCCCGCCCTTTTCTCTCAGCCCAAACCACAAAAGGCCGCGCAGTTTTAGCTCCTACACGGTGCTCCGCCCCCTCCCCAAACGTCGCAGTACTGCAGGGAAAGGAGGTGGGCTCGGCTGGGGCGGGGGACTGAGTCGGCTTCGTCTTTCCGCTCTTCAGCCAATTGGTGAACTCCCCCTGAGGGAGATGGGGGAGCAGGCTGTAGAGAAGGTCCAAGTATTTTTCCCTTTTTCTGCCACGTCCCAACCATTGACGTGAGCCCACTCCTCAGGTTCTGATTGGGCGTATGCATTGTCCCCCAGCAGAGACCCTGTAAGCTGACTGGTCATCTCTGCTATGGGGGAGGGGCTTGCGTACTCTCCTCCTTCGCCTCCACCGCGGCGCAAGGGGCGGGACTCACCGCCACCGCCTCTGCTTCCGCCGCCGCCACCGCCGCCGCCGCCGCCACAGCCGCCAGCCGCTTCGCGTTCGGCGGGGGAGGAGTTGGAGGCGGAGAAGAAGGCGGTGGTGGCGGGTGGAGGTTCGAGCGCTGTTCTCGCTCCGGAGCCGCTGCACATTTCGGTGAGGGTTTTTTTTTTTTTTTTTCAGTCTGGCTTGTTCCGCTGTCGGGAGTGTGGGGGTCGGGGCGAGGAAATTGCTGCGTTCAGCTCCTCAGTTTCTCTGTCTCTAGCTTCGGAAGGAGCGGGGCTTGTCCC", //
			"GTACTGCAGGGAAAGGAGGTGGGCTCGGCTGGGGCGGGGGACTGAGTCGGCTTCGTCTTTCCGCTCTTCAGCCAATTGGTGAACTCCCCCTGAGGGAGATGGGGGAGCAGGCTGTAGAGAAGGTCCAAGTATTTTTCCCTTTTTCTGCCACGTCCCAACCATTGACGTGAGCCCACTCCTCAGGTTCTGATTGGGCGTATGCATTGTCCCCCAGCAGAGACCCTGTAAGCTGACTGGTCATCTCTGCTATGGGGGAGGGGCTTGCGTACTCTCCTCCTTCGCCTCCACCGCGGCGCAAGGGGCGGGACTCACCGCCACCGCCTCTGCTTCCGCCGCCGCCACCGCCGCCGCCGCCGCCACAGCCGCCAGCCGCTTCGCGTTCGGCGGGGGAGGAGTTGGAGGCGGAGAAGAAGGCGGTGGTGGCGGGTGGAGGTTCGAGCGCTGTTCTCGCTCCGGAGCCGCTGCACATTTCGGTGAGGGTTTTTTTTTTTTTTTTTCAGTCTGGCTTGTTCCGCTGTCGGGAGTGTGGGGGTCGGGGCGAGGAAATTGCTGCGTTCAGCTCCTCAGTTTCTCTGTCTCTAGCTTCGGAAGGAGCGGGGCTTGTCCCTGGCCTGCTGCGCCGGCCATGGTGGGTGGGGTTGATGGCTGTGGGAGGCCTTCTTCGCCTCCTGGGCCCTGTCCAGCCCGGACCCGGGCCGCGCTCCTATCCGGGACGGTCGCGCCCGCCGCCCCCCGACAGATGACATTGGTTGGTATCGGGGTCTTCGCTGCCGCTCTTTACTGCTGCCTCTTCTGCTGTCCTTGGTGACTTGGCCAAGGAGCCCGCTGGCTTTCCCCTACACTCGCCACTGTACAAGAGTTTGCCCCCCAACCAACACCTCACACCCACCTTCTGCGGTTTCCCGTCACCCCGCGTAGAGCGGAACCATCTTACGCCACAGGCCTGCAGCACCCCTCCCCCGAGGAGCCTTCTGTTGCCGTCGTTTTGCTGGGCGTCTGCATTTTCCCCGATTGTCAGTGAGGCTCGGCACGCAGACACCCCGCTCGAGCCGCAGCAGTCGCTCTACTCCCGGGATCAGCACCTTTCCGCCAGGGCTGCTCTCAGTTAGCGATGTCAGGCCAGAGCTCCTCTTGCAGATGCACCTTTAATTTGGCCTCCCTCAGATGACTGGTAATCATTTTGCGCACATTCAAAATGGTG", //
			"CAGCAGGGAGTTCTCTTTGGGAAAAAGACCAGATTTGCAAGGGGATAATCTGTTATCAGATAACGTAGCTCTATAATCGTCCATTGGTCCTAGAATGTTGTGCTCCTAGTATAACTGCTTTCTCCTTCCCACCTTCTCTCCAAGGTCTGTGGTTGATTGTTTTGTCATTTAGAAGGCACCTAACACTCTTATCTCTTTTTGGAAAGAAATGTCATGGCTTAGAAAGAAAGTCTGAAGTCCATCTGCTTTGACATATCTGTGCTACCCTAAAATGAGGCTTTAATTTGCCATCGACCTACGTTAATTCATCGTAGACCTTCTTTAATTCACACCAAATCATAAATGATTTGGTGTGAATTAAAGTAGGTCTGTGGTTTCTCTAGTAAACGCATAATAGACTAAATAATGATAATTCAGATTATTGTTGAGCTAAACTCTGACCTCCATGTGGACATGTGCCATGATTCTTTGCTCACCCTCTGCCATTGCATCACTGCTGAGTTTAGCAGCTCTTAATTAAAACTCACTAAATTAAGGAGGATGAATGACCTTAAAGATACGAACATGAATAAGAGACATCATTTACCCTCAAAGAAACTAGAGTCTGATTGGGCAGGGAGGAAAAGGTATTAAAATTATGTCTTTCTGCATTGTGTGGATTTGAAGTTTGTTTTTTGTTGTTAAGAGTCTTATTATTAGGATAATGACACTGTTTTTTCTTTATTAAGTTACTTGTGTGGCAGTTAAGATGATTCTGGTGGCTCTTAACATTTTTTTTTTCTCCAGTCGGGAACATGCCCTGTGTGCCGCCGTCATTTCCCACCTGCGGTTATTGAAGCATCTGCAGCTCCTTCCTCTGAGCCTGATCCTGATGCCCCACCTTCAAATGACAGTATTGCAGAAGCACCCTAAACCTTGACAGTTGAAATGAGATCAGTGTATCAAAGTAAATCTGCAAATTCCTTCTAAATTTCATGTGCAAATAATTATATATAAATATATTTAAAAATGCTATATATAGTATATGCCATAGTTTAGAAAGAATATTAACCTTTCTAAACTAAATTTAGGTTTGCAGAAAGTATTAAACATTTTTAAGCTGAATGTTGAGACAGTGCATCCATTTTCTTTAGTTGAATATGTTTGTATTAATTGTAAAGCCAAGCTTATCAGTTGACTCTCTCCAGAATAAATAATCATC", //
			"CTTACATCCACTTAACGACGCAAGCAATTTAAAGCCAAGAGTTCTTGTTGGCAAGAATTGGGAAAGTTGGACGGACAGTATACAATTACCGAGCAGCCAATAAACAGGAAAAGTCATACAGTTCGAATGTTCTGACCAGGCGGATATTCTGTCTTTAAGAAACACTAATCATATACCTGGGAAAGAAAAAAATCTTGGGCCCCTTCAAACTGGGAATTACTCAGGGCAAATCTGCCTCCTATTCTATCCAAGTCACCTCTTTTGCTGACAAAGAATAGATGCATATTCTGATTGCCTCCTTTAGAAAGACTTATCAGAAACTCAAAAGAATGCTACTGTCTATTACCTACCTGGAAGCCCCCAGTTGGGGGGGGCGGGGCTTGCTTTTAACGGTCTCCGCCTTTCTGGAAGGAACTAATGTACTTTTTACATATTGATTGATGTCTCCTGTCTCCCCAAAATGTATAAAACCAAGCTGTGCCCTGACCACCTTGGGCACATGTCCTCAGGACTTCCTGAGGCTGTGTCACGGGCATGTCCTCAACGTTGGCAAAATAAACTTTCTAAGTTAACTGAGACCTGTCTCAAATTTTCGAGGTTCACATACCGTAGGTAAAAGTGTCAGAAGCCTAAAGGTGAGCCAGTGGAAAAGCCTGAAGACCCCTGCAAGGTTATTAGCTAAGCCTCCATAACGCAGGCGGCAAGTGTCCAGGAGTCTGGCGTCTGCTCGCCTGCACCACGACCTCCCTCCTGGGGAGAAGGACATGCATGGTAAGCCATTTCTCCATTTCCATACTGATTGCTTCCTCTCACCACCACGGCCCATCTGCGAAGCAAAACTAATCCTGGCGTGGAACAGGCCGCCTGAGGGAAACAGCTAGAGCCCGGCGCGGGGCGGAGCTGGTTGTGCGCATGCTCCGTGCAGGCGCAGGCGGGGGCTGAGCGCCGTCGGGTTACGCCTGCGCTCCGGGTGAGCCCGCGCCTGCGCCTTTGCGGCCGTGATTCGGTCCCGCTGTCCTAGGCGGGATGGTGCCGCTGTGCCAGGTAAGGGTGGCGGGTGTGCGTGCGGGCCTGGGTGCGGAGCCCTCCTCGACGTGTCTCTCCCGCCCTTTCCCTCCACATACCCAGCCTTGGTCAGTCGGACCTCCCCACTAGCCCCCAACCTGGCCGGCGTCTTGGGTTCGGGGGCGCCCCCGCCCCC", //
			"AAAGTAGATGGAGGGGTTTCTGATGAAAATGTTTATCAAACACATGCATCTACTTTTTTTTTTCTTTGAGATGGAGTCTCATTCTGTTGCCCAGGCTGGAGTGCAGTGGTGCAGTCTCAGCTCACTGCAACCTCTGCCTCCTGGGTTCAAGCAATTCTCCTGCCTCAGTCTCCCAAGCAGTTGGGATTACAGGTGCATGCCACCATGCCCAGCTAATTTTTTGTATTTTTCTTTTTTAATAGAGATGGGGTTTCACCACGTTGGCCAGGCTGGTCTCGAACTCCCGACTTCAGATGATCCACCCACTTTGGCCTCCCAAAGTGCTGGGATTACAGGCATGAGCCACCACGCCTGGCCCAAGCATCTACTTCTACTTGTTTCTAAAATATATTTTTTGATGGCATAAAACCATGAGGACAGGGAGTCTCACATTCCAGAGAATCCCAGTCTATCACTCACAAAGTAGGATGTATCCAATTTGGTTTGAATGGCTTGGGTCGGTTCTGGGTCCTCAGCTTCCATGACCTCAGGTCGTAGTGAATCTACACTCCAGGGAGGTGGAGCCACACAAAGGACAGCGGGAAGAGCTGGGAGCTTCCTTCTGTTCCTGTTTCCTCTCCTGAGGCTTCTTAGGTGAATCATGGAGCTTTGCTCCCTGGCAGTCCCTTTGCTAAAATGAGGATAAAAACATCTGTCCTCTCTCCTTCAGGGGTGTAGTTGCTGGACTAAACTGAGATCAGAAGTAGGTATAATCTAGAAATTCTCATTCTCAGACTTTGAATGTTAAAATCATCTCCCTTTAACATTCAGATTACAACGATGGCATGGACTGGCCTGCCCAGGTTACCAGATGAGCTTTGTAGTCTAATCCAGACCTCCCCTTAACTCTCCTGCTGTGGAAGTCAGTCCTCATAATTCCACCATTGTGGCATGTGATGCGACACTGTCCACTGACCACAGGGCCTTTCCCACTGTAAAATGTGATGTGTTCACCACTGTTTAACTGCTGTTACCAACTCCCACGAAGGAGGATCTGCCTTCTCTAACATGTCACTCAGCATGGGTGCCCCCAGGCACAAATCACGTAGTGCCAGCAGGTTGGTGGTGGCTAGAAGTGGTGTTCCAGCATCAGCCCTCCTCTCCTGCCCACCAGTCCTGGCACAGCACCCAGGTCAACAGTAAAAATCCTACAGGTAAATTG", //
			"CACCAGACCCTCCAAGATAAATGAAGAGTTAAAAACTTTGCTTCTGGCCGGGCGTTGTGGCTCACACCTGTAATCCCGGCACTTTGGGAGGCCGAGGTGGGTGGATCACTTGAGGTCAGGAGTTCGAGACCAGCCTGGGCAACATGGTGAAACCCCATCTCTACTAAAAATACAAAAATTGGCCAGGCGTGATGGTGCACACCTGTCATCCCTGCTACTCCAGAGGCTGGCTCATGAGAATCGGTTGAACCCAGGAGGCAGAGGTTGCAGTGAGCCGAGATCACACCACTGCACTCCAGCCTGGGTGACAGAATGAGACTCCGTCTCAAAAAAACCAAAAACTAAAAACGACAGAAAAAGTTGCTTCTGCTTCATTTCCTTTTCATCTCTCAGTAGAGTTTAACATATGAAATGTATTTTTCGAAATTTTGTTAATATGTTTATTCATGTTTTAATAATAGCATAATAACCTTATAAATTCCTAAAGACGTAAATAAGAACGGTTGTTCCCACTAACACCCCAACCTGAAGTGTGGCTCTGGCTTTACCTGAAGTGGGTACAGGGATGCGGAGGGGTGGAAGGACCGGTGGGGACAGATGGACCCATGAACCAGCGGGTGGATGTGAGCTGAGGGGAGGGATACGGACTGGAGGAGCCAAAGCCCGTGACGTAGGGGTGCGTGCTTCCCTCAATGGGGAGACCCTGCTGCCGCTGTTGTTGGCCAAGTCGCCGTGGCCGAAGCGCGGTGGCCGAAGCGGGGGACGGGGGTGGGACTTACGGGGACAGAAGGCGGAGCTTCCAGCAGCTGCTGGCCCCCCTGGGTCCAGAGGAGCCTTGCCGCCCTCACCTGCGCAGAGCCTGGAGCCGACGCGTCACCCCCAGCGGAAGCGCCTCGCTGCCCGGGCGTCTGGCGAGGTGTGCGCTCCGGGTCCCAGCTCCAGCGCTTCGCGGCAGTCGCCCGCCTCTTTCCGCAAGCAGGGCGGGGACTGGGCGGCATCTACCGAGATCTGCGATTCGAGGCGCCGAGGAGCCGAAGACCACAGCTCAGCTCAGACGGCGCCCTAGGGCCGCACAGAGGGTCGGGCAGTGCCGGAGAGAGGTTTGAAAGCGCCGCCGCCAACTCGACAGCGCGTCCCAGGTCAGTGTGAGGCGCCGTCCCTGCCGGCTGCACTTGGGAAACTTTGGCTTGCAGTGGCGGCT", //
			"AACGTCTATTTGTTCAGGAGGCGTTTTCCCGCGTGTCCCAGCATCTTCCCCACTGGTTATTTCAGGGGTGTGTGTGTCTGTGTGTGTGTGTGTGTGTGTGTGTGTGTGTGTGTGTGCGCGCGCGCGCCCTGTGCCCATCTGGTTCTAATCCTGGTTATGGATCCACAACCCAACCCCACCCATCCCGTCTCCCCCTCGCATTCCTCGTTCTATAAATAAATCCAGGTTCTTTGAGCTCTGTCATCCAAGGATACAATAGCAGTTGTTTGTTAAGAGAAATTTAAACTCAACACCAATACTGAGTCCAGTAAACCAGAAAATATGCTTCTGGTGATTTGGGTGGAAGGCCATCTCCAGTTTAAGAAGCTTTATGTGCCACTATCATCATTGTCTGTTATCCTTGATCTCCTTTGGTGAGGGGAAGGCGGGAAAGAGAGGACCTCTCCGACCCTTGGATTTTTCTTTTAAGCAGAGGCTAATAAAACAAAGCGCAAACTGACTGTCCTGAGGAGTCTGCTCACACCGCTGCCCCGCAGATCATTCCACAGACCCCTTGTCCCATCCCTTCTGGACAGACCCAGGACTGATGAGGGTTCGGGTTCGGGAGGTGTGGTGTGGTGGGGGCTTGTGTGGCTGTAGCGGCCGGCTTTCCTAAACCCCAGAGCGGGATACTCGGGATCTGAGCAGAGGGGAAAGTCATCTAAGAGGCTGCTGCTTTACAAGGTCTGGGCACAGAGCTGCCCCTCCCTTCCTCCCTGAAGTTTGCATGAAGTGCACATTAGGCTGTAATTAGGGGATTTGGGAGGAGAACTTTCCTGGTGACGCTTTGCTTTTCTTCTGCTCTTGGTGAGAAAGTGCCTCCTTCTTCCCAGGATCAGGACCTCTGCCATCCAGCGCCACAAAGAGACATTCTGCACACACACTCACACACGCACACACACACACACTCTCACACTCGCCCAGAGACAAACTTAAGGTGAGGAGAAAGAGCGCTAGCTTCACTTGATCTCCAGCTTCCAACTTAAGCAGAACTTGAGAGCATCCGAACTCCTGGATTTCAGGACAAGGTAAAACTTGCAAATTGTTTCCCCCTCTTTTGGTTATACCTTTGTGCCTTTAAGATTTCAGAGGGCAATACACATGCAATGTATATATACATTAAACAAATTTTATTCTTTTCTTTTGAGTTTAGTTTTCATTT", //
			"AAGGAATCCATGAGCCATTGCAGACAAAATGCTGCTTATAGTGCTGGCGAACAGTAGACATTCCATGAAAGGTGTTTTTATCATTGTTTATTCCCCATACTCCCTCACAAGCACTCAAGACGTTTACAATTTTAAATTAGGTTGTTGGATAGTCAGTACTTTAACACATTGGAATGAACCCTTAGTAGTATAATTTTAAATACTATGGTATGGAACTAAAGGAGATACATAGATAGATGTGTCATATATTTTTAAGACGAATGAAATTCAAGATGGTTGCAGTGGTAGGATAAACAACTCTAACCTAGTGTGTGTATCATATACAGCTCCCTATAAATTACACTATACATGACCACACTAACGTGAACATTAATTTAGCAAACTCTCCACCTTTGCTCATCTTGACTCCTCATCTAGAATGTCTCCTGTGTTTGATTGTTAAATTATACACCTTCTTTATACTCTCTCCTCCACAGCGCCCCTTCTCTGATTCCTGTAAACAAGCAGAGAGCACATCACAACTCTGGTACTGTTTGTCTGTTGTTTCATATTTGTGTTTACAGTTATCTCAATTATATTCCAAATTCCAAAAGGCCACCTTTATATTTGTTCACCCAAAGCACAATCCTGGGCTGAAAATAGGTAATATTTTAAACTGTAATTTTGTGGACAAAAGTTAGTGACTACAAACAACCAACAACAACAACAAAAATAAGCATTTCAAAAAAGTTCTTAGAGGAACAAGGAAGTGAACTTCTGAAAATAAGACTAGTAAATCAGGTAGAATTTTGAAGCTAACATTCTTGGTTTCCTTGACAAATAAATTTCACAATTTGTTTGCTTATCTACCATCTTCTTTATGTGATATAATGAAGAAAAAGAAAAGATTTAAAGCAAACGTGAAAGACTAAAGCCATCAGCATCACAGTAGTGCAAATCTTCACAGGGGAAAATACAGAGACATGTTTCAGTCTCAGAAGAGACTTGGGGACCTTCTGTGAATAAGTGAATTAGGTTCTGGAGTAGAGGCTCTCCTACAAGGACCTCCAGGAACAGACTCAGAGCTCTTTAATAAGTATCCCAGGGAGTCAGGCTTTCTTGGAAAAAACAAATTAGTGTGAGAAGCCAGTACTGTGCAATTTCAGTTTTAAAAATAGCCGGTATTGTCAAGGCTGCTGTGTGCATGAGAGAAATAGAAGGT", //
			"TCCCTAATCAACATGCTAGTCTATTGGATGCAGTGTCGGAGGCAAAAATCACAGAGGTTCTGAAAGCATCAGCCCATGGTTTTCTAAACATCATGGTTCAAGACCCTTAACACGAAACAGAAAGTTTCCCCGAGGCGCCGTAAACAACCCATTTGGGCGCTTCCCTGATAATTATAGTGAAATCTGGCATCTAATTTTTTTTGGTGGACTCTCAAATTTTATATTTATGTTTTGATTCCTAGAAATAAAAAATGTTTTTATAAGGAATTCTTTGATCGTTTATGTTTTATTCTTGATAGAAACCTACTACTTTATAACTTCGTTTATGTTTTACTCTTGATAGAAACCTACTACTTTATAACTTCGAACATTATTGATGTTCTTCCTGTATTTCTGAGAGGTGACAGCTTGCTGGCATCCCTCGCTGGCTCTCGGCACCTCCTCGGCCTCAGCCCACTCTGGCCGCGCTTGAGGAGCCCTTCAGCCCGCAGCTGCACCGTGGGAGCCCCTCTCTGTGCTGGCTGAGGCCTGAGCGGGCTCCCTCTGCTGGCGGGGAGGTGTGGAGGGAGAGGCGCGGGCCGGAACCTGGGCTGCCTGCGGGGCTCGCAGGTCCAGCGCGACTTCCGGGTGGGCGCGGGCTCGGCGCGACTTCCGGGTGGGCGCGGGCTCGGCGCGCCCCGCACTCTTGAGCGGTCGGCTGGCGCCGCCGGCCCTGGGCAGTGAGAGGCTTAGCACCCGGGCCAGCAGCTGCGGAGGGTGCACTGGGTCCTCCAACAGTGATGGCCCGCCGGCGCCGCGCTCGAATTTTCGCTGGGCCTCAGCCACCTCCCCGCGGGGCAAGGGGGCAGGGCTCGGGACCTGCAGCCTGCCATGCTGGAGCCCTCACCCTCCTCCCCGCCCCCGTCCCCTGCCCCCCGCCCCCCGCCCCCCAACCGCAGGCTCCCGCGCGCCACCCCGAGGGGACGGGCGCCACCTCCTGCTACGCGGCACCCGGTCCCGTCAACCGCCCAACGGCTGAGGAGTGCGGCAGCGCGCCAGAGACTGGCGGGCAGCTCCGCCCGCGGCCGGGATGCACTAGGCAAAGCCAGCTGGGCTCCTGAGTCCGGTGGGTACTTGGAGAACTTACTACGTCTAGCTGGAGGATTGTAAATGCACCAATCAGCATGCTGTGTCTAGCTCAAGGTATGTGAACGCACTAATC", //
			"CATATTCAACATCCACAACTCTGAGAGATGGGTACCATTATTATTGCCATTTAAGATGAGGAAACTGTCTCATTATGCTTAGAAACTTGTCCCAAGTCAGAGCCCAACTATAAACCCTGCCTAATTGCAAAGTCTCTACTTTTAATGATGACTCTATACTACTTCCATCAGATAACCCGTCAACAACACTGAGAAGAGCATATGAGAGGCAATGCTCCTCTCGTGTGGCTAAGCAGATGAAGGTTTCCGTATAGGAATTCAATAAAAATCTATGGCTGACAATGAAAATGACTACTCAGATCCCTGTGCATTTTTAAAGGTCAATTTGTTTTTGAAACTTTGTGGTTTTTTTCCTACTGAATTTAATCATTCTTAAATTCATACCATCTCCCAAAATTTGATTCTGCAATATTCTCTTGAATTTTGAAACCTAGTTGAATTGAAGGGGAATGTTCATGCTGTTGAGAAGAAAACTGGCACCCCTGCTATCCAGAAGTGTTCTGTAAAGGACTCTTTTGAATATGTAAAGGAATTAGAATATCTCTTAATATATGTATTGATATATTCATTAGTATTGGCTACATATATTTGTAGAAACCAGGAGTCACTATTACCCAGAACATTTAGCCCCATCAAATGTCCAACTGGATCTGAAACAAAAATAACCTTGGAACAAAATAGGCCACATTTGTCAGAAACTGACTCCCAAACTCTGTGTGAGCCACCCCGCCCCATCCCCAGCCTTGTTTTTTTTTTGTTTTTCCCTACTCTGCAGTCAGCCTGACTTTGGCAAGCAGTCAAGAAGTGAACAAGAGAGATGGAAGTGCTCTGGGTATCTGACCCCTTACTGGCCGCTCAACCATGCAAGCTGTGAAGCCCAGTTTAAAAAAAAAATGTTGCCTTTCTTTTCCTCTCACCCCTTAACGAACTGCCTCATTCTCTTCCCCACCATCACAGAAGGAAAACAATCAAAAGCATTAGTATTCCCGTGAAAAGGGCCAGTCACACAGGGGATGACTGCAAGTCCTTCGTAACCACTCCAGGACGCTGCCAGGGCCTGAGAAGGGACAAACTCCAATTGCTCCAGTTCAAACTGGTCCTGGCCTGGTCAAACGGAGGAGACAGAGCAGTGATTCACAGCAGGACGTGGTACTTGCAGACTGACCGTCCTCTCAGGACTCACTAGGGTCTCTTCTCTGAC", //
			"GAATGCAGCATGAAGGACATTTGACTCTTTCTCTTCATTATATACAATAGTGTCTTTGACAGAACTTATAAACCCCAAACTGAAGCCCTGTGCTTTCTTGCCTTCCAGTGATAAGGTTTCCAAAGTCATTATTATTTCATGGACCAAAACCAAACCAATTCCCCATCCCAAATTACTTATATCTTGTGGTTGGTCATGTCCTTAAATAATATCAGTCTATCTGTCTCTGTCTCTGCCTCTGTCTCTCTTTTTCTCTCTCTCTCTCATTTGAAATCTCAGTGAGAGTTTTGGTAAAGTTGTGTAATCCTTGTATCAACTTAGTTTTTCCATGTTTTTTAATGCCATCAGGGTTCACAGATTGATGAAGGGTCAGTTCCTTTGGGTTTAGTCTCTATTCTTGCGTTGTGGCTTTCTATTTGGAGAGTGAATAACTCAGAACTAAGTTATTGCTTCACTGTTAAGGCTGGCAGGGGAAGAAAGAGGCAGTAGTGGAGCACAGAATCTATCTGTGTTAGCCCTCTGTCACCGACTACTCAATCCAGCTTTTGGCTTCACCTAAATGATAAATGGTCCTATTGTTTGGGCTTTAGTATTTTAAAAAATAGAACTAAATCTAGATTAAGCAAGGACAGAGACCCATTTCATTGCACTGTACTTTAATATTCCTGAGATCCATGGCATTACGGGTGGCTCCTTTTAACAGCCATCTCTCTCCAGGGCAGAACACTATGCTGTCTTTGTTTCATCTGATCTTTCAGCTAAATAAGAGATGGTCACTTATCCTCACTACTCCCATATCCTGCTGGATTTCTCTTGCGAAAAGAGAAACTCACCACACGGGAAAAGGTAGAGGTTTCAGGCAGAGCATTTCAGCGTGTGTTTACAGGCAGGACTGACTTACTCAGCAAATCCTGTTCAGAGGAGCTTCAGCCAATGAAGAGGCTATATCCCGGCTTGCTTGGGGGCTTCTGAGTTTGAGCTGCATGCAGAAATGTAAGGCAGTGCTTCCTGGCGCTGATGGCAAAATGGGACCCATCTCCAGTTATTCCTCGGGCACCCCAGCTTTGCAAATCTGGGAAAGAACCTACATAAACATATTGGTGATGTAGCCAGCTTAAAAGGCCGAGAGAATCCTTATGGATTCAGGAGACTGTTTTTTTATTTAGATTTTTATTTTTTATTTGGAAGATGAAATGCTTCT", //
			"TTTATTAGGAATGCCAATGGTGCAATGGTCTAACTGTGCAATGATCTTATCGTGTTTTCATAATTAGGCTTTGGGACTGCCATAGGGAGAATCAACTCGAACACAGTTCACTCGTGTCAAACACACTTGAGAGCGCTCTGGAAGATCACTGCCATACATCATGCAATCCAGTTCTACTTAGGCTCTGACAGGTACATTTTGCCTGTTTCAAGTTTGTGATGAGCTAATAAGTGGTCCCCTCTGGTCCTTCAGACCTCTGTCATACTTGTTTTTCTGGAAAAAGAAGAGTAAATATGAGGATGAGCATGAAACAATCAAAGTTGTCACCTAGATATATTGAGAATCTTTTAGTGTCAAGTCAGAGAAAATCTGGCTCAAAACCGGCTAAACAAAAAAAGAAATTTAGACTCTCATGTAGCGGAAAAACCTGGAGCTGTCAAAGCTTTGGAGCAAGGCTTGATTACATGGCTCACCTATGTCATCAAATGAAATACTTTTTTCCATTTTTCTTCTTGGCTTCCTATGACATCTCGCTTACTCCTAAAGCTGAATTCCATTACACTTCCAAAATTATTTCCACTATCTTCCGGATCTTTATTGTTCTACATGAATCTCCAACAGGGAGAGAGAACCTCTTCCCTCGTAGTTCTCTGAGCCAGGAAGCATTTTTTCCCAGAAGCCCCAGCAAACACCTCCTTGTTTCTCACTGGCCTGAACTGAGTCTTCTGATTTTAACTGATCACTGCAGCAAAGCCAATGGGATGTGCTGATGGGCTTAAACCACCCAGAGTTCTACTCTAGAACTGAGAGTAGGGTGTACTTCCTCCAAGCACATGGGCTATATGGGTAAAGGTACTGTTACCAGGATAGTGGGAGAATGCTATGTGCTGAGAGGGTTAAACTAGCAAAAGCCTATTATCCCACCATTAGGAAACATTGACTATTGTGAAGTCAGTGGCCACCAGACCCTGAGCTCCCCGCCCCATGTGCTGTGGAGTCTAGGATGTTGAAGAGAGCTGGGAGGAGATGCCAAGAACACTAGGCTGCTTCCTACCATCTAGCTTACAGTCCAACATGTGAGTAACACCAATACAAATGAAAAGCCATCAAATGTGTCCAATAGTGAATATCATCCCGGCAGTCTCTGAGCTAGGAGGGAGAGTCCTGGAGAGGTGTTTATTTTCTGATATAAAAAAGGTAA", //
			"TCCAAATTGGCTTTCTAGTGATTTGGGTTGCTTCATTAAATGTTTTTGGTTGTTTCATAACAATAGTAATCTCATTTTCTTTCAAAAGTAGAGTTTGAAATTTGGGAAAACATGGGTGGGGAGGGAATTCTACATTTTCTTCCTAGTTGAAGTAGGAAGACAGGTCTCTTTTCTGTATTAATTGGACTCCTGAAATTCCCCCAGGGTAGCAGTGGCTCTCTGTGGGCACAGGGCTCTTATGCTTATGTTTCTGTGCTTCAATGTACCTTCCATGCTGACAGCAAATAGAAGGTGTTCCTGCACAAGCAAAACCTAACGGAGAGGCTGCAATAATTTGTCCAAAACGAAGTGCAAACTGCAAGGGAGAGTGGACTTTTTCAGACTGATCCTGTTTACTAGTCTGGTGCTTTCCTCCACCTGTGTAGACACAGATGCTTTATTGAACACCAGCCTCCTACAGTTAGAAGGTATTTTGACTCTTAGGAATGGGGGAACCAGTTGCAAAGTTCTGGCCCCTGATCCCCCTCCTATTCCTACTAGAGGGCTTAAGATAGGCTCTGAGCGGCACTTCCCACTTTCTCCTCTTTTTCTGAAGCTTCGAGCCCCAAACCTCGCCCTTTAAATTCACCAAGGGACCACATGCATCATATAAGGGTGAGGAAAAGGGACCTGGATGTAGACTTCCTGACTATCCAAAGGAACCTACTTCTCTTTGCTGTTCTCAAGTTCAGGCGTCCGGAGAAAGGCAGGGAGCTAGGGGCTCTGGGTGCTAGCGTAGACGTGGCCCTTAGCTGAGTGGCGCGAGAGGGGAGGAGGTTGGAAGAGCGCGTCTATGCGTTCCACCTGAACCTGCAAGCTAGTCGCTTGCTGCCCAGGTGCCGATTCGCGGCTGCATGCGCCCCACTGCAGCAAAGAGCAGCCGCAGCCTCTGCCTCGGCCACCACTGCTGCTGGGAAAGAGTCGTGGGGCTGCTGACGCGGTTGGGAGGAGCCTCGCCTTTAATGCACCAGCCGCCTCCAGCCTCCTGCAGCAGCAGCAGCAGCAGCAGCAGCAGCAGCTGCGAGTGCGCGCGTGTGGGTGTGAGGGTGAGTGCGCTGGCGCCGGCTGCAGCGCCCTGCCCAGCTCCCCGCCAGCTTCCCTACCCCCGCCTGCCGGAGCCGCCCTCCCGTGGGACCAGCACCCTCATTCCGGCCAGGCAAGC", //
			"GGGGGCTGCGCCTCGGCCTCCTCCAGGGCAGGTGCCCCTATGCCCCTCAAGTCCCAGCACCCAGCACCACAGAACCGCCCTGTCATCTCGGCCCCGGAACCTTGTGGGGAGCCAACTCCTGCGGAGGGCCGGGCAGCCCCGGGCGGAATGAGCCCCCTCCGCAAAGCCTGTCTGGGGAGCGGGGGAGGGGCGGCAGGTGCCCGGAGAAAGGCGAGATTGGAAGAGCGGCCGGGATGGTGGCGCTGCATACGGGCGACCTGCAGGGAGAACGGAAGCTGAGGCCGGGGTCAGGGGTGCGCGCTCCGCTCCTCCACTCCTGCGTGAAGGGCGGCCCACTCCCGAGCAGCTGCGCTGGGCGCCCGGCACGGACCCGTCTCCCTGCTCAGGTCGATCCCCGGCCGCGCTGCCCCCACCTGGAAGCCCAGTCCCCTGGGTGCGCCCCGCCTTGGGGTCCGAGCACCCTCACAGCCCGGCCCTGCCGCTTCCCTGCACCGTCTGCGGCTCCTTAAAGAACTGGGCGCGGTCGGGGCTCGCCCCGCACCGTCCTCGAGGCCCCGCCGCCAACCCACCCCACCCCAGGAGGCCGCGGCCGCGCTCATCCATCCCCCGCCGCACCGTCAACGCGCGTCCTGCGGGGCGGAGAGATCGGGGTCCGGTCGTCCCCGGGGGGCCCCAGAGCCAGGTGAGGGGCCCCGCCGCCCCCTGCCCAGCGGTTCCCACGACCCGCGGGTCGGCCGCGCCCTGCGCTGAGCGGATACCCGGCGGTCCGCGGGGCCTGAGCAGAGTCGGCCTTGTAAGGCAGAGGTCTCGGTGCGCCCGCGCGTCCCCGGGCCGGGTGGGGCGGCGGGGGCGGGGCTGCCAATGAGCGCGCGGGGCGGGACGCTGGGCGGGCGCCGGACGCTGGGGGCCGGGGGCGGGCCCGGGACCGGGCTGGGCGGGGCTGGGCGGTGCTGGGCGGGACGGGCGGGCGCGGGGCGGCGGCTGGAGAGCTCCGCTGAGCACCGGGTCGCGCGCGGCCGAGTCGCGGAGGCGCCTCCGCTGCAGTCGCTGCGTCTCCAAGGTAAGCGGGCGCGGGGCTCGGTCCCGCGGGCGCGGCGCGGGGGGCACCCGGGGGCGGAGACTGGGCCCGTCAGCCCCAGCAGCCACCTGCGCCGCAGCCCCTGCCCCCCGCGCTCCCCCGAGCCCCCTTCCCGGACCGCCC", //
			"AAGCCTTCCAGCAAGACAACTACTAGCCAAAGGACACCAAGAGTCGCAATCTCCCAGGAAGTGGTCAAGAACCTCGTGAATGAGATCCAGGGATCATCACCCTGGCCCCTTCTTCTTCAAGGCGCAGAAGGCTGCAACGACCTCCAAATGACTAGGTGCGGCAGTGCCCTGCAGCCTCGGTCCCTACTATGAAAAGCTGCAACACATGAGGCTTTTGTTATTTTAAAGTGGGTGTGGGGTTGACAGAGAGAGGGATAAAGACTAAGGCAGAAAATTTTTTAAAAAGCCACCTCTACCCACAGCAGTCCCATAACTCGAGGCTAATCACTTCAGTGGAGCCAGGCACAGCGTGGGGTAGAAGCTCAGCGGTTCCCTTTTAGCTGATCTTCAATTTCTGGCGTCTCGAACCCAAGAAATGAAAAAGGGAAGCCATGGGGGAGCAGGAGGAGAAATCAAGGCGCGAAAGGACCCTCAAAGCTGTACTGTCACTCCACGCCCAGGCTGTCATTCTCTCCAGCACAAGCCCCTCGGTGGAGCATGTGTTTAGGGTCTGCAGAGCACAGGAGACAAAGCAGAAGCCCAGCAGCCGCCTCAACCCTGGCATGATCAGGTTCCGCATCTCCGGTTCTCCTCCGGTTTTGGGCCCTCACCTGCCACGAATGCGGTCGTCACGCGAGCAGTTGTGAGATGGTCAAGCCCATTCAGGTCTAGGTTACCCCCTTTCATCCCGGAGGCAGGGGTGAGGTGTTTGTCAAGAGACTTCAGTTGTAGGCGTGGACGTAGGCTACTTCCTTCAGTAACAGCTATAGGGCCACATAAGGGAACCTCAGGCTACTACACCTCAGGAATCTCCGTCACGCCTCTTCCCCGCCCTCCTCGGGTCTCACGCCGAAACCTCACCGGGCGGAACGATTTCCGGCAAGAGCCAAATGATCAGTCCTAACCTTTCTGACCGTGGTTCCAAGGTTCCCCAAAGGACTGCCCTCTAAAGGACCCGGGTAGTTCCGCTTCCGGCAGCGCGAGATAAATCACGAGAGGAAGCTTAAATCTGTCGTTTGAATTTAGGACCACCTCGGTGAGTGGTCGTTCTGGTGTGCTGTGTCATACCTACTGTTTTTTAAAGTGAGGCGTAACCCGACAGTAATTTCAAAACCATTCGCCTCGACCGGCCTAAGGAAGGGTTTAATTGAGTCTGTGGGGA", //
			"TAGTCATCTTTGCAATCTCAAACCCAAAGGAAGCCTAACTTGGTACAGTGCATGGCACATTCAATGTCTGTTGAATACAAGTAAAGCTCATACTACCTAGTCTATTTTTCAGTATCTCTTGAGTTAATGACCTGGTTAACAAGTCAGAGTCACAGTCCCAAGGGCAACAACTTTTTAAAGGCTCCCCTAACGTACTTCAGGATCCCTGACTGAGAGTGGCTGACTATGGCAGTATATGGGCATACTAATCACTGGAGAAATAATAGCGAGAAATAACCAAGGAAAAGCACTACTCTCCAAAAACATTAAAAAAAAAAAACAACTGAACAACTAGTAAGAAAGAACGCTGGATCTGAGCACCCTGTAAGTCGCTTCCCCGTGGGCCGGGTAAAGGTTTTAAAACCCGCCGCCTCCAGGAAGACGGCCACGTGCACAAGGATTAGCTGCAAACTCCGTGCCCAGTCTGGCCGGGAGAATTGCTAAAACCGACCTACCGTTAACACCCGGGCTACTCTGAAATTAAGCAAGGCTGTAGTCAAGTAAGTAACCCAAGAAAAGGCGCACAGAGCCGCCGCCGCAGGTGTAGCCCGGGGACATCCCCTCCTACCTTGCAAACGGGGCCTGGGATCCGGTCCCTCTCTTCTTCCTCCACCTCCTCTGCTGTGGCGTGCCGCTTAGCTGGCGGCTCCGCATCTATTTCGTTTCTTTTTGGCTTCTTCGCCTGAACTGCAAAGCCTCCACGCCGTTTCCTCTTGGTTGCCGCCATCTTGCCTCGCCGCCCGCGCTCTTGGCCTCCTTTCCGGCGCTGCCGCTCGCTCCTATTTCCGATCTCTATGGTTCGGCGGCTCTAAGCGCTCAGCTCCGCGGTCGTCCTCCAGGTCTGTGCCGCCTCCTTCCGGTCTCGGTGGCGCGGCACGCGCGGCTCTCTAGGCCTCCTTCAGCTCTGTGGTGACGGTGGCCGAGGTGGAGGGCCGGTCTGAAGAGTGGCGGGACTGGCTTCACTTCCTCCGCGGTTCCTCGGAGCCGCCTCGCTCCTCTTCAGGGACTTTGCTGAGAAGGGCTCTCGGGCGTCCAGACCCCACCGCAAAGGTAGGGTTTCCGCACTCGGGGACCCCGAGACTAGTAAGCGATGCTGTCCCGCGGGCGGGCTGGTGAATGGGGTTGTCTGGGACTCTTGGAGGATTATCTGGACACTCCGGGC", //
			"GAGCTCGCCGGACCTCGCCAGCACTGATGGGAGTTTTACTGTCTAATCCCTTCAGGTGTTTGGCGATCCGCCGAGAAGTTGTTGGCCCCAGGAGCATCCCTCGGGGCCGAATGCGCAGTGGACGATGCCCCTTCTGACCCAACAGATCCAAGACGAGGATGATCAGTACAGCCTTGTGGCCAGCCTTGACAACGTTAGGAATCTCTCCACTATCTTGAAAGCTATTCATTTCCGAGAACATGCCACGTGTTTCGCAACTAAAAATGGTATCAAAGTAACAGTGGAAAATGCAAAGTGTGTGCAAGCAAATGCTTTTATTCAGGTATGGAAGTAGGCGCCTTTAAGCCTGTGCCATAGATACTCTAAATGTAATTTAATGAAATCGGAAGGTTTCAGTAGACAAAATAACTTAATAGTCATAATAACTTGTCAATAGTCATAATAAATAACTTAATAAATAAATAATAGTCTTAAGTTAGAGAACAAAACGTCATTACTTGAATTTTTTTTCTCTCCCGGTAACAAATTCTTTTCCCATCTTGTTTTACAGAGATAATGGTTAGTAATGACTAAAGTTTATTAAACTAGGCTATTGGCTAGGCTACAGTCTTCTTATTGTTTAACTTATTTAATCTTAACAATAGTCCTGTAAGGTAGGTTCTGTTATTATTCCCGTTTTATATATGTGGAAACGAGGCACAATTTTAAGTGATTTGCCAACAGTCCCACAGCTGGTAAGTGGCTGAATTGGGATAAGAACGCTGAATATTTGTTTCCAGAATATATGCTCTTTTTTCCACTGCGATATGCTATTACTAAAGAGTATTTTTTTCCTTTACTGGTTGTAATAGAATTTTTGTTCTTCCAAAGTAATATTTGTGTGGTTCAGATAGGATTGCAAGTTCAACTAAGCGTTTTTTAACACGTAAGTGCACAGCATGATCTCAGGGATAGAGAGGTAAATAAGACACGGTTCTTGTCCTCATGCAGGGGCATGGTATGGTGGCTCACACCTGTATTTCCAGCTGCTCAGGAGACCGAGGCTGAAGCACGAAGATCGCTTGAGCTCAGGCATTAAAGACCAGCCTGGGTAATCATAGTGAGACCCCCGTATCAATAAAAAATCATAATAAAAGTTGAAACTAATGTTGCTCATGCATTATCAAGTCATTCGTCTATTTTCCAAGTGACCTTAGCAGTA", //
			"TTCTAAATTTATTATCATTTTTGAGACAGGGTCTCACTCTGTTGCCCAGGCTGGAGTGCAGTGGCGCCATAATGGTTCACTGCAGCCTCAACCTCCTGGGCTCAAGCAATTCTCCCACCTCAGCCTCCCGAGTAGCTGGGACTACAGGCAAGTGCCACCACACCCAGCTAGTTTTTGTATTTTTTGTGGAGACAGTGTTTTTACCATGTTGCACAGGCTGGTCTCAAACTCCTAGGCTTAAGCCTGCCTCAGGCCTCACATAGTGCCAAGATTAATAGGCGTGAGCCACGGTGCCGGCCCATCATTTTATTTTAATTACCATTAAATGGCAGGAGTCAGGCAAAGCAGGACTCATTAAGCAGGAGATCACTAGGGCTCAGGATGATGAGGTGACTTGTTCAGGATCTCCAGCTGGTCAGTGCAGAATTCCTTGGGCTGCGGTTTCTAAGGCCTTACTTGACATCCAAATGTGACTCGGCATGCACCAAGGAAGGAGTGGGCCCCTTCTTCACTATGGATGGAGAAGCCTCAGAGAGTAAGTGGCAACAGGTAGGATAGGAACGGGGTGGGAGGATGTGGGCTGGGCATCTGGCAGTTTCCTCAGGGCCCTGAGCACTGGAATCCCCCACCTTAGGCGAAGGAGGAAATGGAGGTCTTCAGGACAATCTAGGGAGGCCAGGCCAGGTTGCCCCAGCCCATTCTCGGGTGCCTTTTCCCAGCCCAGCTGAGGAAGCATCAGGCTTCCAACTCTTCTCCCCAGATCCCCCCCAACCCCAACTCCCTGGAATTCACAGCACGGGACAGCCAGTCTGGGAAGTGACAGGTGGAGGCTGACAAATAAGGGCCCTCTGCTCTGCCAGCGGGCAGGATGGGGCCTCAGCTGGGCCTTGCGGGGCTGCCTGGTGGCGGCTCCTCCATCTCGGGGAATTCCCCTCCTGCCTAATAGTCAGCTCAGCCCAGCCCTCCTTCCTGCAGAAGCACAGTGAGCCGAGGAGCCTTCATAGGGACAGCCGCCCCTGGTGCACACACCCTCGTATTCTCCTGCCCTTCCCCAGGTGAGTGACTAGATGGGGGCCGGTGGGCCAGGACAGGGGAAAGGACCCACCGCACAAGGGTGCTGGGGCAAGCAGACACCCTCAGCTCTGTGCCAAGAGCCACCTGGCCTCCCATGTCCTGCCAGCCAGGGTCTGCTGCTCTCAGC", //
			"ATAAAGCTATATTTAGTTTAAAGAGCTGTTCTTTATTCCAAGGTTAGACCTTCCACTTTTTGGAATTAAAATTTAGCTTTCTTTCTTTTTAAAAATAAAAGGATAAGCTTTTAAAAAGCTTTCCTACCCTTAAAGCAAGTTTATGTTCCTTACCGGTCTAGGAAATCCCAGTCTGTTGGGATTATCTAGCCCACATATAAGTAGGTAGGGTGGAAGGCGGCCTCTGGATTTGCCCACCCGGGTTTGAATCTTGATTCAGCCCCTTACTGAGGTCATCTAACCTTTTTGTGACTCAAATATTATTTAAGAAAAGGTAGTAAAACCCACCTCCCTAGCGTGATTGTGCGAAGAAAATAATGCACGCAAAGCATCTACAATGTCTGGAGCAAAGCGTTTACGATGTCTGGGAATCCTTTACAATGTCTGGAGCAAAAAATGTTCGCGGTAGAGTCTTTATGGGTCTTTTCAGCTCGGAATTTCTATAGCTTTGTTAAGAAGAGTCCTGGAGTGCCCTGGACGGAACTCTGGGGGCCTGAGCTCCAGCTCTGGCTTAGGGCCAAATGCTTACGTGCCTTTTGGCAGGTCCCCTCCCTGTTCTGGCCTGTGTTCCCCTCGGCACAAGGAAGGGGCTGGGTGGTCTCTGTGGCTCGCCTCCGCTCTGAAATTCCGGCTTTCAAGGTGGCCCCAGGCCTTCGACAGGGGCTTCCGGCGCTGGGAGTGCGGCAAGGCGCCGCGCAGGCCCGGGACGGTGGCCCCAGGGAAGTCGCTCCTAGAGGGGAAGGCCGCCGAAGGGACGAGAGACACCGCGGGCCCCCGGAACCGCAGGCAGCCGGTGTTTACCTCGCTCGGTCCCACCCCAGGCGGCCGCGGGCCCGCCCGGAGCGCGCGGCGCGATTGGGCGAGTGGCCTGCCAGTCTCCGGGGACTTTCCCAGGGGTGGGGCGGCCCGGCCAGGCCCCCGGCACTTCCTCGTCCTCGGCCCGGGTGCCCTGCCCCCGTCCAGGAGCCCTAGGAGTGCTACGGGGGGCCGGAGCCTTGCCCGGGCCGCTGCCCCGTCCCTGGATTCGGGGCTGGACGCAGCAAGCGGGGCGCTGTGTCCCCAAGCTCCCCGTCCTCGGGTAAGCCGGCCGGCCCGGAGCGGGGATGGAGGGGAGGAAGGGAGGCCAGGCCCAGATCGTCCAGAGCCTCGGTGGCGGCGGGGA", //
			"GCCAGTTATTTGTAGAATGTTTTTCAGTATAGACTTATTTGCTGTCACGATTGAGGCCTCACAAGTGATTGGATCTCATGAGGAGATTGAGGTTACGTGTTTTGGCAAGAATCTCACGGAGGTGACGTTCTGTGTACATCAGAGTGTGCAGAATATTTCTCAGTCCAGCAGACCAGGGGTACATGATGTCAGTGTTTTACTACTGTTAACCTTCATCACTTGGCCAAGTGCTGTCTTTCAAGTTTCTGCAATCAAGTGACCAATTTTCCCTTTGTAATTAATAAGTGTTTATTTTTGAGGAGAAACTTAGAGACTATGCAAATACTCTATTTTTTCTTAAACTTTCACTCACTGATTTTAGTGACCATTGGTGGATCTCCACCTGCAGCAGTTATTACTGTAGTAATGGAATGGTGATTTCATATTTTCCTCATTCTTCCTACACTTATTAATCAGAATTCTTTTGTAAGGAAGAGCTGTCCCTTCTCCCCCATTTATTTATTGATTCAGCTATTTTGTTCTATCAGTATGAACTCAAGAGTATTTATTTCATACTGTGACTTATAATCCAATACTATTGTGATGTATTTTGCTGTTCAAAGTGTTCCCGTTTTGGCCATAGATGTTTTTTTCAGGTTGTCTCTTGTGCCCTTTTGACATGCTCTTTTATTATTATTGTTATTAGTATTATTCATTTGAGCACTTCCTTACTTTCCATCACTATAAGATGCATCAAACTCACCTTATATTTTATCTGCTCCAGCCCCTCAAAGCAGCCCTGTTTCTTTTTAATAAAAAGAAAATGGTATTTAGAAACCAACTGAGCACTAGGTTTGCTCAGTGCTGCTGGATGTCTCTGCTTTAGCCCCTTACAGAGGACAGAGCTGGGAAACATCTATGTGTATTACACAGATCTTTATTTCTGCATCTTTCTAAAAATGTTTTTAAATAAACATTTTATATAACTTCAACTTTTGTAAAAGTTTTATAAAACATATATGTTTTATAGAATGAGTATATCTATATCTATATCTGTCTGTATCTACATCTCAAATGTCTTACCTTCCAGGCCAGAGAGTTTTTTGGCTGATGTGTGTAGGGAGTGAGGAGAGTGGCTGGTAGTACCTTAGAGGTTGTACCTAAACAGGGAGAATGACCGTAGGGAGGGGAGAAGGGAAGGGGAGAGGGCCTGGCAGCCACC", //
			"ATGCTCCAAAATGAGCCTAAGATAGGGGTTTGTCTTCCTCCTTTTAAGCCTCCTCAGGGCAAGCATCTCACAGCGCATCACAACTCTATGGCTTTCAAATATGCTTATTTGCACAACACAGCCCCTAAAGGTAATCTAATTACTTATCTGGGTTACAACCAAAGCAACAGTAACAAGTTTCAAAACTGAAGGAAAAACCGTCCAGGATGTAGTCAGAAACAGGATGCTAATCTCAAGCGTTGCACCTTCCTAGGAATTTTTAAGGGTTAATTTTTATTACATACAATGTTTGCCTCACGGGCTGACTTAAAAACTTGACTCCCACATGAGCTACACAGAAAACTGTGCAAGAAGGAAAGGTTTTCACAAACACACTCACACCTATGAATTACTGAAGTCCCTGCTCTGTGTTGACCTGCAGTGCAGTATGTTCAGATCGCTCCCGTTCATCTACGGTGTAAACTTCTCCAAAACATAGCTTCTTATTTACACAGGAAAGCTCTGAAGGGCAACTAATGATAAAGCCACAGGAAAGTTTTCTCTGCAAGCAATCAAGGCCTTGCTCGGGAAAAGAAACGTTGAGTCTGATTTTGTTCGTTTAAGCCCAAGTGTACAAGGAAACAATAAACATCACTTCCTTATTCTAGAGGACTGAACAAAGGCTTTGCCGTGCATAGATATGATAGAAAAGGAAGTGGCACGTTTGAAGAAATAAATAGCAGCAGCGTTTAAGAGGATCCTCTGACCCTCCCTTACCTCCAAACAGTGTCTCAAGGCACTGCCCTGGATTGAACCGCAGTTCAAACTTCTTAAAACCACCCGAATGGCCCAGGTCTGTGAGTTGGCTCCTCCATTCATAAAGCCATAGAACACCAAGAAAAAGTTATGGGGCCGCAAGAACTAAGGAAGAGGCGGCGATTTAGAAGGGGGAGGCCCGCAAGATTCCAAATTCAGGTCCTCCTCCCCACTACTCCGTCTCCCATCATCATTCTCTTCTCGTTTATCATCATCCTTCCACTTCAAATCACACTCAGTCCTCCTCCTTCTGACACGCCATTCTTCTTCTGTTTTTGTTTGGTAAAGCAAAGTTAAAATTACGGGCCTTAGGACTAAGATAACTAAGTTCGAAGCCAGGCCAGGCCACGTAGTAGTCGTGTGTCCTTGCATAAGCCACTTAATCTATTTCGTGCCTCGGTTTTCC", //
			"GCCGCCGTGCCCAGCCAAGTTTCTGAGTTTTGTTTATTTTAGAAAGGAGTCTTCCTGTGCTGCCCAGGCTGGTCTCAAATACCTGGGCTCAAGTGATCCTCCAACCTCAGCCTCCCAAATAGTTGGGACTACAGGTGAGCACCACCATGCCTAGCCAATCTCCATGTTTGATTGTTAAGTGGAAAAAGCAAGGTGCACAACAAACATACTATGGTGCTGTTGCTAAAGGAGAAGCACAAGGCCTACACATGTTTCTATTTACAGAGGTAAGCTCTGGAAGGATATACAAAAATCTGGTCACAGAGGTTGTCTCTTGGGAGGAAAAAAAGGGGACTGAGGAGAAATTAAAAAAAGACTTTTCATTGTACATTCTTTTGACCCTATTGTATTTTATATCATTTGCACATTTGCTTATTTAAAAAGTTTTCAGAAATTTAAAAGCAGCTCTTTTTCCAAAGTGCTCCTCACTTCATGTCATGGGTCCTGTATCCTAAGTTCCCCTATTTCAATCACAGTGCAAATTTACCCTCCACAGGCATGCCTTTCCTGCATCTAGACTCACTAGAAATAGCTGATGCATCTCTCCTCCCTTGCCTGCCCTCCACAGGAAAAGAAGCAATGAAAAAAAAGAAAATGAGATCCTCTGGTCTCTGTCACCTTTTTATCAACTTGTACTACCTGCCATTATCTGACACCCACATCTTTTCTTGAATCTGCCTCCTCCTATGGGACAGCGACATTTTCTTAAGTTGCAGATGATGGAGAAAATTTTTTAAAAGAAAAAAAGGGGGCAGGGGGAGGGAAGCCCAGAGTTTGGCCAAATGCACGTTTTTCAAGTTCCTGCTTGAGCTTAGATTCCCTTTCCGCTTTAAGGACATGCCCTCTGGAAAGAAGGGTGTTTTGTTTCCAGCCCACACAGACCCAAACTTTTTTCATCACGTGCCTGGTCCGAGCCAGTACGCAGCTCTGTAATGAGATCACCATGGCCAGCCTCCCACCCGAGCAAAAACAAGGTTAGCCTGCAGCAGAGCCGCAGCAGCAACAGCCACCAAAGCGGGGGCTGAAAGGGAAGAGAACTTAGATTTGACCTGTGCAAGGACCTCAGCAGCTTGGACACAGCCCACTGCAGAGGTAAGTGGGAACTACGGGGGAGGGGTGGTGTTTGCAAAGGTCAGGACATGTCCAGGTTCACTTAAAGTAA", //
			"TAACATTGGCTCAAAAACAAACAAACAAACAAACAAACAAAAAAACAAAAAAAAGAGCAAGTTCCTCCAATTGGTCTGGCCAATCCAGCCTGAGAGAGAGAGAGAGAGTGTGTGCACGTGAGAGAATGCGAGTCAGTGAGTGAGCATGCTCATTTCCAAAGCTCAGGACTAGCCCTTCAAAAAAACATCCCTGCCCATGAAAAATGACTTCTGCTTGGTCTTTTGATCAGAAGTGGGATAGGATAATGCTGAGAAGAAACAGTTCAGTGCAGATTCAGATTGCTTAGAAGGGGTGAATTAAAACAATGTTTCCTCAGATCTTGAAAGTCTGGATTAAAATGTAGTGTCCTGGCCAGGTGTAGTGGCACATGCTTGTAATCCCAGCATTTTGGGATGCTGAGACAGGCAGATCACCTGAGGTCAGGAGTTCAAGACCAGCCTTGCCAACATGGCGAAACCCCATCTCTACTAAAAATCCAAAATTTAGCCGGGCTTGGTGGCAGGCACCTGCAATCCCAGCTACTCCGGAGGCTAAGGCAGGAGAATTGCTTGAACTTGGGAGGCAAGAATCGCTTGAACTTGGAAGGTGGAGATTGCAGTGAGCCGAGACTGCACCACTGCACTCCAGCCTGGGCAACACAGTAAGACTGTCTCAATTAAAAAAAAAAGTAAAAAAGTAGTGTCCTATTAGATCAGCTCCTCCAACATCCCCTCTGACACCCTGTACATGGCCCCATTAGCTAATTGCAGATGAGCCCCAGGCACACACAGGGTAGGGACCACCACCTCCAGTTCTTTCCTATGAAAGAATATGAGCAGACCCCGTCTGAGTTTAGAAACGGGATATGAGTGGGTCATCTGTAATTCCTGTCTGTCGTACAACGGAAATTGGTGATACACCTGGCAGCTGGTGTTTTCCCTCTCTGAATCTGGGGCATTGTTTGGTCATCGAGCTCACAATGACTTCTGGTTTAGAGGAGAACCTTTTGGAGGAGAATATACAAGGACCTCTCAGTGATTTCCAGTGGTCTCTCCTTGACCCTCAGAATGGCTAGTCGTCTTGTCCAGCTGGGAGCCTGGGCTAATCTTGCTGGAAGGCAGAGAGGGGTTATTAAAAGTCAGAAAGTGGCCGGGCACAGTGGCTTGTGCCTGTAATCCCAGCACTTTGGGAGGATGGGGCAGGTGAATTGCTTGAGGTTTA", //
			"CCAGCTAACTTCGCTACTAAAAATACAAACATTAGGTGGGCATGGTGGCAGGTGCCTGTAATCCCAGCTACTCAGGAAGCTGGGGCAGGAGAATCACTTGAACCCTGGAGGCGGAGGTTGCAGTGAGCCAAGATGGTGCTTCACTCCAGCCTGGGTGACAGAGCAACACTGCCTCAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGTCGGCCGGGCGCGGTGGCTCACTCCTGTAATCCCAGCACTTAGGGAGGCTGAGGCGGGCAGATCACAAGGTCAGGAGATGGAGACCATCCTGGCTAACACAGTGAAACCCCGTCTCTATTAAAAATACAAAAAAATTAGCCGGGTGTGGTGGCGGGCTCCTGTAGTCCCAGCTACTCCAGAGTCTGAGGCAGGAGAATGGCATGAACCCGGGAGGCGGAGCTGGCAGTGAGCCAAGATCGTGCCACTGCACTCCAGCCTGGGCGACAGAGCGAGACTCTGTCTCAAAAAACAAAAAAACAAAAAAAGTCAGAAAACGTGTACAATTCACTCTTTCCTTCTAAAATAAGAGGAGGGTAGGGATGGAAGAAATGGGCCTGGCCTATGCACACAGTGAAGCCTGTATGACTGGGAAAGGATTTCCTCATCATGGTTTTGGCAGATGCTGCTATGATGACTATCTCAACCTTGCCTGCCCATTGGAATTAACTGGGGAGCCCAGGTCCCACCCCCAGGGACTCTTTATGTAATTGGATTGGAGGGGACCCTGGGTATTGGGACTTTTTTTAAGCTCCCAGGTGATTTCAATGTGTAGATAATGGGTATCTCCATTTGCCAAGAGGACTGGTGATGAAATCCTATGGCTGATAGAGGCTCGCCTGTCCTCTGACTGACAGAATTGCCTGCTAGTTTCTCTGGAGTCAGGCTAATTCCCTTCCCTCCCCCAACTTGAGCTCTAATTTTCATAAACACACCCATGTACAGCCTGAGAGCTCCTGATAAGGAAGAGGCTCCCACAGAAGCTGGAAAAGGCCAGCTGGAAAACGCAGATGTGCCTTCCTGGTTGGTTGAGATGCTGATCCTACAGCACTCCCGCTGTGCCTCAGCAGTGAGCTGGGTGTAAAGGCAGGAGGCTTGCTGGGGTCTGACACTTCCCTGCCCTCCTCCAGGAGGGACACATCTGGGGCTCTATGAGGAGGACAGCTTTCATCCTGGG", //
			"CTTACTTAATACTCTAAGCTTCCCATTCTATTCCATTATATGGAAATACTCTAATCTGTTTAGTCCCTTCCCTACTAATGGGTATTTAGGCTGCTTAAATTTTTTTTTTAATGCTGTGACGGACATTTATGCATACAAACAGACGTGTGTCCTTTTCTTCTTTCTCCCTTTCTGTCTTGGCTCTCTGACTCTCTCACTTTATTACTTCAGTTACTGCCAAATTGCCCTCTAGAAGGTTTGTACCAATCCTACTCCCCTACAGTGCTGGCAAGTATTTTCCAAGTCCCAGCCGCCACAGTTACTTCGGTTGATGGAGCATGAAGGGTGCTGGTGGAGGGGCACAGACATGGAATTAAGCAAAGCCGAGTCCTAGCTGGGCATGGTGGCAGGTGCCGGTAGCCCCAACTATTCAGGAGGCTGAAGCAAGAGGATCACTTCGGCTTGGCAGGTCGAGGCTACAGTGAGCCATGATCACACAACTGCACTTCCAGCCTGGATGACAGAGAGAGGCTTTGTCTCAAAAAAACAAACAAACAAACAACAAAAAAAAAACAACGATGAGTTTTCCACTTATACTGACTAGATGACACCTGAAGTTCTGCTGCTCAGTTAGTAGGATGACAGGGATTAAGTTTCCTGACTGACTCCTAGCCCAGGGCTCACTGTGACCCATACCATACAGAGTACTTCCTTTGCTTCTCCTTCCTTGGCAATTCATAAAGGGTTCAGTGGGCGCAGACTCCCAAATGTCAACCTGAATTCATTTCTATTTTAGAGGGTATTATATATCTTAACATACTTTCATATGTATTTATATGAAAACCACCAACCACCTTGAAGGTTGTCCCTATTTTATGGCTAAGAAAATGGAGACACTCCTTAAATGGCCTGTCCAAGTTCCTACCCAAGACTACAGGTAACCCCTTGGTCCCAAGCCCTAAGACCTCCCTGGCAAAGAAATGTCTAGGATGAAGCCTCAGTCACCACAAAAAAGTATGTTCTGTTAGTCTATGTGCTGTGAGAACCCAAAGTCGCCCTGACTGTTTCTGTTTCCCAATCTCAGGTGCCATCCAAGTGCCTTGTCTCTTCTTCTGGAGCTTCAATGGGCTTCTATTGGTGGTTGACACAACAGGAAAACCTAACTTCATCTCTCGCTACCGAATTCAGGTCGGCAAGAATGAACCTGTAAGTGTTGTTGCAA", //
			"TGCTACCGGTGATAGTGGGCCCATTAGTAATGGGCTCCCACTTGTCCTCCATCACCATGTGGTTTTCCTTGGCCCTCATCATCACCACCATCTCCCACTGTGGCTACCACCTTCCCTTCCTGCCTTCGCCTGAATTCCACGACTACCACCATCTCAAGTAAGGACCTTCTCCCCACAATGGGTCCCTAGGCAACAACCATCACCCTCCCTCCTTAAGCTTCTGATAGCAGGCACTGGTATCTCAGTTTACATGTGAGTGCTAGTTGGGGAAGAGGTCTGATGGCTGGGAAAATAACCACACTGGGATGACCTTACTCTTTTCCTTCTGAAATTGGGTCCTCCTCCAACTGGGGGTTTTAAACCTTTATTTGGACTCTGGGCCCTTTGAATAAGCTCTGGACATTCTCTTTAGAAAAGTACATATATGCATAAAACTCCTGCAGGCAATCACAAGGAAGTCATGTATCTCTGAAAGCCCCAGACTTTTTCAAGTGTAGTCTACAAACCTCCTATACCAGAATCACCTGAGATGCTTGTTTAAAATATAGGCTCTTGAATTCCACTCCAAATATGCTGAACTAAGGTATCTGGGGGAGCAGGGCCTGGAAACCTGCATTTTAGTTAAGTGTTCCAGGGGATTCTGATGCTGGTGATCTCATACCTGAAGAAGCCTAGTTCTGCTAGAGATTCTCAGACTGTGGGCATCAATCACACAGCAAGCTTTTCAAAGGAGGACTCCAGATACGGCAAATTGCAGGTTTGGAGTGGCCCCCGGGAGATGCATTGGTAGCAACTCCCCAGCTTATTCTGATGCAGATGGTCAGGATGGTGGTGACAGGTGTGGGGTAGGAGGGCTGAGAAATGTGTGAGGCTGGCACAAATGAAATGGGATCTGCACGGAAGCCTCCAGCATGGGCACTGCAGTGCCGTCCTCTCTCTTCTAGGTTCAACCAGTGCTATGGGGTGCTGGGTGTGCTGGACCACCTCCATGGGACTGACACCATGTTCAAGCAGACCAAGGCCTACGAGAGACATGTCCTCCTGCTGGGCTTCACCCCGCTCTCTGAGAGCATCCCAGACTCCCCAAAGAGGATGGAGTGAGAGACAGCCTAAGTGTCATCCTGGCTGTCCCTCAGCCATGGGATGCAGACACGGCTTCCTGATTGCACCTAACAATTTGCCTCCTTCGGCCACACGCCCT", //
			"AAATAATGAATTAGCCAAGACTGTATATTAGGTGTGAGATGACGATACAGGAAAGTATAGTTTGTGTATTTTGGTGGTGATTGACTGTCATTGGTGTGATTAGGGAAGGTTTTATGAAGACATGGGATTTGACTGGACTCCCAGAAAGACTAAAGGACTGGGATTGGAGAAGGCATTTCAGGCAGGGAGAGTTAAATTAGTCTAAATTGAAGCTCAGCAGCTGGAGATTACAGGATGTGAAAGATCTTTGGCTGGATATGGGGGTTCAGTGGGAGACTTACTAGTTGGAGCCACTGCTTAAGTTAAATTCCAATATGACAGATTTTAGGAAGATATTTTAGGATTGTGCTAAGATATTTTAGTATTCATTGTGATAGCTTTACTGCAGTAGTCTTTGGCCTCTTGCAGTAAAATAGGGTTTGTTTCAGTTTCACCTATTTGTACTACCTCTCTTTTTCGACTAGTTTTCCAATTTCTCTCTGTATTCTCTTAATTTCTCTCTGTGTTTTTCTCACTTCTGTCAGGTGCTTTTCCCTTAACTTCTCCCCATCTATCAGACACTCAGAGCTACTACAGAATTTGGGAGGAGGATTTGGAACATAAACTGTGACCTACAAGTGAATTTCCCCAGGTGGATGATAAACTCCACTGCAGGCCACAGAGGGGCCTCCTCATTCCAGCAATCTGGCCAGGCTGACAGACTCATAGCCTATGGGAGCAGGGGAGGGTGTAGGGGCCAGTATTTCTCGGGCTAGTGACGAACTGCCTGATTCTTTGCAGCCTAGACAGAGGAGGCAGGAGCCCCAGGGGCGGGCTAATCGCCTGGGCTGGGGATGCCTGGGCAGATGCAGAGGAAGCTGGAAAGGTGGCAGTGCACCTGGGTCGCTGGAGCTGCCGCCGTTCCTAGGAGACCAAGGAGCAGCAAGCCTGCGGGGGAGGGGGAGCAAGTGGGTTGCTGCTTTTAGCAGCTGAAAGGGCTGCAGGGAGCTCTGGGTAAGACATTTTCTGTTGCTGCTGCTTTTGCGGTAGAAGCTGCTGCGAGTAAGTCAGAGGAAGGAGGATTGAGAAGGGAGGAGGCTTCACTTGCAGCCATGCTGAATCACTGTTGCTGATCAGATTTCCCACTGGTCTGCTGGGAGAATCAAGAACAGAACTAGGATCCCCGAGTGCATACACAGTTCAGCAGCTACCACCCTGGCTG", //
			"CTGCTCCCAGCCTAAATAGCCTTTTTTGATGTATGTATTTTTTTTTAAATTGCTATTCATCTTCCTGGAATGTTCTCTCCTCCATCTTTTGGCCTGTCTGACTCCCGTTCATCCTTCATTACTGCTTAAGCATCACTTCCTCAGGAAGGCCTTTTATGACCTCTTTGGCTTAGGTTAAATTCCTTTGCTACTTACTCCCATGGTACTTTATACTTCTCTTTAGTATTACTACTCATCTTATTACTTCCTCAATGTTTGATTTCCCTGGCCAATCATACTCTTTATAAGGGTAAGAACTATACCTTTAAAATTCACCACCATGTCCCTGCTGCCTAGCATGGTCTTCCATATAGCAGGTGATCAATAAAAATTTGTTTAATTGAGCTTAGCAATATATAATGAATATCTTTCCATGTCAATGAAATTGCAGTTACAACCTAATTTTAATGCCTTCATAAGATTCCATTATATATACTCTAATTGAAAATTCATTTCCTTCGTTGGAAATTATAGTTTTATGTAATATTTTGTTATTATAAATAATGATATAATGGGCATCCTTGCATCTTAATCTTTATGTACATTCCTGATTATTTTACTAGAATAGATTCCTAGAATAAGAAGTACTCGATTAAAAGGTATGCACACTTTGAAGACTTTTGAGACATGTGCCGCATTGATCTAAAGAACAGTTGTACCAGCTTATGCTCCCATCAGCAAGATATCTAAGATTTATATATTTTTAAGGCCCATTGGCTTTGCCTAAACCTTGGTAAACAGATACTAAAAATCTCCAGTAACATTGCAAAATTTTGTTCTAATTATCAGGTTCAGCTTCAAAACGTGTAAAATGCATTGTGTACTGAGAGTAAGAGGCTGACTCTCGTTAGATCCATTTCTATCAGTGTGTTTAAAGCCAAAAGGAAAGTAAAATACACATGGCTTTCTTCTGACTTGAGTTGTGTGATTATGGTCTTTTACTCAAGCTTAAATGTTTTTATTTTTTTGTCTTCCGAGTTCATGTTGCCCAAATGTCCTGAGGTGGTTCCAAATACCAATCCCACACTTCTTGACCATTGTCCTGTGGACAATAACTTGGAATAAATGTGAAACTGAAGTCTGAGTGCCCATCAGAGAGTGTCCCAATCCAACCAGTCTATAAGAAGATCTGGGGTGAGCAGTCAACCTATTTGCCCTTTAC", //
			"CCTTTCCTTTCACGTCCAGAGTTGTGGGTGCCATCCACCGAGTTGGGCAGGCACAGCACCAGTGTCTAAGCTGCAGGTGGGCTCCTGGCCATGCCCAGGTGCCACTGTGCACCTGTACTGCTCATGTTTTGGGAAGGATACAAAGCAGGCATTGCTGGTGTGCGTCCAATCAGGACACCATCATCATGTAGCATGTGGATGGGTCCATGCCTTTCTGAGGGTTATCAGGGTGCTGCCATCATGCAGCATGTGGATGATCCATGCTGTTCTGAGGGTTGTCAGGGCGCCACCATCATGTAGCCTCAGGATGAGTTTATGTTGTTCTGAGGGTTATCAGGGTGCCGCCATCATGCAGCTTGTGGATGATCCATGATGTTCTGAGGGTTATCAGGAAGCCGCCATCATGTAGCCTGTGGTTGAGTCTGTGCTGTTCTGAGGGTTTCAGGGCACTGCCATCATGCAGCAGGGCGCACATGGGATGGGGGGACACACTCAGGGGGTTGTGAAGCTGGACCCTGATCAAGGGAGGATGGAGACCCCGCTAGGGCTGGTGTGAGTGTGGATAAGTCCATGCTGTTCTGAGGGTTATCAGGGCGCCGCGGTCATGTTGTGTGTGGATGAGTTCATGACTTTCTGAGGGTTATCAGGGTGCCACCATTATGCAGCATGTGGATGAATCTGTGCTGTTCCGAGGGTTGTTAGGACGCTGCGGTCATGTCATGTGTGGATGAGGCCGTGCTGTTCCGAGGGCTATTAGGACGCTGCGGTCATGTTGTGTGTGGATGAGTCCGTGCTCTTCTGAGGGCTATTAGGACGCTGCGGTCATGTTGTGTGTGGATGAGTCCGTGCTGTTCCGAGGGCTATTAGGACGCTGCGGTCATGCCGTGTGTGGATGAGTCCGTGCTGTTCTGAGGGCTATTAGGACGCTGAGGTCATGCTGTGTGTGGGTGAGTCCGTGCTGTTCTAAGGGTTATTAGGACACTGTGGTCATGTTTTGTGTAGATGAGGCCGTGCTGTTCCGAGGGTTATTAGGATGCTGTGGTCATGCCGTGTGTGGATGAGTCCATGCTGTTCTGAGGGCTATGGAGCAGGGCACCTCTGGTTCTGTGGAATCGCTGCCTTGATACTCCATGGGGAATCCCAGATCAATGAACACAGACAGCCCCCTGCTTCTCAGGCTCTTCCAGGCCGCTGGCCCGGG", //
			"TGGACCAGTCACAGGGCCCTGTTTAACACCCTGGCTCTGTGGTTCCAGGAAGAGACAAGGACACTCCCTCCTTGGAAGAGGGTCGCCCTCACCTGAGATCCCACCACTGGGAACTCCCGGAAGCTGCTTCCTCAAATCCCAGCCTTTCTCAGAAAGAAGTCGCTTGGCCCTGGGCATCAAAACCCCAGAGCAGCACCAGCTGTGTTCAAGGATTCCACCTTAGGGCCACACGGTGTCCCCTGGGCTCAGACATGTGGCAGATGCTCTCAGGCTGGCACTGAGCCAGTCTCTCCAGCAGCCTGGCCAGGCCAAGGCCCAGTGCAGGCAAAGCACAGTGTGTGGCTGGGGGCTTCTGTGGAGTGGGAGGAGCTCTGGGGCTGCCATGGCCCCAGGAAAGCTGATGGCACCCTGCTGTGGACAGTCCCTCTCAGACCCCCACGGGCTGACCTGCCAGGACCCAGCACAGGGGAAGAGCAGGCAGCCGGGCAGTCTCTGCAGGGAGAGAGCTCCTCTGGGAGGCTGGATGGACGTGGGAGGCGGGGCCCAGGTCAGCGCAGAGCTGGAGGATGCCTACAGCCCGGCGGACCTGAACAGCCAGCGCCCTTACCCCTCGCATGGCCAAACTCTGAACGTAAGCCAAGTAAAACCTCAGCAGCTGAGCCAGGTACAAGTGGAATTGTGGTGATGACGTGGGGTGGGGCCACGCACAGGGGTCCCTGTGAGAGGTCACCTGGCCTCAGAGATCCAGATACAGGCATCCAGGCCCGAGAGGGTGAGCTCCTGAAGATGCTGCCTGCTCTGGGAGGACTTGGTGCTGTCCAGGCCCCAGGAGCTGCCGCAGCGGGCAGTGGAAGGAAGGCACGTTCAGCGTTCACTCTCCCAAGAGCTGCAGGTGACAAAAAGCAAGTGCTCCCGGAATATGTCAGCTGCAGGTGGCCCCGAGGCAGCCCACCTGGGTGTGGGCACTTTCTTACTCAGAGCCCAGCACACGTGGTAGCTTAGACTCAAGAGCCATCTCCAGGACCACGTTTTATTTTCAAGCGAGTGCTCAAATGCTGATGTTCTGCTCTGCGCTAGACTTTAAGGGGACCGTCAGGATGTTGGCTTCGGGCCTGACTGGCTGTCTGGTCGGCATGGGCACTGGCAGTGTAAGTCAGTGGTGGGCGCTGGTGATATGGATGCCAGCGGGTGGGCATTGGCA", //
			"CCGGGGGACTCGGGGAAGGGCACCCGTGCAGATGGGGGACGCCCCGGGGGATCTGGCAGGGAGAGGGCATCTGGGTGGGGGAGGCTTCGGGGAGAGGGCACCGGGCAGTGAGGGGAGGGTCGGAGAGGACGCCGGGCCGGGAGTAGGGTAGGGGGCCGGGGGCCGGGGACGTTACCGATGCCCGTAGTGGACGAGGAACATGGCCAGGAGGATGCAGTTGGGCGCGCTGCGGAGACGCGGGGCGGACTCGCTGGCGTACTGGTAGAGCGGCAGGGCCAGCGAGGGCAGCTCCTGCACCACCCAGGCGGCCCGCGCCGGCACTCGGAGCCTGTGGCTGGGCAGCGCGTGGCGGCCGTACACTGAGTTCGTCTGACGATTGCGCGCGAAGACCGCGCAGCCCACGGCGCACTGCAGGTAGGCGAGCGCGGCCAGCAGGCGCTCCTCCGCCACCCCCGTCGCCGTTGCCATCGCCAGGGCTGGGCAGCGTGCTCCATGCCCCAGAGGCCGCGGCGGGCAACATATAGGGCGGCGGCGCGGGGGCAGGAGGGGCGGCTACCGGAGGGGCGGCTACCGGAGTCCCGCACTGCCGGGACTCTGCAGAAAGGGTTCTCAAGTCAGGCTTCTCCGGGGTAGCCGGGGCTGTGGGTGCGGCCCCGGGGGCGGGGCGAGCACTTGCGCGGGGAGGGACCTGCAAGCGCCCCACCCACGCCGGGGCCGCAGTAACCCCGCGCCGCTCTGCGCACGCGCGGCTTCAGGCTGTCCGCGGAGCTCCTTGAGCCCGCGTGGGAAACCTGGGCGCTTCGCGCGGAGACGCCTTTGTGGCTGCTTCCTGGAGCATTTTTTTCCCCTTAGAGCTGTTCGCTGTTTTCCCTGTCACGCCGCTTTTCTGAAAGACGGGTTGTGCGCTTTGGGCTTATTCCTGGACGGTCCCCAAAGGTCCCCGAAAGGACCAGGCCCGGACACGTGGGGCCTTGGTTTCCGGCCGGAAGTCCCGGGAGGTGTAGGGCTAGAGTTCTGGCCGTGGCGGGCCGGTTTCTGCGTGCTGCGTGCGCGGCCGCGTGGGCTATGGGGCGGCGGTCGCGGGGTCGGCGGCTCCAGCAACAGCAGCGGCCGGAGGACGCGGAGGATGGCGCCGAGGGTGGTGGAAAGCGCGGCGAGGCGGTAGGAGCCGGGACCCGGCGGGCGGGCCAGGGGCTCCTCC", //
			"CTAGTGCATATTAGTGAGTAACACGACTGGCGCGCGAGGTGGTCCATTTTATTTTTGGTAGCTAGACGCCAATAGGACGCACCACTTCAGCTCCAAACGGGAACTGGTGGCCCCAAGATGTTTTCTTTTCTTTTTCATTTTTTTGAGACAGGGTCTCGCTCTGTCGCCCAGGCTGGAATGCAGTGGAGAGATCTCAACTCATCGCTACCCCCTCCTTCTGGGTTCGAGTGATCCTCCCACCTCAGCCTCAGTAGCTGGGACCACAGACACGCGCCACCACGCCGGCTAATTTTTATATTTTTTGCAGAGAAAGGGTCTCGCTATGTTGCCTAGGCTGGTCTCGAACTCCTGGCCTCAAGTGATCCACCCGCCTCGGCCTCCCAAAGTGCTGGAATTACTGGCGTGAGCCACCGCGCCCGGTCTGCCCCCAAGACTTTCATTGAAAAGATGGAATGGTCGCTTCTGTGCGGATCCTCCAAACCATCGTTGGGAGAGGCCTCCTTAAAGTCTAGGAGAAAAGTTGTTTGGGACCGATATGAAAGTAAGGCAAAGCCGGATGGAAGAGAGTAGCCGAAACAAGCAAGAAAAGGAAAAGCGATCGTGATGGGTTTAGGAAAGGTGATCTGGCGTGCTAAATGGCATTTCCGGAGAAATCTGAAAACTATTGCTTTACTGAAGTATAGCAAAGAGCTGTCCGGGCTCTGGAAACTCCTTAACGGATCAAAACGGATGCAATGGGTATCCTTCTCCAAAGACACACCTCAAACGGGCCGGGAGCCGGGCAGTTAGCCACCCGCTGCCTCCCCTTCCGCAGTCCGGCTCGCAGCCGCTCAGCTACCTCTAAAGCCCGGAGCAGGAAAGTCCGCGCAGGCCTGGATGGCCCGGGTGGTAATCTAGCAGGCCTGCACACTGCGCATGCGCAGGGGGTGGACTGCCAGGTCGGCTCAGGGAGCCGTGACGAGTCCGGAAGCGCCTGCGCGCGCTCCTCCGTACGAGAACTAGTTTTGTTCCGTGCCCTCTGGACTGGAACCTTTTGGAGAGAACCCCCGGCAGGACCAACCCCGCACCCGCCAGCACCGCGGCAATGTCCAGCAATAGTTTTCCTTACAATGAGCAGTCCGGAGGAGGGGAGGCGACGGAGCTGGGTCAGGAGGCGACCTCAACCATTTCCCCCTCGGGGGCCTTCGGCCTCTTTAGCAGC", //
			"ATCCCAGTACTTTGGGAAGCCGAGGCGGGTGGATCACCTGAGGTCAGGAGTTCGAGACCAGCCTGGCCAACATGGAGAAACACCGTCTCTACTAAAAATACAAAAATTAGCTGGGCGTGGTGGCGGGCATCCGTAATCCCAGCTACTGGAGAGGCTGAGGCAGGAGAATCACGCCACTGCACTCCAGCCTGGGCGACTGAGCGAGACTGTCTCCAAAAACAAAAAACAAAAAAAAAACACCTCGCTTTTTTGGTGATAGTCACTGTGTAAAAGTTGGAAAAAAAATTAAAATTACAACCCTAGACACCGAGGGTTGTACGGTGGGAGGTAATTTTAATATTTTTTGTATTTTATTTTCCAACGAGGTTACAACCCTAAGCCACACAGGGTTGCGGCGTGAGCGTCGGGCTGCGGGGGCGCCTCCACCCTCCGTAGGGTGGGCCTGGGACGCTCCCGGGGCTGCAGGGGGACCGGCTTCGCGCTCGGCGGCCGCGCGCCTCCGCCGCCCTCTCCGGGAAGCGTGGGTGTGGTGAGCCCCTCCGTCTCCCCGTATTCCACCCCGGGCTCCGGTTCAAAGGCTAACTGTTCGCTGCTACCTAAAAGCACGGCAGCGCAGGCCTGGCAGGGCGGCCCCTCAGCCCCGCTCCGCGTTGTGTCCAAGGACTGGTCAGCGGAGGCCCTGTCGTCTCAATTCCAGTCTCCCCAGGGCCGGAACCAGTGCCTTAGGAAGATGCGGGCACGGCCCTGGTCAGACGACGAGTCGGCTACCAGGAATCCCGCACCCAGGACTCTTTCCGAGGTCGGAGGGGCGCAGATGACCAGCGAGAGGCGCGGCAGGCTAGAGGGATCCCAGTGCGCAGGGGCGGGGCCGCGGCGGCCGGAGCGAAGAGTATAAGCGCATGCGAGACGGCGTAGGGGACTAGAGGGTTCGGAGCGCGTGCGTGGGGCGGGGCCGCGGCGGGACAAATGCAGAGCGCACGCGCTTGGCTGCCTGGCGACTGCACAAGCAGGGATGGCGTCGCTTCAGCGTTCTCGGGTGCTACGCTGCTGCAGCTGCCGCCTCTTCCAGGCGCACCAGGTCTGGCGGGTGCCAGCCCCCTCCTCCTCCTCCGGCGCGGGGACAGCGGACGGGGAGTGGCGGGGCTGTGGGTGTTGTCTCGAATGGCTCCGGGATGCACCGGGCCCGAAGGAGTGCAGGGAC", //
			"CTGCCTGTGCCATAGCAGGACCCGTACTCTCAAATTCCATCTTGTACCATCTGCCTGTGCCATAGCAGGACCCATGGTCTCAAATTCCGTCTTGTACCATCTGCCTGTGCCATAGCAGGACCCATGCTCTCAAATTCCGTCTCGTACCGTCTTCCCCTTAGACCCCTCAGCTCCAGCGATGCTGGCCTCTTTGCTGTTCCTCCAAAATTTCAGGCTCAGGGTCTTCACACTCATTCTCATGTATTGCTTCTGCTGGAATGTTCTTCCCCTAGACAGCCATGTGGGTTGCTTCTTCATATCTTCCTGTTTGTGCTCAAACGTTACCTTCTTAGAGATGCATTTCTTGACCATTTTGAACACCTTATGTAAAACGGTATGCTCTTATGTACGACCTCTTTCCTGGCCTTGTCTTTCTCAGGAGCACTTACTAGCATCTGATAGGTTATATGGCTCACTTGTTTCTTTCCTGTCTCTTCTCACTTGAATGTGAGTTCCATGATGACAGGGAGTCTTGTCCATTCCTGTATTGTCTACCCCAAGATGAGGCCTGGCACACAGTTGGTATTCAATAAATATTTGTTGGATGAATTTGTGACTGGTAAGTGGGAAAAAATGTATTTTATTTTGTATTCCTTTGATTATCAGTGAAGGTAAGTATCATTTCATATATTTTTTGGCAGTATGTTATACAATATGGCAATATGTGTTACTTCTCTTGTGACCTTTTTACTCATGTGCACTAACCATTTTGTGTAGGGCTTTGGGTTGAGTTTGGCTTCAGTAGGTAGAGTTCAGGGTGATTGTGGGACAGCCACATAGATGTAAACTATTAGTTCCGGAGACTGTTGGTTTGTAAATCATTCTAGAGACAGCCGTCAGTCATTAGGATTCCTGCATGCTGCTTTAGGGGTCTTTGGGCCTTGCAGAATACAGCTTTGTCCTCCCTGAGGTGCATCTCAAATGGATCTGTTTCTCAGCATTCTGCCAGCACTTTTTCAATGTAATATATTTTTTGGTTTCTCAGGCTTATGGTGAAGGCTCTGGTGCTGATTGTAGACGCCATGTCCAAAAGTTAAATCTACTACAGGGACAAGTTTCAGAGCTGCCACTCAGGTACAGTTGTTTGAGAAGAGCGTGGTTGTCTTGCCCTGTTTTCGTCTTGACTGGATAATAGAAGTACACAGTGGAAATCCTGAGCTGG", //
			"TGCCTGCTTGCCTGCCTCCCTCCCTCCCTCCCTCCCTTCCTTCCTTCCTTCCTTCTTTCCTCCTTTCGACAGAGTCTCGCTGTGTCCCCCAGCCGGAGTGCAGTGGCGTGATCTCAGCTCACTGCAACTTCTGCCTCCTGAGTTCAAGCAATTCTCCTGCCTCAGCCTCCCAAGAAGCTGGGATTACAGGCACGCACCACCACGCCCGGCTAATTTTTGTATTTTTAGTAGAGACGGGGTTTCACCATGTTGCCTAGGCTAGTCTTGAACTCCTGAGCTCAGGTGACCCGCCCGCCTCAGCCTCCCACGGTGCTAGGGTTACAGGTGTGAGCCACCGCGCCTGGCCATGTTTCCACCCCTCTGTTCTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTGAGACGGAGTCTCGCTCTGCCCCCCAGGCTGGAGTGTAATGGCACCATCTCGGCTCACTGCAGGCCCCGCCTCCCGGGTTCACACCATTCTCCTGCCTCAGCCTCCTGAGTAGCTGGGACTACAGGTGCCTGCCATCACGCCCGGCTAATTTTTTGTTTTTTTTTTGTTGTTGTTTTTTTTTTAGTAGAGACGGGGTTTCACCGTATTGGCCAGGGTGGTCTCGATCTCCTGACCTTGTGATCCGCCAGCCTCGGCCTCCCAAGGTGCTGGGATTACAGGTGTGAGCCACCGCGCCCGGCTACTTCTGTTCTTATAAATGAAATTTGCTATTTTTTATAAGACTGGGACTGGCACCTCTGCCTTCTCTGTGTGGAATCACTGGGCCAGGGAGCATAGACTTTCATGAGTCTCTTGGTGGGCCGTGCCCTGCCGCCTTCCCGAGGCCTGTGCCAGTGAGCACTGTCCTGCCAGCATTGCATAGTATTGGCGTCCATCACCTTCCAGCCGGTGCCTTTCCTAGAGTGGAGCTGGACATGACTCCTGAGATCCTCAGGCCCAAGAGTTGGGCTAGGGGTTTTCGATAAAACTGCCTGGTTTCCAGTACTGAGAATCAACTTTTTTCTCAATCTGGCAGGAAAAATCGCAGCCCTCAGAGAGTCGCTGGCTGAAGTATCTAGAAAAGGACTCCCAAGAACTGGAGCTGGAAGGAACAGGAGTGTGTTTCAGCAAACAGCCTTCATCCAAAATGGAGGAGCCAGGCCCCCGCTTCAGTCAAGACCTGCCTAGAAAAAGGTACCAAGTGC", //
			"TTTTGAGTAGGAGTTTCACTCTTGTTCCCCTGGCTGGAGTGTAATGGCACGATCTTGGCTCACTGCAACCTCCGCCTCCCAGGTTCAAGCGATTCTCCTGCCTCAGCCTCCTGAGTAGCTGGGATTACAGGCACCCGCCACCACGCCTGGCTAATTTTTGTATTTTTAGTAGAGACAGGGCTTCCCCATGTTGGCCAGGCTGGTCTTGAACTCCTGACCTCAAGTGATCCACCCGCCTCGGCCTCCCAAAGTACTGGGATTACAGGCATGAGCCACTGCACCCAGCCTTTTGTTTTTGAGATGGGGTGTCACTCTGTCACCTAGGCTGGAGTGCAATGGTACAATCACACCTCATAGCAGCCTCCATCTCCCGGGCTTAAGTGATCCTCCCGAGCAGCTGGGATCACAGGCACTCACCACCACAGTTGGCTAATTTTTTGTAGAAGAGGGGGTTTCACCATGTTGCCCAAACTGGTGTCAAACTCCTGGGCTCAAGCAATCTGCCTGCCTCAGCCTCCCAAAGTGCTGGGATTCAGGCATGAGCCACTGCTCCCAGCCTACAACCCAACTTTCTTTTTTTTGAGACAGAGTTCGCTCTGTTGCCTAGGCTGGAGTGGAATGGCGTGATTTCGGCTCACTGCAACCTCTGCCTCCCAGGTTCAAGTGATTCTCCTGCCTCAGCCTCCCGAGTAGCTGAGATTACAGGCGCGTGCCACCACGCCAGGTTAATTTTTGTATTTTTAGTAGTGATAGGGTTTCGCCATGTTGGCCAGGTTGATCTCGAACTCCTGGCCTCAGGTGATCTACCTGCCTTGGCCTCCCAAAGTGCTGGGATTACAGGCGTGAGCCACTGCGCCCGGCCGACACCCAACTTTCAGAGCATCTTTGGTTCCTGGGGTGAGAGAAGCCAGACCCCAGCACCAGTCCTATCCCACAGGGAGCTCGGTGCTGCCACCTCTGCAGCTGCAGAACCCGGCAGGCTCTGGAGAGCTGCCCCCCGGTGGGAGGTAGCAGAATGGAGCCAGCGCACCCCTTTCCCGATGTCTCGAGGCCCTTCTGCTCAAAATTCTTTTGTAGGCACCGGTAACCTGATTCTAAGGTCCCCAGGAGATGGATCACCTGATGACATGAGAATGTCTTGTTTTTGGAAAGCCTGGTCATGCAATTTAGCAGAAGGTTTGCGGTGAGGTGGAAGGCATTT", //
			"GGTGGCCTAGGAGTTTACTTTGAGATTGAAATTTATTTTCTATGGTCAACTGTATAATTACGATGATACCACCCCTTGCTTTCCATCTGACACTTGGTTTGAGAAACTGTAGGTAGGAAAGGGAAAGAAGAATGTCCCTCCTTGGGCCTCTTCTTTTGGGCCTGTGCTTCCTAGAGTCAAATGTCAAGCTGGTATTTCCTTGGTGCTAAGAGCAACCACCACTCTGAGGCAGAGACGGGATGGGACCTAGCCAAAGTCCCAGTAAACAAACTGTATCTTGTAAGCGCAATTTAATTAGGGTCAAACTATTCTGTATGGACTTGCTTTTTCGACTTCTGAGGATTTAGTAGTCATGATTAAGTGCTGTCACGCTACTGCGATCTACAAACAGTAGCTCCACAACATCCGCCCGCATCGTGGCAAAGAATTATCAAGAAATCCCCAAGGGGGCGGTGAGGAGCCGCCGGTCGGCCAGGTCGCCCGGTCCCTCAGTCTGGTGAAAAGCTCCGTTGCCCACCCTGAGGGCTTCGCGGAAGGAAGTTGGCCGTATCGCTGCACCTTAGAGATGCGACGGCCGAGGCTCCCAGCTCAAGCTTCTGGTAGTAAGGCCTGTGTGCCTGGCATTCGGGGAGGCCACTTTTCTATCCCCAGCTTCCGGGCCACCACTTGGGTGCGTCGTGGGGATGTCGGCAGCTACGTTTGCGGCACAGCTCCGGCTAAACTGAAAAACGAATCCTCAGGTTTGACAAAACTAAACTAAAAAGACACTTTTCTCCTCTCACTCTGACGTCCCAAAAGTTTCCTTTTTAGGGGTCGGGGCCTACGGTGGGCGAGGAGAAACTCGAGGTATCTACCCACCGCACCAAACAACTCATGCGTCGCTTCCTTTCGACTTGGAGCTGGCCAGTATCCCGGCGTCCTCTCGGCCGACGCTGCGGTCTGAGGCCACTCTGGCCGGCAGCCGACGTCTCTATGGCTCCGACCGAGGGGCGGAGCCGCTGCTCGAGCTGCTGCTGGATTGAACTCAGAGAAGCCGGGGTGGGTATGGCTGAGCCGCTGCGTGGGACTGAGGGGAGGGGGCCGGGTGGACGCCGCGGAACTGCGTCCTGAGGTGGGAGACCGGAGGTGGGGGACCACTGAGTCTGATGTCTGGAGGGTCCGGTTGCGGGTCGGAGGGCGTGCTGGAACAGCCGCCACGCGG", //
			"GAGCCCAGCCTTGGCTGCCCAGGGAGCCCCCTTTCTCCTCAGACACCAAGGCAAGCTGCTATGATGGCCGCGGGCTCAGCTACCGCGGCCTGGCCAGGACCACGCTCTCGGGTGCGCCCTGTCAGCCGTGGGCCTCGGAGGCCACCTACCGGAACGTGACTGCCGAGCAAGCGCGGAACTGGGGACTGGGCGGCCACGCCTTCTGCCGGTGCGCCGCGTGGGGCTGGGTGACCCCTCCGCCCCAGGGCTCCGGGCTCCCGGCGCTCTAACGGCGCCCCGTCGTGTGGCTACAGGAACCCGGACAACGACATCCGCCCGTGGTGCTTCGTGCTGAACCGCGACCGGCTGAGCTGGGAGTACTGCGACCTGGCACAGTGCCAGACCCCAACCCAGGCGGCGCCTCCGACCCCGGTGTCCCCTAGGCTTCATGTCCCACTCATGCCCGCGCAGCCGGCACCGCCGAAGCCTCAGCCCACGACCCGGACCCCGCCTCAGTCCCAGACCCCGGGAGGTTAGGAAGTGGGGGGGGGAAGGAGGAGCCGAGAGGGCGCCGGGCGAGCTAGATTCCGGCCAGCCGGCCGCGGGCTCTCCGTCCTCAGCCCCTGCTCCTCCACAGCCTTGCCGGCGAAGCGGGAGCAGCCGCCTTCCCTGACCAGGAACGGCCCACTGAGCTGCGGGCAGCGGCTCCGCAAGAGTCTGTCTTCGATGACCCGCGTCGTTGGCGGGCTGGTGGCGCTACGCGGGGCGCACCCCTACATCGCCGCGCTGTACTGGGGCCACAGTTTCTGCGCCGGCAGCCTCATCGCCCCCTGCTGGGTGCTGACGGCCGCTCACTGCCTGCAGGACCGGCGAGTACCCGCCCGCCCAGAGCCGCCCCAGGGGCCGCGGCTCCTCCGTCTCCCAGCGCAGCTTCCACGCTGCACCCGAACCCGTGCCCTACCTTCTCCCGCCCCACCCTTCTTTCCACGCCCCTCCGGAGCTCCCGGGGAGGAAGCTGGAACACGGGATTGGGGTTCGGGAGCAGGGGGCTTCCCCAGAACGCTTGTGGCCAGGTCTGAGAGCGCTGCCTCTCCCCTACCCCCCCCGCAGGCCCGCACCCGAGGATCTGACGGTGGTGCTCGGCCAGGAACGCCGTAACCACAGCTGTGAGCCGTGCCAGACGTTGGCCGTGCGCTCCTACCGCTTGCACGAGGCCTTCTCG", //
			"GTTCATGCATCCCCAAAGACCCAAAGAGAGGGTGTGATACAAATAATGGACAAAATTACAAAAAACAAAGCAAGGTAATATATAACCTAGAATTCTTCTATAAGCTTTGTTGTCTCTTTGCTCTCCAGGTTAACTGGTTTGATAAAAACTCCTTAAGTGAAGTCTAGATTAATTTTTTCATTGTCCTATTTTAGCAGATTGTTTTTACTGTAGTAAATATTCACCTGGGATTGGGTTTAAATTGTAAAGACAAATGCTCCCTTCTTTTCTTGAGTTTCAATACAATCTACTCGTTCCATTAAAATGTGGTGGAAACTCTCTTTTTAATCAAAAGTAAGTTGATTAATAAGCTATTTCTTCCAGAACACTGATGTTCTGAAGAGTTAAGGCCCTCTTTCATTAAAAGTAGAAAGTACAAGAGTGAAAAAGTTCACTGTGTTTTTAAAACAAAAAGTGAAAGAGAAATAACCCAAATCAAGGAAGTCCCCACTTCCTCCTGCACAGCCTTCTTAAGCATGCCTGTTCTTATTCTGCTTTTTCCTGCTACTTCCCATTTGCTATTTCATATCTAGCCAAACAGGGAGACTGAATTTCCTTGAATTTTTATGAATCTGCTTGTAAGTTTCCTTTAATTTCATGATCAAAGAGCAGCTCTGTAGTGACTATCTCTTAGTATTTGGCGATATTTTAGCCAAAGGTTCTCTTATAGCTTCCCATAAATGATGGGCTTCAAGGAAGGTTGTAGATGCAGAATCAGTGTACAAAACAAACATTGCTTTCATCTGAGGAACTCACGAGGCAAAAAAAAAAGAAAAAGTGTTTTAAAAAAAGCTGACCTAGGACCTTAGAGTGAGAAAAGGAAGTATGAAAATTAATTCTCATTGGAGCAGTTTCCAATCTAGTTGAGGAGCCAAACCTAATACACCCAATACAATTAGAGAACCATAAAGGGCAAACATATTAAAAGAGCTTAATTTTGTAGTGTGGCCTGCAGGCTTCCAGAAAAAAACAGGAGATCAGTGAGCTAGCAGCAGAGAAGCCTTCCTGGAAAAGGTGAGATTCGACATCCCTCTGTTCTATCTCAGGGGTCTTAGCCAGGCACATTAATGTTGGGCACGTAGAGTAGATGGCTACTTGTTTCTTGTATAAAATGTTAGACATTCGGGTTATAAAAAAGAAAATGTATCGGAGTTTATAACTA", //
			"CAGGTAACATCTTTTTTGAAAAAGGATTTTGTATGGATATAGGATTAATTAGTGTTTGGTCAGCATCATGTATAGAAATTTGGTGACCTTACACTAAATAAGCACTGTAATACTTACAACAAGATCTGGAAATTTGCAACCACCAAAAAAGGAATCATGCCTTTCCTCTGCCATGGAAAACAAAGATGACCTTCTACCCTTGAACTTACTTTAGTAGAAGGCTAAATATTCCAATTAAAGGGGCCAAATCTGTATATCAGAATGTGTTCAGAGAGGTACATGGATGATTGGAAAGTCTGACTACTTGGGATCAAATACTATGCCCTTAACATCTGATGCTAACAAATATTACCCCAAAATTAATACATGAACCTAGACTGAGCTGTACTTTTTTTTTAGGGAAATGAATGGCATTCTATGATAAGCTTATGAGTGTTTCCTTCATTTAAATACTGGTAATATGAATTTAAATATAATTGGAGTTGCTTATTAAAATTTTCATATACTTTTGCTCTTTTTAATAATGATTATAGTTATGTTTTTACACAAGTGCTATATACCCAAGATAGAAAACTTGAAAAACTCCAAAAAGTACAAAAAAAGAATTATTTATAATAATATATCCACTCAAGTCAAATGCTATTCACATTTTGGTGTGAGTCTTTCATAGATAAGAGAGAGATGGAGACACACACACACACACACACACATATGTATCCAAAAATATAGAAGTTATATAAAACATTAATAATGTATAGCAAAATTTAATATCTAGGTACTTCTAAAAAATTTTGTGTCAATTTTTCAGTCTTCAAAGCTACATCTATACCTGTTCCTGTATTATTGAAAAATAATAACAAGATAAACCTGGCCAGCTATACAAATGCTCTGAGGGTTGAGATAACATGCAAATGTAGCCTCACATATCAATGTGTGAAAGCTGGACAGACCACACAGCTCAGAAAAGGAAGTACACTCTGCATAGCTAACTTGCACATTCACTATCCAAGCTGCACCATCTTCGGGGTCATTGTGTGCCAGGCATATCAACTCTTTTCAATAAAAATGGTAAGTAATAGATTTTATAACTCTCCTTGGTCTCCTGGCAAATGTGCTACTCTTCTCTGCATAGAACAGTCAGAGTTGAGGGTGCTTCACAGTCAGGTCCTCTGAGCCTTTGGGATTTTGCTTGGCCACTTTT", //
			"ATTTAGTCCCAATGGTAGCCTCTGTTTTGGGGGGTTTCTTTTCTTTTCTTGTCTTTTCTTTTCTTTTTTTCTCTTTTCTTTTTTGAGTTTCTTTCTTTCTTTCCTTCTCTTTATTTTTCTTTCTTTCTTTCTTCTTTTTTTTTTTTCCTTCAGACAGGATCTTGCTCTGTTACCCAGGCTGGAGTGCAATGGTGGTGCAATCATAGCTCACTGAAGCCTTGAACTCCTGTGCTCAAGAAATCCTCCCACCTCAGCTTCCCAAATGTCTGGGGCTACAGGCATGCACCATCACACTTGGCTAATTTTTTTTTTATTTTTGTAGAGACAGGGTCTCACCATGTTGTCAAGACTGGTCTTGAACTCCTGGTCTCAAGCAATCCTCCTGCCTCAGCCTTCCAAAGTACCGGGATTACAGGCATGACCCACTACACTCAGCCCCACTGGTGGTCTTTTCACATGGATTATCATCTAAATACTCCATTTCTTCCAGCCTCAAGAATAGTCTAACAGAGAGCCGACAGAGTGAGTCTAGTGGGCTTTCCACACATGAGTGACCACTCTTCAGTTGATATGGTCAAGCACACCACTTTACCTCCTGACCTCAGGTGCAATTAGCTTCCAAATATTCCACATGCTCCAGTGGACCTTTCTAATTGTAAGCATCCCCCTTTGGCCCCCATTGCAGGTGACACTCCTGGGTGTGTGGAAAGATTATGGGTCCAAAGAACTTACAGCTTCTTCCTCTTGAATAAACATGGTGGCAGGCAGGAGAATCTTGTTTTGGTTTTACCCTTTTAAATATCACATGCTTGGTGGGAATGTTAGCAGAAGTATCCAGGCTGAACAGACACTTTTAAGTTCCCCTTTCTCCTCCTTATTACTCAATATCCTGAGGGCATAATAGAACTTTTCTTGTCTAAGTTTGCCTGGTCAGGGGAGGGAGGTGTGGCTCACTCAACCACTGACCGGGACAAAACCCTGCCAGGCATCAGAAAGCTCCGCAGTTCTTGAGTTCCACATGCAGAGCAGATGCGACAGCTAGAAGTGAGTAGGGCCCAGACCCTGGCCCAGGAAGATCCACTAAAGGAGGCCATCCTTCCGCCTTCTTCTGCAGGAGTCAGGTAACAGCTATGCCTAGGTTTCTTCTTTCAAATAAATGTGTAATTTTTTATAGCAGGAAAAAAGCACCACTTCCAAATTC", //
			"GATGACAAACAGCTGCAGATCAGCCAAGGAGGCCTTTCAAAGCAACCAGCAACTCCAAATTTTCCTCGGAGCCTTTGGCTGTCTTGTATACATTGCTGATTCTTCTGAAAAATTCACAAGGTCTTTTCCCCCCATTTAACTTGATTTGCAGGGAAGTAATTAATAGAAATGGGCCTCTGACTCTGCCTTCTATTAGCTGTGTGACTTTGGGAGAGCCACTTAACCTCTCTGTGTCTCAGTTTTCTCATCTAAACATGGGGATAATAATAGAACTCATCTCATGGAGTTGTTGAGGGGATAAAACTATATACACTTTTTGAGTACTAATGTAAAATAATAATTAATATTCACCATTTTAAATATTTTTTCATTAAATACAAATAATTCTGCGTGGGAAACAGATGGGCTTATCACCGGAAGGGTGAAGTTGTGGCTGTGATTTTCTGCCCATGCTATGGGAATGCCACCGGAGGTTTTAGGTAGAGTAAAACGTCCCCATGCAATGTGAGATACACACACTTCAAATAGTCCGATACCTTAAGAAATTAATCATAAGTGATAGGTGAGGGGGAGGAGGATAAGGGTGCTCTTTCCAATAAAACCACATGTTTTATGATGTACCATTAAAGGCGATCCTGAATATCAAAGGCGTTATGGTGCACTTAAGGCACCCCGTGGAAAAGTTGACCATCCGTTTCAAAAAGGAAAGGCTAAAAAGGTTTCCATTTCCCTTGAGAAATTATTGTAAAGGTTGAAATCCACACTGCCTGACTGTCATGTCCATTATTTGTTTACTACCATTGGGCTTCAAATTCCCCCCAGCTTCTGCTGGAGTCCAGAGGCTTGGAAGCTCGCAGCAGCTGATTTCTGGTGCAATGCTGCACCTTGCCAAGTTACGGCTGCTTCATAGAGCAATCACACTGACATCTAGTGGTCTAAAGAGTAAGTGCAGGCGGTGGCTTTGAAGCTTTTACCAGCCTCAATGTGCTTTAAAAACCACATTTTCCACCATTTACACAATACGTTAACAAGAGAGAGAGATCAGGGATGTAGAAATGATATTTCATTTGTCTGAGGTGTTACTAGAGGCTGCTCTACAGGAACATTTCTAAAAACAAGAAAGTGAAGAGAGTGAACCGTGGCTCGCTGGCGGGTTTGGGCTGTGGTTCCAGGGGTTGCGGTTAAGACTGTGACTCCAGAA", //
			"ATGGCCACATTGGTTAACTTCAGTGATTTTTTTGAAGCCTCCAATTTTGAAATATTTCTAAAAGAACTAAAAATTTAAACTCCTTGCTTTTGAGACAAAGAGCAAACACACCATTAACTTTCCCAGGTAAAATGGGATTACATTCACAGAGAAAAATGAATGCCTATTATCTTTGCTAAGTAATACCTAAATTCTAGACAAGCAGGGGAGACTCAGTAAAAGCACCTCTGAGAAAAAGGGATGACTGTGGAAAGAAAGGTCTGGAAATAAGACTGACAGCCTGTCCCTATTACCTCTTCCTTATCTTCCCCCTTGTGCCTCCAGACCATAACCTCTAAACAGAACAATTCTGAAAGTGAGTACAATCGCCTGCTAGTATGGATACAGGTGAATCTTTAGTGCTAAATAATTACAAATAAGGAGAAGAAGTCATTTTATTACTTACTTATAAAAATGAGCAAGGTTTTTCTGCCACATCTCCCTGTTAACAAACAGAACTTCAATGTGGATAGAAGCAAAAAAAAATACTCTTGCAAACCACAGAGATGTATGTTAAACATGGCCATGCTGTATGAAGCACACAGCCTCAATTTACAGAAGAACTTACTTTCATTTTTTAAAGGTAGTTTCTACTTAAGAGTTTCAATTATAAAATATGGAAGAAATCTTTTTTTAGTCACTTCCATATTTTTTCAAGTGAAACTTTAAAAATAAATAATACCAAGTTAGAGTAAATCCAACAAGATAGCTAGAGAAACAGTGAGAATAAAGAAAGGATTTGTTCCTTAAGTAAAAATAAGCAAGTCAGTCCGAGATAAGCAAAAGAAAAAGTATATGCACAACCATAGCCTTCCTCTTTCAAGAAACTCTCGCAACTTATTTTGCCAGAGAGGAAGATAAATGTTACAATATCTTGCTAACCAATCAGCAACGCTGCAGGCCTCAAACCAGGAAGCTCTGTAAGGTATAGTATTTTAAACTGTGCAAGTAAGACTTTTGTCTCTCAGCTATTTTTTGTTCCCTATGTTTGTAGGATGGAAAGGCAGATGTAAAGTCCCTCATGGCGAAATATAACACGGGGGGCAACCCGACAGAGGATGTCTCAGTCAATAGCCGACCCTTCAGAGTCACAGGGCCAAACTCATCTTCAGGAATACAAGCAAGAAAGAACTTATTCAACAACCAAGGAAATGCCAGCCCT", //
			"TTACCATGTTGGCCACGCTGGTCTCTAACTCCTGACCTCAGGTGATCTGCCCACCTCGGCCTCCCAAATTGCTGGGATTTTAGGTGTGAGCCACTGAGCGTGGCCTATTTTATTTCATTTTTAGGGTGGTTTCCCAGGCTGGCCTCAATCGATCTTCCCATTTCAGCCTCCCGAGTAGTTGGGTCTACAAGTGTGCACCACCTCGCTCAGCTGCAGGATACTTTTAATAATGAAGGGAGGCACCATTAGTAATTATGCCAGGGCAATAGGCACAAACCAGGCGAATCCATTCTCCTCTGTGTGCTCCCAGATAGCCATATGACCTCATTGCCATTCCTAAAACACCCCAGGCACTCTCCTGCCTCAGGACCTTTGCACTTCTTGCCCCTCTGCTAGGAGTTTCTTACCCAAGACCTACATGAGTTGTGTTCCCTCATCTTGTTCAGCCCTTTGCCCAAATGTCACTTTCTAGGTGAGGCCCTCTCTGATCACCCAATCTAAAATTGCGACCCTCCCCCTAGCTGAAACTCTCCATTACCCTTGCCTGGATTTTCTCTATACCGCTTTTTACTAACAGATACTTGATATATTTGTTTCGTTTATTGTCTGTCTCCCCCAGCCCCTCCAGATTGTAAATTCCATGAGAACAGAGATTGTAGTAAGTTTTCTACCCTAATATATTCCCAGCATGGAGAACAGTTTCTAGCATATATTAGATGCTCAATAAATATTTTCTGAACAAATGATTTTTTTTAAACATGACCTGCACTCCCTTCCAAATCACTGGCAGGCCATACGCCTCCTGTATAGAAATTCTGAGCAGGTGGCATTTGAACTGGGCCTTACAAGCCACTCTTCACCCCAAACAGGGAACATGGTAGCTGGATTATGTTCTGTTGGGCCACATCTGCATCCCAGGATTCTGCTGCAGCTCATTGGCTGGGCAGCCTCCATTGTGAGGTAGCTCTGGTTTTTTCCTTGCCAAAGACAGACAAGCACTGAGCCTGGCCCCACGTCCAGGCACCAGGACCTCCAATCCAATGGTGTCTTCTTCCTCCCAGTCACACTGTTCACTTTGCCCTACAGCAGCAAGGGCTAAACTAGGAGAGGCCAAAAGCCCAGGCAAGTCCTGGTTGGGCAAGCAATTGCGAGTGAAACTGAGGACCATCCTGACTGAGATGGGTCTTTCATAAGTCCCCCA", //
			"TCCCAAACTGTTCCCATGAAGACACTTTCAGCAGCCGTGGCTTTATGCTTGCTGAAGAAAGTGGTCTAGCTGCAATTAAATATGAGGGCTTCAAATTAGAATGGGATTCAGGTTAACCAACTATTTCATTCACACTAAAATATAACTAGGCTGTCGGCCGGGCGTGGTGGCTCACATCTGTAATCCCAGCACTTTGGGAGGCCGAGGCAGGCGGATCATGAGGTCAGGAGATGGAGACCATCCTAGCTAACACGGTGAAACCCCGTCTCTACTAAAAATGCAAAAAGTTAGCCAGGCGTGGTGGTGGGTGTCTGTAGTCCCAGCTACTCGGGAGGCTGAGACAGGAGAATGGTGTGAACCCGGGAGGCTGAGGTTGCAGTGAGCCAAGATCGCGCCACTGCACTCCAGCCTGGGCAACAGGGCAAGACTCCATCTCAAAAAACAAACAAACAAACAAACTATATATATATAAAACTAGGCAGTCAATCAAGTCTGCATGGTATCAAGTTCTTCCCATATTTGATCGGGGGAACTAATGGTCATCTGACATGTGAGGGTACTAGGAGGGTAAAAGATGAAGAAAAGAGATTGAGATTATAAAGCAGAGAAACCAAAGACTTTACAGTTTTGCCCGAGTTTGACCTAGTGATCCTCATTGGTGAAGCAAAAACAATCTATTCAGAAGTCTTTCTGAATTCACTGCTGGTGTTTCTTTTCTATTTGGCATCATATATAAAGCATTTCTTTTAAAGCACATGCCATTTTTTTTCAAAGGGACCTTTTGTTCTCAGATGATCCATTATTTGTGGTATCACTGCAGAATTTGGGACAAATTTCAACTTATATCATAGAACTCCTTAGTGTACTTTATTATTCAATCCCCTCACTCCTTCCCTTATGCCTCCAAACACACTCCTCTTCCTCTCACTGCCAGAGTGATGTCAGAAGATTCTGTAGCAGGTTCTCGGAAGCCTATTGTACAGTAACAAAGAATTCCAGCAGTGAGCTGTGAGTTCTGTCCTTGCTGGCTTTCCCAAGTGGAAGGCTACCCTCATCTTTCACTAATGGCAGACTAGGACAAGCATTACCCAGGCCTGCAAGCTTGTTAAGGACTAGGAACCCAAGCAAGAACTCACCATGCCAATCAAACCTGTTGGGTGGATATGTGGGCAGGTGTTGAAGAACTTTTCTGGAAGAAGTA", //
			"AGGCCGAGGCAGGTGGATCACGAGGTCAGAAAATCCAGACCACCCTAGTCAACACGGTAAAACCCCGTCTCTACTAATACACACACACAAAAAATTTGTCGGGCGTGGTGGCACGCGCCTGTAGTCCCAGCTACTTGGGAGGCTGAGGCAGAATAGCTTGAACCTGGGAGGCAGAGGTTGCGGTCAGCCGAGATTGCGCCACTGCACTCCAGCCTTGGCTACAGAACAAGATTCGGTCGAAATATATACATGTATATTGTTATTAGAGGGCTATGTGGAAGTAGCTATTAAACACGAGGGCTTTGAAGCTAGAATTCCAAACCCTTCTGCAAAGGTGTGCTCTCAGTTAACATCCTTTATCTGTAAGAGGGAAATCAAAATTATTTGATAATGTCTTGAGGAGTAAATGAAGCAATGCATATAAACTGTTTAGCCCAGACCGGTCTGAAAATACATATTGAATAATTTATTCTTATTTTGCCCATTATATAATATGCCAAAGTAAGTGATTACGGTGTGTTTTCCCCGACTATGTTGCCTAGGCTGGCCTCGAACTACTTGGACGAAGCGATCCTCAGCCTCCTGAGTAACTGGGACCACAGGCCGGCGCCACCCTAGCTTTAATTAAGGCCGTTTTGTTGTTGTTATTCCCCTGCTCCACACAAGGGGCGCTGCCGCTCCAAGGCTGCCAGGGCGGGAGTCAGGATGGCGGAAGTCTGTCTAGCCACGACCACCGCCATCATGGAATACAGGCCCACCTCACCCCGCGGGTGTAGCCGCTTCGTACTGTCGCTTAGGCCGACCCGCTCGCAGCGACAACCAGCCCTCTACCTCTTTTCGCTCTCCCTTAAGTAATAAACCGTCTTTCCTTATGACGAGTCTTAAACTCTTTGGGAGGAATAATGCCGGCGTCTTCCGGAACCCGACCTCGCCCCGTGACCTCAGAGGTATACTTCCGGGACACGGAAGTGACCCCCGTCGCTCCGCCCCCTCCCACTCTCTCTTTCCGGTGTGGAGTCTGGAGACGACGTGCAGGTAGGAGGCCCGGGCGCGACAATCGGGGGGCATCCTGCGGCGAGGGGACCCTGTGGGGCTTGGGACGAGAGACGGGGGTCTTTCCGTGGGAACCGAGCTAGGTGCCGGGCAAGAGACGCGCGGCTGGCCCACCTGGATCCTGGCCAACTCGGGATTGAGTTCGTTC", //
			"GCAGCTCATGAGCCATCTCAACAGATGCATTCGGAAGAAGTCATAGATGACTCTGATTGGAAACTCACAATCGGCATCGATGTTTAGTACTAGTAACTGTCAAAGGCCCACGGTTGTCAACCGCTGCCCACTTCGACACCTAACGGCATTCCGGAACATTCCACAAGATGGCGCCCTTGCTTTCCAATCTTCGGGCTACGGGCGGAGGACGCCTTGCCAGGACGCCGACCTCGCTCTGCAAGGTTCAGGGGCCGACCTCAGACAGAGGCTCGCTCTAGGGGCATGGGCGGGGGCCAGACGGGCCTCGAGTGGGATCCTGGCACGGTTGGCCGATCGGCCCGGGATCCCGGCTGCGGGCCCGTGTCTGCCCTAGCGCGATGGGCTCGAATGCCCGGCTCCACGAATCCCGGCCTTTCACCCCCACGGCCTCTCGGCACTGCCCCAGCTAGACACCAGCGAGCTCGTCACAGCGCGGGCAGGCACTGCGGGGCAGCTGCAGACGAGCCCTGTCATACGGCCGGAGGAGGCCTCGCCTGCCCGTGCGGCGGGCTCCGGGCACCCTGATACCTTGACACAGGAGGCCCCGGGCGGCCGGGAAGGGCTGTGCGGGGGAGGGACTGCGTCTACACCCGCGCCCGACTCGCTCGGGTCACGGCACTGCCCTGCCACCAGGCGGCTGGGGCGGGGGTGAGGCCAGGCGTCGGCCCTCGCGATTCTGTTTTGTACGCTGAGGCTCGGAGCCTTCGAAGAACGGGACAGCGGGGCCCACGGACGGCTCTGAAGGCCTTCCCGCGGCCGCAAGAGATCTCGGCGGCCGGCGTTTGAGGCGCGCGGCTCCGCTGGGAAGCGAGACCCGGGTGGCGGGACAAGACTTGCTTCCCGGCCACGCGCGCTCGGCCGGCCGTGGGGCGGGGCATAGGCCGTGACGTGATGTCGCGTATCGAGTCTCCGCCCCCTTCCCGCCTCCCCGTATATAAGACTTCGCCGAGCGCTCTCACTCGCACAAGTGGACCGGGGTGTTGGGTGCTAGTCGGCACCAGAGGCAAGGGTGCGAGGACCACGGCCGGCTCGGACGTGTGACCGCGCCTAGGGGGTGGCAGCGGGCAGTGCGGGGCGGCAAGGCGACCATGGAGCTTTTGCGGACTATCACCTACCAGCCAGCCGCCAGCACCAAAATGTGCGAGCAGGCGCTGGGCAAGGG", //
			"GGCGGCCCCTCACCACCATCACCACCATTCGCACTCGGGGCCGGAGATCTCGCGGATTATCGTCGACCCCACGACTGGGAAGCGCTACTGCCGGGGCAAAGTGCTGGGAAAGGTGACAAGCGGAGGGCGCCGGGCTGCCGGGCGGGCGGGAGGTCTGTCCGGGCGACCCCAAGCCGGGCAAGCCCGCGGGGTCCCTCGCCCGCCGCTTCCGGGTTTTTCGGGGGTCCGATCCCAGCGCGAGGTGGGCTCCTCACCTGCCCTCTGGCTTTTGTGCGCCGGCAGCGCTGGGGATCGGCTTTGCATTCTTAGCCTCAGAACTGTGTGTCTCCCGCAGCCCAGAGAAATGCATCTTGAGTCTGGAAGCGAGGGCTTTCTACATCAGACCTCTCTCCCTCTATCTGCTCCCAGGGAATTTTCCTGACAAGTCTCATCAGAGATTATTTTTCATGTCTCCCCGTTCCAACTCCCTCCATAAAGCTCCCCTTTATAAAAGAAGAAGAGAAACTGTTTGAACTTGCTGTTTCCAGCAATGCATTTCTCTTCATGAGGGAGCTTTAACGAAGTCGAATGCCTCCCTCTGTCCACCCTCCTCCATTCCTCAATTTTTTTTTTTTTGCTTTTCTTTCTTCCCATTTATTTATTTATTTAAAATATCTGCCGTCCATTTTTGTGCCATCCTCAATTCCGTCTCGGCTTTGTTTCCTTTCAGGGTGGCTTTGCAAAATGTTACGAGATGACAGATTTGACAAATAACAAAGTCTACGCCGCAAAAATTATTCCTCACAGCAGAGTAGCTAAACCTCATCAAAGGGAAAAGGTGCGTATGACTCTTGAGTAAAGTATTTTCTTTGTGTGCAAGGATGGCCCTTCCCTGTTAGGAAAATGTCTTCTGCATGTGTAATCACTGGCTTTTCCGAGGTGACTGGAAGCTAATAAGCCTTACCTTGCTTTTTCATTTAGATTGACAAAGAAATAGAGCTTCACAGAATTCTTCATCATAAGCATGTAGTGCAGTTTTACCACTACTTCGAGGACAAAGAAAACATTTACATTCTCTTGGAATACTGCAGTAGAAGGGTAAGTGTCAACTCCTATTTGAGAACATTTGCTTACCCCGAATTAACATGGTATTCAAAGAGTATTTTATCTGGGATTTTAGCTGTGGCAGCACTTACACTGCAAAGTTAACTTTCATTGCTCA", //
			"CACTGCCCTAGGGCCCCGCTGGGGTGGAAAACAGACCCAGACAGCGCCCCCAGCCCTCCCTCCCCAGGGAGGAGTGAAGGGCATAACAGACAGCGGAGGACACCGGGACAGGAAGGAGGGGCCAGCCGCTCTCAAACTAAGTCTAAACCACGGCGGTGAGGAAGTGTGGTCCCAAATCCAGCCCCCTCCCGGCCGAGCTCACAGAGCTCAGGGCTGGAACTCGGAACCCTGGCCCGACGCAGGCGGCAGCACCGAAGGGAACTTGAGCCCCGGCCCCCTCAGGGGACCCACGACCGGGGCGCAGCTTTGCTGGCAGACGCCGCAATACTGCGCAGCCGCGGCCCCGCAAGCAGAAACAGGGCTGGGTCTGAGGCCTACGTGGAGCTGGGGCCCGACGGCTCACGGAAGGCAGCGGGGGGCACTGGGCGCCCCGGATCCGAGCCTCCCGGGCGGCAGGCCCTCCCAGCTGTGCGGCGCCCGGATCCCCACAAAGGCACCCCAAGCGCGGACGGCGGGACGGGCGACAGCACCAGGGTGGTGTCCCAGGCTTCCCCACCGCCGCGGACCAACTCGGCCCCGGCCCGGGCGGGGCTTCCGGAGAGATTTATCCCCCTTCCCTCTAACCTTCCTGGGGCTCCGGCTCAGGCTGGACAACGGCTCGGAGACCCAGTCCCCCTCAAGTGTTGACCCCCCCACCATCTAGAACCAGCCAACCCACCAAACCACCTCTCCCAAGCCGGGCCTCTTCCCGGCGTCCGCCGCTGACCTGATCAGAGCCGCTCTGCCGGCCCGAGGGAGCTGAGAAGGGGCTTGGGAATGGGGTCTTGAGGAGGGGCTGGCCGGAATGTGCAGCACTAGCCCAGCTGGAGCGGGCGGCGCGGGCGCGCCTGTCCCCCAGCCCACCCAACTCCCGGCCCGCGCGGCCCCCGGCACACCATAGAGACGTGGTCCTCCGCCGGCCTAGAGCGTCTCTCCAGAGCGTGTGCAGGAGCGAGACGTCATTTTGCGTCGCCTCGGGCAGGGTAGCCAGCGGCTGCATCTGAGGCGGACGCCGCAGTGTAGGGCACACCCTGAGGGTAGCGAGAGACGTGACAGGACCGAGCAGCTGGGGGCTGCAGCCTGCTCTGTCCCCACCGATCCCAGAGAGAGGCTGTGAGTCTGGATCTGGGCGTTCGCACCCTGAGAGAGGGGGTCAAAGCTC", //
			"GGGAAAGGCGCCTACAACAGCGAAGGGCCGTGGGCCTTCGGTGGACCGAGGCTCCTCAGGCCAGCGTCCTGTGAGGGCTGGGAAACCGACTGGGACAGAGAAAGGACACCTAGCTAGGGCGACAGAGAGCGGGCAGACGCCCGGGCACTGCGGACATTGCACTGGGGCGGGGGGCGGGGGGAACGCTGGGAGGCGAGGGCCAGGCTTCCTCTGGCTGAGTTGCCAGAATCGCCCCGGGGGGTACTGGAGCCGGAAGAGGCCTGGGCCCAGTCACTGTCTACACCCAAGAAATGGGGGGAGGGGCGGCCAGCACTCACGAGAGATTCAGCCTCAGGAAAAAGAACCAGATTCACTCACCAATCCTCACCCCACCCTTTGATATGAATGATGGACATTTTCCTAATTTTTGGTAAACGCAGCTTCAAGACCCCTCCCGCCTGGCGTCTCTGGTCTAGAGCAAGAGAAGGCCCCCAGTAGGGGTCCTCCCGCCCCCCCTTTTGACTGCCTGCGGGGGGCAGAGATCCCCAGATCTAGGTGTGCCCTCCACCACCCTGCTCTCAACCCGGGATCCGGAGGCCGCAGCGTCCCCCACCCCCCAGCGACACTGACGGTTCCTGCCCAGGCCCGCAGTGGCGTGAAGGTGGCCTTTACCTCCTCTCGCAGGCTGGGAGGGGGTGCAGCCCGCGCAGCTGGCCCAGCTGCAGCAGTCGCCGCCGCCCTCCATTCATTAAAAAAAAAAAAAAAAAGGCAACAACAGCCTCCACTGGAACCTCCACCCCTCTTTGTAAAAGAACGACGGCTCCTGGTGGAGCTTGAGCCAGTCCCAGCAATCACGGGTTAAGATCTTTTCCCGCCCCCCATTCATAAAAAAAGGAGCGGAGCGCATGCGCGCTCAGGGTCTGTTGTGCCCACCCATTCATAGAATCGCGAACGGCACGCGTGCGCACTGGCGCTCCCTTCCCTCCCATTCATAAAAAAGCCATTTTCCCAGGCAGTGGTTGCAACATCGCCGCGGAGGTAGCGAGCTGAGCTGACAGCGCGGAGCTGGCGCTGTGGAGCGCAGGGAGCCTTGCCGGTTCCTCCGACCGGCGTCTGCGAGTACAGCGGCGGCTAACCTGCCCCGGCTTCAGGATTTACACAGACGTGGGGCGATGCTTGTGACCCTGCAGCTCCTCAAAGTAAGTCCATGGCTGGTTTGGAG", //
			"TAAGATCTTTTCCCGCCCCCCATTCATAAAAAAAGGAGCGGAGCGCATGCGCGCTCAGGGTCTGTTGTGCCCACCCATTCATAGAATCGCGAACGGCACGCGTGCGCACTGGCGCTCCCTTCCCTCCCATTCATAAAAAAGCCATTTTCCCAGGCAGTGGTTGCAACATCGCCGCGGAGGTAGCGAGCTGAGCTGACAGCGCGGAGCTGGCGCTGTGGAGCGCAGGGAGCCTTGCCGGTTCCTCCGACCGGCGTCTGCGAGTACAGCGGCGGCTAACCTGCCCCGGCTTCAGGATTTACACAGACGTGGGGCGATGCTTGTGACCCTGCAGCTCCTCAAAGTAAGTCCATGGCTGGTTTGGAGATCTGAAACTGCCCCTCTCTGAGCCGCGACGCGACTCCCCCCTTGGGCTGGGGCAAATGGTGCGCTATTTCTAGGCGCGCCTCAGTGTTCCTTCCCTCAGCCTGCGCCTCCTTGAGTCGCTGCGGTGCGCCGCAGCTCCCTGACGCCCTCGGCCCGGCCGGCACGGCGCCGGGGGTGTTCACAGCCTGGCACCGCCGTTTCCGACCCCTGGGCATTGCCAAGCCCGGCTTCCCCGCCCTCTGAGACTCGGGAAGCCCCGTGCGTGTCCCGGGAGTTTGCCTCTGGCCTGGCCTCTTTCCTTCTCCTGCCACCATCCGCCTCGAACGCTGGAACGGGCGTCTCTGGGCTTGGTCAGGAGCCACCGTTTGGGTCGGGGGGCGCCCTCGCTCTGGGCGGCTCGCCCACCTCCCAGCTCGGTGCCCACGGTGCCAGCTTTGCCGTGTATCTCCCAGCCCGCCGTAGCGGCCTATGCTCGACGTAGATTTTTTCTTTCTCTTCGGAGCCGTGACGGACTGAATTCCCTCCCCTTCTGCCTCTCCCTCGATGGTCAGCGAGGGGCTGGCTCAGGACTGGGCATTCCTACTCCCTCTCCTTGGCGAGGCGGTTCCTCGCCGTCTGACCTTTGCTCCGCTGCCGCATTTTATGCTATGCAACTGCCTTGCAGTTGACTTTTTCTTGCCTGGCCTCTGTAAGGCCTGAGGCTCCTCGGCCACCCTGTACCTCCCTATCCTCGGGCACCGAGCTGTGCCAGGCCCCGGAGCCCCCAGGCCGCGCTAGGGCTGCGGTGGGGGTTGAAGGTGCGTTTACATAACACCAGGCGTGTGGGAGCTGGAGGAAG", //
			"CCCATAAAAAGAACGTTATTAGACCTATTCTCACAGGTGGCAGTGGAGTTTAACCCTGAAGGAGTTTAATCCTGAAAGGGAAATGGCAAACGGAGTAAAACACCATGCCTCAGAATCATCCTATCCAAGGGCAAAGGAAGCTGGTGTTTATTATTAGATTCAACAAGCATCTACTATATGGCTATTTGACTTATCCAAAAATCTGGTTGTGCCTAGAATATAGAACTTTGTTTGAATTTAGTATATCATATTTTTATTACAAATACAAGATAATAGATACTGGTTATTAACAAGCCAAAAATACAGAGATTGATACAGAACAAGTTAGAAATCTCCTACTCTAATCACAGTGGCCTGAGGGAACCAATATTAGCAGTTTAGTACGTATCTTTCTACATCTTTTTCAGTGTTCGTCTGAATATATAAAAATAATGTATACAGTGAGTGGTTTCCGGTTTTATTAAAACACGAACAAATGAAATATATAACTTGTTTATTTCATTTAACAATAGATTAGTGACATGTCACCAGATAAATACACAGATTTATTTATGTCAGATTTTTAAATCTAGAGTACGAATTTACTACCATTTACTTAGCTGCTCCCTGAATGAGGTACATTCGGGTGATTTTTCAGGTTTTACTACATGTAATGCTACAATAAACACCCTTGTGACAGCAGACTGTTTTAATTTTATCACCACAGACACTCGAGCCTTGAAATGTCACCTTTCAAATGACGCCTTTCCCCATCCTCCCTGTCAATTGGAGAACGGAGCTGAAAGGGGAGTGAAGAAGACTGCCAACATGGATAATTACTCCACACAGCACATGGCCAGCAGCCTCCTGCTACAATATTTACATCCTATTCCATTGAGATGGCACTCTCTGCTCACATGACCCAAATGCAGTTCTCACTCCGCCAAAAGCACAGCCGAATGCGAACTGCATTGGCAGCTTTGCACATGCTCTGTCTGGCCCTGTGAATCCTCACTCACCCATTCAGATTTCTGTTGGTGTAAAGCGACATTCCAGCTGCTGAAGCTCCGTGATCTGCTGTGTTTTTCCAGCCCAGATCCAAGGCAGGTGGCCCATACAATTTTACTATCTTTTTGGAGTTTGAGTAATTGTGTTTATTGATTTGGGGGTGGAGGGTGTATTATGCCCATGTACAGCTGATGAATGAGGCCATAGAGTAACT", //
			"TGCTTTATGGATATGTAACCAGTGCAGCCACAGAGGGTCTGGTACTCAGATGGGACCCTGTGCTTAAGGCATAATGTTCTGTAGTCCCCATCTGGAAATTCTCACTTTATCTTTTAATTGTTTTTGTAAACGAAGTCTGATACAAGACTGTAGCCTCTGCTCAGACAGTTCTGCTTCCCAGGCTCCCTGGGACAAATTCTCAGCCCCCTGCTCCTTGGCCCTACCCCGAAACAAGCTTTTTAAGTGCATCACTGCGCCACCAATCAGTACTCATCATAGTCACTTCTTTAACATTAAGATGAACTCTGTCAGTTTCTTGGTGGTCAGCCTCTACAGCAAATGGGGACACAAATGATCAGACCTAATGAGCAAGTTGTTGTCTAGGTGGCCCACCGATCACTCTAGCTCTGACGTCAGTCCATTGCACCTGTATTTAATTTCAATCCATCCTTCGTTCACTAGTTATGGAAGACAATTTAGCTCCTGTGGGAGACAAAAATCTCTGCAGTGACTTGAGACAAGTGGACACTTGTTGGAGAGCTTAGATAGAGTGTATTTTTATTCTTTTTTTTCCATTTCCTTTTCTCTTTTCATCTTTTTTCTTTCTTTCTTTCTTTCCTTTTTTTCCTTTGTATTTTTATCTTAACATGCCTAGACAGAGAAGAACACTGGATTGTACATAATGGGCTTTCTTTGACTCTCTTTTCCAGAGTGGAAATAGGATTCTGTGCTCCATCATTCTGAGCCATCACCAGGGGCCGTCCATAATCAACTATGATGATGAGAGTGGGAAGACATGTGTACATATCGCAGCGGCAGCGGGCTTCAGCGATATTATTCATGAGCTGGCAAGAGTCCCTGAGTGTAACCTGCAGGCTCTGGATGTGGATGACAGGTATCCATGGTAACCAGAAAGTGCTGGGAAACTAGTCATTTCCCCCTAACTGAATCTTAAAATAAAGGAGATGTTTAAGGAGGTGTCCTATACAGCTATATCCCTATTGCCAGTAATACACATGGGCTTGTGACAATAAAAATTGCTCTATTTTAGTGAGAGGAGAAAAAATTATTGTTATGGAGAGGAAGACTGCGACTGTACAGTGTTGTCAGATTTTCCACTTTTTGCTAAATGTTCTCTGTGTTCTCAGAAGACGTTCTGTATTTCAGCTTCCTGCTATGTGCCCTCTTTGTTCCTAAAGGT", //
			"TTTTAATGGAGACAGTGTTTTGCCTTGTTGGCCAGGCTGCTCTTGAACTCCTGACCTCAGATGATCCATCTGCCTCAACCTCCCAAAGTGCCAGAATTACAGGCGTGAGCCACTGTGCCTAGCCATGAAGGACTTTTTAAACATAACATCATGAAAGAAAGAAAAAATGAATAAGGAATTAAATGTGATTTTTTTTTAAGTTTTATAGAAAAATCACTTAAAAATTGAAAGTCAGCCAGGCATGGTGGCTCACGCCTATAATCCCAGCACTCTGGGAGGCCAAGGCGGGTGGATCATGAGTTCAGGAGTTCAGAACCAGCCTGGCCAACATAGTGAAACCCCGTCTCTACTAAAAATACAAAAATTAGCCGGGTGTGGTAACATGTGCCTGTCATCTCAGCTACTCAGGAGGCTGAGGTGGGAGAATCGCTTGAACCTGGGAGGCGAAGGTTGCAGTGAGCCAAGACCACACCATTGCACTCCAGCCTGGATGACAGAGTGAGACTCTGTCTCAAAAAATAAAATAAAAATAAAAATAAATTAAAAGTCAAGTAACAAATTACAGAAAATGTTTTCATAAACATAACCTATAAAAGACCAATGCCTTTAATATAAAGAGCACATACAAATTAGTAAGAAAAACACTTATACTTTAATAGAAAAATGACAAAAGGCATTAGTCATTTCAATGAAGAAACTCAAATGGCAAACAAACTTATATTTCAAAAGTTCAAACCTTACTATAAATTAAATAAAAAGGAAAATGACAGTGAAGCATATTTTGCCTACCAAAATGGACAAAGATTTTTAAAGTTAGTGACAATACTCAGTATTACAGAAGGAGGAGAAACATAGCTAGTAGGAGGACAAATTGACTCAATCTTCCGGAAGGCAGTTTGGCACTATGTATAAAGAACCTCAAAACTGTTCATACCCTTTGACCCACTACTTCCTCTTCTAGGAAATTGTTAGAAAGGCCAGAGAGTGTAGATGGCAGGGAATACTTAGAGCTAGAGGAGAGGCCCCTACACATGTCCACCATCATCCTTCGGGCTCATTTAACACTTACTATTTCTATACTGTAAATGGCAACAAGATTTTCCCTACCTCAACCTGACTTTGAGCAGGAGCTGTTGCTGGCTAAGCAGCCTGAACAATTCACATGCTCAGACGCTTGTGCTTTTTTCACATGGCCCCTCCA", //
			"CCTCAAAAGAATTTCATGGGGTAATGGATGATATGTAAGTTTTACAAAAAGCACATGGAATTAACTCCAAGTAACCTGCACAAATCCTAACCGTTGGAGTCAGGACATGAAGCTAGCTGTGGGATTCTGAAGCCTACCCCTTTCTCTAGTATTGAAGGCCCTAATTTCTTGACCAACAAGGCTGGTCTTTAGCAGTTTCTTCCATCCACAAAAAACTTCAGCAGCCCGTCAATATTTACCAGGCTCACCCTTAACACCTGAAAATGATGTATCTGGCCCACATTCGTTGACGCCCCACCACTCCTCACACACACTAGTCTTATGGAATGCTTCTAACAAGATTGTACAAAAATGAGTAAACTCATTAAGTTTATAAGAATTCTGGAAAGACCCCCAAACAGGAAAGGGCTATATTCCGGTTTCTTTCAACAACGTTTCCTAGTTTCTACTTGGGATTTTTCTCTAAAACCTCTCTAATATGGGTTTATTTCTTTGAAATCATGTTGTTGAAAGAATTTAAAATAAAAATATTTCAGATATATGATGATGACATGGAAATTCATCCCTATGCTCCCACTTACGGAAAGTGGGATCAGCTTGCATAGGCAACAGTACACAAGCTATGGAAGCTCATCCTCCACTGCTGAAATGTCTAATCTGAAATTACAATGAGCTTCCTTCTCTTTTTCTCTCATTTATTCCACATGGAACATGGCAATTATCAGGCGTTTTATCTCCAAGCTCAAAGCCCGACACAATATAAAAGTTTAAGTGCTCTGCATGTTAAAAAGAACTATGTGGAGTTTACAGCCCCTTCCATTCCTGAAATTGTACAGTTCATCCTCTCTGGCAGACACAATAAACAAACGATTTCTTGTTCCACGTACACAATCTTAAAAAAAAAATTGGGAAGGGCTAGCGCGCATGCTCAATACTCAAGCCAGCCAGCCCATCCCACAGCCCTTTGCTACTGCGCCTGCGTGTGGGAGACGCCTCTCCCAGTTTGCGGGCGGGAAGCACCCTGGAGAACCCGGATGGAACCCGGGCCTGGCTGAGGCGGAGGTTGTTGCGGATGCTTCTCTCTCCAGGCGCACCGCAGATTCCTGCCCGGGGGTTGGACCCGAGGAACCGCCGTCTCTACTGGTTCCGACGGCCAGAGGCTGGGCCCTAGGCTGAGCGGAGAGTGGGACTGGAGCCTGAA", //
			"TTAAGAGTGTTCTCCTATGAGAGGGATTTTTCTGAAGTCTCGGTAACAAGAATAGTTGTGTTTCCCCTGAGCAAGTTGTTTTGCTTTTTTTTTTTTTTTTTTTTTTCCACAGAGACTTGCTCTGTCGCCCAGGCTGGAGTGCAGTGGCATGATCTTGGCTTAGTAGCTGGGACTACAGGCGTATGCCACCATGCCCAGCTAATTTTTTGTATTTTTAGTAGAGATGGGGTTTCACCATATTGCCCAGGCTGGTCTCGAACTCCTGAGCTCAGGCAGTCCACCCACCTCAGCCTCCTAAAGTGCTGGGATTACAGGCATGAGCTACCACGCCCGACCATTTTGCTTTTTTATATGGACACTATGAATCATGGACTGATAGTGTGTTACACTTAGCAGCAAGGAAGCTCTGAGCCAATTTGTAGCCTACCTTTGCATCAGGGGTGGCTTAATCAGACTTAAAGACCTATTTGAAATTCTCTGCCTAAGTTGGAGTCAAAACTGCTCAAGGTTCAAATAGTATTACAGACCTGTTTAGTCTTTAACTATTAAAAAGAACTACCATGTTCTCACTCATAAATGGTAGCTAAATAATGTGTACACATGGACGTGGATAATGGAGTGATGGGTAAAGGAGACTTGGAAGGGTGGGGTGGAAGAGTGGGGTGGGAGGGAGTAGATGAGAAATAATGAGTACAATGTATGTTATTTGGGTGACGGATACCCTAAAAACCCTCACTTTGCCACTATGTGGTCTATGCATATAACAAAATTGTGCTTGTACCCCATAAATGTATACAATTATTTTAACCTCCATGTCAGAATCCACCCTTCCTTATTTAGTGAATTGCTCCTTTATTCAGTGATTCTTAACTGAGGGTATGCATACATCTTTGGGGCTGAGTGGAAGAAGGTGGTGGTAGTGAGATGCCCTTTCTAGGGATACAAAATGTGCTAGATTTGTTTGTAACATACATTATTGAAAACAAGTTAAGCCATAACTATAATTCACCAATAAAGGACTTACAGGTGTTCATAACTTGTCTAAGCTCAAATCTCCCATATATAACTTGCTGGCAAATCATTTATTCCAGGGCATTAGCTATGAGTTGCTAGCTTTAAGTTGAACTATATCCTGTCATGGCTTACTTCGAATTGACATGCTGCAGGCTGTATCTTGACATATACAAGATAGCATGTGATG", //
			"CTGTACAACTCTTTTTAACAACACTTAAGAAATCATAATTATTAAGGCAGAAGCTCAGAATGCTTTGGCTAAGGATGTTACTCCTTGGGCTGCTTTATATATCTTGACTATTTTATTTATGTCAGACATATTAATGAATATTGCCTGCCTCTCTATTTTCCTCAAAAGGAATATTGCTCAAACCACTAATAAAGAAATTAGTCTAAAACATTCACATAAATGATAAACGTCCTTACCTTAACCAAAAGATAAGAGAACTCAAGCTTTAGCCAGTTTCTAAACATGAAGGGTTTCCATAGACCTTAGCTGCCATTTTACATGTTCCAGTGGCATCCTAGATTCCCAATAGTTTTAGTGCTATGAGCATACAATTTTGACATTCAGTCCCTCAGAACTACACACATTCTAATTTTGGTAGTATGAATAATCTCTCCTATGTGATAAGTTATTTAACCTTCTAATATTTCTTAGAATCGAAAAGAACAAATGAGCTGCAACAAAGAATGGCAAATAACTTTTATGCTTAACACAATTTAATAACCTGAAATGAAGTAAGTATGTGCTATGTTTCCATTAAAAAGTTTCCAGCCACAATTAATTGAACGAAAACTTGTCTTGTTCCAAGATTATTCTTGGAAATGTAATTTTAAAATCTGCTTGAAATGAGGAAACTTACTTTTTTATACCATATGAAAGCAATTTCATTTTTTAGGAATGATTTTTGGATAGACTTCCGATTGGATATTTTCCATTGGAACTAGCAGCATAGGGGGTCGGGAGGGGGGAGGGGAGGGAAGCAATTCTGTGTTCTTTTGCCAGCACTGACAAAGGTCTGGTTGTCAGTGATACCTTTACAGCTAAATTTACTCCAGAGTGACAGAAACAGGTGCACCTCGGCCTGCCAGACACTTGTGCAGAGAGATCACGCATCTCACGGCTTGACGATCAAGGGGGCAAAGCCTCGGTCTTCATAGAAAAGGAGAGGAGGCAAACGCAGCCCAAACTGGGGGGTTTCTCTTCAAAGCCAGCTGGTCTGGCTTTATTCTGCAGGAATTTTTTTACCTGTCAGGGTTTGGACAACAAAGCCCTCAGCAGGTGCTGACGGGTACAACTTCCTGGAGAAGCAGAAAGGCACTGGTGAGTTTCAATTGCCAAAATATATTTTTTAATCTCTAAAAGTTAATTTTGTTGTCTTGAAAGA", //
			"TAAGACATCCCAGAACCGTTCCCCTGTGTTAACAAATAAAGGGGGTACAAGGGGAAAGGAAGTTTTCTGTTTTGTCTTTAAAGTGTGTGTTTTATCCTGAATACCCTACTTCTTGCTTTACTTTGTGGCTTTTTCCTAGCCAAAACTCGCACACGCGTTAGCTTAATAGAGCTATGAGTTGGCTTTTGTTTTTTGAAATATAATTCCTTAGATGCTGAGAGCTCAGACTATAAAATCCAGTTTGGGGCCCGTGTTCTTTCCTATTGGTCTGTCAGGTGAAAAACTCCGGCTGGCCAGAGTGGGACAGGGGCGGCGGCTAGACCTGCAGAGCCCCTCTTGCTCTCCCAGGTTTGCAAAATGGTTGGAGCGCTTCGGGCAGCCTGCGTCACCAGACGTGGACCGTGTGCCAGCGCTTTGTGCACTGTTAAAGGGTGGGTTCATTTTACTTGGCCCCTGGTATTTTCGTGGGTCGCTCTGAGCCCATCCCGAGCTCTTTTGGATCGCATTTCTCCAGCCCAGGATCCCAAAACCAGACCAGCAGGACAACTACCCCGGCACTGCCACCGCCCGTTTCCACAGTCCACCCACAGCCGTTGCGCCCTCCTCCATTCCCCAGAGCCCATCACGCACCCTCTTTACCAACAAACCACCGGACGCTTTTCCCCCAGCCGTGACTTCCACAACCCCCAACTAAATACAAGTCGGCACACCCCCTTCCTTCCCCGCGCCAAACCAGCGTGGCCGCCCACTCGTCGCACCCAGGTCCAGCAGGTCCAGCAGGTCCCCAGGAGGGCCCAGAGCCTGCCTCCTGCTCGCCCGTGGCGCACGCACGTGGCGCACTCACCCCCTACCAGCAGCCTCCGAGCTCTAGAAACACTCCAAGGAAGGAACCCCCGCCCCTTCAGCTCCGTCGGCCGGGAGAAAAGGGGGGGGGGAAGGGGGGGGAAGCAGAGACGGGCAGAGAGAAGAGTATATAGGTGATGAGAGCAGTGTTGGAGGGGAGGATCGGCAATAATTGGATGGCGTTGCAGCTGTTGCGCCTCTGCTGAACGCCCAGCCAGAGCGGATTCACTTTTCACACAAATCAACCGGAGGGATTGTTGCTGCCTGCAATCAATAACTCAGCGGAGTGAGTTGCTCAAACTAGAGGCGTCCCTCGTCTGGGTCTTTGAGGGCCGTCCGGATTGAGTCCCAGCGCTCT", //
			"CAACTGGAAGAAAGAGAACTCACCAAATCACTTGGTAGTGACTACAAGACCAAGTTTGGACTCACTCACTGTGGGCCTAAGCCTAAAAACCTAAACTCAACTAACACCCATAAAGTAAAACTGGCCAAAATTTTTCATTTCTCTTTGACTCTGAAGCAACTTAAAAATAAACCTCAGAGATGTCAACCCAAATATAGAGGCTTTATAAATGATTCTTCACTATGCAAAGTCCTCCTGCTATTACAAATGGGAAAAGAGATATTGACCATCCTGGTCTGATGTGTCTCAAGGCTGGTTCTCTTTCTGGGACTAGCCTCAGTCTGAAGCCAAGGAAAGGTTCATTTGTGTGCACAAAGTCATGGAAAGCCAGACTAAAACAGAAATCAGAACACAGGAGCATTTGTCTGTGCTCAGTATAGTTAAACACTGCTTGGAAAAATAAGGTATTATTGGTAATAGAATTTATAAACCGTACTGAATACACATCACTAATCCCAAAGTATTTTAAATTGGCCTTTAGGCAATGCATAAAATAAGCCCTTTCAGTAACCTCAACTGCATTGCAAACATTTCCCGATTTGACAAATTGTGTTAGCTTCGGATATAATGGATTTGACATTCTTCTGTAATTCTCTATCAGATATAAAACTGAAGGTACAGAAATACAGATAAACATAGAATCCTTAGATATGTGTAAACCTTGTCTTGAAATAACAAATCCTTTGTATATTGCAAATTCTCAATCACAGACATTCCACATGCTGTATATTCTTCTTGGGTTAATGCTGCACCTAGTGAATTTTTGAAATTTAGATATTTGGCACTATTTATGTTAAAGATTATTATCACAATGCATGACAATTTCAGTTGTCTTATATTCAAAACACTTTGGTTTAAAATCAGAATATTGTTACTTCAGATTAGAAGTTTTAGAAATAAATCTATTTGAAAGTATTTTCATATCAGCATTAACAAAATACTATATACTGTTTTGCACTTCCACATTTGAAGGTGCCAAAGAAGAGTTGCAAACTGTGAAGTAACTTCTATGAAGAGATGAAGTAAAGAACGGAAGGCAAATGATTGTGGCAGTAAAGAAGTGTATGTGGTGAGTGCTCACATTTGTTTTTATCATTTACCACAGAGTTACCATAAAGGTAAAACTGCCAAACTCCTTGGCCTTAGGTATTTACAGAATAGA", //
			"TAAAATGAATTCATGGTAATGGCAAACTCAACATTTGAAAAACCGGCAAACAAAATACATTGTAAATACTAATCTGCAAGAAATACTACTATTTCAGATGGTTTTTGCTTTGTGACAGTTTGTGATGCATTCCTATAATAAAAGGGAGGGAATGGTCTTAGTTTTGCTGATAGTCACAATGATGAGCTGGAACTTGAAGATTGTTCAGATCTAGTTGTACTTGCCTATAAAATAAACCTTCCCCTTTCAAGTGAAGCCTTGGGACAGTTGCCTAAATGTTGAGGCCACTCTTCTGAGAGAAGCTACTCACATCAAATACCTCCTACATTGAAGCAGGAGGCTTTTTGAACCATACTGTCCTCATATATTAAAATAGTCATCTGGAGGCATATTGCTGAACTTAACAGGAAATACAACTAGTCATTGGGTTAAACGTAGTGCTAAAAGGAATTTTCTAAAGTGAGGAAATCTTTAGTTAGCTAGATATTTCAATATACTATTGTCTAATCTCATATATTCCATGTTCTATTAATTTTCCCTTCTTGCTTTCACAATATTTTTTAAAAAATAGTTTGGTTTAAACATTTTATGTAATCTATAATTCTACTGGCATGCAATCTTGTCTTTTCATGATTCTATACTAGTTGATGTATTTTATCTTTGGCTTTATTGGGCATGGTATATATTAACAGTTGGTTAGGGCGGGAGGTGCCCAATCTCTAGTAGCCTAAATATTAAAGCCTTTTTTTTTTTTTTTTGTAAGGAAAAGTGTTTATCATGCTTTAACCTTAACATAAGTATCCTCCCTTATCAGTCTCTATTTCTGCTTCCTGTGCAGCTGAAACTCTTCAGGATAAGTGAAGTCATCAGCTATCGGCTAGGGTAGCCTAATGCATTTGGGAAGTTGGTTTGGTTTACATGTTAATGACCTGGATGAAGTCCAGTAAGGTGTGGAAAAGATAGAGTGGGGGGAGGGGGGGAACTCCGAGGAAGCTGCTTCCTCGGTAAATATTTCCAAACTGATTAGAAAGTTCATGGCCCCGCGATGAAGTCGAGATCTCAAATACTGGTAATTATTTTATTATTTCGTACATTACTAACATTAGAGAAACCTTTCGGCTCGCTTTCTAGCGCTCCAACCTCTCAGGGTGAAAACCGCGACTTCTGGGCGTGCAGGAAAAGCACGCTGGAATCGAGTGCT", //
			"TGGCTCGCAGCGTCCCCGGAGAGCAAGCGAGCGCGCCGCAGCGGGAGACTAGGTTCTCTCCGAGCGTCCTCCGCAGAGGCGCCGCGAGAGGAGCAGGGAGCCGCAGCTCGCCGTGTTGTCTTCATTTCGTGCAATAAAGAATTGTCATTAGGTTTGCGTATGGCATGTGCTATTACCCCACCGTAAAACTAAAATTAGCAAAATGTCAGGAATGGAAAGATTGATTCACCAAGATGCAATTATCATTTAAAAGTGCTTGATTGAGGTACTGATGTTCAGTGATTTATTCTGCATACCATATACATAATTAAAGTAGTGTAGTGGAGTAATTTATCAATCTAGTTGAGACTGGAGGGGTGAGGAGGGAGCTGCTGTATGTTTGTTTTATTAAAATGCTCCGAGGTCTAGTCCCGCCCCCCTTTTGCAAGAGTGAAACTGATGATTTCTCCAGCTCGCGAGGAAAGAGTCAACGGTTTGGGATTGTGGGGGAGAGAGAGACGGAGAGAAAAAGGCAGCGCGAAGCAAAGGCAAGGACAAAATTAAATAAAGGGGGAAAAAAGGAGGCAAAAGACATTTCATCGGACGTGCTGCTTAGAACCCCAACCATTCGTGCTCCGTCTTCCCTACCACCCCCGCCTCCCCTCCCAGTATCCTTCAATCCCCCCCGCCCCCACCACCCCCAGTCTTTTTTACGCGATGTTTCAAACGCTGTGAGCTGTTCTCCTTTTCCCATTCGTCTTCTGTCACTTCCTTCCTGGACGCAGTTTTCTGGACGAGTCTGGTTACTTTTAATCCGACCGGCCGCTGAGAGCCACTTTCTCCTCCTCCTCCTCCTCCTCCTTCTCTTCCTCCTCCTTCTTCCTCCTCCTCCTCCTCTTCCGAGCGGCCTCGGCGCGCGCGAATGCGCGGCCCCGCGCCCCCCCCCTCGCGCGCGCTCCCCTCGCGCGCGCGCACACACGCACACATCGTCTCCAGCTCTCTGCTCGCTCTGCTCGCAGTCACAGACACTTGAGCACACGCGTACACCCAGACATCTTCGGGCTGCTATTGGATTGACTTTGAAGGTTCTGTGTGGGTCGCCGTGGCTGCATGTTTGAATCAGGTGGAGAAGCACTTCAACGCTGGACGAAGTAAAGATTATTGTTGTTATTTTTTTTTTCTCTCTCTCTCTCTCTTAAGAAAGGAAAATATCCCAAGGACT", //
			"CAATATCTTACTAAGTTTTAACAAGTTTATTACCTTCAGTTGCAGTTATATATTCACTCTTTATATTCACTCTTATTTCAAGTGAGAAACATAGGTAAATAACAAAGCTTTCATTTTGTAGGCCTGGGAATTATACAGTTGGGTTCATTCTGAAATGCCTGATAAATTGCACAAGGACTATCCATTTCATTTACACACTAAAATATCTTAAAAGAGATATAATTTTTTTTCCTGCTGTGTCTTTACACTTGAAAATTTCTTTGTAAAAGTGGAATAAAGAAAGACTATTTAGAAAAGATAAAATCAAAGATAAATGCAAAATTTTCTATAAATATGGAGTATATTTATTAGAAAATAACAACAAAAGAAAAACTTTCCTTGGAAGGAAACATATTATTCTACATAAAGGGAAGTCGTGTTAGAACACAATATCCAATGACCAGCAGCTTGCCTTAATGAGGTAGAAGTTAGTGGGTGCCAACTTTGGTGTGAGTTTTCTGCCATTAGGATGGTAAGCGTAGACCTGTGATATTTTTGCATGTTTGTGATTTCAAACAGCTCTCTGATTCTACTTCCTAAGTTTCTAAATGTTTTTTATCCAATATACTTATGAAAGAAAGGGGTCATTCCTGTAATGGGTCTTAAATTGAAAGGAGGCCATCAAAAACTTGTAACATTCATGGGCCTCTTCCTGAGGTGTCATACAACATACCTGTACACAAAAACATTCATGAGTTTCTTCTACTAACACAAACATTCTATAGCATCCAAGTGTAGACACTAGATTGAACTTCATTCTGTATATGTGTCATTTAGTTCTCCTGACTCAAAATACACAGTGTATTAACAATATTTGACTATCACTTAAGATTTTCTAATTTAGTCAATAATACAGTGCTATTACAATTGTTGGACACATTTCAAAGAATGTAAGGGGGTGTGTTCCTTTCGTTCAAGGAAGCAAACATGTAGAAAGGGGTGGACCTTTCCCACAAGAGCCACATTTCTTCCCTTGGAGAATTGAAGCAAATATGCAGTACGTAAGTGAATAGCAGCATGAGAAAGAAAATAATTTGCAATGATCTCCTATAGTTAGTGAGCAAAGAAAATTGTCAGTTTTTTTTAAAGTAGCTCTTATTGACAACCTATCTTAAACTGAATACTGAAAAAAAGTCTATGAAAGTTTTATAATTTCAGTATG", //
			"AAAAAAAAAATACCCAAGGTGATAAAGAATTTGGAAATTAAATTAAAGAAATCAGAAGACAGCTAATGGTCAGACCGAATGTGTAACAGCCTTCCTGTTCCAAGAACTTTGAATTGGAGCCAAACAATCTATTAAATAGCCAGGACATTGGCTTAGCATTACATAATACATGTGGGAATTTATCAGGTCCGTTGTCAGACACCAGCTAAAATAAACAGGGAAATTCTGAGCAGTGTGTAAAGTTGTTGCATTCATTGACCTAAAACGAAGTAGAATAATTGGAGCTGTTGCTTGAAAAACCCATTGTGGAATGCTTTCTTGTTAACTTTCACATTTCAGAGCAATTAACTTCCGTTGTGTTCAAATATTTTCAGATGTAATTTAAAACCAAGTAAAGTATGTACTTTTAATATTCTGGGTTTCTTGGGCTTATCTCATGTTATTGATGATCATTTCTCTAATTTTAATTCCTGACCCAGAGATCTCCAGTATGTAATGTGAAGCTTCTGCAATTTTGTATCTTACTCCACAAAGACATGATATGTCTGTTTCTAAATATTTAAATTTAAAGGAAGGATGGCTTGTTATAAAAAATATGGTTATAAATTGAAGAATAATCTTAATTTTGCACCATTTATTTTGTGTCAATGCTTATCTTTTTTGATTCTTGTTATCTAAGATGTAGTTTAAGGGCTTTCAAAAAGAAAAGATCTCACTGCTGATAGATGCTTGTAATCTAAGATTAAATATTCCATATTATTTTTAAGGTGTTCTCACAGCAATAATTTTTTTAAAGTAAGATTAAAGGATTTAGATTTAAATTAGTTGACATTTACATATATCAAATAGCACTTTCATAATCGTCCATCCTATTTGCATAACGAGGGACGTTATCACCTCTGCTGTCAAAACAGGAGATTGTTTTCCCTTCAGAAATGAATTAGCTGCCCTACTTAGCATACACAGGTACATAAAGGTTCATTAACTCTCTGATTTAGGTAATTTTTCATAACGGGTGAAATTGTCACCTAATTTCATTAAAAAGATTAGGAAACTTTTTACAAAAAAAAATACATTTCAAGTGTTAGTCTTTAAAAAAGCATAGTTTGCATCATTTAAATGACTTGTTAAATTACTTATGAGATCATTTTAATTGTTAAAAAATAAAACTACTTGATCTGCTCCTGTCTGTGATATACCA", //
			"AGCTTAATAAAAGGTTCAACCTTAAAGTTGCATCCCACATCGTTAGTTTAACTTTACAGATGAGATTTTTTTTTCCCCTCTCTAAATTCTAAAACATGATACATCTGGCTGAGAGGCTGGAAAATTGTTACATGCCTGTGCAACATTCTTACAACCTAATCAGTTCTTCAACTTTTCTCGCAGTTTAATCATTGGAGAATGTTCTTTGTTTTTGTAGTCACTGTTTCACATAGTAACAGCTTGTTGTATTTTGGTCTGTTTGCCAGAGGGTAGGTTTATTTTTAAAACCCTGAACTCTGAATTTTTCTTTATTTTTCCAAGATTGAAAGCAGCTGAAATGCCTGGAGTTACAGGTTGAAGGCTGCTTCACAATATGTGGAGGAAGCTTCTTTCCCTCCTCCCTTTGTGTGTGTGTGCTTTTTTTTTAATTTTTCCCTAACAAATGTCTTCTAACTGGTAGTTGTCTAAAAATAGAAAACTGCTTAACTAAAAATAGCCTGGCTCTCAGTCGTGCCAAGGAAAATGGAGAGGAATAGGCGTGGAATAGAAGGCTGAGTAATCCAGTTCTATTCATTCAGAGAAACCTTTGAGAATGCGGGCGATGATAAAGTGCCCTCAAAAAACCACAGGCCCGACTCTCTCTTCAGACTTCTAGGTGCCAATAAACATTTTAGCCCCAGGATACTTTTTTTTTTTTCCTCATAGGTTGTAAACATTTTCTGAAAGACAAGGCTTTGTTTCAGGGAACCTGACAAACTGCCATAAATAACTAACTTTCCTAAGGTCTTAAAAATTCATATTTGTAAGATGTATTTTAAAAGAGAATCTTAAATGAAAGTATTGTAAAATAATGTACGTCGTGTAATAATTAACAGAGGAAACTCTGTTATAATAAAAACCACTTACTGCTTGCAATTAAACTCAAAACTTTAAGGAGTTTCACAGCAAGAAAATTATTCCTGTGACTCATTGTACCTTAAGAGAGCAGCTGACTCATTTTAGTTTAGCTCCCTGAGACATGAAATTCTCTTGTTCAGTCTGTATCCCCTTAACATGCCAACATTTAGTCTCTTTCTTAAAATTATTATTTTCACTCAAGCAGATTTTTACTTTTAATCTTTAAAATGATGTAGAAACTACTTTGAAAAATAGCATTTCTGGAGTAGGATGGCTTTTAGTTTCAACAAATGGAATTCTGAGT", //
			"TTTTCACTTTTCTTTCCACCTGTGTTTAGGAAGAGACCTTCTCAGTATTATAAATTTTGATAAGATTTTCACATATATAGAACCCTAACAAACAAAAGTAGTTCTCCATATAAGCACAGCGTCTGAGCATTCTTCACCGATGCTTGGTGGGGATATTAGCTGTGTTCTCTTACACCCTTTAGCTGTCAAGTACATCAGAAGTGGAGGCTGTGGGGACAGGACACCACTAATGCCAACCATTAGTTTAAATTCACACTTGGATCTGTTTCCAACATTTCTGCTTTTGTCATCATCACAGGTTCGGAAATCATTGTGTAATGTCCTGATTTCTGATTTTTCTGTGGGAAGAGAGTGTTGCAATTTGCCACCTTAATTGTATTCTGCTGTAATTGTCTCTCACTAGATATTTGAAGCACGTAAGAATTCTGTATCATGAAACTTGAGTCCTTATACCCTCTGTTCATTGACTGAATTTAGTTGCAAGAGGAATCTTTCGCTTTTATTTTTTTAAACTGCTGATGAGCACTCGATTGAAATGACTCAGCTTTCATCTGTGGTTTCTCTTCCCTAATAGAATAAAGCATATGTAATAACCTGAAATACTGGACAGCTTGAACTTCTTTAATGCCCCTGAATGTCTCTTACAGCCTTTGTCTAGGTTTGAACTTTCCAAATTATGTATTTCAGACGTTGAGAAAGAAGGGCCTTAATGGCTGTGACAGCCCAGACCCCGATGCGGACGATTCCGTAGGTCACAGCCCTGAGTCTGAGGACAAGTACAGGAAAATTAACGAAGATATTGATCTAATGATCAGCAGGCAAAGATTGTGTGTAAGTACTCAGAACACCCTTCATTTTTTTTACTCTTGATATTTCTCTGAACCTGGCAGGCATTGAACAAGAAAGAAAACAAAGGCTGTGAAAGCCCCGATCCCGACTCCTCTTATGCACTCACCCCACGCACTGAAGAAAAATACAAAAAAATTAATGAAGAATTTGATAATATGATCAAGAGTCATAAAATTCCTGTAAGTACCAAAGGTAGATGGCTGGTCTGCTGATAACTGCTGCAGTAACATACCTTAACCCTCTCAGTGATGGCTTTCTCTAAGACTCTTCGGAAAGTCAAACAGATAAATAGCCATCTCATCCCACGGGCAGCTCATTTAGAAAGCAAACCCTAAAGGATGCTGAGATTATG", //
			"AAGGCTGTGTGTGGACTGGCGCTGCCCCTGCAGGCCAACACTGGGAGACCAGGGACCCGCATTCGCCTGCAGCCAGCCTCACACTTGCGCAGAAGCCGCCACTGGGCCCAGGGCCAGTCCAGACCCCATCCCCAGGGCCGGCCAGCCTCTGTGACCCGTCTGTCAGGGGAGGGGCAGCACTGATTTCCAAGGATGCAGCCCGTATCAGGGCCTCCTGGCACCTGCCCTGCGCTGGAAGATTCTTCTAGTCTCCCGGCCCCAGTGTCAGAATTCTGAGAACTGGCCGGGCGCGATGGTTCACTCCTGTAATCCCAGGACTTGGGAGGCCAATGTAGGCGGATCACCCGAGGTCAGGAGTTCGAGACCAGCCTGGCCAACATGGTGAAACCCCGTCTCTACTAAAAATACAAAAAAATTAGCCGGGCGTGTTGGCGGGAGCCTGTAATCCCAGCTAAAGAGGCTGGGGCAGGAGAATCACTTGAACCCGGGAGGCAGAGGTTGCAGTGAGCTGAGATTGCGCCACTGCACTCCAGCCTGGGCGACACAGCAAGACTCTGTCGAAAAAAAAAAAAGAAAAGAAAGCTGAGAATTAGGAGGAACTGCAAACCCTTTCCAGGCGCCCTTTGCAGTGCTAACACGCCCCGGGTTGCACGCGCAACCAGCCAAAAGCCGCCAAACTCTGCTTCTTGACATCCTTCTCTCACACTGCACAGCTTTCAAAACGCCAGAAGCCCTGCTCTGCACAGCGGCTCCCCTGTCTGCGGACTGGGCCGTTCCGTTCTGGACCCAGGCCGGAGGCCGTGGGCTGCACCGCTCCAGGAGGCCAGGCTGGCGCGCGCCCGAGGCCGGGGCTGAGGGGGCGGCGCGGAACTCGGGCCGCGTGCGGGAGCGCCACACGCCCGCCAGGGGGCGCCGCCCGCCCGGCCCCGCCCCGCCCCGGCCCGCTCCGCACGCTCCGCTCCGCACGCTCCGCTCCGGGACGCCGACGGCGGCGGGCGCTTCCGTCTCGCGGGCCTCGGCCCGGTGCGAGCGGCTCCGCGATGTGGCTGAGCCCGGAGGAGGTGCTGGTGGCCAATGCGCTGTGGGTGACGGAGCGGGCCAACCCCTTCTTCGTGCTGCAGCGACGCCGGGGCCACGGCAGGGGCGGCGGCCTTACGGGTGAGAGGCGCGCGGGCGGCCGGGCCTGTGGGCGCCGCGGCTG", //
			"CGGCGGTGTGGCTCCTTCCCTGGCACTGAGATGTGTGATCCTCCAAGGCACCTCACAGCCTGCGAGCCCGTGTGTGTCCTAGTCGGTCCCTCTGGGGGAGCTCAGGTGGGAAGCAGGATCTCCAGGCAGGCAGTCAGTCTGCGCTGGGGCCGTGCCCACTGGATTCAGGCAGCTTGTCGTCAACATTTGTTTAAGTGGACCCAAAATCCATTTCTAACATCACAGGGGCCAGAGGGGTAGACTGTGGGTGTGAACCTAGCAGAGTTCTCTCTTTTTTTTTTTCCTTTTTCCTTTTATATTAATAACAACTTTATTGAGGTAAAATTGATACGCAATACACTGTACATATTTAAGGAATACAATTTGGTACGTTTTAACAGGTATACGCCCATGAAGCCATCACCACAGTCAAGATCAGGGTTCTCTCAAAAAAACACAGAATGTGAAGTGTTTTAGGAGGGCGCCCAGCACCCTGGCAGCTGCCCCAGCCCTGGGTGGAGTGGGACCGTGGCCACGGGACCAGAGCCAGTGGGAGGGTGTGAGGGGTAGAGAGCCCAGCCCAGGCCAGAGCCCACCATGGGGAATCGACCACCTGCCCAGCCAAGAAGTTCAGGCAGTCAGCTGGCCCTGGTCGAGCTGGGCTCTCCCCCACTGGACTTTGCTTTCTGCCAAGACAGCATCACTCTCAGGCCGGTGTGGCACCATGATGGCAGCAGCAGGCCTTGCATGCTCCCTGGGTTATCCCAACACTCCTGCAGAAGGGCCACTCATTTCCTTGGTAGCAACCGGGTGACGCAGTCGCCCTAAACCCACCCTGAGCCCAGCCCAACATCACAGGCCATCACCCAGCCCTCTGTGCAGCCCAGGAAGCATGGGCCCGAGGGTGGGCAGGGGTGTGCCTCCACGTTAGGATAGGGCCTCTCCTGTCCTTTGCACTGACCCCCCTCAGAAAAGGGCCCCACAGGGTGAGCCCAGAACCCGCTCCCTGAGACTGACTCGCTGGTGAGAGTGGGGTTAATTACTGAGGACTCGGTGTCCAGCAGCCAGAGAGGAGCTGAGGGCTGAGCTGCAGCCACAGCCTCCTGCCCAGCCAGACAGGCTCTTCTCCGTGCACCGTTTGCCCCCATGGCCCTTCTTGCACCGGGCCCCGATCCAGCTCTGGATGGATGGGCGCATCAGAGGCCACCTGGCAGCACAGTTG", //
			"GATCTAAGAGTATCCTCTTAGAAGTAGATAGAAATAGGGAATAGAATAGAAAAACAGAAACAGATCTAAGTGCATCCTCAGATGCAGTTGCCATGAACCTCCTGTGGTAGAATCGAATCCCTCTTGGAGTTTAGGCAGGACCTCTTAAATGGCAGCAAATCCATTCAAGTCATCTTACACAGAAGAGGGAATACATTGCCCCTGTATTGCTAGACTGAAAAGTCTAGCAATGCACAGCTTCAGGCATGGCTGGATCCAGGTGCTGAAATGTGCCGTCAGGCTTCCCCATCTCTGATGTCCTCTGCACTGCTCTTCCTACACCATGCCAGGACTGTCGTCACAGCTCCAGGGTCATACTCTCTTCTCAGTAATCCCAGTGCAAAGAGAATTTTTCTTTCCCTTGTGCTTTTAGCAAAAGACCCAGCCTTAATGGTTGGAATCTGCCTTTCTGTTGTTGTTGCCAGTTCCCGTCCACCTCTCCAGCTGCCTCAGGTTCCGTGCTCAAGGTGGCTGCTGCGCTGTGGCCCTAGCGTCACAACAGTTGAGCCAGCTCTGCCTGCCTCCTAGAGAGGTCATAGGAGCGAGAGCAAGCCTCAGGCCTCTTGTTTCCCTGGAGCCCTGCCAGGAAGGCTCAGCCCTTCTCTGGGGCCAGCAGACCAGCTCTGCCCCCTGCAGGCTGCCAAGGACTCTGTCTCTCAGCCAACCCCCTCCCCATCTGTTAACAAGGGGCTGATGACAGCCTGAGAGCTTTCGGGAGGTGCTGCAGTGTCTGGCCCGTGCTTACCGCCCCAGGGCTCCGCAGGCTTGGACAGAGGGGGAGGGTGGACACACCACCAGCCTAGGGGTCCCCTCGACCCACAGGCTTAGCGGCTGGACCGTCTCACCAGACCGTGTCCCACAGGCCAAGCACCTGGCTAGCCAGTACTGGGGGTGCAGCCGCACAATGGCCGGCCGTCGGGACCCCAGCCTGCCCTACCTGGAGCAGTACCGGATTGATGCCAGCCAGTTCCGGGAACTCTTTGCCAGCCTGACACCCTGGGCCTGTGGCTCCCACACACCTCTGCTGGCAGGGCGCATGTTCAGGCTCCTGGACGAAAACAAGGACTCGCTGATCAACTTCAAGGAGTTCGTGACAGGGATGAGTGAGTGCCTGCAGGGCCTACCAGGGGCTTGACCCTCCCTCACCCTTTCCCAGGAAGCC", //
			"TGGGCTCTGCTGGGCTTGTGGCCCATGCCCGCTACACTCACCCAGGACCATGCGATCCGCAACCCTGGGTACTGCTTGGTGACCTCCAGCAAAGACTGCACTGAATGAGGCCGGCCAGCACGCACAACGCGGCCCGACCTCTGAGCTACCCAAAGGGAGAGCAGCAAGTTGCCTGTGCGTGGCTGGTTCCTGTGCGCCCATAGGAGCATGAAACGCCCCCAAAGGGCCCACTGGCCAGATGGTCACTGGGGCAGGCGGGCAGCTGCCATTCACTGGACTCCTAATTGTGTTGATTGGATTTTTACCTGGGACATGTATTTCCTTTTCAAACACAATGTCAAAGTCAGGAAAGCGGCTGGATGCAGTAGGAGGCCCAGGCGGGTGGATCACTTCAAGTCAGGAGTTGGAGACCAGCCTGGCCAACACGATGAAACCTGTCTCTACTAAAAATACAAAAATTAGCTGGGCGTGGTGGTGTGTGCTTGTAGTCCCAGCTACTTGGGAGGCTGAGGCGGGAGAATCACCTGAACCCGGTAGGTGGAGGTTGCAGTGAGCCGAGATCGTGCCATTGAACTCCAGCCTGGGCAACAAGAGCAAAACTCCAGGCTGGGTGCGGTGGCTCACGCCTGTAATCCCAGCACTTTGGGAGGCTGAGGTGGGAGAATCACTGTAGCCCAGTAGGCGGAGCTTGCAGTGAGCTGAGATCATGTGACTGCACTGCAGCATGGGTGATAAAGTGAGAGTCCATTAAAAAAAAAAAAGCTGAGGAAAGTAACAAATAACCAGAACCAATCATCACCATCAAAACTATTACCGGGCACTGTCATGGGCAGGTGCTGGGACTGGCCAGGCTCGTGGGCAGGGAAAGCAAGGCTCTGCCCAGGGCTGGAGGGAACTTACTCTGAGAGGATCTCCTTGTTTCCTCCTGCCCTCACTCTGCCTTCTCTCCAAGCACAGCTCTGAGCCCAGAGGAAGCCGAGTCAGCCCTGGAGGCGGCCCATTATTTCACAGAGGACAGCTCCTCAGAAGGTGAGCGGCCACTGGAACACCCGACATGCCCAGCCAGCACTCTGCAGCCACCGCCGCCTTCCTGTGGGGCTCGGGCAGTGGCCAGCAGTCCCACCTGGGCTCAGGGAGCAGGCCGCATGGGGTTCTCCCCAGGGGACCACGGGCTGGGCTCTGCCATGCTGGCGGGGGCTGG", //
			"TTTCACATGGGGGATAATGCACACCAAGGAACCGACTCAAAAGAGAACCAAAAATAGTGTGTACCAAGATGCCCATGGCAGTCCTGGTGACAGTGGCAGAGGCTGACTTGAGCTTGAGGACCTTGATTTCAAGGACAGAAACTACAGAAGCAGGTACACCTTCTGTTGTACATGGAACCAGCAGGCCACTCTAGGCTTGTCCCGCATGCTTCTGGGAGCGGCATGTTGGTGCAGAGCCCTGGCCTCAGACCGCATGTGGCCCCCAGGAAGCAGGGCCTCCATTCCAGGGTGAGTTGCCTGAGCCCAGAGAGGTGTGCCCTTCACTGCCACCAGACAGCCAGCGAGAGCAGCTCAGAACTGGGGTGCTGCCGACCTGCCTGAGGTGCCCCCACCAGCCACACTGCCTTTGGGGAACAGCTCCAGGAGAGCTGGTCGGCTGCTTCTCTCCCCAGGTGCATGTTCCCACGCAGGGAGTATAGTGCGCGCCAGTTCCGGCAAATGTCCTCCCCGAAACGCTGCACCAAGCACAGGAGCTGTGCACAGACCACCCCTCAGTAACAGGCACAGCAGGCGCGGGTGGAAGGGGTCATTAGGGTTCCCCTGAGTTCTAGCAGGAACATTCCCCAGAGTTCTAGCAGGAACTATAGAATTCGTTAGTCCTCAGACTGGTCTATAGCCCTCATCATTGTTCACGTCAAAACCAGCATGTTGAGACTTGTATTCATTTGAAAAAAGGAATTGAGGGTTTGGCGGCCTTTATTTTAACCTGACCAAGTGAGGGAATGCTCAGGCCCTTTTGCTCTGGTGCCATAGGGCGGGGCTGGGCGGGCCAGGCAGGAGGTGTGGCATGGGAGACCTGCTCCCCAGGGCCTGGCCTGGGGCTGGCTGTACAGAAACACAGACTACATCTCAAGGACCCCAGGAGCTTGCAGTCCCAACAGCAGAATGTTATTCATGTTCTTTTTATTTTTGCGTTTGTCCAGAAGCACTACCACAGGAAGAGCAAGAAGGAAGTGGAAGTGAGGAGAGAGGAGGTACAGGCCATTCTCTGCCGCTGTTCTTAGAGACCCCAGCGTTGAGGCACCCTTGCTTGCTGGTCTTTTCCCCAGTCACTGCCAGGGAGCCCCCTACCCCGGGAGATGGCGGATGCGTCCCCCGATCAGGGAGCAGGCCTCGGGATGCACAGGAGGCTCCACGCTCT", //
			"GGTGATGCGTGCCTGTGATCCCAGCTACTCAGGAGGCTGAGACAGAAGAATCGCTCGAACCCAGGAGGCGGAGGTTGCAGTGAGCCGAGATCGCGCCACTGCACTCCAGTCTGGGTAACAGAGAGAGACTCCGTCTCAAAAAAGAAAAAAAAAAGATTTTTAAACACAGTATGCACACAATAATACTCCTAAGAGCCCTGTGAGTCCTACGATTACTGTCCCCATTTCAGATCAGAGCCCACGTTCCAAGCATAAAGCCTCCCATCTAAACAGCAGGGCTGTGGTGGCGATTCCAGGAGATACTTGGAAGCCAGAGTTAACTGCAGGTTAAGAACACAGGGCTCTGCAGCCAGACTGTCCGGTTTCGAATCCCGATGCCACCTTTTGTTTGGTTCTGGGATTTGGGGGCAAACCGAGGTAAATAAGTTGCCCCGACTCGTAGGAAGCGCCCATAAATGTCAGTTCTCATTCTTCCTAGTTAGCTGTTTTGGGGTCCTCTCCACAGCAACGGAATAGGGCAGTTTAACTCTGGTTAACTCTCTGGCCTCCGATGCTAAGGTTGCGCCCTCTGCTTGGCCCTCTGAGCGTCAGGGCTTCCGCTGATTCTGCCCCAGAGCCCTGTTGCCGTCCGCTCACCTCCTGATGATCCTCCACCACCCACACTGGGAGCTTGGGGTAACGCCGGAGACCAGCGCGCCCTCCCGCGGAGTCACTCATGTTTTTGCGCCCGTCAGTTTTGCAGTAAAGCGGTGGCAGCTGGAGAAGACCCGGGAAAGCCGGCGCCTCATCCCGGCCAGCTCTCTCCGGGCGGAAGCGCTCCCTCACCCGGTTGCGTTTTCGGATTGGAGGACGTAGGGGGAGGGGGCGCGGGCTCCAAAGGGTCTTTTGCTGACGATTGGTCTAGACGTCCATCGCTTTCGTTGACGCGCCAATCACCGCGCACAAGGCCCTGCGGTGGGCTGAAGAGTTTTCCCTCCCTTGGCCCAGCTTTCTCAGGTTTGCTTTTTAATTCCCTCGGTTTCCTGTTCCGGAGGCGCGGGCGGTGCCACTGTCTTGGTACCTGCGGTAGTAGCCTGGCTTTGCTCTGACGGCGATCTCGCGGCCCGAGAGCCTTTTATAGGTAAAAGGGGTACCTTGCAAGTGTTAGAAGAGAGTTTCCGAGTGTCAGGATTTAAACGGGGCTGGAGTCTCCGGGGCTGGG", //
			"AAAAACTCAGTTAACGCGTCTCTGGGACTTCTGGGTCTGTTATTTTCCCCAGCTAGCCAGCCAATTCTCTGCAGTAATCTCCCTCCCTTTTTCATTCTTTAATTCATTCAAAAATATATTTACGGAAAACCTGTTATGTGTAGGCAGTAGGCAATTGTCTGATGATACATTGGTAAAGTAAGATAGATGTGGATTTATTTTATGCTGAGCCTTCCTTCATTTTCACATGTATCTTGAGTATTAGAACAGGTAATGAGAGGGGTATACTCTAGTGGCTAAGAGCTGGAGTCTAGAGAAAGACTTCGCAGTTCACCAAGCTACCTGTTATCTTAGGCATGGTACCTGATGACTCTTCTGCCTTAGCTTCCTGCACTGTTCAGTGGACATGGTAGCCAAACCCATGTTATAGGGATGTTGTGATGGTTACATGAATGAATATTGTGAAGCATTTGGGACACTGCCTGGCTCAAAAAATGTTAGCTATTATTATTTTTATTATCATTTGTTATTATTATATTTATTACCACAATTGCCTTTGCTTAAGAGCCAGATGGGCAGCATGAATTGGGTGTTTTCAGTGACCTATGAATCCATAATTGATGAGTTATCACCCTACTATCCCTGATCTGCCTAAGGAGCCAGAGATTCTAAAATATTTTAAAAACTTGAGAAACAGAAATGTTTATGGTTATGGTTTTATTAATTCTTTAGAGGTGATACTTGTGGTTTTAATGGGTTCTTGTCATGTTTGTGTTGCTGCTGCTAATACTGTTAATTAAGCACCACATTATGCAGGTTTCTTGGCGAGGCCAGGCCAGCTGGGTTCTTGTCATTAAAATCCAGCAGTTGTGGTCAAGCTTCATGTGCAGTGTTGTTAGCCGCTGAGATGAAGGTGAAGCCTTCTCATTTGGTTCATGGCCTTTTGGGTCAGGTAACAAACGTGTATAATATGAGCCTGCAGGGAGACGTTGGGGAACTGAACCTCACTTCTTTTTATTGCAGATTCCACTGTGTAAAGTAATTAGATTCAACATAGACTACACGATTCATTTCATTGAAGAGATGATGCCGGAGGTAAGGGAAAAGATTGTGTGTGGTTTGATGTAGTTTGCTCTTTTGCTTGTTCCATACTACTATTTTAAAGCATTGAACTTGGCCAGATCTGGCTTTTGCCAGGAATATCTCTAAAGTATCATATTCA", //
			"TAATTTATTCACTAAATACTGAAACTATTATTCATTGTTTTGAATGAGTGCAAAGCTCTGCGGACAAATGTCGTCTTTTGCAGTTTCCGATATGGTCTGGCAGCATTGTAAATAATCCTATTATAGTTAACGAAAGGATTATACACATAGTTGTATTGTTTGTATTACTTCAAAATAAGTGGGCCATTCACTGAGCCTTAGAACTTTTTTTTTAATGTTACGGTATTTTTGACAATGGGAATTATATTCAAGTATCAAAGTCTACCATGCAAAACTAGCTTATTTACAAAGTGCAAAGGCATTCTGACTAAGGAACTTAAAAGAACTTCTTTTTCAATGAAAGAAAAGTAATTTAAAGATGAAAATCACATAAAAATTAAACAGAGAGATCATTCTTTTTTTTTTTTTTAGCTCAATGAAAGCTATTTTTATTCCTTCTTTTGAAAATGCACCATTTGTTTGCTTTGTCAAGGTACCTTTAAATGAAGTCATCCTAGGGGGTCACCATTTATTAATGCAAAATTTGACATATTGTTGTCATTAAGATCAGTATTAAGTTTTTCTCTTTTTGGGCAGATTGAATCTATAAATTACCCCATTTCTGTTCATTAAATTTAACATAGAATCCTTAAAAACAGGGGATCTCAGGATTGGTGTTGAAAATGGCAAGACCCAAGAATGTGCGTATTCTGGCATCATATCTAATCTTCAAGTAATATTTTGACTTATTTGATGTGAACAGGGTTGGGTGCTCAGCATTTTTTCCCTGATATACAGCGGCATTCTGTGTACCTTTTAAGCTATTCCTTTAGCCCTTAGCTAAATGCAGTTTGGTGTTTGTACTCTATGATTTGCATCTTACACATCTCTTGCTGCTTCTGATATTAATGATAAAAAGTGGGGGAAGATGAAATGGTAGCACTGAAAAATAAAGAGGGCCTGGAAGTTGAAACTATTACTAAAATTTTGCTTAAAGGTCTAGGTCTCTTACTCTTTCTGATAGATGGCAGTGATTGTGGTTTATTTTAAAGTATTCTTTTTTCCCCAGCATTTATTGCAGCGCTGTACATTGATAAGGATTTGGAATATGTTCATACTTTCATGAATGTCTGCTTCTTTCCACGATTGAAAGTAAGCTCATATAAAGTTTGAATTAGTGGTCTGCTTTATGTTTGGTGATTCCAGAACCCATTACTAAATA", //
			"GGCGCCCGTCACCCGCATCCCTCACGCCTCCACGTCCCATCTCCCCCTCATGCAAACCCCACCCTGCCCACTAATAATTCCCACTGCAGAATCAGACCGAAACCTGGAGAGACAGCATTGCCTTTCTGCTCCCCTCGCCCCCCTCCCCTCCTGCTATAAATAACCCGGACTAGCGGGTCAGGAACGTCACACGGCGAGAAAACAGGACCCCGAGGTTTTCTTCTCTGGGAATAGGGGGCAAAGGGTGAGGAGAGGAGAAAGAAATCGCTCGTGAGTAGTGAAGAAAAGCTTGCTCTTTGACTACTAGAAGAGACCAGCCATTATTTTATTCTTTTCTTTCTCTTTCTTCCTTTTTTATTTTTCCGTGCCTTTCTTATTCTGTACCCGCCTCCCCAACCCCCCGCCCCAAATCCAACGGGTTGCTCAGCATTCCTCCCATTGGTTTTTTCCCTCCCTCCCGCTTCGACTCCCAACGCCCCCTCCCCCCTCCTCCCACCCGCTGCACAACCCCCGCCCCCCCCATCCCAGTCTCGCCCCAGGCAGGGCATGCTATTTGCCAAGGAAATAAAGTTCAAACTCCTTGCCCCGTGTCCGGCTGAGACCAGAAGTGGGCTTACCGGCACCTATCTTCCGCTGGCACGCCACACCTGCTCTCCTCTAATTTTCTAATCTGGTTTTGCATATTTAAAAGGACACACACACACACCAAAAACCCACAACCCGCTCGGTGCAAAACTGCCTCTTGCAACTACCGAAACTTACCAGACGCTGTAGTTATTTTGTCTTAACGAAAGGATAACAACAAAAATTTTATTCTGGATGCTGCTGTGGGGTGACTTTTCTATATTATTCAAATATGTATGTATTATACATATACATGTTTTTCCTTTTTCCCTTTTAATTTTTTTTTTTTTTTTTAAACAAGGAAATCTGCTCGGTCCCCGGCAGCCGCCGCTTCCCCTTTGATGTTTTGGTACGCCGTGCGCATGCGCCTCACATTAGAATTACTGCACTGGGCAGACTAAGTTGGATCTCCTCTCTTCAGTGAAACCCTCAATTCCATCAAAAACTAAAGGGATGTGGAGAGTGCGGAAAAGGGGCTACTTTGGGATTTGGTCCTTCCCCTTAATAATCGCCGCTGTCTGTGCGCAGAGGTAAGTGTGCCCAGCGTTTCTCTTTTCCGTTCTCACTTTGAAGGGTT", //
			"CGTCGATAGCACCTTAGAGTATAACAGTAGTCTCAAAACATCTGTAACAGAGGAAGTAGTGAGCAGGTGGGGCTCTTTGCAAAAGTGGTGTAGTATTAAGAGGGTATTGTTTGAATAGAGATAGACTTGTCTATTATGGTTTCCTCAAGCTAGCTAAACAGGTCACTTAGCTTCTTCAAGCTGCAGTTTCTCATTTGTACAATATTGTGACCCTTAAAGGTAATAAGATAATCAAGATAGAGATCTTTGTGCAATGCTTGGTACTGATATTTCCATAAGTAGCAGCTGTTATCATTATAGCATCATCATTATCATTATCAAATTACTGCTGGAATACTGTGCTCAGTTAGAGTTACATTTTAAGAGGGATATGTAGCAATCAGAACAACCAAAATATGAATGATATATAAACCATGGAGTCTGGAAAGCCAGTAGAAGGAACTGGAGTGTTGGACGGAACATTTACCCTGAGGAAGGAACACTGAGGCTAGGAAGGATGGCAGTGGTTGAACAGGATAGCTATTTTCAAATAAATGCAGAATTTCCTCTTAAGGAAAGAGAGTAGACTCTGTGTACCCTAAGGACAAAATTAACTAATGGTAAAAGATGCCAAAAGGCAATTTTTGGCTGAAAGTTAAAAAAAGAAAAGTTCATAGCAGTTACAGCTGGTGAAAAGTCAGATGGGCTGCCGTATTCTGTAAAACCATTTGCTGGGAATAAAGGAATTATTTCACTGAGCAAGTGATGGGATTACATGATCTCTAAGTAGCTGTAGGTTAAGGGTTACTTGGCACAAGCAAAAATATGTAGGACTTGAGTTAAAGGACAACTAGGCAGGCCCTGGTTTATGTGAAGATGCAGATCCCATATGCATATACCTCTGAGAGCAATGCTGTTTAGGAAAAGATGAAAATCACATCCCTGCTAAAATTTAAAGTAGAACAGCGTATGTGTTTTGCTGATGGTCTGTTGTTGTTTAGCTAGCAATCTTTCTTCTCTCAGATTTTCCTCATAGGAATACATCTCACACTTGTGCCTATGAGGAAAAAGGCTAAAACTCTAATATTTTTCTGTCTTTCTGAATAATCTAAACCTTCTTGAATATTTTGATCTTCTTAAGGCTCAACAGAAGAGAAGGTATGTGTGCATGTGTCTGTTTATGTGTGTGTGTGAAAGGGGGGTGTAAAGTGGAGGTGTCACA", //
			"CTAACCAGATGAATAAGGGACACAAGAGACATAACACAAATCAGAGAAGTTAAAACCAAGCTTATTCCTAAGTGCAACTAGCCTAAGCAAGCTCATGCCTGCCTGCTGAAGCCCCAAGTCTTCTACCTTGATAAAGGTTACTCCCTCCATTTTAACTGCTCCTGCCTCCCCTATCTTTTTTCGAACACCATGGCTGATTTATCTAAACCCTATGCTGCTTCACAGTATTTTGAAAACAAATTAGAAATCTCCTTCCAGTTTTCCAGAGGTGATTAAATCCAAGACGCCTTAGGAATGTAGAGGATAGAATAATATAAACAACAGATCTGAGATAACCATGGCCGGCAGCAGTTGTGGTTAGTTGCTTCTTTTCCTTTTTCATCCATCAGTGATGGAAGCTGGATTGGGTCCACTGTATAAAGTTCGGTGGGCATAAATCTGCTATTTGATCCTGGAAAAACTCTAAGGATCTAGAGTCAGCGAGGAATTGCAGCAATGCTGGTGTTTCAAAGGGCCTAAGAAAAGCCAGGAGTCCTGGCTGCCCAGCCCCGACACTGTCTTACAGGCTGCAGGTTTACCTTGGTTAGCACAACAGGCCAAGCTCTCCCTTGCTCTAAGTCGACATGACATGATTTGTTCCCTTTCGGCTGTGTCAGTTCTGCAAACCAGATACCAGAGGCTGCAGGCTCAGGAGAGCCTGCCTGTCTGGAACACCGTTTGTTTGGACAGGACAGGCCACTAAGGCGTTTCATCTCTTTAAAGCTCCCAGGGCAGAGAACATATTTGAGGAATTCCTTTTTTCAAAGATTTTTTTTTTTTGCCCCTTCCAACATTTTGTTTAAGAGAAAATGAGATGTCTATCTTCTGTTTCCTTATTTTTTTTCAGGAAAGCAAAGGGTTGTTGACTTCCTCCCTCTCCCCTCCCTTCCTTCCTTCTTCCCGAAGGTACTCATCTGAGAAAAAGAGAACGAAGTATTGTTCCTGTTCTCTTTCTTAAGTCATTCGAAACACTGTCCCTGCGAGTTCTTTAAGTTCCCTGCAATCTGTACACAAGAGAAAGAGGGAGAGAGAGAGGGAGAGAGACAGAGAGAGCAGGGATCAGGTAAAGGAGTGGGGCTGCTGCAGCCATTCAGACCTCAGGAGCTGGTAAGTTACCCATGCAATCATTTCTGCTTCCTTTTCAAGGTTCCTGGGTGGATAC", //
			"GCTCCCTCCTGGGCGACCTTTCCACGTTTCTTTCGCCCCCCCCGGAGCGGCACCTCCATACCTCCCGGGCCGCTAGTCCCCCGAAGCAGGCCCCAGCCCCGGCGCAAGCTGCCTCCTCCGCAGTCCCCGCCCGCCGCCAGCCCGGCCACCGCCGGACGGCGCCCTTCCCAGCCCCGCGCCCCGCTCCCCAGCCCTCCTCGCGCGTCCGAGCCCACGTCGCTTGCCCCGGCTGCCAAGTCTTCCAGACGGCCGGGCCCTCCCGGCACAGCGCGGGAACCCCGGCCCCCGGCCCCACGGGACGAGGGCTACAGGGCCCGGCCGGCGAGCCAGGGCACCCCGGGGTCTCCAGGCGGCCCACCGCCCTCACCCCCCACCGGCGTCCCCAAGCCCCCAGACCGTCCCCGCCGCGCCCTCAGCGCCGCCGCCCCCGGGCCGGCCGAGGCTCCGCGGCGAGTCCCGCCCAGCGCCCCGGAGGCGCCAGCCCCACGGCCCGAGCGTGCGCAGCGCCCCCCGCGGCCGCGCCAAGCGCAGGCGACGGCACAGGAAAGGAGGCCGCGGCGCGCCCGGCCCGGCCCCCTCCCCAGCCCGCCCCCGGGGCCGCTGGCGGTGTCCGCGCCTCCGCGCCCGCCCCTGATTTCCTCCGGGCGGCGGCGGCGGCGGCGGCGCCTCGTGCGCGCGGTTATTTATTTCCGGGCCCCAGAGCGCTTATAATGGAGCCGCTGTCAGCAGAACCTTCTGCCGCCGCCGCCGCCGCCGCCGTCCCTCCTCTTTTTTTTCCCGGCAGATCTTTGTTGTGTGGGAGGGCAGCAGGGATGGACTTGAGCTTGCGGATCCCCTGCTAGAGCAGCCGCGCTCGGAGAAGGCGCCGCAGCCGCGAGGAGGAGCCGCCGCCGCCGCGCCCGAGGCCCCGCCGCCCGCGGCCTCTGTCGGCCCGCGCCCCGCTCGCCCCGTCGCCCCGTCGCCCCTCGCCTCCCCGCAGAGTCCCCTCGCGGCAGCAGATGTGTGTGGGGTCAGCCCACGGCGGGGACTATGGTGAAATTCCCGGCGCTCACGCACTACTGGCCCCTGATCCGGTTCTTGGTGCCCCTGGGCATCACCAACATAGCCATCGACTTCGGGGAGCAGGTAAGCCCCGGCCCCGCGCCCCACGCCCGCTCGCCCTGCCTTGGCCTTACACGCCCGGACACGGGGGCGGAGGGGA", //
			"TTGAGTAACAAACTCAAACATTTTATGAAAAATAAACAGACTTTATGCTATGGTATGGTTCCATTTATGTTTATATAATAGACTAAGTATTACTCATTACAAGTACAATAAATATTTGTTGATCCGGTTGATGTAACCGTAAGCCTTGTTCTCTCTCAGTCCAGTGAAAGTATACATTTTACTGATAATCTAAAAGTATTAATTCCTATACACCAAAAGATCTGCGTATCATCCCCCTCTTGGAATACACATTCACTTGCACATATCCCCAGCCTGCTCCTCGCAACAGAGCTTGCTTTGGGGAGGAAATTTCAGGATCACCTCCAGAAGAAAACACCGACAGCGCTCCTAGAGCCATCGTGGTGCCCTCTAGCAACTTACGGGCTCCAAGCGAAAAAAGAGCTCGGGTTGGTGCGGGGGGGCGGGGAGCGGGAAACTCCATCACAGACGCAAGTGAAAACTCCTGCGGCTTCTAGATTCCATCCGAGAAGGAAAGATGAAAATCAAGGTTAGAAAGCAGTGGCAGGGCGGTGTCCCCTGCCTTGCTTTAATTCACGCGGCTCTCGTAACAAGCCACAGATGTGCATCACTCAAGCGGGCTTCCCAGTCCTCATCTCCCCTCTGGATGGTCTTCTGCGACCGCAGAGTGGTAGTTAGGGAAGATCGCAGCGGGCAGAAACTCCGCCAGGGGAGAAAGGAGCCTGGGGGCGACGGTCCGCCTAATCGTTCCAGGAAGCCCCTCCGCGTGCCGGTGGGCGGGCGCTCAGGCCCGAGGATGCGGGGCGAGGAGGCGGGTACTGGTGATTCTCCTGGCAGCCCCCGCCCGCCCCGCCAGGGGGAGCGTCTGCGCCCGACGTAGGCTGCGGGGCCTGGGCTGGCGGCGCAGGGCGGTGACTCGGCTCGGTGGCTGCCCTCCCGCGTCCCCACCCCCTCCCCGCCCGCCCCCGGCGCGGCCCTGCCCGCCCCCTCCGCCCCCTCCCGCAGCGCCATGCGCAGACTCAGTTCCTGGAGAAAGATGGCGACAGCCGAGAAGCAGAAACACGACGGGCGGGTGAAGATCGGCCACTACATTCTGGGTGACACGCTGGGGGTCGGCACCTTCGGCAAAGTGAAGGGTGAGAACCCCGCGGGACTAGGCTTGGCCCCAGCTGAGGAGCCGCACGACTTCCGCTTTCCGCACCTCTTCATCCTCCCCCGCCCG", //
			"ACCATTGTTCTTTTCTTGTTTTTTTTTTCGTCTCTGGATTTTATTTAGTTTTAGCAAAGTGACATAGTTCAGGTCAAACATATTCTTAAATATACGAAGTTAGGGAATTAGAATAGGAAATCATTCAGGGCATGGCCTTTCCAGTCACACAGAACTGGGTCATAATCCTGGTTCTGTCACTTACTGTCTTTGTGACCTTGAGCATGTTCCTTGATTTTTTTTTTAACCTCAATTCCTTCATTTAAAACAAAGAGATATTATCTTCATATAATTGTTTCGATGATGAGGGGAGATAGTGTATATGAAGTGTTTAGCATAAAACTTAGCATATAGTAAGTACTCAGTAGTTGTCTACTATTTTTCTTGTTGTTACATTATCTTCACTTAACATTATATCAAAAGAGTATTATAATGAAGCTACCTAATATGTTCTCTTTCTAAGCTGGAAATAAGCTGTTCTTTTCATTGTTTGTCGCTGTCATTTGTCCCATAACTTTTTTCCCCCCATTGCTGTGTCAATACATTCTCCCTCTTAAAACTGGAGGGAAATTGGAGCATTTCCCTCAACATACACAATGAAGTGCGAGCTAGAAGAGGAAAATTTCAGTGCAAAAAGTGACAATAATACAGTATGAATATAAATATAAATTAAGTCATATTCTAAATTCCCTAAACTGTTACTTTCAGTCTCAGCTCTATTCAAAGTCCTTCTCCTGATCCTGCCATTTTTCTCTCAGGGAACATGAATGGTTTAAACAGGACCTTCCAAAATATCTCTTTCCTGAGGATCCATCATATAGTTCAACCATGATTGATGATGAAGCCTTAAAAGAAGTATGTGAAAAGTTTGAGTGCTCAGAAGAGGAAGTTCTCAGCTGTCTTTACAACAGAAATCACCAGGATCCTTTGGCAGTTGCCTACCATCTCATAATAGATAACAGGAGAATAATGAATGAAGCCAAAGATTTCTATTTGGCGACAAGCCCACCTGATTCTTTTCTTGATGATCATCACCTGACTCGGCCCCATCCTGAAAGAGTACCATTCTTGGTTGCTGAAACACCAAGGGCACGCCATACCCTTGATGAATTAAATCCACAGAAATCCAAACACCAAGGTGTAAGGAAAGCAAAATGGCATTTAGGAATTAGAAGTCAAAGTCGACCAAATGATATTATGGCAGAAGTATGTAGAGCAATCA", //
			"TTACACTATATGCAATTATCTTAAATGTTTCCTTGATTATTATATGTTTCTTCCAATTACGTTTGAATTCCAAGAGAGCTGGGTCTTTAACTATATTCCCCACTTCTGGAACATACAAGATGCTCAATAAATATTTGTAGAATGAAAAGAACTGTAAAATTTCAGCAGATGGATCAAATGAAACTTAAGGTTGCTTGTAGCTTTAACATGAGCATTGATAAAAATGAGTAAGTAGAGAAGGAAGTGATGACAACCATTGCAACAAAGCAATGGAAGCAATGTGCATATTTATCCTTAAAAAACACAAACCTGTCATATGTATGTGTATGTGTGCGTGTATATATAATGTTGGCTTTTTAGTTTCCCGTGGAGTGTGAACTGAGTTTCTGAGGTTTATAGTAAAGCATTTAACAAGGATTTCAGGTAGTTGTCTAAATATAAAACATTGTATCATAGAATTAGAGCGGAAAAGGATTTAGAATACAAGTTCTTCAATTTTACAGGGGTGGAAACTAAGGCCTAGAGATCTAAGTGTACTTGGGCATTGGGAAATAAATACAGCGATGTGTCTGTCGATGAGGAATGCGTATGCCAATCATACTTCTGTATAATCATTACTAATAACATAGTAATACTGCCTTACATTTGAAAAGTGTTTGAGTGTGACTAGACCCTGCACCAGTGGTTCTGAAACTTGAGCGTGATCAGAATCACCTAGAGGATTTGTAAAAGCACAGATCGCTGGGCCCCACCTCCAAAATTTCTGGCTCGGTAGGTCTGGGGCCGGGGACTGGGAAAGCTTAAGAGAAACCATCCCAACTCCTCTCAGCTCTCCCAACTCAACCTCTCAGCTCTCCCAACTCAACCGGCCGAGCTCGGCACTCAGGGAAAACTACAAGTCCCGGCGTGCCCCGCGCGCAGCGGCGGAAAGTCGGACACTTCCGCTCCACGCAGCGCCTGTGCTGCCGCGTCTCGACGTGTCACCGGCGGCGCCGCTGCTGTGGCAAAGGAAGGAGCTGACTGGGGGAGTTCGAGGCGGCGGGCGGCGGTGACCCCGGCCTGGAACTGCCCCGGTACGGAAGTGTTCCGGGGTCCGTGGGGAGCAGGAGAGGGAGGCGGCGGACCGTCCCGCGCGGGGCACGATGTTGAACATGTGGAAGGTGCGCGAGCTGGTGGACAAAGCGTGAGTATCGGGGGGCAG", //
			"AAGCCATGTGTGATCATGTGCAATAGTGTGTGAGAGAGAAGCTGTCATGTGTAATAATGTGTGAGAGAAGCCATGTATGATCATGTGTAATAACGTGAGAGAAGCCATGTGTGACCATGTGTAATAACGTGTGTGAGAGAAGCCATGTGTGTGGTCATGTGTAATAAAATGCATGAGAAGCCATGTGTGATCATGTGTAATAGCATGTGAGAGAGAAGCCATGTATGATCATGTGTAATAACGCATGTGAGAGAAGCCATGTGTGACCATGTGTAATGTGTGTGAGAGAAGCCATGTGTGATCATGTGTAATAGCATGTGTGTGAGAAGCCATGTGTATGATCATGTGTAATAACATGCATGAGAGAAGCCGTGTGTGATCATGTGTAATAGCATGTATGTGAGAAACCATGTGTGATCATGTGTAATGTGTGTGAGAGAAGCCATGTGTGTGATCATGTGTAATAGCATGTGTGTGAGAAGCCGTGTGTGATCATGTGTAATAGCATGTGTGTGTGAGAAGCCAGGTGTGATCACGTTTAATAGCGTGTGTGTGAGAGAAGCCATGTGTGTGATCATGTGTAATAATGTGTGTGTGAGAGAAGCCACGTGTGTGATCATGTGTAATAGCATGTGTGTGTGAGAGAGAAACCATGTGTGTGATCATGTGTAATACTGTCTGAGAGAGCCCATGTGTGTGATCACGTGTAATAACGTGTGTGTGAGAGGGAGAGAAGCCATGTGTGTGATCACGTGTAATTGTGTGTGTGTGTGAGAGAGAGAGAAAGAGAGAGGGAACAGTACAAAGGAGGATCCCTTTAGAAAAGTATTCCAACGTCTTATGGAGCCAGAGGGCTGATTCACAGCTGCTAATCTCTGTCATGCTTTCCAGCACCACCCCCTCCCTCTCATCATAACTTGTGATCTTAGAGGCAGGAAAGAATTTGAGCCCCTCCCCCAGCCTGACCATCTCTGTTGGTTGCTCGGCCAGGCTCCACGTCAAATCCAGTTTGAAACACAGACCCTAGGACCACGCAGGAGGTGGTGGGCTCGCAGGAAGGTTCCTCTCCCAGTGGCCATGGGTAGCAACAGTGGGCAGGCTGGCCGCCACATCTATAAATCCCTAGCTGATGATGGCCCCTTTGACTCTGTGGAGCCGCCTAAAAGACCCACCAGCAGACTCATCATGCACAGCATGGCCA", //
			"TAAACCCTCAAAAATCCATAAGTCTATCAATAGCTATCAATTCTATCTATAGCTACTGTAAACGTTGTGGTACATCTCTTTCAATATTTAAGTGCCTTGATATATTTATTTTCACGGTTGAGATCAGAATTTGTGGTTTTGCAACCCGAATTTTCCATTTAAAAATATAATAAGAACTTACTTCCCCAGTCATTAATCTTCCTAAACATAATTCTTAAGTTTGTAAAAAATCCATCCTACAGATACACCTAAAAGTCAGTTTATTTACTTTGTTTCCCCAGGAAAGAATCCTACATGACCATGGTTCTGGGCCTGTGTAAGTGCTGCCTTGTGAACAGTGTAGATAAGATCACAGAATCATCCTCCAAATAAAGTACTTAAAGCCAATGTAAAAGTTTTTCGAAGGCAAAGGGGCACTGTGAATCCAGCATTTTCCAGTGTTCCCTAGACGTGAATCATCTCAAAGAGAGGCCATTATTCGAGTGCAACAGGGCTCCACGCCTCCAGCGATGTCAAAGTTCTGCTAAGCGTCCAAAACCTAGCAAAAGGTTCCAATGGGCCATTGGAACTCGGCGGCCGCGGCGGAGGCTGCTCCCAAGGCGGCAGCAATCACACCGGGACCGGCTGCAGAGTTCCCAGTCCCGCGGAGCGCTAGCTGCCGGGCGGACGCGGAAGCCCGAGAGTCCTGGTCTTCCCGTCCAAGTTTAATCCGCGCCTACGCGGTAAATTCCTGGAGGTGCTCCCAGCCTAGCCCGCTGCGCCCGGCTCCGCGCCTGGGCAACCCGGGCTCCCTCCCGTCCCTGGCGCCCTCCGCACCCGCCTAAGGCGCGGCGGTCGAGCCCTGGCTGCGCCCTGTGGGCGCGCTCCTCCGAATCCCAAGTTCGGGTCGCGAGTCTGAACTTGGGAGGAACGCGCCCCGGGTCCCCGCCCTTCCCGGACTGCCCGATCCCCATCCCCCGCAACCGGGAGCGCGCAGGCGCAGCCGGCCGGCAGTCCCGTCAGCTGTCCCAGAGCCTGTGTCGCGCCCGTGCCGGTAGCGCCCGTGCCGGTAGCGCCGCTGCCACCGCTCACCATGGGCCCGGGTCCTCGGCTGCTGCTACCTCTCGTGCTTTGCGTGGGGCTCGGCGCACTTGTGTTTTCTTCGGGGGCCGAGGGCTTCCGCAAGCGAGGCCCCTCGGTGACGGCCAAGGTGACCGAGAGC", //
			"TATTGAAAATCTACTCTTGCTTTTTGGCGAGGATGAAAGGGCTTTGGGGGGCGGGGGTGGGGGGATGGAAAATGTGAATGAAACGAAAAGCAGGGAAGGAGGTTGAGACAGTCATGGTCATGGGCTTCATTATTAAACAGGAAAATTCTAGAGACAAACTGAAAAGAGAACGGAAGTCTGTAAGAGGGTTGCCAAGTCAGAAAGCAAAAGCAAAGTGTCGCTTTATTAAAAAGGTGGGGGCGGGTCCTGATTGAGCTGAGAACAAAACATCTGGGGGAGTTCTAAAGCTAGGAGGCCAGGCCTGCAGGGAGGGGGTCGGTCATCCCGGGTGTCCCGCTCACGGGTGCGGCTTCCGTCGACACTGTCCGTCGACACTGTGCAGGTAAACACTGCTGAACGGGTTTTGTTTTTGCTTTTTTTTTTTTTTTTTTGGCTCCCAGACTGGAGTGCAGTGGCGCGATCTCGGCTCACTGCAACCTCCTTCTCCCGGGTTCAAGCGATTCTCCTGCCTCAGCCCCCCCGAGAAGCTGGGATTACAGGCGCGCACAACCAAGCTCGGCGTATTATTTTATTTATTTGCTTGTTTATTTTTGAGACTGGGTCTCCCTATGTTACGCCGGCTGGTGCCACTATTTTCAAAGATGTAACATACTATTTTTACAAGGATTGCTTTCTTGTTTAGTCTAGATTTTACCGATGGAAAGAAAGGACGGAAAAGAGCGCGTGCCGCATTTTGCTCCCCTTTGCCCACCAGGCTCCATCACCTCCGGAGCGCATCGCGAGACCGTCTGCGTTACCCGCGTCCCCGCGCCCGGAGCCGCCCAGGAGGGGGCCCGGACCCTGTGGTGCGGAGCGCGCACTGCTCGGGTGCACCGGACGCCCCGCGGCGGGCTGGGAGGGACGGGGGGAGGGGGGCGCGGGGAGCGGCCTGAATCCCGGGCCCGCCTCCGGCCCCCGCCCCGCCCCGCCCCGCCCCAGGCCAGCCGGCGCCCGCGCGGACACTTTCAGCCCCGAGCCGCGGCCGCTCGGGTCGGACCCACGCGCAGCGGCCGGAGATGCAGCGGGGCGCCGCGCTGTGCCTGCGACTGTGGCTCTGCCTGGGACTCCTGGACGGTGAGCGCGGCGAACGGGCCACCCGCCCGAGCGTGGGGCTGGCCGGGGAGGGGGTACCGCGCACCTGGGGCGGGCGCTGAGACCGCTC", //
			"TCAGTGAGCTGCCCCACTCCTCCCTGTCCCCTCCCCGCAGCCGGCCCACGCAGACATCAAGACGGGCTACCTGTCCATCATCATGGACCCCGGGGAGGTGCCTCTGGAGGAGCAATGCGAATACCTGTCCTACGATGCCAGCCAGTGGGAATTCCCCCGAGAGCGGCTGCACCTGGGTGAGGCCAGCACCAGCCTGCCCAACCCACACCTCCTGCCCGACCCACCTGCTGCAGGCCGGAGTGCCTGAATCCCTCCATTTTCAAAACTTGCACCAGGAAGTTCCGCCTGGCCTGACCTGTCCCTGTGGGCGTGGGTACCTAGAACCTCAGCCAGGGTCCCCAGTTCCCAGGCTCCCTGAGGTCACCTGCTGCCACCCTGAGCAGGGTGTATCTGTCACACCTACCCGGGAGGATGCGCGGCCTCCTGTCCACCTCTCCTTGTCTTCCCGCTGTGGCTCCCCACGCTCCTCCTGCTGCCAGGCCTGTCCTCCTGGACCTCTGCACGGGGCTCCCCCGCCGCCCACCTGTCTCCACGCTCACCCCGTACAGCTCACCTGCAACTGGCCCCCTGCCCCGACAGGGAGAGTGCTCGGCTACGGCGCCTTCGGGAAGGTGGTGGAAGCCTCCGCTTTCGGCATCCACAAGGGCAGCAGCTGTGACACCGTGGCCGTGAAAATGCTGAAAGGTGTGGGGTCAGCGGGCGGAAGGGGCAGCCTAGCTGGTGAGGAGCAGCAGCTCGGGGTGGGCCGGTGGGACGGCAACTCGGGAAGGGAGACTGAGGCTGGAAGCCAGTCTCATCCGAGATTCTACAGGGAGGCTGCGGGCTTCAGAGCCGAGGGACCACCCGAACCCCCTCTGCCTCAGTGGGGCCGGCAGGATGTCCTGGGGGCCCTTAGTAACCCTCCCCCTCCCAAAAGCGCCGGGTGGCGGGGAACGGGGACCTGCCAGGGTCGGGGCCGGTGTGAGGCCCGTGTCCCCTCCAGAGGGCGCCACGGCCAGCGAGCACCGCGCGCTGATGTCGGAGCTCAAGATCCTCATTCACATCGGCAACCACCTCAACGTGGTCAACCTCCTCGGGGCGTGCACCAAGCCGCAGGGTACGGAGCGGGCGGCCCGCTGGGACGGCGAGACCCCGGGCGCGGGGCGCCGCGGGCGGGAGGGGGCGCGGGTGCAAACGCGGAGCGCGCCCGCAGCGCGCCCCG", //
			"CAGGGAGATACCTTCAACCCAGGCAGAGGACCCTGTTAGGGGGTGCCAGAGCATCTGGCCTCAGCCCTCTCTTCCCATCCACCACGGGACACGCTTCCCTCCGTCTCCCCATCCACCGCAGGACACGCTTCCCTCCGTCTCCCCATCCACCACGGGACACGCTTCCCTCCGTCTCCCCATCCACCACGGGACAAGCTTCCCTCTGTCTCCCCAGGCCCGGCTGCCCCTGAAGTGGATGGCCCCTGAAAGCATCTTCGACAAGGTGTACACCACGCAGAGTGACGTGTGGTCCTTTGGGGTGCTTCTCTGGGAGATCTTCTCTCTGGGTGAGTGCAGGATGGGGTGCCGGTGGGGAGAGGAGGCGAGGTCACTGGCGGCAGGTTACATGACCGCCCCTGCTGGAGTAAGGGACTGGGCACCTGGAGGTGTGTGCCAGGTCGTTCTGAAGCAGGTGGTTTGTGGGCCACCAAGGGCCCCCCACCTCTCCCCAGCCTGGCCCAGCCTCCGAGAGGACTTTGCCTCCAACGTGGGCCACCCTCTGTGGGGTGCACATTGAAGCCTGAGCCAGGACTGAGCCTTTTCACATGGCAGCCCCAGAGGGAGCTCAGTGCCCAGTCAGCAGACGTGCCTACTCCAAGAGCTCCAAGAGCTCCATGTGGGAACTCAGGCCCCGCTGCGAGCATGATGAAGTCTCCAGGCCAGGGTAGGGCAGTGAACTTGAAAAAGTTAAAATGTCACCAAGTGGCAACTGATCCACGCTACTGCGGAAACTAAAGGAGCAGGAAGAGTGTAGGAGGCGTGGGAGCTCGGGTAAGCCAGAGGGGCCCAAGGAGGGAGTTCAGCAGTCCAGGACGGGGAGCAGCAGGGGTTGGGAGGAGCAGCAGGGGTTGGGGGGAGCAGCAGGGGTTGGGGGGAGCAGCAGGAGAAGCAGCGAGAAGCAAGAGAGGCTTTGTTGAAACTGCCTGCAGGCGGCTCCCCCGCCGCTGATGATCCAGTAACCATGATGTCTACCGTGTCGCCAGTGGGTGGATGCTCTTGGCACCCCTAGCAAGCAGGCGGCACAGTCCTCTTTTAACCAAATGAACTAGAGCTGGGGAGGGCAGCGACTTGCCCACAGGTCCCAGGTAGAGCACAGTCGGGTGAGCCATCTCCACCCCACCACGGATGGGGAAAGGGACAGGACGGGGTGACTTGATGGGGA", //
			"GCCTGGCCAGAAATTCTGTTTTTTGAGATAGGATCTCACTCAGTTGCCCAGGCTGGAGTGCAGTGGCACAATCATGGCCCATTGAAGCCTTAACTCCCCACACTCAAGTGATCCTCTCAACTCAGCCTCCTGAGTAGCTGGGACTATAGGTGTGTGCCACCATGCCCAGCTAATTTTTGTATTTTTTGGGTAGAGACTAGGGTTTCACCATATTGCCCAGGCTGGCCTTAAGCTCCTGGGCTCAAGCGATCCACCCACCTCAGCCTCCCAAAGTGCTGGAATTACAGGCTTGAGTCACCAAGCCTGGCCAAAAGTGACATTCTTAGGAACACCTTTCAATCTGAATTATATAATAAAAGGATTATAAAATAATCATTTTGATTTTTTTTTTTTTTTGAGTCTTGCTCTGTCGCCCAGGCTGGAGTGCAGTGGCGCGATCTCGGCTCACTGCAAGCTCTGCCTCCCTGGTTCACGCCCTTCTCCTGCCTCAGCCTCCCGAGTAGCTGGGCCTACAGGTGCCCGCCACCACGCCCTGCTAATTTTGTTTTTGTATTTTCAGTAGAGACGGGGTTTCACTGTGTTAGCCAGGATGGTGTTGATCTCCTGACCTCATGATGCGCCCACCTCAGCCTCCCAAAGTGCTGGCATTACAGGCGTGAGCCACCGTGCCCGGCCATCATTTTGATTTTATAAAAACCCAGACCTTAAACTTTGTTACTCATGCCTGTGATTAAAAGTTTGCCATGCCTCAGGTTTTTTTTTTTTTTTTTAATTATAGCACAGCAGGAGGGTGTTGGGAGGTGCCTGGAAACCCTGGCTGACTTGTCAGGCCATCAGCCTGCTGCCTTGACTGCGATACTGGATGGAATGGGTCCAAGGTCTCCATGTTTCTTGGTGTTTCTACTTTCTTTATCTTCGACGCTGGTTCTTTATCTCTAAGGACACAGATAATAGGCAGCCAAGTTTGCCTGAACAGACTTTTTTGGCAGTTTTCAACCTTGTTTTCTGGACGGGAAGAAGAGGGTGGTAAAAAGCCTACCATACTTGCCGACTGAAAGGGACATCTACAGGGCTGAGTGTTTGGGTGAGTCATTTTCCTTTGGTCCTACCACATTTAGGTAAAATGAAGCTTTTTGGTATAGTGATATAGTGGGGAATGTGCAGGTTTTAGAGTAAAATAGACCAGGGTTTGAATTTCCTC", //
			"AAAAAAAGCTGGGCATGGTGGCACATGCCTGTAGTCCCAGATACTTGGGAGGCTGAGGTGGGAGGATTGCTTGAGCCTGGGATACTGAGGCTGCAGTGAGCTATAATTGGGCCACTGGACCCAGACCCTGACTCAAAGGGGAAAAAAAAGGTGCTGAGAAGCAACTTGGCTGGTCAAATGATAGGAAGGCTCAAAAGCCTAGAGCCTGTAAGGAACAGGTAGGATATGCAAAATGGTGAGACCCTCCTGCCTAGATTTCTGGTTAAAAAGGCAGCAGAAAGTTCATGATTTTGTGAAGGTCATAATCTCACTGAATCTCAGAGCAGTTTTTTCTTTCCCCTATATGGATACAGGCAGAGGGAAGAAGAGAAAGGGATATTTATTCTTTGTTATTTATTTATTTTCAACAATAAAAGTTCTGATTGTGGAAAAAATATCTAGCTGATTATAGGAATTTAATAAAGAAACCAAAACAAATTAGTCTTATAGCTGATGATAGTATTGGCCTACATTATTGTCATACACTGAATTCTGAGTTATCAGGCCAAATATGTGGACTTGGCAAAGATGAGACTTACTGTTAATTGTTCCAGCTCTCCAAACTGGATGTGACTGGCACCAGTGGCTATATGAGTGAATATAGAAGCTGAAATCCACATTATGTTAACTTTGGTTCTATTCCACCAAAATATTTTATTCCAGAGATATGTAAGATGAAGCTTTTCCAGGGCAGAGATCATGATCATATCGCCTAGCACAGTGCCTGACCCATAATAGCTGCCCAAGAAATATGTGTTGAATAAATAAATGGCACAAAGGTTATTCTGGGTTGCTAAGATCAGAACAGTATTTGCCATTATTTTGGAGGTTGGCGTGGGATTTGTACACCCTGGAACAGTTTCTTCTCTTTGCAGCTAGTGATATTTTTCTGTTTATGAAAAACACACATTCTTGCACACTGGCACAATGGTCCTGGGCAAAGTGGCACAGAAAGATTTCCACTCCATTTCTTGGAAGAGCCAGCAATACATCTCTCTAATGGTGAGTTGCTTCTCTTTTACCAGAGGATTTAAATTTGATGTGTTTTTAGTTCTTCCTTTCTTCCCAGTAGAGGAGACTACCTCAAGGAGAAAATTCATTAGCTGAGTTTGACTGAATTTGATAGCATTTTACTGCTCTGTGAGAATCAAGAGTAGTCACA", //
			"GACACTGGACCCCAGCCTGAGTGACAGAGCAAGACCTTGTCTCACCAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGGCAAGGGAATCCTTCCCTCTGGGAGCTTACATCTTAGGTTTCATATTCAGAGCAAAGTATTTATTACAGTCAGTTATGTCAACAAGAGCCTGTGTTGCATATGTGGGGCAGGCGGAGGAGGTTTTTAACTCTCTTCGGATTTACTCTCAAGACTTTTACGCACTTATGATAAAGCTTTTGAAGCAATCATATTGCTCCTGTTCTCCAAGCATGGAATCACAGGTAAAGCTTTCATTTAAGATATTTAAAGAGAGATACAAGGCTAAATGCACAGACTGCATCTTCACTGGGCTGCTCTGAAATCAGGCTCTGTTGATCCTGTTTGGCAGATTTGGAAACTTTTATTTCACTTCGCTCAAGTTTAAATAGGCTCTGAGAAATTAATTCGGATAAAACAATTCAACCCAGTGAACAGTGTCCGATTTGTAAACCATGATATGAATTTATTGACAAAAACTGTACAGCTAGTATAACTAGTCCTAGTTATATGCCTTGGACAATCCCTTCTTATTGGGTATGTAGATTTATTTGAGAGACTCCCTGAATGATGGAAAGGCTACTAGCTGAGTTTGGAATCAGAGTATGGGCCTTTGTATGCTCTTAACGGAGATCCAGAATTTAGGTTTGCTAGGATATAAACTCCACGAAGGCAGGGATTTACTGACGTATCTTAAGAGACTAGAACATAGTAAGCATTCACTAAGTATCTGTTGAATGAGTGAGTGAATCTGAATTAAGATTTAACTGCCCTGAGAGTAAGTGTTGGTTCTGGTCATTAGAATCAGTTGCCTCGTTCCTGGACAGAGTTCCTGGGAGCCTCATAGAACATTCTTATTCAGATTCTTCTGGTTTTAGACTCTTTGTGGACCCCACCCTTCGGTTATGACAGCATTAACTGGGTATGTGACACTTAGGTTACACATGGTTTACTCTCTCTGATGAAATGTGGGAGACACTGTGGCCTCTGTAGCCCATTTACAGATCATTTCATAGGGTAACTTTATTTTTTTCAAATCTTGTCCCAAGTTCTATGAGGGTTCTTGTTTTACATTTTCCAAGTTACTAACTTCCAGCTCAGGTAGGTAAGGGACAACTGAATTGTGGTGGCTGCTATTTTTTGTG", //
			"TTACACATGGTTTACTCTCTCTGATGAAATGTGGGAGACACTGTGGCCTCTGTAGCCCATTTACAGATCATTTCATAGGGTAACTTTATTTTTTTCAAATCTTGTCCCAAGTTCTATGAGGGTTCTTGTTTTACATTTTCCAAGTTACTAACTTCCAGCTCAGGTAGGTAAGGGACAACTGAATTGTGGTGGCTGCTATTTTTTGTGCAGGTGTTGATCTACCGGCTGTCAAGGAGTCCAAATGGAAGATCTCAAAGCCCTTAACAGACTATAACAAAGAAGAGATTGGCTGAATATTCATACCCTTGTGAGTTCAAGGGAGTTGTAGGGATGGTCTGGGTAGCCATGCAAGTTCAGAGAGCTCATTCTTTCTGCCTCCTTTCAGCTCTGCCTCCGCAGCTGGCTGATTTTAGTATACGAAGCTCCCTGTAAGCCCGCAGCCTATCGACTTCCACAGTCCCAGACTAAGGCACCGAAGCCTTACTGTTCTCTCTGGAGTCTAGGTCAGGTCCTGAGGTGAAGGGGTAGGGTGGGGGCGGGCAGGACGCAGGTGCAGCCCCGTGGTCCCGGATCTAACTCACATCTCTGGGAATGGGAACCGAAGTCACCGCTTAGGTCTCCCCCAGCGGTTTTCGGTTCAGGTCTCTGTGCTCCTCACCTGGATATGGGCGCCCTGGGGGCCTGGCTTCCTCCGCAGAGACCAGAAGCGATCTCTGTGTTCCGGATTTTCCCTTTATCTTTCCTCTCCCCAGTGTCCTCTCTGTTTCCCAACCTCCCCATACCGAGGACCCCACAACCCGCCCCGGGATGGCGCCTGGCTGCTCGCACCCTAATTCAGGACCCCAATTGGTGGGAGAGGAACCGCGGTCCTCAAGCTTGCAGGACACAGGGCCGCTGCCCGGAATGCTGTGGGGGTGGGGACCCTCGCCGCCCCGCCCCCACCAGCGGCCTGCCCCGCCCCCTCCCACGCACGTCACGTCCGGCCCGGCCTCCGCCCGCCAGCCGCTCTCCTCCCGCTCGCCGTTAGGGAGGCTCTGCACCTCAGCCGCCGCCTCTGTCGCCACTCTCTCCCCCTGTTCCGCGTCTGCGTCGCCACCACCGCCGGGGGTATCCGGGGGGTCGCTTACCGGGCTGGCCCCGCGCCACAGCCGTAGCTTTAACCTCCCGTCAGCGGCCGCCCCAGGAAGAGGCTTCTCGGCCG", //
			"CTGAAGTTAACTTCGAAATTGTTTTCAAATGGATAGATAGGTAGATTAACTACATGTCTTAGCAGGCATTTCAGGATGTTTTGCAACTAATAATCAACTAAAAGATTTGTTATGGGAATGTGGTTTTCCTTCCCTTCAGTTTTTAATGTTTCTCCTGGAGGTGTATGTTACTAATAACACTTCTTTGAAAACCACACTGATTAATATTACTCTTCTGCTTACCTGAAGCTTCTTGGGCAGAGTCCTGCAGTGTAGTTTACTGAGCTTATTAAAAGATTTGTTAATAGATGACTAGAATTCACAGAGGATAATGATACCATTAGAATTCACAAAGGATGATGAGACCAAATATTAAACACTTTGCCAAACAAGTTCTTCTCTCTCTCTCTAGTCTAATTGCTGGTCTTTTAGCATTATGCAAAGAAGAGAGAGGAGACTGGTTCTCTATCTACTGCATACCTATGTTATCTCAGTTAAACTAAACTTAACAGCTGTACAGGTATTATTCTTTTTTTTTTTTTTTTTTTTTTGAGACAGGATCTCACTTTGTTGCCCGGGCTGGAGTGCAGTGGTGCAAACACAACCCATTGCAGCCTCCACCTCCTGGCAGAAGTGATCCTCCTGCCTCAGCCTTCCAAGTAGCTGGGACTACAGGCACTACTATACCTGGCTAATTTCTAAATTTTTTTGTAGAGATGGGGTCTCACTATATTGCCCAGGCTGGTCTCAAACTCCTGGGCTCAAGTGATCCTCCCACCTTGGCCTCTCATAGTGCTGGGATTATAGCCATGAGCCAATGCACCTGGCCTGTACAGGTAGTTTCTTGTTTTATATAAAGAAACCTAGGCTCAGAGACATGGAAGTTGTTCACCATCATGTGTCTACAAAATGACGGAGTTATTAATTGAATCTAAATCTTTCTGGGACTGTTCTTTTTGCTCTTTTCCAACATATGCTATCGCTTCTCTAAATTAGGGAGCTCAGGCAACCACTATGTTGTATACAAAAAAAAGAAGAAAGTACTTAAGTATAGTAGCAAGCTTAAATCATATCTTTGATTCTTTGATCTTTTGAAAATACTTCCATGTTTAGTTCTGAGTATGTGAGGAAATGAGGAAGGAATGGGATGGGGTAGGGTGTCCACACATAATTGAGGAAAATGGATTATTTGAATGGTGTAGTCCTTGAAGATGTGTCCTGT", //
	};

	static void getSequenceData(int[] sequence, int index) {

		String string = testSequenceStrings[index];
		byte[] stringBytes = string.getBytes();

		int stringLength = string.length();
		int sequenceLength = sequence.length;

		// if (stringLength != sequenceLength) {
		//
		// System.out.println("Error! stringLength != sequenceLength");
		// System.out.println("Error! stringLength  = " + stringLength);
		// System.out.println("Error! sequenceLength  = " + sequenceLength);
		// System.exit(1);
		// }

		for (int i = 0; i < sequence.length; i++) {
			switch (stringBytes[i]) {
			case (byte) 'A':
				sequence[i] = 0;
				break;
			case (byte) 'C':
				sequence[i] = 1;
				break;
			case (byte) 'G':
				sequence[i] = 2;
				break;
			case (byte) 'T':
				sequence[i] = 3;
				break;
			}

		}

	}

}

// tryIndex = 33232 // numStrands with matching entrez ids

/*
 * 
 * correlationComparisonCount = 3306 seedVSBestCorrelation = 0.301274039198559 seedVSRandomCorrelation = 0.06198325356310924 averageSeedVSBestCorrelation = 0.09085567668085992 averageSeedVSRandomCorrelation = 0.0732814061424743 averageDeltaCorrelation =
 * 0.017574270538385874 averageDeltaCorrelationMagnitude = 0.014537970469910287 deltaCorelationMean = 0.017574270538385874 deltaCorelationSTD = 0.16271515806275624 deltaCorelationSTDE = 0.0028299358297994574 deltaCorelationZ = 6.210130404134029
 * deltaCorelationMagnitudeMean = 0.014537970469910287 deltaCorelationMagnitudeSTD = 0.11265910947029238 deltaCorelationMagnitudeSTDE = 0.0019593629397472454 deltaCorelationMagnitudeZ = 7.419743517138107 seedGeneSymbol = FAM168A bestGeneSymbol = C3ORF62
 * 
 * numGenesAnalyzedPerSecond = 0.4920205164281116 estimatedAnalysisDuration (s) = 69448.32351313652 estimatedAnalysisDuration (h) = 19.291200975871256 estimatedAnalysisDuration (d) = 0.8038000406613023
 */
