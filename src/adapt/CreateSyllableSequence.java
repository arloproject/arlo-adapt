package adapt;

public class CreateSyllableSequence {

	public static void main(String[] args) {

		System.out.println("CreateSyllableSequence");

		String path = "/home/dtcheng/idealGroundTruth.csv";

		try {
			SimpleTable table = IO.readDelimitedTable(path, "\t", 1, true, true);

			int numRows = table.numDataRows;
			int numColumns = table.numDataColumns;

			System.out.println("numRows = " + numRows);
			System.out.println("numColumns = " + numColumns);

			int syllableIndex = 0;

			for (int i = 0; i < numRows; i++) {

				String time = table.stringMatrix[i][1];

				System.out.print(time);

				for (int j = 2; j < numColumns - 1; j++) {

					String x = table.stringMatrix[i][j];

					x = x.replaceAll("\"", "");
					x = x.replaceAll(" -", "");
					x = x.replaceAll("-", "");
					x = x.replaceAll(" ", "");

					boolean unnamed = false;

					if (x.equalsIgnoreCase("unamed")) {
						unnamed = true;
					}
					if (x.equalsIgnoreCase("unknown")) {
						unnamed = true;
					}
					if (x.equalsIgnoreCase("fragment")) {
						unnamed = true;
					}

					if (x.length() > 0 && !unnamed) {

						boolean containsParentheses = false;

						if (x.contains("(") && x.contains(")")) {
							containsParentheses = true;
						}

						if (containsParentheses) {

							int index1 = x.indexOf("(");
							int index2 = x.indexOf(")");

							boolean numberFirst = false;

							if (index1 != 0) {
								numberFirst = true;
							}

							if (numberFirst) {
								System.out.println("Error!  numberFirst, row = " + (i + 3) + ", value = " + x);
								System.exit(1);
							} else {

								int number = 0;
								try {
									number = Integer.parseInt(x.substring(index1 + 1, index2));
								} catch (Exception e) {
									// TODO Auto-generated catch block
									System.out.println("Error!  in Integer.parseInt, row = " + (i + 3) + ", value = " + x);
									e.printStackTrace();
									System.exit(1);
								}

								String name = x.substring(index2 + 1);

								for (int k = 0; k < number; k++) {
									System.out.print(",");
									System.out.print(name);
								}
							}

						} else {
							System.out.print(",");
							System.out.print(x);
						}

					}
				}

				System.out.println();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println();
		System.out.println("Finished!");
	}
}
