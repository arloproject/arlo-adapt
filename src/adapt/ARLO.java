package adapt;

public class ARLO {
	
	// Doppler Parameters

	int numDopplerTrials = (int) 100e6;
	
	double dopplerMinFreq = 3800;
	double dopplerMaxFreq = 4200;
	
	double dopplerMaxXYSpeed = 30;

	double dopplerMinXVelocity = -dopplerMaxXYSpeed;
	double dopplerMaxXVelocity = dopplerMaxXYSpeed;

	double dopplerMinYVelocity = -dopplerMaxXYSpeed;
	double dopplerMaxYVelocity = dopplerMaxXYSpeed;


	double dopplerMinZVelocity = 0;
	double dopplerMaxZVelocity = 0;
	
	// Location Parameters
	
	boolean assumeZ = false;
	double assumedZ = 0.0;

	int maxNumIterations = (int) 1e6;
	int numReductionSteps = 1;

	int numGoodEvents = 0;
	double averageAbsDiffScore = Double.NaN;
	double[] averageSTDs;
	double minX = 0.0;
	double maxX = 0.0;
	double minY = 0.0;
	double maxY = 0.0;
	double minZ = 0.0;
	double maxZ = 0.0;

	boolean reportStats = false;
	boolean reportEvents = false;

	double minAverageTOADErrorInMS = Double.POSITIVE_INFINITY;

	boolean useAbsDiff = false;
	boolean useVariance = true;

	int X_DIMENSION_INDEX = 0;
	int Y_DIMENSION_INDEX = 1;
	int Z_DIMENSION_INDEX = 2;

	int numSearchSpaceDimensions = 3;
	int numSensors = -1;
	Sensor[] sensors;
	int numEvents = -1;
	Event[] events;

	double speedOfSoundInMetersPerSecond = 340.29;

	public double getSpeedOfSoundInMetersPerSecond() {
		return speedOfSoundInMetersPerSecond;
	}

	public void setSpeedOfSoundInMetersPerSecond(double speedOfSoundInMetersPerSecond) {
		this.speedOfSoundInMetersPerSecond = speedOfSoundInMetersPerSecond;
	}

	ARLO() {

	}

	void setSensors(Sensor[] sensors) {
		this.numSensors = sensors.length;
		this.sensors = sensors;
	}

	void setEvents(Event[] events) {
		this.numEvents = events.length;
		this.events = events;
	}

	double initialSearchRadius = Double.NaN;

	public double getInitialSearchRadius() {
		return initialSearchRadius;
	}

	public void setInitialSearchRadius(double initialSearchRadius) {
		this.initialSearchRadius = initialSearchRadius;
	}

	double searchRadiusReductionFactor = Double.NaN;

	public double getSearchRadiusReductionFactor() {
		return searchRadiusReductionFactor;
	}

	public void setSearchRadiusReductionFactor(double searchRadiusReductionFactor) {
		this.searchRadiusReductionFactor = searchRadiusReductionFactor;
	}

	double endSearchRadius = Double.NaN;
	public int numRepetitions = 1;
	public double toaNoiseLevel = 0.0;

	public double getEndSearchRadius() {
		return endSearchRadius;
	}

	public void setEndSearchRadius(double endSearchRadius) {
		this.endSearchRadius = endSearchRadius;
	}

	void run() {
		// pick starting point for search

		double[] startPoint = new double[3];

		{
			double[] coordSums = new double[3];
			for (int sensorIndex = 0; sensorIndex < numSensors; sensorIndex++) {
				for (int j = 0; j < numSearchSpaceDimensions; j++) {
					coordSums[j] += sensors[sensorIndex].position[j];
				}
			}

			for (int j = 0; j < numSearchSpaceDimensions; j++) {
				startPoint[j] = coordSums[j] / numSensors;
				System.out.println("micCenterOfMass[j] = " + startPoint[j]);

			}

			if (startPoint[2] < minZ)
				startPoint[2] = minZ;

			for (int j = 0; j < numSearchSpaceDimensions; j++) {
				System.out.println("startPoint[j] = " + startPoint[j]);

			}
		}

		double scoreSum = 0.0;
		numGoodEvents = 0;

		double[][] points = new double[numRepetitions][3];

		double[] stdSums = new double[3];
		for (int eventIndex = 0; eventIndex < numEvents; eventIndex++) {

			Event event = events[eventIndex];

			int numEventSensors = event.times.length;

			int numTOADs = numEventSensors * (numEventSensors - 1) / 2;

			if (reportStats) {
				System.out.println("numEventSensors = " + numEventSensors);
				System.out.println("numEvents = " + numEvents);
				System.out.println("numTOADs = " + numTOADs);
			}

			double toaErrorSum = 0.0;
			for (int repetitionIndex = 0; repetitionIndex < numRepetitions; repetitionIndex++) {

				double[] eventTimeOfArrivalDifferences = new double[numTOADs];

				double[] bestPoint = new double[3];
				double bestOverallScore = Double.NEGATIVE_INFINITY;

				// compute target distance matrix

				double[] toaRandomOffsets = new double[numEventSensors];
				for (int sensorIndex = 0; sensorIndex < numEventSensors; sensorIndex++) {
					toaRandomOffsets[sensorIndex] = (Math.random() * 2 - 1.0) * toaNoiseLevel;
				}
				int toadIndex = 0;
				for (int sensorIndex1 = 0; sensorIndex1 < numEventSensors - 1; sensorIndex1++) {

					for (int sensorIndex2 = sensorIndex1 + 1; sensorIndex2 < numEventSensors; sensorIndex2++) {

						double diff = (events[eventIndex].times[sensorIndex1] + toaRandomOffsets[sensorIndex1]) - (events[eventIndex].times[sensorIndex2] + toaRandomOffsets[sensorIndex2]);
						eventTimeOfArrivalDifferences[toadIndex++] = diff;

					}
				}

				// search space for solution

				for (int j = 0; j < numSearchSpaceDimensions; j++) {
					bestPoint[j] = startPoint[j];
				}

				double searchRadius = initialSearchRadius;

				double[] candidatePoint = new double[numSearchSpaceDimensions];
				double[] candidateTimeOfArrivals = new double[numEventSensors];
				double[] candidateTimeOfArrivalDifferences = new double[numTOADs];

				// for (int iterationIndex = 0; iterationIndex< numIterations; iterationIndex++) {
				int iterationIndex = 0;

				while (true) {

					// pick next candidate point
					// for (int j = 0; j < numSearchSpaceDimensions; j++) {
					// candidatePoint[j] = bestPoint[j] + searchRadius * (Math.random() - 0.5);
					// }
					// break;
					// if (candidatePoint[Z_DIMENSION_INDEX] < minZ) {
					// candidatePoint[Z_DIMENSION_INDEX] = minZ;
					// }

					if (true)
						if (iterationIndex > 0 && (iterationIndex % (maxNumIterations / numReductionSteps) == 0)) {
							// pick next candidate point
							for (int j = 0; j < numSearchSpaceDimensions; j++) {
								startPoint[j] = bestPoint[j];
							}

							searchRadius *= searchRadiusReductionFactor;

							// System.out.println("searchRadius = " + searchRadius);

						}

					// pick next candidate point
					for (int j = 0; j < numSearchSpaceDimensions; j++) {
						candidatePoint[j] = startPoint[j] + searchRadius * 2 * (Math.random() - 0.5);
					}
					// break;
					if (candidatePoint[Z_DIMENSION_INDEX] < 0.0) {
						candidatePoint[Z_DIMENSION_INDEX] = 0.0;
					}

					if (assumeZ) {
						candidatePoint[Z_DIMENSION_INDEX] = assumedZ;
					}

					// calculate sensor time of arrivals

					for (int sensorIndex = 0; sensorIndex < numEventSensors; sensorIndex++) {

						double distance = distanceBetweenPoints(candidatePoint, events[eventIndex].positions[sensorIndex]);

						double time = (distance / speedOfSoundInMetersPerSecond);

						candidateTimeOfArrivals[sensorIndex] = time;

					}

					// calculate difference matrix

					toadIndex = 0;
					for (int sensorIndex1 = 0; sensorIndex1 < numEventSensors - 1; sensorIndex1++) {

						for (int sensorIndex2 = sensorIndex1 + 1; sensorIndex2 < numEventSensors; sensorIndex2++) {

							double diff = candidateTimeOfArrivals[sensorIndex1] - candidateTimeOfArrivals[sensorIndex2];

							candidateTimeOfArrivalDifferences[toadIndex++] = diff;

						}
					}

					// calculate score

					double candidateAbsDiffScore = 0.0;
					for (int i = 0; i < numTOADs; i++) {
						double difference = Math.abs(candidateTimeOfArrivalDifferences[i] - eventTimeOfArrivalDifferences[i]);
						candidateAbsDiffScore -= difference;
					}

					double candidateOverallScore = candidateAbsDiffScore / numTOADs;
					//

					if (candidateOverallScore > bestOverallScore) {
						bestOverallScore = candidateOverallScore;
						for (int j = 0; j < numSearchSpaceDimensions; j++) {
							bestPoint[j] = candidatePoint[j];
						}

					}

					// update search radius

					// searchRadius *= searchRadiusReductionFactor;

					iterationIndex++;

					if (iterationIndex == maxNumIterations)
						break;
					//
					// if (searchRadius < endSearchRadius) {
					// break;
					// }

				}

				for (int j = 0; j < numSearchSpaceDimensions; j++) {
					points[repetitionIndex][j] = bestPoint[j];
				}
				toaErrorSum += -bestOverallScore;

				if (false) {

					if (-bestOverallScore * 1000 < minAverageTOADErrorInMS) {

						numGoodEvents++;

						// System.out.print(eventIndex + "\t" + numEventSensors + "\t" + formatDouble7p1(event.times[0]) + "\t" + iterationIndex + "\t" + formatDouble6p3(bestOverallScore * 1000));
						System.out.print(eventIndex + "\t" + numEventSensors + "\t" + formatDouble7p1(event.times[0]) + "\t" + iterationIndex + "\t" + formatDouble6p3(bestOverallScore * 1000));
						for (int j = 0; j < numSearchSpaceDimensions; j++) {
							System.out.print("\t" + formatDouble6p1(bestPoint[j]));
						}
						for (int i = 0; i < numSensors; i++) {

							if (i < numEventSensors) {
								System.out.print("\t" + event.tagIDs[i]);
								System.out.print("\t" + event.fileNames[i]);
								System.out.print("\t" + formatDouble6p3(event.fileTimes[i]));
							} else {
								System.out.print("\t" + "na");
								System.out.print("\t" + "na");
								System.out.print("\t" + "na");
							}

						}

						System.out.println();
					}

				}

				scoreSum += bestOverallScore;

			}

			double[] xyzMeans = new double[numSearchSpaceDimensions];
			double[] xyzStds = new double[numSearchSpaceDimensions];
			for (int j = 0; j < numSearchSpaceDimensions; j++) {

				double sum = 0.0;
				for (int repetitionIndex = 0; repetitionIndex < numRepetitions; repetitionIndex++) {
					sum += points[repetitionIndex][j];
				}
				double mean = sum / numRepetitions;

				xyzMeans[j] = mean;

				double varianceSum = 0;
				for (int repetitionIndex = 0; repetitionIndex < numRepetitions; repetitionIndex++) {
					double diff = points[repetitionIndex][j] - mean;
					varianceSum += diff * diff;
				}

				double std = Math.sqrt(varianceSum / numRepetitions);
				xyzStds[j] = std;

			}

			for (int j = 0; j < numSearchSpaceDimensions; j++) {
				stdSums[j] += xyzStds[j];
			}

			/****************************************************/
			/* SOLVE FOR VELOCITY VECTOR USING DOPPLER ANALYSIS */
			/****************************************************/


			double bestFrequencyErrorStd = Double.MAX_VALUE;
			double bestFrequency = Double.NaN;
			double bestXVelocity = Double.NaN;
			double bestYVelocity = Double.NaN;
			double bestZVelocity = Double.NaN;


			// double minZVelocity = -20;
			// double maxZVelocity = 20;

			double birdX = xyzMeans[0];
			double birdY = xyzMeans[1];
			double birdZ = xyzMeans[2];
			// System.out.println("birdX = " + birdX);
			// System.out.println("birdY = " + birdY);
			// System.out.println("birdZ = " + birdZ);

			double frequencySum = 0.0;
			for (int i = 0; i < numEventSensors; i++) {
				frequencySum += event.frequencies[i];
			}
			double meanFrequency = frequencySum / numEventSensors;

			double frequencyVarianceSum = 0;
			for (int i = 0; i < numEventSensors; i++) {
				double frequencyDiff = event.frequencies[i] - meanFrequency;
				frequencyVarianceSum += frequencyDiff * frequencyDiff;
			}
			double frequencyStd = Math.sqrt(frequencyVarianceSum);

			double candidateFrequency = Double.NaN;
			double candidateXVelocity = Double.NaN;
			double candidateYVelocity = Double.NaN;
			double candidateZVelocity = Double.NaN;

			for (int j = 0; j < numDopplerTrials; j++) {

				// guess freq, x velocity, and y velocity

				while (true) {
					candidateFrequency = Math.random() * (dopplerMaxFreq - dopplerMinFreq) + dopplerMinFreq;
					candidateXVelocity = Math.random() * (dopplerMaxXVelocity - dopplerMinXVelocity) + dopplerMinXVelocity;
					candidateYVelocity = Math.random() * (dopplerMaxYVelocity - dopplerMinYVelocity) + dopplerMinYVelocity;
					candidateZVelocity = Math.random() * (dopplerMaxZVelocity - dopplerMinZVelocity) + dopplerMinZVelocity;

					if (Math.sqrt(candidateXVelocity * candidateXVelocity + candidateYVelocity * candidateYVelocity) < dopplerMaxXYSpeed) {
						break;
					}

				}

				// double candidateZVelocity = 0.0;

				// System.out.println("candidateFrequency = " + candidateFrequency);
				// System.out.println("candidateXVelocity = " + candidateXVelocity);
				// System.out.println("candidateYVelocity = " + candidateYVelocity);
				// System.out.println("candidateZVelocity = " + candidateZVelocity);

				double timeStepSize = 1e-9;

				double candidateBirdNewX = birdX + timeStepSize * candidateXVelocity;
				double candidateBirdNewY = birdY + timeStepSize * candidateYVelocity;
				double candidateBirdNewZ = birdZ + timeStepSize * candidateZVelocity;

				// System.out.println();
				double frequencyErrorVarianceSum = 0;
				for (int i = 0; i < numEventSensors; i++) {

					double sensorX = event.positions[i][0];
					double sensorY = event.positions[i][1];
					double sensorZ = event.positions[i][2];
					// System.out.println("sensorX = " + sensorX);
					// System.out.println("sensorY = " + sensorY);

					double xDiff1 = birdX - sensorX;
					double yDiff1 = birdY - sensorY;
					double zDiff1 = birdZ - sensorZ;
					double distance1 = Math.sqrt((xDiff1 * xDiff1) + (yDiff1 * yDiff1) + (zDiff1 * zDiff1));

					double xDiff2 = candidateBirdNewX - sensorX;
					double yDiff2 = candidateBirdNewY - sensorY;
					double zDiff2 = candidateBirdNewZ - sensorZ;
					double distance2 = Math.sqrt((xDiff2 * xDiff2) + (yDiff2 * yDiff2) + (zDiff2 * zDiff2));

					// System.out.println("distance1 = " + distance1);
					// System.out.println("distance2 = " + distance2);

					double radialVelocity = (distance2 - distance1) / timeStepSize;

					// System.out.println("radialVelocity = " + radialVelocity);

					double frequencyShiftFactor = speedOfSoundInMetersPerSecond / (speedOfSoundInMetersPerSecond + radialVelocity);

					// System.out.println("frequencyShiftFactor = " + frequencyShiftFactor);

					double predictedSensorFrequency = candidateFrequency * frequencyShiftFactor;

					// System.out.println("predictedSensorFrequency = " + predictedSensorFrequency);

					double actualSensorFrequency = event.frequencies[i];

					// System.out.println("actualSensorFrequency = " + actualSensorFrequency);

					double frequencyDiff = predictedSensorFrequency - actualSensorFrequency;

					double frequencyVariance = frequencyDiff * frequencyDiff;
					// double frequencyVariance = Math.abs(frequencyDiff);

					frequencyErrorVarianceSum += frequencyVariance;

					// System.out.print("\t" + event.fileNames[i]);
					// System.out.print("\t" + formatDouble6p3(event.fileTimes[i]) + "\t" + formatDouble6p3(event.frequencies[i]));
					// System.out.println();

				}

				double frequencyErrorStd = Math.sqrt(frequencyErrorVarianceSum);
				// System.out.println("frequencyErrorStd = " + frequencyErrorStd);

				if (frequencyErrorStd < bestFrequencyErrorStd) {
					bestFrequencyErrorStd = frequencyErrorStd;
					bestFrequency = candidateFrequency;
					bestXVelocity = candidateXVelocity;
					bestYVelocity = candidateYVelocity;
					bestZVelocity = candidateZVelocity;
				}

			}
			// System.out.println("bestFrequencyErrorStd = " + bestFrequencyErrorStd);
			// System.out.println("bestFrequency = " + bestFrequency);
			// System.out.println("bestXVelocity = " + bestXVelocity);
			// System.out.println("bestYVelocity = " + bestYVelocity);
			// System.out.println("bestZVelocity = " + bestZVelocity);

			// System.out.println();

			// //////////////////
			// report results //
			// //////////////////

			// System.out.print(eventIndex + "\t" + numEventSensors + "\t" + formatDouble7p1(event.times[0]) + "\t" + iterationIndex + "\t" + formatDouble6p3(bestOverallScore * 1000));
			System.out.print(eventIndex + "\t" + numEventSensors + "\t" + formatDouble7p1(event.times[0]) + "\t" + maxNumIterations);

			double toaError = toaErrorSum / numRepetitions;
			System.out.print("\t" + formatDouble1p5(toaError));

			for (int j = 0; j < numSearchSpaceDimensions; j++) {
				System.out.print("\t" + formatDouble1p3(xyzMeans[j]));
			}
			for (int j = 0; j < numSearchSpaceDimensions; j++) {
				System.out.print("\t" + formatDouble1p3(xyzStds[j]));
			}

			System.out.print("\t" + formatDouble1p3(bestXVelocity));
			System.out.print("\t" + formatDouble1p3(bestYVelocity));
			System.out.print("\t" + formatDouble1p3(bestZVelocity));

			double relativeError = bestFrequencyErrorStd / frequencyStd;
			System.out.print("\t" + formatDouble1p3(relativeError));
			
			System.out.print("\t" + formatDouble1p3(bestFrequency));

			for (int i = 0; i < numSensors; i++) {

				if (i < numEventSensors) {
					System.out.print("\t" + event.tagIDs[i]);
					System.out.print("\t" + event.fileNames[i]);
					System.out.print("\t" + formatDouble6p3(event.fileTimes[i]) + "\t" + formatDouble6p3(event.frequencies[i]));
				} else {
					System.out.print("\t" + "na");
					System.out.print("\t" + "na");
					System.out.print("\t" + "na" + "\t" + "na");
				}

			}

			System.out.println();

		}

		averageSTDs = new double[3];

		for (int j = 0; j < numSearchSpaceDimensions; j++) {
			averageSTDs[j] = stdSums[j] / numEvents;
		}

		averageAbsDiffScore = -scoreSum / numEvents / numRepetitions * 1000;

	}

	String formatDouble1p1(double x) {
		return String.format("%1.1f", x);
	}

	String formatDouble1p3(double x) {
		return String.format("%1.3f", x);
	}

	String formatDouble1p5(double x) {
		return String.format("%1.5f", x);
	}

	String formatDouble6p1(double x) {
		return String.format("%6.1f", x);
	}

	String formatDouble6p3(double x) {
		return String.format("%6.3f", x);
	}

	String formatDouble7p1(double x) {
		return String.format("%7.1f", x);
	}

	double distanceBetweenPoints(double[] pointA, double[] pointB) {

		double sum = 0;

		for (int j = 0; j < numSearchSpaceDimensions; j++) {

			double diff = pointA[j] - pointB[j];

			sum += diff * diff;

		}

		return Math.sqrt(sum);

	}
}
