package adapt;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

public class ClineSelectedRandomSubset {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		boolean useJavaToCopyFiles = false;
		boolean printScriptOnly = !useJavaToCopyFiles;

		Vector<File> searchDirectoryFileVector = new Vector<File>();
		Vector<File> resultFileVector = new Vector<File>();

		// boolean samplingEquallyWeightedByDirectory = true;
		// boolean sampleEquallyWeightedByFile = false;

		// String outputDirectory = "/home/dtcheng/samples/fbis/";
		// searchDirectoryFileVector.add(new File("/RAID/PRODUCTIONSCANS/FBIS/FILM"));
		// searchDirectoryFileVector.add(new File("/RAID/PRODUCTIONSCANS/FBIS/FICHE"));
		// int numIterations = 2000;
		//
		// String outputDirectory = "/home/dtcheng/samples/swb/";
		// searchDirectoryFileVector.add(new File("/RAID/PRODUCTIONSCANS/SWB/FILM"));
		// searchDirectoryFileVector.add(new File("/RAID/PRODUCTIONSCANS/SWB/FICHE"));
		// int numIterations = 2000;

		boolean samplingEquallyWeightedByDirectory = false;
		boolean sampleEquallyWeightedByFile = true;

		String outputDirectory = "/home/dtcheng/100kByFile/";
		searchDirectoryFileVector.add(new File("/RAID/PRODUCTIONSCANS/FBIS/FILM"));
		searchDirectoryFileVector.add(new File("/RAID/PRODUCTIONSCANS/FBIS/FICHE"));
		searchDirectoryFileVector.add(new File("/RAID/PRODUCTIONSCANS/SWB/FILM"));
		searchDirectoryFileVector.add(new File("/RAID/PRODUCTIONSCANS/SWB/FICHE"));
		int numIterations = 100000;

		IO.getPathStrings(searchDirectoryFileVector, resultFileVector);

		System.out.println("resultFileVector.size() = " + resultFileVector.size());

		HashMap<String, Vector<String>> directoryPathHashMap = new HashMap<String, Vector<String>>();

		Vector<String> filePathVector = new Vector<String>();
		for (Iterator<File> iterator = resultFileVector.iterator(); iterator.hasNext();) {
			File file = (File) iterator.next();

			String filePath = file.getAbsolutePath();
			if (!(filePath.substring(filePath.length() - 4).equalsIgnoreCase(".tif")))
				continue;

			String directoryPath = file.getParent();
			Vector<String> directoryFilePathVector = directoryPathHashMap.get(directoryPath);

			if (directoryFilePathVector == null) {
				directoryFilePathVector = new Vector();
				directoryPathHashMap.put(directoryPath, directoryFilePathVector);
			}
			directoryFilePathVector.add(filePath);
			filePathVector.add(filePath);

		}

		Set<String> directoryNameSet = directoryPathHashMap.keySet();

		int numDirectories = directoryNameSet.size();
		String[] directoryPathStrings = new String[numDirectories];

		{
			int i = 0;
			for (Iterator iterator = directoryNameSet.iterator(); iterator.hasNext();) {
				directoryPathStrings[i++] = (String) iterator.next();
			}
		}

		HashMap<String, Boolean> filePaths = new HashMap<String, Boolean>();

		for (int i = 0; i < numIterations; i++) {

			String filePath = null;
			while (true) {

				if (samplingEquallyWeightedByDirectory) {
					String randomDirectoryPath = directoryPathStrings[(int) (numDirectories * Math.random())];

					Vector<String> directoryFilePathVector = directoryPathHashMap.get(randomDirectoryPath);

					filePath = directoryFilePathVector.elementAt((int) (directoryFilePathVector.size() * Math.random()));

					Boolean filePathExists = filePaths.get(filePath);

					if (filePathExists == null) {
						filePaths.put(filePath, true);
						break;
					}
				}

				if (sampleEquallyWeightedByFile) {

					filePath = filePathVector.elementAt((int) (filePathVector.size() * Math.random()));

					Boolean filePathExists = filePaths.get(filePath);

					if (filePathExists == null) {
						filePaths.put(filePath, true);
						break;
					}
				}

			}

			String command = "cp --parents " + filePath + " " + outputDirectory;

			System.out.println(command);

			if (useJavaToCopyFiles) {
				try {
					Process child = Runtime.getRuntime().exec(command);

					try {
						child.waitFor();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}

	}
}
