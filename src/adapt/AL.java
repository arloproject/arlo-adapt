package adapt;

import java.beans.beancontext.BeanContext;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Random;

import com.mysql.jdbc.BestResponseTimeBalanceStrategy;

public class AL {

	public static void main(String[] args) throws Exception {

		if (args.length == 0) {
			args = new String[] { //
			//
					"-verbose", "false", //
					// "-workDirectiory", "/media/Patriot/pollen", //
					// "-exampleFileName", "examplesWidthAndConfidence.tab", //
					// "-workDirectiory", "/home/dtcheng", //
					"-workDirectiory", "/media/Patriot/pollen", //
					// "-exampleFileName", "examples.tab", //
					"-exampleFileName", "examples.tab", //
					"-numExamples", "13649", //
					// "-numExamples", "7200", //
					"-numInputs", "12661", //
					"-numOutputs", "1", //
					"-numClasses", "119", //
					"-outputName1", "class", // best
					"-numExperimentRepititions", "1", //
					// "-numBiasPoints", "12661", //
					"-numBiasPoints", "63305", // 4 rounds
					"-numInternalValidations", "1", //
					"-internalNumTrainExamples", "6000", //
					"-internalNumTestExamples", "0", //
					"-externalNumTestExamples", "6000", //
					"-numFeaturesToSelect", "1", //
					"-numExternalValidations", "999999", //
			};
		}

		HashMap<String, String> argHashMap = ParseArgs.parse(args);

		Experiment experiment = new Experiment(argHashMap);
	}
}

class Experiment {

	static boolean useCache = true;
	static boolean useExhaustiveFeatureSearch = true;

	Experiment(HashMap<String, String> args) throws Exception {

		/* read parameters */

		boolean verbose = Boolean.parseBoolean(args.get("verbose"));
		String workDirectiory = args.get("workDirectiory");
		String exampleFileName = args.get("exampleFileName");
		int numExamples = Integer.parseInt(args.get("numExamples"));
		// int numInputs = Integer.parseInt(args.get("numInputs"));
		int numClasses = Integer.parseInt(args.get("numClasses"));
		int numInputs = Integer.parseInt(args.get("numInputs"));
		int numOutputs = Integer.parseInt(args.get("numOutputs"));
		int numBiasPoints = Integer.parseInt(args.get("numBiasPoints"));
		int numInternalValidations = Integer.parseInt(args.get("numInternalValidations"));
		int internalNumTrainExamples = Integer.parseInt(args.get("internalNumTrainExamples"));
		int internalNumTestExamples = Integer.parseInt(args.get("internalNumTestExamples"));
		int numExternalValidations = Integer.parseInt(args.get("numExternalValidations"));
		int externalNumTestExamples = Integer.parseInt(args.get("externalNumTestExamples"));
		int numFeaturesToSelect = Integer.parseInt(args.get("numFeaturesToSelect"));

		int internalNumExamples = internalNumTrainExamples + internalNumTestExamples;
		int experimentNumExamples = internalNumExamples + externalNumTestExamples;

		/* report parameters */

		int numWidthFeatures = 1;
		int numRadialColorPixelFeatures = 63;
		int numRadialColorMeanFeatures = numRadialColorPixelFeatures * 3;
		int numRadialColorStdFeatures = numRadialColorPixelFeatures * 3;
		int numRadialFeatures = numRadialColorMeanFeatures + numRadialColorStdFeatures;
		int numTextureFeatures = 12282;

		int computedNumInputs = numWidthFeatures + numRadialFeatures + numTextureFeatures;

		if (computedNumInputs != numInputs) {
			throw new Exception("computedNumInputs != numInputs");
		}

		// if (useExhaustiveFeatureSearch && (numInternalValidations % numInputs != 0)) {
		// throw new Exception("useExhaustiveFeatureSearch && (numInternalValidations % numInputs != 0");
		// }

		System.out.printf("Experiment args:\n");
		System.out.printf("verbose                    = %s\n", verbose);
		System.out.printf("workDirectiory             = %s\n", workDirectiory);
		System.out.printf("exampleFileName            = %s\n", exampleFileName);
		System.out.printf("numExamples                = %d\n", numExamples);
		System.out.printf("numInputs                  = %d\n", numInputs);
		System.out.printf("numWidthFeatures           = %d\n", numWidthFeatures);
		System.out.printf("numRadialColorMeanFeatures = %d\n", numRadialColorMeanFeatures);
		System.out.printf("numRadialColorStdFeatures  = %d\n", numRadialColorStdFeatures);
		System.out.printf("numTextureFeatures         = %d\n", numTextureFeatures);
		System.out.printf("numOutputs                 = %d\n", numOutputs);
		System.out.printf("numBiasPoints              = %d\n", numBiasPoints);
		System.out.printf("numInternalValidations     = %d\n", numInternalValidations);
		System.out.printf("experimentNumExamples      = %d\n", experimentNumExamples);
		System.out.printf("internalNumExamples        = %d\n", internalNumExamples);
		System.out.printf("internalNumTrainExamples   = %d\n", internalNumTrainExamples);
		System.out.printf("internalNumTestExamples    = %d\n", internalNumTestExamples);
		System.out.printf("numExternalValidations     = %d\n", numExternalValidations);
		System.out.printf("externalNumTestExamples    = %d\n", externalNumTestExamples);

		/* read example file */

		int[] allExampleIndicies = new int[numExamples];
		double[][] exampleInputs = new double[numExamples][numInputs];
		int[] exampleOutputConficences = new int[numExamples];
		int[] exampleOutputClassIndicies = new int[numExamples];
		String exampleFilePath = workDirectiory + File.separatorChar + exampleFileName;

		if (!useCache) {

			System.out.printf("\n");
			System.out.printf("Counting Lines...\n", externalNumTestExamples);
			int numExamplesInFile = IO.countLines(exampleFilePath);
			System.out.printf("...done\n", externalNumTestExamples);
			System.out.printf("numExamplesInFile = %d\n", numExamplesInFile);

			if (numExamplesInFile != numExamples) {
				throw new Exception("numExamplesInFile != numExamples");
			}

			allExampleIndicies = new int[numExamples];
			for (int i = 0; i < numExamples; i++) {
				allExampleIndicies[i] = i;
			}

			exampleInputs = new double[numExamples][numInputs];
			exampleOutputConficences = new int[numExamples];
			exampleOutputClassIndicies = new int[numExamples];

			// SimpleTable table = IO.readDelimitedTable(exampleFilePath, "\t", false);

			BufferedReader bufferedReader = new BufferedReader(new FileReader(new File(exampleFilePath)));

			String line = null;
			String delimiter = "\t";
			for (int i = 0; i < numExamples; i++) {

				line = bufferedReader.readLine();

				String[] parts = line.split(delimiter, -1);

				for (int j = 0; j < numInputs; j++) {
					exampleInputs[i][j] = Double.parseDouble(parts[j]);

				}

				exampleOutputConficences[i] = Integer.parseInt(parts[numInputs + 0]);
				exampleOutputClassIndicies[i] = Integer.parseInt(parts[numInputs + 1]);

			}

			/* normalize examples (warning: this causes unregulated overfitting) */

			boolean applyRankNormalization = false;
			if (applyRankNormalization) {
				double[] featureValues = new double[numExamples];
				for (int i = 0; i < numInputs; i++) {
					System.out.printf("rank normalizing feature %d\n", i);
					for (int e = 0; e < numExamples; e++) {
						featureValues[e] = exampleInputs[e][i];
					}
					Arrays.sort(featureValues);
					for (int e = 0; e < numExamples; e++) {
						exampleInputs[e][i] = (double) Arrays.binarySearch(featureValues, exampleInputs[e][i]) / numExamples;
					}

				}
			}

			boolean applyLinearNormalization = true;

			if (applyLinearNormalization) /* !!! */
			{
				double[] inputMinValues = new double[numInputs];
				double[] inputMaxValues = new double[numInputs];
				double[] inputRanges = new double[numInputs];
				for (int i = 0; i < numInputs; i++) {
					inputMinValues[i] = Double.POSITIVE_INFINITY;
					inputMaxValues[i] = Double.NEGATIVE_INFINITY;
				}

				for (int exampleIndex = 0; exampleIndex < internalNumTrainExamples; exampleIndex++) {

					double[] trainInputValues = exampleInputs[allExampleIndicies[exampleIndex]];

					for (int i = 0; i < numInputs; i++) {

						if (trainInputValues[i] < inputMinValues[i]) {
							inputMinValues[i] = trainInputValues[i];
						}
						if (trainInputValues[i] > inputMaxValues[i]) {
							inputMaxValues[i] = trainInputValues[i];
						}

					}

				}
				for (int i = 0; i < numInputs; i++) {
					inputRanges[i] = inputMaxValues[i] - inputMinValues[i];
				}

				for (int exampleIndex = 0; exampleIndex < internalNumTrainExamples; exampleIndex++) {
					double[] trainInputValues = exampleInputs[allExampleIndicies[exampleIndex]];
					for (int i = 0; i < numInputs; i++) {
						trainInputValues[i] = (trainInputValues[i] - inputMinValues[i]) / inputRanges[i];
					}

				}
			}
			IO.writeObject(exampleFilePath + ".ser", new Object[] { allExampleIndicies, exampleInputs, exampleOutputConficences, exampleOutputClassIndicies });
		} else {

			Object[] objects = (Object[]) IO.readObject(exampleFilePath + ".ser");

			int index = 0;
			allExampleIndicies = (int[]) objects[index++];
			exampleInputs = (double[][]) objects[index++];
			exampleOutputConficences = (int[]) objects[index++];
			exampleOutputClassIndicies = (int[]) objects[index++];

		}

		/* run experiment */

		// int externalExampleSelectionRandomInitialSeed = 222222222;
		// int internalExampleSelectionRandomInitialSeed = 333333333;
		// int internalBiasSelectionRandomInitialSeed = 444444444;
		int externalExampleSelectionRandomInitialSeed = 222222222;
		int internalExampleSelectionRandomInitialSeed = 333333333;
		int internalBiasSelectionRandomInitialSeed = 444444444;

		// Random externalExampleSelectionRandom = new Random(externalExampleSelectionRandomInitialSeed);
		// Random internalExampleSelectionRandom = new Random(internalExampleSelectionRandomInitialSeed);
		// Random biasSelectionRandom = new Random(internalBiasSelectionRandomInitialSeed);

		Random externalExampleSelectionRandom = new Random();
		Random internalExampleSelectionRandom = new Random();
		Random biasSelectionRandom = new Random();

		int numBiasSpaceDimensions = numInputs + 4; // (feature weights + distanceWeightingPower + minDistance + minTrainConfidence + minTestConfidence)

		double[][] biasParameters = new double[numBiasPoints][numBiasSpaceDimensions];
		int maxNumBestFeatures = numInputs;
		int[] bestFeaturesSoFar = new int[maxNumBestFeatures];

		int numPerformanceMetrics = 1; // (accuracy)
		double[][] biasPerformanceSum = new double[numBiasPoints][numPerformanceMetrics];
		double[][] biasPerformance = new double[numBiasPoints][numPerformanceMetrics];

		double[] classWeights = new double[numClasses];

		boolean useLeaveOneOut = false;

		if ((numInternalValidations == (internalNumTrainExamples + internalNumTestExamples)) && (internalNumTestExamples == 1)) {
			useLeaveOneOut = true;
			System.out.printf("useLeaveOneOut  = %b\n", useLeaveOneOut);
		}

		int[] allExampleIndiciesInternalExampleCopy = null;

		if (useLeaveOneOut) {
			allExampleIndiciesInternalExampleCopy = new int[internalNumExamples];
		}

		// create internal and external examples //

		double predictedAccuracies[] = new double[numExternalValidations];
		double actualAccuracies[] = new double[numExternalValidations];
		double actualOptimism[] = new double[numExternalValidations];

		for (int externalValidationIndex = 0; externalValidationIndex < numExternalValidations; externalValidationIndex++) {

			// double[][] biasParameters = new double[numBiasPoints][numBiasSpaceDimensions];

			for (int i = 0; i < numBiasPoints; i++) {
				Arrays.fill(biasParameters[i], 0.0);
			}

			if (verbose)
				System.out.printf("externalValidationIndex  = %d\n", externalValidationIndex);

			// randomize examples //

			Utility.randomizeIntArrayInPlace(externalExampleSelectionRandom, allExampleIndicies, numExamples);

			/*******************************************/
			/* select points in bias space to evaluate */
			/*******************************************/

			double[] widthWeights = new double[numBiasPoints];
			double[] radialColorMeanWeights = new double[numBiasPoints];
			double[] radialColorStdWeights = new double[numBiasPoints];
			double[] textureWeights = new double[numBiasPoints];

			int numRoundsOfFeatureSetExpansion = numBiasPoints / numInputs;
			System.out.printf("numRoundsOfFeatureSetExpansion  = %d\n", numRoundsOfFeatureSetExpansion);
			for (int biasPointIndex = 0; biasPointIndex < numBiasPoints; biasPointIndex++) {

				boolean mode1 = false;
				boolean mode2 = true;
				if (mode1) {

					// double widthWeight = biasSelectionRandom.nextDouble() / numWidthFeatures * 1.0;
					// double radialColorMeanWeight = biasSelectionRandom.nextDouble() / numRadialColorMeanFeatures * 1.0;
					// double radialColorStdWeight = biasSelectionRandom.nextDouble() / numRadialColorStdFeatures * 1.0;
					// double textureWeight = biasSelectionRandom.nextDouble() * 1.0;

					// 1,0,0,0 = 38.4
					// 1,0.1,0,0 = 42.1
					// 1,0.1,0.1,0 = 46.9

					// actualAccuracy = 0.532297 0.153879 0.007526
					// double widthWeight = 8.0;
					// double radialColorMeanWeight = 0.05;
					// double radialColorStdWeight = 0.05;
					// double textureWeight = 0.01;

					double widthWeight = 1.0;
					double radialColorMeanWeight = 0.00;
					double radialColorStdWeight = 0.00;
					double textureWeight = 1.00;
					int numTextureLevels = 11;
					int numTextureLevelsToUse = 11;
					double levelToLevelWeightMultiplier = 0.55;

					// double widthWeight = 0.5 / numWidthFeatures * 1.0;
					// double radialColorMeanWeight = 0.5 / numRadialColorMeanFeatures * 1.0;
					// double radialColorStdWeight = 0.5 / numRadialColorStdFeatures * 1.0;
					// double textureWeight = 0.5 / numTextureFeatures * 1.0;

					double weightSum = 0.0;
					weightSum += widthWeight * numWidthFeatures + radialColorMeanWeight * numRadialColorMeanFeatures + radialColorStdWeight * numRadialColorStdFeatures + textureWeight * numTextureFeatures;

					// System.out.printf("weightSum  = %f\n", weightSum);

					if (weightSum == 0.0) {
						weightSum = 1.0;
					}

					widthWeight /= weightSum;
					radialColorMeanWeight /= weightSum;
					radialColorStdWeight /= weightSum;
					textureWeight /= weightSum;

					widthWeights[biasPointIndex] = widthWeight;
					radialColorMeanWeights[biasPointIndex] = radialColorMeanWeight;
					radialColorStdWeights[biasPointIndex] = radialColorStdWeight;
					textureWeights[biasPointIndex] = textureWeight;

					// System.out.printf("widthWeight           = %f\n", widthWeight);
					// System.out.printf("radialColorMeanWeight = %f\n", radialColorMeanWeight);
					// System.out.printf("radialColorStdWeight  = %f\n", radialColorStdWeight);
					// System.out.printf("textureWeight         = %f\n", textureWeight);

					int biasDimensionIndex = 0;
					biasParameters[biasPointIndex][biasDimensionIndex++] = widthWeight;
					for (int i = 0; i < numRadialFeatures; i++) {
						if ((i % 6) / 3 == 0) {
							biasParameters[biasPointIndex][biasDimensionIndex++] = radialColorMeanWeight;
						} else {
							biasParameters[biasPointIndex][biasDimensionIndex++] = radialColorStdWeight;
						}
					}

					{
						double currentTextureWeight = textureWeight;
						for (int textureLevel = 0; textureLevel < numTextureLevels; textureLevel++) {

							int numFeaturesInLevel = 3 * (int) Math.pow(2, textureLevel + 1);
							for (int i = 0; i < numFeaturesInLevel; i++) {
								if (textureLevel < numTextureLevelsToUse) {
									biasParameters[biasPointIndex][biasDimensionIndex++] = currentTextureWeight;
								} else {
									biasParameters[biasPointIndex][biasDimensionIndex++] = 0.0;
								}
							}
							currentTextureWeight *= levelToLevelWeightMultiplier;

						}
					}

					biasParameters[biasPointIndex][biasDimensionIndex++] = /* biasSelectionRandom.nextDouble() * 2 * */6;
					biasParameters[biasPointIndex][biasDimensionIndex++] = /* biasSelectionRandom.nextDouble() * 2 * */1e-6;
					// biasParameters[biasPointIndex][biasDimensionIndex++] = 0.0;
					// biasParameters[biasPointIndex][biasDimensionIndex++] = 0.0000001;
					biasParameters[biasPointIndex][biasDimensionIndex++] = 0.0;
					biasParameters[biasPointIndex][biasDimensionIndex++] = 0.0;

					// System.out.printf("biasDimensionIndex = %d\n", numBiasSpaceDimensions);
					// System.out.printf("biasDimensionIndex = %d\n", biasDimensionIndex);

				} else if (mode2) {

					// if (roundIndex == 0) {
					// for (int i = 0; i < numInputs; i++) {
					// biasParameters[biasPointIndex][i] = 0.0;
					// }
					// } else {
					//
					// for (int i = 0; i < numInputs; i++) {
					// biasParameters[biasPointIndex][i] = bestAccumulatedInputWeights[i];
					// }
					//
					// }

					if (useExhaustiveFeatureSearch) {

						biasParameters[biasPointIndex][biasPointIndex % numInputs] = 1.0;

					} else {

						// use random selections */
						int numFeaturesSelected = 0;

						while (numFeaturesSelected < numFeaturesToSelect) {

							int randomIndex = (int) (biasSelectionRandom.nextDouble() * numInputs);

							if (biasParameters[biasPointIndex][randomIndex] == 1.0) {
								continue;
							}
							biasParameters[biasPointIndex][randomIndex] = 1.0;

							numFeaturesSelected++;
						}
					}

					biasParameters[biasPointIndex][numInputs + 0] = /* biasSelectionRandom.nextDouble() * 2 * */6;
					biasParameters[biasPointIndex][numInputs + 1] = /* biasSelectionRandom.nextDouble() * 2 * */1e-6;
					biasParameters[biasPointIndex][numInputs + 2] = 0.0;
					biasParameters[biasPointIndex][numInputs + 3] = 0.0;

				}
			}

			/******************************/
			/* INTERNAL BIAS OPTIMIZATION */
			/******************************/

			for (int biasIndex = 0; biasIndex < numBiasPoints; biasIndex++) {
				biasPerformanceSum[biasIndex][0] = 0.0;
			}

			// make copy if leaveOneOut

			if (useLeaveOneOut)
				System.arraycopy(allExampleIndicies, 0, allExampleIndiciesInternalExampleCopy, 0, internalNumExamples);

			Utility.randomizeIntArrayInPlace(internalExampleSelectionRandom, allExampleIndicies, internalNumExamples);

			for (int internalValidationIndex = 0; internalValidationIndex < numInternalValidations; internalValidationIndex++) {

				// if (verbose)
				System.out.printf("internalValidationIndex  = %d\n", internalValidationIndex);

				// randomly select training and testing examples (shuffle internal examples)

				if (!useLeaveOneOut)
					Utility.randomizeIntArrayInPlace(internalExampleSelectionRandom, allExampleIndicies, internalNumExamples);
				else {
					int trainIndex = 0;
					for (int internalTrainExampleIndex = 0; internalTrainExampleIndex < internalNumExamples; internalTrainExampleIndex++) {
						if (internalTrainExampleIndex != internalValidationIndex) {
							allExampleIndicies[trainIndex++] = allExampleIndiciesInternalExampleCopy[internalTrainExampleIndex];
						}
					}
					allExampleIndicies[internalNumTrainExamples] = allExampleIndiciesInternalExampleCopy[internalValidationIndex];
				}

				double bestAccuracyThisInternalValidation = Double.NEGATIVE_INFINITY;
				int bestFeatureIndex = -1;

				long startTime = System.currentTimeMillis();

				int numBestFeatures = 0;
				for (int biasIndex = 0; biasIndex < numBiasPoints; biasIndex++) {

					int roundIndex = biasIndex / numInputs;

					if (useExhaustiveFeatureSearch && (biasIndex % numInputs == 0)) {
						System.out.printf("starting roundIndex  = %d\n", roundIndex);

						bestAccuracyThisInternalValidation = Double.NEGATIVE_INFINITY;
						bestFeatureIndex = -1;

					}

					boolean currentFeatureAlreadySelected = false;
					if (useExhaustiveFeatureSearch) {

						int featureIndex = biasIndex % numInputs;
						currentFeatureAlreadySelected = false;

						for (int j = 0; j < numBestFeatures; j++) {
							if (bestFeaturesSoFar[j] == featureIndex) {
								currentFeatureAlreadySelected = true;
								break;
							}
						}

						// if (currentFeatureAlreadySelected) {
						// continue;
						// }

					}

					if (!currentFeatureAlreadySelected) {

						// System.out.printf("biasIndex  = %d\n", biasIndex);

						double minTestConfidence = biasParameters[biasIndex][numInputs + 3];
						// System.out.printf("minTestConfidence  = %f\n", minTestConfidence);

						// create model with train examples (trivial for now)

						Model model = new Model(allExampleIndicies, internalNumTrainExamples, numInputs, numClasses, exampleInputs, exampleOutputConficences, exampleOutputClassIndicies, biasParameters[biasIndex]);

						// test model with test examples
						long numErrors = 0;
						long numTries = 0;

						int numInternalTestExamplesToUse = -1;

						if (useExhaustiveFeatureSearch) {
							numInternalTestExamplesToUse = internalNumTrainExamples;
						} else {
							numInternalTestExamplesToUse = internalNumTestExamples;
						}

						for (int internalTestExampleIndex = 0; internalTestExampleIndex < numInternalTestExamplesToUse; internalTestExampleIndex++) {

							int testExampleIndex = -1;

							if (useExhaustiveFeatureSearch) {
								testExampleIndex = internalTestExampleIndex;
							} else {
								testExampleIndex = internalNumTrainExamples + internalTestExampleIndex;
							}

							int confidence = exampleOutputConficences[allExampleIndicies[testExampleIndex]];

							if (confidence < minTestConfidence)
								continue;

							int predictedClassIndex = model.predictClass(exampleInputs[allExampleIndicies[testExampleIndex]], classWeights, testExampleIndex);
							int actualClassIndex = exampleOutputClassIndicies[allExampleIndicies[testExampleIndex]];

							// System.out.printf("\n");
							// System.out.printf("internal predictedClassIndex = %d\n", predictedClassIndex);
							// System.out.printf("internal actualClassIndex    = %d\n", actualClassIndex);

							if (predictedClassIndex != actualClassIndex) {
								numErrors++;
							}
							numTries++;
						}

						double accuracy = 1.0 - (double) numErrors / numTries;

						if (accuracy > bestAccuracyThisInternalValidation) {
							bestAccuracyThisInternalValidation = accuracy;
							bestFeatureIndex = biasIndex % numInputs;
						}

						long currentTime = System.currentTimeMillis();
						double duration = (currentTime - startTime) / 1000.0;
						double biasEvaluationsPerSecond = (biasIndex + 1) / duration;
						double secondsPerRound = numBiasPoints / biasEvaluationsPerSecond;
						double minutesPerRound = secondsPerRound / 60;
						double hoursPerRound = minutesPerRound / 60;

						if (false) {
							System.out.printf("\n");
							System.out.printf("biasIndex                          = %d\n", biasIndex);
							System.out.printf("accuracy                           = %f\n", accuracy);
							System.out.printf("bestAccuracyThisInternalValidation = %f\n", bestAccuracyThisInternalValidation);
							System.out.printf("bestFeatureIndex                   = %d\n", bestFeatureIndex);
							System.out.printf("biasEvaluationsPerSecond           = %f\n", biasEvaluationsPerSecond);
							System.out.printf("secondsPerRound                    = %f\n", secondsPerRound);
							System.out.printf("minutesPerRound                    = %f\n", minutesPerRound);
							System.out.printf("hoursPerRound                      = %f\n", hoursPerRound);
						}

						biasPerformanceSum[biasIndex][0] += accuracy;
					}

					// biasPerformance[]

					// record performance scores for bias point

					if (useExhaustiveFeatureSearch && ((biasIndex + 1) % numInputs == 0)) {

						System.out.printf("finished roundIndex  = %d\n", roundIndex);

						bestFeaturesSoFar[numBestFeatures++] = bestFeatureIndex;

						for (int j = 0; j < numBestFeatures; j++) {
							System.out.printf("bestFeaturesSoFar[%d] = %d\n", j, bestFeaturesSoFar[j]);
						}

						if (roundIndex < numRoundsOfFeatureSetExpansion) {
							for (int i = biasIndex; i < biasIndex + numInputs; i++) {
								for (int j = 0; j < numBestFeatures; j++) {
									biasParameters[biasIndex][bestFeaturesSoFar[j]] += 1.0;
								}
							}
						}

					}

				}

			}

			for (int biasIndex = 0; biasIndex < numBiasPoints; biasIndex++) {
				biasPerformance[biasIndex][0] = biasPerformanceSum[biasIndex][0] / numInternalValidations;
			}

			double bestAccuracy = Double.NEGATIVE_INFINITY;

			int bestBiasPointIndex = -1;

			for (int biasIndex = 0; biasIndex < numBiasPoints; biasIndex++) {

				if (biasPerformance[biasIndex][0] > bestAccuracy) {
					bestAccuracy = biasPerformance[biasIndex][0];
					bestBiasPointIndex = biasIndex;
				}

			}

			if (numInternalValidations == 0)
				bestBiasPointIndex = 0;

			if (false)
				for (int i = 0; i < numBiasSpaceDimensions; i++) {
					System.out.printf("biasParameters[%d]  = %f\n", i, biasParameters[bestBiasPointIndex][i]);
				}

			double internalBestAccuracy = bestAccuracy;

			// System.out.printf("internalAccuracy = %f\n",internalAccuracy );

			{

				Utility.randomizeIntArrayInPlace(internalExampleSelectionRandom, allExampleIndicies, internalNumExamples);
				Model model = new Model(allExampleIndicies, internalNumTrainExamples, numInputs, numClasses, exampleInputs, exampleOutputConficences, exampleOutputClassIndicies, biasParameters[bestBiasPointIndex]);

				// test model with external test examples
				long numErrors = 0;
				long numTries = 0;

				double minTestConfidence = biasParameters[bestBiasPointIndex][numInputs + 3];

				for (int externalTestExampleIndex = 0; externalTestExampleIndex < externalNumTestExamples; externalTestExampleIndex++) {
					int confidence = exampleOutputConficences[allExampleIndicies[internalNumExamples + externalTestExampleIndex]];

					if (confidence < minTestConfidence)
						continue;

					int predictedClassIndex = model.predictClass(exampleInputs[allExampleIndicies[internalNumExamples + externalTestExampleIndex]], classWeights, -1);
					int actualClassIndex = exampleOutputClassIndicies[allExampleIndicies[internalNumExamples + externalTestExampleIndex]];

					// System.out.printf("\n");
					// System.out.printf("external predictedClassIndex = %d\n", predictedClassIndex);
					// System.out.printf("external actualClassIndex    = %d\n", actualClassIndex);

					boolean correctClassification = true;
					int accuracy = 1;
					if (predictedClassIndex != actualClassIndex) {
						correctClassification = false;
						accuracy = 0;
						numErrors++;
					}
					numTries++;

					/* compute machine prediction confidence */
					double secondBestClassWeight = Double.NEGATIVE_INFINITY;
					double weightSum = 0.0;
					for (int i = 0; i < numClasses; i++) {

						weightSum += classWeights[i];
						if (i == predictedClassIndex) {
							continue;
						}
						if (classWeights[i] > secondBestClassWeight) {
							secondBestClassWeight = classWeights[i];
						}

					}

					if (weightSum == 0)
						weightSum = 1.0;

					double machinePredictionConfidence = (classWeights[predictedClassIndex] - secondBestClassWeight) / weightSum;

					// System.out.printf("classWeights[predictedClassIndex] = %9.6f\n", classWeights[predictedClassIndex]);
					// System.out.printf("secondBestClassWeight             = %9.6f\n", secondBestClassWeight);
					// System.out.printf("weightSum                         = %9.6f\n", weightSum);

					if (false)
						System.out.printf("EXTERNAL_CLASSIFICATION\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%9.6f\n", //
								numInternalValidations, numBiasPoints, numExamples, internalNumTrainExamples, internalNumTestExamples, externalNumTestExamples, numExternalValidations, externalValidationIndex, //
								externalTestExampleIndex, //
								predictedClassIndex, actualClassIndex, accuracy, confidence, machinePredictionConfidence);
				}

				double externalAccuracy = 1.0 - (double) numErrors / numTries;
				double optimism = internalBestAccuracy - externalAccuracy;

				System.out.printf("EXTERNAL_EVALUATION\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%9.6f\t%9.6f\t%9.6f\t%9.6f\t%9.6f\t%9.6f\t%9.6f\n", //
						numInternalValidations, numBiasPoints, numExamples, internalNumTrainExamples, internalNumTestExamples, externalNumTestExamples, numExternalValidations, externalValidationIndex, //
						widthWeights[bestBiasPointIndex], radialColorMeanWeights[bestBiasPointIndex], radialColorStdWeights[bestBiasPointIndex], textureWeights[bestBiasPointIndex],
						//
						internalBestAccuracy, externalAccuracy, optimism);

				predictedAccuracies[externalValidationIndex] = internalBestAccuracy;
				actualAccuracies[externalValidationIndex] = externalAccuracy;
				actualOptimism[externalValidationIndex] = optimism;

				int numObservations = externalValidationIndex + 1;

				double predictedAccuracyMean = Utility.computeSampleMean(predictedAccuracies, numObservations);
				double predictedAccuracySTD = Utility.computeSampleSTD(predictedAccuracies, predictedAccuracyMean, numObservations);
				double predictedAccuracySEM = Utility.computeStandardErrorOfMean(predictedAccuracySTD, numObservations);

				double actualAccuracyMean = Utility.computeSampleMean(actualAccuracies, numObservations);
				double actualAccuracySTD = Utility.computeSampleSTD(actualAccuracies, actualAccuracyMean, numObservations);
				double actualAccuracySEM = Utility.computeStandardErrorOfMean(actualAccuracySTD, numObservations);

				double actualOptimismMean = Utility.computeSampleMean(actualOptimism, numObservations);
				double actualOptimismSTD = Utility.computeSampleSTD(actualOptimism, actualOptimismMean, numObservations);
				double actualOptimismSEM = Utility.computeStandardErrorOfMean(actualOptimismSTD, numObservations);

				System.out.printf("actualAccuracy    = \t%9.6f\t%9.6f\t%9.6f\n", actualAccuracyMean, actualAccuracySTD, actualAccuracySEM);
				System.out.printf("predictedAccuracy = \t%9.6f\t%9.6f\t%9.6f\n", predictedAccuracyMean, predictedAccuracySTD, predictedAccuracySEM);
				System.out.printf("accuracyOptimism  = \t%9.6f\t%9.6f\t%9.6f\n", actualOptimismMean, actualOptimismSTD, actualOptimismSEM);

			}

			// use external examples to compute bias x objective space

			for (int biasIndex = 0; biasIndex < numBiasPoints; biasIndex++) {
				// create model with a random subset of the internal train examples of EQUAL size as before
				// test model with external test examples
				// accumulate / record performance scores for bias point
			}

			// compare internal and external bias x objective spaces
			// claim #1: no single point in bias space will have biased accuracies
			// claim #2: the process of selecting the "best" bias point and using it will lead to overfitting

		}

	}
}

class Model {

	int[] allExampleIndicies;
	int internalNumTrainExamples;
	double[][] exampleInputs;
	int[] exampleOutputConfidences;
	int[] exampleOutputClassIndicies;
	double[] biasParameters;
	int numInputs;
	int numClasses;
	double[] inputMinValues;
	double[] inputMaxValues;
	double[] inputRanges;

	// double[] featureWeights;
	double distanceWeightingPower;
	double minDistance;
	double minTrainConfidence;
	int numActiveInputs;
	int[] activeInputFeatureIndices;
	double[] activeInputFeatureWeights;

	Model(int[] allExampleIndicies, int internalNumTrainExamples, int numInputs, int numClasses, double[][] exampleInputs, int[] exampleOutputConfidences, int[] exampleOutputClassIndicies, double[] biasParameters) {

		this.allExampleIndicies = allExampleIndicies;
		this.numInputs = numInputs;
		this.numClasses = numClasses;
		this.internalNumTrainExamples = internalNumTrainExamples;
		this.exampleInputs = exampleInputs;
		this.exampleOutputConfidences = exampleOutputConfidences;
		this.exampleOutputClassIndicies = exampleOutputClassIndicies;
		this.biasParameters = biasParameters;

		// create active feature list //
		numActiveInputs = 0;
		for (int i = 0; i < numInputs; i++) {
			if (biasParameters[i] > 0)
				numActiveInputs++;
		}

		activeInputFeatureIndices = new int[numActiveInputs];
		activeInputFeatureWeights = new double[numActiveInputs];
		numActiveInputs = 0;
		for (int i = 0; i < numInputs; i++) {
			if (biasParameters[i] > 0) {
				activeInputFeatureIndices[numActiveInputs] = i;
				activeInputFeatureWeights[numActiveInputs] = biasParameters[i];
				numActiveInputs++;
			}
		}

		//
		// featureWeights = new double[numInputs];
		// for (int i = 0; i < numInputs; i++) {
		// featureWeights[i] = biasParameters[i];
		// }

		distanceWeightingPower = biasParameters[numInputs + 0];
		minDistance = biasParameters[numInputs + 1];
		minTrainConfidence = biasParameters[numInputs + 2];

	}

	int predictClass(double[] testInputValues, double[] outputClassWeights, int exampleIndexToPredict) {

		for (int i = 0; i < numClasses; i++) {
			outputClassWeights[i] = 0.0;
		}

		double overallDifferenceSum;
		double[] trainInputValues;
		for (int exampleIndex = 0; exampleIndex < internalNumTrainExamples; exampleIndex++) {

			if (exampleIndex == exampleIndexToPredict) {

				continue;
			}

			// if (exampleOutputConfidences[allExampleIndicies[exampleIndex]] < minTrainConfidence) {
			// continue;
			// }

			int ei = allExampleIndicies[exampleIndex];

			trainInputValues = exampleInputs[ei];

			overallDifferenceSum = 0.0;
			int i;
			for (int ai = 0; ai < numActiveInputs; ai++) {
				i = activeInputFeatureIndices[ai];
				overallDifferenceSum += Math.abs(activeInputFeatureWeights[ai] * (testInputValues[i] - trainInputValues[i]));
			}

			outputClassWeights[exampleOutputClassIndicies[ei]] += 1.0 / (overallDifferenceSum * overallDifferenceSum);

			// boolean fastestMode = true;
			//
			// boolean fastMode = false;
			//
			// if (fastestMode) {
			// outputClassWeights[exampleOutputClassIndicies[allExampleIndicies[exampleIndex]]] += 1.0 / (overallDifferenceSum * overallDifferenceSum);
			// } else if (fastMode) {
			// double distance = overallDifferenceSum;
			// if (distance < minDistance)
			// distance = minDistance;
			//
			// outputClassWeights[exampleOutputClassIndicies[allExampleIndicies[exampleIndex]]] += 1.0 / (distance * distance);
			// } else {
			//
			// double distance = overallDifferenceSum;
			// if (distance < minDistance)
			// distance = minDistance;
			//
			// double weight = 1.0 / Math.pow(distance, distanceWeightingPower);
			//
			// if (Double.isNaN(weight)) {
			// // System.out.printf("setting weight = 0.0\n");
			// weight = 0.0;
			// }
			//
			// outputClassWeights[exampleOutputClassIndicies[allExampleIndicies[exampleIndex]]] += weight;
			// }

		}

		if (false) {

			double sum = 0.0;
			for (int i = 0; i < numClasses; i++) {

				System.out.printf("outputClassWeights[%d] = %f\n", i, outputClassWeights[i]);

				sum += outputClassWeights[i];
			}

			System.out.printf("sum = %f\n", sum);

		}

		double bestClassWeight = Double.NEGATIVE_INFINITY;
		int bestClassIndex = -1;
		for (int i = 0; i < numClasses; i++) {
			if (outputClassWeights[i] > bestClassWeight) {
				bestClassWeight = outputClassWeights[i];
				bestClassIndex = i;
			}

		}

		// System.out.printf("bestClassIndex = %d\n", bestClassIndex);

		return bestClassIndex;
	}
}

class ParseArgs {

	public static HashMap<String, String> parse(String[] args) {

		HashMap<String, String> argHashMap = new HashMap<String, String>();

		int numArgs = args.length / 2;

		int index = 0;
		for (int i = 0; i < numArgs; i++) {

			String key = args[index++];
			String value = args[index++];

			argHashMap.put(key.substring(1), value);
		}

		return argHashMap;

	}

}

/*
 * 
 * 
 * 
 * //biasParameters[0] = 1.000000 //biasParameters[1] = 1.069378 //biasParameters[2] = 0.001903 //averageExternalAccuracy = 0.372304 averageAccuracyOptimism = 0.005021
 * 
 * 
 * 
 * biasParameters[0] = 0.000000 biasParameters[1] = 0.000000 biasParameters[2] = 0.000000 biasParameters[3] = 0.230055 biasParameters[4] = 0.336673 biasParameters[5] = 0.991268 biasParameters[6] = 0.456023 biasParameters[7] = 0.220306 biasParameters[8] =
 * 0.011866 biasParameters[9] = 0.226997 biasParameters[10] = 6.660094 biasParameters[11] = 0.004003 biasParameters[12] = 9.000000 biasParameters[13] = 9.000000 averageExternalAccuracy = 0.270343 averageAccuracyOptimism = 0.028793
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * outputClassWeights[0] = 621.000000 outputClassWeights[1] = 987.000000 outputClassWeights[2] = 761.000000 outputClassWeights[3] = 657.000000 outputClassWeights[4] = 581.000000 outputClassWeights[5] = 365.000000 outputClassWeights[6] = 772.000000
 * outputClassWeights[7] = 184.000000 outputClassWeights[8] = 234.000000 outputClassWeights[9] = 782.000000 outputClassWeights[10] = 788.000000 outputClassWeights[11] = 265.000000 outputClassWeights[12] = 257.000000 outputClassWeights[13] = 417.000000
 * outputClassWeights[14] = 281.000000 outputClassWeights[15] = 221.000000 outputClassWeights[16] = 244.000000 outputClassWeights[17] = 169.000000 outputClassWeights[18] = 232.000000 outputClassWeights[19] = 224.000000 outputClassWeights[20] = 233.000000
 * outputClassWeights[21] = 302.000000 outputClassWeights[22] = 172.000000 outputClassWeights[23] = 170.000000 outputClassWeights[24] = 155.000000 outputClassWeights[25] = 166.000000 outputClassWeights[26] = 106.000000 outputClassWeights[27] = 152.000000
 * outputClassWeights[28] = 164.000000 outputClassWeights[29] = 170.000000 outputClassWeights[30] = 127.000000 outputClassWeights[31] = 114.000000 outputClassWeights[32] = 146.000000 outputClassWeights[33] = 106.000000 outputClassWeights[34] = 160.000000
 * outputClassWeights[35] = 134.000000 outputClassWeights[36] = 168.000000 outputClassWeights[37] = 129.000000 outputClassWeights[38] = 80.000000 outputClassWeights[39] = 94.000000 outputClassWeights[40] = 137.000000 outputClassWeights[41] = 79.000000
 * outputClassWeights[42] = 89.000000 outputClassWeights[43] = 51.000000 outputClassWeights[44] = 64.000000 outputClassWeights[45] = 43.000000 outputClassWeights[46] = 74.000000 outputClassWeights[47] = 54.000000 outputClassWeights[48] = 68.000000
 * outputClassWeights[49] = 58.000000 outputClassWeights[50] = 72.000000 outputClassWeights[51] = 105.000000 outputClassWeights[52] = 45.000000 outputClassWeights[53] = 52.000000 outputClassWeights[54] = 52.000000 outputClassWeights[55] = 39.000000
 * outputClassWeights[56] = 42.000000 outputClassWeights[57] = 46.000000 outputClassWeights[58] = 52.000000 outputClassWeights[59] = 24.000000 outputClassWeights[60] = 27.000000 outputClassWeights[61] = 27.000000 outputClassWeights[62] = 20.000000
 * outputClassWeights[63] = 18.000000 outputClassWeights[64] = 28.000000 outputClassWeights[65] = 27.000000 outputClassWeights[66] = 22.000000 outputClassWeights[67] = 19.000000 outputClassWeights[68] = 18.000000 outputClassWeights[69] = 22.000000
 * outputClassWeights[70] = 19.000000 outputClassWeights[71] = 19.000000 outputClassWeights[72] = 13.000000 outputClassWeights[73] = 13.000000 outputClassWeights[74] = 8.000000 outputClassWeights[75] = 4.000000 outputClassWeights[76] = 3.000000
 * outputClassWeights[77] = 3.000000 outputClassWeights[78] = 1.000000 outputClassWeights[79] = 0.000000 outputClassWeights[80] = 0.000000 outputClassWeights[81] = 0.000000 outputClassWeights[82] = 0.000000 outputClassWeights[83] = 0.000000
 * outputClassWeights[84] = 0.000000 outputClassWeights[85] = 0.000000 outputClassWeights[86] = 0.000000 outputClassWeights[87] = 0.000000 outputClassWeights[88] = 0.000000 outputClassWeights[89] = 0.000000 outputClassWeights[90] = 0.000000
 * outputClassWeights[91] = 0.000000 outputClassWeights[92] = 0.000000 outputClassWeights[93] = 0.000000 outputClassWeights[94] = 0.000000 outputClassWeights[95] = 0.000000 outputClassWeights[96] = 0.000000 outputClassWeights[97] = 0.000000
 * outputClassWeights[98] = 0.000000 outputClassWeights[99] = 0.000000 outputClassWeights[100] = 0.000000 outputClassWeights[101] = 0.000000 outputClassWeights[102] = 0.000000 outputClassWeights[103] = 0.000000 outputClassWeights[104] = 0.000000
 * outputClassWeights[105] = 0.000000 outputClassWeights[106] = 0.000000 outputClassWeights[107] = 0.000000 outputClassWeights[108] = 0.000000 outputClassWeights[109] = 0.000000 outputClassWeights[110] = 0.000000 outputClassWeights[111] = 0.000000
 * outputClassWeights[112] = 0.000000 outputClassWeights[113] = 0.000000 outputClassWeights[114] = 0.000000 outputClassWeights[115] = 0.000000 outputClassWeights[116] = 0.000000 outputClassWeights[117] = 0.000000 outputClassWeights[118] = 0.000000
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * actualAccuracy = 0.294410 0.145021 0.011429 predictedAccuracy = -Infinity NaN NaN
 * 
 * 
 * 
 * using width only baseline : actualAccuracy = 0.343814 0.043671 0.004434 predictedAccuracy = 0.336701 0.047362 0.004809 accuracyOptimism = -0.007113 0.064565 0.006556
 */
