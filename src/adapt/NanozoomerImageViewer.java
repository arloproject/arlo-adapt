/*
 Copyright (c) 2012 University of Illinois Board of Trustees
 All rights reserved.
 
 Developed by: David Tcheng
               Illinois Informatics Institute
               University of Illinois at Urbana-Champaign
               www.informatics.illinois.edu/icubed/
               
Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the
"Software"), to deal with the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:
 
 Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimers.
 Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimers in the
documentation and/or other materials provided with the distribution.
 Neither the names of <Name of Development Group, Name of Institution>,
nor the names of its contributors may be used to endorse or promote
products derived from this Software without specific prior written
permission.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
*/

package adapt;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.GraphicsDevice;
import java.awt.Image;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.VolatileImage;
import java.io.File;
import java.io.IOException;
import java.util.Date;

import javax.imageio.ImageIO;

public class NanozoomerImageViewer extends Component {

	VolatileImage backBuffer = null;
	BufferedImage image = null;
	Frame mainFrame = null;
	NanozoomerImageViewer viewer = null;
	RawNanoReader rnr = null;
	KeyListener keyListener = null;
	GraphicsDevice device = null;

	int width;
	int height;
	int taggableWidth;
	int taggableHeight;

	String imageText;

	int count;

	NanozoomerImageViewer(BufferedImage image, RawNanoReader rawNanoReader) {
		this.image = image;
		this.rnr = rawNanoReader;
		this.device = rnr.device;
		this.count = 0;
	}

	public void paint(Graphics graphics) {
		
		
		

		count++;

		if (image == null)
			return;
		
		
		Graphics imageGraphics = image.getGraphics();

		Font font = new Font("Verdana", Font.BOLD, 24);
		imageGraphics.setFont(font);
		imageGraphics.setColor(Color.BLACK);

		if (false) {
			try {
				File outputfile = new File("/home/dtcheng/image." + count + ".bmp");
				ImageIO.write((BufferedImage) image, "bmp", outputfile);
			} catch (IOException e) {
				System.exit(1);
			}
		}

		if (!rnr.answerShownForTrial[rnr.trialIndex] && rnr.markSpot) {

			int xDiff = (rnr.mousePressX - rnr.mouseDraggedX);
			int yDiff = (rnr.mousePressY - rnr.mouseDraggedY);
			int distance = (int) Math.sqrt((xDiff * xDiff) + (yDiff * yDiff));

			int tagWidth = distance * 2;
			// int hieght = Math.abs(rawNanoReader.mousePressY - rawNanoReader.mouseDraggedY) * 2;
			int tagHeight = tagWidth;

			// int x = (rawNanoReader.mousePressX + rawNanoReader.mouseDraggedX) / 2;
			// int y = (rawNanoReader.mousePressY + rawNanoReader.mouseDraggedY) / 2;

			int x = rnr.mousePressX;
			int y = rnr.mousePressY;

			imageGraphics.drawOval(x - tagWidth / 2, y - tagHeight / 2, tagWidth, tagHeight);

			String label = (int) (tagWidth * 226.0 / 1000) + "um";
			imageGraphics.drawString(label, x - imageGraphics.getFontMetrics().bytesWidth(label.getBytes(), 0, label.length()) / 2, y + imageGraphics.getFontMetrics().getHeight() / 2);

			if (rnr.createExamplePhase1) {

				if (tagWidth > rnr.minTagWidth) {
					rnr.trialClassificationX[rnr.trialIndex][rnr.trialNumTags[rnr.trialIndex]] = x;
					rnr.trialClassificationY[rnr.trialIndex][rnr.trialNumTags[rnr.trialIndex]] = y;
					rnr.trialClassificationZ[rnr.trialIndex][rnr.trialNumTags[rnr.trialIndex]] = rnr.zPlaneIndex;
					rnr.trialClassificationW[rnr.trialIndex][rnr.trialNumTags[rnr.trialIndex]] = tagWidth;
					rnr.trialClassificationText[rnr.trialIndex][rnr.trialNumTags[rnr.trialIndex]] = "";
					rnr.trialNumTags[rnr.trialIndex]++;
				}
				rnr.createExamplePhase1 = false;
				rnr.markSpot = false;
			}
		}

		if (!rnr.showingAnswer) {

			RawNanoReader rnrToShow = rnr;

			if (!rnr.computeAndShowProbabilityField) {
				imageGraphics.setColor(Color.red);
			} else {
				imageGraphics.setColor(Color.red);
			}

			for (int i = 0; i < rnrToShow.trialNumTags[rnr.trialIndex]; i++) {

				int x = rnrToShow.trialClassificationX[rnr.trialIndex][i];
				int y = rnrToShow.trialClassificationY[rnr.trialIndex][i];
				int z = rnrToShow.trialClassificationZ[rnr.trialIndex][i];
				int d = rnrToShow.trialClassificationW[rnr.trialIndex][i];
				String label = rnrToShow.trialClassificationText[rnr.trialIndex][i];
				// g.setColor(Color.black);
				// g.fillOval(x - d / 2, y - d / 2, d, d);
				imageGraphics.drawOval(x - d / 2, y - d / 2, d, d);

				imageGraphics.drawString(label, x - imageGraphics.getFontMetrics().bytesWidth(label.getBytes(), 0, label.length()) / 2, y + imageGraphics.getFontMetrics().getHeight() / 2);
			}
		}

		if (rnr.testerMode && rnr.showingAnswer) {

			imageGraphics.setColor(Color.red);
			RawNanoReader rnrToShow = rnr.masterRNR;

			for (int i = 0; i < rnrToShow.trialNumTags[rnr.trialIndex]; i++) {

				int x = rnrToShow.trialClassificationX[rnr.trialIndex][i];
				int y = rnrToShow.trialClassificationY[rnr.trialIndex][i];
				int z = rnrToShow.trialClassificationZ[rnr.trialIndex][i];
				int d = rnrToShow.trialClassificationW[rnr.trialIndex][i];
				String label = rnrToShow.trialClassificationText[rnr.trialIndex][i];
				// g.setColor(Color.black);
				// g.fillOval(x - d / 2, y - d / 2, d, d);
				imageGraphics.drawOval(x - d / 2, y - d / 2, d, d);

				imageGraphics.drawString(label, x - imageGraphics.getFontMetrics().bytesWidth(label.getBytes(), 0, label.length()) / 2, y + imageGraphics.getFontMetrics().getHeight() / 2);
			}

		}

		if (rnr.showStatusLine && !rnr.computeAndShowProbabilityField) {

			imageGraphics.setColor(Color.black);
			imageGraphics.drawString("Round #" + (rnr.trialViewRounds[rnr.trialIndex] + 1) + "  Slide #" + (rnr.trialViewSlides[rnr.trialIndex] + 1) + "  Zplane = " + rnr.zPlaneIndex /* + "  count = " + count */, 0, 20);

			String message = "";

			if (rnr.testerMode) {
				if (rnr.showingAnswer) {
					message = "THE EXPERT PREDICTED THIS";
				} else {
					message = "WHAT DO YOU PREDICT?";
				}
			}

			int x = width / 2 - imageGraphics.getFontMetrics().bytesWidth(message.getBytes(), 0, message.length()) / 2;

			if (rnr.testerMode) {
				if (rnr.showingAnswer) {
					imageGraphics.setColor(Color.red);
				} else {
					imageGraphics.setColor(Color.black);
				}
			} else {
				imageGraphics.setColor(Color.black);
			}
			imageGraphics.drawString(message, x, 20);
		}

		int xPad = (width - taggableWidth) / 2 - 1;
		int yPad = (height - taggableHeight) / 2 - 1;

		if (rnr.testerMode) {
			if (rnr.showingAnswer) {
				imageGraphics.setColor(Color.red);
			} else {
				imageGraphics.setColor(Color.black);
			}
		} else {
			imageGraphics.setColor(Color.black);
		}

		if (rnr.showTaggingRegionBox) {

			imageGraphics.drawRect(xPad, yPad, taggableWidth + 2, taggableHeight + 2);
		}

		// mainFrame.setTitle(imageText);

		


		if (rnr.writeImagesToDiskAfterGeneration) {
			File outputfile = new File("images/" + new Date(System.currentTimeMillis()) + "." + (rnr.zPlaneIndex ) + ".png");
			try {
				ImageIO.write(image, "png", outputfile);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		
		
		Graphics g2 = null;
		BufferStrategy bufferStrategy = null;
		if (rnr.useBuffering) {

			bufferStrategy = mainFrame.getBufferStrategy();

			g2 = bufferStrategy.getDrawGraphics();

		} else {

			g2 = graphics;
		}
		
		
		g2.drawImage(image, 0, 0, width, height, 0, 0, width, height, this);

		if (rnr.useBuffering) {

			bufferStrategy.show();
			g2.dispose();

		}
		



	}

	public void createFrame(int width, int height, BufferedImage image) {

		this.image = image;
		this.width = width;
		this.height = height;

		mainFrame = new Frame();
		mainFrame.setAlwaysOnTop(false);

		mainFrame.setIgnoreRepaint(false);
		mainFrame.setUndecorated(true);

		if (rnr.useBuffering) {
			device.setFullScreenWindow(mainFrame);
			mainFrame.createBufferStrategy(rnr.numberBuffers);
		}
		// device.setDisplayMode(null);

		mainFrame.setSize(width, height);

		mainFrame.setLocation(0, 0);
//		mainFrame.setOpacity(1.0f);
		viewer = this;

		mainFrame.add(viewer);

		mainFrame.addMouseListener(rnr.mouseListener);
		mainFrame.addMouseWheelListener(rnr.mouseWheelListener);
		mainFrame.addMouseMotionListener(rnr.mouseMotionListener);
		mainFrame.addKeyListener(rnr.keyListener);

		// if (rnr.showWindow)
		mainFrame.setVisible(true);

		System.out.println(mainFrame.getSize().getHeight());
		System.out.println(mainFrame.getSize().getWidth());
	}

}
