/*
 Copyright (c) 2012 University of Illinois Board of Trustees
 All rights reserved.
 
 Developed by: David Tcheng
               Illinois Informatics Institute
               University of Illinois at Urbana-Champaign
               www.informatics.illinois.edu/icubed/
               
Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the
"Software"), to deal with the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:
 
 Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimers.
 Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimers in the
documentation and/or other materials provided with the distribution.
 Neither the names of <Name of Development Group, Name of Institution>,
nor the names of its contributors may be used to endorse or promote
products derived from this Software without specific prior written
permission.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 */

package adapt;

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * @author dtcheng
 */
public class IO {

	/** Creates a new instance of Utility */
	public IO() {
	}

	static public void p(String string) {

		System.out.println(string);

	}

	static public void p(byte value) {

		System.out.println((char) value);

	}

	static public void p(int value) {

		System.out.println(value);

	}

	static public void println(String string) {

		System.out.println(string);

	}

	static public void print(String string) {

		System.out.print(string);

	}

	static public String getMethod() {

		return Thread.currentThread().getStackTrace()[2].toString().split("\\(")[0];

	}

	static public String getMethod(int index) {

		return Thread.currentThread().getStackTrace()[index].toString().split("\\(")[0];

	}

	static public void printCurrentMethod() {

		String method = Thread.currentThread().getStackTrace()[2].toString().split("\\(")[0];

		p("Current Method = " + method);

	}

	static public void pm() {

		String method = Thread.currentThread().getStackTrace()[2].toString().split("\\(")[0];

		p("Current Method = " + method);

	}

	static public void pms() {

		String method = Thread.currentThread().getStackTrace()[2].toString().split("\\(")[0];

		p("Starting Method " + method);

	}

	static public void pme() {

		String method = Thread.currentThread().getStackTrace()[2].toString().split("\\(")[0];

		p("Ending Method " + method);

	}

	static public String getFileNameFromPath(String path, String delimiter) {

		int length = path.length();

		int index = path.lastIndexOf(delimiter);

		return path.substring(index, length);

	}

	static public boolean fileDoesNotExists(String path) {
		File file = new File(path);
		return !file.exists();
	}

	static public boolean fileExists(String path) {
		File file = new File(path);
		return file.exists();
	}

	static public boolean delete(String path) {
		File file = new File(path);
		return file.delete();
	}

	static public long fileLength(String path) {
		File file = new File(path);
		return file.length();
	}

	static public String getTranslatedPath(String path) {

		String originalPathRoot = null;
		String originalPathRest = null;

		if (path.charAt(1) == ':') {
			String[] parts = path.split("\\\\");
			originalPathRest = parts[2] + "/" + parts[3] + "/" + parts[4];
		} else if (path.charAt(0) == '\\' && path.charAt(1) == '\\') {
			String[] parts = path.split("\\\\");
			originalPathRest = parts[5] + "/" + parts[6] + "/" + parts[7];
		} else if (path.charAt(0) == '/') {
			String[] parts = path.split("/");
			originalPathRest = parts[2] + "/" + parts[3] + "/" + parts[4];
		} else {
			System.out.println("can't parse path = " + path);
		}

		String translatedPath = "/music/mbft/" + originalPathRest;

		return translatedPath;
	}

	static public String getRelativePath(String path) {

		String originalPathRoot = null;
		String originalPathRest = null;

		if (path.charAt(1) == ':') {
			originalPathRoot = path.substring(0, 3);
			originalPathRest = path.substring(3, path.length());
		} else if (path.charAt(0) == '\\' && path.charAt(1) == '\\') {
			int index = path.indexOf("\\", 3);
			index = path.indexOf("\\", index + 1);
			originalPathRest = path.substring(index + 1, path.length());
		} else if (path.charAt(0) == '/') {
			int index = path.indexOf("/", 1);
			index = path.indexOf("/", index + 1);
			index = path.indexOf("/", index + 1);
			originalPathRest = path.substring(index + 1, path.length());
		} else {
			System.out.println("can't parse path = " + path);
		}

		return originalPathRest;
	}

	static public String getDirectoryNameFromFilePath(String path) {

		String name = null;

		int index = path.lastIndexOf("/");

		name = path.substring(0, index);

		return name;
	}

	static public String getFileNameFromPath(String path) {

		String name = null;

		int index = path.lastIndexOf(File.separatorChar);

		name = path.substring(index + 1);

		return name;
	}

	public static Object readObject(String filename) {
		File file = new File(filename);
		return readObject(file);
	}

	public static Object readObject(File inputFile) {

		if (!inputFile.exists()) {
			throw new RuntimeException("The file does not exist!\nFile: " + inputFile.getAbsolutePath());
		}
		if (!inputFile.canRead()) {
			throw new RuntimeException("Unable to read from file: " + inputFile.getAbsolutePath());
		}

		FileInputStream fileInputStream;
		try {
			fileInputStream = new FileInputStream(inputFile);
		} catch (FileNotFoundException fnf) {
			throw new RuntimeException("File could not be opened for reading,\n either file exists but is a directory " + "rather than a regular file, does not exist, or cannot be opened for some other reason.", fnf);
		}
		ObjectInputStream objectInputStream;
		try {
			objectInputStream = new ObjectInputStream(fileInputStream);
		} catch (IOException ioe) {
			throw new RuntimeException("I/O error occured while reading stream header", ioe);
		}
		Object theOb;
		try {
			theOb = objectInputStream.readObject();
			objectInputStream.close();
		} catch (ClassNotFoundException cnf) {
			throw new RuntimeException("The class of the Serialized Object could not be found!", cnf);
		} catch (InvalidClassException ice) {
			throw new RuntimeException("Something is wrong with a class used by Serialization (wrong JRE version?)!", ice);
		} catch (StreamCorruptedException sce) {
			throw new RuntimeException("Control information in the stream is inconsistent!", sce);
		} catch (IOException ioe) {
			throw new RuntimeException("I/O error occured while reading in Object", ioe);
		}

		return theOb;
	}

	static public Object readObjectFromStream(InputStream stream) {

		ObjectInputStream inputFromStream = null;
		try {
			inputFromStream = new ObjectInputStream(stream);

			Object object = inputFromStream.readObject();

			return object;
		} catch (StreamCorruptedException ex) {
			Logger.getLogger(IO.class.getName()).log(Level.SEVERE, "StreamCorruptedException", ex);
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(IO.class.getName()).log(Level.SEVERE, "ClassNotFoundException", ex);
		} catch (IOException ex) {
			Logger.getLogger(IO.class.getName()).log(Level.SEVERE, "IOException", ex);
		} finally {
			try {
				inputFromStream.close();
			} catch (IOException ex) {
				Logger.getLogger(IO.class.getName()).log(Level.SEVERE, "IOException on inputFromStream.close()", ex);
			}
		}

		return null;
	}

	public static void rename(String oldFilePath, String newFilePath) {
		File file = new File(oldFilePath);
		file.renameTo(new File(newFilePath));
	}

	public static void createDirectoryPathForFile(String filePath) {
		String directoryPath = filePath.substring(0, filePath.lastIndexOf(File.separator));
		File directory = new File(directoryPath);
		directory.mkdirs();
	}

	public static void writeObject(String filename, Object object) {
		File file = new File(filename);

		writeObject(file, object);
	}

	public static void writeObject(File outputFile, Object object) {

		FileOutputStream fw;
		try {
			fw = new FileOutputStream(outputFile, false);
		} catch (FileNotFoundException fnf) {
			throw new RuntimeException("File could not be opened for writing,\n either file exists but is a directory rather than a regular file, " + "does not exist but cannot be created, or cannot be opened for some other reason.", fnf);
		}
		ObjectOutputStream out;
		try {
			out = new ObjectOutputStream(fw);
		} catch (IOException ioe) {
			throw new RuntimeException("I/O error occured while writing stream header", ioe);
		}
		try {
			out.writeObject(object);
		} catch (NotSerializableException nse) {
			throw new RuntimeException("The input Object is not Serializable and can't be written to a file", nse);
		} catch (IOException ioe) {
			throw new RuntimeException("I/O error occured while writing out Object", ioe);
		}
		try {
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	static public byte[] convertObjectToBytes(Object object) {

		byte[] buf = null;
		try {

			// Serialize to a byte array
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutput out = new ObjectOutputStream(bos);
			out.writeObject(object);
			out.close();

			// Get the bytes of the serialized object
			buf = bos.toByteArray();

		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}

		return buf;
	}

	static public Object convertBytesToObject(byte[] bytes) {

		Object object = null;
		try {
			// Deserialize from a byte array
			ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(bytes));
			object = (Object) in.readObject();
			in.close();

		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}

		return object;
	}

	static public Object convertBytesToObject(InputStream stream) {

		Object object = null;
		try {
			// Deserialize from a byte array
			ObjectInputStream in = new ObjectInputStream(stream);
			object = (Object) in.readObject();
			in.close();

		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}

		return object;
	}

	static public Vector getMP3PathStrings(String rootMP3DirectoryPath, String fileNameFilterString) {

		Vector mp3FilePaths = new Vector();

		System.out.println("rootMP3DirectoryPath = " + rootMP3DirectoryPath);

		File file = new File(rootMP3DirectoryPath);

		Hashtable fileNameHashtable = new Hashtable();
		int numUniqueFileNames = 0;

		int numMP3Files = 0;
		if (file.isDirectory()) {

			String[] genreDirectoryNames = file.list();

			for (int d = 0; d < genreDirectoryNames.length; d++) {

				if (!genreDirectoryNames[d].equalsIgnoreCase("Incomplete")) {

					String path = rootMP3DirectoryPath + "/" + genreDirectoryNames[d];

					File genreDirectory = new File(path);

					if (genreDirectory.isDirectory()) {

						String[] mp3FileNames = genreDirectory.list();

						for (int f = 0; f < mp3FileNames.length; f++) {

							String fileName = mp3FileNames[f];

							Object result = fileNameHashtable.get(fileName);

							if ((result == null) && (fileName.endsWith(".mp3") || fileName.endsWith(".MP3")) && ((fileNameFilterString == null) || (fileName.indexOf(fileNameFilterString) != -1))) {

								mp3FilePaths.add(path + "/" + fileName);

								fileNameHashtable.put(fileName, new Integer(numUniqueFileNames++));
							}
						}
					}

				}
			}

		}
		return mp3FilePaths;
	}

	static public String[] getPathStrings(String searchDirectoryString) {

		Vector<File> searchDirectoryFileVector = new Vector<File>();
		Vector<File> resultFileVector = new Vector<File>();
		searchDirectoryFileVector.add(new File(searchDirectoryString));
		getPathStrings(searchDirectoryFileVector, resultFileVector);
		int numFiles = resultFileVector.size();
		String[] results = new String[numFiles];
		for (int i = 0; i < results.length; i++) {
			results[i] = resultFileVector.get(i).getAbsolutePath();
		}
		return results;
	}

	static public void getPathStrings(Vector<File> searchDirectoryFileVector, Vector<File> resultDirectoryVector, Vector<File> resultFileVector) {
		getPathStrings(searchDirectoryFileVector, resultDirectoryVector, resultFileVector, true);
	}

	static public void getPathStrings(Vector<File> searchDirectoryFileVector, Vector<File> resultFileVector) {
		getPathStrings(searchDirectoryFileVector, null, resultFileVector, true);
	}

	static public void getPathStrings(Vector<File> searchDirectoryFileVector, Vector<File> resultDirectoryVector, Vector<File> resultFileVector, boolean recursive) {

		if (searchDirectoryFileVector.size() == 0) {
			return;
		}

		while (true) {

			// System.out.println("searchDirectoryFileVector.size() = " + searchDirectoryFileVector.size());

			File directory = (File) searchDirectoryFileVector.remove(0);

			if (resultDirectoryVector != null)
				resultDirectoryVector.add(directory);

			if (directory.isDirectory()) {
				// System.out.println("getting file list for: " + directory.getAbsolutePath());
				File[] files = directory.listFiles();

				int numFilesInDirectory = files.length;

				for (int i = 0; i < numFilesInDirectory; i++) {

					File file = files[i];

					if (file.isFile()) {
						resultFileVector.add(file);
					}

					if (file.isDirectory() && recursive) {
						searchDirectoryFileVector.add(file);
					}

				}
			}

			if (searchDirectoryFileVector.size() == 0) {
				break;
			}
		}

	}

	static public SimpleTable readDelimitedTable(String path) throws Exception {

		return readDelimitedTable(path, "\t", 0, true, true);

	}

	static public SimpleTable readDelimitedTable(String path, String delimiter) throws Exception {

		return readDelimitedTable(path, delimiter, 0, true, true);

	}

	static public SimpleTable readDelimitedTable(String path, String delimiter, boolean hasHeader) throws Exception {

		return readDelimitedTable(path, delimiter, 0, hasHeader, true);

	}

	static public SimpleTable readDelimitedTable(String path, String delimiter, boolean hasHeader, boolean fixedNumberOfColumns) throws Exception {

		return readDelimitedTable(path, delimiter, 0, hasHeader, fixedNumberOfColumns);

	}

	static public SimpleTable readDelimitedTable(String path, String delimiter, int initialNumLinesToIgnore, boolean hasHeader, boolean fixedNumberOfColumns) throws Exception {

		// ///////////////////////////
		// COUNT NUMBER OF LINES //
		// ///////////////////////////

		int numLines = 0;
		int maxNumCols = Integer.MIN_VALUE;
		{
			BufferedReader bufferedReader = new BufferedReader(new FileReader(new File(path)));

			for (int i = 0; i < initialNumLinesToIgnore; i++) {
				bufferedReader.readLine();
			}

			while (true) {
				String line = bufferedReader.readLine();

				if (line == null)
					break;
				// System.out.println("line = " + line);

				if (fixedNumberOfColumns) {
					int numCols = line.split(delimiter, -1).length;

					if (numCols > maxNumCols) {
						maxNumCols = numCols;
					}
				}

				numLines++;
			}
			bufferedReader.close();
		}

		// ///////////////////////////////////////////////////////
		// READ COLUMN NAMES AND CALCUALTE NUMBER OF COLUMNS //
		// ///////////////////////////////////////////////////////
		int dataColumnIndex = -1;
		// int numDataColumns = -1;
		String[] columnNames = null;
		HashMap<String, Integer> nameToIndex = new HashMap<String, Integer>();
		{

			BufferedReader bufferedReader = new BufferedReader(new FileReader(new File(path)));

			for (int i = 0; i < initialNumLinesToIgnore; i++) {
				bufferedReader.readLine();
			}

			String line = bufferedReader.readLine();

			String[] strings = line.split(delimiter, -1);

			// numDataColumns = strings.length;

			columnNames = new String[strings.length];
			for (int f = 0; f < strings.length; f++) {
				if (hasHeader)
					columnNames[f] = strings[f];
				else
					columnNames[f] = "column" + (f + 1);

				nameToIndex.put(columnNames[f], f);

			}

			bufferedReader.close();
		}

		// /////////////////////
		// READ TABLE DATA //
		// /////////////////////

		int numDataRows;

		if (hasHeader)
			numDataRows = numLines - 1;
		else
			numDataRows = numLines;

		String[][] stringMatrix = new String[numDataRows][];

		{

			BufferedReader bufferedReader = new BufferedReader(new FileReader(new File(path)));

			for (int i = 0; i < initialNumLinesToIgnore; i++) {
				bufferedReader.readLine();
			}

			// skip column names
			String line = null;
			if (hasHeader)
				line = bufferedReader.readLine();

			for (int i = 0; i < numDataRows; i++) {

				line = bufferedReader.readLine();

				String[] parts = line.split(delimiter, -1);

				if (fixedNumberOfColumns) {

					if (parts.length == maxNumCols) {

						stringMatrix[i] = parts;
					} else {
						String[] newParts = new String[maxNumCols];
						for (int j = 0; j < parts.length; j++) {
							newParts[j] = parts[j];
						}
						parts = newParts;
					}

				}

				stringMatrix[i] = parts;

			}

			bufferedReader.close();

			SimpleTable simpleTable = new SimpleTable();

			simpleTable.delimiter = delimiter;
			simpleTable.columnNames = columnNames;
			simpleTable.nameToIndex = nameToIndex;
			simpleTable.numDataColumns = columnNames.length;
			simpleTable.numDataRows = numDataRows;
			simpleTable.stringMatrix = stringMatrix;

			return simpleTable;
		}
	}

	static public int countLines(String path) throws Exception {

		int numLines = 0;

		BufferedReader bufferedReader = new BufferedReader(new FileReader(new File(path)));

		while (true) {
			String line = bufferedReader.readLine();

			if (line == null)
				break;
			numLines++;
		}
		bufferedReader.close();

		return numLines;

	}

	static public String readTextFileToString(String path) {

		long length = IO.fileLength(path);

		RandomAccessFile raf = null;
		try {
			raf = new RandomAccessFile(new File(path), "r");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		byte[] bytes = new byte[(int) length];

		try {
			raf.read(bytes);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return new String(bytes);

	}

	static public void createEmptyFile(String path) {

		File file = new File(path);
		try {
			file.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	static public void writeTextFileFromString(String path, String textString) {

		BufferedWriter bufferedWriter = null;
		try {
			bufferedWriter = new BufferedWriter(new FileWriter(new File(path)));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			bufferedWriter.write(textString);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			bufferedWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	static public void writeDelimitedTableSplitUp(SimpleTable table, String path, int numSplits, boolean randomize) throws Exception {

		int numRows = table.numDataRows;

		int[] randomIndices = null;

		if (randomize)
			randomIndices = Utility.randomIntArray(-1, numRows);

		double targetRowsPerFile = (double) numRows / (double) numSplits;

		for (int splitIndex = 0; splitIndex < numSplits; splitIndex++) {

			BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(new File(path + "." + splitIndex)));

			if (table.printHeader) {
				for (int c = 0; c < table.numDataColumns; c++) {
					if (c > 0)
						bufferedWriter.write(table.delimiter);
					bufferedWriter.write(table.columnNames[c]);
				}
				bufferedWriter.write(table.eol);
			}

			int startRow = (int) (splitIndex * targetRowsPerFile);
			int endRow = (int) ((splitIndex + 1) * targetRowsPerFile);
			if (splitIndex == numSplits - 1) {
				endRow = numRows;
			}

			for (int r = startRow; r < endRow; r++) {
				for (int c = 0; c < table.numDataColumns; c++) {
					if (c > 0)
						bufferedWriter.write(table.delimiter);

					int rowIndex = -1;

					if (randomize)
						rowIndex = randomIndices[r];
					else
						rowIndex = r;

					bufferedWriter.write(table.stringMatrix[rowIndex][c]);
				}
				bufferedWriter.write(table.eol);
			}
			bufferedWriter.close();

		}
	}

	static public void writeDelimitedTable(SimpleTable table, String path) throws Exception {

		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(new File(path)));

		if (table.printHeader) {
			for (int c = 0; c < table.numDataColumns; c++) {
				if (c > 0)
					bufferedWriter.write(table.delimiter);
				bufferedWriter.write(table.columnNames[c]);
			}
			bufferedWriter.write(table.eol);
		}

		for (int r = 0; r < table.numDataRows; r++) {
			for (int c = 0; c < table.numDataColumns; c++) {
				if (c > 0)
					bufferedWriter.write(table.delimiter);
				bufferedWriter.write(table.stringMatrix[r][c]);
			}
			bufferedWriter.write(table.eol);
		}
		bufferedWriter.close();
	}

	static public SimpleTable readUnstructuredTable(String path, String delimiter) throws Exception {

		// ///////////////////////////////////
		// COUNT NUMBER OF ROWS AND COLS //
		// ///////////////////////////////////

		int numRows = 0;
		int maxNumCols = -1;
		{
			BufferedReader bufferedReader = new BufferedReader(new FileReader(new File(path)));
			while (true) {
				String line = bufferedReader.readLine();
				if (line == null) {
					break;
				}
				int numCols = line.split(delimiter, -1).length;
				if (numCols > maxNumCols) {
					maxNumCols = numCols;
				}
				numRows++;
			}
			bufferedReader.close();
		}

		String[][] stringMatrix = new String[numRows][maxNumCols];

		{

			BufferedReader bufferedReader = new BufferedReader(new FileReader(new File(path)));

			for (int i = 0; i < numRows; i++) {

				String line = bufferedReader.readLine();

				String[] strings = line.split(delimiter, -1);

				// System.out.println("SimpleTable: line = " + line);
				// System.out.println("SimpleTable: strings.length = " + strings.length);

				for (int j = 0; j < strings.length; j++) {

					stringMatrix[i][j] = strings[j];

				}

			}
			bufferedReader.close();

			SimpleTable simpleTable = new SimpleTable();

			simpleTable.delimiter = delimiter;
			simpleTable.columnNames = null;
			simpleTable.numDataColumns = maxNumCols;
			simpleTable.numDataRows = numRows;
			simpleTable.stringMatrix = stringMatrix;

			return simpleTable;
		}

	}

	static public String[] readStringLines(String path) throws Exception {

		// ///////////////////////////////////
		// COUNT NUMBER OF ROWS AND COLS //
		// ///////////////////////////////////

		int numRows = 0;
		int maxNumCols = -1;
		{
			BufferedReader bufferedReader = new BufferedReader(new FileReader(new File(path)));
			while (true) {
				String line = bufferedReader.readLine();
				if (line == null) {
					break;
				}
				numRows++;
			}
			bufferedReader.close();
		}

		return readStringLines(path, numRows);
	}

	static public String[] readStringLines(String path, int numRows) throws Exception {

		// ///////////////////////////////////
		// COUNT NUMBER OF ROWS AND COLS //
		// ///////////////////////////////////

		if (numRows < 0) {
			numRows = 0;
			{
				BufferedReader bufferedReader = new BufferedReader(new FileReader(new File(path)));
				while (true) {
					String line = bufferedReader.readLine();
					if (line == null) {
						break;
					}
					numRows++;
				}
				bufferedReader.close();
			}
		}

		String[] stringLines = new String[numRows];

		{

			BufferedReader bufferedReader = new BufferedReader(new FileReader(new File(path)));

			for (int i = 0; i < numRows; i++) {

				stringLines[i] = bufferedReader.readLine();

			}
			bufferedReader.close();
			return stringLines;
		}

	}

	static public void writeStringLines(String path, String[] strings, int numStrings) throws Exception {

		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(new File(path)));
		for (int i = 0; i < numStrings; i++) {
			bufferedWriter.write(strings[i]);
			bufferedWriter.write('\n');
		}

		bufferedWriter.close();

	}



	static public void appendStringLines(String path, String[] strings, int numStrings) throws Exception {

		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(new File(path), true));
		for (int i = 0; i < numStrings; i++) {
			bufferedWriter.write(strings[i]);
			bufferedWriter.write('\n');
		}

		bufferedWriter.close();

	}

	static public void appendStringLines(String path, String[] strings) throws Exception {

		appendStringLines(path, strings, strings.length);

	}

	static public void appendStringLine(String path, String string) throws Exception {

		appendStringLines(path, new String[] { string }, 1);

	}
	

	public int parseDWORD(byte[] buffer, int index) {
		int sum = 0;
		for (int i = 0; i < 4; i++) {
			int byteIntValue = buffer[index];

			// System.out.println("parseDWORD," + i + "," + byteIntValue);

			if (byteIntValue < 0) {
				byteIntValue = 256 + byteIntValue;
			}
			int partialSum = byteIntValue << (i * 8);
			sum += partialSum;
			// System.out.println(index + "," + byteIntValue + "," + partialSum + "," + sum);
			index++;
		}
		return sum;
	}

	public int parseWORD(byte[] buffer, int index) {
		int sum = 0;
		for (int i = 0; i < 2; i++) {
			int byteIntValue = buffer[index];

			// System.out.println("parseWORD," + i + "," + byteIntValue);

			if (byteIntValue < 0) {
				byteIntValue = 256 + byteIntValue;
			}
			int partialSum = byteIntValue << (i * 8);
			sum += partialSum;
			// System.out.println(index + "," + byteIntValue + "," + partialSum + "," + sum);
			index++;
		}
		return sum;
	}

	public void encodeWORD(byte[] buffer, int index, int value) {

		for (int i = 0; i < 2; i++) {

			int byteIntValue = value % 256;
			if (byteIntValue > 127) {
				byteIntValue = byteIntValue - 256;
			}
			buffer[index + i] = (byte) byteIntValue;

			value = value / 256;

			// System.out.println("encodeWORD," + i + "," + byteIntValue);
		}
	}

	public void encodeDWORD(byte[] buffer, int index, int value) {

		for (int i = 0; i < 4; i++) {

			int byteIntValue = value % 256;
			if (byteIntValue > 127) {
				byteIntValue = byteIntValue - 256;
			}
			buffer[index + i] = (byte) byteIntValue;

			value = value / 256;

			// System.out.println("encodeDWORD," + i + "," + byteIntValue);
		}
	}

	public int parseInteger(byte[] buffer, int index) {
		int sum = 0;
		for (int i = 0; i < 4; i++) {
			int byteIntValue = buffer[index];
			// System.out.println("parseInteger," + i + "," + byteIntValue);
			if (byteIntValue < 0) {
				byteIntValue = 256 + byteIntValue;
			}
			int partialSum = byteIntValue << ((3 - i) * 8);
			sum += partialSum;
			// System.out.println(index + "," + byteIntValue + "," + partialSum + "," + sum);
			index++;
		}
		return sum;
	}

	public int parseShort(byte[] buffer, int index) {
		int sum = 0;
		for (int i = 0; i < 2; i++) {
			int byteIntValue = buffer[index];
			// System.out.println("parseShort," + i + "," + byteIntValue);
			if (byteIntValue < 0) {
				byteIntValue = 256 + byteIntValue;
			}
			int partialSum = byteIntValue << ((1 - i) * 8);
			sum += partialSum;
			// System.out.println(index + "," + byteIntValue + "," + partialSum + "," + sum);
			index++;
		}
		return sum;
	}

	public void encodeShort(byte[] buffer, int index, int value) {

		for (int i = 0; i < 2; i++) {

			int byteIntValue = value % 256;
			if (byteIntValue > 127) {
				byteIntValue = byteIntValue - 256;
			}
			buffer[index + (1 - i)] = (byte) byteIntValue;

			value = value / 256;

		}

		for (int i = 0; i < 2; i++) {
			// System.out.println("encodeShort," + i + "," + buffer[index + i]);
		}
	}

	public void encodeInteger(byte[] buffer, int index, int value) {

		for (int i = 0; i < 4; i++) {

			int byteIntValue = value % 256;
			if (byteIntValue > 127) {
				byteIntValue = byteIntValue - 256;
			}
			buffer[index + (3 - i)] = (byte) byteIntValue;

			value = value / 256;

		}

		for (int i = 0; i < 4; i++) {
			// System.out.println("encodeInteger," + i + "," + buffer[index + i]);
		}
	}

}
