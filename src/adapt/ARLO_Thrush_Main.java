package adapt;

import java.util.ArrayList;
import java.util.Arrays;

public class ARLO_Thrush_Main {

	// bestError = 18.303631902559086; @ 500.0 @ e7
	// bestError = 17.977586270042348; @ 600.0 @ e7
	// bestError = 17.757873792478485; @ 750.0 @ e7
	// bestError = 17.82386926882114; @ 1000.0 @ e7
	// bestError = 17.30166998218726; @ 1000.0 @ e8
	// bestError = 17.803794914128833; @ 1000.0 @ e7
	// bestError = 18.319573051368973; @ 2000.0 @ e7

	public static void main(String[] args) {
		

//		int startTime = 39600;
//		int endTime = 43200;

//		int startTime = 40594;
//		int endTime = 40609;
		
		int startTime = 0;
		int endTime = 999999;
		

		String dataFilePath = "/home/dtcheng/gcth-trascript.csv";
//		String dataFilePath = "/home/dtcheng/data/thrush-transcript-gcth.csv";
		// String dataFilePath = "/scratch/users/dtcheng/data/thrush-transcript.csv";

		boolean assumeZ = false; // false is more accurate
		double assumedZ = 240.0; // for thrush

		double initialSearchRadius = 1000.0; // for thrush
		int numReductionSteps = 1; // for thrush
		// int maxNumIterations = (int) 1e7;
		int maxNumIterations = (int) 1e6;
		
		int numRepetitions = 100;
		double toaNoiseLevel = 0.01;
		
//		 int numRepetitions = 1;
//		 double toaNoiseLevel = 0.0;

		// bestError = 20.736342287375603; // original coordinates
		// bestError = 20.94141535462302; // GOOGLE MAPS COORDINATES

		int minNumSensors = 3;
		int maxNumSensors = 10;
		double clusterTimeGap = 0.5;

		int centerTimeColumnIndex = 2;
		int frequncyColumnIndex = 5;
		int fileColumnIndex = 13;

		ARLO arlo = new ARLO();

		arlo.assumeZ = assumeZ;
		arlo.assumedZ = assumedZ;

		arlo.reportEvents = true;
		// arlo.minAverageTOADErrorInMS = 0.1;
		arlo.minAverageTOADErrorInMS = Double.POSITIVE_INFINITY;
		arlo.minZ = 0.5;
		arlo.numReductionSteps = numReductionSteps;

		/******************/
		/* DEFINE SENSORS */
		/******************/

		/*
		 * wav/Audio 01 SW_12.wav wav/Audio 02 NW_12.wav wav/Audio 03 NE_12.wav wav/Audio 04 SE_12.wav wav/Audio 05 CG_12.wav wav/Audio 06 N_13.wav wav/Audio 07 W_06.wav
		 */

		int numSensors = 10;

		Sensor[] sensors = new Sensor[numSensors];

		for (int i = 0; i < numSensors; i++) {

			switch (i) {

			case 0:
				// sensors[i] = new Sensor("CenterDish", "Parabola", 697, 458, 0.5);
				sensors[i] = new Sensor("CenterDish", "Parabola", -0.83, 3.4, 0.5);
				break;

			case 1:
				// sensors[i] = new Sensor("CenterGround", "CenterGround", 700, 455, 0.5);
				sensors[i] = new Sensor("CenterGround", "CenterGround", 0, 0, 0.0);
				break;

			case 2:
				// sensors[i] = new Sensor("InnerNorthEast", "Inner_NE", 799, 559, 0);
				sensors[i] = new Sensor("InnerNorthEast", "Inner_NE", 96.32, 104.19, 0);
				break;

			case 3:
				// sensors[i] = new Sensor("InnerNorthWest", "Inner_NW", 604, 560, 0);
				sensors[i] = new Sensor("InnerNorthWest", "Inner_NW", -97.83, 103.71, 0);
				break;

			case 4:
				// sensors[i] = new Sensor("InnerSouthEast", "Inner_SE", 796, 358, 0);
				sensors[i] = new Sensor("InnerSouthEast", "Inner_SE", 96.71, -92.79, 0);
				break;

			case 5:
				// sensors[i] = new Sensor("InnerSouthWest", "Inner_SW", 601, 361, 0);
				sensors[i] = new Sensor("InnerSouthWest", "Inner_SW", -93.47, -98.85, 0);
				break;

			case 6:
				// sensors[i] = new Sensor("OuterNorthEast", "Outer_NE", 899, 626, 0);
				sensors[i] = new Sensor("OuterNorthEast", "Outer_NE", 193.86, 173.13, 0);
				break;

			case 7:
				// sensors[i] = new Sensor("OuterNorthWest", "Outer_NW", 499, 627, 0);
				sensors[i] = new Sensor("OuterNorthWest", "Outer_NW", -204.85, 168.25, 0);
				break;

			case 8:
				// sensors[i] = new Sensor("OuterSouthEast", "Outer_SE", 900, 256, 0);
				sensors[i] = new Sensor("OuterSouthEast", "Outer_SE", 201.33, -193.37, 0);
				break;

			case 9:
				// sensors[i] = new Sensor("OuterSouthWest", "Outer_SW", 499, 258, 0);
				sensors[i] = new Sensor("OuterSouthWest", "Outer_SW", -198.89, -199.75, 0);
				break;

			}
		}

		arlo.setSensors(sensors);

		//

		//

		//

		/*****************/
		/* DEFINE EVENTS */
		/*****************/

		//

		SimpleTable table = null;
		try {
			table = IO.readDelimitedTable(dataFilePath, ",");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		int numDataRows = table.numDataRows;

		// System.out.println("numDataRows = " + numDataRows);

		int numClusterEvents = 0;

		ArrayList<SensorEvent> sensorEventList = new ArrayList<SensorEvent>();

		double recordingAbsoluteStartTime = 20100928.0 * 24.0 * 3600.0 + 19.0 * 3600.0;

		for (int dataRowIndex = 0; dataRowIndex < numDataRows; dataRowIndex++) {

			String fileName = table.getString(dataRowIndex, fileColumnIndex);

			// System.out.println(fileName);

			int sensorIndex = -1;
			String remains = null;
			for (int i = 0; i < numSensors; i++) {
				if (fileName.contains(sensors[i].matchString)) {
					sensorIndex = i;
					remains = fileName.replaceAll(sensors[i].matchString, "");
				}
			}

			String[] parts = remains.split("_");

			String micName = parts[0];
			String dateNumber = parts[1];
			String hourNumber = parts[2];

			double timeOffset = Double.parseDouble(dateNumber) * 24 * 3600 + Double.parseDouble(hourNumber) / 100 * 3600;

			double fileRelativeStartTime = table.getDouble(dataRowIndex, centerTimeColumnIndex);
			double frequency = table.getDouble(dataRowIndex,frequncyColumnIndex);
			double absoluteStartTime = fileRelativeStartTime + timeOffset;

			double recordingRelativeStartTime = absoluteStartTime - recordingAbsoluteStartTime;
			
			
			if (recordingRelativeStartTime < startTime) {
				continue;
			}
			if (recordingRelativeStartTime > endTime) {
				continue;
			}

			int hours = (int) (fileRelativeStartTime / 3600);

			int minutes = (int) ((fileRelativeStartTime - 3600 * hours) / 60);

			double seconds = (int) ((fileRelativeStartTime - 3600 * hours - 60 * minutes) * 100) / 100.0;

			// Format numberFormat = new Format("5.");
			// System.out.println(recordingRelativeStartTime);

			// System.out.println("BILL" + "\t" + hours + ":" + minutes + ":" + seconds + "\t" + fileName + "\t" + micName + "\t" + dateNumber + "\t" + hourNumber);

			int tagID = table.getInt(dataRowIndex, 1);

			sensorEventList.add(new SensorEvent(recordingRelativeStartTime, sensorIndex, fileName, fileRelativeStartTime, frequency, tagID));

		}

		int numSensorEvents = sensorEventList.size();

		SensorEvent[] sensorEvents = new SensorEvent[numSensorEvents];

		sensorEvents = sensorEventList.toArray(sensorEvents);

		Arrays.sort(sensorEvents, 0, sensorEventList.size());

		if (false)
			for (int i = 0; i < numSensorEvents; i++) {
				System.out.println(sensorEvents[i].time + "\t" + sensorEvents[i].sensorIndex);
			}

		ArrayList<Event> candidateGlobalEventList = new ArrayList<Event>();
		double lastEventTime = sensorEvents[0].time;

		boolean[] hasSensor = new boolean[numSensors];
		for (int sensorEventIndex = 0; sensorEventIndex < numSensorEvents; sensorEventIndex++) {

			double time = sensorEvents[sensorEventIndex].time;

			double timeSinceLastEvent = time - lastEventTime;

			if (timeSinceLastEvent > clusterTimeGap) {

				// process last event cluster

				ArrayList<Double> eventTimes = new ArrayList();
				ArrayList<double[]> eventPositions = new ArrayList();
				ArrayList<String> eventFileNames = new ArrayList();
				ArrayList<Double> eventFileTimes = new ArrayList();
				ArrayList<Double> eventFrequencies = new ArrayList();
				ArrayList<Integer> eventTagIDs = new ArrayList();

				int startIndex = sensorEventIndex - numClusterEvents;
				int endIndex = startIndex + numClusterEvents;

				int numUniqueSensors = 0;
				boolean sensorIndexRepeat = false;
				for (int clusterSensorEventIndex = startIndex; clusterSensorEventIndex < endIndex; clusterSensorEventIndex++) {

					double eventTime = sensorEvents[clusterSensorEventIndex].time;
					int sensorIndex = sensorEvents[clusterSensorEventIndex].sensorIndex;

					if (!hasSensor[sensorIndex]) {
						hasSensor[sensorIndex] = true;
						numUniqueSensors++;
					} else {
						sensorIndexRepeat = true;
					}

					eventTimes.add(eventTime);
					eventPositions.add(sensors[sensorIndex].position);
					eventFileNames.add(sensorEvents[clusterSensorEventIndex].fileName);
					eventFileTimes.add(sensorEvents[clusterSensorEventIndex].fileTime);
					eventFrequencies.add(sensorEvents[clusterSensorEventIndex].frequency);
					eventTagIDs.add(sensorEvents[clusterSensorEventIndex].tagID);

				}

				if ((!sensorIndexRepeat) && (numClusterEvents == numUniqueSensors) && (numUniqueSensors >= minNumSensors) && (numUniqueSensors <= maxNumSensors)) {

					// System.out.println("numUniqueSensors = " + numUniqueSensors);

					double[] times = new double[eventTimes.size()];
					double[][] positions = new double[eventPositions.size()][3];
					String[] fileNames = new String[eventFileNames.size()];
					double[] fileTimes = new double[eventFileTimes.size()];
					double[] frequencies = new double[eventFrequencies.size()];
					int[] tagIDs = new int[eventTagIDs.size()];

					for (int j = 0; j < numUniqueSensors; j++) {
						times[j] = eventTimes.get(j);
						positions[j] = eventPositions.get(j);
						fileNames[j] = eventFileNames.get(j);
						fileTimes[j] = eventFileTimes.get(j);
						frequencies[j] = eventFrequencies.get(j);
						tagIDs[j] = eventTagIDs.get(j);
					}

					candidateGlobalEventList.add(new Event(times, positions, fileNames, fileTimes,  frequencies, tagIDs));
				}

				// start new cluster
				numClusterEvents = 1;

				hasSensor = new boolean[numSensors];

			} else {
				numClusterEvents++;
			}

			lastEventTime = time;
		}

		int numGlobalEvents = candidateGlobalEventList.size();

		System.out.println("numGlobalEvents = " + numGlobalEvents);
		Event[] events = new Event[numGlobalEvents];

		candidateGlobalEventList.toArray(events);

		arlo.setEvents(events);

		// arlo.setInitialSearchRadius(0.01);

		// arlo.setSearchRadiusReductionFactor(0.999999); // fine grain
		// arlo.setSearchRadiusReductionFactor(0.999); // fast
		// arlo.setEndSearchRadius(0.001);

		// double biasSearchRadius = 0.01; // for skywriting
		double biasSearchRadius = 0.00; // for thrush
		System.out.println("biasSearchRadius = " + biasSearchRadius + ";");

		//
		// TASK LEVEL BIASES //
		//

		double bestSpeedOfSound = 337.311; // for thrush
		System.out.println("bestSpeedOfSound = " + bestSpeedOfSound + ";");
		arlo.setSpeedOfSoundInMetersPerSecond(bestSpeedOfSound);

		// double searchRadiusReductionFactor = 1.0 - 1.0 / arlo.maxNumIterations;
		double searchRadiusReductionFactor = 0.5;
		System.out.println("searchRadiusReductionFactor = " + searchRadiusReductionFactor + ";");
		arlo.setSearchRadiusReductionFactor(searchRadiusReductionFactor);

		// double initialSearchRadius = 0.1; // for skywriting
		System.out.println("initialSearchRadius = " + initialSearchRadius + ";");
		arlo.setInitialSearchRadius(initialSearchRadius);

		arlo.maxNumIterations = maxNumIterations;
		arlo.numRepetitions = numRepetitions;
		arlo.toaNoiseLevel = toaNoiseLevel;

		double bestError = Double.POSITIVE_INFINITY;
		int iteration = 0;
		int maxNumBiasOptimiztionIterations = 1;
		while (true) {
			iteration++;

			int candidateSensorIndex = (int) (Math.random() * numSensors);
			int candidateDimensionIndex = (int) (Math.random() * 3);

			double bestBiasValue = sensors[candidateSensorIndex].position[candidateDimensionIndex];

			double candidateBiasValue = bestBiasValue + 2 * Math.random() * biasSearchRadius - biasSearchRadius;

			// arlo.setSpeedOfSoundInMetersPerSecond(biasValue);
			sensors[candidateSensorIndex].position[candidateDimensionIndex] = candidateBiasValue;

			arlo.run();

			System.out.println("arlo.numGoodEvents = " + arlo.numGoodEvents);

			double averageAbsDiffScore = arlo.averageAbsDiffScore;

			double candidateError = arlo.averageSTDs[0] + arlo.averageSTDs[1] + arlo.averageSTDs[2];

			System.out.println("averageAbsDiffScore = " + averageAbsDiffScore);
			System.out.println("arlo.averageSTDs[0] = " + arlo.averageSTDs[0]);
			System.out.println("arlo.averageSTDs[1] = " + arlo.averageSTDs[1]);
			System.out.println("arlo.averageSTDs[2] = " + arlo.averageSTDs[2]);

			candidateError = averageAbsDiffScore;

			if (true) {
				System.out.print("STATUS");
				System.out.print("\titeration = \t" + iteration);
				System.out.print("\tcandidateSensorIndex = \t" + candidateSensorIndex);
				System.out.print("\tcandidateDimensionIndex = \t" + candidateDimensionIndex);
				System.out.print("\tbestBiasValue = \t" + bestBiasValue);
				System.out.print("\tcandidateBiasValue = \t" + candidateBiasValue);
				System.out.print("\tcandidateError = \t" + candidateError);
				System.out.print("\tbiasSearchRadius = " + biasSearchRadius);
				System.out.print("\tbestError = " + bestError);
				System.out.println();
			}

			if (candidateError < bestError) {
				bestBiasValue = candidateBiasValue;
				bestError = candidateError;
			} else {
				sensors[candidateSensorIndex].position[candidateDimensionIndex] = bestBiasValue;
			}

			for (int sensorIndex = 0; sensorIndex < numSensors; sensorIndex++) {
				for (int dimensionIndex = 0; dimensionIndex < 3; dimensionIndex++) {
					System.out.print("sensors[" + sensorIndex + "].point[" + dimensionIndex + "] = " + sensors[sensorIndex].position[dimensionIndex] + "; ");
				}
				System.out.println();
			}
			System.out.println("bestError = " + bestError + ";");

			// biasSearchRadius *= 0.999;

			if (iteration >= maxNumBiasOptimiztionIterations) {
				break;
			}
		}
	}
}

// 16.461465308838694 100.0;
// 16.368630528529387 1000
/*
 * 
 * orig bestError = 48.97787761938916; new bestError = 53.29655264865358; bestError = 47.65411981470382;
 * 
 * 
 * 10x new coords bestError = 309.82147512370625;
 * 
 * 100x new coords bestError = 3107.9393041694816;
 * 
 * 1000x new coords bestError =
 * 
 * 
 * 10x old coords bestError = 310.110752052708;
 * 
 * bestError = 662.4973618836086;
 * 
 * 100x old coords bestError =
 * 
 * 1000x old coords bestError =
 * 
 * 
 * 
 * 
 * 
 * 
 * e6 o a averageAbsDiffScore = 346.09762302574325 arlo.averageSTDs[0] = 4.94064027974039 arlo.averageSTDs[1] = 5.546547241737293 arlo.averageSTDs[2] = 13.321116366889857 1e6 o b averageAbsDiffScore =
 * 346.0232759736311 arlo.averageSTDs[0] = 5.064020567933787 arlo.averageSTDs[1] = 5.608904613870875 arlo.averageSTDs[2] = 13.449537444413412
 * 
 * 1e6 n a averageAbsDiffScore = 343.7895723689931 arlo.averageSTDs[0] = 4.803658595892939 arlo.averageSTDs[1] = 5.3835051282395385 arlo.averageSTDs[2] = 12.583548179284085 1e6 n b averageAbsDiffScore
 * = 344.30843994897833 arlo.averageSTDs[0] = 4.749313019349976 arlo.averageSTDs[1] = 5.527466521665813 arlo.averageSTDs[2] = 12.199170025587062
 * 
 * 
 * 
 * 
 * e7 o a r10 averageAbsDiffScore = 345.36663351469025 arlo.averageSTDs[0] = 5.233451804799397 arlo.averageSTDs[1] = 5.482027909279191 arlo.averageSTDs[2] = 13.08803466310279 1e7 o b r10
 * averageAbsDiffScore = 345.4933524832407 arlo.averageSTDs[0] = 4.54897467718404 arlo.averageSTDs[1] = 5.240278284654995 arlo.averageSTDs[2] = 12.981687280008318
 * 
 * 1e7 n a r10 averageAbsDiffScore = 344.94078255777305 arlo.averageSTDs[0] = 4.887836610218352 arlo.averageSTDs[1] = 5.362167817882405 arlo.averageSTDs[2] = 12.297440832759758 1e7 n b r10
 * averageAbsDiffScore = 344.46031099770363 arlo.averageSTDs[0] = 5.2858073340660985 arlo.averageSTDs[1] = 6.018955699404218 arlo.averageSTDs[2] = 12.567251473416928
 * 
 * 
 * 1e8 o a r10
 * 
 * 1e8 o b r10
 * 
 * 
 * 1e8 n a r10
 * 
 * 1e8 n b r10
 * 
 * 
 * 1e8 o a r20
 * 
 * 1e8 o b r20
 * 
 * 
 * 1e8 n a r20
 * 
 * 1e8 n b r20
 * 
 * 
 * 
 * 
 * 
 * 
 * bestError = 27.609576925746545; // radius = 16000 bestError = 23.703746978591695; // radius = 8000 bestError = 21.631042516427993; // radius = 4000 bestError = 22.054471229410975; // radius = 2000
 * bestError = 23.167395705537583; // radius = 1000 bestError = 53.969441687808704; // radius = 500 bestError = 156.56251233666293; // radius = 250 bestError = 279.8985369960231; // radius = 125
 */