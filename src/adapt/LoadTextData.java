package adapt;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;
import java.util.Vector;

import com.mysql.jdbc.DatabaseMetaData;

public class LoadTextData {

	public static void main(String[] args) {

		int numRounds = (int) 10;
		double weightingPower = 32.0; //
		int windowSizeInPhonemes = 1;  // 2.01@1, 6.2@4, 5.13@4, 
		boolean useCache = true;

		String dataDirectorPath = "/data/bin/";
		// String dataDirectorPath = "/uf/ncsa/dtcheng/data/bin/";
		// String dataDirectorPath = "/d250/data/bin/";

		{
			int argIndex = 0;

			if (args.length > argIndex)
				dataDirectorPath = args[argIndex];
			argIndex++;
			if (args.length > argIndex)
				numRounds = Integer.parseInt(args[argIndex]);
			argIndex++;
			if (args.length > argIndex)
				windowSizeInPhonemes = Integer.parseInt(args[argIndex]);
			argIndex++;
			if (args.length > argIndex)
				weightingPower = Double.parseDouble(args[argIndex]);

		}

		Vector<String> tableNames = new Vector<String>();

		int maxNumSymbols = (int) 100e6;
		int maxNumTexts = (int) 1e3;
		int numFeatures = 6;

		int part_of_speech_weight = 1;
		int accent_weight = 1;
		int stress_weight = 1;
		int tone_weight = 1;
		int phrase_id_weight = 1;
		int break_index_weight = 1;

		int[] featureWeights = new int[] { part_of_speech_weight, accent_weight, stress_weight, tone_weight, phrase_id_weight, break_index_weight };

		String[] featurePatterns = new String[numFeatures];

		int[] symbols = new int[maxNumSymbols];
		int symbolIndex = 0;
		int[] textEndSymbolIndex = new int[maxNumTexts];
		int textIndex = 0;

		HashMap<String, Integer>[] symbolToIndex = new HashMap[numFeatures];

		for (int i = 0; i < numFeatures; i++) {
			symbolToIndex[i] = new HashMap();
		}

		int numSymbols = 0;
		int numExamples = 0;
		int numPhrases = 0;
		int numPhonemes = 0;

		if (!useCache) {
			String driver = "com.mysql.jdbc.Driver";

			try {
				Class.forName(driver).newInstance();
			} catch (InstantiationException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			} catch (IllegalAccessException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			} catch (ClassNotFoundException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}

			String username = "";
			String port = "3306";
			// String machine = "";
			String machine = "";
			String dbInstance = "";
			String password = "";

			String url = "jdbc:mysql://" + machine + ":" + port + "/" + dbInstance;

			System.out.println("creating connection for " + url);

			try {
				Connection connection = DriverManager.getConnection(url, username, password);

				//
				// get list of all tables
				//

				java.sql.DatabaseMetaData md = connection.getMetaData();
				ResultSet rs = md.getTables(null, null, "%", null);
				while (rs.next()) {
					String tableName = rs.getString(3);
					if (tableName.startsWith("old_"))
						continue;
					if (tableName.endsWith("_part"))
						continue;
					if (tableName.endsWith("_freq"))
						continue;
					tableNames.add(tableName);
				}

				int numTables = tableNames.size();
				System.out.println("numTables = " + numTables);

				if (true)
					for (int i = 0; i < numTables; i++) {
						String tableName = tableNames.elementAt(i);
						System.out.println(tableName);

					}

				for (int i = 0; i < numTables; i++) {

					String lastUniquePhraseID = "";

					String tableName = tableNames.elementAt(i);

					String query;
					query = "SELECT *  FROM " + tableName;

					Statement statement = connection.createStatement();
					ResultSet resultSet = statement.executeQuery(query);
					ResultSetMetaData resultSetMetaData = resultSet.getMetaData();

					int numColumns = resultSetMetaData.getColumnCount();
					// System.out.println("numColumns = " + numColumns);
					while (resultSet.next()) {

						int tei_paragraph_id = resultSet.getInt("tei_paragraph_id");
						int sentence_id = resultSet.getInt("sentence_id");
						int phrase_id = resultSet.getInt("phrase_id");
						String word = resultSet.getString("word");
						String part_of_speech = resultSet.getString("part_of_speech");
						String accent = resultSet.getString("accent");
						String phoneme = resultSet.getString("phoneme");
						String stress = resultSet.getString("stress");
						String tone = resultSet.getString("tone");
						int break_index = resultSet.getInt("break_index");
						int phoneme_id = resultSet.getInt("phoneme_id");
						int word_id = resultSet.getInt("word_id");
						int pos_id = resultSet.getInt("pos_id");

						String string = "";

						string += tableName + "\t";
						string += tei_paragraph_id + "\t";
						string += sentence_id + "\t";
						string += phrase_id + "\t";
						string += word + "\t";
						string += part_of_speech + "\t";
						string += accent + "\t";
						string += phoneme + "\t";
						string += stress + "\t";
						string += tone + "\t";
						string += break_index + "\t";
						string += phoneme_id + "\t";
						string += word_id + "\t";
						string += pos_id;

						String uniquePhraseID = /* tei_chapter_id + tei_section_id + */"" + tei_paragraph_id + ":" + sentence_id + ":" + phrase_id;

						if (!uniquePhraseID.equals(lastUniquePhraseID)) {
							numPhrases++;
							lastUniquePhraseID = uniquePhraseID;
						}
						numPhonemes++;

						// String combinedPattern = null;
						// combinedPattern = part_of_speech + ":" + accent + ":" + stress + ":" + tone + ":" + phrase_id + ":" + break_index;

						featurePatterns[0] = part_of_speech;
						featurePatterns[1] = accent;
						featurePatterns[2] = stress;
						featurePatterns[3] = tone;
						featurePatterns[4] = Integer.toString(phrase_id);
						featurePatterns[5] = Integer.toString(break_index);

						for (int f = 0; f < numFeatures; f++) {
							if (!symbolToIndex[f].containsKey(featurePatterns[f])) {
								symbolToIndex[f].put(featurePatterns[f], numSymbols++);
							}
						}

						System.out.print(tableName);
						for (int j = 0; j < numFeatures; j++) {
							int symbol = symbolToIndex[j].get(featurePatterns[j]);
							System.out.print("\t" + symbol);
						}
						System.out.println("\t" + word);

						for (int j = 0; j < numFeatures; j++) {
							int symbol = symbolToIndex[j].get(featurePatterns[j]);
							symbols[symbolIndex++] = symbol;
						}
					}

					textEndSymbolIndex[textIndex] = symbolIndex;

					textIndex++;

					statement.close();
				}
			} catch (Exception e) {
				System.out.println(e);
			}

			System.out.println("numPhrases = " + numPhrases);
			System.out.println("numPhonemes = " + numPhonemes);

			double phonemsPerPhrase = (double) numPhonemes / numPhrases;
			System.out.println("phonemsPerPhrase = " + phonemsPerPhrase);

			int[] temp = null;

			temp = new int[symbolIndex];
			System.arraycopy(symbols, 0, temp, 0, symbolIndex);
			symbols = temp;

			temp = new int[textIndex];
			System.arraycopy(textEndSymbolIndex, 0, temp, 0, textIndex);
			textEndSymbolIndex = temp;

			IO.writeObject(dataDirectorPath + "symbols.ser", symbols);
			IO.writeObject(dataDirectorPath + "textEndSymbolIndex.ser", textEndSymbolIndex);
			IO.writeObject(dataDirectorPath + "tableNames.ser", tableNames);

		} else {

			symbols = (int[]) IO.readObject(dataDirectorPath + "symbols.ser");
			symbolIndex = symbols.length;

			textEndSymbolIndex = (int[]) IO.readObject(dataDirectorPath + "textEndSymbolIndex.ser");
			textIndex = textEndSymbolIndex.length;

			tableNames = (Vector<String>) IO.readObject(dataDirectorPath + "tableNames.ser");

		}

		int numTextSymbols = symbolIndex;
		int numTexts = textIndex;

		System.out.println("numTextSymbols = " + numTextSymbols);
		System.out.println("numTexts = " + numTexts);

		//
		// COMPUTE SIMILARITY MATRIX
		//

		int windowSizeInFeatures = windowSizeInPhonemes * numFeatures;
		double[][] similarityWeights = new double[numTexts][numTexts];

		int[] windowFeatureWeights = new int[windowSizeInFeatures];
		for (int i = 0; i < windowFeatureWeights.length; i++) {
			windowFeatureWeights[i] = featureWeights[i % numFeatures];
		}

		int maxWeight = windowSizeInFeatures;
		double[] differenceSumToWeight = new double[maxWeight + 1];
		for (int i = 0; i < differenceSumToWeight.length; i++) {
			double similarity = 1.0 - (double) i / windowSizeInFeatures;
			similarity = Math.pow(similarity, weightingPower);
			differenceSumToWeight[i] = similarity;
		}

		HashMap<Integer, Double> differenceToWeightMap = new HashMap();

		long startTime = System.currentTimeMillis();
		int numSeedsDone = 0;

		for (int roundIndex = 0; roundIndex < numRounds; roundIndex++) {

			for (int seedTextIndex = 0; seedTextIndex < numTexts; seedTextIndex++) {

				long time = System.currentTimeMillis();

				double duration = (time - startTime) / 1000.0;
				
				double seedsPerSecond = numSeedsDone / duration;
				
				double timePerSeed = duration / (roundIndex * numTexts + seedTextIndex);
				double totalRoundTime = timePerSeed * numTexts;
				double totalTime = totalRoundTime * numRounds;
				double timeLeft = totalTime - duration;
				
				System.out.println("roundIndex       = " + roundIndex);
				System.out.println("seedTextIndex    = " + seedTextIndex);
				System.out.println("seedsPerSecond   = " + seedsPerSecond);
				System.out.println("timePerSeed      = " + timePerSeed);
				System.out.println("totalRoundTime   = " + totalRoundTime);
				System.out.println("totalTime        = " + totalTime);
				System.out.println("timeLeft (s)     = " + timeLeft);
				System.out.println("timeLeft (h)     = " + timeLeft / 3600 );
				System.out.println("timeLeft (d)     = " + timeLeft / 3600 / 24);

				// pick seed window
				int seedStartSymbolIndex = -1;
				if (seedTextIndex == 0) {
					seedStartSymbolIndex = 0;
				} else {
					seedStartSymbolIndex = textEndSymbolIndex[seedTextIndex - 1];
				}
				int seedNumSymbols = textEndSymbolIndex[seedTextIndex] - seedStartSymbolIndex;
				int seedWindowSymbolIndex = (int) (Math.random() * (seedNumSymbols - windowSizeInFeatures)) + seedStartSymbolIndex;
				seedWindowSymbolIndex = seedWindowSymbolIndex / numFeatures * numFeatures;

				// System.out.println("seedWindowSymbolIndex = " + seedWindowSymbolIndex);

				double bestOverallSimilarity = Double.NEGATIVE_INFINITY;
				int bestCandidateTextIndex = -1;
				int numTies = 0;
				int[] tiedCandidateTextIndices = new int[numTexts];
				for (int candidateTextIndex = 0; candidateTextIndex < numTexts; candidateTextIndex++) {

					int candidateTextNumWindows = -1;
					int candidateWindowSymbolIndex = -1;
					if (candidateTextIndex == 0) {
						candidateTextNumWindows = textEndSymbolIndex[0] / numFeatures - windowSizeInFeatures / numFeatures + 1;
						candidateWindowSymbolIndex = 0;
					} else {
						candidateTextNumWindows = (textEndSymbolIndex[candidateTextIndex] - textEndSymbolIndex[candidateTextIndex - 1]) / numFeatures
								- windowSizeInFeatures / numFeatures + 1;
						candidateWindowSymbolIndex = textEndSymbolIndex[candidateTextIndex - 1];
					}

					double candidateTextWeightSum = 0.0;
					int candidateTextWeightCount = 0;
					for (int optimizationRoundIndex = 0; optimizationRoundIndex < candidateTextNumWindows; optimizationRoundIndex++) {

						//ensure non overlapping windows
						if (Math.abs(seedWindowSymbolIndex - candidateWindowSymbolIndex) > windowSizeInFeatures) {

							// compare windows
							int differenceSum = 0;
							int index1 = seedWindowSymbolIndex;
							int index2 = candidateWindowSymbolIndex;
							int featureIndex = 0;
							for (; featureIndex < windowSizeInFeatures; index1++, index2++, featureIndex++) {
								if (symbols[index1] != symbols[index2]) {
									differenceSum += windowFeatureWeights[featureIndex];
								}
							}

							double weight = differenceSumToWeight[differenceSum];

							// double similarity = 1.0 - (double) differenceSum / windowSizeInFeatures;

							// Double weight = differenceToWeightMap.get(similarity);
							// if (weight == null) {
							// weight = Math.pow(similarity, weightingPower);
							// differenceToWeightMap.put(differenceSum, weight);
							// }
							// similarity = weight;

							// similarity = Math.pow(similarity, weightingPower);

							// candidateTextWeightSum += similarity;

							candidateTextWeightSum += weight;
							candidateTextWeightCount++;

						}

						candidateWindowSymbolIndex += numFeatures;

					}

					double candidateTextSimilarity = candidateTextWeightSum / candidateTextWeightCount;

					if (candidateTextSimilarity > bestOverallSimilarity) {
						numTies = 0;
						tiedCandidateTextIndices[numTies++] = candidateTextIndex;
						bestOverallSimilarity = candidateTextSimilarity;
					} else if (candidateTextSimilarity == bestOverallSimilarity) {
						tiedCandidateTextIndices[numTies++] = candidateTextIndex;
					}

				}

				bestCandidateTextIndex = tiedCandidateTextIndices[(int) (Math.random() * numTies)];

				similarityWeights[seedTextIndex][bestCandidateTextIndex]++;

				// System.out.println("seedTextIndex          = " + seedTextIndex);
				// System.out.println("bestCandidateTextIndex = " + bestCandidateTextIndex);

				numSeedsDone++;

			}
		}

		double totalWeight = 0;
		int totalNumCells = 0;
		for (int textIndex1 = 0; textIndex1 < numTexts; textIndex1++) {
			for (int textIndex2 = 0; textIndex2 < numTexts; textIndex2++) {
				totalWeight += similarityWeights[textIndex1][textIndex2];
				totalNumCells++;
			}
		}

		double averageWeight = totalWeight / totalNumCells;

		//
		// print table header
		//
		for (int textIndex1 = 0; textIndex1 < numTexts; textIndex1++) {
			System.out.print("\t");
			// System.out.print(tableNames.elementAt(textIndex1));
			System.out.printf("%7.7s", tableNames.elementAt(textIndex1));
		}
		System.out.println();

		for (int textIndex1 = 0; textIndex1 < numTexts; textIndex1++) {

			System.out.printf("%7.7s", tableNames.elementAt(textIndex1));

			for (int textIndex2 = 0; textIndex2 < numTexts; textIndex2++) {

				// double weight = (similarityWeights[realIndex1][realIndex2] - averageWeight) / averageWeight * 100;
				// double weight = similarityWeights[textIndex1][textIndex2] / averageWeight;
				double weight = similarityWeights[textIndex1][textIndex2];
				// double weight = similarityWeights[textIndex1][textIndex2] / numRounds;

				System.out.print("\t");
				System.out.printf("%7.5f", weight);

			}
			System.out.println();
		}

		double withinClassSimilarityWeight = 0;
		int withinClassSimilarityCount = 0;
		double betweenClassSimilarityWeight = 0;
		int betweenClassSimilarityCount = 0;
		for (int textIndex1 = 0; textIndex1 < numTexts; textIndex1++) {
			for (int textIndex2 = 0; textIndex2 < numTexts; textIndex2++) {
				if (textIndex1 != textIndex2) {
					betweenClassSimilarityWeight += similarityWeights[textIndex1][textIndex2];
					betweenClassSimilarityCount++;
				} else {
					withinClassSimilarityWeight += similarityWeights[textIndex1][textIndex2];
					withinClassSimilarityCount++;
				}
			}
		}
		double withinClassSimilarity = withinClassSimilarityWeight / withinClassSimilarityCount;
		double betweenClassSimilarity = betweenClassSimilarityWeight / betweenClassSimilarityCount;

		double withinVsBetweenSimilarityRatio = withinClassSimilarity / betweenClassSimilarity;

		System.out.println("withinVsBetweenSimilarityRatio = " + withinVsBetweenSimilarityRatio);

	}
}

