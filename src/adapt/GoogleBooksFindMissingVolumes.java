package adapt;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import com.google.gdata.client.GoogleService;
import com.google.gdata.client.books.BooksService;
import com.google.gdata.client.books.VolumeQuery;
import com.google.gdata.client.calendar.CalendarService;
import com.google.gdata.data.books.VolumeEntry;
import com.google.gdata.data.books.VolumeFeed;
import com.google.gdata.data.calendar.CalendarEntry;
import com.google.gdata.data.calendar.CalendarFeed;
import com.google.gdata.data.dublincore.Title;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;

public class GoogleBooksFindMissingVolumes {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		String downloadedVolumeKeysFilePath = null;
		String potentialVolumeKeysFilePath = null;

		if (args.length == 2) {
			downloadedVolumeKeysFilePath = args[0];
			potentialVolumeKeysFilePath = args[1];

		} else {

			downloadedVolumeKeysFilePath = "/data/text/google/found.dat";
			potentialVolumeKeysFilePath = "/data/text/google/gbd.out.all.V3";

		}

		int maxSymbolLength = 12;
		int maxNumSymbols = 256;
		FastHashTable fastHashTable = new FastHashTable(maxSymbolLength, maxNumSymbols);

		// phase 1
		Hashtable<Long, Boolean> downloadedVolumeKeys = new Hashtable<Long, Boolean>();
		{
			BufferedReader bufferedReader = null;
			try {
				bufferedReader = new BufferedReader(new FileReader(new File(downloadedVolumeKeysFilePath)));
			} catch (FileNotFoundException e3) {
				// TODO Auto-generated catch block
				e3.printStackTrace();
			}

			// LOAD FOUND VLOUME KEYS //

			int count = 0;
			while (true) {

				String line = null;
				try {
					line = bufferedReader.readLine();
				} catch (IOException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				if (line == null) {
					break;
				}
				count++;

				String[] lineParts = line.split("\t");

				String volumeKey = lineParts[0];

				if (volumeKey.length() != maxSymbolLength) {
					continue;
				}

				// System.out.println(volumeKey);

				long hashCode = fastHashTable.computeHashCode(volumeKey);

				downloadedVolumeKeys.put(hashCode, true);
				// System.out.println(downloadedVolumeKeys.size());

			}
			try {
				bufferedReader.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		// phase 2
		{
			BufferedReader bufferedReader = null;
			try {
				bufferedReader = new BufferedReader(new FileReader(new File(potentialVolumeKeysFilePath)));
			} catch (FileNotFoundException e3) {
				// TODO Auto-generated catch block
				e3.printStackTrace();
			}

			// LOAD FOUND VLOUME KEYS //

			int count = 0;
			int getCount = 0;
			while (true) {

				String line = null;
				try {
					line = bufferedReader.readLine();
				} catch (IOException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				if (line == null) {
					break;
				}
				count++;

				String[] lineParts = line.split("\t");

				String volumeKey = lineParts[0];

				if (volumeKey.length() != maxSymbolLength) {
					continue;
				}

				// System.out.println(volumeKey);

				long hashCode = fastHashTable.computeHashCode(volumeKey);

				Boolean result = downloadedVolumeKeys.get(hashCode);

				if (result == null) {
					// System.out.println("GET" + "\t" + volumeKey);
					System.out.println(volumeKey);
					downloadedVolumeKeys.put(hashCode, true);
					getCount++;
				}

				// System.out.println(downloadedVolumeKeys.size());

			} // while

			// System.out.println("getCount = " + getCount);

			try {
				bufferedReader.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
