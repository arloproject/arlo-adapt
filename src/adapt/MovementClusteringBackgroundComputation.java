package adapt;

public class MovementClusteringBackgroundComputation extends Thread {

	MovementClusterVis p;

	public MovementClusteringBackgroundComputation(MovementClusterVis movementClusterVis) {
		// TODO Auto-generated constructor stub
		p = movementClusterVis;
	}

	public void swap(int i1, int i2) {

		{
			int cellIndex1 = p.exampleCellIndices[i1];
			int cellIndex2 = p.exampleCellIndices[i2];
			// int cellExampleIndex1 exampleCellIndiceExampleIndices[i1];
			// int cellExampleIndex1 exampleCellIndiceExampleIndices[i1];
			// int temp = p.cellExampleIndices[cellIndex1];
			// p.exampleCellIndices[i1] = p.exampleCellIndices[i2];
			// p.exampleCellIndices[i2] = temp;
		}

		{
			int temp = p.exampleCellIndices[i1];
			p.exampleCellIndices[i1] = p.exampleCellIndices[i2];
			p.exampleCellIndices[i2] = temp;
		}

		{
			double[][] temp = p.examplesACC[i1];
			p.examplesACC[i1] = p.examplesACC[i2];
			p.examplesACC[i2] = temp;
		}
		{
			double temp = p.exampleXs[i1];
			p.exampleXs[i1] = p.exampleXs[i2];
			p.exampleXs[i2] = temp;
		}
		{
			double temp = p.exampleYs[i1];
			p.exampleYs[i1] = p.exampleYs[i2];
			p.exampleYs[i2] = temp;
		}
	}

	public void run() {


		int newXNumCells = MovementClusterVis.screenWidth / p.cellWidth;
		int newYNumCells = MovementClusterVis.screenHeight / p.cellWidth;

		p.xNumCells = newXNumCells;
		p.yNumCells = newYNumCells;

		p.numCells = p.xNumCells * p.yNumCells;

		p.maxNumExamplesPerCell = p.numExamples / p.numCells + 1;

		if (p.cellNumExamples == null || p.cellNumExamples.length != p.numCells) {
			p.cellNumExamples = new int[p.numCells];
			p.cellExampleIndices = new int[p.numCells][p.maxNumExamplesPerCell];
			p.cellXs = new int[p.numCells];
			p.cellYs = new int[p.numCells];

			int cellIndex = 0;
			for (int x = 0; x < p.xNumCells; x++) {
				for (int y = 0; y < p.yNumCells; y++) {
					p.cellXs[cellIndex] = x * p.cellWidth;
					p.cellYs[cellIndex] = y * p.cellHeight;
					cellIndex++;
				}
			}

		} else {
			for (int i = 0; i < p.numCells; i++) {
				p.cellNumExamples[i] = 0;
			}
		}

		double oldPerformance = p.computePerformance();

		int count = 0;
		if (false) {

			System.out.println("MovementClusteringBackgroundComputation working iteration #" + count);
			System.out.println("oldPerformance = " + oldPerformance);

			// make change
			int i1 = (int) (Math.random() * p.numExamples);
			int i2 = (int) (Math.random() * p.numExamples);

			swap(i1, i2);

			double newPerformance = p.computePerformance();
			System.out.println("newPerformance = " + newPerformance);

			double delta = newPerformance - oldPerformance;
			System.out.println("delta = " + delta);

			if (newPerformance > oldPerformance) {
				oldPerformance = newPerformance;

			} else {
				// undo change
				swap(i1, i2);
			}

			count++;
		}

	}

}
