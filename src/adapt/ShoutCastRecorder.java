package adapt;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.util.Date;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

public class ShoutCastRecorder extends Thread {

	String dataDirectoryString;
	String urlString;
	double duration;
	boolean play = false;
	boolean playing = false;
	float sampleRate = Float.NaN;
	int numChannels = -1;

	ShoutCastRecorder(String dataDirectoryString, String urlString, double duration, boolean play) {
		this.dataDirectoryString = dataDirectoryString;
		this.urlString = urlString;
		this.duration = duration;
		this.play = play;
	}

	public void run() {
		try {
			URL url = new URL(urlString);
			AudioInputStream in = AudioSystem.getAudioInputStream(url);
			AudioInputStream din = null;
			AudioFormat baseFormat = in.getFormat();

			AudioFormat decodedFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, baseFormat.getSampleRate(), 16, baseFormat.getChannels(), baseFormat.getChannels() * 2,
					baseFormat.getSampleRate(), false);

			// AudioFormat decodedFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, baseFormat.getSampleRate(), 16, 1, baseFormat.getChannels() * 2,
			// baseFormat.getSampleRate(), false);

			sampleRate = decodedFormat.getSampleRate();
			numChannels = decodedFormat.getChannels();

			if (sampleRate == 44100 && numChannels == 2) {

				System.out.println(decodedFormat);
				din = AudioSystem.getAudioInputStream(decodedFormat, in);
				// Play now.

				if (play) {
					rawplay(decodedFormat, din, duration);
				} else {
					writeFile(decodedFormat, din, duration);
				}
				
				in.close();
			}

		} catch (Exception e) {
			// Handle exception.
		}
	}

	private void rawplay(AudioFormat targetFormat, AudioInputStream din, double duration) {
		// byte[] data = new byte[32];
		byte[] data = new byte[4096];
		// byte[] data = new byte[16000];
		SourceDataLine line = null;
		try {
			line = getLine(targetFormat);
		} catch (LineUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (line != null) {
			// Start
			line.start();
			int nBytesRead = 0, nBytesWritten = 0;
			while (nBytesRead != -1) {

				try {
					nBytesRead = din.read(data, 0, data.length);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				if (nBytesRead != -1) {
					nBytesWritten = line.write(data, 0, nBytesRead);

					this.playing = true;

					// System.out.println(nBytesWritten);
				}

				double time = line.getMicrosecondPosition();
				// System.out.println(time);

				if (line.getMicrosecondPosition() / 1000.0 > duration) {
					break;
				}
			}
			// Stop
			line.drain();
			line.stop();
			line.close();

			if (false) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

			try {
				din.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private void writeFile(AudioFormat targetFormat, AudioInputStream din, double duration) {

		String path = dataDirectoryString + "/" + System.currentTimeMillis() + "_" + urlString.replaceAll("/", "\\\\") + "_" + (int) sampleRate + "_" + (int) numChannels + ".raw";

		FileOutputStream fw;
		try {
			fw = new FileOutputStream(new File(path), false);
		} catch (FileNotFoundException fnf) {
			throw new RuntimeException("File could not be opened for writing,\n either file exists but is a directory rather than a regular file, "
					+ "does not exist but cannot be created, or cannot be opened for some other reason.", fnf);
		}

		// byte[] data = new byte[32];
		byte[] data = new byte[4096];
		// byte[] data = new byte[16000];

		int numBytesToRead = (int) (duration * sampleRate * numChannels * 2);

		int totalNumBytesRead = 0;

		int nBytesRead = 0, nBytesWritten = 0;
		while (nBytesRead != -1) {

			try {
				nBytesRead = din.read(data, 0, data.length);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// System.out.println(totalNumBytesRead);
			try {
				fw.write(data, 0, nBytesRead);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (nBytesRead != -1) {

				totalNumBytesRead += nBytesRead;

				if (totalNumBytesRead >= numBytesToRead) {
					break;
				}

				// for (int i = 0; i < nBytesRead; i++) {
				// System.out.println(data[i]);
				// }

				this.playing = true;

				// System.out.println(nBytesWritten);
			}
		}
		try {
			din.close();
			fw.close();
			data = null;
			System.gc();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private SourceDataLine getLine(AudioFormat audioFormat) throws LineUnavailableException {
		SourceDataLine res = null;
		DataLine.Info info = new DataLine.Info(SourceDataLine.class, audioFormat);
		res = (SourceDataLine) AudioSystem.getLine(info);
		res.open(audioFormat);
		return res;
	}

}
