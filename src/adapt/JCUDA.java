package adapt;

/*
 * JCuda - Java bindings for NVIDIA CUDA driver and runtime API
 * http://www.jcuda.org
 *
 * Copyright 2011 Marco Hutter - http://www.jcuda.org
 */
import static jcuda.driver.JCudaDriver.*;

import java.io.*;
import java.util.Arrays;

import jcuda.*;
import jcuda.driver.*;

/**
 * This is a sample class demonstrating how to use the JCuda driver bindings to load and execute a CUDA vector addition kernel. The sample reads a CUDA file, compiles it to a PTX file using NVCC,
 * loads the PTX file as a module and executes the kernel function. <br />
 */
public class JCUDA {
	/**
	 * Entry point of this sample
	 * 
	 * @param args
	 *            Not used
	 * @throws IOException
	 *             If an IO error occurs
	 */
	public static void main(String args[]) throws IOException {
		int numGPUs = 1;
		int numPasses = (int) 10e6;

		int numElements = (int) 30e6;

		boolean emulate = false;
		boolean test = true;
		boolean copy = true;

		{
			int i = 0;

			if (args.length > i)
				numGPUs = Integer.parseInt(args[i++]);
			if (args.length > i)
				numPasses = Integer.parseInt(args[i++]);
			if (args.length > i)
				numElements = Integer.parseInt(args[i++]);
			if (args.length > i)
				emulate = Boolean.parseBoolean(args[i++]);
			if (args.length > i)
				test = Boolean.parseBoolean(args[i++]);
			if (args.length > i)
				copy = Boolean.parseBoolean(args[i++]);
		}

		System.out.println("numGPUs = " + numGPUs);
		System.out.println("numPasses = " + numPasses);
		System.out.println("numElements = " + numElements);
		System.out.println("emulate = " + emulate);
		System.out.println("test = " + test);
		System.out.println("copy = " + copy);

		CUdevice[] devices = null;
		CUcontext[] contexts = null;
		String ptxFileName = null;

		String code = "" + //
				"extern \"C\"\n" + //
				"__global__ void add(int n, double *a, double *b, double *sum)\n" + //
				"{\n" + //
				"\n" + //
				"\n" + //
				"	int problemIndex = blockIdx.x * gridDim.y * blockDim.x * blockDim.y + blockIdx.y * blockDim.x * blockDim.y + threadIdx.x * blockDim.y + threadIdx.y;\n" + //
				"\n" + //
				"	if (problemIndex < n )\n" + //
				"	{\n" + //
				"		sum[problemIndex] = a[problemIndex] + b[problemIndex];\n" + //
				"//		sum[problemIndex] = a[problemIndex];\n" + //
				"	}\n" + //
				"\n" + //
				"}\n" + //
				""; //

		String codeFilePath = "data/bin/code.cu";
		IO.writeTextFileFromString(codeFilePath, code);

		if (!emulate) {
			// Enable exceptions and omit all subsequent error checks
			JCudaDriver.setExceptionsEnabled(true);

			// Create the PTX file by calling the NVCC
			// String ptxFileName = preparePtxFile("/data/workspace/adapt/src/adapt/JCudaVectorAddKernel.cu");
			// String ptxFileName = preparePtxFile("/uf/ncsa/dtcheng/JCudaVectorAddKernel.cu");
			// ptxFileName = preparePtxFile("data/workspace/adapt/src/adapt/JCudaVectorAddKernel.cu");
			ptxFileName = preparePtxFile(codeFilePath);

			// Initialize the driver and create a context for the first device.
			cuInit(0);

			devices = new CUdevice[numGPUs];
			contexts = new CUcontext[numGPUs];

			for (int i = 0; i < numGPUs; i++) {

				devices[i] = new CUdevice();
				cuDeviceGet(devices[i], i);

				contexts[i] = new CUcontext();
				cuCtxCreate(contexts[i], 0, devices[i]);

			}
		}
		// CUdevice device = new CUdevice();
		// cuDeviceGet(device, deviceIndex);
		// CUcontext context = new CUcontext();
		// cuCtxCreate(context, 0, device);

		// Allocate and fill the host input data
		double hostInputA[][] = new double[numGPUs][];
		double hostInputB[][] = new double[numGPUs][];

		CUdeviceptr[] deviceInputAs = new CUdeviceptr[numGPUs];
		CUdeviceptr[] deviceInputBs = new CUdeviceptr[numGPUs];
		CUdeviceptr[] deviceOutputs = new CUdeviceptr[numGPUs];

		CUfunction[] functions = new CUfunction[numGPUs];

		for (int gpuIndex = 0; gpuIndex < numGPUs; gpuIndex++) {

			if (!emulate) {
				cuCtxSetCurrent(contexts[gpuIndex]);

				// Load the ptx file.
				CUmodule module = new CUmodule();
				cuModuleLoad(module, ptxFileName);
				// cuModuleLoad(module, code);

				// Obtain a function pointer to the "add" function.
				functions[gpuIndex] = new CUfunction();
				cuModuleGetFunction(functions[gpuIndex], module, "add");
			}

			// Allocate and fill the host input data
			hostInputA[gpuIndex] = new double[numElements];
			hostInputB[gpuIndex] = new double[numElements];
			for (int i = 0; i < numElements; i++) {
				hostInputA[gpuIndex][i] = (double) i;
				hostInputB[gpuIndex][i] = (double) i;
			}

			if (!emulate) {
				// Allocate the device input data
				deviceInputAs[gpuIndex] = new CUdeviceptr();
				cuMemAlloc(deviceInputAs[gpuIndex], numElements * Sizeof.DOUBLE);
				deviceInputBs[gpuIndex] = new CUdeviceptr();
				cuMemAlloc(deviceInputBs[gpuIndex], numElements * Sizeof.DOUBLE);
				// Allocate device output memory
				deviceOutputs[gpuIndex] = new CUdeviceptr();
				cuMemAlloc(deviceOutputs[gpuIndex], numElements * Sizeof.DOUBLE);
			}
		}

		// Allocate host output memory
		double hostOutputs[][] = new double[numGPUs][numElements];

		for (int passIndex = 0; passIndex < numPasses; passIndex++) {

			System.out.println("passIndex = " + passIndex);

			if (emulate) {

				Worker[] workers = new Worker[numGPUs];

				for (int gpuIndex = 0; gpuIndex < numGPUs; gpuIndex++) {
					workers[gpuIndex] = new Worker(hostInputA[gpuIndex], hostInputB[gpuIndex], hostOutputs[gpuIndex], numElements);
					workers[gpuIndex].start();
				}

				while (true) {
					boolean allDone = true;
					for (int gpuIndex = 0; gpuIndex < numGPUs; gpuIndex++) {
						if (workers[gpuIndex].isAlive()) {
							allDone = false;
						}
					}
					if (allDone)
						break;
				}

			} else {

				for (int gpuIndex = 0; gpuIndex < numGPUs; gpuIndex++) {

					System.out.println("gpuIndex = " + gpuIndex);

					cuCtxSetCurrent(contexts[gpuIndex]);

					if (passIndex == 0 || copy) {
						// Allocate the device input data, and copy the
						// host input data to the device
						cuMemcpyHtoD(deviceInputAs[gpuIndex], Pointer.to(hostInputA[gpuIndex]), numElements * Sizeof.DOUBLE);
						cuMemcpyHtoD(deviceInputBs[gpuIndex], Pointer.to(hostInputB[gpuIndex]), numElements * Sizeof.DOUBLE);
					}

					// Set up the kernel parameters: A pointer to an array
					// of pointers which point to the actual values.
					Pointer kernelParameters = Pointer.to(Pointer.to(new int[] { numElements }), Pointer.to(deviceInputAs[gpuIndex]), Pointer.to(deviceInputBs[gpuIndex]),
							Pointer.to(deviceOutputs[gpuIndex]));

					// Call the kernel function.

					int numProblems = numElements;
					int GPU_WARP_SIZE = 32;

					int numGridPoints = numProblems / GPU_WARP_SIZE;

					if (numProblems % GPU_WARP_SIZE != 0)
						numGridPoints++;

					if (false) {
						System.out.printf("numProblems = %d\n", numProblems);
						System.out.printf("numGridPoints = %d\n", numGridPoints);
					}

					int gridYDim = 1000;
					int gridXDim = numGridPoints / gridYDim + 1;

					if (false) {
						System.out.printf("gridXDim = %d\n", gridXDim);
						System.out.printf("gridYDim = %d\n", gridYDim);
					}

					int blockXDim = GPU_WARP_SIZE;
					int blockYDim = 1;

					if (false) {
						System.out.printf("blockXDim = %d\n", blockXDim);
						System.out.printf("blockYDim = %d\n", blockYDim);
					}

					CUfunction f = functions[gpuIndex];
					int gridDimX = gridXDim;
					int gridDimY = gridYDim;
					int gridDimZ = 1;
					int blockDimX = blockXDim;
					int blockDimY = blockYDim;
					int blockDimZ = 1;
					int sharedMemBytes = 0;
					CUstream hStream = null;
					Pointer kernelParams = kernelParameters;
					Pointer extra = null;

					cuLaunchKernel(f, gridDimX, gridDimY, gridDimZ, blockDimX, blockDimY, blockDimZ, sharedMemBytes, hStream, kernelParams, extra);

					// cuLaunchKernel(function, gridSizeX, 1, 1, // Grid dimension
					// blockSizeX, 1, 1, // Block dimension
					// 0, null, // Shared memory size and stream
					// kernelParameters, null // Kernel- and extra parameters
					// );

					// cuCtxSynchronize();

					// Copy the device output to the host.
					if (passIndex == 0 || copy) {
						cuMemcpyDtoH(Pointer.to(hostOutputs[gpuIndex]), deviceOutputs[gpuIndex], numElements * Sizeof.DOUBLE);
					}

				}

				for (int gpuIndex = 0; gpuIndex < numGPUs; gpuIndex++) {

					System.out.println("syncing gpuIndex = " + gpuIndex);

					cuCtxSetCurrent(contexts[gpuIndex]);
					cuCtxSynchronize();
				}

			}
			if (test) {
				for (int gpuIndex = 0; gpuIndex < numGPUs; gpuIndex++) {
					// Verify the result
					boolean passed = true;
					for (int i = 0; i < numElements; i++) {

						double expected = i + i;

						// System.out.println("hostOutput["+i+"] = " + hostOutput[i]);

						double error = Math.abs(hostOutputs[gpuIndex][i] - expected);

						if (error > 1e-5) {
							System.out.println("At index " + i + " found " + hostOutputs[gpuIndex][i] + " but expected " + expected);
							passed = false;
							break;
						}
					}
					System.out.println("Test " + (passed ? "PASSED" : "FAILED"));
				}
			}
		}

		// Clean up.

		if (!emulate) {
			for (int gpuIndex = 0; gpuIndex < numGPUs; gpuIndex++) {
				cuCtxSetCurrent(contexts[gpuIndex]);
				cuMemFree(deviceInputAs[gpuIndex]);
				cuMemFree(deviceInputBs[gpuIndex]);
				cuMemFree(deviceOutputs[gpuIndex]);
			}
		}
	}

	/**
	 * The extension of the given file name is replaced with "ptx". If the file with the resulting name does not exist, it is compiled from the given file using NVCC. The name of the PTX file is
	 * returned.
	 * 
	 * @param cuFileName
	 *            The name of the .CU file
	 * @return The name of the PTX file
	 * @throws IOException
	 *             If an I/O error occurs
	 */
	private static String preparePtxFile(String cuFileName) throws IOException {
		int endIndex = cuFileName.lastIndexOf('.');
		if (endIndex == -1) {
			endIndex = cuFileName.length() - 1;
		}
		String ptxFileName = cuFileName.substring(0, endIndex + 1) + "ptx";

		boolean compilePTX = true;
		if (!compilePTX) {
			File ptxFile = new File(ptxFileName);
			if (ptxFile.exists()) {
				return ptxFileName;
			}
		}

		File cuFile = new File(cuFileName);
		if (!cuFile.exists()) {
			throw new IOException("Input file not found: " + cuFileName);
		}
		String modelString = "-m" + System.getProperty("sun.arch.data.model");
		String command = "/usr/local/cuda/bin/nvcc " + modelString + " -ptx " + cuFile.getPath() + " -o " + ptxFileName;

		System.out.println("Executing\n" + command);
		Process process = Runtime.getRuntime().exec(command);

		String errorMessage = new String(toByteArray(process.getErrorStream()));
		String outputMessage = new String(toByteArray(process.getInputStream()));
		int exitValue = 0;
		try {
			exitValue = process.waitFor();
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			throw new IOException("Interrupted while waiting for nvcc output", e);
		}

		if (exitValue != 0) {
			System.out.println("nvcc process exitValue " + exitValue);
			System.out.println("errorMessage:\n" + errorMessage);
			System.out.println("outputMessage:\n" + outputMessage);
			throw new IOException("Could not create .ptx file: " + errorMessage);
		}

		System.out.println("Finished creating PTX file");
		return ptxFileName;
	}

	/**
	 * Fully reads the given InputStream and returns it as a byte array
	 * 
	 * @param inputStream
	 *            The input stream to read
	 * @return The byte array containing the data from the input stream
	 * @throws IOException
	 *             If an I/O error occurs
	 */
	private static byte[] toByteArray(InputStream inputStream) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte buffer[] = new byte[8192];
		while (true) {
			int read = inputStream.read(buffer);
			if (read == -1) {
				break;
			}
			baos.write(buffer, 0, read);
		}
		return baos.toByteArray();
	}

}

class Worker extends Thread {

	double hostInputA[];
	double hostInputB[];
	double hostOutputs[];
	int numElements;

	Worker(double[] hostInputA, double[] hostInputB, double[] hostOutputs, int numElements) {
		this.hostInputA = hostInputA;
		this.hostInputB = hostInputB;
		this.hostOutputs = hostOutputs;
		this.numElements = numElements;
	}

	public void run() {
		for (int i = 0; i < numElements; i++) {
			hostOutputs[i] = hostInputA[i] + hostInputB[i];
		}
	}
}
