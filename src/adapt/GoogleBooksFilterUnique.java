package adapt;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import com.google.gdata.client.GoogleService;
import com.google.gdata.client.books.BooksService;
import com.google.gdata.client.books.VolumeQuery;
import com.google.gdata.client.calendar.CalendarService;
import com.google.gdata.data.books.VolumeEntry;
import com.google.gdata.data.books.VolumeFeed;
import com.google.gdata.data.calendar.CalendarEntry;
import com.google.gdata.data.calendar.CalendarFeed;
import com.google.gdata.data.dublincore.Title;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;

public class GoogleBooksFilterUnique {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		String filePath = null;

		int keyNumber = Integer.parseInt(args[0]);
		int keyIndex = keyNumber - 1;

		int maxKeySize = Integer.parseInt(args[1]);

		int maxSymbolLength = maxKeySize;
		int maxNumSymbols = 256;
		FastHashTable fastHashTable = new FastHashTable(maxSymbolLength, maxNumSymbols);

		
		int numFiles = args.length - 2;

		// phase 1
		Hashtable<Long, Boolean> knownKeys = new Hashtable<Long, Boolean>();

		for (int fileIndex = 0; fileIndex < numFiles; fileIndex++) {

			filePath = args[fileIndex + 2];

			BufferedReader bufferedReader = null;
			try {
				bufferedReader = new BufferedReader(new FileReader(new File(filePath)));
			} catch (FileNotFoundException e3) {
				e3.printStackTrace();
			}

			int count = 0;
			while (true) {

				String line = null;
				try {
					line = bufferedReader.readLine();
				} catch (IOException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				if (line == null) {
					break;
				}
				count++;

				String[] lineParts = line.split("\t");

				String keyString = lineParts[keyIndex];
				
				if (keyString.length() > maxKeySize) {
					continue;
				}

				long hashcode = fastHashTable.computeHashCode(keyString);

				Boolean result = knownKeys.get(hashcode);

				if (result == null) {

					System.out.println(line);

					knownKeys.put(hashcode, true);

				}

			}
			try {
				bufferedReader.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}
}
