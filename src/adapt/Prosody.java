package adapt;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;
import java.util.Vector;

import org.python.modules.synchronize;

import com.mysql.jdbc.DatabaseMetaData;

public class Prosody {

	String username = "";
	String port = "3306";
	// String machine = "";
	String machine = "";
	// String machine = "";
	String dbInstance = "";
	// String dbInstance = "Shakespeare";
	String password = "";

	boolean normalizeForThreeLivesAnalysis = true;
	boolean normalizeForShakespearAnalysis = false;

	boolean useCache = false;

	boolean hasTEI = true;
	int problemGenerationStartTableIndex = 0;
	// int problemGenerationEndTableIndex = 29;
	int problemGenerationEndTableIndex = 1;
//	int maxNumPhonemesPerVolume = 1000;
	int maxNumPhonemesPerVolume = 999999999;

	// public String[] toUseVolumeNames = { "Four_Saints", "moa_2_0", "MatissePortrait_1_0", "MissFurr", "PicassoPortrait_1_0", "tender_1_0", "odyssey_1_1", "iliad_1_1", "ulysses_1_2",
	// "XMLNewEnglandCookBook_1_0", "threelives_2_0", "avary", "curry",
	// "dawson", "edmondson", "grimball", "howard", "jonescharles", "leconte", "jacobs", "lee", "pringle", "sawyer", "taylor", "veney", "worsham", "wood", "wyeth", "zettler", };

	public String[] toUseVolumeNames = { "threelives_2_0", "avary", "curry", "dawson", "edmondson", "grimball", "howard", "jonescharles", "leconte", "jacobs", "lee", "pringle", "sawyer", "taylor", "veney", "worsham", "wood", "wyeth", "zettler", };

	// boolean hasTEI = false;
	// int problemGenerationStartTableIndex = 0;
	// int problemGenerationEndTableIndex = 5;
	// // int maxNumPhonemesPerVolume = 1000;
	// int maxNumPhonemesPerVolume = 1000000;
	// public String[] toUseVolumeNames = { "1h4", "1h6", "2h6", "3h6", "he8", };

	public int numThreads = 16;
	int numRounds = (int) 1;
	double weightingPower = 32.0; //
	int windowSizeInPhonemes = 8; // 2.01@1, 6.2@4, 5.13@4,
	// int windowSizeInPhonemes = 14; // 2.01@1, 6.2@4, 5.13@4,

	int[] windowFeatureWeights;
	double[] differenceSumToWeight;

	double[][] similarityWeights;

	int numTexts = -1;
	public int reportIntervalInProblems = (int) 1e1;
	ArrayList<ProsodyProblem> prosodyProblems = null;
	int numProblems;
	ProsodyProblemSolver[] problemSolvers = null;

	int maxNumPhonemes = (int) 20e6;
	int numFeatures = 6;
	int maxNumSymbols = maxNumPhonemes * numFeatures;
	int maxNumTexts = (int) 1e3;
	int windowSizeInFeatures = -1;

	int part_of_speech_weight = 1;
	int accent_weight = 1;
	int stress_weight = 1;
	int tone_weight = 1;
	int phrase_id_weight = 1;
	int break_index_weight = 1;

	int[] featureWeights = new int[] { part_of_speech_weight, accent_weight, stress_weight, tone_weight, phrase_id_weight, break_index_weight };

	String[] featurePatterns = new String[numFeatures];

	int[] symbols = new int[maxNumSymbols];
	int maxNumDBStringBytes = (int) 0;
	byte[] dbStringBytes = new byte[maxNumDBStringBytes];
	int[] dbStringStartIndices = new int[maxNumPhonemes];

	String[] dbStrings = new String[maxNumSymbols];
	int symbolIndex = 0;
	int stringIndex = 0;

	int[] textEndSymbolIndex = new int[maxNumTexts];
	int textIndex = 0;

	public static void main(String[] args) {

		Prosody prosody = new Prosody();
		prosody.doit(args);
	}

	public void doit(String[] args) {

		 String dataDirectorPath = ".";
//		String dataDirectorPath = "/data/bin/";
		// String dataDirectorPath = "/uf/ncsa/dtcheng/data/bin/";
		// String dataDirectorPath = "/d250/data/bin/";

		{
			int argIndex = 0;

			if (args.length > argIndex)
				dataDirectorPath = args[argIndex];
			argIndex++;

			if (args.length > argIndex)
				numThreads = Integer.parseInt(args[argIndex]);
			argIndex++;

			if (args.length > argIndex)
				numRounds = Integer.parseInt(args[argIndex]);
			argIndex++;

			if (args.length > argIndex)
				windowSizeInPhonemes = Integer.parseInt(args[argIndex]);
			argIndex++;

			if (args.length > argIndex)
				weightingPower = Double.parseDouble(args[argIndex]);
			argIndex++;

			if (args.length > argIndex) {
				problemGenerationStartTableIndex = Integer.parseInt(args[argIndex]);
				problemGenerationEndTableIndex = problemGenerationStartTableIndex + 1;
			}
			argIndex++;

			if (args.length > argIndex)
				problemGenerationEndTableIndex = Integer.parseInt(args[argIndex]);
			argIndex++;

		}

		// Vector<String> tableNames = new Vector<String>();

		HashMap<String, Integer>[] symbolToIndex = new HashMap[numFeatures];

		for (int i = 0; i < numFeatures; i++) {
			symbolToIndex[i] = new HashMap();
		}

		int numSymbols = 0;
		int numExamples = 0;
		int numPhrases = 0;
		int numPhonemes = 0;

		if (!useCache) {
			String driver = "com.mysql.jdbc.Driver";

			try {
				Class.forName(driver).newInstance();
			} catch (InstantiationException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			} catch (IllegalAccessException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			} catch (ClassNotFoundException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}

			String url = "jdbc:mysql://" + machine + ":" + port + "/" + dbInstance;

			System.out.println("creating connection for " + url);

			try {
				Connection connection = DriverManager.getConnection(url, username, password);

				//
				// get list of all tables
				//

				java.sql.DatabaseMetaData md = connection.getMetaData();
				ResultSet rs = md.getTables(null, null, "%", null);
				while (rs.next()) {
					String tableName = rs.getString(3);

					System.out.println("tableName = " + tableName);
				}

				// int numTables = tableNames.size();
				// System.out.println("numTables = " + numTables);
				//
				// if (true)
				// for (int i = 0; i < numTables; i++) {
				// String tableName = tableNames.elementAt(i);
				// System.out.println(tableName);
				//
				// }

				int numTables = toUseVolumeNames.length;

				for (int i = 0; i < numTables; i++) {

					String lastUniquePhraseID = "";

					String tableName = toUseVolumeNames[i];

					System.out.println("processing  " + tableName);

					String query;
					query = "SELECT *  FROM " + tableName;

					Statement statement = connection.createStatement();
					ResultSet resultSet = statement.executeQuery(query);
					ResultSetMetaData resultSetMetaData = resultSet.getMetaData();

					int numColumns = resultSetMetaData.getColumnCount();
					// System.out.println("numColumns = " + numColumns);
					while (resultSet.next()) {

						int tei_paragraph_id = -1;
						if (hasTEI) {
							tei_paragraph_id = resultSet.getInt("tei_paragraph_id");
						}
						int sentence_id = resultSet.getInt("sentence_id");
						int phrase_id = resultSet.getInt("phrase_id");
						String word = resultSet.getString("word");
						String part_of_speech = resultSet.getString("part_of_speech");
						String accent = resultSet.getString("accent");
						String phoneme = resultSet.getString("phoneme");
						String stress = resultSet.getString("stress");
						String tone = resultSet.getString("tone");
						int break_index = resultSet.getInt("break_index");
						// int phoneme_id = resultSet.getInt("phoneme_id");
						// int word_id = resultSet.getInt("word_id");
						// int pos_id = resultSet.getInt("pos_id");

						String string = "";

						string += tableName + "\t";
						string += tei_paragraph_id + "\t";
						string += sentence_id + "\t";
						string += phrase_id + "\t";
						string += word + "\t";
						string += part_of_speech + "\t";
						string += accent + "\t";
						string += phoneme + "\t";
						string += stress + "\t";
						string += tone + "\t";
						string += break_index;
						// string += phoneme_id + "\t";
						// string += word_id + "\t";
						// string += pos_id;

						dbStrings[stringIndex++] = string;

						String uniquePhraseID = /* tei_chapter_id + tei_section_id + */"" + tei_paragraph_id + ":" + sentence_id + ":" + phrase_id;

						if (!uniquePhraseID.equals(lastUniquePhraseID)) {
							numPhrases++;
							lastUniquePhraseID = uniquePhraseID;
						}
						numPhonemes++;

						// String combinedPattern = null;
						// combinedPattern = part_of_speech + ":" + accent + ":" + stress + ":" + tone + ":" + phrase_id + ":" + break_index;

						featurePatterns[0] = part_of_speech;
						featurePatterns[1] = accent;
						featurePatterns[2] = stress;
						featurePatterns[3] = tone;
						featurePatterns[4] = Integer.toString(phrase_id);
						featurePatterns[5] = Integer.toString(break_index);

						for (int f = 0; f < numFeatures; f++) {
							if (!symbolToIndex[f].containsKey(featurePatterns[f])) {
								symbolToIndex[f].put(featurePatterns[f], numSymbols++);
							}
						}

						if (false) {
							System.out.print(tableName);
							for (int j = 0; j < numFeatures; j++) {
								int symbol = symbolToIndex[j].get(featurePatterns[j]);
								System.out.print("\t" + symbol);
							}
							System.out.println("\t" + word);
						}

						for (int j = 0; j < numFeatures; j++) {
							int symbol = symbolToIndex[j].get(featurePatterns[j]);
							symbols[symbolIndex++] = symbol;
						}
					}

					textEndSymbolIndex[textIndex] = symbolIndex;

					textIndex++;

					statement.close();
				}
			} catch (Exception e) {
				System.out.println(e);
			}

			System.out.println("numPhrases = " + numPhrases);
			System.out.println("numPhonemes = " + numPhonemes);

			double phonemsPerPhrase = (double) numPhonemes / numPhrases;
			System.out.println("phonemsPerPhrase = " + phonemsPerPhrase);

			int[] temp = null;

			temp = new int[symbolIndex];
			System.arraycopy(symbols, 0, temp, 0, symbolIndex);
			symbols = temp;

			temp = new int[textIndex];
			System.arraycopy(textEndSymbolIndex, 0, temp, 0, textIndex);
			textEndSymbolIndex = temp;

			IO.writeObject(dataDirectorPath + File.separatorChar + "symbols.ser", symbols);
			IO.writeObject(dataDirectorPath + File.separatorChar + "dbStrings.ser", dbStrings);
			IO.writeObject(dataDirectorPath + File.separatorChar + "textEndSymbolIndex.ser", textEndSymbolIndex);
			// IO.writeObject(dataDirectorPath + File.separatorChar + "tableNames.ser", tableNames);

		} else {

			symbols = (int[]) IO.readObject(dataDirectorPath + File.separatorChar + "symbols.ser");
			symbolIndex = symbols.length;
			dbStrings = (String[]) IO.readObject(dataDirectorPath + File.separatorChar + "dbStrings.ser");

			textEndSymbolIndex = (int[]) IO.readObject(dataDirectorPath + File.separatorChar + "textEndSymbolIndex.ser");
			textIndex = textEndSymbolIndex.length;

			// tableNames = (Vector<String>) IO.readObject(dataDirectorPath + File.separatorChar + "tableNames.ser");

		}

		int numTextSymbols = symbolIndex;
		numTexts = textIndex;

		System.out.println("numTextSymbols = " + numTextSymbols);
		System.out.println("numTexts = " + numTexts);

		// //////////////////////////// //
		// COMPUTE CONFUSION MATRIX //
		// //////////////////////////// //

		windowSizeInFeatures = windowSizeInPhonemes * numFeatures;
		similarityWeights = new double[numTexts][numTexts];

		windowFeatureWeights = new int[windowSizeInFeatures];
		for (int i = 0; i < windowFeatureWeights.length; i++) {
			windowFeatureWeights[i] = featureWeights[i % numFeatures];
		}

		int maxWeight = windowSizeInFeatures;
		differenceSumToWeight = new double[maxWeight + 1];
		for (int i = 0; i < differenceSumToWeight.length; i++) {
			double similarity = 1.0 - (double) i / windowSizeInFeatures;
			similarity = Math.pow(similarity, weightingPower);
			differenceSumToWeight[i] = similarity;
		}

		// create problems

		int numProblemsToCreate = -1;

		boolean useSampling = false;
		boolean useAllText = !useSampling;
		int problemIndex = 0;

		if (useSampling) {
			numProblemsToCreate = numRounds * numTexts;

			prosodyProblems = new ArrayList<ProsodyProblem>(numProblemsToCreate);
			for (int roundIndex = 0; roundIndex < numRounds; roundIndex++) {
				for (int seedTextIndex = 0; seedTextIndex < numTexts; seedTextIndex++) {
					// pick seed window
					int seedStartSymbolIndex = -1;
					if (seedTextIndex == 0) {
						seedStartSymbolIndex = 0;
					} else {
						seedStartSymbolIndex = textEndSymbolIndex[seedTextIndex - 1];
					}
					int seedNumSymbols = textEndSymbolIndex[seedTextIndex] - seedStartSymbolIndex;
					int seedWindowSymbolIndex = (int) (Math.random() * (seedNumSymbols - windowSizeInFeatures)) + seedStartSymbolIndex;
					seedWindowSymbolIndex = seedWindowSymbolIndex / numFeatures * numFeatures;

					ProsodyProblem prosodyProblem = new ProsodyProblem(problemIndex++, seedTextIndex, seedWindowSymbolIndex, numTexts);

					prosodyProblems.add(prosodyProblem);
				}
			}
		}
		if (useAllText) {

			prosodyProblems = new ArrayList<ProsodyProblem>(0);
			for (int seedVolumeIndex = problemGenerationStartTableIndex; seedVolumeIndex < problemGenerationEndTableIndex; seedVolumeIndex++) {

				int seedStartSymbolIndex = -1;
				if (seedVolumeIndex == 0) {
					seedStartSymbolIndex = 0;
				} else {
					seedStartSymbolIndex = textEndSymbolIndex[seedVolumeIndex - 1];
				}

				int seedNumSymbols = textEndSymbolIndex[seedVolumeIndex] - seedStartSymbolIndex;
				System.out.println("seedVolumeIndex = " + seedVolumeIndex);
				System.out.println("seedNumSymbols  = " + seedNumSymbols);

				int numWindows = (seedNumSymbols - windowSizeInFeatures) / numFeatures + 1;

				// int numWindows = seedNumSymbols / numFeatures;

				if (numWindows > maxNumPhonemesPerVolume) {
					numWindows = maxNumPhonemesPerVolume;
				}

				for (int i = 0; i < numWindows; i++) {

					int seedWindowSymbolIndex = i * numFeatures + seedStartSymbolIndex;

					ProsodyProblem prosodyProblem = new ProsodyProblem(problemIndex++, seedVolumeIndex, seedWindowSymbolIndex, numTexts);

					prosodyProblems.add(prosodyProblem);
				}

			}

		}

		numProblems = prosodyProblems.size();
		solveProblems();

		// scale all similarity values //
		Double minSimilarity = Double.POSITIVE_INFINITY;
		Double maxSimilarity = Double.NEGATIVE_INFINITY;
		for (int i = 0; i < numProblems; i++) {

			for (int j = 0; j < numTexts; j++) {

				double similarity = prosodyProblems.get(i).similarities[j];

				if (similarity < minSimilarity) {
					minSimilarity = similarity;
				}
				if (similarity > maxSimilarity) {
					maxSimilarity = similarity;
				}
			}

		}

		double range = maxSimilarity - minSimilarity;

		for (int i = 0; i < numProblems; i++) {

			for (int j = 0; j < numTexts; j++) {

				prosodyProblems.get(i).similarities[j] = (prosodyProblems.get(i).similarities[j] - minSimilarity) / range;

			}

		}

		System.out.print("DATA" + "\t" + "tableName" + "\t" + "tei_paragraph_id" + "\t" + "sentence_id" + "\t" + "phrase_id" + "\t" + "word" + "\t" + "part_of_speech" + "\t" + "accent" + "\t" + "phoneme" + "\t" + "stress" + "\t" + "tone" + "\t"
				+ "break_index");
		// "phoneme_id" + "\t" + "word_id" + "\t" + "pos_id");

		for (int j = 0; j < numTexts; j++) {
			System.out.print("\t" + toUseVolumeNames[j]);
		}
		if (normalizeForThreeLivesAnalysis) {
			System.out.print("\t" + "similarity_sum");
		}
		System.out.println();

		int phonemeIndex = 0;
		int lastVolumeIndex = -1;
		double windowNormalizedSimialaries[] = new double[numTexts];
		for (int i = 0; i < numProblems; i++) {

			ProsodyProblem problem = prosodyProblems.get(i);

			if (problem.seedTextIndex != lastVolumeIndex) {
				lastVolumeIndex = problem.seedTextIndex;
				phonemeIndex = 0;
			}

			String string = dbStrings[problem.seedWindowSymbolIndex / numFeatures];

			System.out.print("DATA" + "\t" + string);

			if (normalizeForThreeLivesAnalysis) {

				double similaritySum = 0.0;
				for (int j = 0; j < numTexts; j++) {

					if (j == problem.seedTextIndex)
						continue;

					if (phonemeIndex < windowSizeInPhonemes / 2)
						similaritySum += 1;
					else
						similaritySum += prosodyProblems.get(i - windowSizeInPhonemes / 2).similarities[j];

				}

				for (int j = 0; j < numTexts; j++) {

					if (j == problem.seedTextIndex)
						System.out.print("\t" + 0.0);
					else if (phonemeIndex < windowSizeInPhonemes / 2)
						System.out.print("\t" + 0.0);
					else
						System.out.printf("\t%7.5f", prosodyProblems.get(i - windowSizeInPhonemes / 2).similarities[j]);

				}

				System.out.printf("\t%7.5f",  similaritySum);

			}

			if (normalizeForShakespearAnalysis) {

				double similaritySum = 0.0;
				for (int j = 0; j < numTexts; j++) {

					if (j == problem.seedTextIndex)
						continue;

					if (phonemeIndex < windowSizeInPhonemes / 2)
						similaritySum += 1;
					else
						similaritySum += prosodyProblems.get(i - windowSizeInPhonemes / 2).similarities[j];

				}

				for (int j = 0; j < numTexts; j++) {

					if (j == problem.seedTextIndex)
						System.out.print("\t" + 0.0);
					else if (phonemeIndex < windowSizeInPhonemes / 2)
						System.out.print("\t" + 0.0);
					else
						System.out.printf("\t%7.5f", prosodyProblems.get(i - windowSizeInPhonemes / 2).similarities[j] / similaritySum);

				}
			}

			System.out.println();

			phonemeIndex++;

		}
		//
		// long startTime = System.currentTimeMillis();
		// int numSeedsDone = 0;
		//
		// for (int roundIndex = 0; roundIndex < numRounds; roundIndex++) {
		//
		// for (int seedTextIndex = 0; seedTextIndex < numTexts; seedTextIndex++) {
		//
		// long time = System.currentTimeMillis();
		//
		// double duration = (time - startTime) / 1000.0;
		//
		// double seedsPerSecond = numSeedsDone / duration;
		//
		// double timePerSeed = duration / (roundIndex * numTexts + seedTextIndex);
		// double totalRoundTime = timePerSeed * numTexts;
		// double totalTime = totalRoundTime * numRounds;
		// double timeLeft = totalTime - duration;
		//
		// System.out.println("roundIndex       = " + roundIndex);
		// System.out.println("seedTextIndex    = " + seedTextIndex);
		// System.out.println("seedsPerSecond   = " + seedsPerSecond);
		// System.out.println("timePerSeed      = " + timePerSeed);
		// System.out.println("totalRoundTime   = " + totalRoundTime);
		// System.out.println("totalTime        = " + totalTime);
		// System.out.println("timeLeft (s)     = " + timeLeft);
		// System.out.println("timeLeft (m)     = " + timeLeft / 60);
		// System.out.println("timeLeft (h)     = " + timeLeft / 3600);
		// System.out.println("timeLeft (d)     = " + timeLeft / 3600 / 24);
		//
		// // pick seed window
		// int seedStartSymbolIndex = -1;
		// if (seedTextIndex == 0) {
		// seedStartSymbolIndex = 0;
		// } else {
		// seedStartSymbolIndex = textEndSymbolIndex[seedTextIndex - 1];
		// }
		// int seedNumSymbols = textEndSymbolIndex[seedTextIndex] - seedStartSymbolIndex;
		// int seedWindowSymbolIndex = (int) (Math.random() * (seedNumSymbols - windowSizeInFeatures)) + seedStartSymbolIndex;
		// seedWindowSymbolIndex = seedWindowSymbolIndex / numFeatures * numFeatures;
		//
		// // System.out.println("seedWindowSymbolIndex = " + seedWindowSymbolIndex);
		//
		// // System.out.println("seedTextIndex          = " + seedTextIndex);
		// // System.out.println("bestCandidateTextIndex = " + bestCandidateTextIndex);
		//
		// numSeedsDone++;
		//
		// }
		// }

		double totalWeight = 0;
		int totalNumCells = 0;
		for (int textIndex1 = 0; textIndex1 < numTexts; textIndex1++) {
			for (int textIndex2 = 0; textIndex2 < numTexts; textIndex2++) {
				totalWeight += similarityWeights[textIndex1][textIndex2];
				totalNumCells++;
			}
		}

		double averageWeight = totalWeight / totalNumCells;

		//
		// print table header
		//
		for (int textIndex1 = 0; textIndex1 < numTexts; textIndex1++) {
			System.out.print("\t");
			// System.out.print(tableNames.elementAt(textIndex1));
			System.out.printf("%15.7s", toUseVolumeNames[textIndex1]);
		}
		System.out.println();

		for (int textIndex1 = 0; textIndex1 < numTexts; textIndex1++) {

			System.out.printf("%15.7s", toUseVolumeNames[textIndex1]);

			for (int textIndex2 = 0; textIndex2 < numTexts; textIndex2++) {

				// double weight = (similarityWeights[realIndex1][realIndex2] - averageWeight) / averageWeight * 100;
				// double weight = similarityWeights[textIndex1][textIndex2] / averageWeight;
				double weight = similarityWeights[textIndex1][textIndex2];
				// double weight = similarityWeights[textIndex1][textIndex2] / numRounds;

				System.out.print("\t");
				System.out.printf("%7.5f", weight);

			}
			System.out.println();
		}

		double withinClassSimilarityWeight = 0;
		int withinClassSimilarityCount = 0;
		double betweenClassSimilarityWeight = 0;
		int betweenClassSimilarityCount = 0;
		for (int textIndex1 = 0; textIndex1 < numTexts; textIndex1++) {
			for (int textIndex2 = 0; textIndex2 < numTexts; textIndex2++) {
				if (textIndex1 != textIndex2) {
					betweenClassSimilarityWeight += similarityWeights[textIndex1][textIndex2];
					betweenClassSimilarityCount++;
				} else {
					withinClassSimilarityWeight += similarityWeights[textIndex1][textIndex2];
					withinClassSimilarityCount++;
				}
			}
		}
		double withinClassSimilarity = withinClassSimilarityWeight / withinClassSimilarityCount;
		double betweenClassSimilarity = betweenClassSimilarityWeight / betweenClassSimilarityCount;

		double withinVsBetweenSimilarityRatio = withinClassSimilarity / betweenClassSimilarity;

		System.out.println("withinVsBetweenSimilarityRatio = " + withinVsBetweenSimilarityRatio);

	}

	int nextProblemIndex = 0;

	int getNextProblemIndexAndIncrement() {

		synchronized (this) {

			if (nextProblemIndex < numProblems) {

				int problemIndex = nextProblemIndex;

				nextProblemIndex++;

				return problemIndex;
			} else {
				return -1;
			}
		}

	}

	public void solveProblem(ProsodyProblem prosodyProblem) {

		int seedTextIndex = prosodyProblem.seedTextIndex;
		int seedWindowSymbolIndex = prosodyProblem.seedWindowSymbolIndex;

		double bestOverallSimilarity = Double.NEGATIVE_INFINITY;
		int bestCandidateTextIndex = -1;
		int numTies = 0;
		int[] tiedCandidateTextIndices = new int[numTexts];
		for (int candidateTextIndex = 0; candidateTextIndex < numTexts; candidateTextIndex++) {

			int candidateTextNumWindows = -1;
			int candidateWindowSymbolIndex = -1;
			if (candidateTextIndex == 0) {
				candidateTextNumWindows = textEndSymbolIndex[0] / numFeatures - windowSizeInFeatures / numFeatures + 1;
				candidateWindowSymbolIndex = 0;
			} else {
				candidateTextNumWindows = (textEndSymbolIndex[candidateTextIndex] - textEndSymbolIndex[candidateTextIndex - 1]) / numFeatures - windowSizeInFeatures / numFeatures + 1;
				candidateWindowSymbolIndex = textEndSymbolIndex[candidateTextIndex - 1];
			}

			double candidateTextWeightSum = 0.0;
			int candidateTextWeightCount = 0;
			for (int candidateWindowIndex = 0; candidateWindowIndex < candidateTextNumWindows; candidateWindowIndex++) {

				// ensure non overlapping windows
				// if (Math.abs(seedWindowSymbolIndex - candidateWindowSymbolIndex) > windowSizeInFeatures) {

				// compare windows
				int differenceSum = 0;
				int index1 = seedWindowSymbolIndex;
				int index2 = candidateWindowSymbolIndex;
				int featureIndex = 0;
				for (; featureIndex < windowSizeInFeatures; index1++, index2++, featureIndex++) {
					if (symbols[index1] != symbols[index2]) {
						differenceSum += windowFeatureWeights[featureIndex];
					}
				}

				double weight = differenceSumToWeight[differenceSum];

				// double similarity = 1.0 - (double) differenceSum / windowSizeInFeatures;

				// Double weight = differenceToWeightMap.get(similarity);
				// if (weight == null) {
				// weight = Math.pow(similarity, weightingPower);
				// differenceToWeightMap.put(differenceSum, weight);
				// }
				// similarity = weight;

				// similarity = Math.pow(similarity, weightingPower);

				// candidateTextWeightSum += similarity;

				candidateTextWeightSum += weight;
				candidateTextWeightCount++;

				// }

				candidateWindowSymbolIndex += numFeatures;

			}

			double candidateTextSimilarity = candidateTextWeightSum / candidateTextWeightCount;

			prosodyProblem.similarities[candidateTextIndex] = candidateTextSimilarity;

			if (candidateTextSimilarity > bestOverallSimilarity) {
				numTies = 0;
				tiedCandidateTextIndices[numTies++] = candidateTextIndex;
				bestOverallSimilarity = candidateTextSimilarity;
			} else if (candidateTextSimilarity == bestOverallSimilarity) {
				tiedCandidateTextIndices[numTies++] = candidateTextIndex;
			}

		}

		bestCandidateTextIndex = tiedCandidateTextIndices[(int) (Math.random() * numTies)];

		synchronized (this) {
			similarityWeights[seedTextIndex][bestCandidateTextIndex]++;
		}

		prosodyProblem.complete = true;
	}

	public void solveProblems() {

		nextProblemIndex = 0;

		/************************/
		/* start worker threads */
		/************************/

		problemSolvers = new ProsodyProblemSolver[numThreads];
		for (int i = 0; i < numThreads; i++) {
			problemSolvers[i] = new ProsodyProblemSolver(this, i);
			problemSolvers[i].start();
		}

		/*************************************************/
		/* wait until all worker threads have terminated */
		/*************************************************/
		int nextReportNumProblemsSolved = reportIntervalInProblems;

		Clock clock = new Clock();
		long startTime = System.currentTimeMillis();
		long lastReportTime = startTime;
		while (true) {

			int numProblemsSolved = countNumContiguousProblemsSolved();

			if (numProblemsSolved >= nextReportNumProblemsSolved) {
				long time = System.currentTimeMillis();

				double duration = (time - startTime) / 1000.0;

				double seedsPerSecond = numProblemsSolved / duration;

				double timePerSeed = duration / numProblemsSolved;
				double totalTimeEst = timePerSeed * numProblems;
				double timeLeft = totalTimeEst - duration;

				System.out.println("seedsPerSecond   = " + seedsPerSecond);
				System.out.println("timePerSeed      = " + timePerSeed);
				System.out.println("totalTimeEst   = " + totalTimeEst);
				System.out.println("timeLeft (s)     = " + timeLeft);
				System.out.println("timeLeft (m)     = " + timeLeft / 60);
				System.out.println("timeLeft (h)     = " + timeLeft / 3600);
				System.out.println("timeLeft (d)     = " + timeLeft / 3600 / 24);

				// System.out.println("numProblemsSolved = " + numProblemsSolved);

				System.out.println("numProblemsSolved = " + numProblemsSolved);
				System.out.println("numProblems       = " + numProblems);
				nextReportNumProblemsSolved += reportIntervalInProblems;
			}

			if (numProblemsSolved == numProblems) {
				break;
			}

			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	int countNumContiguousProblemsSolved() {

		int numProblemsSolved = 0;
		for (Iterator<ProsodyProblem> iterator = prosodyProblems.iterator(); iterator.hasNext();) {

			ProsodyProblem prosodyProblem = (ProsodyProblem) iterator.next();

			if (prosodyProblem.complete) {
				numProblemsSolved++;
			} else {
				break;
			}
		}

		return numProblemsSolved;
	}
}

class ProsodyProblem implements Serializable {

	public boolean complete;
	int overallIndex;

	int seedTextIndex;
	int seedWindowSymbolIndex;
	double similarities[];

	ProsodyProblem(int overallIndex, int seedTextIndex, int seedWindowSymbolIndex, int numTexts) {
		this.overallIndex = overallIndex;
		this.seedTextIndex = seedTextIndex;
		this.seedWindowSymbolIndex = seedWindowSymbolIndex;
		this.similarities = new double[numTexts];
	}

}

class ProsodyProblemSolver extends Thread {

	Prosody prosody;

	int numThreads = -1;
	int threadIndex = -1;

	boolean stopNow = false;
	boolean terminated = false;

	ProsodyProblemSolver(Prosody prosody, int threadIndex) {
		this.prosody = prosody;
		this.numThreads = prosody.numThreads;
		this.threadIndex = threadIndex;
	}

	public void run() {

		while (true) {

			if (stopNow) {
				break;
			}

			int problemIndex = prosody.getNextProblemIndexAndIncrement();

			if (problemIndex == -1) {
				break;
			}

			/*****************/
			/* SOLVE PROBLEM */
			/*****************/

			prosody.solveProblem(prosody.prosodyProblems.get(problemIndex));

		}

		terminated = true;

		return;
	}

	public void terminate() {
		stopNow = true;
	}

}
