package adapt;

import java.net.*;
import java.io.*;

public class LoadWiki {
	public static void main(String[] args) throws Exception {

		int count = 0;
		while (true) {

			String pathPrefix = "/data/text/wiki/raw/";
			String pathSuffix = ".htm";

			BufferedWriter bufferedWriter = null;
			try {
				String pathString = pathPrefix + count + pathSuffix;
				bufferedWriter = new BufferedWriter(new FileWriter(new File(pathString)));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			URL wikipediaURL = new URL("http://en.wikipedia.org/wiki/Special:Random");
			URLConnection wikipediaConnection = wikipediaURL.openConnection();
			BufferedReader in = new BufferedReader(new InputStreamReader(wikipediaConnection.getInputStream()));
			String inputLine;

			int lineCount = 0;

			while ((inputLine = in.readLine()) != null) {

				try {
					bufferedWriter.write(inputLine);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				if (lineCount == 3) {
					String title = inputLine.replaceAll("<title>", "").replaceAll(" - Wikipedia, the free encyclopedia</title>", "");
					System.out.println(count + "\t" + title);

				}
				lineCount++;
			}
			in.close();

			try {
				bufferedWriter.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			count++;

		}
	}
}