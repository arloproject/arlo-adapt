package adapt;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

import com.google.gdata.client.GoogleService;
import com.google.gdata.client.books.BooksService;
import com.google.gdata.client.books.VolumeQuery;
import com.google.gdata.client.calendar.CalendarService;
import com.google.gdata.data.books.VolumeEntry;
import com.google.gdata.data.books.VolumeFeed;
import com.google.gdata.data.calendar.CalendarEntry;
import com.google.gdata.data.calendar.CalendarFeed;
import com.google.gdata.data.dublincore.Title;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;

public class GoogleBooksReadClouds {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		


		

		// exclude no-preview books (by default, they are included)
		// query.setMinViewability(VolumeQuery.MinViewability.PARTIAL);

		BooksService service = new BooksService("exampleCo-exampleApp-1");
		try {
//			service.setUserCredentials("", "");
			service.setUserCredentials("", "");
		} catch (AuthenticationException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}

		
		
		String bookKeysFilePath = args[0];
		int startAttempNumber = Integer.parseInt(args[1]);
		int sleepTimeInMS = Integer.parseInt(args[2]);
		BufferedReader bufferedReader = null;
		try {
			bufferedReader = new BufferedReader(new FileReader(new File(bookKeysFilePath)));
		} catch (FileNotFoundException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}

		int attemptNumber = 0;
		while (true) {
			
			
			String line = null;
			try {
				line = bufferedReader.readLine();
			} catch (IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			if (line == null) {
				break;
			}
			attemptNumber++;

			if (attemptNumber < startAttempNumber) {
				continue;
			}
			
			
			String [] lineParts = line.split("\t");
			
			String volumeKey = lineParts[0];
			

			// TODO Auto-generated method stub
			VolumeQuery query = null;
			try {
				URL url = new URL("http://books.google.com/books?id=" + volumeKey + "&amp;ie=ISO-8859-1&amp;source=gbs_gdata");

				HashMap<String, String> parameters = new HashMap<String, String>();
				
				try {
					Object result = GoogleService.makePostRequest(url, parameters);
					
					
					String resultString = result.toString().replaceAll("\\n", "\\n");
					
					 System.out.println(volumeKey + "\t" + attemptNumber + "\t" + System.currentTimeMillis() + "\t" + resultString);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			

			try {
				Thread.sleep((long) (sleepTimeInMS));
//				Thread.sleep((long) (1000));
				
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}
		try {
			bufferedReader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
