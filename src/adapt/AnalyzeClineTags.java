package adapt;

import java.util.HashMap;

public class AnalyzeClineTags {

	/**
	 * @param args
	 * @throws Exception
	 */

	public static void main(String[] args) throws Exception {

		boolean analyzeMismatches = false;
		boolean analyzeConsensusCountsMismatches = true;

		SimpleTable table = null;

		if (analyzeMismatches) {
			table = IO.readDelimitedTable("/home/dtcheng/data.xls", "\t", true); // original both experts

		}
		if (analyzeConsensusCountsMismatches) {
			table = IO.readDelimitedTable("/home/dtcheng/data3.xls", "\t", true); // expert consensus for expertID == 47
		}

		int numRows = table.numDataRows;

		HashMap<String, Integer> aliasToIndex = new HashMap<String, Integer>();
		HashMap<Integer, String> indexToAlias = new HashMap<Integer, String>();

		int maxNumFiles = 999;

		int numExperts = 2;

		String[][] willNotLoad = new String[maxNumFiles][numExperts];
		String[][] blankPage = new String[maxNumFiles][numExperts];
		String[][] partialImage = new String[maxNumFiles][numExperts];
		String[][] flippedVertical = new String[maxNumFiles][numExperts];
		String[][] flippedHorizontal = new String[maxNumFiles][numExperts];
		String[][] otherBad = new String[maxNumFiles][numExperts];
		String[][] comment = new String[maxNumFiles][numExperts];
		String[][] cover_or_Title_or_TestPage = new String[maxNumFiles][numExperts];
		String[][] toc_or_Index = new String[maxNumFiles][numExperts];
		String[][] warrantsFurtherProcessing = new String[maxNumFiles][numExperts];
		String[][] numberofColumns = new String[maxNumFiles][numExperts];

		int numFiles = 0;
		for (int i = 0; i < numRows; i++) {

			int expertID = table.getInt(i, 0);
			String alias = table.getString(i, 1);
			String name = table.getString(i, 2);
			String value = table.getString(i, 3);

			// System.out.println("name = " + name);
			// System.out.println("value = " + value);
			if (!aliasToIndex.containsKey(alias)) {
				aliasToIndex.put(alias, numFiles);
				indexToAlias.put(numFiles, alias);
				numFiles++;
			}
			int fileIndex = aliasToIndex.get(alias);

			int expertIndex = -1;


			if (analyzeMismatches) {
				if (expertID == 46) {
					expertIndex = 0;
				}
				if (expertID == 47) {
					expertIndex = 1;
				}
			}
			if (analyzeConsensusCountsMismatches) {
				expertIndex = 0;
			}

			if (name.equals("willNotLoad"))
				willNotLoad[fileIndex][expertIndex] = value;
			if (name.equals("blankPage"))
				blankPage[fileIndex][expertIndex] = value;
			if (name.equals("partialImage"))
				partialImage[fileIndex][expertIndex] = value;
			if (name.equals("flippedVertical"))
				flippedVertical[fileIndex][expertIndex] = value;
			if (name.equals("flippedHorizontal"))
				flippedHorizontal[fileIndex][expertIndex] = value;
			if (name.equals("otherBad"))
				otherBad[fileIndex][expertIndex] = value;
			if (name.equals("comment"))
				comment[fileIndex][expertIndex] = value;
			if (name.equals("cover"))
				cover_or_Title_or_TestPage[fileIndex][expertIndex] = value;
			if (name.equals("index"))
				toc_or_Index[fileIndex][expertIndex] = value;
			if (name.equals("furtherProcessing"))
				warrantsFurtherProcessing[fileIndex][expertIndex] = value;
			if (name.equals("columns"))
				numberofColumns[fileIndex][expertIndex] = value;

		}

		System.out.println("numFiles = " + numFiles);

		if (analyzeMismatches) {

			int blankPageCount = 0;
			int partialImageCount = 0;
			int flippedVerticalCount = 0;
			int flippedHorizontalCount = 0;
			int otherBadCount = 0;
			int commentCount = 0;
			int cover_or_Title_or_TestPageCount = 0;
			int toc_or_IndexCount = 0;
			int warrantsFurtherProcessingCount = 0;
			int numberofColumnsCount = 0;

			int anyCount = 0;

			for (int i = 0; i < numFiles; i++) {
				// System.out.println(i + "\t" + numberofColumns[i][0] + "\t" + numberofColumns[i][1] + "\t" + warrantsFurtherProcessing[i][0] + "\t" + warrantsFurtherProcessing[i][1] + "\t" + indexToAlias.get(i));

				boolean mismatch = false;

				if (!blankPage[i][0].equals(blankPage[i][1])) {
					blankPageCount++;
					mismatch = true;
				}
				if (!partialImage[i][0].equals(partialImage[i][1])) {
					partialImageCount++;
					mismatch = true;
				}
				if (!flippedVertical[i][0].equals(flippedVertical[i][1])) {
					flippedVerticalCount++;
					mismatch = true;
				}
				if (!flippedHorizontal[i][0].equals(flippedHorizontal[i][1])) {
					flippedHorizontalCount++;
					mismatch = true;
				}
				if (!otherBad[i][0].equals(otherBad[i][1])) {
					otherBadCount++;
					mismatch = true;
				}
				// if (!comment[i][0].equals(comment[i][1])) {
				// commentCount++;
				// mismatch = true;
				// }
				if (!cover_or_Title_or_TestPage[i][0].equals(cover_or_Title_or_TestPage[i][1])) {
					cover_or_Title_or_TestPageCount++;
					mismatch = true;
				}
				if (!toc_or_Index[i][0].equals(toc_or_Index[i][1])) {
					toc_or_IndexCount++;
					mismatch = true;
				}
				if (!warrantsFurtherProcessing[i][0].equals(warrantsFurtherProcessing[i][1])) {
					warrantsFurtherProcessingCount++;
					mismatch = true;
				}
				if ((numberofColumns[i][0].length() == 1) && (numberofColumns[i][1].length() == 1) && //
						!numberofColumns[i][0].equals(numberofColumns[i][1])) {
					numberofColumnsCount++;
					mismatch = true;
				}

				if (mismatch) {
					anyCount++;

					System.out.println(indexToAlias.get(i));

				}

			}

			System.out.println("blankPageCount                  = " + blankPageCount);
			System.out.println("partialImageCount               = " + partialImageCount);
			System.out.println("flippedVerticalCount            = " + flippedVerticalCount);
			System.out.println("flippedHorizontalCount          = " + flippedHorizontalCount);
			System.out.println("otherBadCount                   = " + otherBadCount);
			// System.out.println("commentCount                    = " + commentCount);
			System.out.println("cover_or_Title_or_TestPageCount = " + cover_or_Title_or_TestPageCount);
			System.out.println("toc_or_IndexCount               = " + toc_or_IndexCount);
			System.out.println("warrantsFurtherProcessingCount  = " + warrantsFurtherProcessingCount);
			System.out.println("numberofColumnsCount            = " + numberofColumnsCount);
			System.out.println("anyCount                        = " + anyCount);

		}

		if (analyzeConsensusCountsMismatches) {

			int willNotLoadCount = 0;
			int blankPageCount = 0;
			int partialImageCount = 0;
			int flippedVerticalCount = 0;
			int flippedHorizontalCount = 0;
			int otherBadCount = 0;
			int commentCount = 0;
			int cover_or_Title_or_TestPageCount = 0;
			int toc_or_IndexCount = 0;
			int warrantsFurtherProcessingCount = 0;
			int numberofColumnsCount = 0;

			int reconsiderCount = 0;

			for (int i = 0; i < numFiles; i++) {
				// System.out.println(i + "\t" + numberofColumns[i][0] + "\t" + numberofColumns[i][1] + "\t" + warrantsFurtherProcessing[i][0] + "\t" + warrantsFurtherProcessing[i][1] + "\t" + indexToAlias.get(i));

				boolean reconsider = false;
				boolean warrantsFurtherProcessingBoolean = false;

				if (willNotLoad[i][0].equals("True")) {
					willNotLoadCount++;
//					reconsider = true;
				}

				if (blankPage[i][0].equals("True")) {
					blankPageCount++;
//					reconsider = true;
				}
				if (partialImage[i][0].equals("True")) {
					partialImageCount++;
					reconsider = true;
				}
				if (flippedVertical[i][0].equals("True")) {
					flippedVerticalCount++;
//					reconsider = true;
				}
				if (flippedHorizontal[i][0].equals("True")) {
					flippedHorizontalCount++;
//					reconsider = true;
				}
				if (otherBad[i][0].equals("True")) {
					otherBadCount++;
					reconsider = true;
				}
				// if (comment[i][0].equals(comment[i][1])) {
				// commentCount++;
				// mismatch = true;
				// }
				if (cover_or_Title_or_TestPage[i][0].equals("True")) {
					cover_or_Title_or_TestPageCount++;
//					reconsider = true;
				}
				if (toc_or_Index[i][0].equals("True")) {
					toc_or_IndexCount++;
//					reconsider = true;
				}
				if (warrantsFurtherProcessing[i][0].equals("True")) {
					warrantsFurtherProcessingCount++;
					warrantsFurtherProcessingBoolean = true;
				}
				// if ((numberofColumns[i][0].length() == 1) && (numberofColumns[i][1].length() == 1) && //
				// numberofColumns[i][0].equals(numberofColumns[i][1])) {
				// numberofColumnsCount++;
				// mismatch = true;
				// }

				if (reconsider && !warrantsFurtherProcessingBoolean) {
					
					reconsiderCount++;

					System.out.println(indexToAlias.get(i));

				}

			}

			System.out.println("willNotLoadCount                = " + willNotLoadCount);
			System.out.println("blankPageCount                  = " + blankPageCount);
			System.out.println("partialImageCount               = " + partialImageCount);
			System.out.println("flippedVerticalCount            = " + flippedVerticalCount);
			System.out.println("flippedHorizontalCount          = " + flippedHorizontalCount);
			System.out.println("otherBadCount                   = " + otherBadCount);
			// System.out.println("commentCount                    = " + commentCount);
			System.out.println("cover_or_Title_or_TestPageCount = " + cover_or_Title_or_TestPageCount);
			System.out.println("toc_or_IndexCount               = " + toc_or_IndexCount);
			System.out.println("warrantsFurtherProcessingCount  = " + warrantsFurtherProcessingCount);
			System.out.println("numberofColumnsCount            = " + numberofColumnsCount);
			System.out.println("reconsiderCount                 = " + reconsiderCount);

		}

	}
}
