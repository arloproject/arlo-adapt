extern "C"
__global__ void add(int n, double *a, double *b, double *sum)
{


	int problemIndex = blockIdx.x * gridDim.y * blockDim.x * blockDim.y + blockIdx.y * blockDim.x * blockDim.y + threadIdx.x * blockDim.y + threadIdx.y;

	if (problemIndex < n )
	{
		sum[problemIndex] = a[problemIndex] + b[problemIndex];
//		sum[problemIndex] = a[problemIndex];
	}

}
