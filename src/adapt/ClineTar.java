package adapt;

import java.security.*;
import java.math.*;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.HeadlessException;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.FileInputStream;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Vector;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.imageio.ImageIO;

import org.python.antlr.PythonParser.exec_stmt_return;
import org.tritonus.share.sampled.file.THeaderlessAudioFileWriter;

import adapt.InstancedBasedPredictorFullCrossValidation.InstancedBasedPredictorKernel;
import arlo.Correlation;

import com.amd.aparapi.Kernel;
import com.amd.aparapi.Range;
import com.kenai.jffi.Array;

import jtar.TarConstants;
import jtar.TarEntry;
import jtar.TarInputStream;

public class ClineTar {

	static public String dataDirectory;

	static boolean useOCRFileCache;
	static boolean fullResolution;

	static int numImagesToRate;

	static public String expertName;
	static Random titleOffsetRandom;
	static Random unseenFileOrderRandom;

	static public boolean mousePressed;
	static public int mousePressX;
	static public int mousePressY;
	static boolean imageSelected;
	private static Boolean clickToProceed;

	static public boolean keyPressed;

	static boolean expertRatingMode;

	static boolean computeOCRCharacterPositions;
	static boolean displayOCRCharacterPositions;
	static int wordDelayTimeInMS;
	static int charDelayTimeInMS;

	static boolean reportClassifications;

	static boolean useBiasOriginAsFirstBiasPick;

	static int applyBiasShowImagesDwellTime;
	static int displayScreenWidth;
	static int displayScreenHeight;
	static int thumbNailSizeX;
	static int thumbNailSizeY;

	static int maxPngDataSize;
	static boolean checkForNANs;

	static boolean normalizeInputValues;
	static int aparapiGroupSize;

	static int numTestImagesPerBatch;

	static int TRAINING_MODE;
	static int TESTING_MODE;

	static boolean useAnnotatedDirectoryTableFilePath;

	private static int maxNumTrials;
	static boolean stopAfterOneTrial;

	static boolean validateSkipImagesWithoutTags;
	static boolean useOnlyMarkedImagesForTrainAndTest;

	static boolean useSEQ;
	static boolean useJTP;
	static boolean useGPU;

	static int experimentWindowSize;
	static String commandString;

	private static boolean optimizerTrueRandom;
	private static int optimizerRandomSeed;
	private static int optimizerXSmoothingWindowSize;
	private static int yWindowSize;
	private static int halfYWindowSize;
	private static int reducedGridSize;
	private static double distanceWeightingPower;
	private static double minDiff;
	private static int targetNumSamples;
	private static double blackWeight;
	private static double titleProbabilityOffset;
	private static double leftIgnoreFraction;
	private static double rightIgnoreFraction;
	private static double headerNoTitleZoneFraction;
	private static double footerNoTitleZoneFraction;

	//

	// bias dimension indices //
	private static int REDUCION_FACTOR_LOG2_INDEX;
	private static int CLASSIFICATION_THRESHOLD_INDEX;

	// data type indices //
	private static final int DATA_TYPE_INT_LINEAR_INDEX = 0;
	private static final int DATA_TYPE_DOUBLE_LINEAR_INDEX = 1;
	private static final int DATA_TYPE_INT_LOG2_INDEX = 2;
	private static final int DATA_TYPE_DOUBLE_LOG_INDEX = 3;

	private static int MAX_NUM_MARKED_REGIONS_PER_IMAGE;

	static int numClasses;

	private static String[] biasNames;
	private static int numBiasDimensions;

	private static int[] biasTypes;

	private static int maxNumTestExamplesToUse;

	private static double[] biasInitialPoint;

	private static double[] biasOrigin;

	private static double[] biasLowerBounds;
	private static double[] biasUpperBounds;

	private static String dataDirectoryPath;

	private static int maxNumEntries;

	private static int maxNumEntriesToProcess;

	private static boolean sequential;
	private static boolean trueRandom; // new random seed each time

	private static Boolean createExamples = false;

	private static Boolean singleColumnOnly = false;
	private static Boolean dualColumnOnly = false;

	private static Boolean fbisOnly = false;
	private static Boolean swbOnly = false;

	private static Boolean createOriginalImageTarFileIndices = false;
	private static Boolean createReducedImages = false;
	private static Boolean createGroundTruthImageTarFileIndices = false;
	private static Boolean validateImagesAndFindMarkedRegions = false;
	private static Boolean analyzMarkedRegions = false;
	private static Boolean applyOCR = false;
	private static Boolean optimizeBias = false;
	private static Boolean useOptimizedBiasToClassifyNewExamples = false;
	private static Boolean analyzeOCR = false;

	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//

	public static void main(String args[]) throws Exception {

		// convertToPNG();
		// applyOCR();
		// getRandomSampleOfPngAndBoxFiles();
		// System.exit(0);

		fullResolution = false;

		clickToProceed = true;

		numImagesToRate = 999999999;

		expertName = "defaultExpertName";
		titleOffsetRandom = new Random(111111111);
		unseenFileOrderRandom = new Random(222222222);

		mousePressX = -1;
		mousePressY = -1;
		imageSelected = false;

		expertRatingMode = false;

		computeOCRCharacterPositions = true;
		displayOCRCharacterPositions = true;
		wordDelayTimeInMS = 0;
		charDelayTimeInMS = 0;

		reportClassifications = false;

		useBiasOriginAsFirstBiasPick = false;

		applyBiasShowImagesDwellTime = 0;
		// int unseenFileOrderRandomSeed = 123;
		// int unseenFileOrderRandomSeed = (int) (Math.random() * 100000);
		displayScreenWidth = /* 1024 + */2000;
		displayScreenHeight = /* 1024 + */1500;
		// int displayScreenWidth = 1024;
		// int displayScreenHeight = 768;
		thumbNailSizeX = displayScreenWidth / 2 / 2;
		thumbNailSizeY = displayScreenHeight / 3;

		maxPngDataSize = 10000000;
		checkForNANs = false;

		normalizeInputValues = true;
		// int groupSize = 256;
		aparapiGroupSize = 16;

		numTestImagesPerBatch = 1; // only works for numTestImagesPerBatch=1 right now

		TRAINING_MODE = 0;
		TESTING_MODE = 1;

		useAnnotatedDirectoryTableFilePath = true;

		// int maxNumTrials = 999999;
		maxNumTrials = -1;
		stopAfterOneTrial = false;

		validateSkipImagesWithoutTags = false;
		useOnlyMarkedImagesForTrainAndTest = true;

		useSEQ = false;
		useJTP = true;
		useGPU = !(useJTP || useSEQ);

		experimentWindowSize = 32;

		// String commandString =
		// "java -Xms16G -Xmx16G -jar j.jar validateImagesAndFindMarkedRegions 123456789 2 750 1 annotatedDirectories.xls groundTruthImageSizes.xls userMarkedRegions.xls machineMarkedRegions.xls marked.tar original.tar sample.tar 1 1";
		// String commandString =
		// "java -Xms12G -Xmx12G -jar j.jar useOptimizedBiasToClassifyNewExamples -1 1 128 11 annotatedDirectories.xls groundTruthImageSizes.xls userMarkedRegions.xls machineMarkedRegions.xls marked.tar original.tar sample.tar 1 1";
		// String commandString =
		// "java -Xms12G -Xmx12G -jar j.jar useOptimizedBiasToClassifyNewExamples -1 1 128 11 annotatedDirectories.xls groundTruthImageSizes.xls userMarkedRegions.xls machineMarkedRegions.xls marked.tar original.tar sample.tar 1 1";
		// String commandString =
		// "java -Xms12G -Xmx12G -jar j.jar useOptimizedBiasToClassifyNewExamples -1 1 128 11 annotatedDirectories.xls groundTruthImageSizes.xls userMarkedRegions.xls machineMarkedRegions.xls marked.tar original.tar sample.tar 1 1";

		//
		// String commandString = "java -Xms12G -Xmx12G -jar j.jar  -1 999999 " + experimentWindowSize + " " + ((experimentWindowSize / 4) - 1)
		// + " annotatedDirectories.xls groundTruthImageSizes.xls usoptimizeBiaserMarkedRegions.xls machineMarkedRegions.xls marked.tar original.tar sample.tar 1 1";
		//
		// String commandString = "java -Xms12G -Xmx12G -jar j.jar optimizeBias -1 999999 " + experimentWindowSize + " " + ((experimentWindowSize / 4) - 1)
		// + " annotatedDirectories.xls groundTruthImageSizes.xls userMarkedRegions.xls machineMarkedRegions.xls marked.tar original.tar sample.tar 1 1";
		// String commandString = "java -Xms12G -Xmx12G -jar j.jar useOptimizedBiasToClassifyNewExamples -1 999999 " + experimentWindowSize + " " + ((experimentWindowSize / 4) - 1)
		// + " annotatedDirectories.xls groundTruthImageSizes.xls userMarkedRegions.xls machineMarkedRegions.xls marked.tar original.tar sample.tar 1 1";

		useOCRFileCache = false;

		// String commandString = "java -Xms12G -Xmx12G -jar j.jar applyOCR -1 999999 " + experimentWindowSize + " " + ((experimentWindowSize / 4) - 1)
		// + " annotatedDirectories.xls groundTruthImageSizes.xls userMarkedRegions.xls machineMarkedRegions.xls marked.tar original.tar sample.tar 8 8";

		// String commandString = "java -Xms12G -Xmx12G -jar j.jar applyOCR -1 999999 " + experimentWindowSize + " " + ((experimentWindowSize / 4) - 1)
		// + " annotatedDirectories.xls groundTruthImageSizes.xls userMarkedRegions.xls machineMarkedRegions.xls marked.tar original.tar marked.tar 8 8";

		// String commandString = "java -Xms12G -Xmx12G -jar j.jar applyOCR -1 999999 " + experimentWindowSize + " " + ((experimentWindowSize / 4) - 1)
		// + " annotatedDirectories.xls groundTruthImageSizes.xls userMarkedRegions.xls machineMarkedRegions.xls marked.tar original.tar PRODUCTIONSCANS.tar 1 1";
		//
		// String commandString = "java -Xms12G -Xmx12G -jar j.jar analyzeOCR -1 999999 " + experimentWindowSize + " " + ((experimentWindowSize / 4) - 1)
		// + " annotatedDirectories.xls groundTruthImageSizes.xls userMarkedRegions.xls machineMarkedRegions.xls marked.tar original.tar sample.tar 1 1";

		// String commandString = "java -Xms12G -Xmx12G -jar j.jar analyzeOCR -1 999999 " + experimentWindowSize + " " + ((experimentWindowSize / 4) - 1)
		// + " annotatedDirectories.xls groundTruthImageSizes.xls userMarkedRegions.xls machineMarkedRegions.xls marked.tar marked.tar marked.tar 1 1";

		String commandString = "java -Xms12G -Xmx12G -jar j.jar useOptimizedBiasToClassifyNewExamples -1 999999 " + experimentWindowSize + " " + ((experimentWindowSize / 4) - 1)
				+ " annotatedDirectories.xls groundTruthImageSizes.xls userMarkedRegions.xls machineMarkedRegions.xls marked.tar original.tar sample10000.tar 1 1";

		optimizerTrueRandom = false;
		optimizerRandomSeed = -1; // e.g. 123
		optimizerXSmoothingWindowSize = 1; // e.g. 8
		yWindowSize = 1;
		halfYWindowSize = yWindowSize / 2;
		reducedGridSize = -1; // e.g. 256
		distanceWeightingPower = 7.0; // e.g. 32.0
		minDiff = Double.NaN;
		targetNumSamples = 999999999;
		// int maxWindowSizeForOptimization = 2;
		blackWeight = -0.0;
		// double titleProbabilityOffset = 0.30;
		titleProbabilityOffset = Double.NaN;
		leftIgnoreFraction = Double.NaN;
		rightIgnoreFraction = Double.NaN;
		headerNoTitleZoneFraction = Double.NaN;
		footerNoTitleZoneFraction = Double.NaN;

		// int maxHalfWindowSizeForOptimization = maxWindowSizeForOptimization / 2;

		//

		// bias dimension indices //
		REDUCION_FACTOR_LOG2_INDEX = 0;
		CLASSIFICATION_THRESHOLD_INDEX = 1;

		MAX_NUM_MARKED_REGIONS_PER_IMAGE = 20;

		numClasses = 2;

		biasNames = new String[] { "distanceWeightingPower", "titleProbabilityOffset", "minDiff", "optimizerXSmoothingWindowSize", "leftIgnoreFraction", "rightIgnoreFraction", "headerNoTitleZoneFraction", "footerNoTitleZoneFraction" };
		numBiasDimensions = biasNames.length;

		biasTypes = new int[] { DATA_TYPE_DOUBLE_LINEAR_INDEX, DATA_TYPE_DOUBLE_LINEAR_INDEX, DATA_TYPE_DOUBLE_LOG_INDEX, DATA_TYPE_INT_LINEAR_INDEX, DATA_TYPE_DOUBLE_LINEAR_INDEX, DATA_TYPE_DOUBLE_LINEAR_INDEX, DATA_TYPE_DOUBLE_LINEAR_INDEX,
				DATA_TYPE_DOUBLE_LINEAR_INDEX };

		maxNumTestExamplesToUse = 999999999;

		biasInitialPoint = null;

		// double[] biasOrigin = { 3.713482681817795, 0.23926972589190584 - 0.10, 0.000001, 37.0 * experimentWindowSize / 64, 0.1713162631270948, 0.2324867553129233, 0.0382631359459857, 0.2331533189378593 };
		// double[] biasOrigin = { 3.713482681817795, 0.23926972589190584 - 0.05, 0.000001, 37.0 * experimentWindowSize / 64, 0.1713162631270948, 0.2324867553129233, 0.0382631359459857, 0.2331533189378593 };
		biasOrigin = new double[] { 3.713482681817795, 0.23926972589190584 + 0.00, 0.000001, 37.0 * experimentWindowSize / 64, 0.1713162631270948, 0.2324867553129233, 0.0382631359459857, 0.2331533189378593 };
		// double[] biasOrigin = { 3.713482681817795, 0.23926972589190584 + 0.05, 0.000001, 37.0 * experimentWindowSize / 64, 0.1713162631270948, 0.2324867553129233, 0.0382631359459857, 0.2331533189378593 };
		// double[] biasOrigin = { 3.713482681817795, 0.23926972589190584 + 0.10, 0.000001, 37.0 * experimentWindowSize / 64, 0.1713162631270948, 0.2324867553129233, 0.0382631359459857, 0.2331533189378593 };

		biasLowerBounds = new double[] { biasOrigin[0] - 0.5, biasOrigin[1] - 0.05, biasOrigin[2], biasOrigin[3] / 1.5, biasOrigin[4] - 0.05, biasOrigin[5] - 0.05, 0.000000000000000000, biasOrigin[7] - 0.05, };
		biasUpperBounds = new double[] { biasOrigin[0] + 0.5, biasOrigin[1] + 0.05, biasOrigin[2], biasOrigin[3] * 1.5, biasOrigin[4] + 0.05, biasOrigin[5] + 0.05, biasOrigin[6] + 0.05, biasOrigin[7] + 0.05, };

		// double[] biasLowerBounds = { biasOrigin[0], biasOrigin[1], biasOrigin[2], biasOrigin[3], biasOrigin[4], biasOrigin[5], biasOrigin[6], biasOrigin[7] };
		// double[] biasUpperBounds = { biasOrigin[0], biasOrigin[1], biasOrigin[2], biasOrigin[3], biasOrigin[4], biasOrigin[5], biasOrigin[6], biasOrigin[7] };

		dataDirectoryPath = "/data/cline/";

		// String tarFilePath = "/data/cline.tar";
		// String tarFilePath = "/data/cline/images.tar";

		maxNumEntries = (int) 7e6;

		// int maxNumEntriesToProcess = 100000;
		maxNumEntriesToProcess = Integer.MAX_VALUE;
		// int maxNumEntriesToProcess = (int) 1e6;

		sequential = false;
		trueRandom = false; // new random seed each time

		//
		//
		//
		//
		//
		//
		// !!! !!! !!!
		//
		// {
		// String originalTarFilePath = args[0];
		// createTarFileIndex(originalTarFilePath, null, false);
		// System.exit(0);
		// }

		// !!! !!! !!!
		//
		//
		//
		//
		//
		//
		//
		//

		if (args.length > 4) {

			expertName = args[0];
			displayScreenWidth = Integer.parseInt(args[1]);
			displayScreenHeight = Integer.parseInt(args[2]);
			numImagesToRate = Integer.parseInt(args[3]);

			thumbNailSizeX = displayScreenWidth / 2 / 2;
			thumbNailSizeY = displayScreenHeight / 3;

		}

		if (false) {
			try {
				// int numFiles = 16;
				int numFiles = 1;
				String pathString = dataDirectoryPath + "pngs.out";

				SimpleTable table = IO.readDelimitedTable(pathString, "/t", false);
				table.printHeader = false;
				IO.writeDelimitedTableSplitUp(table, pathString, numFiles, true);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.exit(0);
		}

		if (true) {

			String[] parts = commandString.split(" ");
			int numParameters = parts.length - 5;

			args = new String[numParameters];
			for (int i = 0; i < numParameters; i++) {
				args[i] = parts[i + 5];
			}

		}

		int index = 0;

		String modeString = args[index++];

		createOriginalImageTarFileIndices = false;
		createReducedImages = false;
		createGroundTruthImageTarFileIndices = false;
		validateImagesAndFindMarkedRegions = false;
		analyzMarkedRegions = false;
		optimizeBias = false;
		useOptimizedBiasToClassifyNewExamples = false;
		analyzeOCR = false;

		if (modeString.contains("createGroundTruthImageTarFileIndices")) {
			createGroundTruthImageTarFileIndices = true;
		}
		if (modeString.contains("createOriginalImageTarFileIndices")) {
			createOriginalImageTarFileIndices = true;
		}
		if (modeString.contains("createReducedImages")) {
			createReducedImages = true;
		}
		if (modeString.contains("validateImagesAndFindMarkedRegions")) {
			validateImagesAndFindMarkedRegions = true;
		}
		if (modeString.contains("analyzMarkedRegions")) {
			analyzMarkedRegions = true;
		}
		if (modeString.contains("applyOCR")) {
			applyOCR = true;
		}
		if (modeString.contains("analyzeOCR")) {
			analyzeOCR = true;
		}
		if (modeString.contains("optimizeBias")) {
			optimizeBias = true;
		}
		if (modeString.contains("useOptimizedBiasToClassifyNewExamples")) {
			useOptimizedBiasToClassifyNewExamples = true;
		}

		double parameter1 = Double.parseDouble(args[index++]);
		double parameter2 = Double.parseDouble(args[index++]);
		double parameter3 = Double.parseDouble(args[index++]);
		double parameter4 = Double.parseDouble(args[index++]);

		String annotatedDirectoryTableFilePath = args[index++];
		String groundTruthImageSizesTableFilePath = args[index++];
		String userMarkedRegionTableFilePath = args[index++];
		String machineMarkedRegionTableFilePath = args[index++];
		String groundTruthTarFilePath = args[index++];
		String originalTarFilePath = args[index++];
		String unseenImageTarFilePath = args[index++];
		int numThreads = Integer.parseInt(args[index++]);
		int threadNumber = Integer.parseInt(args[index++]);

		int threadIndex = threadNumber - 1;

		System.out.println("modeString                       = " + modeString);
		System.out.println("parameter1                       = " + parameter1);
		System.out.println("parameter2                       = " + parameter2);
		System.out.println("parameter3                       = " + parameter3);
		System.out.println("annotatedDirectoryTableFilePath  = " + annotatedDirectoryTableFilePath);
		System.out.println("groundTruthTarFilePath           = " + groundTruthTarFilePath);
		System.out.println("userMarkedRegionTableFilePath    = " + userMarkedRegionTableFilePath);
		System.out.println("machineMarkedRegionTableFilePath = " + machineMarkedRegionTableFilePath);
		System.out.println("originalTarFilePath              = " + originalTarFilePath);
		System.out.println("unseenImageTarFilePath           = " + unseenImageTarFilePath);
		System.out.println("numThreads                       = " + numThreads);
		System.out.println("threadNumber                     = " + threadNumber);

		if (createOriginalImageTarFileIndices) {
			createTarFileIndex(originalTarFilePath, annotatedDirectoryTableFilePath, useAnnotatedDirectoryTableFilePath);
		}
		if (createReducedImages)
			createReducedImages(groundTruthTarFilePath, originalTarFilePath, (int) parameter3, numThreads, threadIndex);

		if (createGroundTruthImageTarFileIndices) {
			createTarFileIndex(groundTruthTarFilePath, annotatedDirectoryTableFilePath, true);
		}

		if (validateImagesAndFindMarkedRegions)
			validateImagesAndFindMarkedRegions(groundTruthTarFilePath, groundTruthImageSizesTableFilePath, originalTarFilePath, userMarkedRegionTableFilePath, machineMarkedRegionTableFilePath, (int) parameter3, numThreads, threadIndex);

		if (applyOCR)
			applyOCR(unseenImageTarFilePath, numThreads, threadIndex);

		if (analyzeOCR)
			analyzeOCR();

		if (optimizeBias)
			optimizeOrApplyBias(0, groundTruthImageSizesTableFilePath, userMarkedRegionTableFilePath, groundTruthTarFilePath, originalTarFilePath, unseenImageTarFilePath, parameter1, parameter2, parameter3, parameter4);

		if (useOptimizedBiasToClassifyNewExamples) {

			createTarFileIndex(unseenImageTarFilePath, null, false);
			createReducedImages(groundTruthTarFilePath, unseenImageTarFilePath, (int) parameter3, numThreads, threadIndex);
			createTarFileIndex(unseenImageTarFilePath, null, false);
			optimizeOrApplyBias(1, groundTruthImageSizesTableFilePath, userMarkedRegionTableFilePath, groundTruthTarFilePath, originalTarFilePath, unseenImageTarFilePath, parameter1, parameter2, parameter3, parameter4);

		}
	}

	private static void analyzeOCR() {

		String[] pathStrings = IO.getPathStrings("sample");
		// String[] pathStrings = IO.getPathStrings("marked");

		Arrays.sort(pathStrings);

		System.out.print("index");
		System.out.print("\t");
		System.out.print("fileName");
		System.out.print("\t");
		System.out.print("dualColumn");
		System.out.print("\t");
		System.out.print("width");
		System.out.print("\t");
		System.out.print("height");
		System.out.print("\t");
		System.out.print("largeNegativeXMotionAverage");
		System.out.print("\t");
		System.out.print("largeNegativeXMotionCount");
		System.out.print("\t");
		System.out.print("largeNegativeXMotionCenterDeviationAverage");
		System.out.print("\t");
		System.out.print("largeNegativeXMotionCenterAverage");
		System.out.print("\t");
		System.out.print("largeLeftColumnXAverage");
		System.out.print("\t");
		System.out.print("largeRightColumnXAverage");
		System.out.print("\t");
		System.out.print("largeLeftColumnXCount");
		System.out.print("\t");
		System.out.print("largeRightColumnXCount");
		System.out.print("\t");
		System.out.print("fractionRightColumn");
		System.out.print("\t");
		System.out.print("computeTime");

		double computeTimeSum = 0.0;
		int numDualColumnPages = 0;
		int numFiles = pathStrings.length;
		for (int i = 0; i < pathStrings.length; i++) {

			String pathName = pathStrings[i];

			if (fbisOnly && !pathName.contains("FBIS"))
				continue;

			if (swbOnly && !pathName.contains("SWB"))
				continue;

			Object[] objects = (Object[]) IO.readObject(pathName);

			int index = 0;

			String[] lines = (String[]) objects[index++];
			String[] words = (String[]) objects[index++];

			char[] textChars = (char[]) objects[index++];
			int[] textCharX1 = (int[]) objects[index++];
			int[] textCharY1 = (int[]) objects[index++];
			int[] textCharX2 = (int[]) objects[index++];
			int[] textCharY2 = (int[]) objects[index++];

			int width = (Integer) objects[index++];
			int height = (Integer) objects[index++];
			int[] wordMinX = (int[]) objects[index++];
			int[] wordMaxX = (int[]) objects[index++];
			int[] wordMinY = (int[]) objects[index++];
			int[] wordMaxY = (int[]) objects[index++];
			Double largeNegativeXMotionAverage = (Double) objects[index++];
			Double largeNegativeXMotionCount = (Double) objects[index++];
			Double largeNegativeXMotionCenterDeviationAverage = (Double) objects[index++];
			Double largeNegativeXMotionCenterAverage = (Double) objects[index++];
			Double largeLeftColumnXAverage = (Double) objects[index++];
			Double largeRightColumnXAverage = (Double) objects[index++];
			Double largeLeftColumnXCount = (Double) objects[index++];
			Double largeRightColumnXCount = (Double) objects[index++];
			Double fractionRightColumn = (Double) objects[index++];
			Double computeTime = (Double) objects[index++];

			if (Double.isNaN(largeNegativeXMotionAverage))
				continue;

			int dualColumn = 0;
			if (Math.abs(largeNegativeXMotionAverage - 0.34) < 0.05 && Math.abs(largeNegativeXMotionCenterDeviationAverage - 0.21) < 0.03 && fractionRightColumn > 0.25) {
				dualColumn = 1;
			}

			computeTimeSum += computeTime;
			numDualColumnPages += dualColumn;

			// String fileName = pathName.substring(pathName.lastIndexOf("#") + 1);

			String identifier = pathName.substring((dataDirectoryPath + "exp/sample/").length()).replaceAll("#", "/");

			if (true) {
				System.out.print(i + 1);
				System.out.print("\t");
				System.out.print(identifier);
				System.out.print("\t");
				System.out.print(dualColumn);
				System.out.print("\t");
				System.out.print(width);
				System.out.print("\t");
				System.out.print(height);
				System.out.print("\t");
				System.out.print(largeNegativeXMotionAverage);
				System.out.print("\t");
				System.out.print(largeNegativeXMotionCount);
				System.out.print("\t");
				System.out.print(largeNegativeXMotionCenterDeviationAverage);
				System.out.print("\t");
				System.out.print(largeNegativeXMotionCenterAverage);
				System.out.print("\t");
				System.out.print(largeLeftColumnXAverage);
				System.out.print("\t");
				System.out.print(largeRightColumnXAverage);
				System.out.print("\t");
				System.out.print(largeLeftColumnXCount);
				System.out.print("\t");
				System.out.print(largeRightColumnXCount);
				System.out.print("\t");
				System.out.print(fractionRightColumn);
				System.out.print("\t");
				System.out.print(computeTime);
				System.out.println();
			}

			if (false) {
				int numLines = lines.length;
				for (int j = 0; j < numLines; j++) {
					System.out.println(lines[j]);
				}
			}

		}

		if (true) {

			double averageComputeTime = computeTimeSum / numFiles;
			double fractionDualColumn = (double) numDualColumnPages / numFiles;
			System.out.println();
			System.out.println();
			System.out.println("averageComputeTime = " + averageComputeTime);
			System.out.println("fractionDualColumn = " + fractionDualColumn);
			System.out.println();
		}

	}

	// ----------------------------------------------------

	private static boolean verbose = true;

	/**
	 * Determines if the Exec class should print which commands are being executed, and print error messages if a problem is found. Default is true.
	 * 
	 * @param verboseFlag
	 *            true: print messages. false: don't.
	 */

	public static void setVerbose(boolean verboseFlag) {
		verbose = verboseFlag;
	}

	/** Will Exec print status messages? */

	public static boolean getVerbose() {
		return (verbose);
	}

	// ----------------------------------------------------
	/**
	 * Starts a process to execute the command. Returns immediately, even if the new process is still running.
	 * 
	 * @param command
	 *            The <B>full</B> pathname of the command to be executed. No shell builtins (e.g. "cd") or shell meta-chars (e.g. ">") allowed.
	 * @return false if a problem is known to occur, but since this returns immediately, problems aren't usually found in time. Returns true otherwise.
	 */

	public static boolean exec(String command) {
		return (exec(command, false, false));
	}

	// ----------------------------------------------------
	/**
	 * Starts a process to execute the command. Waits for the process to finish before returning.
	 * 
	 * @param command
	 *            The <B>full</B> pathname of the command to be executed. No shell builtins or shell meta-chars allowed.
	 * @return false if a problem is known to occur, either due to an exception or from the subprocess returning a non-zero value. Returns true otherwise.
	 */

	public static boolean execWait(String command) {
		return (exec(command, false, true));
	}

	// ----------------------------------------------------
	/**
	 * Starts a process to execute the command. Prints all output the command gives.
	 * 
	 * @param command
	 *            The <B>full</B> pathname of the command to be executed. No shell builtins or shell meta-chars allowed.
	 * @return false if a problem is known to occur, either due to an exception or from the subprocess returning a non-zero value. Returns true otherwise.
	 */

	public static boolean execPrint(String command) {
		return (exec(command, true, false));
	}

	// ----------------------------------------------------
	/**
	 * Starts a process to execute the command. Prints all output the command gives.
	 * 
	 * @param command
	 *            The <B>full</B> pathname of the command to be executed. No shell builtins or shell meta-chars allowed.
	 * @return false if a problem is known to occur, either due to an exception or from the subprocess returning a non-zero value. Returns true otherwise.
	 */

	public static boolean execPrintWait(String command) {
		return (exec(command, true, true));
	}

	// ----------------------------------------------------
	// This creates a Process object via
	// Runtime.getRuntime.exec(). Depending on the
	// flags, it may call waitFor on the process
	// to avoid continuing until the process terminates,
	// or open an input stream from the process to read
	// the results.

	static Vector<Process> procs = null;

	private static boolean exec(String command, boolean printResults, boolean wait) {
		if (verbose) {
			System.out.println("Executing '" + command + "'.");
		}
		try {
			// Start running command, returning immediately.
			Process process = Runtime.getRuntime().exec(command);

			procs.add(process);

			// Print the output. Since we read until
			// there is no more input, this causes us
			// to wait until the process is completed
			if (printResults) {
				BufferedInputStream buffer = new BufferedInputStream(process.getInputStream());
				DataInputStream commandResult = new DataInputStream(buffer);
				String s = null;
				try {
					while ((s = commandResult.readLine()) != null)
						System.out.println("Output: " + s);
					commandResult.close();
					if (process.exitValue() != 0) {
						if (verbose)
							printError(command + " -- p.exitValue() != 0");
						return (false);
					}
					// Ignore read errors; they mean process is done
				} catch (Exception e) {
				}

				// If you don't print the results, then you
				// need to call waitFor to stop until the process
				// is completed
			} else if (wait) {
				try {
					System.out.println(" ");
					int returnVal = process.waitFor();
					if (returnVal != 0) {
						if (verbose)
							printError(command);
						return (false);
					}
				} catch (Exception e) {
					if (verbose)
						printError(command, e);
					return (false);
				}
			}
		} catch (Exception e) {
			if (verbose)
				printError(command, e);
			return (false);
		}
		return (true);
	}

	// ----------------------------------------------------

	private static void printError(String command, Exception e) {
		System.out.println("Error doing exec(" + command + "): " + e.getMessage());
		System.out.println("Did you specify the full " + "pathname?");
	}

	private static void printError(String command) {
		System.out.println("Error executing '" + command + "'.");
	}

	// ----------------------------------------------------

	public static void createTarFileIndex(String tarFilePath, String annotatedDirectoryTableFilePath, boolean useImagesInAnnotedDirectoryOnly) {

		SimpleTable annotatedDirectoryTable = null;
		String[] annotatedDirectoryPathSubstrings = null;

		if (useImagesInAnnotedDirectoryOnly) {
			try {
				annotatedDirectoryTable = IO.readDelimitedTable(annotatedDirectoryTableFilePath, "\t", true);
			} catch (Exception e) {
				e.printStackTrace();
			}
			annotatedDirectoryPathSubstrings = annotatedDirectoryTable.getColumnAsStringArray(0);
		}

		HashMap<String, Integer> entryNameToIndex = new HashMap<String, Integer>();
		String[] entryNames = new String[maxNumEntries];
		int[] entryNameIndices = new int[maxNumEntries];
		long[] positions = new long[maxNumEntries];
		long[] lengths = new long[maxNumEntries];

		long tarFileLength = new File(tarFilePath).length();

		byte[] headerData = new byte[TarConstants.HEADER_BLOCK];

		int maxBodyDataSize = (int) 1e9;
		byte[] bodyData = new byte[maxBodyDataSize];

		int count = 0;
		long totalBytesRead = 0;
		long lastTotalBytesRead = 0;
		long startTimeInMS = System.currentTimeMillis();
		long lastBufferTimeInMS = System.currentTimeMillis();
		int lastNumGigaBytesRead = -1;
		int numGigBytesRead = 0;

		try {
			RandomAccessFile raf = new RandomAccessFile(tarFilePath, "r");

			int pageCount = 0;
			int goodKeyCount = 0;
			while (totalBytesRead < tarFileLength) {

				raf.seek(totalBytesRead);
				raf.readFully(headerData);

				TarEntry tarEntry = new TarEntry(headerData);

				String entryName = tarEntry.getName();

				// System.out.println("entryName = " + entryName);

				// String parts[] = entryName.split("/");

				boolean isGoodFile = true;

				if (useImagesInAnnotedDirectoryOnly) {
					boolean isInAnnotatedDirectory = false;
					for (int i = 0; i < annotatedDirectoryPathSubstrings.length; i++) {
						if (entryName.contains(annotatedDirectoryPathSubstrings[i])) {
							isInAnnotatedDirectory = true;
							System.out.println("(isInAnnotatedDirectory == true) i = " + i);
							break;
						}
					}

					if (!isInAnnotatedDirectory) {
						isGoodFile = false;
						System.out.println("(isInAnnotatedDirectory == false) skipping entryName = " + entryName);
					}
				}
				// if (!entryName.contains("DONE")) {
				// isGoodFile = false;
				// }
				if (!(entryName.endsWith(".png"))) {
					isGoodFile = false;
				}
				// if (!(entryName.endsWith(".png") || entryName.endsWith(".ser"))) {
				// isGoodFile = false;
				// }

				if (isGoodFile) {

					Integer entryNameIndex = entryNameToIndex.get(entryName);
					if (entryNameIndex == null) {
						entryNameIndex = entryNameToIndex.size();
						entryNames[entryNameIndex] = entryName;
						entryNameToIndex.put(entryName, entryNameIndex);
					}
					entryNameIndices[pageCount] = entryNameIndex;

					// if (pageCount % 1 == 0) {
					//
					// System.out.println("entryName = " + entryName);
					// System.out.println("entryNameToIndex.size() = " + entryNameToIndex.size());
					//
					// }

					positions[pageCount] = totalBytesRead + headerData.length;

					lengths[pageCount] = tarEntry.getSize();

					pageCount++;

					if (pageCount > maxNumEntriesToProcess) {
						break;
					}

				}

				totalBytesRead += headerData.length;

				long dataSize = tarEntry.getSize();
				// System.out.println("tarFileEntryName = " + tarFileEntryName);
				// System.out.println("dataSize = " + dataSize);
				long tarSize = -1;

				if (dataSize % TarConstants.HEADER_BLOCK == 0) {
					tarSize = dataSize;
				} else {
					tarSize = (dataSize / TarConstants.HEADER_BLOCK + 1) * TarConstants.HEADER_BLOCK;
				}

				if (isGoodFile) {

					if (dataSize <= maxBodyDataSize) {

						raf.readFully(bodyData, 0, (int) dataSize);

						MessageDigest m = MessageDigest.getInstance("MD5");

						m.update(bodyData, 0, (int) dataSize);

						// System.out.println("MD5: " + new BigInteger(1, m.digest()).toString(16));

						// long sum = 0;
						// for (int i = 0; i < dataSize; i++) {
						// sum += (int) bodyData[i] << 8;
						// }

						goodKeyCount++;

						System.out.println("KEY\t" + goodKeyCount + "\t" + dataSize + "\t" + new BigInteger(1, m.digest()).toString(16) + "\t" + entryName);
					}

				}

				totalBytesRead += tarSize;

				numGigBytesRead = (int) (totalBytesRead / 1000000000);

				count++;

				if (numGigBytesRead != lastNumGigaBytesRead) {

					long bytesRead = totalBytesRead - lastTotalBytesRead;
					lastNumGigaBytesRead = numGigBytesRead;
					lastTotalBytesRead = totalBytesRead;

					double durationInSeconds = (System.currentTimeMillis() - startTimeInMS) / 1000.0;
					double bufferDurationInSeconds = (System.currentTimeMillis() - lastBufferTimeInMS) / 1000.0;
					lastBufferTimeInMS = System.currentTimeMillis();

					long bytesLeft = tarFileLength - totalBytesRead;

					double bytesPerSecond = (double) bytesRead / bufferDurationInSeconds;
					double secondsLeft = bytesLeft / bytesPerSecond;

					System.out.println();
					// System.out.println("tarFileEntryName = " + tarFileEntryName);
					System.out.println("timeLeft (s) = " + secondsLeft);
					System.out.println("timeLeft (h) = " + secondsLeft / 3600);
					System.out.println("timeLeft (d) = " + secondsLeft / 3600 / 24);
					System.out.println("ETA = " + new Date((long) (System.currentTimeMillis() + secondsLeft * 1000)));
					System.out.println("durationInSeconds = " + durationInSeconds);
					System.out.println("totalBytesRead (GB) = " + (double) totalBytesRead / 1000000000);
					System.out.println("overall bytesPerSecond (MB) = " + (double) totalBytesRead / durationInSeconds / 1000000);
					System.out.println("buffer  bytesPerSecond (MB) = " + (double) bytesRead / bufferDurationInSeconds / 1000000);
					System.out.println("bytesRead  = " + bytesRead);
					System.out.println("count = " + count);
				}

			}

			raf.close();

			{
				int[] newEntryNameIndices = new int[pageCount];

				System.arraycopy(entryNameIndices, 0, newEntryNameIndices, 0, pageCount);

				entryNameIndices = newEntryNameIndices;
			}
			IO.writeObject(tarFilePath + "." + "EntryNameIndices.ser", entryNameIndices);

			{
				long[] newPositions = new long[pageCount];
				long[] newLengths = new long[pageCount];

				System.arraycopy(positions, 0, newPositions, 0, pageCount);
				System.arraycopy(lengths, 0, newLengths, 0, pageCount);

				positions = newPositions;
				lengths = newLengths;
			}
			IO.writeObject(tarFilePath + "." + "Positions.ser", positions);
			IO.writeObject(tarFilePath + "." + "Lengths.ser", lengths);

			int numEntryNames = entryNameToIndex.size();
			{

				int entryNameSize = 0;
				for (int i = 0; i < numEntryNames; i++) {
					entryNameSize += entryNames[i].length();
				}
				System.out.println("numEntryNames = " + numEntryNames);
				System.out.println("entryNameSize = " + entryNameSize);

				byte[] entryNameMemory = new byte[entryNameSize];
				int entryNameMemoryPtr = 0;
				int[] entryNameMemoryPtrs = new int[numEntryNames];
				int[] entryNameMemorySizes = new int[numEntryNames];
				for (int i = 0; i < numEntryNames; i++) {

					String entryName = entryNames[i];
					byte[] data = entryName.getBytes();
					int size = data.length;

					System.arraycopy(data, 0, entryNameMemory, entryNameMemoryPtr, size);

					entryNameMemoryPtrs[i] = entryNameMemoryPtr;
					entryNameMemorySizes[i] = size;

					entryNameMemoryPtr += data.length;

				}

				IO.writeObject(tarFilePath + "." + "EntryNameMemory.ser", entryNameMemory);
				IO.writeObject(tarFilePath + "." + "EntryNameMemoryPtrs.ser", entryNameMemoryPtrs);
				IO.writeObject(tarFilePath + "." + "EntryNameMemorySizes.ser", entryNameMemorySizes);
			}

		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	static MouseListener mouseListener = new ClineMouseListener();
	static KeyListener keyListener = new ClineKeyListener();

	public static void validateImagesAndFindMarkedRegions(String groundTruthTarFilePath, String groundTruthImageSizesTableFilePath, String originalTarFilePath, String userMarkedRegionTableFilePath, String machineMarkedRegionTableFilePath,
			int reducedGridSize, int numThreads, int threadIndex) {
		try {

			boolean skipImagesWithNoMachinePredictions = true;

			boolean findRegions = false;
			boolean showImages = true;
			int showImagesDwellTime = 0;

			int screenWidth = reducedGridSize * 2;
			int screenHeight = reducedGridSize;
			// int screenWidth = 2048;
			// int screenHeight = 1024;
			// int screenWidth = 2560;
			// int screenHeight = 1600;

			GraphicsEnvironment env = null;
			GraphicsDevice device = null;

			Frame mainFrame = null;

			if (showImages) {

				try {
					env = GraphicsEnvironment.getLocalGraphicsEnvironment();
					device = env.getDefaultScreenDevice();
				} catch (HeadlessException e1) {
					// TODO Auto-generated catch block
					// e1.printStackTrace();
				}

				mainFrame = new Frame();

				mainFrame.setAlwaysOnTop(false);

				mainFrame.setIgnoreRepaint(false);
				mainFrame.setUndecorated(true);

				// device.setDisplayMode(null);

				mainFrame.setSize(screenWidth, screenHeight);

				mainFrame.setLocation(0, 0);

				mainFrame.addMouseListener(mouseListener);
				mainFrame.addKeyListener(keyListener);

				// if (rnr.showWindow)
				mainFrame.setVisible(true);

				System.out.println(mainFrame.getSize().getHeight());
				System.out.println(mainFrame.getSize().getWidth());
			}

			int numImageFilesParsed = 0;

			int chuckSize = (int) 1e6;

			// int[] fileNameIndices = null;
			// long[] positions = null;
			// long[] lengths = null;

			int bufferSize = -1;
			long startTimeInMS = System.currentTimeMillis();
			long lastBufferTimeInMS = System.currentTimeMillis();

			// read ground truth tar file data //

			RandomAccessFile groundTruthTarRandomAccessFile = new RandomAccessFile(groundTruthTarFilePath, "r");

			int groundTruthNumEntries;

			byte[] groundTruthEntryNameMemory = (byte[]) IO.readObject(groundTruthTarFilePath + ".EntryNameMemory.ser");
			int[] groundTruthEntryNameMemoryPtrs = (int[]) IO.readObject(groundTruthTarFilePath + ".EntryNameMemoryPtrs.ser");
			int[] groundTruthEntryNameMemorySizes = (int[]) IO.readObject(groundTruthTarFilePath + ".EntryNameMemorySizes.ser");

			int[] groundTruthEntryNameIndices = (int[]) IO.readObject(groundTruthTarFilePath + ".EntryNameIndices.ser");
			long[] groundTruthPositions = (long[]) IO.readObject(groundTruthTarFilePath + ".Positions.ser");
			long[] groundTruthLengths = (long[]) IO.readObject(groundTruthTarFilePath + ".Lengths.ser");

			groundTruthNumEntries = groundTruthEntryNameIndices.length;

			System.out.println("groundTruthNumEntries = " + groundTruthNumEntries);

			HashMap<String, Integer> groundTruthEntryNameToIndex = new HashMap<String, Integer>();
			HashMap<Integer, String> groundTruthIndexToEntryName = new HashMap<Integer, String>();

			for (int entryIndex = 0; entryIndex < groundTruthNumEntries; entryIndex++) {

				String entryName = new String(groundTruthEntryNameMemory, groundTruthEntryNameMemoryPtrs[groundTruthEntryNameIndices[entryIndex]], groundTruthEntryNameMemorySizes[groundTruthEntryNameIndices[entryIndex]]);
				// System.out.println(entryName);
				groundTruthEntryNameToIndex.put(entryName, entryIndex);
				groundTruthIndexToEntryName.put(entryIndex, entryName);
			}

			RandomAccessFile originalTarRandomAccessFile = new RandomAccessFile(originalTarFilePath, "r");

			int originalNumEntries;

			byte[] originalEntryNameMemory = (byte[]) IO.readObject(originalTarFilePath + ".EntryNameMemory.ser");
			int[] originalEntryNameMemoryPtrs = (int[]) IO.readObject(originalTarFilePath + ".EntryNameMemoryPtrs.ser");
			int[] originalEntryNameMemorySizes = (int[]) IO.readObject(originalTarFilePath + ".EntryNameMemorySizes.ser");

			int[] originalEntryNameIndices = (int[]) IO.readObject(originalTarFilePath + ".EntryNameIndices.ser");
			long[] originalPositions = (long[]) IO.readObject(originalTarFilePath + ".Positions.ser");
			long[] originalLengths = (long[]) IO.readObject(originalTarFilePath + ".Lengths.ser");

			originalNumEntries = originalEntryNameIndices.length;

			System.out.println("originalNumEntries = " + originalNumEntries);

			HashMap<String, Integer> originalEntryNameToIndex = new HashMap<String, Integer>();
			HashMap<Integer, String> originalIndexToEntryName = new HashMap<Integer, String>();

			for (int entryIndex = 0; entryIndex < originalNumEntries; entryIndex++) {

				String entryName = new String(originalEntryNameMemory, originalEntryNameMemoryPtrs[originalEntryNameIndices[entryIndex]], originalEntryNameMemorySizes[originalEntryNameIndices[entryIndex]]);
				// System.out.println(entryName);
				originalEntryNameToIndex.put(entryName, entryIndex);
				originalIndexToEntryName.put(entryIndex, entryName);
			}

			// match up ground truth and original images //

			HashMap<Integer, Integer> groundTruthToOriginalEntryIndex = new HashMap<Integer, Integer>();

			for (int groundTruthEntryIndex = 0; groundTruthEntryIndex < groundTruthNumEntries; groundTruthEntryIndex++) {
				String groundTruthEntryName = groundTruthIndexToEntryName.get(groundTruthEntryIndex);
				String originalEntryName = groundTruthEntryName.replace("marked", "original");
				Integer orginalIndex = originalEntryNameToIndex.get(originalEntryName);

				if (false) {
					System.out.println("groundTruthEntryName = " + groundTruthEntryName);
					System.out.println("originalEntryName = " + originalEntryName);
					System.out.println("orginalIndex = " + orginalIndex);
				}

				if (orginalIndex != null)
					groundTruthToOriginalEntryIndex.put(groundTruthEntryIndex, orginalIndex);

			}
			System.out.println("groundTruthToOriginalEntryIndex.size() = " + groundTruthToOriginalEntryIndex.size());

			System.out.println("numImages = " + groundTruthNumEntries);

			// System.exit(0);

			/****************************/
			/* read user marked regions */
			/*****************************/

			SimpleTable userMarkedRegionTable = null;
			try {
				userMarkedRegionTable = IO.readDelimitedTable(userMarkedRegionTableFilePath, "\t", true);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// int numRegions = table.getNumRows();

			int numUserMarkedRegions = userMarkedRegionTable.getNumRows();

			// int[] exampleGroundTruthTarFileIndices = new int[maxNumExamples];
			int[] imageUserNumRegions = new int[groundTruthNumEntries];
			int[][] imageUserMarkedRegionX1s = new int[groundTruthNumEntries][MAX_NUM_MARKED_REGIONS_PER_IMAGE];
			int[][] imageUserMarkedRegionY1s = new int[groundTruthNumEntries][MAX_NUM_MARKED_REGIONS_PER_IMAGE];
			int[][] imageUserMarkedRegionX2s = new int[groundTruthNumEntries][MAX_NUM_MARKED_REGIONS_PER_IMAGE];
			int[][] imageUserMarkedRegionY2s = new int[groundTruthNumEntries][MAX_NUM_MARKED_REGIONS_PER_IMAGE];
			int[][] imageUserMarkedRegionImageHeights = new int[groundTruthNumEntries][MAX_NUM_MARKED_REGIONS_PER_IMAGE];
			int[][] imageUserMarkedRegionImageWidths = new int[groundTruthNumEntries][MAX_NUM_MARKED_REGIONS_PER_IMAGE];

			{
				for (int i = 0; i < numUserMarkedRegions; i++) {

					int groundTruthTarFileIndex = userMarkedRegionTable.getInt(i, 0);
					int x1 = userMarkedRegionTable.getInt(i, 1);
					int y1 = userMarkedRegionTable.getInt(i, 2);
					int x2 = userMarkedRegionTable.getInt(i, 3);
					int y2 = userMarkedRegionTable.getInt(i, 4);
					int width = userMarkedRegionTable.getInt(i, 5);
					int hieght = userMarkedRegionTable.getInt(i, 6);

					if (false)
						System.out.printf("%d\t%d\t%d\t%d\t%d\n", groundTruthTarFileIndex, x1, y1, x2, y2, hieght, width);

					if (imageUserNumRegions[groundTruthTarFileIndex] < MAX_NUM_MARKED_REGIONS_PER_IMAGE) {

						imageUserMarkedRegionX1s[groundTruthTarFileIndex][imageUserNumRegions[groundTruthTarFileIndex]] = x1;
						imageUserMarkedRegionY1s[groundTruthTarFileIndex][imageUserNumRegions[groundTruthTarFileIndex]] = y1;
						imageUserMarkedRegionX2s[groundTruthTarFileIndex][imageUserNumRegions[groundTruthTarFileIndex]] = x2;
						imageUserMarkedRegionY2s[groundTruthTarFileIndex][imageUserNumRegions[groundTruthTarFileIndex]] = y2;
						imageUserMarkedRegionImageWidths[groundTruthTarFileIndex][imageUserNumRegions[groundTruthTarFileIndex]] = width;
						imageUserMarkedRegionImageHeights[groundTruthTarFileIndex][imageUserNumRegions[groundTruthTarFileIndex]] = hieght;

						imageUserNumRegions[groundTruthTarFileIndex]++;
					}

				}

			}

			/*******************************/
			/* read machine marked regions */
			/*******************************/
			int numMachineMarkedRegions = -1;
			int[] imageMachineNumRegions = null;
			int[][] imageMachineMarkedRegionX1s = null;
			int[][] imageMachineMarkedRegionY1s = null;
			int[][] imageMachineMarkedRegionX2s = null;
			int[][] imageMachineMarkedRegionY2s = null;
			int[][] imageMachineMarkedRegionImageHeights = null;
			int[][] imageMachineMarkedRegionImageWidths = null;
			if (!validateImagesAndFindMarkedRegions) {
				SimpleTable machineMarkedRegionTable = null;
				try {
					machineMarkedRegionTable = IO.readDelimitedTable(machineMarkedRegionTableFilePath, "\t", true);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				// int numRegions = table.getNumRows();

				numMachineMarkedRegions = machineMarkedRegionTable.getNumRows();

				// int[] exampleGroundTruthTarFileIndices = new int[maxNumExamples];
				imageMachineNumRegions = new int[groundTruthNumEntries];
				imageMachineMarkedRegionX1s = new int[groundTruthNumEntries][MAX_NUM_MARKED_REGIONS_PER_IMAGE];
				imageMachineMarkedRegionY1s = new int[groundTruthNumEntries][MAX_NUM_MARKED_REGIONS_PER_IMAGE];
				imageMachineMarkedRegionX2s = new int[groundTruthNumEntries][MAX_NUM_MARKED_REGIONS_PER_IMAGE];
				imageMachineMarkedRegionY2s = new int[groundTruthNumEntries][MAX_NUM_MARKED_REGIONS_PER_IMAGE];
				imageMachineMarkedRegionImageHeights = new int[groundTruthNumEntries][MAX_NUM_MARKED_REGIONS_PER_IMAGE];
				imageMachineMarkedRegionImageWidths = new int[groundTruthNumEntries][MAX_NUM_MARKED_REGIONS_PER_IMAGE];

				{

					for (int i = 0; i < numMachineMarkedRegions; i++) {

						int groundTruthTarFileIndex = machineMarkedRegionTable.getInt(i, 0);
						int x1 = machineMarkedRegionTable.getInt(i, 1);
						int y1 = machineMarkedRegionTable.getInt(i, 2);
						int x2 = machineMarkedRegionTable.getInt(i, 3);
						int y2 = machineMarkedRegionTable.getInt(i, 4);
						int width = machineMarkedRegionTable.getInt(i, 5);
						int hieght = machineMarkedRegionTable.getInt(i, 6);

						if (false)
							System.out.printf("%d\t%d\t%d\t%d\t%d\n", groundTruthTarFileIndex, x1, y1, x2, y2, hieght, width);

						if (imageMachineNumRegions[groundTruthTarFileIndex] < MAX_NUM_MARKED_REGIONS_PER_IMAGE) {

							imageMachineMarkedRegionX1s[groundTruthTarFileIndex][imageMachineNumRegions[groundTruthTarFileIndex]] = x1;
							imageMachineMarkedRegionY1s[groundTruthTarFileIndex][imageMachineNumRegions[groundTruthTarFileIndex]] = y1;
							imageMachineMarkedRegionX2s[groundTruthTarFileIndex][imageMachineNumRegions[groundTruthTarFileIndex]] = x2;
							imageMachineMarkedRegionY2s[groundTruthTarFileIndex][imageMachineNumRegions[groundTruthTarFileIndex]] = y2;
							imageMachineMarkedRegionImageWidths[groundTruthTarFileIndex][imageMachineNumRegions[groundTruthTarFileIndex]] = width;
							imageMachineMarkedRegionImageHeights[groundTruthTarFileIndex][imageMachineNumRegions[groundTruthTarFileIndex]] = hieght;

							imageMachineNumRegions[groundTruthTarFileIndex]++;
						}

					}

				}
			}

			byte[] groundTruthPngData = new byte[maxPngDataSize];
			byte[] originalPngData = new byte[maxPngDataSize];
			long totalBytesRead = 0;

			int[] randomIndices = null;

			if (!sequential) {
				if (trueRandom) {
					randomIndices = Utility.randomIntArray((int) System.currentTimeMillis(), groundTruthNumEntries);
				} else {
					randomIndices = Utility.randomIntArray(123, groundTruthNumEntries);
				}
			}

			long overallNumPages = 0;
			// CountAndString[] countAndStrings = new CountAndString[maxNumWords];

			int numDirectoryFiles = 0;

			for (int i = 0; i < groundTruthNumEntries; i++) {

				if (i % numThreads != threadIndex) {
					continue;
				}

				int groundTruthIndex = -1;

				if (sequential) {
					groundTruthIndex = i;
				} else {
					groundTruthIndex = randomIndices[i];
				}

				if (validateSkipImagesWithoutTags && imageMachineNumRegions[groundTruthIndex] == 0) {
					continue;
				}

				if (useOnlyMarkedImagesForTrainAndTest && imageUserNumRegions[groundTruthIndex] == 0) {
					continue;
				}

				Integer originalIndex = groundTruthToOriginalEntryIndex.get(groundTruthIndex);

				if (originalIndex == null) {
					System.out.println("Warning!  originalIndex for groundTruthIndex = " + groundTruthIndex + " not found");
					continue;
				}

				// int randomIndex = i;

				// String groundTruthEntryName = new String(groundTruthEntryNameMemory, groundTruthEntryNameMemoryPtrs[groundTruthEntryNameIndices[groundTruthIndex]],
				// groundTruthEntryNameMemorySizes[groundTruthEntryNameIndices[groundTruthIndex]]);

				String groundTruthEntryName = groundTruthIndexToEntryName.get(groundTruthIndex);
				String originalEntryName = originalIndexToEntryName.get(originalIndex);

				if (fbisOnly && !originalEntryName.contains("FBIS"))
					continue;

				if (swbOnly && !originalEntryName.contains("SWB"))
					continue;

				if (true) {
					System.out.println("groundTruthEntryName = " + groundTruthEntryName);
					System.out.println("originalEntryName = " + originalEntryName);
				}

				{

					// focus on dual columns for now !!!
					boolean dualColumn = false;
					for (int j = 0; j < imageUserNumRegions[groundTruthIndex]; j++) {

						int x1 = imageUserMarkedRegionX1s[groundTruthIndex][j];
						int x2 = imageUserMarkedRegionX2s[groundTruthIndex][j];
						int width = imageUserMarkedRegionImageWidths[groundTruthIndex][0];

						if (x1 == x2)
							continue;

						if (x1 > width * 0.25) {
							dualColumn = true;
							break;
						}
						if (x2 < width * 0.75) {
							dualColumn = true;
							break;
						}

					}

					if (!dualColumn) {
						System.out.println("##############################skipping single column page############################");
						continue;
					}

				}

				groundTruthTarRandomAccessFile.seek(groundTruthPositions[groundTruthIndex]);
				originalTarRandomAccessFile.seek(originalPositions[originalIndex]);

				int groundTruthDataSize = (int) groundTruthLengths[groundTruthIndex];
				int originalDataSize = (int) originalLengths[originalIndex];

				groundTruthTarRandomAccessFile.readFully(groundTruthPngData, 0, groundTruthDataSize);
				originalTarRandomAccessFile.readFully(originalPngData, 0, originalDataSize);

				// System.out.println("pageName = " + pageName);
				System.out.println("groundTruthDataSize = " + groundTruthDataSize);
				System.out.println("originalDataSize = " + originalDataSize);
				if (groundTruthDataSize == 0) {
					numDirectoryFiles++;
					continue;
				}

				try {
					// boolean success = tiffReader.parseTiffFileData(data, dataSize);

					// InputStream inputStream = null;
					BufferedImage groundTruthBufferedImage = ImageIO.read(new ByteArrayInputStream(groundTruthPngData, 0, groundTruthDataSize));

					BufferedImage originalBufferedImage = ImageIO.read(new ByteArrayInputStream(originalPngData, 0, originalDataSize));
					// BufferedImage originalBufferedImage = null;

					// System.out.println("groundTruthBufferedImage() = " + groundTruthBufferedImage.getHeight());
					// System.out.println("groundTruthBufferedImage() = " + groundTruthBufferedImage.getWidth());
					//
					// System.out.println("originalBufferedImage() = " + originalBufferedImage.getHeight());
					// System.out.println("originalBufferedImage() = " + originalBufferedImage.getWidth());

					int type = groundTruthBufferedImage.getType();

					String typeString = "unknown";
					typeString = getTypeString(type, typeString);

					int groundTruthImageWidth = groundTruthBufferedImage.getWidth();
					int groundTruthImageHeight = groundTruthBufferedImage.getHeight();

					if (true) {
						int originalImageWidth = originalBufferedImage.getWidth();
						int originalImageHeight = originalBufferedImage.getHeight();

						if (groundTruthImageWidth != originalImageWidth || groundTruthImageHeight != originalImageHeight) {
							System.out.println("Error!  (groundTruthImageWidth != originalImageWidth || groundTruthImageHeight != originalImageHeight)");
							System.exit(1);
						}
					}

					int imageWidth = groundTruthBufferedImage.getWidth();
					int imageHeight = groundTruthBufferedImage.getHeight();

					System.out.println("imageWidth = " + imageWidth);
					System.out.println("imageHeight = " + imageHeight);

					if (imageWidth >= 20000 || imageHeight >= 20000) {
						System.out.println("Error!  Invalid image size for: " + originalEntryName);
						System.out.println("Error!  imageWidth = " + imageWidth);
						System.out.println("Error!  imageHeight = " + imageHeight);
						continue;
					}

					//

					System.out.println(" bufferedImage.getType() = " + typeString);

					// double imageHeightScaleDownFactor = (double) screenHeight / (double) imageHeight;
					// int newScreenWidth = (int) (imageHeightScaleDownFactor * imageWidth);

					if (showImages) {

						mainFrame.getGraphics().clearRect(0, 0, screenWidth, screenHeight);

						mainFrame.getGraphics().drawImage(originalBufferedImage, 0, 0, screenWidth / 2, screenHeight, 0, 0, imageWidth, imageHeight, null);

						if (true) {

							Color lastColor = mainFrame.getGraphics().getColor();

							mainFrame.getGraphics().setColor(Color.GREEN);
							/* draw marked regions on image */

							int imageUserWidth = imageUserMarkedRegionImageWidths[groundTruthIndex][0];
							int imageUserHeight = imageUserMarkedRegionImageHeights[groundTruthIndex][0];

							for (int j = 0; j < imageUserNumRegions[groundTruthIndex]; j++) {

								int x1 = reducedGridSize * imageUserMarkedRegionX1s[groundTruthIndex][j] / imageUserWidth;
								int y1 = reducedGridSize * imageUserMarkedRegionY1s[groundTruthIndex][j] / imageUserHeight;
								int x2 = reducedGridSize * imageUserMarkedRegionX2s[groundTruthIndex][j] / imageUserWidth;
								int y2 = reducedGridSize * imageUserMarkedRegionY2s[groundTruthIndex][j] / imageUserHeight;

								int width = x2 - x1;
								int height = y2 - y1;

								mainFrame.getGraphics().drawRect(x1, y1, width, height);

							}

							mainFrame.getGraphics().setColor(lastColor);
						}

						mainFrame.getGraphics().drawImage(groundTruthBufferedImage, screenWidth / 2, 0, screenWidth, screenHeight, 0, 0, imageWidth, imageHeight, null);

						Thread.sleep(showImagesDwellTime * 1000);
					}

					// look for red box //

					System.out.println("imageWidth = " + imageWidth);
					System.out.println("imageHeight = " + imageHeight);
					System.out.println("IMAGE_SIZE_DATA\t" + groundTruthIndex + "\t" + imageWidth + "\t" + imageHeight);

					if (findRegions) {
						int startX = -1;
						int startY = -1;

						int endX = -1;
						int endY = -1;

						for (int y = 0; y < imageHeight; y++) {

							for (int x = 0; x < imageWidth; x++) {

								int rgb = groundTruthBufferedImage.getRGB(x, y);

								int redPixelValue = -65536;
								if (rgb == redPixelValue) {

									System.out.println("red start position = (" + x + ", " + y + ")");

									startX = x;
									startY = y;

									while (true) {
										if (groundTruthBufferedImage.getRGB(x, y + 1) != redPixelValue) {
											break;
										}
										y++;
										if (y == imageHeight - 1)
											break;
									}
									while (true) {
										if (groundTruthBufferedImage.getRGB(x + 1, y) != redPixelValue) {
											break;
										}
										x++;
										if (x == imageWidth - 1)
											break;
									}

									System.out.println("red end position = (" + x + ", " + y + ")");

									endX = x;
									endY = y;

									System.out.printf("GROUND_TRUTH_DATA\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n", groundTruthIndex, startX, startY, endX, endY, imageWidth, imageHeight);

								}

							}
						}
					}

					Thread.sleep(1000 * 2);
					numImageFilesParsed++;

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				// in.get(headerData);

				overallNumPages++;

				totalBytesRead += groundTruthLengths[groundTruthIndex];

				double duration = (System.currentTimeMillis() - startTimeInMS) / 1000.0;

				int numDone = i;

				int numLeft = groundTruthNumEntries - i;

				double rate = numDone / duration;

				System.out.println("rate  = " + rate);

				double secondsLeft = numLeft / rate;

				System.out.println("secondsLeft  = " + secondsLeft);
				System.out.println("eta  = " + new Date(System.currentTimeMillis() + (int) secondsLeft * 1000));

			}

			groundTruthTarRandomAccessFile.close();
			originalTarRandomAccessFile.close();

			System.out.println("numDirectoryFiles  = " + numDirectoryFiles);
			System.out.println("numTiffFilesParsed  = " + numImageFilesParsed);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void createReducedImages(String groundTruthTarFilePath, String originalTarFilePath, int reducedGridSize, int numThreads, int threadIndex) {

		long startTimeInMS = System.currentTimeMillis();

		RandomAccessFile originalTarRandomAccessFile = null;
		try {
			originalTarRandomAccessFile = new RandomAccessFile(originalTarFilePath, "r");
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		int groundTruthNumEntries;

		// byte[] groundTruthEntryNameMemory = (byte[]) IO.readObject(groundTruthTarFilePath + ".EntryNameMemory.ser");
		// int[] groundTruthEntryNameMemoryPtrs = (int[]) IO.readObject(groundTruthTarFilePath + ".EntryNameMemoryPtrs.ser");
		// int[] groundTruthEntryNameMemorySizes = (int[]) IO.readObject(groundTruthTarFilePath + ".EntryNameMemorySizes.ser");
		//
		// int[] groundTruthEntryNameIndices = (int[]) IO.readObject(groundTruthTarFilePath + ".EntryNameIndices.ser");
		// long[] groundTruthPositions = (long[]) IO.readObject(groundTruthTarFilePath + ".Positions.ser");
		// long[] groundTruthLengths = (long[]) IO.readObject(groundTruthTarFilePath + ".Lengths.ser");
		//
		// groundTruthNumEntries = groundTruthEntryNameIndices.length;
		//
		// System.out.println("groundTruthNumEntries = " + groundTruthNumEntries);
		//
		// HashMap<String, Integer> groundTruthEntryNameToIndex = new HashMap<String, Integer>();
		// HashMap<Integer, String> groundTruthIndexToEntryName = new HashMap<Integer, String>();
		//
		// for (int entryIndex = 0; entryIndex < groundTruthNumEntries; entryIndex++) {
		//
		// String entryName = new String(groundTruthEntryNameMemory, groundTruthEntryNameMemoryPtrs[groundTruthEntryNameIndices[entryIndex]],
		// groundTruthEntryNameMemorySizes[groundTruthEntryNameIndices[entryIndex]]);
		// // System.out.println(entryName);
		// groundTruthEntryNameToIndex.put(entryName, entryIndex);
		// groundTruthIndexToEntryName.put(entryIndex, entryName);
		// }

		int originalNumEntries;

		byte[] originalEntryNameMemory = (byte[]) IO.readObject(originalTarFilePath + ".EntryNameMemory.ser");
		int[] originalEntryNameMemoryPtrs = (int[]) IO.readObject(originalTarFilePath + ".EntryNameMemoryPtrs.ser");
		int[] originalEntryNameMemorySizes = (int[]) IO.readObject(originalTarFilePath + ".EntryNameMemorySizes.ser");

		int[] originalEntryNameIndices = (int[]) IO.readObject(originalTarFilePath + ".EntryNameIndices.ser");
		long[] originalPositions = (long[]) IO.readObject(originalTarFilePath + ".Positions.ser");
		long[] originalLengths = (long[]) IO.readObject(originalTarFilePath + ".Lengths.ser");

		originalNumEntries = originalEntryNameIndices.length;

		System.out.println("originalNumEntries = " + originalNumEntries);

		HashMap<String, Integer> originalEntryNameToIndex = new HashMap<String, Integer>();
		HashMap<Integer, String> originalIndexToEntryName = new HashMap<Integer, String>();

		for (int entryIndex = 0; entryIndex < originalNumEntries; entryIndex++) {

			String entryName = new String(originalEntryNameMemory, originalEntryNameMemoryPtrs[originalEntryNameIndices[entryIndex]], originalEntryNameMemorySizes[originalEntryNameIndices[entryIndex]]);
			System.out.println(entryName);
			originalEntryNameToIndex.put(entryName, entryIndex);
			originalIndexToEntryName.put(entryIndex, entryName);
		}

		// match up ground truth and original images //

		// HashMap<Integer, Integer> groundTruthToOriginalEntryIndex = new HashMap<Integer, Integer>();
		//
		// HashMap<Integer, Integer> originalToGroundTruthEntryIndex = new HashMap<Integer, Integer>();
		//
		// for (int groundTruthEntryIndex = 0; groundTruthEntryIndex < groundTruthNumEntries; groundTruthEntryIndex++) {
		//
		// String groundTruthEntryName = groundTruthIndexToEntryName.get(groundTruthEntryIndex);
		// String originalEntryName = groundTruthEntryName.replace("marked", "original");
		// Integer orginalIndex = originalEntryNameToIndex.get(originalEntryName);
		//
		// // System.out.println("groundTruthEntryName = " + groundTruthEntryName);
		// // System.out.println("originalEntryName = " + originalEntryName);
		// // System.out.println("orginalIndex = " + orginalIndex);
		//
		// if (orginalIndex != null) {
		// groundTruthToOriginalEntryIndex.put(groundTruthEntryIndex, orginalIndex);
		// originalToGroundTruthEntryIndex.put(orginalIndex, groundTruthEntryIndex);
		// } else {
		// System.out.println("Failure! (orginalIndex == null)");
		// System.exit(1);
		// }
		//
		// }
		// System.out.println("groundTruthToOriginalEntryIndex.size() = " + groundTruthToOriginalEntryIndex.size());

		byte[] originalPngData = new byte[maxPngDataSize];

		int[] randomIndices = Utility.randomIntArray(123, originalNumEntries);
		// CountAndString[] countAndStrings = new CountAndString[maxNumWords];

		int numDirectoryFiles = 0;

		for (int index = 0; index < originalNumEntries; index++) {

			int randomIndex = randomIndices[index];

			if (randomIndex % numThreads != threadIndex) {
				continue;
			}

			String originalEntryName = originalIndexToEntryName.get(randomIndex);

			System.out.println("originalEntryName = " + originalEntryName);

			if (!originalEntryName.contains(".png"))
				continue;

			// int originalTarFileEntryIndex = originalToGroundTruthEntryIndex.get(i);

			try {
				originalTarRandomAccessFile.seek(originalPositions[randomIndex]);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			int originalDataSize = (int) originalLengths[randomIndex];

			try {
				originalTarRandomAccessFile.readFully(originalPngData, 0, originalDataSize);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			// display tiff image //
			System.out.println("originalDataSize = " + originalDataSize);
			if (originalDataSize == 0) {
				numDirectoryFiles++;
				continue;
			}

			try {
				// boolean success = tiffReader.parseTiffFileData(data, dataSize);

				// InputStream inputStream = null;
				BufferedImage originalBufferedImage = ImageIO.read(new ByteArrayInputStream(originalPngData, 0, originalDataSize));

				int originalImageWidth = originalBufferedImage.getWidth();
				int originalImageHeight = originalBufferedImage.getHeight();

				System.out.println("originalImageWidth  = " + originalImageWidth);
				System.out.println("originalImageHeight = " + originalImageHeight);

				int reducedImageWidth = reducedGridSize;
				int reducedImageHeight = reducedGridSize;

				double xReductionFactor = (double) originalImageWidth / reducedImageWidth;
				double yReductionFactor = (double) originalImageHeight / reducedImageHeight;

				System.out.println("xReductionFactor = " + xReductionFactor);
				System.out.println("yReductionFactor = " + yReductionFactor);

				// int reducedImageWidth = originalImageWidth / reductionFactor;
				// int reducedImageHeight = originalImageHeight / reductionFactor;
				//
				// System.out.println("reducedImageWidth = " + reducedImageWidth);
				// System.out.println("reducedImageHeight = " + reducedImageHeight);

				int numValues = reducedImageWidth * reducedImageHeight;

				float[] imageValues = new float[numValues];
				int[] imageValueCounts = new int[numValues];

				for (int y = 0; y < originalImageHeight; y++) {

					for (int x = 0; x < originalImageWidth; x++) {

						int rgb = originalBufferedImage.getRGB(x, y);

						int value;
						if (rgb == -1)
							value = 0;
						else
							value = 1;

						if (rgb != -1 && rgb != -16777216)
							System.out.println("rgb = " + rgb);

						int reducedX = (int) (x / xReductionFactor);
						int reducedY = (int) (y / yReductionFactor);

						try {
							imageValues[reducedY * reducedImageWidth + reducedX] += value;
							imageValueCounts[reducedY * reducedImageWidth + reducedX]++;
						} catch (Exception e) {
							System.out.println("reducedX = " + reducedX);
							System.out.println("reducedY = " + reducedY);
							System.exit(1);
						}

					}

				}

				for (int y = 0; y < reducedImageHeight; y++) {
					for (int x = 0; x < reducedImageWidth; x++) {

						if (imageValueCounts[y * reducedImageWidth + x] == 0) {

							System.out.println("x = " + x);
							System.out.println("y = " + y);
							(new Exception()).printStackTrace();
							System.exit(1);

						}

						imageValues[y * reducedImageWidth + x] /= imageValueCounts[y * reducedImageWidth + x];
						// System.out.println("imageValues[y * reducedImageWidth + x] = " + imageValues[y * reducedImageWidth + x]);
					}
				}

				System.out.print("DATA");
				System.out.print("\t");
				System.out.print(index);
				System.out.print("\t");
				System.out.print(randomIndex);
				System.out.print("\t");
				System.out.print(originalEntryName);

				for (int y = 0; y < reducedGridSize; y++) {
					for (int x = 0; x < reducedGridSize; x++) {
						System.out.print("\t");
						float value = imageValues[y * reducedImageWidth + x];
						System.out.print(String.format("%7.5f", value));
					}
				}
				System.out.println();

				// String filePath = "/data/cline/" + originalEntryName.replace(".png", ".ser");
				String filePath = originalEntryName.replace(".png", ".rgs" + reducedGridSize + ".ser");

				IO.writeObject(filePath, imageValues);

				double duration = (System.currentTimeMillis() - startTimeInMS) / 1000.0;

				int numDone = index;

				int numLeft = originalNumEntries - index;

				double rate = numDone / duration;

				System.out.println("rate  = " + rate);

				double secondsLeft = numLeft / rate;

				System.out.println("secondsLeft  = " + secondsLeft);
				System.out.println("eta  = " + new Date(System.currentTimeMillis() + (int) secondsLeft * 1000));

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private static String getTypeString(int type, String typeString) {
		switch (type) {
		case BufferedImage.TYPE_3BYTE_BGR:
			typeString = "TYPE_3BYTE_BGR";
			break;
		case BufferedImage.TYPE_4BYTE_ABGR:
			typeString = "TYPE_4BYTE_ABGR";
			break;
		case BufferedImage.TYPE_4BYTE_ABGR_PRE:
			typeString = "TYPE_4BYTE_ABGR_PRE";
			break;
		case BufferedImage.TYPE_BYTE_BINARY:
			typeString = "TYPE_BYTE_BINARY";
			break;
		case BufferedImage.TYPE_BYTE_GRAY:
			typeString = "TYPE_BYTE_GRAY";
			break;
		case BufferedImage.TYPE_BYTE_INDEXED:
			typeString = "TYPE_BYTE_INDEXED";
			break;
		case BufferedImage.TYPE_CUSTOM:
			typeString = "TYPE_CUSTOM";
			break;
		case BufferedImage.TYPE_INT_ARGB:
			typeString = "TYPE_INT_ARGB";
			break;
		case BufferedImage.TYPE_INT_ARGB_PRE:
			typeString = "TYPE_INT_ARGB_PRE";
			break;
		case BufferedImage.TYPE_INT_BGR:
			typeString = "TYPE_INT_BGR";
			break;
		case BufferedImage.TYPE_INT_RGB:
			typeString = "TYPE_INT_RGB";
			break;
		case BufferedImage.TYPE_USHORT_555_RGB:
			typeString = "TYPE_USHORT_555_RGB";
			break;
		case BufferedImage.TYPE_USHORT_565_RGB:
			typeString = "TYPE_USHORT_565_RGB";
		case BufferedImage.TYPE_USHORT_GRAY:
			typeString = "TYPE_USHORT_GRAY";
			break;
		}
		return typeString;
	}

	private static void applyOCR(String tarFilePath, int numThreads, int threadIndex) throws Exception {

		HashSet<String> badPngFiles = new HashSet<String>();
		badPngFiles.add("sample/SWB#FILM#R483#Scan_1389.png");

		/***********************************/
		/* read unseen image tar file data */
		/***********************************/

		byte[] entryNameMemory = null;
		int[] entryNameMemoryPtrs = null;
		int[] entryNameMemorySizes = null;
		int[] entryNameIndices = null;
		long[] positions = null;
		long[] lengths = null;

		HashMap<String, Integer> entryNameToIndex = null;
		HashMap<Integer, String> indexToEntryName = null;

		int numEntries = -1;

		//
		//
		//

		RandomAccessFile tarRandomAccessFile = null;

		byte[] pngData = new byte[maxPngDataSize];

		try {
			tarRandomAccessFile = new RandomAccessFile(tarFilePath, "r");
		} catch (FileNotFoundException e) {
			throw e;
		}

		entryNameMemory = (byte[]) IO.readObject(tarFilePath + ".EntryNameMemory.ser");
		entryNameMemoryPtrs = (int[]) IO.readObject(tarFilePath + ".EntryNameMemoryPtrs.ser");
		entryNameMemorySizes = (int[]) IO.readObject(tarFilePath + ".EntryNameMemorySizes.ser");
		entryNameIndices = (int[]) IO.readObject(tarFilePath + ".EntryNameIndices.ser");
		positions = (long[]) IO.readObject(tarFilePath + ".Positions.ser");
		lengths = (long[]) IO.readObject(tarFilePath + ".Lengths.ser");

		//
		//
		//

		int[] entryIndicesToClassify = null;
		int numImagesToClassify = 0;
		if (!useOCRFileCache) {

			numEntries = entryNameIndices.length;

			entryIndicesToClassify = new int[numEntries];

			System.out.println("numEntries = " + numEntries);

			entryNameToIndex = new HashMap<String, Integer>();
			indexToEntryName = new HashMap<Integer, String>();

			for (int entryIndex = 0; entryIndex < numEntries; entryIndex++) {

				String entryName = new String(entryNameMemory, entryNameMemoryPtrs[entryNameIndices[entryIndex]], entryNameMemorySizes[entryNameIndices[entryIndex]]);

				entryNameToIndex.put(entryName, entryIndex);
				indexToEntryName.put(entryIndex, entryName);

				// System.out.println(entryIndex + "\t" + entryName);

				String filePathRoot = entryName.substring(0, entryName.indexOf("."));
				String ocrCachFilePath = filePathRoot + ".ocr";

				if (entryName.contains(".png") && IO.fileDoesNotExists(ocrCachFilePath)) {

					if (true)
						System.out.println(entryName);

					entryIndicesToClassify[numImagesToClassify++] = entryIndex;

				}

			}
			IO.writeObject("ocrEntryIndicesToClassify.ser", new Object[] { entryIndicesToClassify, numImagesToClassify });
		} else {

			Object[] objects = (Object[]) IO.readObject("ocrEntryIndicesToClassify.ser");
			entryIndicesToClassify = (int[]) objects[0];
			numImagesToClassify = (Integer) objects[1];

		}

		System.out.println("numUnseenImagesToClassify = " + numImagesToClassify);

		entryIndicesToClassify = Utility.randomizedIntArray(unseenFileOrderRandom, entryIndicesToClassify, numImagesToClassify);

		for (int imageIndex = 0; imageIndex < numImagesToClassify; imageIndex++) {

			if (imageIndex % numThreads != threadIndex) {
				continue;
			}

			int tarFileEntryIndex = entryIndicesToClassify[imageIndex];

			String pngFileName = new String(entryNameMemory, entryNameMemoryPtrs[entryNameIndices[tarFileEntryIndex]], entryNameMemorySizes[entryNameIndices[tarFileEntryIndex]]);

			String filePathRoot = pngFileName.substring(0, pngFileName.indexOf("."));
			String ocrCachFilePath = filePathRoot + ".ocr";

			if (IO.fileExists(ocrCachFilePath)) {
				continue;
			}

			if (badPngFiles.contains(pngFileName))
				continue;

			System.out.println("working imageIndex:  " + imageIndex);
			System.out.println("working pngFileName: " + pngFileName);

			try {
				tarRandomAccessFile.seek(positions[tarFileEntryIndex]);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				System.exit(1);
			}

			int pngDataSize = (int) lengths[tarFileEntryIndex];

			try {
				tarRandomAccessFile.readFully(pngData, 0, pngDataSize);
			} catch (IOException e1) {
				e1.printStackTrace();
				continue;
			}

			System.out.println("pngDataSize = " + pngDataSize);

			//

			BufferedImage bufferedImage = null;
			try {
				bufferedImage = ImageIO.read(new ByteArrayInputStream(pngData, 0, pngDataSize));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				continue;
			}

			if (bufferedImage == null) { // !!!
				continue;
			}

			int imageWidth = bufferedImage.getWidth();
			int imageHeight = bufferedImage.getHeight();

			System.out.println("imageWidth = " + imageWidth);
			System.out.println("imageHeight = " + imageHeight);

			if (imageWidth >= 20000 || imageHeight >= 20000) {
				System.out.println("Error!  Invalid image size for: " + pngFileName);
				System.out.println("Error!  imageWidth = " + imageWidth);
				System.out.println("Error!  imageHeight = " + imageHeight);
				continue;
			}

			//

			//

			//

			// OCR and print text //

			boolean pageIsTwoColumn = false;
			int numWords = 0;
			int maxNumWords;
			String[] wordStrings = null;
			int[] wordMinX = null;
			int[] wordMaxX = null;
			int[] wordMinY = null;
			int[] wordMaxY = null;

			double fractionPageWidthConsideredLargeMotion = 0.2;

			double largeLeftColumnXSum = 0;
			double largeLeftColumnXCount = 0;
			double largeLeftColumnXAverage = Double.NaN;

			double largeRightColumnXSum = 0;
			double largeRightColumnXCount = 0;
			double largeRightColumnXAverage = Double.NaN;

			double largeNegativeXMotionSum = 0;
			double largeNegativeXMotionCount = 0;
			double largeNegativeXMotionAverage = Double.NaN;

			double largeNegativeXMotionCenterSum = 0;
			double largeNegativeXMotionCenterCount = 0;
			double largeNegativeXMotionCenterAverage = Double.NaN;

			double largeNegativeXMotionCenterDeviationSum = 0;
			double largeNegativeXMotionCenterDeviationCount = 0;
			double largeNegativeXMotionCenterDeviationAverage = Double.NaN;
			double fractionRightColumn = Double.NaN;

			double computeTime = Double.NaN;

			{

				long ocrStartTime = System.currentTimeMillis();

				numWords = 0;
				maxNumWords = 100000;
				wordStrings = new String[maxNumWords];
				wordMinX = new int[maxNumWords];
				wordMaxX = new int[maxNumWords];
				wordMinY = new int[maxNumWords];
				wordMaxY = new int[maxNumWords];

				String s = null;

				// run the Unix "ps -ef" command
				// using the Runtime exec method:

				String imagePath = dataDirectoryPath + pngFileName;
				String textPath = dataDirectoryPath + "tesseract_text_output_thread_" + threadIndex;

				Runtime runtime = Runtime.getRuntime();
				String[] envp = new String[] { "LD_LIBRARY_PATH=/lib:/home/dtcheng/cuda:/usr/local/cuda/lib64:/home/dtcheng/aparapi:/home/dtcheng/tesseract-3.01/api/.libs:/usr/local/lib/" };
				// Process p = Runtime.getRuntime().exec("/usr/local/bin/tesseract " + imagePath + " " + textPath, envp);
				Process p = Runtime.getRuntime().exec("/home/dtcheng/tesseract-3.01/api/tesseract " + imagePath + " " + textPath, envp);

				BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));

				BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));

				// read the output from the command
				System.out.println("Here is the standard output of the command:\n");
				int lastY = Integer.MAX_VALUE;
				while ((s = stdInput.readLine()) != null) {

					String[] parts = s.split("\t");

					if (parts[0].equals("ARLO_TEXT") && parts.length == 7) {

						// System.out.println(s);

						wordMinX[numWords] = Integer.parseInt(parts[2]);
						wordMaxX[numWords] = Integer.parseInt(parts[3]);
						wordMaxY[numWords] = Integer.parseInt(parts[4]);
						wordMinY[numWords] = Integer.parseInt(parts[5]);
						wordStrings[numWords] = parts[6];

						if (false) {
							System.out.println("wordMinX[numWords] = " + wordMinX[numWords]);
							System.out.println("wordMaxX[numWords] = " + wordMaxX[numWords]);
							System.out.println("wordMinY[numWords] = " + wordMinY[numWords]);
							System.out.println("wordMaxY[numWords] = " + wordMaxY[numWords]);
							System.out.println("wordStrings[numWords] = '" + wordStrings[numWords] + "'");
						}

						if (wordMinX[numWords] < 0)
							continue;
						if (wordMaxX[numWords] < 0)
							continue;
						if (wordMinY[numWords] < 0)
							continue;
						if (wordMaxY[numWords] < 0)
							continue;

						if (numWords > 0 && (wordMaxY[numWords] > lastY) && (Math.abs(wordMaxY[numWords] - lastY) > 1000)) {
							pageIsTwoColumn = true;
							// System.out.println("##########################   TWO COLUMN PAGE DETECTED   ####################3");
							// Thread.sleep(1000 * 1000);
						}

						lastY = wordMaxY[numWords];
						numWords++;

					}
				}

				// read any errors from the attempted command
				System.out.println("Here is the standard error of the command (if any):\n");
				while ((s = stdError.readLine()) != null) {
					System.out.println(s);
				}

				SimpleTable charTable = IO.readDelimitedTable(textPath + ".box", " ", false);

				int boxStartRow = -1;
				int boxEndRow = -1;
				int linesStartRow = -1;
				int linesEndRow = -1;
				for (int ii = 0; ii < charTable.numDataRows; ii++) {
					if (charTable.getString(ii, 0).equals("BOX_DATA_BEGIN_MARKER")) {
						boxStartRow = ii + 1;
					}
					if (charTable.getString(ii, 0).equals("BOX_DATA_END_MARKER")) {
						boxEndRow = ii;
					}
					if (charTable.getString(ii, 0).equals("LINE_DATA_BEGIN_MARKER")) {
						linesStartRow = ii + 1;
					}
					if (charTable.getString(ii, 0).equals("LINE_DATA_END_MARKER")) {
						linesEndRow = ii;
					}
				}
				if (boxStartRow == -1 || boxEndRow == -1 || linesStartRow == -1 || linesEndRow == -1) {
					System.out.println("Error! (boxStartRow == -1 || boxEndRow == -1 || linesStartRow == -1 ||linesEndRow == -1)");
					continue;
				}

				int numChars = boxEndRow - boxStartRow;

				char[] textChars = new char[charTable.numDataRows];
				int[] textCharX1 = new int[charTable.numDataRows];
				int[] textCharY1 = new int[charTable.numDataRows];
				int[] textCharX2 = new int[charTable.numDataRows];
				int[] textCharY2 = new int[charTable.numDataRows];

				int charIndex = 0;
				for (int ii = boxStartRow; ii < boxEndRow; ii++) {
					textChars[charIndex] = charTable.getChar(ii, 0);
					textCharX1[charIndex] = charTable.getInt(ii, 1);
					textCharY1[charIndex] = charTable.getInt(ii, 2);
					textCharX2[charIndex] = charTable.getInt(ii, 3);
					textCharY2[charIndex] = charTable.getInt(ii, 4);
					charIndex++;
				}

				int numLines = linesEndRow - linesStartRow;

				String[] textLines = new String[numLines];
				int lineIndex = 0;
				for (int ii = linesStartRow; ii < linesEndRow; ii++) {
					textLines[lineIndex++] = charTable.getString(ii, 0);
				}

				// analyze word trajectories to calculate line statistics //

				{

					double lastX = Double.NaN;
					for (int i = 0; i < numWords; i++) {

						double x = (double) wordMinX[i] / imageWidth;

						if (i == 0) {
							lastX = x;
						}

						if (x < lastX && (lastX - x) > fractionPageWidthConsideredLargeMotion) {

							if (x <= 0.30) {
								largeLeftColumnXSum += x;
								largeLeftColumnXCount++;
							}
							if (x > 0.30 & x < 0.80) {
								largeRightColumnXSum += x;
								largeRightColumnXCount++;
							}

							largeNegativeXMotionSum += (lastX - x);
							largeNegativeXMotionCount++;

							largeNegativeXMotionCenterSum += (lastX + x) / 2.0;
							largeNegativeXMotionCenterCount++;

							largeNegativeXMotionCenterDeviationSum += Math.abs((lastX + x) / 2.0 - 0.5);
							largeNegativeXMotionCenterDeviationCount++;
						}

						lastX = x;

					}

				}

				if (largeLeftColumnXCount > 0)
					largeLeftColumnXAverage = largeLeftColumnXSum / largeLeftColumnXCount;
				else
					largeLeftColumnXAverage = 0.0;

				if (largeRightColumnXCount > 0)
					largeRightColumnXAverage = largeRightColumnXSum / largeRightColumnXCount;
				else
					largeRightColumnXAverage = 0.30;

				largeNegativeXMotionAverage = largeNegativeXMotionSum / largeNegativeXMotionCount;
				largeNegativeXMotionCenterAverage = largeNegativeXMotionCenterSum / largeNegativeXMotionCenterCount;
				largeNegativeXMotionCenterDeviationAverage = largeNegativeXMotionCenterDeviationSum / largeNegativeXMotionCenterDeviationCount;

				if (largeLeftColumnXCount + largeRightColumnXCount > 0)
					fractionRightColumn = (largeRightColumnXCount / (largeLeftColumnXCount + largeRightColumnXCount));
				else
					fractionRightColumn = 0;

				String[] newWordStrings = new String[numWords];
				int[] newWordMinX = new int[numWords];
				int[] newWordMaxX = new int[numWords];
				int[] newWordMinY = new int[numWords];
				int[] newWordMaxY = new int[numWords];

				System.arraycopy(wordStrings, 0, newWordStrings, 0, numWords);

				System.arraycopy(wordMinX, 0, newWordMinX, 0, numWords);
				System.arraycopy(wordMaxX, 0, newWordMaxX, 0, numWords);
				System.arraycopy(wordMinY, 0, newWordMinY, 0, numWords);
				System.arraycopy(wordMaxY, 0, newWordMaxY, 0, numWords);

				boolean dualColumn = false;
				if (Math.abs(largeNegativeXMotionAverage - 0.34) < 0.05 && Math.abs(largeNegativeXMotionCenterDeviationAverage - 0.21) < 0.03 && fractionRightColumn > 0.25) {
					dualColumn = true;
				}

				long ocrEndTime = System.currentTimeMillis();
				computeTime = (ocrEndTime - ocrStartTime) / 1000.0;
				Object[] objects = new Object[] { textLines, //
						newWordStrings, //
						textChars, //
						textCharX1, //
						textCharY1, //
						textCharX2, //
						textCharY2, //
						imageWidth, imageHeight, //
						newWordMinX, newWordMaxX, newWordMinY, newWordMaxY, //
						largeNegativeXMotionAverage,//
						largeNegativeXMotionCount,//
						largeNegativeXMotionCenterDeviationAverage, //
						largeNegativeXMotionCenterAverage, //
						largeLeftColumnXAverage, //
						largeRightColumnXAverage, //
						largeLeftColumnXCount, //
						largeRightColumnXCount, //
						fractionRightColumn, //
						computeTime, //
				};

				IO.createDirectoryPathForFile(ocrCachFilePath);
				IO.writeObject(ocrCachFilePath, objects);

			}

		}

	}

	//

	//

	//

	//

	//

	//

	//

	//

	//

	//
	//

	private static void convertToPNG() throws Exception {

		long startTime = System.currentTimeMillis();

		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//

		HashMap<String, Integer> doneFilePaths = new HashMap<String, Integer>();
		int numDoneFilePaths = 0;
		int numBadLines = 0;
		{
			String strings[] = IO.readStringLines("/media/WD3T6/convertAll.sh.out", -1);
			int numStrings = strings.length;
			System.out.println("numStrings = " + numStrings);
			for (int i = 0; i < numStrings; i++) {

				String string = strings[i];
				// System.out.println("string = " + string);

				int index1 = string.indexOf("PRODUCTIONSCANS");
				int index2 = string.indexOf(": unknown field with");
				int index3 = string.indexOf("Bad value 0 for");
				int index4 = string.indexOf("missing an image filename");
				int index5 = string.indexOf("Memory allocation failed");
				int index6 = string.indexOf("core dumped");
				int index7 = string.indexOf("Killed");

				if (index1 < 0 || index2 < 0) {
					numBadLines++;
					System.out.println("bad string = " + string);
					continue;
				}
				if (index3 > 0) {
					numBadLines++;
					System.out.println("bad string = " + string);
					continue;
				}
				if (index4 > 0) {
					System.out.println("bad string = " + string);
					continue;
				}
				if (index5 > 0) {
					System.out.println("bad string = " + string);
					continue;
				}

				if (index6 > 0) {
					System.out.println("bad string = " + string);
					continue;
				}

				if (index7 > 0) {
					System.out.println("bad string = " + string);
					continue;
				}

				String filePath = string.substring(index1, index2);

				// System.out.println("doneFilePath = " + filePath);

				if (!doneFilePaths.containsKey(filePath)) {
					doneFilePaths.put(filePath, numDoneFilePaths++);
				}

			}
		}
		System.out.println("numBadLines          = " + numBadLines);
		System.out.println("doneFilePaths.size() = " + doneFilePaths.size());
		System.out.println("numDoneFilePaths     = " + numDoneFilePaths);

		//
		//
		//
		//
		//
		//
		HashMap<Integer, String> todoFilePaths = new HashMap<Integer, String>();
		int numToDoFilePaths = 0;
		{
			String strings[] = IO.readStringLines("/media/WD3T6/PRODUCTIONSCANS.dua", -1);
			int numStrings = strings.length;
			System.out.println("numStrings = " + numStrings);
			for (int i = 0; i < numStrings; i++) {

				String string = strings[i];
				// System.out.println("string = " + string);

				int index1 = string.indexOf("PRODUCTIONSCANS");
				int index2 = string.indexOf(".tif");
				int index3 = string.indexOf("DONE");

				if (index2 < 0 || index3 < 0)
					continue;

				if (string.length() != index2 + 4) {
					continue;
				}

				String filePath = string.substring(index1);

				// System.out.println("filePath = " + filePath);

				if (!doneFilePaths.containsKey(filePath)) {
					todoFilePaths.put(numToDoFilePaths++, filePath);
				}

			}
		}
		System.out.println("todoFilePaths() = " + todoFilePaths.size());
		System.out.println("numToDoFilePaths     = " + numToDoFilePaths);
		//
		//
		//
		//

		int numFileBatches = 128;
		int maxNumItemsPerFileBatch = numToDoFilePaths / numFileBatches;
		int remainder = numToDoFilePaths % numFileBatches;

		if (remainder != 0) {
			maxNumItemsPerFileBatch++;
		}

		int[] batchSizes = new int[numFileBatches];
		String[][] batchStrings = new String[numFileBatches][maxNumItemsPerFileBatch];
		String[] batchCommands = new String[numFileBatches];

		int[] randomIndices = Utility.randomIntArray(123, numToDoFilePaths);
		for (int i = 0; i < numToDoFilePaths; i++) {
			int batchIndex = i % numFileBatches;

			String filePath = todoFilePaths.get(randomIndices[i]);
			String string = "convert " + filePath + " " + filePath.replace(".tif", ".png");
			// System.out.println(string);

			batchStrings[batchIndex][batchSizes[batchIndex]++] = string;
		}

		// System.exit(0);

		for (int batchIndex = 0; batchIndex < numFileBatches; batchIndex++) {
			String outputPath = "/media/WD3T6/convertAll." + batchIndex + ".sh";
			IO.writeStringLines(outputPath, batchStrings[batchIndex], batchSizes[batchIndex]);
			batchCommands[batchIndex] = "~dtcheng/scratch/convertAll." + batchIndex + ".sh";
		}

		int numProcessesPerQsub = 16;
		int numQsubBatches = numFileBatches / numProcessesPerQsub;

		int numStringsInQsubSH = -1;
		int maxNumQsubStrings = 1000;

		String[] qsubStrings = new String[maxNumQsubStrings];
		int processIndex = 0;
		for (int qsubBatchIndex = 0; qsubBatchIndex < numQsubBatches; qsubBatchIndex++) {

			numStringsInQsubSH = 0;
			qsubStrings[numStringsInQsubSH++] = "#!/bin/bash";
			qsubStrings[numStringsInQsubSH++] = "#$-A TG-SES130004";
			qsubStrings[numStringsInQsubSH++] = "#$ -V";
			qsubStrings[numStringsInQsubSH++] = "#$ -cwd";
			qsubStrings[numStringsInQsubSH++] = "#$ -j y                      # combine the stdout and stderr streams";
			qsubStrings[numStringsInQsubSH++] = "#$ -N ca" + qsubBatchIndex + "            # specify the executable";
			qsubStrings[numStringsInQsubSH++] = "#$ -l h_rt=04:00:00          # run time";
			qsubStrings[numStringsInQsubSH++] = "#$ -o $JOB_NAME.out$JOB_ID   # specify stdout & stderr output";
			qsubStrings[numStringsInQsubSH++] = "#$ -q normal                 # specify the queue";
			qsubStrings[numStringsInQsubSH++] = "#$ -pe 1way 12               # request one node (the 12)";
			qsubStrings[numStringsInQsubSH++] = "cd ~dtcheng/scratch";

			for (int i = 0; i < numProcessesPerQsub; i++) {

				qsubStrings[numStringsInQsubSH++] = "convertAll." + processIndex + ".sh &> convertAll." + processIndex + ".sh.out &";
				processIndex++;
			}
			qsubStrings[numStringsInQsubSH++] = "wait";

			String outputPath = "/media/WD3T6/ca." + qsubBatchIndex + ".sh";
			IO.writeStringLines(outputPath, qsubStrings, numStringsInQsubSH);
		}

		long endTime = System.currentTimeMillis();

		double duration = (endTime - startTime) / 1000.0;

		double rate = numToDoFilePaths / duration;

		System.out.println("numQsubBatches = " + numQsubBatches);

		System.out.println("numStrings = " + numToDoFilePaths);

		System.out.println("numBatches = " + numFileBatches);

		System.out.println("maxNumItemsPerBatch = " + maxNumItemsPerFileBatch);

		System.out.println("duration = " + duration);

		System.out.println("rate = " + rate);

	}

	//

	//

	private static void applyOCR() throws Exception {

		long startTime = System.currentTimeMillis();

		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//
		//

		HashMap<String, Integer> doneFilePaths = new HashMap<String, Integer>();
		int numDoneFilePaths = 0;
		int numBadLines = 0;
		{
			String strings[] = IO.readStringLines("/media/WD3T6/ocrDone.txt", -1);
			int numStrings = strings.length;
			System.out.println("numStrings = " + numStrings);
			for (int i = 0; i < numStrings; i++) {

				String string = strings[i];
				// System.out.println("string = " + string);

				int index1 = string.indexOf("PRODUCTIONSCANS");

				if (index1 < 0) {
					numBadLines++;
					System.out.println("bad string = " + string);
					continue;
				}

				String filePath = string.substring(index1);

				// System.out.println("doneFilePath = " + filePath);

				if (!doneFilePaths.containsKey(filePath)) {
					doneFilePaths.put(filePath, numDoneFilePaths++);
				}

			}
		}
		System.out.println("numBadLines          = " + numBadLines);
		System.out.println("doneFilePaths.size() = " + doneFilePaths.size());
		System.out.println("numDoneFilePaths     = " + numDoneFilePaths);

		//
		//
		//
		//
		//
		//
		HashSet<String> tiffFilePaths = new HashSet<String>();
		HashMap<Integer, String> todoFilePaths = new HashMap<Integer, String>();
		int numToDoFilePaths = 0;
		int numTifFiles = 0;
		{
			String strings[] = IO.readStringLines("/media/WD3T6/PRODUCTIONSCANS.dua", -1);
			int numStrings = strings.length;
			System.out.println("numStrings = " + numStrings);
			for (int i = 0; i < numStrings; i++) {

				String string = strings[i];
				// System.out.println("string = " + string);

				int index1 = string.indexOf("PRODUCTIONSCANS");
				int index2 = string.indexOf(".tif");
				int index3 = string.indexOf("DONE");

				if (index2 < 0 || index3 < 0)
					continue;

				if (string.length() != index2 + 4) {
					continue;
				}

				String tifFilePath = string.substring(index1);
				String pngFilePath = tifFilePath.replace(".tif", ".png");

				if (!tiffFilePaths.contains(tifFilePath)) {
					tiffFilePaths.add(tifFilePath);
					numTifFiles++;
				}

				// System.out.println("filePath = " + filePath);

				if (!doneFilePaths.containsKey(pngFilePath)) {
					todoFilePaths.put(numToDoFilePaths++, tifFilePath);
				}

			}
		}
		System.out.println("numTifFiles     = " + numTifFiles);
		System.out.println("todoFilePaths() = " + todoFilePaths.size());
		System.out.println("numToDoFilePaths     = " + numToDoFilePaths);
		//
		//
		//
		//

		int numFileBatches = 60;
		int maxNumItemsPerFileBatch = numToDoFilePaths / numFileBatches;
		int remainder = numToDoFilePaths % numFileBatches;

		if (remainder != 0) {
			maxNumItemsPerFileBatch++;
		}

		int[] batchSizes = new int[numFileBatches];
		String[][] batchStrings = new String[numFileBatches][maxNumItemsPerFileBatch];
		String[] batchCommands = new String[numFileBatches];

		int[] randomIndices = Utility.randomIntArray(123, numToDoFilePaths);
		for (int i = 0; i < numToDoFilePaths; i++) {
			int batchIndex = i % numFileBatches;

			String filePath = todoFilePaths.get(randomIndices[i]);
			String string = "tesseract " + filePath.replace(".tif", ".png") + " " + filePath.replace(".tif", "") + " >> " + filePath.replace(".tif", "") + ".box; echo DONE_WITH_OCR " + filePath.replace(".tif", ".png");
			// tesseract PRODUCTIONSCANS/SWB/FICHE/DONE/2-11-2008/Scan_01538-002.png out1.pid >> out1.pid.box
			// System.out.println(string);

			batchStrings[batchIndex][batchSizes[batchIndex]++] = string;
		}

		// System.exit(0);

		for (int batchIndex = 0; batchIndex < numFileBatches; batchIndex++) {
			String outputPath = "/media/WD3T6/ocrAll." + batchIndex + ".sh";
			IO.writeStringLines(outputPath, batchStrings[batchIndex], batchSizes[batchIndex]);
			batchCommands[batchIndex] = "~dtcheng/scratch/ocrAll." + batchIndex + ".sh";
		}

		int numProcessesPerQsub = 12;
		int numQsubBatches = numFileBatches / numProcessesPerQsub;

		int numStringsInQsubSH = -1;
		int maxNumQsubStrings = 1000;

		String[] qsubStrings = new String[maxNumQsubStrings];
		int processIndex = 0;
		for (int qsubBatchIndex = 0; qsubBatchIndex < numQsubBatches; qsubBatchIndex++) {

			numStringsInQsubSH = 0;
			qsubStrings[numStringsInQsubSH++] = "#!/bin/bash";
			qsubStrings[numStringsInQsubSH++] = "#$-A TG-SES130004";
			qsubStrings[numStringsInQsubSH++] = "#$ -V";
			qsubStrings[numStringsInQsubSH++] = "#$ -cwd";
			qsubStrings[numStringsInQsubSH++] = "#$ -j y                      # combine the stdout and stderr streams";
			qsubStrings[numStringsInQsubSH++] = "#$ -N ocr" + qsubBatchIndex + "            # specify the executable";
			qsubStrings[numStringsInQsubSH++] = "#$ -l h_rt=01:00:00          # run time";
			qsubStrings[numStringsInQsubSH++] = "#$ -o $JOB_NAME.out$JOB_ID   # specify stdout & stderr output";
			qsubStrings[numStringsInQsubSH++] = "#$ -q normal                 # specify the queue";
			qsubStrings[numStringsInQsubSH++] = "#$ -pe 1way 12               # request one node (the 12)";
			qsubStrings[numStringsInQsubSH++] = "cd ~dtcheng/scratch/ocr.sh";

			for (int i = 0; i < numProcessesPerQsub; i++) {

				qsubStrings[numStringsInQsubSH++] = "ocrAll." + processIndex + ".sh &> ocrAll." + processIndex + ".sh.out &";
				processIndex++;
			}
			qsubStrings[numStringsInQsubSH++] = "wait";

			String outputPath = "/media/WD3T6/ocr." + qsubBatchIndex + ".sh";
			IO.writeStringLines(outputPath, qsubStrings, numStringsInQsubSH);
		}

		long endTime = System.currentTimeMillis();

		double duration = (endTime - startTime) / 1000.0;

		double rate = numToDoFilePaths / duration;

		System.out.println("numQsubBatches = " + numQsubBatches);

		System.out.println("numStrings = " + numToDoFilePaths);

		System.out.println("numBatches = " + numFileBatches);

		System.out.println("maxNumItemsPerBatch = " + maxNumItemsPerFileBatch);

		System.out.println("duration = " + duration);

		System.out.println("rate = " + rate);

	}

	//

	//

	//

	//

	//

	//
	private static void getRandomSampleOfPngAndBoxFiles() throws Exception {

		HashSet<String> tiffFilePaths = new HashSet<String>();
		HashMap<Integer, String> todoFilePaths = new HashMap<Integer, String>();
		int numToDoFilePaths = 0;
		int numTifFiles = 0;
		{
			String strings[] = IO.readStringLines("/media/WD3T6/PRODUCTIONSCANS.dua", -1);
			int numStrings = strings.length;
			System.out.println("numStrings = " + numStrings);
			for (int i = 0; i < numStrings; i++) {

				String string = strings[i];
				// System.out.println("string = " + string);

				int index1 = string.indexOf("PRODUCTIONSCANS");
				int index2 = string.indexOf(".tif");
				int index3 = string.indexOf("DONE");

				if (index1 == -1)
					continue;

				if (index2 < 0 || index3 < 0)
					continue;

				if (string.length() != index2 + 4) {
					continue;
				}

				String tifFilePath = string.substring(index1);

				if (!tiffFilePaths.contains(tifFilePath)) {
					tiffFilePaths.add(tifFilePath);
					todoFilePaths.put(numToDoFilePaths++, tifFilePath);
					numTifFiles++;
				} else {
					// System.out.println("exists tifFilePath = " + tifFilePath);
				}

			}
		}
		System.out.println("numTifFiles     = " + numTifFiles);
		System.out.println("todoFilePaths() = " + todoFilePaths.size());
		System.out.println("numToDoFilePaths     = " + numToDoFilePaths);

		int sampleSize = 10000;

		int[] randomIndices = Utility.randomIntArray(123, numToDoFilePaths);

		for (int i = 0; i < sampleSize; i++) {

			String tifFilePath = todoFilePaths.get(randomIndices[i]);
			String pngFilePath = tifFilePath.replace(".tif", ".png");
			String boxFilePath = tifFilePath.replace(".tif", ".box");

			System.out.println("cp " + pngFilePath + " " + boxFilePath + " sample");

		}
		//
		//

	}

	//

	//

	//

	//

	//

	//
	static int OPTIMIZE_BIAS = 0;
	static int APPLY_BIAS = 1;

	private static void optimizeOrApplyBias(int mode, String groundTruthImageSizesTableFilePath, String groundTruthRegionTableFilePath, String markedTarFilePath, String originalTarFilePath, String unseenTarFilePath, double parameter1, double parameter2,
			double parameter3, double parameter4) throws FileNotFoundException {

		byte[] unseenPngData = new byte[maxPngDataSize];

		if (mode == OPTIMIZE_BIAS) {

		}

		if (mode == APPLY_BIAS) {

		}
		boolean firstTime = true;

		GraphicsEnvironment env = null;
		GraphicsDevice device = null;

		Frame mainFrame = null;

		optimizerRandomSeed = (int) parameter1;
		maxNumTrials = (int) parameter2;
		reducedGridSize = (int) parameter3;
		yWindowSize = (int) parameter4;
		halfYWindowSize = yWindowSize / 2;

		HashMap<Integer, int[]> groundTruthEntryIndexToImageDimensions = new HashMap<Integer, int[]>();
		{
			SimpleTable table = null;
			try {
				table = IO.readDelimitedTable(groundTruthImageSizesTableFilePath, "\t", true);
			} catch (Exception e) {
				e.printStackTrace();
			}

			int numRows = table.getNumRows();

			for (int i = 0; i < numRows; i++) {

				int entryIndex = table.getInt(i, 0);
				int width = table.getInt(i, 1);
				int height = table.getInt(i, 2);

				groundTruthEntryIndexToImageDimensions.put(entryIndex, new int[] { width, height });
			}

		}

		/***********************************/
		/* read ground truth tar file data */
		/***********************************/
		RandomAccessFile groundTruthTarRandomAccessFile = null;
		try {
			groundTruthTarRandomAccessFile = new RandomAccessFile(markedTarFilePath, "r");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		int numMarkedImages;

		byte[] markedEntryNameMemory = (byte[]) IO.readObject(markedTarFilePath + ".EntryNameMemory.ser");
		int[] markedEntryNameMemoryPtrs = (int[]) IO.readObject(markedTarFilePath + ".EntryNameMemoryPtrs.ser");
		int[] markedEntryNameMemorySizes = (int[]) IO.readObject(markedTarFilePath + ".EntryNameMemorySizes.ser");
		int[] markedEntryNameIndices = (int[]) IO.readObject(markedTarFilePath + ".EntryNameIndices.ser");
		long[] markedPositions = (long[]) IO.readObject(markedTarFilePath + ".Positions.ser");
		long[] markedLengths = (long[]) IO.readObject(markedTarFilePath + ".Lengths.ser");

		numMarkedImages = markedEntryNameIndices.length;

		HashMap<String, Integer> groundTruthEntryNameToIndex = new HashMap<String, Integer>();
		HashMap<Integer, String> groundTruthIndexToEntryName = new HashMap<Integer, String>();

		for (int entryIndex = 0; entryIndex < numMarkedImages; entryIndex++) {

			String entryName = new String(markedEntryNameMemory, markedEntryNameMemoryPtrs[markedEntryNameIndices[entryIndex]], markedEntryNameMemorySizes[markedEntryNameIndices[entryIndex]]);
			// System.out.println(entryName);
			groundTruthEntryNameToIndex.put(entryName, entryIndex);
			groundTruthIndexToEntryName.put(entryIndex, entryName);
		}

		/*************************************/
		/* read original truth tar file data */
		/*************************************/
		RandomAccessFile originalTarRandomAccessFile = null;
		try {
			originalTarRandomAccessFile = new RandomAccessFile(originalTarFilePath, "r");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		int originalNumEntries;

		byte[] originalEntryNameMemory = (byte[]) IO.readObject(originalTarFilePath + ".EntryNameMemory.ser");
		int[] originalEntryNameMemoryPtrs = (int[]) IO.readObject(originalTarFilePath + ".EntryNameMemoryPtrs.ser");
		int[] originalEntryNameMemorySizes = (int[]) IO.readObject(originalTarFilePath + ".EntryNameMemorySizes.ser");
		int[] originalEntryNameIndices = (int[]) IO.readObject(originalTarFilePath + ".EntryNameIndices.ser");
		long[] originalPositions = (long[]) IO.readObject(originalTarFilePath + ".Positions.ser");
		long[] originalLengths = (long[]) IO.readObject(originalTarFilePath + ".Lengths.ser");

		originalNumEntries = originalEntryNameIndices.length;

		System.out.println("originalNumEntries = " + originalNumEntries);

		HashMap<String, Integer> originalEntryNameToIndex = new HashMap<String, Integer>();
		HashMap<Integer, String> originalIndexToEntryName = new HashMap<Integer, String>();

		for (int entryIndex = 0; entryIndex < originalNumEntries; entryIndex++) {

			String entryName = new String(originalEntryNameMemory, originalEntryNameMemoryPtrs[originalEntryNameIndices[entryIndex]], originalEntryNameMemorySizes[originalEntryNameIndices[entryIndex]]);
			if (false)
				System.out.println(entryName);
			originalEntryNameToIndex.put(entryName, entryIndex);
			originalIndexToEntryName.put(entryIndex, entryName);
		}

		/***********************************/
		/* read unseen image tar file data */
		/***********************************/

		byte[] unseenEntryNameMemory = null;
		int[] unseenEntryNameMemoryPtrs = null;
		int[] unseenEntryNameMemorySizes = null;
		int[] unseenEntryNameIndices = null;
		long[] unseenPositions = null;
		long[] unseenLengths = null;

		HashMap<String, Integer> unseenEntryNameToIndex = null;
		HashMap<Integer, String> unseenIndexToEntryName = null;

		int unseenNumEntries = -1;

		int[] unseenEntryIndicesToClassify = null;
		// int[] unseenRandomizedEntryIndicesToClassify = null;
		int numUnseenImagesToClassify = 0;

		RandomAccessFile unseenTarRandomAccessFile = null;

		if (mode == APPLY_BIAS) {

			try {
				unseenTarRandomAccessFile = new RandomAccessFile(unseenTarFilePath, "r");
			} catch (FileNotFoundException e) {
				throw e;
			}

			unseenEntryNameMemory = (byte[]) IO.readObject(unseenTarFilePath + ".EntryNameMemory.ser");
			unseenEntryNameMemoryPtrs = (int[]) IO.readObject(unseenTarFilePath + ".EntryNameMemoryPtrs.ser");
			unseenEntryNameMemorySizes = (int[]) IO.readObject(unseenTarFilePath + ".EntryNameMemorySizes.ser");
			unseenEntryNameIndices = (int[]) IO.readObject(unseenTarFilePath + ".EntryNameIndices.ser");
			unseenPositions = (long[]) IO.readObject(unseenTarFilePath + ".Positions.ser");
			unseenLengths = (long[]) IO.readObject(unseenTarFilePath + ".Lengths.ser");

			unseenNumEntries = unseenEntryNameIndices.length;

			unseenEntryIndicesToClassify = new int[unseenNumEntries];

			System.out.println("unseenNumEntries = " + unseenNumEntries);

			unseenEntryNameToIndex = new HashMap<String, Integer>();
			unseenIndexToEntryName = new HashMap<Integer, String>();

			for (int entryIndex = 0; entryIndex < unseenNumEntries; entryIndex++) {

				String entryName = new String(unseenEntryNameMemory, unseenEntryNameMemoryPtrs[unseenEntryNameIndices[entryIndex]], unseenEntryNameMemorySizes[unseenEntryNameIndices[entryIndex]]);

				unseenEntryNameToIndex.put(entryName, entryIndex);
				unseenIndexToEntryName.put(entryIndex, entryName);

				if (entryName.contains(".rgs" + reducedGridSize + ".ser")) {

					if (true)
						System.out.println(entryName);

					unseenEntryIndicesToClassify[numUnseenImagesToClassify++] = entryIndex;
				}
			}

		}
		System.out.println("numUnseenImagesToClassify = " + numUnseenImagesToClassify);

		unseenEntryIndicesToClassify = Utility.randomizedIntArray(unseenFileOrderRandom, unseenEntryIndicesToClassify, numUnseenImagesToClassify);

		// for (int i = 0; i < unseenNumEntries; i++) {
		//
		// String entryName = new String(unseenEntryNameMemory, unseenEntryNameMemoryPtrs[unseenEntryNameIndices[i]], unseenEntryNameMemorySizes[unseenEntryNameIndices[i]]);
		// System.out.println("working on  = entryName" + entryName);
		//
		// exampleInputValueIndex += reducedGridSize;
		// }

		/*********************************************/
		/* match up ground truth and original images */
		/*********************************************/

		HashMap<Integer, Integer> groundTruthToOriginalEntryIndex = new HashMap<Integer, Integer>();

		for (int groundTruthEntryIndex = 0; groundTruthEntryIndex < numMarkedImages; groundTruthEntryIndex++) {

			String groundTruthEntryName = groundTruthIndexToEntryName.get(groundTruthEntryIndex);
			String originalEntryName = groundTruthEntryName.replace("marked", "original");
			Integer orginalIndex = originalEntryNameToIndex.get(originalEntryName);

			if (false) {
				System.out.println("groundTruthEntryName = " + groundTruthEntryName);
				System.out.println("originalEntryName = " + originalEntryName);
				System.out.println("orginalIndex = " + orginalIndex);
			}

			if (orginalIndex != null)
				groundTruthToOriginalEntryIndex.put(groundTruthEntryIndex, orginalIndex);
			else {
				System.out.println("Failure! (orginalIndex == null)");
				System.exit(1);
			}

		}
		System.out.println("groundTruthToOriginalEntryIndex.size() = " + groundTruthToOriginalEntryIndex.size());

		/*********************************/
		/* read marked title region file */
		/*********************************/

		SimpleTable table = null;
		try {
			table = IO.readDelimitedTable(groundTruthRegionTableFilePath, "\t", true);
		} catch (Exception e) {
			e.printStackTrace();
		}

		int numRegions = table.getNumRows();

		int[] imageNumRegions = new int[numMarkedImages];
		int[][] imageMarkedRegionX1s = new int[numMarkedImages][MAX_NUM_MARKED_REGIONS_PER_IMAGE];
		int[][] imageMarkedRegionY1s = new int[numMarkedImages][MAX_NUM_MARKED_REGIONS_PER_IMAGE];
		int[][] imageMarkedRegionX2s = new int[numMarkedImages][MAX_NUM_MARKED_REGIONS_PER_IMAGE];
		int[][] imageMarkedRegionY2s = new int[numMarkedImages][MAX_NUM_MARKED_REGIONS_PER_IMAGE];
		int[][] imageMarkedRegionImageHeights = new int[numMarkedImages][MAX_NUM_MARKED_REGIONS_PER_IMAGE];
		int[][] imageMarkedRegionImageWidths = new int[numMarkedImages][MAX_NUM_MARKED_REGIONS_PER_IMAGE];

		int lastGroundTruthTarFileIndex = -1;

		{

			for (int i = 0; i < numRegions; i++) {

				int groundTruthTarFileIndex = table.getInt(i, 0);
				int x1 = table.getInt(i, 1);
				int y1 = table.getInt(i, 2);
				int x2 = table.getInt(i, 3);
				int y2 = table.getInt(i, 4);
				int width = table.getInt(i, 5);
				int hieght = table.getInt(i, 6);

				if (false)
					System.out.printf("%d\t%d\t%d\t%d\t%d\n", groundTruthTarFileIndex, x1, y1, x2, y2, hieght, width);

				if (imageNumRegions[groundTruthTarFileIndex] < MAX_NUM_MARKED_REGIONS_PER_IMAGE) {

					imageMarkedRegionX1s[groundTruthTarFileIndex][imageNumRegions[groundTruthTarFileIndex]] = x1;
					imageMarkedRegionY1s[groundTruthTarFileIndex][imageNumRegions[groundTruthTarFileIndex]] = y1;
					imageMarkedRegionX2s[groundTruthTarFileIndex][imageNumRegions[groundTruthTarFileIndex]] = x2;
					imageMarkedRegionY2s[groundTruthTarFileIndex][imageNumRegions[groundTruthTarFileIndex]] = y2;
					imageMarkedRegionImageHeights[groundTruthTarFileIndex][imageNumRegions[groundTruthTarFileIndex]] = hieght;
					imageMarkedRegionImageWidths[groundTruthTarFileIndex][imageNumRegions[groundTruthTarFileIndex]] = width;

					imageNumRegions[groundTruthTarFileIndex]++;
				}

			}

		}

		System.out.println("numImages = " + numMarkedImages);

		int[] imageHeights = new int[numMarkedImages];

		/***********************/
		/***********************/
		/***********************/
		/****               ****/
		/**** OPTIMIZE BIAS ****/
		/****               ****/
		/***********************/
		/***********************/
		/***********************/

		double bestPerformance = Double.NEGATIVE_INFINITY;
		double bestPerformanceRecall = Double.NEGATIVE_INFINITY;
		double bestPerformancePrecision = Double.NEGATIVE_INFINITY;
		double[] biasValues = new double[numBiasDimensions];

		double[] bestBiasValues = new double[numBiasDimensions];

		int trialIndex = 0;

		// int maxNumValues = (int) 1e9;

		double[] classWeights = new double[numClasses];

		// float[] allExampleValues = new float[maxNumValues];

		int numExamples = -1;

		if (mode == APPLY_BIAS) {
			numExamples = (int) (numTestImagesPerBatch + numMarkedImages) * reducedGridSize;
		} else {
			numExamples = (int) numMarkedImages * reducedGridSize;
		}

		System.out.println("numExamples = " + numExamples);

		int maxNumInputValues = (int) reducedGridSize;
		float[] exampleInputValues = new float[numExamples * maxNumInputValues];

		float[] exampleOutputValues = new float[numExamples * numClasses];

		double[] exampleWeights = new double[numExamples];
		double[] exampleClassWeights = new double[numExamples * numClasses];
		float[] exampleOutputPredictions = new float[numExamples * numClasses];

		int markedImageOffset = -1;

		if (mode == APPLY_BIAS) {
			markedImageOffset = numTestImagesPerBatch;
		} else {
			markedImageOffset = 0;
		}

		int[] useExampleGroupForTraining = new int[numExamples / reducedGridSize];
		int[] useExampleGroupForTesting = new int[numExamples / reducedGridSize];

		// boolean[] badFile = null;

		Random optimizerRandom = new Random();

		if (optimizerTrueRandom || optimizerRandomSeed == -1)
			optimizerRandom.setSeed(System.currentTimeMillis());
		else
			optimizerRandom.setSeed(optimizerRandomSeed);

		// int unseenImageToClassifyIndex = 0;

		while (true) {

			System.out.println();

			System.out.println("trialIndex = " + trialIndex);

			// if (trialIndex != 1817 && trialIndex != 1818 && trialIndex != 1821 && trialIndex != 1824 && trialIndex != 1825 && trialIndex != 1826) // !!!
			// {
			// trialIndex++;
			// continue;
			// }

			/************************************/
			/* generate new bias point to try */
			/************************************/

			if (trialIndex == 0 && biasOrigin != null && useBiasOriginAsFirstBiasPick || mode == APPLY_BIAS) {
				for (int i = 0; i < numBiasDimensions; i++) {
					biasValues[i] = biasOrigin[i];
				}

			} else {
				for (int i = 0; i < numBiasDimensions; i++) {

					double lowerBounds = biasLowerBounds[i];
					double upperBounds = biasUpperBounds[i];

					double value = Double.NaN;

					double randomValue = optimizerRandom.nextDouble();

					double range;
					switch (biasTypes[i]) {

					case DATA_TYPE_INT_LINEAR_INDEX:
						range = upperBounds - lowerBounds;
						value = (int) (lowerBounds + randomValue * (range * 1));
						break;

					case DATA_TYPE_DOUBLE_LINEAR_INDEX:
						range = upperBounds - lowerBounds;
						value = lowerBounds + randomValue * range;
						break;

					case DATA_TYPE_INT_LOG2_INDEX:
						lowerBounds = Math.log(lowerBounds) / Math.log(2.0);
						upperBounds = Math.log(upperBounds) / Math.log(2.0);
						range = upperBounds - lowerBounds;
						int logValue = (int) (lowerBounds + randomValue * (range + 1));
						value = Math.pow(2, logValue);

						break;

					case DATA_TYPE_DOUBLE_LOG_INDEX:

						lowerBounds = Math.log(lowerBounds);
						upperBounds = Math.log(upperBounds);
						range = upperBounds - lowerBounds;
						value = Math.exp(lowerBounds + randomValue * range);

						break;

					}

					biasValues[i] = value;

				}
			}

			// report new bias //

			for (int i = 0; i < numBiasDimensions; i++) {

				System.out.printf("%s %12.9f\n", biasNames[i], biasValues[i]);

			}

			// set local variables to bias point //

			// int reductionFactor = (int) biasValues[REDUCION_FACTOR_LOG2_INDEX];
			// double classificationThreshold = biasValues[CLASSIFICATION_THRESHOLD_INDEX];

			//

			/******************************/
			/******************************/
			/******************************/
			/******************************/
			/** set local bias variables **/
			/******************************/
			/******************************/
			/******************************/
			/******************************/

			distanceWeightingPower = biasValues[0];
			titleProbabilityOffset = biasValues[1];
			minDiff = biasValues[2];
			optimizerXSmoothingWindowSize = (int) biasValues[3];
			leftIgnoreFraction = biasValues[4];
			rightIgnoreFraction = biasValues[5];
			headerNoTitleZoneFraction = biasValues[6];
			footerNoTitleZoneFraction = biasValues[7];

			//

			System.out.println("reducedGridSize = " + reducedGridSize);
			// System.out.println("classificationThreshold = " + classificationThreshold);

			// evaluate performance of bias

			/*********************/
			/*********************/
			/**                 **/
			/** CREATE EXAMPLES **/
			/**                 **/
			/*********************/
			/*********************/

			int unseenImageStartIndex = -1;
			int unseenImageEndIndex = -1;

			// unseenImageStartIndex = unseenImageToClassifyIndex;
			// unseenImageEndIndex = unseenImageToClassifyIndex + numTestImagesPerBatch;
			unseenImageStartIndex = trialIndex;
			unseenImageEndIndex = trialIndex + numTestImagesPerBatch;
			if (unseenImageEndIndex > unseenNumEntries) {
				unseenImageEndIndex = unseenNumEntries;
			}

			if (createExamples) {

				/*********************************/
				/* set ground truth input values */
				/*********************************/

				{

					int exampleInputValueIndex = -1;

					if (mode == APPLY_BIAS) {
						exampleInputValueIndex = 0;
						for (int unseenImageIndex = unseenImageStartIndex; unseenImageIndex < unseenImageEndIndex; unseenImageIndex++) {

							int unseenTarFileEntryIndex = unseenEntryIndicesToClassify[unseenImageIndex];

							String entryName = new String(unseenEntryNameMemory, unseenEntryNameMemoryPtrs[unseenEntryNameIndices[unseenTarFileEntryIndex]], unseenEntryNameMemorySizes[unseenEntryNameIndices[unseenTarFileEntryIndex]]);

							System.out.println("working on sample file: " + entryName);

							int reducedWidth = reducedGridSize;
							int reducedHeight = reducedGridSize;

							int numValues = reducedWidth * reducedHeight;

							int unseenDataSize = (int) unseenLengths[unseenTarFileEntryIndex];
							byte[] unseenFloatByteData = new byte[(int) unseenLengths[unseenTarFileEntryIndex]];

							try {
								unseenTarRandomAccessFile.seek(unseenPositions[unseenTarFileEntryIndex]);

								unseenTarRandomAccessFile.readFully(unseenFloatByteData, 0, unseenDataSize);

								ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(unseenFloatByteData, 0, unseenDataSize);

								float[] inputFloatValues = (float[]) IO.readObjectFromStream(byteArrayInputStream);

								if (inputFloatValues.length != numValues) {
									System.out.println("(values.length != numValues)");
									(new Exception()).printStackTrace();
									System.exit(1);
								}

								if (checkForNANs)
									for (int valueIndex = 0; valueIndex < inputFloatValues.length; valueIndex++) {
										System.out.println("valueIndex = " + valueIndex);
										System.out.println("values[valueIndex] = " + inputFloatValues[valueIndex]);
										if (Double.isNaN(inputFloatValues[valueIndex])) {
											(new Exception()).printStackTrace();
											System.exit(1);
										}

									}

								/**************************/
								/* normalize float values */
								/**************************/

								if (normalizeInputValues) {
									double sum = 0.0;
									for (int valueIndex = 0; valueIndex < inputFloatValues.length; valueIndex++) {
										sum += inputFloatValues[valueIndex];
									}
									double average = sum / inputFloatValues.length;
									for (int valueIndex = 0; valueIndex < inputFloatValues.length; valueIndex++) {
										inputFloatValues[valueIndex] = (float) (inputFloatValues[valueIndex] / average);
									}

								}

								int inputFloatValueIndex = 0;

								for (int y = 0; y < reducedGridSize; y++) {

									for (int x = 0; x < reducedGridSize; x++) {
										exampleInputValues[exampleInputValueIndex++] = inputFloatValues[inputFloatValueIndex++];
									}

									if (optimizerXSmoothingWindowSize > 1) {

										int currentExampleInputValuesIndex = exampleInputValueIndex - reducedGridSize;

										int endX = -1;

										for (int x = 0; x < reducedGridSize; x++) {

											double sum = 0;
											int startX = x;
											if (x + optimizerXSmoothingWindowSize <= reducedGridSize) {
												endX = x + optimizerXSmoothingWindowSize;
											} else {
												endX = reducedGridSize;
											}
											for (int x2 = startX; x2 < endX; x2++) {
												sum += exampleInputValues[currentExampleInputValuesIndex + x2];
											}

											// inputValues[x] = (float) (sum / (endX - startX));

											exampleInputValues[currentExampleInputValuesIndex + x] = (float) (sum / optimizerXSmoothingWindowSize); // pad with black space (0.0 intensity)
										}

									}

								}

							} catch (Exception e) {
								e.printStackTrace();
								System.exit(1);
							}

							// exampleInputValueIndex += reducedGridSize;
						}

					} else {
						exampleInputValueIndex = 0;
					}

					if ((mode == OPTIMIZE_BIAS) || ((mode == APPLY_BIAS) && trialIndex == 0))

						for (int i = 0; i < numMarkedImages; i++) {

							int exampleGroundTruthTarFileIndex = i;

							String entryName = new String(markedEntryNameMemory, markedEntryNameMemoryPtrs[markedEntryNameIndices[exampleGroundTruthTarFileIndex]], markedEntryNameMemorySizes[markedEntryNameIndices[exampleGroundTruthTarFileIndex]]);

							String exampleValuesEntryName = entryName.replace(".png", ".rgs" + reducedGridSize + ".ser").replace("marked", "original");

							if (exampleValuesEntryName == null) {
								(new Exception()).printStackTrace();
								System.exit(1);
							}

							Integer originalEntryIndex = originalEntryNameToIndex.get(exampleValuesEntryName);

							if (originalEntryIndex == null) {
								(new Exception()).printStackTrace();
								System.exit(1);
							}

							int[] dimensions = groundTruthEntryIndexToImageDimensions.get(exampleGroundTruthTarFileIndex);

							if (dimensions == null) {
								(new Exception()).printStackTrace();
								System.exit(1);
							}

							int reducedWidth = reducedGridSize;
							int reducedHeight = reducedGridSize;

							int numValues = reducedWidth * reducedHeight;

							int originalDataSize = (int) originalLengths[originalEntryIndex];
							byte[] originalFloatByteData = new byte[(int) originalLengths[originalEntryIndex]];

							try {
								originalTarRandomAccessFile.seek(originalPositions[originalEntryIndex]);

								originalTarRandomAccessFile.readFully(originalFloatByteData, 0, originalDataSize);

								ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(originalFloatByteData, 0, originalDataSize);

								float[] inputFloatValues = (float[]) IO.readObjectFromStream(byteArrayInputStream);

								if (inputFloatValues.length != numValues) {
									System.out.println("(values.length != numValues)");
									(new Exception()).printStackTrace();
									System.exit(1);
								}

								if (checkForNANs)
									for (int valueIndex = 0; valueIndex < inputFloatValues.length; valueIndex++) {
										System.out.println("valueIndex = " + valueIndex);
										System.out.println("values[valueIndex] = " + inputFloatValues[valueIndex]);
										if (Double.isNaN(inputFloatValues[valueIndex])) {
											(new Exception()).printStackTrace();
											System.exit(1);
										}

									}

								/**************************/
								/* normalize float values */
								/**************************/

								if (normalizeInputValues) {
									double sum = 0.0;
									for (int valueIndex = 0; valueIndex < inputFloatValues.length; valueIndex++) {
										sum += inputFloatValues[valueIndex];
									}
									double average = sum / inputFloatValues.length;
									for (int valueIndex = 0; valueIndex < inputFloatValues.length; valueIndex++) {
										inputFloatValues[valueIndex] = (float) (inputFloatValues[valueIndex] / average);
									}

								}

								int inputFloatValueIndex = 0;

								for (int y = 0; y < reducedGridSize; y++) {

									// float[] inputValues = exampleInputValues[exampleIndex];
									// int valueIndex = exampleIndex * reducedGridSize;

									for (int x = 0; x < reducedGridSize; x++) {

										if (false) {
											System.out.println(" valueIndex = " + inputFloatValueIndex);
											System.out.println(" values[valueIndex] = " + inputFloatValues[inputFloatValueIndex]);
										}

										exampleInputValues[exampleInputValueIndex++] = inputFloatValues[inputFloatValueIndex++];

									}

									if (optimizerXSmoothingWindowSize > 1) {

										int currentExampleInputValuesIndex = exampleInputValueIndex - reducedGridSize;

										int endX = -1;

										for (int x = 0; x < reducedGridSize; x++) {

											double sum = 0;
											int startX = x;
											if (x + optimizerXSmoothingWindowSize <= reducedGridSize) {
												endX = x + optimizerXSmoothingWindowSize;
											} else {
												endX = reducedGridSize;
											}
											for (int x2 = startX; x2 < endX; x2++) {
												sum += exampleInputValues[currentExampleInputValuesIndex + x2];
											}

											// inputValues[x] = (float) (sum / (endX - startX));

											exampleInputValues[currentExampleInputValuesIndex + x] = (float) (sum / optimizerXSmoothingWindowSize); // pad with black space (0.0 intensity)
										}

									}

								}

							} catch (Exception e) {
								e.printStackTrace();
								System.exit(1);
							}

						}
				}

				/**********************************/
				/* set ground truth output values */
				/**********************************/

				if (mode == APPLY_BIAS) {

					for (int i = 0; i < markedImageOffset; i++) {
						useExampleGroupForTesting[i] = 1;
					}

				}

				for (int i = 0; i < numMarkedImages; i++) {

					int exampleIndex = (markedImageOffset + i) * reducedGridSize;

					for (int j = 0; j < reducedGridSize; j++) {
						exampleOutputValues[(exampleIndex + j) * numClasses + 0] = 1;
						exampleOutputValues[(exampleIndex + j) * numClasses + 1] = 0;
					}

					if (useOnlyMarkedImagesForTrainAndTest) {
						if (imageNumRegions[i] > 0) {
							useExampleGroupForTraining[markedImageOffset + i] = 1;
							if (mode == OPTIMIZE_BIAS) {
								useExampleGroupForTesting[markedImageOffset + i] = 1;
							}
						}
					} else {
						useExampleGroupForTraining[markedImageOffset + i] = 1;
						if (mode == OPTIMIZE_BIAS) {
							useExampleGroupForTesting[markedImageOffset + i] = 1;
						}
					}

					for (int j = 0; j < imageNumRegions[i]; j++) {

						int y1 = imageMarkedRegionY1s[i][j] * reducedGridSize / imageMarkedRegionImageHeights[i][j];
						int y2 = imageMarkedRegionY2s[i][j] * reducedGridSize / imageMarkedRegionImageHeights[i][j];

						for (int j2 = y1; j2 < y2; j2++) {
							exampleOutputValues[(exampleIndex + j2) * numClasses + 0] = 0;
							exampleOutputValues[(exampleIndex + j2) * numClasses + 1] = 1;
						}

					}

				}
			}

			/******************************************/
			/******************************************/
			/**                                      **/
			/** EVALUATE BIAS USING CROSS VALIDATION **/
			/**                                      **/
			/******************************************/
			/******************************************/

			boolean biasFailure = false;

			int maxNumObservations = numMarkedImages;
			int numObservations = 0;
			// double[] predictedMatchScores = new double[maxNumObservations];
			// double[] predictedMatchRegionLengths = new double[maxNumObservations];
			// double[] actualMatchRegionFractions = new double[maxNumObservations];

			double accuracy = Double.NaN;

			int[] randomImageIndices = null;

			randomImageIndices = Utility.randomIntArray(optimizerRandom, numMarkedImages);

			int[][] classificationDistributionMatrix = new int[2][2]; /* [predicted][actual] */

			long startTime = System.currentTimeMillis();

			/*****************************/
			/* USE GPU TO SOLVE PROBLEMS */
			/*****************************/

			int numProblemsPerPass = -1;

			if (mode == APPLY_BIAS) {
				numProblemsPerPass = numTestImagesPerBatch * reducedGridSize;
			} else {
				numProblemsPerPass = numExamples;
			}

			int numPasses = -1;

			if (useJTP) {

				numPasses = 2;

			} else {

				if (mode == APPLY_BIAS)
					numPasses = Math.max((int) ((double) (numTestImagesPerBatch * reducedGridSize) * (double) (numExamples - numTestImagesPerBatch * reducedGridSize) * (double) reducedGridSize / (1e7)), 1) + 1; // last pass for normalization
				else
					numPasses = Math.max((int) ((double) numExamples * (double) numExamples * (double) reducedGridSize / (1e9)), 1) + 1; // last pass for normalization

			}

			System.out.println("numPasses = " + numPasses);
			{
				InstancedBasedPredictorFullCrossValidation.InstancedBasedPredictorKernel kernel = new InstancedBasedPredictorFullCrossValidation.InstancedBasedPredictorKernel(

				//
						numProblemsPerPass,
						//
						numPasses,
						//
						InstancedBasedPredictorFullCrossValidation.InstancedBasedPredictorKernel.TRAINING_MODE,
						//
						numExamples, 0, reducedGridSize, reducedGridSize, numClasses,
						//
						useExampleGroupForTraining, useExampleGroupForTesting,
						//
						exampleInputValues, exampleOutputValues, exampleWeights, exampleClassWeights,
						//
						(float) minDiff, (float) distanceWeightingPower, yWindowSize, reducedGridSize, (float) leftIgnoreFraction, (float) rightIgnoreFraction,
						//
						exampleOutputPredictions
				//
				);

				if (InstancedBasedPredictorKernel.useSEQ)
					kernel.setExecutionMode(Kernel.EXECUTION_MODE.SEQ);
				else //
				if (InstancedBasedPredictorKernel.useJTP)
					kernel.setExecutionMode(Kernel.EXECUTION_MODE.JTP);
				else //
				if (InstancedBasedPredictorKernel.useGPU)
					kernel.setExecutionMode(Kernel.EXECUTION_MODE.GPU);

				//
				//

				kernel.computePredictions(numProblemsPerPass);

				// free kernel resources //

				kernel.dispose();
				// System.gc(); // !!!
				//
				//
			}

			/******************************/
			/* analyze prediction results */
			/******************************/

			int[] predictionDistribution = new int[reducedGridSize];

			if (reportClassifications)
				System.out.println("fileIndex\tx1\ty1\tx2\ty2\twidth\theight");

			int numExamplesToAnalyze = -1;
			if (mode == APPLY_BIAS) {
				numExamplesToAnalyze = numTestImagesPerBatch * reducedGridSize;
			} else {
				numExamplesToAnalyze = numExamples;
			}

			if (mode == OPTIMIZE_BIAS) {

				for (int exampleIndex = 0; exampleIndex < numExamplesToAnalyze; exampleIndex++) {

					int exampleY = exampleIndex % reducedGridSize;
					int exampleGroupIndex = exampleIndex / reducedGridSize;

					if (useExampleGroupForTesting[exampleGroupIndex] == 0)
						continue;

					if (exampleY < halfYWindowSize)
						continue;
					if (exampleY - halfYWindowSize + (yWindowSize - 1) >= reducedGridSize)
						continue;

					/* find predicted class */

					for (int i = 0; i < numClasses; i++) {
						classWeights[i] = exampleOutputPredictions[exampleIndex * numClasses + i];
					}
					boolean hasNAN = false;
					for (int i = 0; i < numClasses; i++) {
						if (Double.isNaN(classWeights[i]))
							hasNAN = true;
					}
					if (hasNAN) {
						System.out.println("hasNAN");
						classWeights[0] = 1;
						classWeights[1] = 0;
					}

					if (true) {
						classWeights[0] -= (titleProbabilityOffset /* - blackWeight * averageValue */);
						classWeights[1] += (titleProbabilityOffset /* + blackWeight * averageValue */);
					}

					if (false) {
						for (int i = 0; i < numClasses; i++) {
							System.out.println("classWeights[" + i + "] = " + classWeights[i]);
						}
					}

					/* determine predicted class */

					int predictedClass = -1;
					double bestPredictedWeight = Double.NEGATIVE_INFINITY;
					for (int i = 0; i < numClasses; i++) {
						if (classWeights[i] > bestPredictedWeight) {
							bestPredictedWeight = classWeights[i];
							predictedClass = i;
						}
					}

					/* !!! catch underflow/overflow failures */
					if (predictedClass == -1) {
						predictedClass = 0;
					}

					if (exampleY < headerNoTitleZoneFraction * reducedGridSize) {
						predictedClass = 0;
					}
					if (exampleY >= reducedGridSize - footerNoTitleZoneFraction * reducedGridSize) {
						predictedClass = 0;
					}

					/* determine actual class */

					int actualClass = -1;
					double bestActualWeight = Double.NEGATIVE_INFINITY;
					for (int i = 0; i < numClasses; i++) {
						if (exampleOutputValues[(exampleIndex) * numClasses + i] > bestActualWeight) {
							bestActualWeight = exampleOutputValues[exampleIndex * numClasses + i];
							actualClass = i;
						}
					}

					if (false) {

						int testImageIndex = exampleIndex / reducedGridSize;
						System.out.println("testImageIndex = " + testImageIndex);
						System.out.println("testImageY = " + exampleIndex);
						// System.out.println("bestDiffSum = " + bestDiff);
						// System.out.println("bestTrainImageIndex = " + bestTrainImageIndex);
						// System.out.println("bestTrainImageY = " + bestTrainImageY);
						// System.out.println("bestTrainClass = " + bestTrainClass);

						// int actualClass = exampleOutputValues[testImageIndex * reducedGridSize + testImageY];

						System.out.println("predictedClass = " + predictedClass);
						System.out.println("actualClass = " + actualClass);
					}

					classificationDistributionMatrix[predictedClass][actualClass]++;

					int testImageIndex = exampleIndex / reducedGridSize;
					int testImageY = exampleIndex % reducedGridSize;

					if (predictedClass == 1) {

						predictionDistribution[testImageY]++;

						int width = groundTruthEntryIndexToImageDimensions.get(testImageIndex)[0];
						int height = groundTruthEntryIndexToImageDimensions.get(testImageIndex)[1];

						int fileIndex = testImageIndex;
						int x1 = 0;
						int y1 = height * testImageY / reducedGridSize;
						int x2 = width;
						int y2 = height * (testImageY + 1) / reducedGridSize;

						if (reportClassifications)
							System.out.println(fileIndex + "\t" + x1 + "\t" + y1 + "\t" + x2 + "\t" + y2 + "\t" + width + "\t" + height + "\t");

						// System.out.println("testImageIndex = " + testImageIndex);
						// System.out.println("testImageY = " + testImageY);
					}

				}
			}

			if (mode == APPLY_BIAS) {

				if (firstTime) {
					try {
						env = GraphicsEnvironment.getLocalGraphicsEnvironment();
						device = env.getDefaultScreenDevice();
					} catch (HeadlessException e1) {
						// TODO Auto-generated catch block
						// e1.printStackTrace();
					}

					mainFrame = new Frame();
					mainFrame.setAlwaysOnTop(false);

					mainFrame.setIgnoreRepaint(false);
					mainFrame.setUndecorated(true);

					// device.setDisplayMode(null);

					mainFrame.setSize(displayScreenWidth, displayScreenHeight);

					mainFrame.setLocation(0, 0);

					mainFrame.addMouseListener(mouseListener);

					// if (rnr.showWindow)
					mainFrame.setVisible(true);
				}
				firstTime = false;

				for (int unseenImageIndex = unseenImageStartIndex; unseenImageIndex < unseenImageEndIndex; unseenImageIndex++) {

					int batchImageIndex = unseenImageIndex - unseenImageStartIndex;

					Integer unseenIndex = unseenEntryIndicesToClassify[unseenImageIndex];

					if (unseenIndex == null) {
						System.out.println("Warning!  unseenIndex = " + unseenIndex + " not found");
						continue;
					}

					String unseenEntryName = unseenIndexToEntryName.get(unseenIndex);
					// unseenEntryName = "sample/SWB#FICHE#DONE#2-11-2008#Scan_01994-083.rgs64.ser"; //SWB/FICHE/DONE/2-11-2008/Scan_01994-083.ocr !!!

					String pngFileName = unseenEntryName.replace(".rgs" + reducedGridSize + ".ser", ".png");

					//

					//

					//

					//
					//

					//

					System.out.println("unseenEntryName = " + unseenEntryName);
					System.out.println("pngFileName = " + pngFileName);

					int pngTarFileEntryIndex = unseenEntryNameToIndex.get(pngFileName);
					System.out.println("pngTarFileEntryIndex = " + pngTarFileEntryIndex);

					try {

						//

						//

						// OCR and print text //

						boolean pageIsTwoColumn = false;
						int numWords = 0;
						int maxNumWords;
						String[] wordStrings = null;
						String[] wordLines = null;
						int[] wordMinX = null;
						int[] wordMaxX = null;
						int[] wordMinY = null;
						int[] wordMaxY = null;

						double fractionPageWidthConsideredLargeMotion = 0.2;

						double largeLeftColumnXSum = 0;
						double largeLeftColumnXCount = 0;
						double largeLeftColumnXAverage = Double.NaN;

						double largeRightColumnXSum = 0;
						double largeRightColumnXCount = 0;
						double largeRightColumnXAverage = Double.NaN;

						double largeNegativeXMotionSum = 0;
						double largeNegativeXMotionCount = 0;
						double largeNegativeXMotionAverage = Double.NaN;

						double largeNegativeXMotionCenterSum = 0;
						double largeNegativeXMotionCenterCount = 0;
						double largeNegativeXMotionCenterAverage = Double.NaN;

						double largeNegativeXMotionCenterDeviationSum = 0;
						double largeNegativeXMotionCenterDeviationCount = 0;
						double largeNegativeXMotionCenterDeviationAverage = Double.NaN;
						double fractionRightColumn = Double.NaN;

						double computeTime = Double.NaN;

						String filePathRoot = unseenEntryName.substring(0, unseenEntryName.indexOf("."));
						String ocrCachFilePath = filePathRoot + ".ocr";

						if (IO.fileDoesNotExists(ocrCachFilePath)) {

							System.out.println("Error! no OCR file for" + ocrCachFilePath);
							continue;
						}

						Object[] objects = (Object[]) IO.readObject(ocrCachFilePath);

						int index = 0;
						wordLines = (String[]) objects[index++];
						wordStrings = (String[]) objects[index++];

						char[] textChars = (char[]) objects[index++];
						int[] textCharX1 = (int[]) objects[index++];
						int[] textCharY1 = (int[]) objects[index++];
						int[] textCharX2 = (int[]) objects[index++];
						int[] textCharY2 = (int[]) objects[index++];

						int imageWidth = (Integer) objects[index++];
						int imageHeight = (Integer) objects[index++];
						wordMinX = (int[]) objects[index++];
						wordMaxX = (int[]) objects[index++];
						wordMinY = (int[]) objects[index++];
						wordMaxY = (int[]) objects[index++];
						largeNegativeXMotionAverage = (Double) objects[index++];
						largeNegativeXMotionCount = (Double) objects[index++];
						largeNegativeXMotionCenterDeviationAverage = (Double) objects[index++];
						largeNegativeXMotionCenterAverage = (Double) objects[index++];
						largeLeftColumnXAverage = (Double) objects[index++];
						largeRightColumnXAverage = (Double) objects[index++];
						largeLeftColumnXCount = (Double) objects[index++];
						largeRightColumnXCount = (Double) objects[index++];
						fractionRightColumn = (Double) objects[index++];
						computeTime = (Double) objects[index++];

						numWords = wordStrings.length;

						boolean dualColumn = false;
						if (Math.abs(largeNegativeXMotionAverage - 0.34) < 0.05 && Math.abs(largeNegativeXMotionCenterDeviationAverage - 0.21) < 0.03 && fractionRightColumn > 0.25) {
							dualColumn = true;
						}

						if (singleColumnOnly && dualColumn)
							continue;

						if (dualColumnOnly && !dualColumn)
							continue;

						//

						//

						//

						//

						//
						try {
							unseenTarRandomAccessFile.seek(unseenPositions[pngTarFileEntryIndex]);
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
							System.exit(1);
						}

						int unseenDataSize = (int) unseenLengths[pngTarFileEntryIndex];

						try {
							unseenTarRandomAccessFile.readFully(unseenPngData, 0, unseenDataSize);
						} catch (IOException e1) {
							e1.printStackTrace();
							System.exit(1);
						}

						System.out.println("unseenDataSize = " + unseenDataSize);

						//

						//

						//

						//

						//

						//

						//

						//

						BufferedImage unseenBufferedImage = ImageIO.read(new ByteArrayInputStream(unseenPngData, 0, unseenDataSize));

						int bufferedImageWidth = unseenBufferedImage.getWidth();
						int bufferedImageHeight = unseenBufferedImage.getHeight();

						if (bufferedImageWidth != imageWidth || bufferedImageHeight != imageHeight) {
							throw new Exception("bufferedImageWidth != imageWidth || bufferedImageHeight  != imageHeight");
						}

						System.out.println("imageWidth = " + imageWidth);
						System.out.println("imageHeight = " + imageHeight);

						if (imageWidth >= 20000 || imageHeight >= 20000) {
							System.out.println("Error!  Invalid image size for: " + pngFileName);
							System.out.println("Error!  imageWidth = " + imageWidth);
							System.out.println("Error!  imageHeight = " + imageHeight);
							continue;
						}

						//

						//

						//

						//

						//

						//

						//

						//

						//

						//

						//

						//

						//

						//

						//

						//

						// double imageHeightScaleDownFactor = (double) screenHeight / (double) imageHeight;
						// int newScreenWidth = (int) (imageHeightScaleDownFactor * imageWidth);

						boolean showImages = true;

						if (showImages) {

							Graphics g = mainFrame.getGraphics();

							g.clearRect(0, 0, displayScreenWidth, displayScreenHeight);

							if (fullResolution) {
								g.drawImage(unseenBufferedImage, 0, 0, imageWidth, imageHeight, 0, 0, imageWidth, imageHeight, null);
							} else {
								g.drawImage(unseenBufferedImage, 0, 0, (displayScreenWidth / 2), displayScreenHeight, 0, 0, imageWidth, imageHeight, null);
							}

							if (expertRatingMode) {

								int thumbNailSizeX = displayScreenWidth / 2 / 2;
								int thumbNailSizeY = displayScreenHeight / 3;

								for (int yPlanelIndex = 0; yPlanelIndex < 3; yPlanelIndex++) {
									for (int xPlanelIndex = 0; xPlanelIndex < 2; xPlanelIndex++) {

										int dx1 = (displayScreenWidth / 2) + thumbNailSizeX * xPlanelIndex;
										int dy1 = thumbNailSizeY * yPlanelIndex;

										int dx2 = (displayScreenWidth / 2) + thumbNailSizeX * (xPlanelIndex + 1);
										int dy2 = thumbNailSizeY * (yPlanelIndex + 1);

										g.drawImage(unseenBufferedImage, dx1, dy1, dx2, dy2, 0, 0, imageWidth, imageHeight, null);

									}

								}

							}
							//

							//

							//
							//

							//

							//

							//

							//

							g.setFont(new Font("Courier New", Font.PLAIN, 24));
							g.setColor(Color.RED);

							g.drawString(unseenEntryName.substring(0, unseenEntryName.indexOf(".")), 0, 20);

							g.drawString("  la = " + (float) largeNegativeXMotionAverage + //
									"  lc = " + (float) largeNegativeXMotionCount + //
									" d = " + (float) largeNegativeXMotionCenterDeviationAverage + //
									" c = " + (float) largeNegativeXMotionCenterAverage + //
									" x1 = " + (float) largeLeftColumnXAverage + //
									" x2 = " + (float) largeRightColumnXAverage + //
									" x1c = " + (float) largeLeftColumnXCount + //
									" x2c = " + (float) largeRightColumnXCount, //
									0, displayScreenHeight / 2);

							double splitX1 = largeLeftColumnXAverage;
							double splitX2 = largeRightColumnXAverage;
							g.drawLine((int) (splitX1 * displayScreenWidth / 2), 0, (int) (splitX1 * displayScreenWidth / 2), displayScreenHeight);
							g.drawLine((int) (splitX2 * displayScreenWidth / 2), 0, (int) (splitX2 * displayScreenWidth / 2), displayScreenHeight);

							//

							//

							//

							//

							//

							if (displayOCRCharacterPositions) {

								// String fontName = "Courier New";
								// g.setFont(new Font(fontName, Font.PLAIN, 24));
								//
								// FontMetrics metrics = g.getFontMetrics();

								// int fontSize = 13;
								int fontSize = -1;

								if (fullResolution) {
									fontSize = imageHeight / 100;
								} else {
									fontSize = displayScreenHeight / 100;
								}

								g.setFont(new Font("Courier", Font.PLAIN, fontSize));
								g.setColor(Color.RED);

								boolean showWords = true;
								boolean showChars = true;

								if (showWords) {
									for (int i = 0; i < numWords; i++) {

										// int size = (wordMaxY[i] - wordMinY[i]) / 4;

										// int widthAt24 = metrics.stringWidth(wordStrings[i]);
										// int fontSize = (int) (4.0 * ((double) wordMaxX[i] - (double) wordMinX[i]) / (double) widthAt24);

										// int x1 = (int) ((double) wordMinX[i] / imageWidth * (displayScreenWidth / 2));
										// int y1 = (int) ((double) (imageHeight - (0.80 * wordMinY[i] + 0.20 * wordMaxY[i])) / imageHeight * displayScreenHeight);

										int x1 = -1;
										int y1 = -1;

										if (fullResolution) {

											x1 = (int) ((double) wordMinX[i]);
											y1 = (int) ((double) (imageHeight - (0.80 * wordMinY[i] + 0.20 * wordMaxY[i])));

										} else {

											x1 = (int) ((double) wordMinX[i] / imageWidth * (displayScreenWidth / 2));
											y1 = (int) ((double) (imageHeight - (0.80 * wordMinY[i] + 0.20 * wordMaxY[i])) / imageHeight * displayScreenHeight);
										}

										int minWordLengthInCharacters = 0;
										if (wordStrings[i].length() < minWordLengthInCharacters)
											continue;

										boolean overlay = false;

										int xOffset = -1;
										if (overlay) {
											xOffset = 0;
										} else {
											xOffset = (displayScreenWidth / 2);
										}

										g.drawString(wordStrings[i], x1 + xOffset, y1);

										Thread.sleep(wordDelayTimeInMS);
									}

									Color lastColor = g.getColor();
								}

								if (showChars) {
									int numChars = textChars.length;
									for (int i = 0; i < numChars; i++) {

										int x1 = -1;
										int y1 = -1;

										if (fullResolution) {
											x1 = (int) ((double) textCharX1[i]);
											y1 = (int) ((double) (imageHeight - (1.0 * textCharY1[i] + 0.00 * textCharY2[i])));

										} else {
											x1 = (int) ((double) textCharX1[i] / imageWidth * (displayScreenWidth / 2));
											y1 = (int) ((double) (imageHeight - (1.0 * textCharY1[i] + 0.00 * textCharY2[i])) / imageHeight * displayScreenHeight);
										}

										// int size = (wordMaxY[i] - wordMinY[i]) / 4;

										// int widthAt24 = metrics.stringWidth(wordStrings[i]);
										// int fontSize = (int) (4.0 * ((double) wordMaxX[i] - (double) wordMinX[i]) / (double) widthAt24);

										// int x1 = (int) ((double) wordMinX[i] / imageWidth * (displayScreenWidth / 2));
										// int y1 = (int) ((double) (imageHeight - (0.80 * wordMinY[i] + 0.20 * wordMaxY[i])) / imageHeight * displayScreenHeight);

										boolean overlay = true;

										int xOffset = -1;
										if (overlay) {
											xOffset = 0;
										} else {
											xOffset = (displayScreenWidth / 2);
										}

										g.drawChars(textChars, i, 1, x1 + xOffset, y1);

										Thread.sleep(wordDelayTimeInMS);
									}

									Color lastColor = g.getColor();
								}
							}

							g.setColor(Color.RED);

							/* draw marked regions on image */

							double[] randomOffsets = new double[6];

							if (expertRatingMode) {

								int choiceIndex = 0;
								for (int yPlanelIndex = 0; yPlanelIndex < 3; yPlanelIndex++) {
									for (int xPlanelIndex = 0; xPlanelIndex < 2; xPlanelIndex++) {

										double randomTitleProbabilityOffset = (titleOffsetRandom.nextDouble() * 2.0 - 1.0) * 0.1;
										System.out.println("randomTitleProbabilityOffset = " + randomTitleProbabilityOffset);

										randomOffsets[choiceIndex] = randomTitleProbabilityOffset;

										boolean[] markSegments = new boolean[reducedGridSize];

										for (int exampleIndex = 0; exampleIndex < reducedGridSize; exampleIndex++) {

											int exampleY = exampleIndex % reducedGridSize;

											if (exampleY < halfYWindowSize)
												continue;
											if (exampleY - halfYWindowSize + (yWindowSize - 1) >= reducedGridSize)
												continue;

											/* find predicted class */

											for (int j = 0; j < numClasses; j++) {
												classWeights[j] = exampleOutputPredictions[batchImageIndex * reducedGridSize + exampleIndex * numClasses + j];
											}
											boolean hasNAN = false;
											for (int j = 0; j < numClasses; j++) {
												if (Double.isNaN(classWeights[j]))
													hasNAN = true;
											}
											if (hasNAN) {
												System.out.println("hasNAN");
												classWeights[0] = 1;
												classWeights[1] = 0;
											}

											if (true) {
												classWeights[0] -= (titleProbabilityOffset + randomTitleProbabilityOffset /* - blackWeight * averageValue */);
												classWeights[1] += (titleProbabilityOffset + randomTitleProbabilityOffset /* + blackWeight * averageValue */);
											}

											if (false) {
												for (int j = 0; j < numClasses; j++) {
													System.out.println("classWeights[" + j + "] = " + classWeights[j]);
												}
											}

											/* determine predicted class */

											int predictedClass = -1;
											double bestPredictedWeight = Double.NEGATIVE_INFINITY;
											for (int j = 0; j < numClasses; j++) {
												if (classWeights[j] > bestPredictedWeight) {
													bestPredictedWeight = classWeights[j];
													predictedClass = j;
												}
											}

											if (predictedClass == 1) {

												markSegments[exampleIndex] = true;

											}

										}

										for (int exampleIndex = 0; exampleIndex < reducedGridSize; exampleIndex++) {

											if (markSegments[exampleIndex]) {

												// int startSegmentIndex = exampleIndex % reducedGridSize;
												int numSegments = 1;
												while (true) {
													if (exampleIndex + numSegments >= reducedGridSize)
														break;
													if (markSegments[exampleIndex + numSegments]) {
														numSegments++;
													} else {
														break;
													}
												}

												int testImageIndex = exampleIndex / reducedGridSize;
												int testImageY = exampleIndex % reducedGridSize;

												int fileIndex = testImageIndex;
												float x1 = 0.0f;
												float y1 = (float) testImageY / reducedGridSize;
												float x2 = 1.0f;
												float y2 = (float) (testImageY + numSegments) / reducedGridSize;

												System.out.println(fileIndex + "\t" + classWeights[0] + "\t" + classWeights[1] + "\t" + y1 + "\t" + y2);
												System.out.println(titleProbabilityOffset);

												int dx1 = (displayScreenWidth / 2) + thumbNailSizeX * xPlanelIndex;
												int dy1 = thumbNailSizeY * yPlanelIndex;

												// int dx2 = (displayScreenWidth / 2) + thumbNailSizeX * (xPlanelIndex + 1);
												// int dy2 = thumbNailSizeY * (yPlanelIndex + 1);

												g.fillRect((int) (dx1 + x1 * thumbNailSizeX), (int) (dy1 + y1 * thumbNailSizeY), thumbNailSizeX - 1, (int) ((y2 - y1) * thumbNailSizeY) /*
																																														 * displayScreenHeight / reducedGridSize
																																														 */);

												exampleIndex += numSegments;
											}

										}
										choiceIndex++;
									}
								}

							} else {

								boolean[] markSegments = new boolean[reducedGridSize];

								for (int exampleIndex = 0; exampleIndex < reducedGridSize; exampleIndex++) {

									int exampleY = exampleIndex % reducedGridSize;

									if (exampleY < halfYWindowSize)
										continue;
									if (exampleY - halfYWindowSize + (yWindowSize - 1) >= reducedGridSize)
										continue;

									/* find predicted class */

									for (int j = 0; j < numClasses; j++) {
										classWeights[j] = exampleOutputPredictions[batchImageIndex * reducedGridSize + exampleIndex * numClasses + j];
									}
									boolean hasNAN = false;
									for (int j = 0; j < numClasses; j++) {
										if (Double.isNaN(classWeights[j]))
											hasNAN = true;
									}
									if (hasNAN) {
										System.out.println("hasNAN");
										classWeights[0] = 1;
										classWeights[1] = 0;
									}

									if (true) {
										classWeights[0] -= (titleProbabilityOffset /* - blackWeight * averageValue */);
										classWeights[1] += (titleProbabilityOffset /* + blackWeight * averageValue */);
									}

									if (false) {
										for (int j = 0; j < numClasses; j++) {
											System.out.println("classWeights[" + j + "] = " + classWeights[j]);
										}
									}

									/* determine predicted class */

									int predictedClass = -1;
									double bestPredictedWeight = Double.NEGATIVE_INFINITY;
									for (int j = 0; j < numClasses; j++) {
										if (classWeights[j] > bestPredictedWeight) {
											bestPredictedWeight = classWeights[j];
											predictedClass = j;
										}
									}

									if (predictedClass == 1) {

										markSegments[exampleIndex] = true;

									}

								}

								for (int exampleIndex = 0; exampleIndex < reducedGridSize; exampleIndex++) {

									if (markSegments[exampleIndex]) {

										// int startSegmentIndex = exampleIndex % reducedGridSize;
										int numSegments = 1;
										while (true) {
											if (exampleIndex + numSegments >= reducedGridSize)
												break;
											if (markSegments[exampleIndex + numSegments]) {
												numSegments++;
											} else {
												break;
											}
										}

										int testImageIndex = exampleIndex / reducedGridSize;
										int testImageY = exampleIndex % reducedGridSize;

										int fileIndex = testImageIndex;
										float x1 = 0.0f;
										float y1 = (float) testImageY / reducedGridSize;
										float x2 = 1.0f;
										float y2 = (float) (testImageY + numSegments) / reducedGridSize;

										System.out.println(fileIndex + "\t" + classWeights[0] + "\t" + classWeights[1] + "\t" + y1 + "\t" + y2);
										System.out.println(titleProbabilityOffset);

										g.drawRect((int) (x1 * displayScreenWidth / 2 + displayScreenWidth / 2), (int) (y1 * displayScreenHeight), displayScreenWidth / 2 - 1, (int) ((y2 - y1) * displayScreenHeight) /* displayScreenHeight / reducedGridSize */);

										exampleIndex += numSegments;
									}

								}
							}

							if (expertRatingMode) {

								imageSelected = false;
								while (!imageSelected) {

									Thread.sleep(1);
									//
									// System.out.println("waiting for selection\n");
									//
									// System.out.println("imageSelected = " + imageSelected);

								}

								int choiceIndex = -1;
								{
									System.out.println("mousePressX = " + mousePressX);
									System.out.println("mousePressY = " + mousePressY);

									choiceIndex = 0;
									for (int yPlanelIndex = 0; yPlanelIndex < 3; yPlanelIndex++) {
										for (int xPlanelIndex = 0; xPlanelIndex < 2; xPlanelIndex++) {

											int dx1 = (displayScreenWidth / 2) + thumbNailSizeX * xPlanelIndex;
											int dy1 = thumbNailSizeY * yPlanelIndex;

											int dx2 = (displayScreenWidth / 2) + thumbNailSizeX * (xPlanelIndex + 1);
											int dy2 = thumbNailSizeY * (yPlanelIndex + 1);

											if ((mousePressX >= dx1) && (mousePressX <= dx2) && (mousePressY >= dy1) && (mousePressY <= dy2)) {

												g.setColor(Color.GREEN);
												g.fillRect(dx1, dy1, dx2 - dx1, dy2 - dy1);
												Thread.sleep(1000);

												break;
											}

											choiceIndex++;

										}

									}

								}

								System.out.println("choiceIndex = " + choiceIndex);

								IO.createEmptyFile("ratings" + "_" + expertName + "_" + unseenImageIndex + "_" + System.currentTimeMillis() + "_" + randomOffsets[choiceIndex] + "_" + choiceIndex);
							}

							// mainFrame.getGraphics().setColor(lastColor);

							if (clickToProceed) {

								mousePressed = false;
								keyPressed = false;
								while (!mousePressed && !keyPressed) {
									Thread.sleep(1);
									continue;
								}

							} else {

								Thread.sleep(applyBiasShowImagesDwellTime * 1000);
							}
						}

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

			}

			/* analyze classification performance */

			for (int i = 0; i < 2; i++) {

				String predictedString = null;

				if (i == 0)
					predictedString = "text ";
				else
					predictedString = "title";

				for (int j = 0; j < 2; j++) {

					String actualString = null;

					if (j == 0)
						actualString = "text ";
					else
						actualString = "title";

					System.out.println("predicted : " + predictedString + "  actual : " + actualString + "  count = " + classificationDistributionMatrix[i][j]);

				}

			}

			long currentTime = System.currentTimeMillis();
			double duration = (currentTime - startTime) / 1000.0;

			double actualTextFraction = ((double) classificationDistributionMatrix[0][0] + (double) classificationDistributionMatrix[1][0])
					/ (classificationDistributionMatrix[0][0] + classificationDistributionMatrix[0][1] + classificationDistributionMatrix[1][0] + classificationDistributionMatrix[1][1]);
			double actualTitleFraction = ((double) classificationDistributionMatrix[0][1] + (double) classificationDistributionMatrix[1][1])
					/ (classificationDistributionMatrix[0][0] + classificationDistributionMatrix[0][1] + classificationDistributionMatrix[1][0] + classificationDistributionMatrix[1][1]);

			double predictedTextFraction = ((double) classificationDistributionMatrix[0][0] + (double) classificationDistributionMatrix[0][1])
					/ (classificationDistributionMatrix[0][0] + classificationDistributionMatrix[0][1] + classificationDistributionMatrix[1][0] + classificationDistributionMatrix[1][1]);
			double predictedTitleFraction = ((double) classificationDistributionMatrix[1][0] + (double) classificationDistributionMatrix[1][1])
					/ (classificationDistributionMatrix[0][0] + classificationDistributionMatrix[0][1] + classificationDistributionMatrix[1][0] + classificationDistributionMatrix[1][1]);

			accuracy = ((double) classificationDistributionMatrix[0][0] + (double) classificationDistributionMatrix[1][1])
					/ (classificationDistributionMatrix[0][0] + classificationDistributionMatrix[0][1] + classificationDistributionMatrix[1][0] + classificationDistributionMatrix[1][1]);

			double baseLineAccuracy = ((double) classificationDistributionMatrix[0][0] + (double) classificationDistributionMatrix[1][0])
					/ (classificationDistributionMatrix[0][0] + classificationDistributionMatrix[0][1] + classificationDistributionMatrix[1][0] + classificationDistributionMatrix[1][1]);

			double deltaBaselineAccuracy = accuracy - baseLineAccuracy;

			double predictedTextAccuracy = ((double) classificationDistributionMatrix[0][0]) / (classificationDistributionMatrix[0][0] + classificationDistributionMatrix[0][1]);
			double predictedTitleAccuracy = ((double) classificationDistributionMatrix[1][1]) / (classificationDistributionMatrix[1][1] + classificationDistributionMatrix[1][0]);

			double textPredicivePower = predictedTextAccuracy / actualTextFraction;
			double titlePredicivePower = predictedTitleAccuracy / actualTitleFraction;

			int truePositive = classificationDistributionMatrix[1][1];
			int trueNegative = classificationDistributionMatrix[0][0];
			int falsePositive = classificationDistributionMatrix[1][0];
			int falseNegative = classificationDistributionMatrix[0][1];

			double recall = (double) truePositive / ((double) truePositive + (double) falseNegative);
			double precision = (double) truePositive / ((double) truePositive + (double) falsePositive);

			System.out.println("duration (s)           = " + duration);
			System.out.println("duration (h)           = " + duration / 3600);
			System.out.println("duration (d)           = " + duration / 3600 / 24);

			if (mode == OPTIMIZE_BIAS) {

				System.out.println("BIAS: optimizerTrueRandom           = " + optimizerTrueRandom);
				System.out.println("BIAS: optimizerRandomSeed           = " + optimizerRandomSeed);
				System.out.println("BIAS: xSmoothingWindowSize          = " + optimizerXSmoothingWindowSize);
				System.out.println("BIAS: optimizerYWindowSize          = " + yWindowSize);
				System.out.println("BIAS: reducedGridSize               = " + reducedGridSize);
				System.out.println("BIAS: distanceWeightingPower        = " + distanceWeightingPower);
				System.out.println("BIAS: minDiff                       = " + minDiff);
				System.out.println("BIAS: targetNumSamples              = " + targetNumSamples);
				System.out.println("BIAS: optimizerYSmoothingWindowSize = " + yWindowSize);
				System.out.println("BIAS: blackWeight                   = " + blackWeight);
				System.out.println("BIAS: titleProbabilityOffset        = " + titleProbabilityOffset);
				System.out.println("BIAS: leftIgnoreFraction            = " + leftIgnoreFraction);
				System.out.println("BIAS: rightIgnoreFraction           = " + rightIgnoreFraction);

				System.out.println("actualTextFraction     = " + actualTextFraction);
				System.out.println("actualTitleFraction    = " + actualTitleFraction);
				System.out.println("predictedTextFraction  = " + predictedTextFraction);
				System.out.println("predictedTitleFraction = " + predictedTitleFraction);
				System.out.println("accuracy               = " + accuracy);
				System.out.println("predictedTextAccuracy  = " + predictedTextAccuracy);
				System.out.println("predictedTitleAccuracy = " + predictedTitleAccuracy);
				System.out.println("baseLineAccuracy       = " + baseLineAccuracy);
				System.out.println("deltaBaselineAccuracy  = " + deltaBaselineAccuracy);
				System.out.println("textPredicivePower     = " + textPredicivePower);
				System.out.println("titlePredicivePower    = " + titlePredicivePower);

				System.out.println("truePositive    = " + truePositive);
				System.out.println("trueNegative    = " + trueNegative);
				System.out.println("falsePositive   = " + falsePositive);
				System.out.println("falseNegative   = " + falseNegative);
				System.out.println("recall          = " + recall);
				System.out.println("precision       = " + precision);

				// recall = tp / (tp + fn)
				// precision = tp / (tp + fp)

				double targetRecall = 0.50;
				double targetPrecision = 0.90;
				double diff1 = Math.abs(recall - targetRecall);
				double diff2 = Math.abs(precision - targetPrecision);
				double distance = Math.sqrt(diff1 * diff1 + diff2 * diff2);

				double performance = 1.0 - distance;

				if (performance > bestPerformance) {

					bestPerformance = performance;
					bestPerformanceRecall = recall;
					bestPerformancePrecision = precision;

					for (int i = 0; i < numBiasDimensions; i++) {
						bestBiasValues[i] = biasValues[i];
					}

					// report new bias //
					for (int i = 0; i < numBiasDimensions; i++) {

						System.out.printf("NEW BEST BIAS: %s\t%f\n", biasNames[i], bestBiasValues[i]);

					}
					System.out.printf("NEW BEST PERFORMANCE: %f\n", bestPerformance);

				}

				// report new bias //
				for (int i = 0; i < numBiasDimensions; i++) {

					System.out.printf("BEST BIAS: %s\t%f\n", biasNames[i], bestBiasValues[i]);

				}
				System.out.printf("BEST PERFORMANCE: %f\t%f\t%f\n", bestPerformance, bestPerformanceRecall, bestPerformancePrecision);
				//

				System.out.print("EXPH");
				for (int i = 0; i < numBiasDimensions; i++) {
					System.out.print("\t" + biasNames[i]);
				}
				System.out.print("\t" + "performance" + "\t" + "recall" + "\t" + "precision");
				System.out.println();

				System.out.print("EXPD\t");
				for (int i = 0; i < numBiasDimensions; i++) {
					if (i > 0)
						System.out.print("\t");
					System.out.print(bestBiasValues[i]);
				}
				System.out.print("\t" + bestPerformance + "\t" + bestPerformanceRecall + "\t" + bestPerformancePrecision);
				System.out.println();

			}

			trialIndex++;

			if (mode == APPLY_BIAS) {

				// unseenImageToClassifyIndex += numTestImagesPerBatch;

				if (trialIndex == numImagesToRate)
					System.exit(0);
			}

			if ((mode == OPTIMIZE_BIAS) && (trialIndex == maxNumTrials))
				System.exit(0);

			if (stopAfterOneTrial)
				System.exit(0);
		}

		// TODO Auto-generated method stub

	}
}

class InstancedBasedPredictorFullCrossValidation {

	public static class InstancedBasedPredictorKernel extends Kernel {

		static boolean useSEQ = ClineTar.useSEQ;
		static boolean useJTP = ClineTar.useJTP;
		static boolean useGPU = ClineTar.useGPU;

		int numProblems;
		int numPasses;

		int mode;
		static int TRAINING_MODE = 0;
		static int TESTING_MODE = 1;

		int numTrainingExamples;
		int numTestingExamples;
		int numExamplesPerGroup;
		int numInputFeatues;
		int numOutputFeatures;

		int[] useExampleGroupForTraining;
		int[] useExampleGroupForTesting;

		float[] exampleInputValues;
		float[] exampleOutputValues;

		double[] exampleWeights;
		double[] exampleClassWeights;

		float minDiff;
		float distanceWeightingPower;
		int yWindowSize;
		int reducedGridSize;
		float leftIgnoreFraction;
		float rightIgnoreFraction;

		float[] exampleOutputPredictions;

		Range range;
		long StartTime;

		@Local
		// float[] exampleClassWeghtSums = new float[ClineTar.numClasses];
		public InstancedBasedPredictorKernel(
		//
				int numProblems,
				//
				int numPasses,
				//
				int mode,
				//
				int numTrainingExamples, int numTestingExamples, int numExamplesPerGroup, int numInputFeatues, int numOutputFeatures,
				//
				int[] useExampleGroupForTraining, int[] useExampleGroupForTesting,
				//
				float[] exampleInputValues, float[] exampleOutputValues, double[] exampleWeights, double[] exampleClassWeights,
				//
				float minDiff, float distanceWeightingPower, int yWindowSize, int reducedGridSize, float leftIgnoreFraction, float rightIgnoreFraction,

				//
				float[] exampleOutputPredictions
		//
		) {

			//
			// this.javaSimulationGPU = javaSimulationGPU;
			//
			this.numProblems = numProblems;
			this.numPasses = numPasses;
			//
			this.mode = mode;
			//
			this.numTrainingExamples = numTrainingExamples;
			this.numTestingExamples = numTestingExamples;
			this.numExamplesPerGroup = numExamplesPerGroup;
			this.numInputFeatues = numInputFeatues;
			this.numOutputFeatures = numOutputFeatures;
			//
			this.useExampleGroupForTraining = useExampleGroupForTraining;
			this.useExampleGroupForTesting = useExampleGroupForTesting;
			//
			this.exampleInputValues = exampleInputValues;
			this.exampleOutputValues = exampleOutputValues;
			this.exampleWeights = exampleWeights;
			this.exampleClassWeights = exampleClassWeights;
			//
			this.minDiff = minDiff;
			this.distanceWeightingPower = distanceWeightingPower;
			this.yWindowSize = yWindowSize;
			this.reducedGridSize = reducedGridSize;
			this.leftIgnoreFraction = leftIgnoreFraction;
			this.rightIgnoreFraction = rightIgnoreFraction;
			//
			this.exampleOutputPredictions = exampleOutputPredictions;
			//
			System.out.println("numProblems = " + numProblems);
			System.out.println("numExamples   = " + numTrainingExamples);
			System.out.println("numExamplesPerGroup   = " + numExamplesPerGroup);
			System.out.println("numInputFeatues   = " + numInputFeatues);
			System.out.println("numOutputFeatures   = " + numOutputFeatures);

			long sizeOfFloat = 4;
			long numBytes =
			/**/
			exampleInputValues.length * sizeOfFloat +
			/**/
			exampleOutputValues.length * sizeOfFloat +
			/**/
			exampleWeights.length * sizeOfFloat +
			/**/
			exampleClassWeights.length * sizeOfFloat +
			/**/
			exampleOutputPredictions.length * sizeOfFloat;

			System.out.println("numBytes (M)   = " + numBytes / 1e6);

			setExplicit(false); // This gives us a performance boost

		}

		@Override
		public void run() {

			int problemIndex = getGlobalId();

			if (problemIndex >= numProblems) {
				return;
			}

			int passIndex = getPassId();

			int numComputePasses = numPasses - 1;

			int testExampleIndex = problemIndex;
			int testExampleGroupIndex = testExampleIndex / numExamplesPerGroup;
			int testExampleY = testExampleIndex % numExamplesPerGroup;

			if (useExampleGroupForTesting[testExampleGroupIndex] == 0) {
				return;
			}

			int halfYWindowSize = yWindowSize / 2;

			if (testExampleY < halfYWindowSize) {
				return;
			}
			if (testExampleY - halfYWindowSize + (yWindowSize - 1) >= reducedGridSize) {
				return;
			}

			int testExampleStartValueIndex = testExampleIndex * numInputFeatues;

			if (passIndex == 0) {
				exampleWeights[testExampleIndex] = 0.0f;
				for (int i = 0; i < numOutputFeatures; i++) {
					exampleClassWeights[testExampleIndex * numOutputFeatures + i] = 0.0f;
				}
			}

			if (passIndex < numComputePasses) {

				/***************************************/
				/* do next pass of weight accumulation */
				/***************************************/

				int targetExamplesPerPass = (numTrainingExamples / numComputePasses);
				int trainStartIndex = targetExamplesPerPass * passIndex;
				int trainEndIndex = trainStartIndex + targetExamplesPerPass;
				if (trainEndIndex > numTrainingExamples) {
					trainEndIndex = numTrainingExamples;
				}

				for (int trainExampleIndex = trainStartIndex; trainExampleIndex < trainEndIndex; trainExampleIndex++) {

					int trainExampleGroupIndex = trainExampleIndex / numExamplesPerGroup;
					int trainExampleY = trainExampleIndex % numExamplesPerGroup;

					if (trainExampleY < halfYWindowSize) {
						continue;
					}
					if (trainExampleY - halfYWindowSize + (yWindowSize - 1) >= reducedGridSize) {
						continue;
					}
					if (useExampleGroupForTraining[trainExampleGroupIndex] == 0) {
						continue;
					}

					if (testExampleGroupIndex != trainExampleGroupIndex) {

						int testExampleValueIndex = testExampleStartValueIndex - halfYWindowSize * numInputFeatues;
						int trainExampleValueIndex = trainExampleIndex * numInputFeatues - halfYWindowSize * numInputFeatues;
						float diffSum = 0.0f;

						int numLeftToSkip = (int) (reducedGridSize * leftIgnoreFraction);
						int numRightToSkip = (int) (reducedGridSize * rightIgnoreFraction);
						int numToUse = reducedGridSize - numLeftToSkip - numRightToSkip;

						for (int i = 0; i < yWindowSize; i++) {

							testExampleValueIndex += numLeftToSkip;
							trainExampleValueIndex += numLeftToSkip;

							for (int v = 0; v < numToUse; v++) {

								diffSum += Math.abs(exampleInputValues[testExampleValueIndex++] - exampleInputValues[trainExampleValueIndex++]);

								// float diff = exampleInputValues[testExampleValueIndex++] - exampleInputValues[trainExampleValueIndex++];
								// if (diff < 0)
								// diffSum -= diff;
								// else
								// diffSum += diff;
							}

							testExampleValueIndex += numRightToSkip;
							trainExampleValueIndex += numRightToSkip;

						}

						float diff = diffSum / (numToUse * yWindowSize);

						if (diff < minDiff) {

							if (useSEQ) {
								// System.out.println("Warning! diff = " + diff + " setting to " + minDiff);
							}
							diff = minDiff;
						}

						double weight = (double) (1.0f / Math.pow(diff, distanceWeightingPower));

						if (useSEQ) {

							if (Float.isNaN(diff) || Double.isNaN(weight)) {

								System.out.println("Error! diff = " + diff);
								System.out.println("Error! weight = " + weight);
								new Exception().printStackTrace();
								System.exit(1);
							}
						}

						// weightSum += weight;
						exampleWeights[testExampleIndex] += weight;

						for (int i = 0; i < numOutputFeatures; i++) {
							exampleClassWeights[testExampleIndex * numOutputFeatures + i] += weight * exampleOutputValues[trainExampleIndex * numOutputFeatures + i];
						}

					}

				}
			} else {

				/**************************************/
				/* do pass of weight comparison */
				/**************************************/

				for (int i = 0; i < numOutputFeatures; i++) {

					exampleOutputPredictions[testExampleIndex * numOutputFeatures + i] = (float) (exampleClassWeights[testExampleIndex * numOutputFeatures + i] / exampleWeights[testExampleIndex]);

					if (useSEQ) {

						if (Float.isNaN(exampleOutputPredictions[testExampleIndex * numOutputFeatures + i])) {

							System.out.println("Error! exampleWeights[testExampleIndex] = " + exampleWeights[testExampleIndex]);
							System.out.println("Error! exampleOutputPredictions = " + exampleOutputPredictions[testExampleIndex * numOutputFeatures + i]);
							new Exception().printStackTrace();
							System.exit(1);
						}
					}

				}

			}

		}

		public void computePredictions(int numProblemsPerPass) {

			// int numProblemsPerPass = -1;
			//
			// if (mode == ClineTar.APPLY_BIAS) {
			// numProblemsPerPass = ClineTar.numTestImagesPerBatch * reducedGridSize;
			// } else {
			// numProblemsPerPass = numTrainingExamples;
			// }

			int groupSize = -1;

			if (useSEQ)
				groupSize = 1;
			else
				groupSize = ClineTar.aparapiGroupSize;

			int expandedNumProblemsPerPass = numProblemsPerPass / groupSize * groupSize;
			if (expandedNumProblemsPerPass < numProblemsPerPass) {
				expandedNumProblemsPerPass += groupSize;
			}

			System.out.println("expandedNumProblemsPerPass = " + expandedNumProblemsPerPass);

			range = Range.create(expandedNumProblemsPerPass, groupSize);

			System.out.println("range = " + range);
			System.out.println("range.getWorkGroupSize() = " + range.getWorkGroupSize());

			execute(range, numPasses);

		}

	}

	static boolean running = false;

}

class ClineKeyListener implements KeyListener {

	@Override
	public void keyPressed(KeyEvent arg0) {

		ClineTar.keyPressed = true;

		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

}

class ClineMouseListener implements MouseListener {

	// ClineMouseListener() {
	// }

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		System.out.println("MouseEvent = " + e);

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

		ClineTar.mousePressed = true;

		System.out.println("MouseEvent = " + e);
		System.out.println("getButton = " + e.getButton());

		ClineTar.mousePressX = e.getX();
		ClineTar.mousePressY = e.getY();
		ClineTar.imageSelected = true;

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		System.out.println("MouseEvent = " + e);

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		System.out.println("MouseEvent = " + e);
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		System.out.println("MouseEvent = " + e);
		// TODO Auto-generated method stub

	}
}

// todo rank order value normalization