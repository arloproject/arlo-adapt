package adapt;

/*
 * @(#)Dash.java	1.4 98/12/03
 *
 * Copyright 1998 by Sun Microsystems, Inc.,
 * 901 San Antonio Road, Palo Alto, California, 94303, U.S.A.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information
 * of Sun Microsystems, Inc. ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with Sun.
 */

import ij.plugin.ScreenGrabber;

import java.awt.*;
import java.awt.geom.*;
import java.awt.event.*;
import java.awt.font.TextLayout;
import java.awt.font.FontRenderContext;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import javax.print.attribute.standard.MediaSize.Other;
import javax.swing.*;

public class ClusterVis extends JApplet {

	double dataValueOffset = 0.0;

	final boolean t = true;
	final boolean f = false;
	final long numMSPerHour = 3600L * 1000L;
	final long numMSPerDay = numMSPerHour * 24L;

	boolean computeAccuracyMode;
	boolean createBehaviorTranscriptMode;
	boolean createTimeColoredCluster;
	boolean showClusterDensity;
	boolean createBehaviorColoredCluster;
	boolean createTimeOfDayBehaviorColoredCluster;

	boolean isMonkey;
	boolean isAntEater;
	boolean isFisher;
	boolean isAlabatross;
	boolean isOilBird;
	boolean isSwallowTail;
	boolean isTortoise;

	long timeShift;

	boolean useEuclideanDistance;
	boolean useHammingDistance;

	String examplesFilePath;
	int T_INDEX;
	int DATA_INDEX;

	double distanceWeightingPower;
	double distanceWeightingPowerStart;
	double distanceWeightingPowerEnd;
	double distanceWeightingPowerIncrement;

	double distancePower;
	double distancePowerStart;
	double distancePowerEnd;
	double distancePowerIncrement;

	//

	int X_INDEX;
	int Y_INDEX;
	int Z_INDEX;

	// for visual transcript
	int screenXSize;
	int screenYSize;
	int screenWidth;
	int screenHeight;

	double xOffset = 0.0;
	double yOffset = 0.0;
	double xZoom = 1.0;
	double yZoom = 1.0;

	//

	boolean whiteBackground;
	boolean blackBackground;

	//

	boolean xAxisIsTime;
	boolean xAxisIsTimeOfDay;
	boolean xAxisIsXplusY;

	boolean yAxisIsTimeOfDay;

	//

	boolean showAllExamples;

	int allExamplesPointXWidth;
	int allExamplesPointYWidth;

	boolean colorAllExamplesByCoordinates;
	boolean colorAllExamplesByPredictedClass;
	boolean colorTimeOfDay;
	int numTimeColors;
	boolean colorByIntensity;

	boolean useLogIntensity;
	boolean useLinearIntensity;
	boolean useBinaryIntensity;

	//

	boolean showGroundTruthExamples;

	int groundTruthPointWidth;

	int numClasses;
	boolean useClassColoredGroundTruth;
	boolean useClassNumbers;
	boolean useBlackGroundTruth;

	//

	//

	private void setParameters() {

		isMonkey = t;
		// isAntEater = t;
		// isFisher = t;
		// isAlabatross = t;
		// isOilBird = t;
		// isSwallowTail = t;
		// isTortoise = t;

//		computeAccuracyMode = t;
		// showClusterDensity = t;
		// createTimeColoredCluster = t;
//		 createBehaviorColoredCluster = t;
		 createBehaviorTranscriptMode = t;
		// xOffset = 0.0;
		// xZoom = 1.0;
		// createTimeOfDayBehaviorColoredCluster = t;

		String focusFile = null;

		if (isMonkey) {
			// focusFile = "/d250/move/mo2";
			// focusFile = "/d250/move/mo.2d.ns";

			// focusFile = "/d250/move/mo4";
			timeShift = 12L * 3600L * 1000L;

			// 10 class
			// distanceWeightingPower = 0.5;
			// distancePower = 1.0;
			// numClasses = 10;

			// 3 class, 3D
			// focusFile = "/d250/move/mo.best";
			// distanceWeightingPower = 2.0;
			// distancePower = 1.3;
			// numClasses = 3;

			// 3 class, 3D
			focusFile = "/d250/move/mo4";
			distanceWeightingPower = 2.8;
			distancePower = 1.5;
			numClasses = 3;

			// 3 class, 162D
			// focusFile = "/d250/move/rawMonkey.dat";
			// distanceWeightingPower = 2.8;
			// distancePower = 0.4;
			// numClasses = 3;

			// neighest neighbor
			// distanceWeightingPower = 100;
			// distancePower = 1.0;
		}

		if (isAntEater) {
			// focusFile = "/d250/move/ae.1d.ns";
			// focusFile = "/d250/move/ae.2d.ns";
			focusFile = "/d250/move/ant";
			timeShift = 13L * 3600L * 1000L;
		}

		if (isFisher) {
			focusFile = "/d250/move/fi";
			timeShift = -1L * 3600L * 1000L;
			// xOffset = 0.865;
			// xOffset = 0.26;
			// xZoom = 8.0;
			// xZoom = 14.0;
		}
		if (isOilBird) {
			// focusFile = "/d250/move/ob.1d.1k.e";
			// focusFile = "/d250/move/ob.2d.1k.e";
			focusFile = "/d250/move/ob";
			timeShift = -4L * 3600L * 1000L;
		}

		if (isAlabatross) {
			focusFile = "/d250/move/al";
			timeShift = -6L * 3600L * 1000L;
		}
		if (isSwallowTail) {
			focusFile = "/d250/move/st";
			timeShift = -6L * 3600L * 1000L;
		}

		if (isTortoise) {
			focusFile = "/d250/move/to";
			timeShift = -6L * 3600L * 1000L;
		}

		if (computeAccuracyMode) {

			useEuclideanDistance = t;
			useHammingDistance = f;

			//

			if (f) {
				examplesFilePath = "/d250/move/monkey.dat";
				T_INDEX = 0;
				DATA_INDEX = 1;

				distanceWeightingPower = 3.0; // orig 10 class 35.20%
				distanceWeightingPower = 2.8; // new 3 class
				distanceWeightingPowerStart = 2.8; // 3.0 35.20%
				distanceWeightingPowerEnd = 2.8;
				distanceWeightingPowerIncrement = 0.1;

				distancePower = 112;
				distancePower = 0.4;// new 3 class
				distancePowerStart = 0.4; // 112 35.20%
				distancePowerEnd = 0.4; // 86 max before failure of performance measure
				distancePowerIncrement = 0.1;
			} else {
				examplesFilePath = focusFile;
				T_INDEX = 2;
				DATA_INDEX = 3;

				distanceWeightingPower = Double.NaN;
				distanceWeightingPowerStart = 2.8;
				distanceWeightingPowerEnd = distanceWeightingPowerStart;
//				 distanceWeightingPowerStart = 0.1;
//				 distanceWeightingPowerEnd = 100;
				distanceWeightingPowerIncrement = 0.1;

				distancePower = Double.NaN;
				distancePowerStart = 1.5;
				distancePowerEnd = distancePowerStart;
				 distancePowerStart = 0.0;
				 distancePowerEnd = 200.0;
				distancePowerIncrement = 0.1;
			}

			//

			X_INDEX = 0;
			Y_INDEX = 1;
			Z_INDEX = 2;

		}

		if (createBehaviorTranscriptMode) {

			yOffset = 0.0;
			yZoom = 1.0;

			useEuclideanDistance = t;
			useHammingDistance = f;

			//

			//

			examplesFilePath = focusFile;
			T_INDEX = 2;
			DATA_INDEX = 3;

			//

			X_INDEX = 0;
			Y_INDEX = 1;

			// for visual transcript
			// screenXSize = 1550;
			// screenYSize = 1100;
			screenXSize = 1100;
			screenYSize = 700;
			screenWidth = screenXSize;
			screenHeight = screenYSize;

			//

			whiteBackground = f;
			blackBackground = !whiteBackground;

			//
			xAxisIsTime = t;
			xAxisIsTimeOfDay = f;
			yAxisIsTimeOfDay = t;
			//
			showAllExamples = t;

			allExamplesPointXWidth = Math.max((int) xZoom * 1, 1);
			allExamplesPointYWidth = Math.max((int) yZoom * 1, 1);
			if (isMonkey) {
				allExamplesPointXWidth = Math.max((int) xZoom * 12, 1);
				allExamplesPointYWidth = Math.max((int) yZoom * 2, 1);
			} else if (isAntEater) {
				allExamplesPointXWidth = Math.max((int) xZoom * 60, 1);
				allExamplesPointYWidth = Math.max((int) yZoom * 3, 1);
			} else if (isFisher) {
				allExamplesPointXWidth = Math.max((int) xZoom * 3, 1);
				allExamplesPointYWidth = Math.max((int) yZoom * 4, 1);
			} else if (isAlabatross) {
				allExamplesPointXWidth = Math.max((int) xZoom * 80, 1);
				allExamplesPointYWidth = Math.max((int) yZoom * 3, 1);
			} else if (isOilBird) {
				allExamplesPointXWidth = Math.max((int) xZoom * 7, 1);
				allExamplesPointYWidth = Math.max((int) yZoom * 3, 1);
			} else if (isSwallowTail) {
				allExamplesPointXWidth = Math.max((int) xZoom * 7, 1);
				allExamplesPointYWidth = Math.max((int) yZoom * 3, 1);
			} else if (isTortoise) {
				allExamplesPointXWidth = Math.max((int) xZoom * 7, 1);
				allExamplesPointYWidth = Math.max((int) yZoom * 4, 1);
			}

			colorAllExamplesByCoordinates = f;
			colorAllExamplesByPredictedClass = t;
			colorTimeOfDay = f;
			numTimeColors = 3;
			colorByIntensity = f;
			useLogIntensity = t;
			useLinearIntensity = f;
			useBinaryIntensity = f;
			//
			showGroundTruthExamples = f;
			groundTruthPointWidth = 10;
			useClassColoredGroundTruth = f;
			useClassNumbers = f;
			useBlackGroundTruth = !useClassColoredGroundTruth;
			//
		}

		if (createTimeColoredCluster) {

			useEuclideanDistance = t;
			useHammingDistance = f;
			//

			examplesFilePath = focusFile;
			T_INDEX = 2;
			DATA_INDEX = 3;

			//

			X_INDEX = 0;
			Y_INDEX = 1;

			screenXSize = 700;
			screenYSize = 700;
			screenWidth = screenXSize;
			screenHeight = screenYSize;

			xOffset = 0.0;
			yOffset = 0.0;
			xZoom = 1.0;
			yZoom = 1.0;

			//

			whiteBackground = f;
			blackBackground = !whiteBackground;

			//

			xAxisIsTime = f;
			xAxisIsTimeOfDay = f;

			yAxisIsTimeOfDay = f;

			//

			showAllExamples = t;

			// for visual transcript

			allExamplesPointXWidth = 2;
			allExamplesPointYWidth = 2;

			colorAllExamplesByCoordinates = f;
			colorTimeOfDay = t;
			numTimeColors = 3;
			colorByIntensity = f;

			useLogIntensity = t;
			useLinearIntensity = f;
			useBinaryIntensity = f;

			//

			showGroundTruthExamples = f;

			groundTruthPointWidth = 8;

			useClassColoredGroundTruth = t;
			useClassNumbers = f;
			useBlackGroundTruth = !useClassColoredGroundTruth;

			//
		}

		if (showClusterDensity) {

			useEuclideanDistance = t;
			useHammingDistance = f;
			//

			examplesFilePath = focusFile;
			T_INDEX = 2;
			DATA_INDEX = 3;

			//

			X_INDEX = 0;
			Y_INDEX = 1;

			screenXSize = 700;
			screenYSize = 700;
			screenWidth = screenXSize;
			screenHeight = screenYSize;

			xOffset = 0.0;
			yOffset = 0.0;
			xZoom = 1.0;
			yZoom = 1.0;

			//

			whiteBackground = f;
			blackBackground = !whiteBackground;

			//

			xAxisIsTime = f;
			xAxisIsTimeOfDay = f;

			yAxisIsTimeOfDay = f;

			//

			showAllExamples = t;

			// for visual transcript

			allExamplesPointXWidth = 2;
			allExamplesPointYWidth = 2;

			colorAllExamplesByCoordinates = f;
			colorTimeOfDay = f;
			numTimeColors = 3;
			colorByIntensity = t;

			useLogIntensity = t;
			useLinearIntensity = f;
			useBinaryIntensity = f;

			//

			showGroundTruthExamples = f;

			groundTruthPointWidth = 8;

			useClassColoredGroundTruth = t;
			useClassNumbers = f;
			useBlackGroundTruth = !useClassColoredGroundTruth;

			//
		}

		if (createBehaviorColoredCluster) {

			useEuclideanDistance = t;
			useHammingDistance = f;
			//

			examplesFilePath = focusFile;
			T_INDEX = 2;
			DATA_INDEX = 3;

			//

			X_INDEX = 0;
			Y_INDEX = 1;

			screenXSize = 700;
			screenYSize = 700;
			screenWidth = screenXSize;
			screenHeight = screenYSize;

			xOffset = 0.0;
			yOffset = 0.0;
			xZoom = 1.0;
			yZoom = 1.0;

			//

			whiteBackground = f;
			blackBackground = !whiteBackground;

			//

			xAxisIsTime = f;
			xAxisIsTimeOfDay = f;

			yAxisIsTimeOfDay = f;

			//

			showAllExamples = t;

			// for visual transcript

			allExamplesPointXWidth = 2;
			allExamplesPointYWidth = 2;

			colorAllExamplesByCoordinates = t;
			colorTimeOfDay = f;
			numTimeColors = 3;
			colorByIntensity = f;

			useLogIntensity = t;
			useLinearIntensity = f;
			useBinaryIntensity = f;

			//

			showGroundTruthExamples = t;

			groundTruthPointWidth = 4;

			useClassColoredGroundTruth = t;
			useClassNumbers = f;
			useBlackGroundTruth = !useClassColoredGroundTruth;

			//
		}

		if (createTimeOfDayBehaviorColoredCluster) {

			useEuclideanDistance = t;
			useHammingDistance = f;
			//

			examplesFilePath = focusFile;
			T_INDEX = 2;
			DATA_INDEX = 3;

			//

			X_INDEX = 0;
			Y_INDEX = 1;

			screenXSize = 700;
			screenYSize = 700;
			screenWidth = screenXSize;
			screenHeight = screenYSize;

			xOffset = 0.0;
			yOffset = 0.0;
			xZoom = 1.0;
			yZoom = 1.0;

			//

			whiteBackground = t;
			blackBackground = !whiteBackground;

			//

			xAxisIsTime = f;
			xAxisIsTimeOfDay = f;
			xAxisIsXplusY = t;

			yAxisIsTimeOfDay = t;

			//

			showAllExamples = t;

			// for visual transcript

			allExamplesPointXWidth = 4;
			allExamplesPointYWidth = 4;

			colorAllExamplesByCoordinates = t;
			colorTimeOfDay = f;
			numTimeColors = 3;
			colorByIntensity = f;

			useLogIntensity = t;
			useLinearIntensity = f;
			useBinaryIntensity = f;

			//

			showGroundTruthExamples = t;

			groundTruthPointWidth = 8;

			useClassColoredGroundTruth = f;
			useClassNumbers = f;
			useBlackGroundTruth = !useClassColoredGroundTruth;

			//
		}

	}

	public ClusterVis() {

		setParameters();

		loadData();

		if (computeAccuracyMode) {
			computeClassificationAccuracy();
			System.exit(0);
		}

		if (showAllExamples && colorAllExamplesByPredictedClass) {
			classifyAllExmples();
		}

		if (showAllExamples || showGroundTruthExamples) {

			JFrame f = new JFrame("MovementClusterVis");

			f.getContentPane().add("Center", this);
			f.pack();
			f.setSize(new Dimension(screenWidth + 10, screenHeight + 30));
			f.setAlwaysOnTop(true);
			f.setVisible(true);
			f.show();

		}
	}

	private void computeClassificationAccuracy() {

		for (distanceWeightingPower = distanceWeightingPowerStart; distanceWeightingPower <= distanceWeightingPowerEnd; distanceWeightingPower += distanceWeightingPowerIncrement) {

			for (distancePower = distancePowerStart; distancePower <= distancePowerEnd; distancePower += distancePowerIncrement) {

				for (int numInputsToUse = numInputs; numInputsToUse <= numInputs; numInputsToUse++) {

					int numCorrect = 0;

					double withinClassDistanceSum = 0.0;
					double betweenClassDistanceSum = 0.0;
					double allDistanceSum = 0.0;
					int withinClassDistanceCount = 0;
					int betweenClassDistanceCount = 0;
					int allDistanceCount = 0;

					double[] classConfidenceSums = new double[numClasses];

					int[][] predictedVsActualClassCounts = new int[numClasses][numClasses];

					for (int seedExampleIndex = 0; seedExampleIndex < numGroundTruthExamples; seedExampleIndex++) {

						for (int i = 0; i < classConfidenceSums.length; i++) {
							classConfidenceSums[i] = 0;
						}

						for (int otherExamplIndex = 0; otherExamplIndex < numGroundTruthExamples; otherExamplIndex++) {

							if (seedExampleIndex == otherExamplIndex) {
								continue;
							}

							double distanceSum = 0.0;
							for (int i = 0; i < numInputsToUse; i++) {
								double difference = Math.abs(groundExampleInputs[seedExampleIndex][i] - groundExampleInputs[otherExamplIndex][i]);
								if (useEuclideanDistance)
									distanceSum += Math.pow(difference, distancePower);
								if (useHammingDistance)
									distanceSum += difference;
							}

							double distance = Double.NaN;

							// if (useEuclideanDistance)
							// distance = Math.sqrt(distanceSum) / numInputsToUse;
							// if (useHammingDistance)
							// distance = distanceSum / numInputsToUse;

							distance = Math.pow(distanceSum, 1.0 / distancePower);

							double weight = Double.NaN;

							if (distance > 0.0) {
								weight = 1.0 / Math.pow(distance, distanceWeightingPower);
							}

							classConfidenceSums[groundExampleClassIndices[otherExamplIndex]] += weight;

							// update within and between class distance sums and counts

							allDistanceSum += distance;
							allDistanceCount++;

							if (groundExampleClassIndices[seedExampleIndex] == groundExampleClassIndices[otherExamplIndex]) {
								withinClassDistanceSum += distance;
								withinClassDistanceCount++;
							} else {
								betweenClassDistanceSum += distance;
								betweenClassDistanceCount++;
							}

						}

						double bestWeight = Double.NEGATIVE_INFINITY;
						int bestWeightIndex = -1;
						for (int i = 0; i < numClasses; i++) {
							if (classConfidenceSums[i] > bestWeight) {
								bestWeight = classConfidenceSums[i];
								bestWeightIndex = i;
							}
						}

						int predictedClassIndex = bestWeightIndex;
						int actualClassIndex = groundExampleClassIndices[seedExampleIndex];

						if (actualClassIndex == predictedClassIndex) {
							numCorrect++;
						}

						predictedVsActualClassCounts[predictedClassIndex][actualClassIndex]++;

					}

					for (int i = 0; i < numClasses; i++) {
						for (int j = 0; j < numClasses; j++) {
							System.out.print("\t" + predictedVsActualClassCounts[i][j]);
						}
						System.out.println();
					}

					double accuracy = (double) numCorrect / (double) numGroundTruthExamples;

					if (f) {
						System.out.println();
						System.out.println("numInputsToUse = " + numInputsToUse);
						System.out.println("accuracy = " + accuracy);
					}

					double withinClassDistance = withinClassDistanceSum / withinClassDistanceCount;
					double betweenClassDistance = betweenClassDistanceSum / betweenClassDistanceCount;
					double allDistance = allDistanceSum / allDistanceCount;

					if (f) {
						System.out.println("withinClassDistance = " + withinClassDistance);
						System.out.println("betweenClassDistance = " + betweenClassDistance);
						System.out.println("allDistance = " + allDistance);
					}

					double performance = (betweenClassDistance - withinClassDistance) / allDistance;
					if (f) {
						System.out.println("performance = " + numInputsToUse);
					}

					System.out.println("DATA" + "\t" + distanceWeightingPower + "\t" + distancePower + "\t" + numInputsToUse + "\t" + accuracy + "\t" + performance);

				}
			}
		}

	}

	private void classifyAllExmples() {

		double[] classConfidenceSums = new double[numClasses];

		for (int allExampleIndex = 0; allExampleIndex < allExampleData.length; allExampleIndex++) {

			for (int i = 0; i < classConfidenceSums.length; i++) {
				classConfidenceSums[i] = 0;
			}

			for (int otherExamplIndex = 0; otherExamplIndex < numGroundTruthExamples; otherExamplIndex++) {

				double distanceSum = 0.0;
				for (int i = 0; i < numInputs; i++) {
					double difference = Math.abs(allExampleData[allExampleIndex][DATA_INDEX + i] - groundExampleInputs[otherExamplIndex][i]);
					if (useEuclideanDistance)
						distanceSum += Math.pow(difference, distancePower);
					if (useHammingDistance)
						distanceSum += difference;
				}

				double distance = Double.NaN;

				distance = Math.pow(distanceSum, 1.0 / distancePower);

				double weight = Double.NaN;

				if (distance > 0.0) {
					weight = 1.0 / Math.pow(distance, distanceWeightingPower);
				}

				classConfidenceSums[groundExampleClassIndices[otherExamplIndex]] += weight;

			}

			double bestWeight = Double.NEGATIVE_INFINITY;
			int bestWeightIndex = -1;
			for (int i = 0; i < numClasses; i++) {
				if (classConfidenceSums[i] > bestWeight) {
					bestWeight = classConfidenceSums[i];
					bestWeightIndex = i;
				}
			}

			int predictedClassIndex = bestWeightIndex;
			allExampleClassIndices[allExampleIndex] = predictedClassIndex;

			if (allExampleIndex % 1000 == 0) {
				System.out.println("allExampleIndex = " + allExampleIndex + "  predictedClassIndex = " + predictedClassIndex);
			}

		}

	}

	SimpleTable table = null;

	public void allocateExampleStructures() {

	}

	double allExampleData[][];
	int allExampleClassIndices[];
	double obs[][];
	long groundExampleTimes[];
	double groundExampleInputs[][];
	int groundExampleClassIndices[];

	int numGroundTruthExamples;
	int numInputs;

	String groundTruthFilePath = "/d250/move/ground.xls";

	public void loadData() {

		if (groundTruthFilePath != null) {

			table = null;

			try {
				table = IO.readDelimitedTable(groundTruthFilePath, "\t");
			} catch (Exception e) {
				e.printStackTrace();
			}

			int numDataRows = table.numDataRows;
			int numDataColumns = table.numDataColumns;

			System.out.println("numDataRows    = " + numDataRows);
			System.out.println("numDataColumns = " + numDataColumns);

			obs = new double[numDataRows][2];

			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aa");
			numClasses = 0;
			numGroundTruthExamples = 0;
			for (int i = 0; i < numDataRows; i++) {

				String className = table.getString(i, 0);

				// String[] classNames = new String[] { "Drink", "Eat", "Forage", "Jump", "Locomote", "Overlord", "Rest", "Scan", "Scratch", "Threaten"};

				// String[] classNames = new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" };
				String[] classNames = new String[] { "sitting", "standing", "lying down", };

				numClasses = classNames.length;

				int classIndex = -1;
				for (int j = 0; j < classNames.length; j++) {
					if (classNames[j].equals(className)) {
						classIndex = j;
						break;
					}
				}

				if (classIndex == -1) {
					continue;
				}

				// int classNumber = table.getInt(i, 0);
				// if (classNumber == 0) {
				// continue;
				// }

				String dateString = table.stringMatrix[i][1];
				String timeString = table.stringMatrix[i][2];

				String simpleDateString = dateString + " " + timeString;

				Date date = null;
				try {
					date = simpleDateFormat.parse(simpleDateString);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				long time = date.getTime();

				// System.out.println("simpleDateString = " + simpleDateString);
				// System.out.println("date = " + date);
				// System.out.println("time = " + time);

				obs[numGroundTruthExamples][0] = time;
				obs[numGroundTruthExamples][1] = classIndex;

				numGroundTruthExamples++;

			}

			System.out.println("numExamples = " + numGroundTruthExamples);
			System.out.println("numClasses  = " + numClasses);

		}

		{

			table = null;

			try {
				table = IO.readDelimitedTable(examplesFilePath, "\t");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			int numDataRows = table.numDataRows;
			int numDataColumns = table.numDataColumns;

			System.out.println("numDataRows    = " + numDataRows);
			System.out.println("numDataColumns = " + numDataColumns);

			numInputs = numDataColumns - DATA_INDEX;
			System.out.println("numInputs = " + numInputs);

			groundExampleTimes = new long[numGroundTruthExamples];
			groundExampleInputs = new double[numGroundTruthExamples][numInputs];
			groundExampleClassIndices = new int[numGroundTruthExamples];

			allExampleData = new double[numDataRows][numDataColumns];
			allExampleClassIndices = new int[numDataRows];

			int exampleIndex = 0;

			for (int i = 0; i < numDataRows; i++) {

				for (int j = 0; j < numDataColumns; j++) {

					if (j >= DATA_INDEX)
						allExampleData[i][j] = table.getDouble(i, j) + dataValueOffset;
					else
						allExampleData[i][j] = table.getDouble(i, j);
				}

				if (groundTruthFilePath != null) {

					long time = table.getLong(i, T_INDEX);

					boolean found = false;
					int obsIndex = -1;
					for (int j = 0; j < numGroundTruthExamples; j++) {

						if (obs[j][0] == time) {
							found = true;
							obsIndex = j;
						}
					}

					if (found) {

						groundExampleTimes[exampleIndex] = time;
						for (int j = 0; j < numInputs; j++) {

							groundExampleInputs[exampleIndex][j] = table.getDouble(i, j + DATA_INDEX) + +dataValueOffset;

						}

						groundExampleClassIndices[exampleIndex] = (int) obs[obsIndex][1];

						exampleIndex++;
					}

				}

			}

			if (exampleIndex < numGroundTruthExamples) {

				System.out.println("numExampleNotFound = " + (numGroundTruthExamples - exampleIndex));

				numGroundTruthExamples = exampleIndex;

			}
			System.out.println("numExamples = " + numGroundTruthExamples);
			System.out.println("exampleIndex = " + exampleIndex);
		}

	}

	public void drawImage(int screenWidth, int screenHeight, Graphics2D g2) {

		// double xFocus = 0.40;
		// double yFocus = 0.10;
		// double zoomFactor = 2;

		// double xFocus = 0.30;
		// double yFocus = 0.10;
		// double zoomFactor = 2;

		// 27, 158, 119; 217, 95, 2; 117, 112, 179; 231, 41, 138; 102, 166, 30; 230, 171, 2; 166, 118, 29; 102, 102, 102;

		// 8 colors for qualitative distinction

		Color colors3[] = new Color[] { new Color(255, 0, 0), new Color(0, 255, 0), new Color(0, 0, 255) };

		Color colors4[] = new Color[] { new Color(128, 128, 128), new Color(255, 0, 0), new Color(0, 255, 0), new Color(0, 0, 255) };

		Color colors8[] = new Color[] { new Color(27, 158, 119), new Color(217, 95, 2), new Color(117, 112, 179), new Color(231, 41, 138), new Color(102, 166, 30),
				new Color(230, 171, 2), new Color(166, 118, 29), new Color(102, 102, 102) };

		// 11 colors for qualitative distinction

		Color colors11[] = new Color[] { new Color(166, 206, 227), new Color(31, 120, 180), new Color(178, 223, 138), new Color(51, 160, 44), new Color(251, 154, 153),
				new Color(227, 26, 28), new Color(253, 191, 111), new Color(255, 127, 0), new Color(202, 178, 214), new Color(106, 61, 154), new Color(255, 255, 153), };

		// Color colors11[] = new Color[] { new Color(255, 0, 0), new Color(0, 255, 0), new Color(0, 0, 255), new Color(0, 255, 255), new Color(255, 0, 255), new Color(128, 0, 0),
		// new Color(230, 171, 2), new Color(217, 95, 2), new Color(117, 112, 179), new Color(231, 41, 138), new Color(102, 166, 30), };

		System.out.println("drawImage");

		if (blackBackground) {

			g2.setColor(Color.black);
			g2.fillRect(0, 0, screenXSize, screenYSize);
		}
		if (whiteBackground) {

			g2.setColor(Color.white);
			g2.fillRect(0, 0, screenXSize, screenYSize);
		}

		double minTimeInMS = Double.POSITIVE_INFINITY;
		double maxTimeInMS = Double.NEGATIVE_INFINITY;

		for (int i = 0; i < allExampleData.length; i++) {

			double time = allExampleData[i][T_INDEX];
			;

			if (time < minTimeInMS) {
				minTimeInMS = time;
			}
			if (time > maxTimeInMS) {
				maxTimeInMS = time;
			}
		}

		if (showAllExamples) {

			double[][] sums = new double[screenXSize][screenYSize];
			double[][][] sumsByColor = null;

			if (colorAllExamplesByPredictedClass)
				sumsByColor = new double[screenXSize][screenYSize][numClasses];
			else
				sumsByColor = new double[screenXSize][screenYSize][numTimeColors];

			double[][][] sumsByCoordinate = new double[screenXSize][screenYSize][3];
			int[][] countsByCoordinate = new int[screenXSize][screenYSize];

			double maxSum = 0.0;

			for (int i = 0; i < allExampleData.length; i++) {

				int colorIndex = -1;

				if (colorAllExamplesByPredictedClass) {
					colorIndex = allExampleClassIndices[i];
				} else {
					colorIndex = (int) ((double) (((long) allExampleData[i][T_INDEX] + timeShift) % numMSPerDay) / numMSPerDay * numTimeColors);
				}

				int xIndex;
				if (xAxisIsTimeOfDay)
					xIndex = (int) ((double) (((long) allExampleData[i][T_INDEX] + timeShift) % numMSPerDay) / numMSPerDay * screenXSize * xZoom);
				else if (xAxisIsTime)
					xIndex = (int) ((double) (((long) allExampleData[i][T_INDEX] - minTimeInMS) / (maxTimeInMS - minTimeInMS) - xOffset) * screenXSize * xZoom);
				else {

					if (xAxisIsXplusY)
						xIndex = (int) (((allExampleData[i][DATA_INDEX + X_INDEX] + allExampleData[i][DATA_INDEX + Y_INDEX]) / 2 - xOffset) * screenXSize * xZoom);
					else
						xIndex = (int) ((allExampleData[i][DATA_INDEX + X_INDEX] - xOffset) * screenXSize * xZoom);
				}

				int yIndex;

				if (yAxisIsTimeOfDay)
					yIndex = (int) ((double) (((long) allExampleData[i][T_INDEX] + timeShift) % numMSPerDay) / numMSPerDay * screenYSize * yZoom);
				else {
					if (numInputs == 1)
						// yIndex = (int) ((allExampleData[i][DATA_INDEX + X_INDEX] - yOffset) * screenYSize * yZoom);
						yIndex = (int) (Math.random() * screenYSize);
					else
						yIndex = (int) ((allExampleData[i][DATA_INDEX + Y_INDEX] - yOffset) * screenYSize * yZoom);
				}

				if (xIndex < 0 || xIndex >= screenXSize || yIndex < 0 || yIndex >= screenYSize) {
					continue;
				}

				// if (numInputs == 1) {
				// for (int j = 0; j < screenYSize; j++) {
				//
				// sums[xIndex][j]++;
				// sumsByTimeOfDay[xIndex][j][timeColorIndex]++;
				// }
				// }
				// else {
				sums[xIndex][yIndex]++;
				sumsByColor[xIndex][yIndex][colorIndex]++;
				// }

				if (numInputs > 0)
					sumsByCoordinate[xIndex][yIndex][0] += allExampleData[i][DATA_INDEX + X_INDEX];
				if (numInputs > 1)
					sumsByCoordinate[xIndex][yIndex][1] += allExampleData[i][DATA_INDEX + Y_INDEX];
				if (numInputs > 2)
					sumsByCoordinate[xIndex][yIndex][2] += allExampleData[i][DATA_INDEX + Z_INDEX];

				countsByCoordinate[xIndex][yIndex]++;

				// if (true) {
				// if (numInputs > 0)
				// sumsByCoordinate[xIndex][yIndex][0] = allExampleData[i][DATA_INDEX + X_INDEX];
				// if (numInputs > 1)
				// sumsByCoordinate[xIndex][yIndex][1] = allExampleData[i][DATA_INDEX + Y_INDEX];
				// if (numInputs > 2)
				// sumsByCoordinate[xIndex][yIndex][2] = allExampleData[i][DATA_INDEX + Z_INDEX];
				// countsByCoordinate[xIndex][yIndex] = 1;
				// }

				if (sums[xIndex][yIndex] > maxSum) {
					maxSum = sums[xIndex][yIndex];
				}

			}

			for (int i = 0; i < screenXSize; i++) {
				for (int j = 0; j < screenYSize; j++) {

					if (sums[i][j] > 0) {

						double intensity = Double.NaN;
						if (useLogIntensity)
							intensity = Math.log10(sums[i][j]) / Math.log10(maxSum);
						else if (useLinearIntensity)
							intensity = sums[i][j] / maxSum;

						if (colorTimeOfDay) {

							double maxValue = Double.NEGATIVE_INFINITY;
							int bestTimeIndex = -1;
							for (int k = 0; k < numTimeColors; k++) {
								if (sumsByColor[i][j][k] > maxValue) {
									maxValue = sumsByColor[i][j][k];
									bestTimeIndex = k;
								}
							}

							if (numTimeColors <= 3) {
								g2.setColor(colors3[bestTimeIndex]);
							} else if (numClasses <= 4) {
								g2.setColor(colors4[bestTimeIndex]);
							} else if (numTimeColors <= 8) {
								g2.setColor(colors8[bestTimeIndex]);
							} else {
								g2.setColor(colors11[bestTimeIndex]);
							}
						} else if (colorByIntensity) {

							if (useBinaryIntensity) {
								if (blackBackground)
									g2.setColor(Color.white);
								if (whiteBackground)
									g2.setColor(Color.black);

							} else {
								if (whiteBackground)
									g2.setColor(new Color(1.0f - (float) intensity, 1.0f - (float) intensity, 1.0f - (float) intensity));
								if (blackBackground)
									g2.setColor(new Color((float) intensity, (float) intensity, (float) intensity));
							}
						} else if (colorAllExamplesByCoordinates) {
							float x = Float.NaN;
							float y = Float.NaN;
							float z = Float.NaN;
							if (numInputs == 1) {

								double value = (sumsByCoordinate[i][j][0] / countsByCoordinate[i][j]) * 11;

								// System.out.println(value);

								int colorIndex = (int) (value);

								Color color = colors11[colorIndex];

								x = color.getRed() / 255.0f;
								y = color.getGreen() / 255.0f;
								z = color.getBlue() / 255.0f;
								// x = (float) (sumsByCoordinate[i][j][0] / countsByCoordinate[i][j]);
								// y = (float) (0.0);
								// z = (float) (0.0);
							} else if (numInputs == 2) {
								x = (float) (sumsByCoordinate[i][j][0] / countsByCoordinate[i][j]);
								y = (float) (sumsByCoordinate[i][j][1] / countsByCoordinate[i][j]);
								z = (float) (((sumsByCoordinate[i][j][0] - sumsByCoordinate[i][j][1]) / countsByCoordinate[i][j] + 1.0) / 2.0);

							} else if (numInputs >= 3) {
								x = (float) (sumsByCoordinate[i][j][0] / countsByCoordinate[i][j]);
								y = (float) (sumsByCoordinate[i][j][1] / countsByCoordinate[i][j]);
								z = (float) (sumsByCoordinate[i][j][2] / countsByCoordinate[i][j]);

							}

							// System.out.println("x = " + x);
							// System.out.println("y = " + y);
							// System.out.println("z = " + z);
							g2.setColor(new Color(x, y, z));

						} else if (colorAllExamplesByPredictedClass) {

							double maxValue = Double.NEGATIVE_INFINITY;
							int bestTimeIndex = -1;
							for (int k = 0; k < numClasses; k++) {
								if (sumsByColor[i][j][k] > maxValue) {
									maxValue = sumsByColor[i][j][k];
									bestTimeIndex = k;
								}
							}

							if (numClasses <= 3) {
								g2.setColor(colors3[bestTimeIndex]);
							} else if (numClasses <= 4) {
								g2.setColor(colors4[bestTimeIndex]);
							} else if (numClasses <= 8) {
								g2.setColor(colors8[bestTimeIndex]);
							} else {
								g2.setColor(colors11[bestTimeIndex]);
							}

						}

						g2.fillRect(i - allExamplesPointXWidth / 2, j - allExamplesPointYWidth / 2, allExamplesPointXWidth, allExamplesPointYWidth);
					}

				}
			}
		}

		if (showGroundTruthExamples) {

			if (useClassNumbers) {

				for (int i = 0; i < numGroundTruthExamples; i++) {

					int classIndex = groundExampleClassIndices[i];

					int xIndex;
					if (xAxisIsTimeOfDay)
						xIndex = (int) ((double) ((groundExampleTimes[i] + timeShift) % numMSPerDay) / numMSPerDay * screenXSize * xZoom);
					if (xAxisIsTime)
						xIndex = (int) ((double) ((groundExampleTimes[i] - minTimeInMS) / (maxTimeInMS - minTimeInMS) - xOffset) * screenXSize * xZoom);
					else
						xIndex = (int) ((groundExampleInputs[i][X_INDEX] - xOffset) * screenXSize * xZoom);

					// int xIndex = (int) ((exampleInputs[i][X_INDEX] * zoomFactor - xFocus * zoomFactor) * screenXSize* zoomFactor);
					int yIndex;

					if (yAxisIsTimeOfDay)
						yIndex = (int) ((double) ((groundExampleTimes[i] + timeShift) % numMSPerDay) / numMSPerDay * screenYSize * yZoom);
					else
						yIndex = (int) ((groundExampleInputs[i][Y_INDEX] + yOffset) * screenYSize * yZoom);

					// String str = Integer.toString(classIndex);
					char chars[] = new char[1];
					chars[0] = (char) ('A' + classIndex);
					String str = new String(chars);

					g2.setColor(colors11[classIndex]);
					g2.drawString(str, xIndex, yIndex);

				}
			} else {

				double[][][] sums = new double[screenXSize][screenYSize][numClasses];

				for (int i = 0; i < numGroundTruthExamples; i++) {

					int classIndex = groundExampleClassIndices[i];

					int xIndex;
					if (xAxisIsTimeOfDay)
						xIndex = (int) ((double) ((groundExampleTimes[i] + timeShift) % numMSPerDay) / numMSPerDay * screenXSize * xZoom);
					if (xAxisIsTime)
						xIndex = (int) ((double) ((groundExampleTimes[i] - minTimeInMS) / (maxTimeInMS - minTimeInMS) - xOffset) * screenXSize * xZoom);
					else
						xIndex = (int) ((groundExampleInputs[i][X_INDEX] - xOffset) * screenXSize * xZoom);

					int yIndex;

					if (yAxisIsTimeOfDay)
						yIndex = (int) ((double) ((groundExampleTimes[i] + timeShift) % numMSPerDay) / numMSPerDay * screenYSize * yZoom);
					else
						yIndex = (int) ((groundExampleInputs[i][Y_INDEX] + yOffset) * screenYSize * yZoom);

					if (xIndex < 0 || xIndex >= screenXSize || yIndex < 0 || yIndex >= screenYSize) {
						continue;
					}

					sums[xIndex][yIndex][classIndex]++;

				}

				for (int i = 0; i < screenXSize; i++) {
					for (int j = 0; j < screenYSize; j++) {

						int classIndex = -1;
						double max = Double.NEGATIVE_INFINITY;
						for (int c = 0; c < numClasses; c++) {
							if (sums[i][j][c] > max) {
								max = sums[i][j][c];
								classIndex = c;
							}
						}

						int width = groundTruthPointWidth;
						if (sums[i][j][classIndex] > 0) {

							if (useClassColoredGroundTruth) {

								if (numClasses <= 3) {
									g2.setColor(colors3[classIndex]);
								} else if (numClasses <= 4) {
									g2.setColor(colors4[classIndex]);
								} else if (numClasses <= 8) {
									g2.setColor(colors8[classIndex]);
								} else {
									g2.setColor(colors11[classIndex]);
								}

							} else if (useBlackGroundTruth)

								if (whiteBackground)
									g2.setColor(Color.black);
								else
									g2.setColor(Color.white);

							g2.fillOval(i - width / 2, j - width / 2, width, width);
						}

					}
				}
			}

		}

	}

	boolean firstTime = true;

	public void paint(Graphics g) {

		if (firstTime) {
			Graphics2D g2 = (Graphics2D) g;
			Dimension d = getSize();
			g2.setBackground(getBackground());
			g2.clearRect(0, 0, d.width, d.height);
			g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			drawImage(d.width, d.height, g2);
			firstTime = false;
		}

	}

	public static void main(String argv[]) {

		System.out.println(new Date(0L));

		final ClusterVis demo = new ClusterVis();

	}
}