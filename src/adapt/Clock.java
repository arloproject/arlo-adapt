package adapt;

import java.util.*;

public class Clock {

	Date start_date = new Date();
	String title = null;

	public Clock() {
		this.title = "";
		this.start_date = new Date();
	}

	public Clock(String title) {
		this.title = title;
		this.start_date = new Date();
	}

	public void resetClock() {
		this.start_date = new Date();
	}

	public void report(String title) {
		Date current_date = new Date();
		double seconds = (double) ((current_date.getTime() - start_date.getTime()) / 1000.0);
		System.out.println(title + seconds);
	}

	public void report() {
		Date current_date = new Date();
		double seconds = (double) ((current_date.getTime() - start_date.getTime()) / 1000.0);
		System.out.println(title + seconds);
	}

	public double getTime() {
		Date current_date = new Date();
		return (double) ((current_date.getTime() - start_date.getTime()) / 1000.0);
	}

}
