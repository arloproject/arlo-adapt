package adapt;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;

import jtar.TarConstants;
import jtar.TarEntry;

import com.urfilez.indexing.audio.featureExtraction.MetaData;

public class AudioFeaturesTar {

	double bandEnergyMeanDistributionWeight = 1.0; // fair
	double bandEnergyDeviationDistributionWeight = 1.0; // good
	double bandEnergyContourDistributionWeight = 1.0; // bad
	double rhythmEnergyDistributionWeight = 1.0; // good
	double noteEnergyDistributionWeight = 1.0; // fair

	int maxNumFeatures = 534;
	int maxNumTracks = 3104240;
	int maxTrackCountToProcess = 999999999;

	// String tarFilePath = "/home/dtcheng/URFILES/data/audio_features.tar";
	String tarFilePath = "/home/dtcheng/URFILES/data/.audio_features.tar.fXdsKI";
	String compiledFeaturesFilePath = "/home/dtcheng/URFILES/data/compiledFeatures.ser";

	int[] audioFeatureTrackIDs = new int[maxNumTracks];
	int[] albumNumbers = new int[maxNumTracks];
	int[] trackNumbers = new int[maxNumTracks];

	int bandEnergyMeanDistributionFeatureCount = 22;
	int bandEnergyDeviationDistributionFeatureCount = 253;
	int bandEnergyContourDistributionFeatureCount = 7;
	int rhythmEnergyDistributionFeatureCount = 240;
	int noteEnergyDistributionFeatureCount = 12;

	Hashtable<Integer, String> trackIDToTrackName = new Hashtable<Integer, String>();

	int trackCount;
	short[][] centerWindowFeaturesShort = null;

	double[] bestDifferenceSums;
	int[] bestAudioFeaturesTrackIDs;

	Hashtable<Integer, Integer> audioFeaturesTrackIDToTrackIndex;

	//
	//
	//
	//
	//
	//

	AudioFeaturesTar() {

		if (false)
			createCompiledAudioFeatures();

		loadCompiledAudioFeatures();

	}

	//
	//
	//

	public void createCompiledAudioFeatures() {

		float[] centerWindowFeatures = new float[maxNumFeatures];
		float[] centerWindowFeatureMins = new float[maxNumFeatures];
		float[] centerWindowFeatureMaxs = new float[maxNumFeatures];

		for (int i = 0; i < maxNumFeatures; i++) {
			centerWindowFeatureMins[i] = Float.POSITIVE_INFINITY;
			centerWindowFeatureMaxs[i] = Float.NEGATIVE_INFINITY;
		}

		centerWindowFeaturesShort = new short[maxNumTracks][maxNumFeatures];

		HashMap<String, Integer> entryNameToIndex = new HashMap<String, Integer>();

		long tarFileLength = new File(tarFilePath).length();

		byte[] headerData = new byte[TarConstants.HEADER_BLOCK];

		int maxBodyDataSize = (int) 1e9;
		byte[] bodyData = new byte[maxBodyDataSize];

		// 1st pass calculate min and max values for each feature
		// 2nd pass normalize and save feature values
		for (int passIndex = 0; passIndex < 2; passIndex++) {

			int count = 0;
			long totalBytesRead = 0;
			long lastTotalBytesRead = 0;
			long startTimeInMS = System.currentTimeMillis();
			long lastBufferTimeInMS = System.currentTimeMillis();
			int lastNumGigaBytesRead = -1;
			int numGigBytesRead = 0;

			int trackCount = 0;
			int goodKeyCount = 0;

			try {
				RandomAccessFile raf = new RandomAccessFile(tarFilePath, "r");

				while (totalBytesRead < tarFileLength && trackCount < maxTrackCountToProcess) {

					raf.seek(totalBytesRead);
					raf.readFully(headerData);

					TarEntry tarEntry = new TarEntry(headerData);

					String entryName = tarEntry.getName();
					// System.out.println("entryName = " + entryName);

					// String parts[] = entryName.split("/");

					boolean isGoodFile = true;

					if (!(entryName.endsWith(".UnMaskedTLKR_LW_RG_meta"))) {
						isGoodFile = false;
					}

					totalBytesRead += headerData.length;

					long dataSize = tarEntry.getSize();
					// System.out.println("tarFileEntryName = " + tarFileEntryName);
					// System.out.println("dataSize = " + dataSize);
					long tarSize = -1;

					if (dataSize % TarConstants.HEADER_BLOCK == 0) {
						tarSize = dataSize;
					} else {
						tarSize = (dataSize / TarConstants.HEADER_BLOCK + 1) * TarConstants.HEADER_BLOCK;
					}

					if (isGoodFile) {

						if (dataSize <= maxBodyDataSize) {

							raf.readFully(bodyData, 0, (int) dataSize);

							if (false) {
								MessageDigest m = MessageDigest.getInstance("MD5");

								m.update(bodyData, 0, (int) dataSize);

							}

							// System.out.println("MD5: " + new BigInteger(1, m.digest()).toString(16));

							// long sum = 0;
							// for (int i = 0; i < dataSize; i++) {
							// sum += (int) bodyData[i] << 8;
							// }

							goodKeyCount++;

							// System.out.println("KEY\t" + goodKeyCount + "\t" + dataSize + "\t" + new BigInteger(1, m.digest()).toString(16) + "\t" + entryName);

							ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bodyData, 0, (int) dataSize);

							MetaData metaData = null;

							try {
								metaData = (MetaData) IO.readObjectFromStream(byteArrayInputStream);
							} catch (Exception e) {
								System.out.println("Error!  Could not read serialized object.  entryName = " + entryName);
								continue;
							}

							if (metaData == null) {
								continue;
							}

							if (isGoodFile) {

								Integer entryNameIndex = entryNameToIndex.get(entryName);
								if (entryNameIndex == null) {
									entryNameIndex = entryNameToIndex.size();
									entryNameToIndex.put(entryName, entryNameIndex);
								}

							}

							if (metaData.framesPerWindow != 1722) {
								System.out.println("framesPerWindow = " + metaData.framesPerWindow);
							}

							if (metaData.audioSampleRate != 44100.0 && metaData.audioSampleRate != 48000.0) {
								System.out.println("audioSampleRate = " + metaData.audioSampleRate);
							}

							if (metaData.numWindows < 1 || metaData.numWindows > 17) {
								System.out.println("numWindows = " + metaData.numWindows);
							}

							if (metaData.spectraSampleRate != 43.066406f) {
								System.out.println("spectraSampleRate = " + metaData.spectraSampleRate);
							}

							int bandEnergyMeanDistributionFeatureCount = -1;
							int bandEnergyDeviationDistributionFeatureCount = -1;
							int bandEnergyPathIntegralDistributionFeatureCount = -1;
							int bandEnergyContourDistributionFeatureCount = -1;
							int bandEnergyPitchDistributionFeatureCount = -1;
							int rhythmEnergyDistributionFeatureCount = -1;
							int noteEnergyDistributionFeatureCount = -1;

							bandEnergyMeanDistributionFeatureCount = metaData.bandEnergyMeanDistribution[0].length;
							bandEnergyDeviationDistributionFeatureCount = metaData.bandEnergyDeviationDistribution[0].length;
							// bandEnergyPathIntegralDistributionFeatureCount = metaData.bandEnergyPathIntegralDistribution[0].length;
							bandEnergyContourDistributionFeatureCount = metaData.bandEnergyContourDistribution[0].length;
							// bandEnergyPitchDistributionFeatureCount = metaData.bandEnergyPitchDistribution[0].length;
							rhythmEnergyDistributionFeatureCount = metaData.rhythmEnergyDistribution[0].length;
							noteEnergyDistributionFeatureCount = metaData.noteEnergyDistribution[0].length;

							if (false) {
								System.out.println("bandEnergyMeanDistributionFeatureCount         = " + bandEnergyMeanDistributionFeatureCount);
								System.out.println("bandEnergyDeviationDistributionFeatureCount    = " + bandEnergyDeviationDistributionFeatureCount);
								// System.out.println("bandEnergyPathIntegralDistributionFeatureCount = " + bandEnergyPathIntegralDistributionFeatureCount);
								System.out.println("bandEnergyContourDistributionFeatureCount      = " + bandEnergyContourDistributionFeatureCount);
								// System.out.println("bandEnergyPitchDistributionFeatureCount        = " + bandEnergyPitchDistributionFeatureCount);
								System.out.println("rhythmEnergyDistributionFeatureCount           = " + rhythmEnergyDistributionFeatureCount);
								System.out.println("noteEnergyDistributionFeatureCount             = " + noteEnergyDistributionFeatureCount);
							}

							int numFeatures = bandEnergyMeanDistributionFeatureCount + bandEnergyDeviationDistributionFeatureCount + bandEnergyContourDistributionFeatureCount + rhythmEnergyDistributionFeatureCount + noteEnergyDistributionFeatureCount;

							// System.out.println("numFeatures = " + numFeatures);

							int f = 0;
							int w = metaData.numWindows / 2;

							for (int i = 0; i < bandEnergyMeanDistributionFeatureCount; i++) {
								centerWindowFeatures[f++] = metaData.bandEnergyMeanDistribution[w][i];
							}
							for (int i = 0; i < bandEnergyDeviationDistributionFeatureCount; i++) {
								centerWindowFeatures[f++] = metaData.bandEnergyDeviationDistribution[w][i];
							}
							for (int i = 0; i < bandEnergyContourDistributionFeatureCount; i++) {
								centerWindowFeatures[f++] = metaData.bandEnergyContourDistribution[w][i];
							}
							for (int i = 0; i < rhythmEnergyDistributionFeatureCount; i++) {
								centerWindowFeatures[f++] = metaData.rhythmEnergyDistribution[w][i];
							}
							for (int i = 0; i < noteEnergyDistributionFeatureCount; i++) {
								centerWindowFeatures[f++] = metaData.noteEnergyDistribution[w][i];
							}
							// System.out.println("f = " + f);

							// min max calculation
							if (passIndex == 0) {
								for (int i = 0; i < numFeatures; i++) {

									if (centerWindowFeatures[i] < centerWindowFeatureMins[i]) {
										centerWindowFeatureMins[i] = centerWindowFeatures[i];
									}

									if (centerWindowFeatures[i] > centerWindowFeatureMaxs[i]) {
										centerWindowFeatureMaxs[i] = centerWindowFeatures[i];
									}
								}
							}

							// value normalization
							if (passIndex == 1) {
								for (int i = 0; i < numFeatures; i++) {
									centerWindowFeaturesShort[trackCount][i] = (short) ((centerWindowFeatures[i] - centerWindowFeatureMins[i]) / (centerWindowFeatureMaxs[i] - centerWindowFeatureMins[i]) * Short.MAX_VALUE);
								}
							}

						}

						trackCount++;

					}

					totalBytesRead += tarSize;

					numGigBytesRead = (int) (totalBytesRead / 1000000000);

					count++;

					if (numGigBytesRead != lastNumGigaBytesRead) {

						long bytesRead = totalBytesRead - lastTotalBytesRead;
						lastNumGigaBytesRead = numGigBytesRead;
						lastTotalBytesRead = totalBytesRead;

						double durationInSeconds = (System.currentTimeMillis() - startTimeInMS) / 1000.0;
						double bufferDurationInSeconds = (System.currentTimeMillis() - lastBufferTimeInMS) / 1000.0;
						lastBufferTimeInMS = System.currentTimeMillis();

						long bytesLeft = tarFileLength - totalBytesRead;

						double bytesPerSecond = (double) bytesRead / bufferDurationInSeconds;
						double secondsLeft = bytesLeft / bytesPerSecond;

						System.out.println();
						// System.out.println("tarFileEntryName = " + tarFileEntryName);
						System.out.println("timeLeft (s) = " + secondsLeft);
						System.out.println("timeLeft (h) = " + secondsLeft / 3600);
						System.out.println("timeLeft (d) = " + secondsLeft / 3600 / 24);
						System.out.println("ETA = " + new Date((long) (System.currentTimeMillis() + secondsLeft * 1000)));
						System.out.println("durationInSeconds = " + durationInSeconds);
						System.out.println("totalBytesRead (GB) = " + (double) totalBytesRead / 1000000000);
						System.out.println("overall bytesPerSecond (MB) = " + (double) totalBytesRead / durationInSeconds / 1000000);
						System.out.println("buffer  bytesPerSecond (MB) = " + (double) bytesRead / bufferDurationInSeconds / 1000000);
						System.out.println("bytesRead  = " + bytesRead);
						System.out.println("count = " + count);
					}

				}

				raf.close();

			} catch (IOException e) {
				e.printStackTrace();
				System.exit(1);
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (passIndex == 0) {

				for (int i = 0; i < maxNumFeatures; i++) {

					System.out.println("centerWindowFeatureMins[" + i + "] = " + centerWindowFeatureMins[i]);
					System.out.println("centerWindowFeatureMaxs[" + i + "] = " + centerWindowFeatureMaxs[i]);

				}
			}

			if (passIndex == 1) {
				System.out.println("trackCount = " + trackCount);
				IO.writeObject(compiledFeaturesFilePath, new Object[] { trackCount, centerWindowFeaturesShort });
			}
		}

	}

	//
	//
	//

	public void loadCompiledAudioFeatures() {

		System.out.println("Reading album and track keys with precomputed audio features...");

		audioFeaturesTrackIDToTrackIndex = new Hashtable<Integer, Integer>();
		{

			SimpleTable keyTable = null;
			try {
				keyTable = IO.readDelimitedTable("/home/dtcheng/URFILES/data/keys.txt", "\t", false);
			} catch (Exception e1) {
				e1.printStackTrace();
				System.exit(1);
			}

			int numRows = keyTable.numDataRows;

			System.out.println("numRows      = " + numRows);

			for (int i = 0; i < numRows; i++) {

				String key = keyTable.getString(i, 0);

				// System.out.println("key = " + key);

				int index1 = key.indexOf('-');
				// System.out.println("index1 = " + index1);

				int albumNumber = Integer.parseInt(key.substring(0, index1));
				int trackNumber = Integer.parseInt(key.substring(index1 + 1));
				// System.out.println("albumNumber = " + albumNumber);
				// System.out.println("trackNumber = " + trackNumber);

				albumNumbers[i] = albumNumber;
				trackNumbers[i] = trackNumber;

				audioFeatureTrackIDs[i] = trackNumber;

				audioFeaturesTrackIDToTrackIndex.put(audioFeatureTrackIDs[i], i);

			}
			System.out.println("keyToAlbumNumber.size()      = " + audioFeaturesTrackIDToTrackIndex.size());
		}

		//
		//
		//

		System.out.println("Reading track names...");

		trackIDToTrackName = new Hashtable<Integer, String>();

		SimpleTable metaDataTable = null;
		try {
			metaDataTable = IO.readDelimitedTable("/home/dtcheng/URFILES/data/cf_all.csv", "\t", true);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		int numRows = metaDataTable.numDataRows;

		System.out.println("numRows      = " + numRows);

		for (int i = 0; i < numRows; i++) {

			int trackNumber = metaDataTable.getInt(i, 0);
			int albumNumber = metaDataTable.getInt(i, 1);
			String name = metaDataTable.getString(i, 3);

			// long key = albumNumber * 1000000000L + trackNumber;

			trackIDToTrackName.put(trackNumber, name);

		}

		System.out.println("audioFeaturesTrackNames()   = " + trackIDToTrackName.size());

		//
		//
		//

		System.out.println("Reading compiled features...");
		Object[] objects = (Object[]) IO.readObject(compiledFeaturesFilePath);

		trackCount = (Integer) objects[0];
		centerWindowFeaturesShort = (short[][]) objects[1];

	}

	public String getURL(int albumNumber, int trackNumber, String label) {

		String url = null;
		try {

			String queryString = "http://audiopreviewmp3.urfilez.com/?albumid=" + albumNumber + "&trackid=" + albumNumber + "_" + trackNumber + ".mp3";
			System.out.println(label + " = " + queryString);

			URL oracle = new URL(queryString);
			BufferedReader in = new BufferedReader(new InputStreamReader(oracle.openStream()));

			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				// System.out.println(inputLine);
				if (inputLine.indexOf("AccessKeyId") != -1) {
					int index1 = inputLine.indexOf("http");
					int index2 = inputLine.indexOf("</div>");
					url = inputLine.substring(index1, index2);
				}
			}
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return url;
	}

	//
	//
	//
	//
	//
	// //
	//
	//
	// * TrackID ID of the track last played
	// * features comma separated features values couples (loudness:12,key:-10), min:-100, max: 100
	// * resultCount Number of tracks to play next
	// * Returns TrackIDs to be played next

	public int[] extendPlayListNew(int seedTrackID, String featureString, int resultCount) {

		bandEnergyMeanDistributionWeight = 0;
		bandEnergyDeviationDistributionWeight = 0;
		bandEnergyContourDistributionWeight = 0;
		rhythmEnergyDistributionWeight = 0;
		noteEnergyDistributionWeight = 0;

		String[] parameterKeyValues = featureString.split(",");

		for (int i = 0; i < parameterKeyValues.length; i++) {

			String[] keyValue = parameterKeyValues[i].split(":");

			String key = keyValue[0];
			int value = Integer.parseInt(keyValue[1]);

			if (key.equalsIgnoreCase("TimbreMean")) {
				bandEnergyMeanDistributionWeight = value;

			} else if (key.equalsIgnoreCase("TimbreCovariance")) {
				bandEnergyDeviationDistributionWeight = value;

			} else if (key.equalsIgnoreCase("Loudness")) {
				bandEnergyContourDistributionWeight = value;

			} else if (key.equalsIgnoreCase("Rhythm")) {
				rhythmEnergyDistributionWeight = value;

			} else if (key.equalsIgnoreCase("Key")) {
				noteEnergyDistributionWeight = value;

			} else {
				System.exit(1);
			}

		}

		double weightSum = 0;
		weightSum += bandEnergyMeanDistributionWeight;
		weightSum += bandEnergyDeviationDistributionWeight;
		weightSum += bandEnergyContourDistributionWeight;
		weightSum += rhythmEnergyDistributionWeight;
		weightSum += noteEnergyDistributionWeight;

		bandEnergyMeanDistributionWeight /= weightSum;
		bandEnergyDeviationDistributionWeight /= weightSum;
		bandEnergyContourDistributionWeight /= weightSum;
		rhythmEnergyDistributionWeight /= weightSum;
		noteEnergyDistributionWeight /= weightSum;

		System.out.println("TimbreMean Weight       = " + bandEnergyMeanDistributionWeight);
		System.out.println("TimbreCovariance Weight = " + bandEnergyDeviationDistributionWeight);
		System.out.println("Loudness Weight         = " + bandEnergyContourDistributionWeight);
		System.out.println("Rhythm Weight           = " + rhythmEnergyDistributionWeight);
		System.out.println("Key Weight              = " + noteEnergyDistributionWeight);

		int resultCountMinusOne = resultCount - 1;

		bestDifferenceSums = new double[resultCount];
		bestAudioFeaturesTrackIDs = new int[resultCount];

		for (int i = 0; i < resultCount; i++) {
			bestDifferenceSums[i] = Double.POSITIVE_INFINITY;
			bestAudioFeaturesTrackIDs[i] = -1;
		}

		double[] weights = new double[maxNumFeatures];

		// for (int f = 0; f < maxNumFeatures; f++) {
		// weights[f] = Math.random();
		// }
		// for (int f = 0; f < maxNumFeatures; f++) {
		// weights[f] = 1.0;
		// }

		{
			int f = 0;

			for (int i = 0; i < bandEnergyMeanDistributionFeatureCount; i++) {
				weights[f++] = bandEnergyMeanDistributionWeight;
			}
			for (int i = 0; i < bandEnergyDeviationDistributionFeatureCount; i++) {
				weights[f++] = bandEnergyDeviationDistributionWeight;
			}
			for (int i = 0; i < bandEnergyContourDistributionFeatureCount; i++) {
				weights[f++] = bandEnergyContourDistributionWeight;
			}
			for (int i = 0; i < rhythmEnergyDistributionFeatureCount; i++) {
				weights[f++] = rhythmEnergyDistributionWeight;
			}
			for (int i = 0; i < noteEnergyDistributionFeatureCount; i++) {
				weights[f++] = noteEnergyDistributionWeight;
			}
		}

		int seedAudioFeaturesTrackIndex = audioFeaturesTrackIDToTrackIndex.get(seedTrackID);

		short[] seedFeatures = centerWindowFeaturesShort[seedAudioFeaturesTrackIndex];

		for (int trackIndex = 0; trackIndex < maxNumTracks; trackIndex++) {

			if (trackIndex != seedAudioFeaturesTrackIndex) {

				double differenceSum = 0.0;

				short[] testFeatures = centerWindowFeaturesShort[trackIndex];

				for (int f = 0; f < maxNumFeatures; f++) {
					int diff = testFeatures[f] - seedFeatures[f];
					differenceSum += diff * diff * weights[f];
				}

				if (differenceSum < bestDifferenceSums[resultCountMinusOne]) {

					int insertIndex = resultCountMinusOne;

					while (true) {
						if (differenceSum > bestDifferenceSums[insertIndex])
							break;
						insertIndex--;
						if (insertIndex < 0)
							break;
					}
					insertIndex++;

					// make room for insert
					int j = resultCountMinusOne;
					while (j > insertIndex) {
						bestDifferenceSums[j] = bestDifferenceSums[j - 1];
						bestAudioFeaturesTrackIDs[j] = bestAudioFeaturesTrackIDs[j - 1];
						j--;
					}

					// insert
					bestDifferenceSums[insertIndex] = differenceSum;
					bestAudioFeaturesTrackIDs[insertIndex] = audioFeatureTrackIDs[trackIndex];

				}

			}

		}

		return bestAudioFeaturesTrackIDs;
	}

	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//

	public static void main(String args[]) throws Exception {

		AudioFeaturesTar recommender = new AudioFeaturesTar();

		int numTrials = 1000000;
		int numTrialsComplete = 0;

		double averageDifferencePerFeatureSum = 0.0;
		double durationSum = 0.0;

		System.out.println("Starting Trials...");
		int replicationCount = 0;
		for (int trialIndex = 0; trialIndex < numTrials; trialIndex++) {

			int seedAudioFileIndex = (int) (Math.random() * recommender.trackCount);

			int seedTrackID = recommender.audioFeatureTrackIDs[seedAudioFileIndex];

			long startTime = System.currentTimeMillis();

			// int TrackID;
			// String featureString;
			int resultCount = 100;

			int[] bestTrackIDs = recommender.extendPlayListNew(seedTrackID, "TimbreMean:100,TimbreCovariance:100,Loudness:100,Rhythm:100,key:100", resultCount);
			long endTime = System.currentTimeMillis();

			float searchTime = (endTime - startTime) / 1000.0f;

			System.out.println("resultIndex\tbestTrackIDs\tbestDifferenceSums\tbestTrackName\n");
			for (int resultIndex = 0; resultIndex < resultCount; resultIndex++) {

				int seedAlbumNumber = recommender.albumNumbers[seedAudioFileIndex];
				int seedTrackNumber = recommender.trackNumbers[seedAudioFileIndex];

				float difference = (float) (recommender.bestDifferenceSums[resultIndex] / recommender.maxNumFeatures / Short.MAX_VALUE);

				System.out.println(resultIndex + "\t" + bestTrackIDs[resultIndex] + "\t" + difference + "\t" + recommender.trackIDToTrackName.get(bestTrackIDs[resultIndex]));
			}

			String seedTrackName = recommender.trackIDToTrackName.get(seedTrackID);
			int seedAlbumNumber = recommender.albumNumbers[seedAudioFileIndex];
			int seedTrackNumber = recommender.trackNumbers[seedAudioFileIndex];

			System.out.println();
			System.out.println("seedTrackName      = " + seedTrackName);
			System.out.println("seedAudioFileIndex = " + seedAudioFileIndex);
			System.out.println("seedAlbumNumber    = " + seedAlbumNumber);
			System.out.println("seedTrackNumber    = " + seedTrackNumber);

			String seedURL = recommender.getURL(recommender.albumNumbers[seedAudioFileIndex], recommender.trackNumbers[seedAudioFileIndex], "seedURL1");
			System.out.println("seedURL2 = " + seedURL);
//			{
//				System.out.println("Playing Seed Preview");
//				MP3Player mp3Player = new MP3Player(seedURL);
//				mp3Player.play();
//			}

			for (int resultIndex = 0; resultIndex < resultCount; resultIndex++) {

				int bestTrackID = bestTrackIDs[resultIndex];

				int bestAudioFileIndex = recommender.audioFeaturesTrackIDToTrackIndex.get(bestTrackID);

				String bestTrackName = recommender.trackIDToTrackName.get(bestTrackID);

				if (seedTrackName == null || bestTrackName == null) {
					System.out.println("seedTrackName = " + seedTrackName);
					System.out.println("bestTrackName = " + bestTrackName);
					System.out.println("Warning! seed track or best track haS unkown title SKIPPING trial");
					continue;
				}

				numTrialsComplete++;

				float difference = (float) (recommender.bestDifferenceSums[resultIndex] / recommender.maxNumFeatures / Short.MAX_VALUE);

				averageDifferencePerFeatureSum += difference;
				durationSum += searchTime;

				boolean duplicate = false;

				if (difference == 0.0f || seedTrackName.equalsIgnoreCase(bestTrackName) || seedTrackName.startsWith(bestTrackName) || bestTrackName.startsWith(seedTrackName) || seedTrackName.endsWith(bestTrackName) || bestTrackName.endsWith(seedTrackName)) {
					replicationCount++;
					duplicate = true;
				}

				float replicationRate = (float) replicationCount / numTrialsComplete;

				int bestAlbumNumber = recommender.albumNumbers[bestAudioFileIndex];
				int bestTrackNumber = recommender.trackNumbers[bestAudioFileIndex];

				System.out.println();
				System.out.println("bandEnergyMeanDistributionWeight       = " + recommender.bandEnergyMeanDistributionWeight);
				System.out.println("bandEnergyDeviationDistributionWeight  = " + recommender.bandEnergyDeviationDistributionWeight);
				System.out.println("bandEnergyContourDistributionWeight    = " + recommender.bandEnergyContourDistributionWeight);
				System.out.println("rhythmEnergyDistributionWeight         = " + recommender.rhythmEnergyDistributionWeight);
				System.out.println("noteEnergyDistributionWeight           = " + recommender.noteEnergyDistributionWeight);
				System.out.println();
				System.out.println("numTrialsComplete  = " + numTrialsComplete);
				System.out.println("searchTime         = " + searchTime);
				System.out.println("difference         = " + difference);
				System.out.println();
				System.out.println("bestTrackName      = " + bestTrackName);
				System.out.println("bestAudioFileIndex = " + bestAudioFileIndex);
				System.out.println("bestAlbumNumber    = " + bestAlbumNumber);
				System.out.println("bestTrackNumber    = " + bestTrackNumber);
				System.out.println();
				System.out.println("replicationRate    = " + replicationRate);

				String bestURL = recommender.getURL(recommender.albumNumbers[bestAudioFileIndex], recommender.trackNumbers[bestAudioFileIndex], "bestURL1");

				System.out.println("bestURL2 = " + bestURL);

				if (duplicate) {

					System.out.println("Warning! Duplicate track names.  SKIPPING playing");

				} 
// else {
//
//					{
//						System.out.println("Playing Best Match Preview");
//						MP3Player mp3Player = new MP3Player(bestURL);
//						mp3Player.play();
//					}
//
//				}

			}

		}

	}
}
