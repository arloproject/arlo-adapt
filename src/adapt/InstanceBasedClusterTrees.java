package adapt;

public class InstanceBasedClusterTrees {

	/**
	 * @param args
	 */

	int numExamples;
	int numFeatures;
	int numTriesPerSplit;
	int maxDepth;

	int overallNumNodes;
	long overallNumFeatureValues;

	int[][] exampleFeatureValues;

	int numNodes;
	int[] nodeStartExampleIndices;
	int[] nodeEndExampleIndices;

	public void setParameters(int numExamples, int numFeatures, int numTriesPerSplit, int maxDepth) {

		this.numExamples = numExamples;
		this.numFeatures = numFeatures;
		this.numTriesPerSplit = numTriesPerSplit;
		this.maxDepth = maxDepth;

		overallNumFeatureValues = numExamples * numFeatures;

		overallNumNodes = 2 >> maxDepth - 1;

		nodeStartExampleIndices = new int[overallNumNodes];
		nodeEndExampleIndices = new int[overallNumNodes];
		numNodes = 1;
		nodeStartExampleIndices[0] = 0;
		nodeEndExampleIndices[0] = numExamples;

	}

	public void allocateMemory() {
		exampleFeatureValues = new int[numExamples][numFeatures];
	}

	public void setExampleFeatureValues(int exampleIndex, int[] featureValues) {

		System.arraycopy(featureValues, 0, exampleFeatureValues[exampleIndex], 0, numFeatures);

	}

	public void createClusterTree() {

		int startExampleIndex = nodeStartExampleIndices[0];
		int endExampleIndex = nodeEndExampleIndices[0];

		int numNodeExamples = endExampleIndex - startExampleIndex;

		double bestPerformance = Double.NEGATIVE_INFINITY;
		int bestCentroidExampleIndex1 = -1;
		int bestCentroidExampleIndex2 = -1;

		for (int splitTryIndex = 0; splitTryIndex < numTriesPerSplit; splitTryIndex++) {

			int centroidExampleIndex1 = (int) (Math.random() * numNodeExamples);
			int centroidExampleIndex2 = (int) (Math.random() * numNodeExamples);

			// evalaute split

			double exampleDistanceSum1 = 0.0;
			double exampleDistanceSum2 = 0.0;
			int exampleCount1 = 0;
			int exampleCount2 = 0;

			for (int i = startExampleIndex; i < endExampleIndex; i++) {

				double distanceSum1 = 0.0;
				for (int j = 0; j < numFeatures; j++) {
					distanceSum1 += Math.abs(exampleFeatureValues[i][j] - exampleFeatureValues[centroidExampleIndex1][j]);
				}

				double distanceSum2 = 0.0;
				for (int j = 0; j < numFeatures; j++) {
					distanceSum2 += Math.abs(exampleFeatureValues[i][j] - exampleFeatureValues[centroidExampleIndex2][j]);
				}

				if ((distanceSum1 < distanceSum2) || ((distanceSum1 == distanceSum2) && (Math.random() < 0.5))) {
					exampleDistanceSum1 += distanceSum1;
					exampleCount1++;
				} else {
					exampleDistanceSum2 += distanceSum2;
					exampleCount2++;
				}

			}
			
			
			double averageDistance = (exampleDistanceSum1 + exampleDistanceSum2) / numNodeExamples / numFeatures;
			System.out.printf("averageDistance = %f\n", averageDistance);

		}

	}

	public static void main(String[] args) {

		InstanceBasedClusterTrees ibct = new InstanceBasedClusterTrees();

		int numExamples = (int) 28e3;
		int numFeatures = (int) (100 * 128);
		int numTriesPerSplit = 1000;
		int maxDepth = 1;

		int [][] allExampleSpectra = (int[][]) IO.readObject("/data/allExampleSpectra.ser");
		
		ibct.setParameters(numExamples, numFeatures, numTriesPerSplit, maxDepth);

		ibct.allocateMemory();

		int[] featureValues = new int[numFeatures];
		for (int exampleIndex = 0; exampleIndex < numExamples; exampleIndex++) {
			for (int j = 0; j < numFeatures; j++) {
				featureValues[j] = allExampleSpectra[exampleIndex][j];
			}
			ibct.setExampleFeatureValues(exampleIndex, featureValues);
		}

		ibct.createClusterTree();

	}

}
