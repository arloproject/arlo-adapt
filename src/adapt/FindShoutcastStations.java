package adapt;

import java.net.*;
import java.util.Date;
import java.io.*;

public class FindShoutcastStations {
	public static void main(String[] args) throws Exception {
		int MAX_STATION_NUMBER = (int) 10e6;
		int count = 0;
		int maxStationNumber = -1;
		while (true) {

			int randomInt = (int) (Math.random() * MAX_STATION_NUMBER);

			URL streamURL = new URL("http://yp.shoutcast.com/sbin/tunein-station.pls?id=" + randomInt);

			URLConnection shoutcastConnection = null;

			try {
				shoutcastConnection = streamURL.openConnection();

				shoutcastConnection.setReadTimeout(1000);
				// System.out.println(shoutcastConnection.getReadTimeout());

				BufferedReader in = new BufferedReader(new InputStreamReader(shoutcastConnection.getInputStream()));


				String inputLine;

				int lineCount = 0;

				while ((inputLine = in.readLine()) != null) {

					if (inputLine.contains("File1=http://")) {
						
						String httpString = inputLine.substring(6);

						System.out.println(randomInt + "\t" + maxStationNumber + "\t" + (new Date()) + "\t" + httpString);
						System.out.flush();
						
						if (randomInt > maxStationNumber) {
							maxStationNumber = randomInt;
						}
					}
					lineCount++;
				}
				in.close();

			} catch (Exception e) {
				// TODO: handle exception
			}

			count++;

		}
	}
}