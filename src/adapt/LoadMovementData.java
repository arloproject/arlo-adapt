package adapt;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LoadMovementData {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// String fileRoot = "Galapagos Tortoise Programme EOBS with ACC";
		// String fileRoot = "Oilbirds EOBS with ACC";
		String fileRoot = "monkeyACC162";

		// String rawDataFilePath = "/home/dtcheng/move/Tamandua mexicana in Panama EOBS with ACC.csv";
		// String rawDataFilePath = "/home/dtcheng/move/Urban fisher GPS tracking EOBS with ACC.csv";
//		String rawDataFilePath = "/home/dtcheng/move/Galapagos Albatrosses EOBS with ACC.csv";
//		String rawDataFilePath = "/home/dtcheng/move/Oilbirds EOBS with ACC.csv";
//		String rawDataFilePath = "/home/dtcheng/move/Galapagos Swallow-Tailed Gulls Movement Ecology EOBS with ACC.csv";
		String rawDataFilePath = "/home/dtcheng/move/Galapagos Tortoise Programme EOBS with ACC.csv";
		
		
		
		SimpleTable table = null;

		try {
			table = IO.readDelimitedTable(rawDataFilePath, ",");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		int numDataRows = table.numDataRows;
		int numDataColumns = table.numDataColumns;

		System.out.println("numDataRows    = " + numDataRows);
		System.out.println("numDataColumns = " + numDataColumns);

		int numCandidateExamples = 0;
		int[] exampleTableRowIndices = new int[numDataRows];
		int[] exampleNumACCValueFrames = new int[numDataRows];

		int numFrameLengthBins = 1000;
		int[] frameLengthCounts = new int[numFrameLengthBins];

		int numDimensions = -1;

		int timeStampIndex = 0;
		int dataTypeIndex = 3;
		int accRateIndex = 4;
		int accValuesIndex = 5;

		boolean useXYZ = false;
		boolean useXY = true;
		boolean useX = false;

		for (int rowIndex = 0; rowIndex < numDataRows; rowIndex++) {
			

			System.out.println("rowIndex = " + rowIndex);

			String dataTypeString = table.getString(rowIndex, dataTypeIndex);

			boolean hasXYZ = false;
			boolean hasXY = false;
			boolean hasX = false;

			if (!dataTypeString.equals("\"XYZ\"")) {
				hasXYZ = true;
				numDimensions = 3;
			}

			if (!dataTypeString.equals("\"XY\"")) {
				numDimensions = 2;
				hasXY = true;
			}
			if (!dataTypeString.equals("\"X\"")) {
				numDimensions = 1;
				hasX = true;
			}

			if (useXYZ && !hasXYZ) {
				continue;
			}
			if (useXY && !hasXY) {
				continue;
			}
			if (useX && !hasX) {
				continue;
			}

			String timeStampString = table.getString(rowIndex, timeStampIndex);
			String accRateString = table.getString(rowIndex, accRateIndex);
			String accValuesString = table.getString(rowIndex, accValuesIndex).replaceAll("\"", "");

			String[] parts = accValuesString.split(" ");
			int numFrames = parts.length / numDimensions;

			exampleTableRowIndices[numCandidateExamples] = rowIndex;
			exampleNumACCValueFrames[numCandidateExamples] = numFrames;

			frameLengthCounts[numFrames]++;

			numCandidateExamples++;
		}

		System.out.println("numCandidateExamples    = " + numCandidateExamples);

		int maxCount = -1;
		int maxCountIndex = -1;
		for (int i = 0; i < frameLengthCounts.length; i++) {

			if (frameLengthCounts[i] > 0) {
				System.out.println("frameLengthCounts[" + i + "]    = " + frameLengthCounts[i]);
			}

			if (frameLengthCounts[i] > maxCount) {
				maxCount = frameLengthCounts[i];
				maxCountIndex = i;
			}

		}

		System.out.println("maxCount    = " + maxCount);
		System.out.println("maxCountIndex    = " + maxCountIndex);

		int bestNumFrames = maxCountIndex;
		System.out.println("bestFrameLenth    = " + bestNumFrames);

		int numExamples = 0;
		int numFrames = bestNumFrames;

		for (int i = 0; i < numCandidateExamples; i++) {

			if (exampleNumACCValueFrames[i] == numFrames) {

				exampleTableRowIndices[numExamples] = exampleTableRowIndices[i];
				numExamples++;

			}

		}
		System.out.println("numExamples    = " + numExamples);

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

		for (int exampleIndex = 0; exampleIndex < numExamples; exampleIndex++) {

			int rowIndex = exampleTableRowIndices[exampleIndex];

			String timeStampString = table.getString(rowIndex, timeStampIndex);
			Date date = null;
			try {
				date = simpleDateFormat.parse(timeStampString);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			long time = date.getTime();

			// System.out.println("timeStampString = " + timeStampString + "  date = " + date + "  time = " + time);

			String valuesString = table.getString(rowIndex, accValuesIndex).replaceAll("\"", "");

			// System.out.println(valuesString);

			String[] values = valuesString.split(" ");

			System.out.print(time);
			int valueIndex = 0;
			for (int j = 0; j < numFrames; j++) {
				for (int i = 0; i < numDimensions; i++) {

					int value = Integer.parseInt(values[valueIndex++]);
					System.out.print("\t" + value);

				}
			}
			System.out.println();

		}

	}

}
