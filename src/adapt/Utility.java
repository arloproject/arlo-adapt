/*
 * Utility.java
 *
 * Created on January 30, 2006, 9:57 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package adapt;

import java.io.*;
import java.util.*;
import java.util.regex.*;

//import javazoom.spi.mpeg.sampled.file.*;
//import javazoom.jl.player.advanced.*;
import javax.sound.sampled.*;

import Jama.*;

/**
 * 
 * @author dtcheng
 */
public class Utility {

	/** Creates a new instance of Utility */
	public Utility() {
	}

	
	
	

    public static double correlation(double[] xValues, double[] yValues, int numObservations) {

        double correlation = Double.NaN;


        double N = numObservations;
        double sum_sq_x = 0;
        double sum_sq_y = 0;
        double sum_coproduct = 0;
        double mean_x = xValues[0];
        double mean_y = yValues[0];

        for (int i = 2; i <= N; i++) {

            double sweep = (i - 1.0) / i;
            double delta_x = xValues[i - 1] - mean_x;
            double delta_y = yValues[i - 1] - mean_y;
            sum_sq_x += delta_x * delta_x * sweep;
            sum_sq_y += delta_y * delta_y * sweep;
            sum_coproduct += delta_x * delta_y * sweep;
            mean_x += delta_x / i;
            mean_y += delta_y / i;
        }

        double pop_sd_x = Math.sqrt(sum_sq_x / N);
        double pop_sd_y = Math.sqrt(sum_sq_y / N);
        double cov_x_y = sum_coproduct / N;
        correlation = cov_x_y / (pop_sd_x * pop_sd_y);


        return correlation;

    }



    public static double correlation(double[] xValues, double[] yValues, int offsetX, int offsetY, int numObservations) {

        double correlation = Double.NaN;


        double N = numObservations;
        double sum_sq_x = 0;
        double sum_sq_y = 0;
        double sum_coproduct = 0;
        double mean_x = xValues[offsetX];
        double mean_y = yValues[offsetY];

        int iXOffset = -1 + offsetX;
        int iYOffset = -1 + offsetY;
        for (int i = 2; i <= N; i++) {

            double sweep = (i - 1.0) / i;
            double delta_x = xValues[i + iXOffset] - mean_x;
            double delta_y = yValues[i + iYOffset] - mean_y;
            sum_sq_x += delta_x * delta_x * sweep;
            sum_sq_y += delta_y * delta_y * sweep;
            sum_coproduct += delta_x * delta_y * sweep;
            mean_x += delta_x / i;
            mean_y += delta_y / i;
        }

        double pop_sd_x = Math.sqrt(sum_sq_x / N);
        double pop_sd_y = Math.sqrt(sum_sq_y / N);
        double cov_x_y = sum_coproduct / N;
        correlation = cov_x_y / (pop_sd_x * pop_sd_y);


        return correlation;

    }

    public static double correlation(int[] xValues, int[] yValues, int offsetX, int offsetY, int numObservations) {

        double correlation = Double.NaN;


        double N = numObservations;
        double sum_sq_x = 0;
        double sum_sq_y = 0;
        double sum_coproduct = 0;
        double mean_x = xValues[offsetX];
        double mean_y = yValues[offsetY];

        int iXOffset = -1 + offsetX;
        int iYOffset = -1 + offsetY;
        for (int i = 2; i <= N; i++) {

            double sweep = (i - 1.0) / i;
            double delta_x = xValues[i + iXOffset] - mean_x;
            double delta_y = yValues[i + iYOffset] - mean_y;
            sum_sq_x += delta_x * delta_x * sweep;
            sum_sq_y += delta_y * delta_y * sweep;
            sum_coproduct += delta_x * delta_y * sweep;
            mean_x += delta_x / i;
            mean_y += delta_y / i;
        }

        double pop_sd_x = Math.sqrt(sum_sq_x / N);
        double pop_sd_y = Math.sqrt(sum_sq_y / N);
        double cov_x_y = sum_coproduct / N;
        correlation = cov_x_y / (pop_sd_x * pop_sd_y);


        return correlation;

    }
    
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	double[][] fft_1d(double[][] array) {
		double u_r, u_i, w_r, w_i, t_r, t_i;
		int ln, nv2, k, l, le, le1, j, ip, i, n;

		n = array.length;
		ln = (int) (Math.log((double) n) / Math.log(2) + 0.5);
		nv2 = n / 2;
		j = 1;
		for (i = 1; i < n; i++) {
			if (i < j) {
				t_r = array[i - 1][0];
				t_i = array[i - 1][1];
				array[i - 1][0] = array[j - 1][0];
				array[i - 1][1] = array[j - 1][1];
				array[j - 1][0] = t_r;
				array[j - 1][1] = t_i;
			}
			k = nv2;
			while (k < j) {
				j = j - k;
				k = k / 2;
			}
			j = j + k;
		}

		for (l = 1; l <= ln; l++) /* loops thru stages */
		{
			le = (int) (Math.exp((double) l * Math.log(2)) + 0.5);
			le1 = le / 2;
			u_r = 1.0;
			u_i = 0.0;
			w_r = Math.cos(Math.PI / (double) le1);
			w_i = -Math.sin(Math.PI / (double) le1);
			for (j = 1; j <= le1; j++) /* loops thru 1/2 twiddle values per stage */
			{
				for (i = j; i <= n; i += le) /* loops thru points per 1/2 twiddle */
				{
					ip = i + le1;
					t_r = array[ip - 1][0] * u_r - u_i * array[ip - 1][1];
					t_i = array[ip - 1][1] * u_r + u_i * array[ip - 1][0];

					array[ip - 1][0] = array[i - 1][0] - t_r;
					array[ip - 1][1] = array[i - 1][1] - t_i;

					array[i - 1][0] = array[i - 1][0] + t_r;
					array[i - 1][1] = array[i - 1][1] + t_i;
				}
				t_r = u_r * w_r - w_i * u_i;
				u_i = w_r * u_i + w_i * u_r;
				u_r = t_r;
			}
		}
		return array;
	} /* end of FFT_1d */

	static public int randomInt(Random generator, int min, int max) {
		return (int) ((generator.nextDouble() * (max - min + 1)) + min);
	}

	static public int[] randomIntArray(int seed, int numElements) {

		Random generator = null;
		if (seed < 0)
			generator = new Random();
		else
			generator = new Random(seed);

		return randomIntArray(generator, numElements);
	}

	static public void randomIntArray(Random generator, int numElements, int[] data) {

		for (int i = 0; i < numElements; i++) {
			data[i] = i;
		}

		int randomIndex;
		int temp;
		for (int i = 0; i < numElements - 1; i++) {
			randomIndex = randomInt(generator, i + 1, numElements - 1);
			temp = data[i];
			data[i] = data[randomIndex];
			data[randomIndex] = temp;
		}

	}

	static void randomizeIntArrayInPlace(Random generator, int[] data, int numElements) {

		int randomIndex;
		int temp;
		for (int i = 0; i < numElements - 1; i++) {
			randomIndex = randomInt(generator, i + 1, numElements - 1);
			temp = data[i];
			data[i] = data[randomIndex];
			data[randomIndex] = temp;
		}

	}

	static void randomizeStringArrayInPlace(Random generator, String[] data, int numElements) {
		int randomIndex;
		String temp;
		for (int i = 0; i < numElements - 1; i++) {
			randomIndex = randomInt(generator, i + 1, numElements - 1);
			temp = data[i];
			data[i] = data[randomIndex];
			data[randomIndex] = temp;
		}
	}
	
	static public int[] randomIntArray(Random generator, int numElements) {

		int[] data = new int[numElements];

		for (int i = 0; i < numElements; i++) {
			data[i] = i;
		}

		int randomIndex;
		int temp;
		for (int i = 0; i < numElements - 1; i++) {
			randomIndex = randomInt(generator, i + 1, numElements - 1);
			temp = data[i];
			data[i] = data[randomIndex];
			data[randomIndex] = temp;
		}

		return data;
	}

	static public int[] randomizedIntArray(int seed, int[] intArray, int size) {

		// int size = intArray.length;

		int[] randomizedIntArray = new int[size];

		int[] randomIntArray = randomIntArray(seed, size);

		for (int i = 0; i < size; i++) {
			randomizedIntArray[i] = intArray[randomIntArray[i]];
		}

		return randomizedIntArray;

	}

	static public int[] randomizedIntArray(Random random, int[] intArray, int size) {

		// int size = intArray.length;

		int[] randomizedIntArray = new int[size];

		int[] randomIntArray = randomIntArray(random, size);

		for (int i = 0; i < size; i++) {
			randomizedIntArray[i] = intArray[randomIntArray[i]];
		}

		return randomizedIntArray;

	}

	static public Vector randomizedVector(int seed, Vector vector) {

		int size = vector.size();

		Vector randomizedVector = new Vector(size);

		int[] randomIntArray = randomIntArray(seed, size);

		for (int i = 0; i < size; i++) {
			randomizedVector.add(vector.elementAt(randomIntArray[i]));
		}

		return randomizedVector;

	}

	// are replaced with a space.
	public static CharSequence removeNonWordCharacters(CharSequence inputStr) {
		String patternStr = "\\W";
		String replaceStr = "";
		Pattern pattern = Pattern.compile(patternStr);
		Matcher matcher = pattern.matcher(inputStr);
		return matcher.replaceAll(replaceStr);
	}

	static long startTime;

	static public void resetClock() {
		startTime = System.currentTimeMillis();
	}

	static public void reportDuration() {
		long endTime = System.currentTimeMillis();
		double duration = (endTime - startTime) / 1000.0;
		System.out.println("duration = " + duration);
		startTime = System.currentTimeMillis();
	}

	static public void reportDuration(String string) {
		long endTime = System.currentTimeMillis();
		double duration = (endTime - startTime) / 1000.0;
		System.out.println(string + " duration = " + duration);
		startTime = System.currentTimeMillis();
	}

	static long startTime1;

	static public void resetClock1() {
		startTime1 = System.currentTimeMillis();
	}

	static public void reportDuration1() {
		long endTime = System.currentTimeMillis();
		double duration = (endTime - startTime1) / 1000.0;
		System.out.println("duration = " + duration);
		startTime1 = System.currentTimeMillis();
	}

	static public void reportDuration1(String string) {
		long endTime = System.currentTimeMillis();
		double duration = (endTime - startTime1) / 1000.0;
		System.out.println(string + " duration = " + duration);
		startTime1 = System.currentTimeMillis();
	}

	static public double getDuration1() {
		long endTime = System.currentTimeMillis();
		return (endTime - startTime1) / 1000.0;
	}

	static long startTime2;

	static public void resetClock2() {
		startTime2 = System.currentTimeMillis();
	}

	static public void reportDuration2() {
		long endTime = System.currentTimeMillis();
		double duration = (endTime - startTime2) / 1000.0;
		System.out.println("duration = " + duration);
		startTime2 = System.currentTimeMillis();
	}

	static public void reportDuration2(String string) {
		long endTime = System.currentTimeMillis();
		double duration = (endTime - startTime2) / 1000.0;
		System.out.println(string + " duration = " + duration);
		startTime2 = System.currentTimeMillis();
	}

	static public double getDuration2() {
		long endTime = System.currentTimeMillis();
		return (endTime - startTime2) / 1000.0;
	}

	static public double[] multipleRegression(double[][] xValues, double[] yValues) {

		int numExamples = xValues.length;
		int numInputs = xValues[0].length;

		double[][] aArray = new double[numExamples][1 + numInputs];
		double[][] bArray = new double[numExamples][1];

		for (int i = 0; i < numExamples; i++) {
			for (int j = 0; j < numInputs; j++) {
				aArray[i][j] = xValues[i][j];
			}
			aArray[i][numInputs] = 1.0;
		}

		for (int i = 0; i < numExamples; i++) {
			bArray[i][0] = yValues[i];
		}

		Matrix A = new Matrix(aArray);
		Matrix b = new Matrix(bArray);
		Matrix x = A.solve(b);
		double[][] matrix = x.getArray();

		double[] weights = new double[numInputs + 1];

		for (int i = 0; i < numInputs + 1; i++) {

			weights[i] = matrix[i][0];

			// if (i < numInputs) {
			// System.out.println("weight #" + (i + 1) + " = " + weights[i]);
			// } else {
			// System.out.println("offset = " + weights[i]);
			// }

		}

		// System.out.println(x);

		return weights;
	}

	static public double computeSampleMean(double[] values, int numObservations) {

		double sum = 0.0;
		for (int i = 0; i < numObservations; i++) {
			sum += values[i];
		}
		return sum / numObservations;

	}

	static public double computeSampleSTD(double[] values, double mean, int numObservations) {

		double varianceSum = 0.0;
		for (int i = 0; i < numObservations; i++) {
			double diff = values[i] - mean;
			varianceSum += diff * diff;
		}

		return Math.sqrt(varianceSum / (numObservations - 1));

	}

	static public double computeStandardErrorOfMean(double std, int numObservations) {

		return std / Math.sqrt(numObservations);

	}
	

	static public String formatString(String string, int size) {

		if (string == null) {
			string = "<null>";
		}

		byte[] byteString = new byte[size];

		for (int i = 0; i < size; i++) {
			if (i < string.length())
				byteString[i] = (byte) string.charAt(i);
			else
				byteString[i] = (byte) ' ';
		}

		return new String(byteString);
	}

	static public String frontPadString(String string, int size, char padChar) {

		byte[] byteString = new byte[size];
		int numToPad = size - string.length();

		for (int i = 0; i < size; i++) {
			if (i < numToPad)
				byteString[i] = (byte) padChar;
			else
				byteString[i] = (byte) string.charAt(i - numToPad);
		}

		return new String(byteString);
	}

}
