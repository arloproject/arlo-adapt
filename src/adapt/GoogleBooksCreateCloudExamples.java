package adapt;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Array;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

import com.google.gdata.client.GoogleService;
import com.google.gdata.client.books.BooksService;
import com.google.gdata.client.books.VolumeQuery;
import com.google.gdata.client.calendar.CalendarService;
import com.google.gdata.data.books.VolumeEntry;
import com.google.gdata.data.books.VolumeFeed;
import com.google.gdata.data.calendar.CalendarEntry;
import com.google.gdata.data.calendar.CalendarFeed;
import com.google.gdata.data.dublincore.Title;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;

public class GoogleBooksCreateCloudExamples {

//	private static String volumeMetaDataFilePath = "/data/text/google/dlc.out.v2";
	private static int reportInterval = 1000;
	private static int maxCount = 999999999;
//	private static int maxCount = 100;

	public static int maxNumTerms = (int) 2e6;
	public static int maxNumVolumeTerms = 1000;
	public static int maxTermSize = 1000;
	public static int maxNumCharacters = 256;
	public static FastHashTable fastHashTable = new FastHashTable(maxTermSize, maxNumCharacters);

	/**
	 * @param args
	 */
	public static void main(String[] args) {


		  String volumeMetaDataFilePath = args[0];

		Hashtable<Long, int[]> terms = new Hashtable<Long, int[]>(); // 0: initialIndex, 1: count, 2: sortIndex, 3: reported

		for (int phaseIndex = 0; phaseIndex < 2; phaseIndex++) {
			
			System.out.println("Phase " + phaseIndex + " starting.");

			long[] volumeTerms = new long[maxNumVolumeTerms];

			BufferedReader bufferedReader = null;
			try {
				bufferedReader = new BufferedReader(new FileReader(new File(volumeMetaDataFilePath)));
			} catch (FileNotFoundException e3) {
				// TODO Auto-generated catch block
				e3.printStackTrace();
			}

			int count = 0;
			int numExamples = 0;
			int numVolumes = 0;

			int numTerms = 0;
			int maxTermSize = -1;
			int cloudSizeSum = 0;
			int maxCloudSize = -1;
			while (true) {

				int cloudSizeAvg = -1;

				if (numExamples > 0) {
					cloudSizeAvg = cloudSizeSum / numExamples;
				}

				if (count > maxCount) {
					break;
				}
				String line = null;
				try {
					line = bufferedReader.readLine();
				} catch (IOException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				if (line == null) {
					break;
				}

				String[] lineParts = line.split("\t");

				String volumeKey = lineParts[0];

				String data = lineParts[3];

				System.out.println("MONITOR" + "\t" + "volumeKey     = " + volumeKey);
				System.out.println("MONITOR" + "\t" + "count         = " + count);
				System.out.println("MONITOR" + "\t" + "numExamples   = " + numExamples);
				System.out.println("MONITOR" + "\t" + "cloudSizeAvg  = " + cloudSizeAvg);
				System.out.println("MONITOR" + "\t" + "maxCloudSize  = " + maxCloudSize);
				System.out.println("MONITOR" + "\t" + "maxTermSize   = " + maxTermSize);
				System.out.println("MONITOR" + "\t" + "ratio         = " + (float) numExamples / (float) count);
				System.out.println("MONITOR" + "\t" + "terms.size()  = " + terms.size());
				count++;

				int index1 = data.indexOf("<div class=cloud>");
				if (index1 == -1) {
					continue;
				}
				int index2 = data.indexOf("</div></div></div></div><div class=vertical_module_list_row>", index1);
				if (index2 == -1) {
					continue;
				}

				String cloudHTML = null;
				cloudHTML = data.substring(index1, index2);

				if (true) {
					// System.out.println();
					// System.out.println(key);
					// System.out.println(data);
					// System.out.println(index1);
					// System.out.println(index2);
					// System.out.println(cloudHTML);

					int cloudSize = 0;
					int lastIndex = index1 + "<div class=cloud>".length();
					int volumeTermIndex = 0;
					while (true) {
						String searchString = "class=cloud";

						int wordIndex1 = data.indexOf(searchString, lastIndex);
						if (wordIndex1 == -1) {
							break;
						}

						wordIndex1 += searchString.length();

						int level = Integer.parseInt(data.substring(wordIndex1, wordIndex1 + 1));

						wordIndex1 += 2;

						int wordIndex2 = data.indexOf("</a>", wordIndex1);

						if (level < 99) {

							String term = data.substring(wordIndex1, wordIndex2);
							// term = term.toLowerCase();

							int termSize = term.length();
							if (termSize > maxTermSize) {
								maxTermSize = termSize;
							}

							// long hashCode = term.hashCode();
							long hashCode = fastHashTable.computeHashCode(term.getBytes());

							// Integer termIndex = terms.get(hashCode);
							int[] values = terms.get(hashCode);

							int termIndex = -1;
							switch (phaseIndex) {

							case 0:
								if (values == null) {
									System.out.println("TERM" + "\t" + numTerms + "\t" + hashCode + "\t" + term);
									termIndex = numTerms++;

									values = new int[] { termIndex, 0, -1, 0};
									terms.put(hashCode, values);
								} else {
									termIndex = values[0];
								}
								values[1]++;

								break;

							case 1:
								termIndex = values[2];
								
								if (values[3] == 0) {

									System.out.println("SORT" + "\t" + hashCode + "\t" + termIndex + "\t" + term);
									numTerms++;
									
									values[3] = 1;
								}
								
								break;
							}

							volumeTerms[volumeTermIndex++] = termIndex;

							cloudSize++;

						}

						lastIndex = wordIndex2;

					}

					switch (phaseIndex) {

					case 0:

						break;

					case 1:

						Arrays.sort(volumeTerms, 0, volumeTermIndex);

						System.out.print("EXAMPLE" + "\t" + numExamples + "\t" + volumeKey + "\t" + volumeTermIndex);
						for (int i = 0; i < volumeTermIndex; i++) {
							System.out.print("\t" + volumeTerms[i]);
						}
						System.out.println();

						break;
					}

					if (cloudSize > maxCloudSize) {
						maxCloudSize = cloudSize;
					}

					cloudSizeSum += cloudSize;

					// System.out.println();
				}

				numExamples++;

				numVolumes++;

			}
			try {
				bufferedReader.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			switch (phaseIndex) {

			case 0:

				CountAndHashcode[] countAndHashcodes = new CountAndHashcode[numTerms];

				Iterator<Entry<Long, int[]>> iterator = terms.entrySet().iterator();

				for (int i = 0; i < numTerms; i++) {
					Entry<Long, int[]> entry = iterator.next();
					int[] values = entry.getValue();
					long hashcode = entry.getKey();
					countAndHashcodes[i] = new CountAndHashcode(values[1], hashcode);
				}

				Arrays.sort(countAndHashcodes);

				for (int i = 0; i < numTerms; i++) {
					int[] values = terms.get(countAndHashcodes[i].hashcode);
					values[2] = i;
				}

				break;

			case 1:

				break;
			}

			System.out.println("Phase " + phaseIndex + " finished.");
		}

		System.out.println("All Phases Finished!");
	}
}

class CountAndHashcode implements Comparable<CountAndHashcode> {

	int count;
	long hashcode;

	CountAndHashcode(int count, long hashcode) {
		this.count = count;
		this.hashcode = hashcode;
	}

	public int compareTo(CountAndHashcode o) {

		if (this.count < o.count)
			return 1;
		if (this.count > o.count)
			return -1;

		return 0;
	}

}
