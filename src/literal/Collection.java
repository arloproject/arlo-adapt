package literal;
import types.*;

import java.util.HashMap;
import java.util.Map;


public class Collection {
	
	public static <K,V> Map<K,V> Map( Tuple2<K,V> ... entries ){
		Map<K,V> map = new HashMap<K,V>();

		for( Tuple2<K,V> entry : entries ){
			map.put( entry._1 , entry._2 );
		}
		return map;
	}
}
