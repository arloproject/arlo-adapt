package flows.workflows;

import java.io.IOException;
import java.util.HashMap;

import flows.modules.*;

public class LayeredLearning {

	// thor 1
	// fastestMode
	// timeElapsedInSeconds = 107.202

	// thor 2
	// fastestMode
	// timeElapsedInSeconds = 56.154

	// thor 4
	// fastestMode
	// timeElapsedInSeconds = 56.154

	// thor 8
	// fastMode = true
	// timeElapsedInSeconds = 128.491

	// thor 16
	// fastMode = true
	// timeElapsedInSeconds = 47.89

	// thor 32
	// fastMode = true
	// timeElapsedInSeconds = 26.308
	// timeElapsedInSeconds = 29.98
	// timeElapsedInSeconds = 28.239
	// moduleExecutionsPerSecond = 1.4306455656361768E9

	// thor 64
	// fastMode = true
	// timeElapsedInSeconds = 24.069
	// timeElapsedInSeconds = 23.571
	// timeElapsedInSeconds = 23.395
	// moduleExecutionsPerSecond = 1.7268647256251335E9

	// thor 128
	// fastMode = true
	// timeElapsedInSeconds = 22.503
	// timeElapsedInSeconds = 22.623
	// timeElapsedInSeconds = 22.358
	//
	// numModulesExecuted = 40400000512
	// timeElapsedInSeconds = 22.372
	// moduleExecutionsPerSecond = 1.8058287373502593E9

	// thor 256
	// timeElapsedInSeconds = 21.893
	// timeElapsedInSeconds = 22.079
	// numModulesExecuted = 40400001024
	// timeElapsedInSeconds = 22.093
	// moduleExecutionsPerSecond = 1.8286335501742632E9
	//
	// no exec count
	// timeElapsedInSeconds = 19.183
	//
	// fastest; no memory restrictions
	// numModulesExecuted = 404000001024
	// timeElapsedInSeconds = 216.602
	// moduleExecutionsPerSecond = 1.865172071467484E9

	// thor 512
	// numModulesExecuted = 40399898624
	// timeElapsedInSeconds = 22.734
	// moduleExecutionsPerSecond = 1.7770695268760445E9
	// numModulesExecuted = 40399898624
	// timeElapsedInSeconds = 22.745
	// moduleExecutionsPerSecond = 1.7762100955814464E9
	//
	// fastest; no memory restrictions
	// numModulesExecuted = 404000002048
	// timeElapsedInSeconds = 209.435
	// moduleExecutionsPerSecond = 1.928999460682312E9
	// numModulesExecuted = 404000002048
	// timeElapsedInSeconds = 208.796
	// moduleExecutionsPerSecond = 1.9349029772984157E9
	// numModulesExecuted = 404000002048
	// timeElapsedInSeconds = 208.448
	// moduleExecutionsPerSecond = 1.9381332612833896E9
	// numModulesExecuted = 404000002048
	// timeElapsedInSeconds = 209.104
	// moduleExecutionsPerSecond = 1.9320529595225341E9
	// numModulesExecuted = 404000002048
	// timeElapsedInSeconds = 208.356
	// moduleExecutionsPerSecond = 1.938989047821997E9
	// numModulesExecuted = 404000002048
	// timeElapsedInSeconds = 208.747
	// moduleExecutionsPerSecond = 1.935357164644282E9
	// no counter
	// timeElapsedInSeconds = 185.021

	// thor 16
	// fastMode = true
	// timeElapsedInSeconds = 57.361
	// timeElapsedInSeconds = 64.344
	// timeElapsedInSeconds = 40.059

	// thor 32
	// fastMode = true
	// timeElapsedInSeconds = 58.943
	// timeElapsedInSeconds = 36.067
	// timeElapsedInSeconds = 53.696
	//
	// thor 64 (unstable)
	// numModulesExecuted = 40400000256
	// timeElapsedInSeconds = 41.143
	// moduleExecutionsPerSecond = 9.819410411491627E8
	// numModulesExecuted = 40400000256
	// timeElapsedInSeconds = 39.419
	// moduleExecutionsPerSecond = 1.024886482559172E9
	// fastMode = true
	// timeElapsedInSeconds = 33.36
	// timeElapsedInSeconds = 32.732
	// timeElapsedInSeconds = 33.056
	// timeElapsedInSeconds = 53.554

	//
	// thor 128 (unstable)
	// numModulesExecuted = 40400000512
	// timeElapsedInSeconds = 40.249
	// moduleExecutionsPerSecond = 1.0037516587244403E9
	// numModulesExecuted = 40400000512
	// timeElapsedInSeconds = 38.747
	// moduleExecutionsPerSecond = 1.0426613805455906E9
	// numModulesExecuted = 40400000512
	// timeElapsedInSeconds = 40.641
	// moduleExecutionsPerSecond = 9.940700404025491E8
	// fastMode = true
	// timeElapsedInSeconds = 64.809
	// timeElapsedInSeconds = 33.896

	//
	// thor 256 (unstable)
	// numModulesExecuted = 40400001024
	// timeElapsedInSeconds = 38.436
	// moduleExecutionsPerSecond = 1.0510979556665626E9
	// numModulesExecuted = 40400001024
	// timeElapsedInSeconds = 39.864
	// moduleExecutionsPerSecond = 1.0134457411198074E9
	// fastMode = true
	// timeElapsedInSeconds = 31.857
	// timeElapsedInSeconds = 31.492
	// timeElapsedInSeconds = 55.375
	// timeElapsedInSeconds = 31.766
	// timeElapsedInSeconds = 55.469
	// timeElapsedInSeconds = 51.335
	// timeElapsedInSeconds = 54.045
	// timeElapsedInSeconds = 31.761
	//
	// timeElapsedInSeconds = 53.292
	// timeElapsedInSeconds = 54.828
	// timeElapsedInSeconds = 33.231
	// timeElapsedInSeconds = 52.81
	// timeElapsedInSeconds = 31.938
	// timeElapsedInSeconds = 52.808
	// timeElapsedInSeconds = 32.539
	//
	// uber fast
	// numModulesExecuted = 404000001024
	// timeElapsedInSeconds = 178.152
	// moduleExecutionsPerSecond = 2.2677264416004314E9
	//
	// numModulesExecuted = 404000001024
	// estimatedNumModulesExecuted = 404000001024
	// timeElapsedInSeconds = 177.631
	// moduleExecutionsPerSecond = 2.2743777889219785E9
	// numModulesExecuted = 404000001024
	// estimatedNumModulesExecuted = 404000001024
	// timeElapsedInSeconds = 177.38
	// moduleExecutionsPerSecond = 2.2775961270943737E9
	// numModulesExecuted = 404000001024
	// estimatedNumModulesExecuted = 404000001024
	// timeElapsedInSeconds = 177.553
	// moduleExecutionsPerSecond   = 2.2753769354727883E9


	// thor 512
	// timeElapsedInSeconds = 33.355

	// numModulesExecuted = 40400001024
	// timeElapsedInSeconds = 39.116
	// moduleExecutionsPerSecond = 1.0328254684528071E9

	// forge 256t; 160m;
	// numModulesExecuted = 404000001024
	// timeElapsedInSeconds = 18.1******
	// moduleExecutionsPerSecond = 2.23E9******
	// fastMode = true
	// timeElapsedInSeconds = 16.33
	// timeElapsedInSeconds = 18.16
	// timeElapsedInSeconds = 18.58
	// timeElapsedInSeconds = 15.86
	// timeElapsedInSeconds = 19.37
	// timeElapsedInSeconds = 16.16

	// forge 64t; 160m;
	// timeElapsedInSeconds = 18.26
	// timeElapsedInSeconds = 19.14
	// timeElapsedInSeconds = 18.44

	// forge 128t; 160m;
	// timeElapsedInSeconds = 23.38
	//
	// fastest mode 1e9 observations
	// timeElapsedInSeconds = 119.60
	// moduleExecutionsPerSecond = 3.37e9
	// timeElapsedInSeconds = 108.92
	// moduleExecutionsPerSecond = 3.70e9
	//
	// forge 128t; no memory restrictions
	// timeElapsedInSeconds = 118.32
	// moduleExecutionsPerSecond = 3.4e9

	// forge 256t; 160m;
	// fastest mode 1e9 observations
	// timeElapsedInSeconds = 101.03
	// moduleExecutionsPerSecond = 3.99e9
	// timeElapsedInSeconds = 107.89
	// moduleExecutionsPerSecond = 3.74e9
	//
	// forge 256t; no memory restrictions
	// timeElapsedInSeconds = 93.19
	// moduleExecutionsPerSecond = 4.33e9
	// uber fast
	// numModulesExecuted = 404000001024
	// timeElapsedInSeconds = 104.29
	// moduleExecutionsPerSecond = 3.87E9

	// forge 512t; 160m;
	// timeElapsedInSeconds = 16.59
	// timeElapsedInSeconds = 23.??
	// timeElapsedInSeconds = 23.95
	//
	// fastest mode 1e9 observations
	// timeElapsedInSeconds = 101.14
	// moduleExecutionsPerSecond = 3.99e9
	// timeElapsedInSeconds = 101.19
	// moduleExecutionsPerSecond = 3.99e9
	//
	// forge 512t; no memory restrictions
	// timeElapsedInSeconds = 91.83
	// moduleExecutionsPerSecond = 4.39e9
	// timeElapsedInSeconds = 97.02
	// moduleExecutionsPerSecond = 4.16e9
	// timeElapsedInSeconds = 88.69
	// moduleExecutionsPerSecond = 4.55e9
	// uber fast
	// numModulesExecuted = 404000001024
	// timeElapsedInSeconds = 102.98
	// moduleExecutionsPerSecond = 3.92E9
	// uber fast ; 25m ; no count
	// numModulesExecuted = 404000001024
	// timeElapsedInSeconds = 99.22
	// moduleExecutionsPerSecond = 3.97e9?
	// timeElapsedInSeconds = 96.82
	// moduleExecutionsPerSecond = 4.17e9

	// forge 1024t; 160m;
	// fastest mode 1e9 observations
	// timeElapsedInSeconds = 105.43
	// moduleExecutionsPerSecond = 3.83e9
	//
	// forge 1024t; no memory restrictions
	// timeElapsedInSeconds = 91.13
	// moduleExecutionsPerSecond = 4.43e9

	/*************************/
	/* EXPERIMENT PARAMETERS */
	/*************************/

	static final int controllerSleepTime = 10;
	static final boolean countExecutions = true; // sometimes it is faster to do this!?!
	static final boolean reportCounts = false; // 
	static final boolean estimateCounts = true; // 

	static final int executionMode = Floe.FASTEST_MODE;

	static int numThreads = 256; // 32 (sun); 128 is fastest on openjdk and 1.7 (unstable)
	static int totalNumObservations = (int) 1000e6;// 32 is fastest
	static int numObservations = totalNumObservations / numThreads;

	static final int optimizerNumBiasesToTry = 100;
	static final int maxNumThreadsToReport = 8;

	// memory scope
	/*******************/
	/* STRING LITERALS */
	/*******************/

	// memory scope

	static final String GlobalMemory = Floe.GlobalMemory;
	static final String ThreadMemory = Floe.ThreadMemory;

	// memory behavior:

	static final String ReadOnly = Floe.ReadOnly;
	static final String Volitile = Floe.Volitile;

	// memory data type

	static final String FloatArray = Floe.FloatArray;
	static final String DoubleArray = Floe.DoubleArray;
	static final String IntegerArray = Floe.IntegerArray;
	static final String LongArray = Floe.LongArray;

	/*************/
	/* VARIABLES */
	/*************/

	// global

	static final String AllData = "AllData";
	static final String NumExamples = "NumExamples";
	static final String NumInVars = "NumInVars";
	static final String NumOutVars = "NumOutVars";
	static final String L0_NumObservationsToMake = "L0_NumObservations";
	static final String L1_OptimizerExecutionCount = "L1_OptimizerExecutionCount";
	static final String L0_TrainingFraction = "L0_TrainingFraction";
	static final String L1_OptimizerNumBiasesToTry = "L1_OptimizerNumBiasesToTry";
	static final String L1_OptimizerNumTrialsPerBias = "L1_OptimizerNumTrialsPerBias";
	static final String L1_TrainingFraction = "L1_TrainingFraction";
	static final String DeltaPerformances = "DeltaPerformances";

	// thread

	// level 0
	static final String L0_NumTrainExamples = "L0_NumTrainExamples";
	static final String L0_NumTestExamples = "L0_NumTestExamples";
	static final String L0_TrainIndices = "L0_TrainIndices";
	static final String L0_TestIndices = "L0_TestIndices";
	static final String L0_Model = "L0_Model";
	static final String L0_ActualPerformance = "L0_ActualPerformance";
	static final String L0_ObservationIndex = "L0_ObservationIndex";

	// level 1
	static final String L1_OptimizerBiasIndex = "L1_OptimizerBiasIndex";
	static final String L1_OptimizerBiasTrialIndex = "L1_OptimizerBiasTrialIndex";
	static final String L1_NumTrainExamples = "L1_NumTrainExamples";
	static final String L1_NumTestExamples = "L1_NumTestExamples";
	static final String L1_TrainIndices = "L1_TrainIndices";
	static final String L1_TestIndices = "L1_TestIndices";
	static final String L1_Bias = "L1_Bias";
	static final String L1_Performance = "L1_Performance";
	static final String L1_Model = "L1_Model";
	static final String L1_BestBias = "L1_BestBias";
	static final String L1_BestBiasPerformance = "L1_BestBiasPerformance";

	/***********************/
	/* MODULES CLASS NAMES */
	/***********************/

	static final String ReadExamples = "ReadExamples";
	static final String SetIntegerToZero = "SetIntegerToZero";
	static final String Observer = "Observer";
	static final String End = "End";
	static final String Split = "Split";
	static final String InitializeOptimizer = "InitializeOptimizer";
	static final String Optimizer = "Optimizer";
	static final String CreateModel = "CreateModel";
	static final String TestModel = "TestModel";
	static final String SaveResults = "SaveResults";

	/**************************/
	/* MODULES INSTANCE NAMES */
	/**************************/

	static final String L0_ReadAllExamples = "L0_ReadAllExamples";
	static final String L0_InitializeObserver = "L0_InitializeObserver";
	static final String L0_Observer = "L0_Observer";
	static final String L0_End = "L0_End";
	static final String L0_Split = "L0_Split";
	static final String L1_InitializeOptimizer = "L1_InitializeOptimizer";
	static final String L1_Optimizer = "L1_Optimizer";
	static final String L1_Split = "L1_Split";

	static final String L1_CreateModel = "L1_CreateModel";
	static final String L1_TestModel = "L1_TestModel";
	static final String L0_CreateModel = "L0_CreateModel";
	static final String L0_TestModel = "L0_TestModel";
	static final String L0_SaveResults = "L0_SaveResults";

	public static void main(String args[]) throws IOException {

		if (args.length > 0) {
			numThreads = Integer.parseInt(args[0]);
		}
		if (args.length > 1) {
			totalNumObservations = Integer.parseInt(args[1]);
		}

		numObservations = totalNumObservations / numThreads;

		/*********************/
		/** VARIABLES **/
		/*********************/

		Object[][] variables = new Object[][] {

				//

				/********************/
				/* GLOBAL VARIABLES */
				/********************/

				{ AllData, GlobalMemory, ReadOnly, DoubleArray, 1, null },

				{ NumExamples, GlobalMemory, ReadOnly, IntegerArray, 1, null },

				{ NumInVars, GlobalMemory, ReadOnly, IntegerArray, 1, null },

				{ NumOutVars, GlobalMemory, ReadOnly, IntegerArray, 1, null },

				{ NumOutVars, GlobalMemory, ReadOnly, IntegerArray, 1, null },

				{ L0_NumObservationsToMake, GlobalMemory, ReadOnly, IntegerArray, 1, new int[] { LayeredLearning.numObservations } },

				// { L0_ObservationIndex, GlobalMemory, Volitile, Integer, 1, null },
				{ L0_ObservationIndex, ThreadMemory, Volitile, IntegerArray, 1, null },

				// { L1_OptimizerExecutionCount, GlobalMemory, Volitile, Integer, 1, null },
				{ L1_OptimizerExecutionCount, ThreadMemory, Volitile, LongArray, 1, null },

				{ L0_TrainingFraction, GlobalMemory, ReadOnly, DoubleArray, 1, null },

				{ L1_OptimizerNumBiasesToTry, GlobalMemory, ReadOnly, IntegerArray, 1, new int[] { LayeredLearning.optimizerNumBiasesToTry } },

				{ L1_OptimizerNumTrialsPerBias, GlobalMemory, ReadOnly, IntegerArray, 1, null },

				{ L1_TrainingFraction, GlobalMemory, ReadOnly, DoubleArray, 1, null },

				{ DeltaPerformances, GlobalMemory, Volitile, DoubleArray, 1, null },
				/*****************************/
				/* VOLITILE THREAD VARIABLES */
				/*****************************/

				// level 0

				{ L0_NumTrainExamples, ThreadMemory, Volitile, IntegerArray, 1, null },

				{ L0_NumTestExamples, ThreadMemory, Volitile, IntegerArray, 1, null },

				{ L0_TrainIndices, ThreadMemory, Volitile, IntegerArray, 1, null },

				{ L0_TestIndices, ThreadMemory, Volitile, IntegerArray, 1, null },

				{ L0_Model, ThreadMemory, Volitile, IntegerArray, 1, null },

				{ L0_ActualPerformance, ThreadMemory, Volitile, DoubleArray, 1, null },
				// level 1

				{ L1_OptimizerBiasIndex, ThreadMemory, Volitile, IntegerArray, 1, null },

				{ L1_OptimizerBiasTrialIndex, ThreadMemory, Volitile, IntegerArray, 1, null },

				{ L1_NumTrainExamples, ThreadMemory, Volitile, IntegerArray, 1, null },

				{ L1_NumTestExamples, ThreadMemory, Volitile, IntegerArray, 1, null },

				{ L1_TrainIndices, ThreadMemory, Volitile, IntegerArray, 1, null },

				{ L1_TestIndices, ThreadMemory, Volitile, IntegerArray, 1, null },

				{ L1_Bias, ThreadMemory, Volitile, DoubleArray, 1, null },

				{ L1_Performance, ThreadMemory, Volitile, DoubleArray, 1, null },

				{ L1_Model, ThreadMemory, Volitile, DoubleArray, 1, null },

				{ L1_BestBias, ThreadMemory, Volitile, DoubleArray, 1, null },

				{ L1_BestBiasPerformance, ThreadMemory, Volitile, DoubleArray, 1, null },

		};

		/*************/
		/** MODULES **/
		/*************/

		Object[][][] modules = new Object[][][] {

				//

				{ { L0_ReadAllExamples }, { ReadExamples }, {}, { AllData, NumExamples, NumInVars, NumOutVars } },

				{ { L0_InitializeObserver }, { SetIntegerToZero }, {}, { L0_ObservationIndex } },

				{ { L0_Observer }, { Observer }, { L0_NumObservationsToMake, L0_ObservationIndex }, { L0_ObservationIndex } },

				{ { L0_End }, { End }, {}, {} },

				{ { L0_Split }, { Split }, { NumExamples, NumInVars, NumOutVars, L0_TrainingFraction }, { L0_NumTrainExamples, L0_NumTestExamples } },

				{ { L1_InitializeOptimizer }, { SetIntegerToZero }, {}, { L1_OptimizerBiasIndex } },

				{ { L1_Optimizer }, { Optimizer }, { L1_OptimizerNumBiasesToTry, L1_OptimizerBiasIndex, L1_OptimizerExecutionCount },
						{ L1_OptimizerBiasIndex, L1_OptimizerExecutionCount } },

				{ { L1_Split }, { Split }, { NumExamples, NumInVars, NumOutVars, L0_NumTrainExamples, L0_TrainIndices, L1_TrainingFraction }, { L0_NumTrainExamples } },

				{ { L1_CreateModel }, { CreateModel }, { AllData, NumExamples, NumInVars, NumOutVars, L1_TrainIndices }, { L1_Model } },

				{ { L1_TestModel }, { TestModel }, { AllData, NumExamples, NumInVars, NumOutVars, L1_TestIndices }, { L1_Performance } },

				{ { L0_CreateModel }, { CreateModel }, { AllData, NumExamples, NumInVars, NumOutVars, L0_TrainIndices }, { L1_Model } },

				{ { L0_TestModel }, { TestModel }, { AllData, NumExamples, NumInVars, NumOutVars, L0_TestIndices }, { L0_ActualPerformance } },

				{ { L0_SaveResults }, { SaveResults }, { L1_Performance, L0_ActualPerformance }, { DeltaPerformances } },

		};

		/***********************/
		/** CONNECTIONS **/
		/***********************/

		Object[][][] connections = new Object[][][] {

				//

				{ { L0_ReadAllExamples }, { L0_InitializeObserver } },

				{ { L0_InitializeObserver }, { L0_Observer } },

				{ { L0_Observer }, { L0_Split, L0_End } },

				{ { L0_End }, { L0_End } },

				{ { L0_Split }, { L1_InitializeOptimizer } },

				{ { L1_InitializeOptimizer }, { L1_Optimizer } },

				{ { L1_Optimizer }, { L1_Split, L0_Observer } },

				{ { L1_Split }, { L1_CreateModel } },

				{ { L1_CreateModel }, { L1_TestModel } },

				{ { L1_TestModel }, { L1_Optimizer } },

				{ { L0_CreateModel }, { L0_TestModel } },

				{ { L0_TestModel }, { L0_SaveResults } },

				{ { L0_SaveResults }, { L0_Split } },

		};

		Object[] program = new Object[] { variables, modules, connections };

		Floe floe = new Floe();

		floe.maxNumThreadsToReport = maxNumThreadsToReport;

		floe.setNumCPUThreads(numThreads); // 1 (1.0), 2 (1.16) 32 (2.20)
		floe.setNumGPUThreads(1);

		floe.execute(program);
	}
}

class Floe {

	static final int DEBUG_MODE = 0;
	static final int FAST_MODE = 1;
	static final int FASTEST_MODE = 2;
	static final int executionMode = LayeredLearning.executionMode;

	/*******************/
	/* STRING LITERALS */
	/*******************/

	// memory scope

	public static final String GlobalMemory = "Global";
	public static final String ThreadMemory = "Thread";

	// memory behavior:

	public static final String ReadOnly = "ReadOnly";
	public static final String Volitile = "Volitile";

	// memory data type

	public static final String FloatArray = "FloatArray";
	public static final String DoubleArray = "Double";
	public static final String IntegerArray = "Integer";
	public static final String LongArray = "Long";

	int numCPUThreads = 1;

	int maxNumThreadsToReport = Integer.MAX_VALUE;

	int numGPUThreads = 1;

	/*************/
	/* VARIABLES */
	/*************/

	static HashMap<String, Boolean> isGlobal;
	static HashMap<String, Object>[] threadVariables;
	static HashMap<String, Object> globalVariables;

	/***********/
	/* MODULES */
	/***********/

	static HashMap<String, Module[]> moduleNameToThreadClassInstances;

	// /***************/
	// /* CONNECTIONS */
	// /***************/
	//
	// static HashMap<Object, Object[]> moduleConnections;

	public int getNumCPUThreads() {
		return numCPUThreads;
	}

	public void setNumCPUThreads(int numCPUThreads) {
		this.numCPUThreads = numCPUThreads;
	}

	public int getNumGPUThreads() {
		return numGPUThreads;
	}

	public void setNumGPUThreads(int numGPUThreads) {
		this.numGPUThreads = numGPUThreads;
	}

	static public Object getVariable(int threadIndex, String variableName) {

		Object object = null;

		if (isGlobal.get(variableName)) {
			object = globalVariables.get(variableName);
		} else {
			object = threadVariables[threadIndex].get(variableName);
		}

		return object;
	}

	static public void setVariable(int threadIndex, String variableName, Object value) {

		if (isGlobal.get(variableName)) {
			globalVariables.put(variableName, value);
		} else {
			threadVariables[threadIndex].put(variableName, value);

		}

	}

	Object[][] variables = null;
	Object[][][] modules = null;
	Object[][][] connections = null;

	public void execute(Object[] program) {

		long startTime = System.currentTimeMillis();

		{
			int i = 0;
			variables = (Object[][]) program[i++];
			modules = (Object[][][]) program[i++];
			connections = (Object[][][]) program[i++];
		}

		/******************************/
		/* INITIALIZE VARIABLE MEMORY */
		/******************************/

		isGlobal = new HashMap<String, Boolean>();
		globalVariables = new HashMap<String, Object>();
		threadVariables = new HashMap[numCPUThreads];

		for (int i = 0; i < numCPUThreads; i++) {
			threadVariables[i] = new HashMap<String, Object>();
		}

		for (int i = 0; i < variables.length; i++) {

			String variableName = (String) variables[i][0];
			String variableScope = (String) variables[i][1];
			String variableBehavior = (String) variables[i][2];
			String variableDataType = (String) variables[i][3];
			Integer variableDataLength = (Integer) variables[i][4];
			Object variableInitialValue = (Object) variables[i][5];

			if (false) {
				System.out.println();
				System.out.println("variableName     = " + variableName);
				System.out.println("variableScope    = " + variableScope);
				System.out.println("variableBehavior = " + variableBehavior);
				System.out.println("variableDataType = " + variableDataType);
				System.out.println("variableDataLength = " + variableDataLength);
				System.out.println("variableInitialValue = " + variableInitialValue);
			}

			if (variableScope == GlobalMemory) {
				isGlobal.put(variableName, true);
			} else {
				isGlobal.put(variableName, false);
			}

			// initialize thread values //
			if (isGlobal.get(variableName)) {

				if (variableDataType == Floe.FloatArray) {

					float values[] = new float[variableDataLength];

					if (variableInitialValue != null) {
						for (int j = 0; j < values.length; j++) {
							values[j] = ((float[]) variableInitialValue)[j];
						}
					}

					globalVariables.put(variableName, values);

				} else if (variableDataType == Floe.DoubleArray) {

					double values[] = new double[variableDataLength];

					if (variableInitialValue != null) {
						for (int j = 0; j < values.length; j++) {
							values[j] = ((double[]) variableInitialValue)[j];
						}
					}

					globalVariables.put(variableName, values);

				} else if (variableDataType == Floe.IntegerArray) {

					int values[] = new int[variableDataLength];

					if (variableInitialValue != null) {
						for (int j = 0; j < values.length; j++) {
							values[j] = ((int[]) variableInitialValue)[j];
						}
					}

					globalVariables.put(variableName, values);

				} else if (variableDataType == Floe.LongArray) {

					long values[] = new long[variableDataLength];

					if (variableInitialValue != null) {
						for (int j = 0; j < values.length; j++) {
							values[j] = ((long[]) variableInitialValue)[j];
						}
					}

					globalVariables.put(variableName, values);

				} else {
					fail();
				}

			} else
				for (int threadIndex = 0; threadIndex < numCPUThreads; threadIndex++) {

					if (variableDataType == Floe.FloatArray) {

						float values[] = new float[variableDataLength];

						if (variableInitialValue != null) {
							for (int j = 0; j < values.length; j++) {
								values[j] = ((float[]) variableInitialValue)[j];
							}
						}

						threadVariables[threadIndex].put(variableName, values);

					} else if (variableDataType == Floe.DoubleArray) {

						double values[] = new double[variableDataLength];

						if (variableInitialValue != null) {
							for (int j = 0; j < values.length; j++) {
								values[j] = ((double[]) variableInitialValue)[j];
							}
						}

						threadVariables[threadIndex].put(variableName, values);

					} else if (variableDataType == Floe.IntegerArray) {

						int values[] = new int[variableDataLength];

						if (variableInitialValue != null) {
							for (int j = 0; j < values.length; j++) {
								values[j] = ((int[]) variableInitialValue)[j];
							}
						}
						threadVariables[threadIndex].put(variableName, values);

					} else if (variableDataType == Floe.LongArray) {

						long values[] = new long[variableDataLength];

						if (variableInitialValue != null) {
							for (int j = 0; j < values.length; j++) {
								values[j] = ((long[]) variableInitialValue)[j];
							}
						}
						threadVariables[threadIndex].put(variableName, values);

					} else {
						fail();
					}

				}

		}

		/**********************/
		/* INITIALIZE MODULES */
		/**********************/
		moduleNameToThreadClassInstances = new HashMap<String, Module[]>();

		for (int i = 0; i < modules.length; i++) {

			String moduleName = (String) modules[i][0][0];
			String moduleClassName = (String) modules[i][1][0];
			Object[] inputVars = (Object[]) modules[i][2];
			Object[] outputVars = (Object[]) modules[i][3];

			Class moduleClass = null;
			try {

				moduleClass = Class.forName("flows.workflows." + moduleClassName);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (false) {
				System.out.println();
				System.out.println("moduleName      = " + moduleName);
				System.out.println("moduleClassName = " + moduleClassName);
				System.out.println("moduleClass     = " + moduleClass);
				System.out.println("inputVars       = " + inputVars);
				System.out.println("outputVars      = " + outputVars);
			}

			try {

				Module[] modules = new Module[numCPUThreads];

				for (int threadIndex = 0; threadIndex < numCPUThreads; threadIndex++) {

					Module module = (Module) moduleClass.newInstance();

					module.threadIndex = threadIndex;
					module.inputVars = inputVars;
					module.outputVars = outputVars;
					module.initialize();

					modules[threadIndex] = module;
				}

				moduleNameToThreadClassInstances.put(moduleName, modules);

			} catch (InstantiationException e) {
				fail();
			} catch (IllegalAccessException e) {
				fail();
			}

		}

		/**************************/
		/* INITIALIZE CONNECTIONS */
		/**************************/
		// moduleConnections = new HashMap<Object, Object[]>();

		for (int i = 0; i < connections.length; i++) {

			String moduleName = (String) connections[i][0][0];
			Object[] connectedModuleNames = (Object[]) connections[i][1];

			Module[] moduleClassInstances = (Module[]) moduleNameToThreadClassInstances.get(moduleName);

			if (false) {
				System.out.println();
				System.out.println("moduleName           = " + moduleName);
				System.out.println("moduleClassInstances = " + moduleClassInstances);
			}

			for (int threadIndex = 0; threadIndex < numCPUThreads; threadIndex++) {

				Module[] classInstances = new Module[connectedModuleNames.length];

				for (int j = 0; j < connectedModuleNames.length; j++) {

					String connectedModuleName = (String) connectedModuleNames[j];

					Module connectedModuleClassInstance = moduleNameToThreadClassInstances.get(connectedModuleName)[threadIndex];

					if (false) {
						System.out.println("  connectedModuleName          = " + connectedModuleName);
						System.out.println("  connectedModuleClassInstance = " + connectedModuleClassInstance);
					}

					classInstances[j] = connectedModuleClassInstance;

					switch (j) {
					case 0:
						moduleClassInstances[threadIndex].nextModule1 = connectedModuleClassInstance;
						break;
					case 1:
						moduleClassInstances[threadIndex].nextModule2 = connectedModuleClassInstance;
						break;
					default:
						fail();
					}

				}

				moduleClassInstances[threadIndex].nextModules = classInstances;
			}

		}

		// moduleConnections.put(moduleClassInstance, classInstances);

		/**********************/
		/* CREATE CPU THREADS */
		/**********************/

		CPUThread[] cpuThreads = new CPUThread[numCPUThreads];

		for (int i = 0; i < numCPUThreads; i++) {
			cpuThreads[i] = new CPUThread(this, i);
			// cpuThreads[i].start();
		}

		/*********************/
		/* START CPU THREADS */
		/*********************/

		for (int i = 0; i < numCPUThreads; i++) {
			cpuThreads[i].start();
		}

		// wait for threads to finish

		long lastReportTime = System.currentTimeMillis();

		while (true) {

			boolean allDone = true;
			for (int i = 0; i < numCPUThreads; i++) {
				if (cpuThreads[i].isAlive())
					allDone = false;
			}
			if (allDone)
				break;

			long currentTime = System.currentTimeMillis();

			if (currentTime - lastReportTime > 1000) {

				long numModulesExecuted = 0;
				long estimatedNumModulesExecuted = 0;

				switch (executionMode) {

				case DEBUG_MODE:

					for (int i = 0; i < numCPUThreads; i++) {
						System.out.println("cpuThreads[" + i + "].numExecutions      = " + cpuThreads[i].numExecutions);
						numModulesExecuted += cpuThreads[i].numExecutions;
					}
					for (int threadIndex = 0; threadIndex < numCPUThreads; threadIndex++) {
						int[] L0_ObservationIndex = (int[]) getVariable(threadIndex, LayeredLearning.L0_ObservationIndex);
						long[] L1_OptimizerExecutionCount = (long[]) getVariable(threadIndex, LayeredLearning.L1_OptimizerExecutionCount);
						System.out.println("L0_ObservationIndex        = " + L0_ObservationIndex[0]);
						System.out.println("L1_OptimizerExecutionCount = " + L1_OptimizerExecutionCount[0]);
					}

					break;

				case FAST_MODE:
					break;

				case FASTEST_MODE:

					if (LayeredLearning.countExecutions) {
						for (int i = 0; i < Math.min(numCPUThreads, maxNumThreadsToReport); i++) {
							System.out.println("cpuThreads[" + i + "].firstModule.executionCount      = " + cpuThreads[i].firstModule.executionCount);
						}
						for (int i = 0; i < numCPUThreads; i++) {
							numModulesExecuted += cpuThreads[i].firstModule.executionCount;
						}
					}
					if (LayeredLearning.reportCounts) {
						for (int threadIndex = 0; threadIndex < Math.min(numCPUThreads, maxNumThreadsToReport); threadIndex++) {
							int L0_ObservationIndex = ((AllModules) cpuThreads[threadIndex].firstModule).L0_ObservationIndex;
							long L1_OptimizerExecutionCount = ((AllModules) cpuThreads[threadIndex].firstModule).L1_OptimizerExecutionCount;
							System.out.println("L0_ObservationIndex         = " + L0_ObservationIndex);
							System.out.println("L1_OptimizerExecutionCount  = " + L1_OptimizerExecutionCount);
						}
					}

					if (LayeredLearning.estimateCounts) {
						for (int threadIndex = 0; threadIndex < numCPUThreads; threadIndex++) {
							int L0_ObservationIndex = ((AllModules) cpuThreads[threadIndex].firstModule).L0_ObservationIndex;
							long L1_OptimizerNumBiasesToTry = ((AllModules) cpuThreads[threadIndex].firstModule).L1_OptimizerNumBiasesToTry;

							estimatedNumModulesExecuted += (L0_ObservationIndex) * (L1_OptimizerNumBiasesToTry + 1) * 4 - L1_OptimizerNumBiasesToTry * 4;
						}
					}

					break;

				default:
					break;
				}

				double timeElapsedInSeconds = (currentTime - startTime) / 1000.0;

				double moduleExecutionsPerSecond = estimatedNumModulesExecuted / timeElapsedInSeconds;

				System.out.println();
				System.out.println("numModulesExecuted          = " + numModulesExecuted);
				System.out.println("estimatedNumModulesExecuted = " + estimatedNumModulesExecuted);
				System.out.println("timeElapsedInSeconds        = " + timeElapsedInSeconds);
				System.out.println("moduleExecutionsPerSecond   = " + moduleExecutionsPerSecond);

				lastReportTime = currentTime;
			}

			try {
				Thread.sleep(LayeredLearning.controllerSleepTime);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.exit(1);
			}
		}

		System.out.println();
		System.out.println("Final Results:");

		long numModulesExecuted = 0;
		long estimatedNumModulesExecuted = 0;

		switch (executionMode) {

		case DEBUG_MODE:

			for (int i = 0; i < numCPUThreads; i++) {
				System.out.println("cpuThreads[" + i + "].numExecutions      = " + cpuThreads[i].numExecutions);
				numModulesExecuted += cpuThreads[i].numExecutions;
			}
			for (int threadIndex = 0; threadIndex < numCPUThreads; threadIndex++) {
				int[] L0_ObservationIndex = (int[]) getVariable(threadIndex, LayeredLearning.L0_ObservationIndex);
				long[] L1_OptimizerExecutionCount = (long[]) getVariable(threadIndex, LayeredLearning.L1_OptimizerExecutionCount);
				System.out.println("L0_ObservationIndex        = " + L0_ObservationIndex[0]);
				System.out.println("L1_OptimizerExecutionCount = " + L1_OptimizerExecutionCount[0]);
			}

			break;

		case FAST_MODE:
			break;

		case FASTEST_MODE:

			if (LayeredLearning.countExecutions) {
				for (int i = 0; i < Math.min(numCPUThreads, maxNumThreadsToReport); i++) {
					System.out.println("cpuThreads[" + i + "].firstModule.executionCount      = " + cpuThreads[i].firstModule.executionCount);
				}
				for (int i = 0; i < numCPUThreads; i++) {
					numModulesExecuted += cpuThreads[i].firstModule.executionCount;
				}
			}
			if (LayeredLearning.reportCounts) {
				for (int threadIndex = 0; threadIndex < Math.min(numCPUThreads, maxNumThreadsToReport); threadIndex++) {
					int L0_ObservationIndex = ((AllModules) cpuThreads[threadIndex].firstModule).L0_ObservationIndex;
					long L1_OptimizerExecutionCount = ((AllModules) cpuThreads[threadIndex].firstModule).L1_OptimizerExecutionCount;
					System.out.println("L0_ObservationIndex         = " + L0_ObservationIndex);
					System.out.println("L1_OptimizerExecutionCount  = " + L1_OptimizerExecutionCount);
				}
			}

			if (LayeredLearning.estimateCounts) {
				for (int threadIndex = 0; threadIndex < numCPUThreads; threadIndex++) {
					int L0_ObservationIndex = ((AllModules) cpuThreads[threadIndex].firstModule).L0_ObservationIndex;
					long L1_OptimizerNumBiasesToTry = ((AllModules) cpuThreads[threadIndex].firstModule).L1_OptimizerNumBiasesToTry;

					estimatedNumModulesExecuted += (L0_ObservationIndex) * (L1_OptimizerNumBiasesToTry + 1) * 4 - L1_OptimizerNumBiasesToTry * 4;
				}
			}

			break;

		default:
			break;
		}

		long currentTime = System.currentTimeMillis();

		double timeElapsedInSeconds = (currentTime - startTime) / 1000.0;

		double moduleExecutionsPerSecond = estimatedNumModulesExecuted / timeElapsedInSeconds;

		System.out.println();

		System.out.println("numModulesExecuted          = " + numModulesExecuted);
		System.out.println("estimatedNumModulesExecuted = " + estimatedNumModulesExecuted);
		System.out.println("timeElapsedInSeconds        = " + timeElapsedInSeconds);
		System.out.println("moduleExecutionsPerSecond   = " + moduleExecutionsPerSecond);
		System.out.println();
		System.out.println("All Finished!!!");

	}

	private void fail() {
		new Exception().printStackTrace();
		System.exit(1);
	}

	{
		// TODO Auto-generated method stub

	}

}

/**************/
/* CPU THREAD */
/**************/

class CPUThread extends Thread {

	Floe floe;

	int threadIndex;

	long numExecutions;

	Module firstModule = null;

	CPUThread(Floe floe, int threadIndex) {
		this.floe = floe;
		this.threadIndex = threadIndex;
		this.numExecutions = 0;
	}

	public void run() {

		boolean verbose = false;

		if (verbose)
			System.out.println("Starting Thread #" + threadIndex);

		Module moduleClassInstance = null;

		switch (floe.executionMode) {

		case Floe.DEBUG_MODE:
		case Floe.FAST_MODE:

			int nextModuleIndex = 0;

			String firstModuleName = (String) floe.modules[nextModuleIndex][0][0];

			if (verbose)
				System.out.println("Thread #" + threadIndex + ":  firstModuleName = " + firstModuleName);

			moduleClassInstance = ((Module[]) floe.moduleNameToThreadClassInstances.get(firstModuleName))[threadIndex];

			break;

		case Floe.FASTEST_MODE:

			moduleClassInstance = new AllModules();
			moduleClassInstance.threadIndex = threadIndex;
			moduleClassInstance.initialize();

			break;

		default:
			break;
		}

		firstModule = moduleClassInstance;

		boolean fastestMode = true;
		boolean fastMode = false;
		if (fastestMode) {
			moduleClassInstance.execute();
		} else if (fastMode) {
			while ((moduleClassInstance = moduleClassInstance.execute()) != null) {
			}
		} else {
			while (true) {
				try {
					moduleClassInstance = moduleClassInstance.execute();
				} catch (Exception e) {
					// e.printStackTrace();
					break;
				}
				numExecutions++;
			}
		}

	}
}

/**********/
/* MODULE */
/**********/
class Module {

	public int threadIndex = -1;

	public long executionCount = 0;

	public boolean verbose = true;

	public boolean firstExecution = true;

	public Object[] inputVars = null;
	public Object[] outputVars = null;

	public Module[] nextModules = null;

	public final int maxNumFlowConnections = 3;
	public Module nextModule1 = null;
	public Module nextModule2 = null;

	void initialize() {

		// new Exception().printStackTrace();
		// System.out.println("Warning! initialize() not defined for " + this.getClass());
		// System.exit(1);
	}

	Module execute() {

		new Exception().printStackTrace();
		System.out.println("Error! execute(int threadIndex) not defined");
		return null;
	}

}

/****************/
/* USER MODULES */
/****************/

class ReadExamples extends Module {

	static boolean finished = false;

	String NumExamplesVarName = null;
	String NumInVarsVarName = null;
	String NumOutVarsVarName = null;

	int[] NumExamplesVarValue = null;
	int[] NumInVarsVarValue = null;
	int[] NumOutVarsVarValue = null;

	void initialize() {

		NumExamplesVarName = (String) this.outputVars[1];
		NumInVarsVarName = (String) this.outputVars[2];
		NumOutVarsVarName = (String) this.outputVars[3];

		NumExamplesVarValue = (int[]) Floe.getVariable(threadIndex, NumExamplesVarName);
		NumInVarsVarValue = (int[]) Floe.getVariable(threadIndex, NumInVarsVarName);
		NumOutVarsVarValue = (int[]) Floe.getVariable(threadIndex, NumOutVarsVarName);
	}

	Module execute() {

		if (threadIndex == 0) {

			NumExamplesVarValue[0] = 1000;
			NumInVarsVarValue[0] = 1000;
			NumOutVarsVarValue[0] = 1;

			finished = true;

		} else {

			while (true) {

				if (finished) {
					break;
				} else {
					try {
						Thread.sleep(1);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						System.exit(1);
					}
				}
			}

		}
		return nextModule1;
	}

}

class Split extends Module {
	Module execute() {
		return nextModule1;
	}

}

class SetIntegerToZero extends Module {

	String outputVarNameToSetName = null;
	int[] outputVarNameValue = null;

	void initialize() {
		outputVarNameToSetName = (String) this.outputVars[0];
		outputVarNameValue = (int[]) Floe.getVariable(threadIndex, outputVarNameToSetName);
	}

	Module execute() {

		outputVarNameValue[0] = 0;

		return nextModule1;
	}

}

class Observer extends Module {

	String numObservationsVarName = null;
	String observationIndexVarName = null;

	int[] numObservationsVarValue = null;
	int[] observationIndexVarValue = null;

	void initialize() {

		numObservationsVarName = (String) this.inputVars[0];
		observationIndexVarName = (String) this.inputVars[1];

		numObservationsVarValue = (int[]) Floe.getVariable(threadIndex, numObservationsVarName);
		observationIndexVarValue = (int[]) Floe.getVariable(threadIndex, observationIndexVarName);

	}

	Module execute() {

		if (observationIndexVarValue[0]++ < numObservationsVarValue[0]) {
			return nextModule1;
		} else {
			return nextModule2;
		}
	}

}

class Optimizer extends Module {

	String optimizerNumBiasesToTryVarName = null;
	String optimizerBiasIndexVarName = null;
	String optimizerExecutionCountVarName = null;

	int[] optimizerNumBiasesToTryVarValue = null;
	int[] optimizerBiasIndexVarValue = null;
	long[] optimizerExecutionCountVarValue = null;

	void initialize() {

		optimizerNumBiasesToTryVarName = (String) this.inputVars[0];
		optimizerBiasIndexVarName = (String) this.inputVars[1];
		optimizerExecutionCountVarName = (String) this.inputVars[2];

		optimizerNumBiasesToTryVarValue = (int[]) Floe.getVariable(threadIndex, optimizerNumBiasesToTryVarName);
		optimizerBiasIndexVarValue = (int[]) Floe.getVariable(threadIndex, optimizerBiasIndexVarName);
		optimizerExecutionCountVarValue = (long[]) Floe.getVariable(threadIndex, optimizerExecutionCountVarName);
	}

	Module execute() {

		optimizerExecutionCountVarValue[0]++;

		if (optimizerBiasIndexVarValue[0]++ < optimizerNumBiasesToTryVarValue[0]) {
			return nextModule1;
		} else {
			return nextModule2;
		}
	}

}

class CreateModel extends Module {
	Module execute() {
		return nextModule1;
	}

}

class TestModel extends Module {
	Module execute() {
		return nextModule1;
	}

}

class SaveResults extends Module {
	Module execute() {
		return nextModule1;
	}

}

class End extends Module {
	Module execute() {
		return null;
	}

}

class AllModules extends Module {

	final int L0_ReadAllExamples = 0;
	final int L0_InitializeObserver = 1;
	final int L0_Observer = 2;
	final int L0_End = 3;
	final int L0_Split = 4;
	final int L1_InitializeOptimizer = 5;
	final int L1_Optimizer = 6;
	final int L1_Split = 7;
	final int L1_CreateModel = 8;
	final int L1_TestModel = 9;
	final int L0_CreateModel = 10;
	final int L0_TestModel = 11;
	final int L0_SaveResults = 12;

	public String NumExamplesVarName = null;
	public String NumInVarsVarName = null;
	public String NumOutVarsVarName = null;

	public int NumExamplesVarValue = Integer.MIN_VALUE;
	public int NumInVarsVarValue = Integer.MIN_VALUE;
	public int NumOutVarsVarValue = Integer.MIN_VALUE;

	public String outputVarNameToSetName = null;
	public int[] outputVarNameValue = null;

	public String numObservationsVarName = null;
	public String observationIndexVarName = null;

	public int numObservationsVarValue = Integer.MIN_VALUE;
	public int L0_ObservationIndex = Integer.MIN_VALUE;

	public String optimizerNumBiasesToTryVarName = null;
	public String optimizerBiasIndexVarName = null;
	public String optimizerExecutionCountVarName = null;

	public int L1_OptimizerNumBiasesToTry = Integer.MIN_VALUE;
	public int optimizerBiasIndexVarValue = Integer.MIN_VALUE;
	public long L1_OptimizerExecutionCount = Long.MIN_VALUE;

	// local module variables

	static boolean L0_ReadAllExamples_finished = false;

	void initialize() {

		NumExamplesVarValue = ((int[]) Floe.getVariable(threadIndex, LayeredLearning.NumExamples))[0];
		NumInVarsVarValue = ((int[]) Floe.getVariable(threadIndex, LayeredLearning.NumInVars))[0];
		NumOutVarsVarValue = ((int[]) Floe.getVariable(threadIndex, LayeredLearning.NumOutVars))[0];

		//

		numObservationsVarValue = ((int[]) Floe.getVariable(threadIndex, LayeredLearning.L0_NumObservationsToMake))[0];
		L0_ObservationIndex = ((int[]) Floe.getVariable(threadIndex, LayeredLearning.L0_ObservationIndex))[0];

		//

		L1_OptimizerNumBiasesToTry = ((int[]) Floe.getVariable(threadIndex, LayeredLearning.L1_OptimizerNumBiasesToTry))[0];
		optimizerBiasIndexVarValue = ((int[]) Floe.getVariable(threadIndex, LayeredLearning.L1_OptimizerBiasIndex))[0];
		L1_OptimizerExecutionCount = ((long[]) Floe.getVariable(threadIndex, LayeredLearning.L1_OptimizerExecutionCount))[0];

	}

	Module execute() {
		//

		int nextModuleIndex = L0_ReadAllExamples;

		boolean enabled = true;
		while (enabled) {

			// if (this.executionCount % 10 == 0)
			// Thread.yield();

			if (LayeredLearning.countExecutions)
				this.executionCount++;

			switch (nextModuleIndex) {

			case L0_ReadAllExamples:

				if (threadIndex == 0) {

					NumExamplesVarValue = 1000;
					NumInVarsVarValue = 1000;
					NumOutVarsVarValue = 1;

					L0_ReadAllExamples_finished = true;

				} else {

					while (true) {

						if (L0_ReadAllExamples_finished) {
							break;
						} else {
							try {
								Thread.sleep(1);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								System.exit(1);
							}
						}
					}

				}

				nextModuleIndex = L0_InitializeObserver;

				break;

			case L0_InitializeObserver:

				L0_ObservationIndex = 0;

				nextModuleIndex = L0_Observer;

				break;

			case L0_Observer:

				if (L0_ObservationIndex++ < numObservationsVarValue) {
					nextModuleIndex = L0_Split;
				} else {
					nextModuleIndex = L0_End;
				}

				break;

			case L0_End:

				nextModuleIndex = -1;
				enabled = false;

				break;

			case L0_Split:

				nextModuleIndex = L1_InitializeOptimizer;

				break;

			case L1_InitializeOptimizer:

				optimizerBiasIndexVarValue = 0;

				nextModuleIndex = L1_Optimizer;

				break;

			case L1_Optimizer:

				L1_OptimizerExecutionCount++;

				if (optimizerBiasIndexVarValue++ < L1_OptimizerNumBiasesToTry) {
					nextModuleIndex = L1_Split;
				} else {
					nextModuleIndex = L0_Observer;
				}

				break;

			case L1_Split:

				nextModuleIndex = L1_CreateModel;

				break;

			case L1_CreateModel:

				nextModuleIndex = L1_TestModel;

				break;

			case L1_TestModel:

				nextModuleIndex = L1_Optimizer;

				break;

			case L0_CreateModel:

				nextModuleIndex = L0_TestModel;

				break;

			case L0_TestModel:

				nextModuleIndex = L0_SaveResults;

				break;

			case L0_SaveResults:

				nextModuleIndex = L0_Observer;

				break;

			default:
				break;
			}

		}

		return null;

	}
}
