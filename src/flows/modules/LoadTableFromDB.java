package flows.modules;

import java.sql.*;
import java.util.*;

import flows.Module;

public class LoadTableFromDB {

	public Connection getConnection(String url, String driver, String username, String password) {

		Connection connection = null;

		try {
			System.out.println("Entering getConnection");
			if (connection == null) {
				Class classNm = Class.forName(driver);
				DriverManager.registerDriver((Driver) Class.forName(driver).newInstance());

				// make the connection
				if (username == null) {
					connection = DriverManager.getConnection(url);
				} else {
					System.out.println("URL is " + url);
					connection = DriverManager.getConnection(url, username, password);
				}
			}
			System.out.println("Successful Connection");
			return connection;
		} catch (Exception e) {
			System.out.println("Cannot connect to the database. Please check the user " + "name, password, server machine, " + "and database instance you have entered.");
			System.out.println("Error occurred when connecting to a database.");
			return null;
		}
	}

	public void doit() throws Exception {

	}

	String tableName = null;
	String username = null;
	String port = null;
	String machine = null;
	String dbInstance = null;
	String password = null;
	String driver = null;

	public int feedModule(Object objects[]) {

		int i = 0;
		tableName = (String) objects[i++];
		username = (String) objects[i++];
		port = (String) objects[i++];
		machine = (String) objects[i++];
		dbInstance = (String) objects[i++];
		password = (String) objects[i++];
		driver = (String) objects[i++];

		// TODO Auto-generated method stub
		return 0;
	}

	public int executeModule() throws Exception {

		String url = "jdbc:oracle:thin:@" + machine + ":" + port + ":" + dbInstance;

		System.out.println("creating connection");

		Connection connection = getConnection(url, driver, username, password);

		String resultStrings[];
		Vector vector = new Vector();

		String query;
		query = "SELECT *  FROM " + tableName;

		System.out.println("issuing query: " + query);

		Statement statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(query);

		ResultSetMetaData resultSetMetaData = resultSet.getMetaData();

		// count number of rows
		int exampleIndex = 0;
		while (resultSet.next()) {
			exampleIndex++;
		}
		statement.close();

		statement = connection.createStatement();
		resultSet = statement.executeQuery(query);

		resultSetMetaData = resultSet.getMetaData();

		int NumExamples = exampleIndex;

		System.out.println("NumExamples = " + NumExamples);

		int numColumns = resultSetMetaData.getColumnCount();

		while (resultSet.next()) {

			for (int j = 0; j < 7; j++) {
				String string = resultSet.getString(j);
				System.out.println("string = " + string);
			}
			exampleIndex++;
		}
		statement.close();

		return 0;
	}

}
