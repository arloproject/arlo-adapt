package flows;

public class Module {

	int numReadVariables = 0;
	int numWriteVariables = 0;

	String readVariableNames[];
	String writeVariableNames[];

	public int getNumReadVariables() throws Exception {
		return 0;
	}

	public int getNumWriteVariables() throws Exception {
		return 0;
	}

	public String[] getReadVariableNames() throws Exception {
		return readVariableNames;
	}

	public String[] getWriteVariableNames() throws Exception {
		return writeVariableNames;
	}

	public String getReadVariableName(int index) throws Exception {
		return readVariableNames[index];
	}

	public String getWriteVariableName(int index) throws Exception {
		return writeVariableNames[index];
	}

	public int initializeModule() throws Exception {
		return 0;
	}

	public int executeModule() throws Exception {
		return 0;
	}

}
