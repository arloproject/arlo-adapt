package arlo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.RandomAccess;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;

/**
 * Prints out object to log file and standard output.
 * 
 * @author David Tcheng
 */
public class BytesToWavData {


	final static boolean verbose = false;


	static int unsignedByte(byte value) {
		if (value >= 0) {
			return value;
		}
		return 256 + value;
	}

	public static int[] getWavDataAsInt(AudioFile audioFile, double startTime, double endTime, int numPadFrames, boolean removeDCOffset) {


		// long byteDataOffset = (long) ((long) audioFile.numChannels * (audioFile.bitsPerSample / 8) * startTime * audioFile.sampleRate);
		long byteDataOffset = ((long) (startTime * audioFile.sampleRate)) * audioFile.nBlockAlign;

		double duration = endTime - startTime;
		int numFrames = (int) (duration * audioFile.sampleRate);

		if (numFrames == 0) {
			numFrames = 1;
		}

		numFrames += numPadFrames;

		if (verbose)
			System.out.println("numFrames = " + numFrames);

		long byteDataSize = audioFile.nBlockAlign * numFrames;
		if (verbose)
			System.out.println("byteDataSize = " + byteDataSize);

		if (byteDataSize <= 0) {

			System.err.println("invalid sample file size, path = " + audioFile.filePath + ".  Delete file and retry!");

			return null;
		}
		

		long startPosition =  audioFile.dataStartIndex + byteDataOffset;
		

		if (verbose) {
			System.out.println("audioFile.filePath = " + audioFile.filePath);
			System.out.println("startPosition = " + startPosition);
		}

		RandomAccessFile file = null;
		ByteBuffer fileByteBuffer = null;
		try {
			file = new RandomAccessFile(audioFile.filePath, "r");

			FileChannel rwCh = file.getChannel();

			// Map entire file to memory and close the channel
			long fileSize = rwCh.size();
			
			//!!!
			
//			if (fileSize > Integer.MAX_VALUE) {
//				System.out.println("Error!  fileSize = " + fileSize);
//				System.out.println("Error!  filePath = " + audioFile.filePath);
//				fileSize = Integer.MAX_VALUE;
//			}
			
			// System.out.println("fileSize = " + fileSize);
			fileByteBuffer = rwCh.map(FileChannel.MapMode.READ_ONLY, startPosition, Math.min(fileSize - startPosition, Integer.MAX_VALUE));

		} catch (FileNotFoundException e) {
			System.out.println("Error!  Could not open: " + audioFile.filePath);
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		
		
		// fileByteBuffer.position(position);
		fileByteBuffer.position(0);

		int[] intData = new int[numFrames];

		switch (audioFile.bitsPerSample) {
		case 8:
			switch (audioFile.numChannels) {
			case 1:
				for (int frameIndex = 0; frameIndex < numFrames; frameIndex++) {

					int value = 0;
					value += unsignedByte(fileByteBuffer.get());

					intData[frameIndex] = value;
				}
				break;
			case 2:
				for (int frameIndex = 0; frameIndex < numFrames; frameIndex++) {

					int value = 0;
					value += unsignedByte(fileByteBuffer.get());
					value += unsignedByte(fileByteBuffer.get());

					intData[frameIndex] = value / 2;
				}
				break;
			case 3:
				for (int frameIndex = 0; frameIndex < numFrames; frameIndex++) {

					int value = 0;
					value += unsignedByte(fileByteBuffer.get());
					value += unsignedByte(fileByteBuffer.get());
					value += unsignedByte(fileByteBuffer.get());

					intData[frameIndex] = value / 3;
				}
				break;
			case 4:
				for (int frameIndex = 0; frameIndex < numFrames; frameIndex++) {

					int value = 0;
					value += unsignedByte(fileByteBuffer.get());
					value += unsignedByte(fileByteBuffer.get());
					value += unsignedByte(fileByteBuffer.get());
					value += unsignedByte(fileByteBuffer.get());

					intData[frameIndex] = value / 4;
				}
				break;
			}
			break;
		case 16:
			switch (audioFile.numChannels) {
			case 1:
				for (int frameIndex = 0; frameIndex < numFrames; frameIndex++) {

					int value = 0;

					int value1 = unsignedByte(fileByteBuffer.get());
					int value2 = unsignedByte(fileByteBuffer.get());
					value += (short) ((value2 << 8) + value1);

					intData[frameIndex] = value;
				}
				break;
			case 2:
				for (int frameIndex = 0; frameIndex < numFrames; frameIndex++) {

					int value = 0;
					{
						int value1 = unsignedByte(fileByteBuffer.get());
						int value2 = unsignedByte(fileByteBuffer.get());
						value += (short) ((value2 << 8) + value1);
					}
					{
						int value1 = unsignedByte(fileByteBuffer.get());
						int value2 = unsignedByte(fileByteBuffer.get());
						value += (short) ((value2 << 8) + value1);
					}

					intData[frameIndex] = value / 2;
				}
				break;
			case 3:
				for (int frameIndex = 0; frameIndex < numFrames; frameIndex++) {

					int value = 0;
					{
						int value1 = unsignedByte(fileByteBuffer.get());
						int value2 = unsignedByte(fileByteBuffer.get());
						value += (short) ((value2 << 8) + value1);
					}
					{
						int value1 = unsignedByte(fileByteBuffer.get());
						int value2 = unsignedByte(fileByteBuffer.get());
						value += (short) ((value2 << 8) + value1);
					}
					{
						int value1 = unsignedByte(fileByteBuffer.get());
						int value2 = unsignedByte(fileByteBuffer.get());
						value += (short) ((value2 << 8) + value1);
					}

					intData[frameIndex] = value / 3;
				}
				break;
			case 4:
				for (int frameIndex = 0; frameIndex < numFrames; frameIndex++) {

					int value = 0;
					{
						int value1 = unsignedByte(fileByteBuffer.get());
						int value2 = unsignedByte(fileByteBuffer.get());
						value += (short) ((value2 << 8) + value1);
					}
					{
						int value1 = unsignedByte(fileByteBuffer.get());
						int value2 = unsignedByte(fileByteBuffer.get());
						value += (short) ((value2 << 8) + value1);
					}
					{
						int value1 = unsignedByte(fileByteBuffer.get());
						int value2 = unsignedByte(fileByteBuffer.get());
						value += (short) ((value2 << 8) + value1);
					}
					{
						int value1 = unsignedByte(fileByteBuffer.get());
						int value2 = unsignedByte(fileByteBuffer.get());
						value += (short) ((value2 << 8) + value1);
					}

					intData[frameIndex] = value / 4;
				}
				break;
			}
			break;
		case 24:
			switch (audioFile.numChannels) {
			case 1:
				for (int frameIndex = 0; frameIndex < numFrames; frameIndex++) {

					int value = 0;
					{
						int value1 = unsignedByte(fileByteBuffer.get());
						int value2 = unsignedByte(fileByteBuffer.get());
						int value3 = unsignedByte(fileByteBuffer.get());
						value += ((int) (((((value3 << 8) + value2) << 8) + value1) << 8)) / 8; // ?? / 256 instead?
					}

					intData[frameIndex] = value;
				}
				break;
			case 2:
				for (int frameIndex = 0; frameIndex < numFrames; frameIndex++) {

					int value = 0;
					{
						int value1 = unsignedByte(fileByteBuffer.get());
						int value2 = unsignedByte(fileByteBuffer.get());
						int value3 = unsignedByte(fileByteBuffer.get());
						value += ((int) (((((value3 << 8) + value2) << 8) + value1) << 8)) / 8; // ?? / 256 instead?
					}
					{
						int value1 = unsignedByte(fileByteBuffer.get());
						int value2 = unsignedByte(fileByteBuffer.get());
						int value3 = unsignedByte(fileByteBuffer.get());
						value += ((int) (((((value3 << 8) + value2) << 8) + value1) << 8)) / 8; // ?? / 256 instead?
					}

					intData[frameIndex] = value / 2;
				}
				break;
			case 3:
				for (int frameIndex = 0; frameIndex < numFrames; frameIndex++) {

					int value = 0;
					{
						int value1 = unsignedByte(fileByteBuffer.get());
						int value2 = unsignedByte(fileByteBuffer.get());
						int value3 = unsignedByte(fileByteBuffer.get());
						value += ((int) (((((value3 << 8) + value2) << 8) + value1) << 8)) / 8; // ?? / 256 instead?
					}
					{
						int value1 = unsignedByte(fileByteBuffer.get());
						int value2 = unsignedByte(fileByteBuffer.get());
						int value3 = unsignedByte(fileByteBuffer.get());
						value += ((int) (((((value3 << 8) + value2) << 8) + value1) << 8)) / 8; // ?? / 256 instead?
					}
					{
						int value1 = unsignedByte(fileByteBuffer.get());
						int value2 = unsignedByte(fileByteBuffer.get());
						int value3 = unsignedByte(fileByteBuffer.get());
						value += ((int) (((((value3 << 8) + value2) << 8) + value1) << 8)) / 8; // ?? / 256 instead?
					}

					intData[frameIndex] = value / 3;
				}
				break;
			case 4:
				for (int frameIndex = 0; frameIndex < numFrames; frameIndex++) {

					int value = 0;
					{
						int value1 = unsignedByte(fileByteBuffer.get());
						int value2 = unsignedByte(fileByteBuffer.get());
						int value3 = unsignedByte(fileByteBuffer.get());
						value += ((int) (((((value3 << 8) + value2) << 8) + value1) << 8)) / 8; // ?? / 256 instead?
					}
					{
						int value1 = unsignedByte(fileByteBuffer.get());
						int value2 = unsignedByte(fileByteBuffer.get());
						int value3 = unsignedByte(fileByteBuffer.get());
						value += ((int) (((((value3 << 8) + value2) << 8) + value1) << 8)) / 8; // ?? / 256 instead?
					}
					{
						int value1 = unsignedByte(fileByteBuffer.get());
						int value2 = unsignedByte(fileByteBuffer.get());
						int value3 = unsignedByte(fileByteBuffer.get());
						value += ((int) (((((value3 << 8) + value2) << 8) + value1) << 8)) / 8; // ?? / 256 instead?
					}
					{
						int value1 = unsignedByte(fileByteBuffer.get());
						int value2 = unsignedByte(fileByteBuffer.get());
						int value3 = unsignedByte(fileByteBuffer.get());
						value += ((int) (((((value3 << 8) + value2) << 8) + value1) << 8)) / 8; // ?? / 256 instead?
					}

					intData[frameIndex] = value / 4;
				}
				break;
			}
			

		case 32:
			switch (audioFile.numChannels) {
			case 1:
				for (int frameIndex = 0; frameIndex < numFrames; frameIndex++) {

					int value = 0;
					{
						int value1 = unsignedByte(fileByteBuffer.get());
						int value2 = unsignedByte(fileByteBuffer.get());
						int value3 = unsignedByte(fileByteBuffer.get());
						int value4 = unsignedByte(fileByteBuffer.get());
						value += ((int) ((((((value4 << 8) + value3) << 8) + value2) << 8) + value1) << 8) / 8; // ?? / 256 instead?
					}

					intData[frameIndex] = value;
				}
				break;
			case 2:
				for (int frameIndex = 0; frameIndex < numFrames; frameIndex++) {

					int value = 0;
					{
						int value1 = unsignedByte(fileByteBuffer.get());
						int value2 = unsignedByte(fileByteBuffer.get());
						int value3 = unsignedByte(fileByteBuffer.get());
						int value4 = unsignedByte(fileByteBuffer.get());
						value += ((int) ((((((value4 << 8) + value3) << 8) + value2) << 8) + value1) << 8) / 8; // ?? / 256 instead?
					}
					{
						int value1 = unsignedByte(fileByteBuffer.get());
						int value2 = unsignedByte(fileByteBuffer.get());
						int value3 = unsignedByte(fileByteBuffer.get());
						int value4 = unsignedByte(fileByteBuffer.get());
						value += ((int) ((((((value4 << 8) + value3) << 8) + value2) << 8) + value1) << 8) / 8; // ?? / 256 instead?
					}

					intData[frameIndex] = value / 2;
				}
				break;
			case 3:
				for (int frameIndex = 0; frameIndex < numFrames; frameIndex++) {

					int value = 0;
					{
						int value1 = unsignedByte(fileByteBuffer.get());
						int value2 = unsignedByte(fileByteBuffer.get());
						int value3 = unsignedByte(fileByteBuffer.get());
						int value4 = unsignedByte(fileByteBuffer.get());
						value += ((int) ((((((value4 << 8) + value3) << 8) + value2) << 8) + value1) << 8) / 8; // ?? / 256 instead?
					}
					{
						int value1 = unsignedByte(fileByteBuffer.get());
						int value2 = unsignedByte(fileByteBuffer.get());
						int value3 = unsignedByte(fileByteBuffer.get());
						int value4 = unsignedByte(fileByteBuffer.get());
						value += ((int) ((((((value4 << 8) + value3) << 8) + value2) << 8) + value1) << 8) / 8; // ?? / 256 instead?
					}
					{
						int value1 = unsignedByte(fileByteBuffer.get());
						int value2 = unsignedByte(fileByteBuffer.get());
						int value3 = unsignedByte(fileByteBuffer.get());
						int value4 = unsignedByte(fileByteBuffer.get());
						value += ((int) ((((((value4 << 8) + value3) << 8) + value2) << 8) + value1) << 8) / 8; // ?? / 256 instead?
					}

					intData[frameIndex] = value / 3;
				}
				break;
			case 4:
				for (int frameIndex = 0; frameIndex < numFrames; frameIndex++) {

					int value = 0;
					{
						int value1 = unsignedByte(fileByteBuffer.get());
						int value2 = unsignedByte(fileByteBuffer.get());
						int value3 = unsignedByte(fileByteBuffer.get());
						int value4 = unsignedByte(fileByteBuffer.get());
						value += ((int) (((((value3 << 8) + value2) << 8) + value1) << 8)) / 8; // ?? / 256 instead?
					}
					{
						int value1 = unsignedByte(fileByteBuffer.get());
						int value2 = unsignedByte(fileByteBuffer.get());
						int value3 = unsignedByte(fileByteBuffer.get());
						int value4 = unsignedByte(fileByteBuffer.get());
						value += ((int) (((((value3 << 8) + value2) << 8) + value1) << 8)) / 8; // ?? / 256 instead?
					}
					{
						int value1 = unsignedByte(fileByteBuffer.get());
						int value2 = unsignedByte(fileByteBuffer.get());
						int value3 = unsignedByte(fileByteBuffer.get());
						int value4 = unsignedByte(fileByteBuffer.get());
						value += ((int) (((((value3 << 8) + value2) << 8) + value1) << 8)) / 8; // ?? / 256 instead?
					}
					{
						int value1 = unsignedByte(fileByteBuffer.get());
						int value2 = unsignedByte(fileByteBuffer.get());
						int value3 = unsignedByte(fileByteBuffer.get());
						int value4 = unsignedByte(fileByteBuffer.get());
						value += ((int) (((((value3 << 8) + value2) << 8) + value1) << 8)) / 8; // ?? / 256 instead?
					}

					intData[frameIndex] = value / 4;
				}
				break;
			}
			
			break;
		}

		// remove any DC offset
		if (removeDCOffset) {
			long sum = 0;
			for (int i = 0; i < numFrames; i++) {
				sum += intData[i];
			}
			int average = (int) (sum / numFrames);
			for (int i = 0; i < numFrames; i++) {
				intData[i] -= average;
			}
		}

		try {
			file.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return intData;

	}

}
