package arlo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NesterTagClass {

	int id;

	int user_id;
	int project_id;

	String className;
	String displayName;

	NesterTagClass(int id, int user_id, int project_id, String className, String displayName)
	{
		this.id = id;
		this.user_id = user_id;
		this.project_id = project_id;

		this.className = className;
		this.displayName = displayName;
	}


	/** Get a list of TagClasses in a Project.
	* 
	* @param projectId Database ID of the Project.
	* @param connection An opened Database connection.
	* @return A list of NesterTagClass objects.
	*/

	public static Vector<NesterTagClass> getNesterProjectTagClasses(int projectId, Connection connection) {

		Vector<NesterTagClass> tagClasses = new Vector<NesterTagClass>();

		try {

			// logger.log(Level.INFO, "connection = " + connection);

			Statement s1 = connection.createStatement();
			s1.executeQuery("select * from tools_tagclass where project_id = " + projectId);
			ResultSet rs1 = s1.getResultSet();

			while (rs1.next()) {
				NesterTagClass nesterTagClass = null;

				nesterTagClass = new NesterTagClass(rs1.getInt("id"), rs1.getInt("user_id"), rs1.getInt("project_id"),
					rs1.getString("className"), rs1.getString("displayName"));

				tagClasses.add(nesterTagClass);

			}
			rs1.close();
			s1.close();

			return tagClasses;

		} catch (Exception e) {
			Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
			logger.log(Level.INFO, "Exception = " + e);
		}

		return tagClasses;
	}

	/** Get a NesterTagClass from a specified Project by name.
	 *
	 * @note If more than one TagClass in the Project has the same name, the first is returned. 
	 * @param name The className of the TagClass.
	 * @param projectId The database id of the Project to search within.
	 * @param connection An opened database connection.
	 * @return An initialized NesterTagClass, or null if not found.
	 */

	public static NesterTagClass getNesterTagClassByName(String name, int projectId, Connection connection) {

		NesterTagClass tagClass = null;

		try {

			Statement s1 = connection.createStatement();
			s1.executeQuery("SELECT * FROM tools_tagclass WHERE project_id = " + projectId + " AND className = '" + name + "'");
			ResultSet rs1 = s1.getResultSet();

			if (rs1.next()) {
				tagClass = new NesterTagClass(rs1.getInt("id"), rs1.getInt("user_id"), rs1.getInt("project_id"),
					rs1.getString("className"), rs1.getString("displayName"));
			}

//			if (rs1.next()) {
//				// TODO warn about multiple records
//			}

			rs1.close();
			s1.close();

		} catch (Exception e) {
			Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
			logger.log(Level.INFO, "Exception = " + e);
		}

		return tagClass;
	}

	/** Add a new TagClass to the database.
	 *
	 * @param nesterTagClass A NesterTagClass Object
	 * @param connection An opened database connection.
	 * @return Database id of the new class, -1 if any error occurs.
	 */

	public static int addTagClass(NesterTagClass nesterTagClass, Connection connection) {

		boolean success = false;
		int id = -1;
		try {

			Statement stmt1 = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			ResultSet uprs1 = stmt1.executeQuery("SELECT * FROM tools_tagclass");

			uprs1.moveToInsertRow();
			uprs1.updateInt("user_id", nesterTagClass.user_id);
			uprs1.updateInt("project_id", nesterTagClass.project_id);
			uprs1.updateString("className", nesterTagClass.className);
			uprs1.updateString("displayName", nesterTagClass.displayName);
			uprs1.insertRow();

			uprs1 = stmt1.executeQuery("SELECT LAST_INSERT_ID()");

			if (uprs1.next()) {
				id = uprs1.getInt(1);
				nesterTagClass.id = id;
			} else {
				Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
				logger.log(Level.SEVERE, "Failed to get new id from addTagClass()");
				return -1;
			}

			uprs1.close();
			stmt1.close();

		} catch (Exception e) {
			Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
			logger.log(Level.SEVERE, "Failed to addTagClass() - Exception = " + e);
			return -1;
		}

		return id;
	}


}
