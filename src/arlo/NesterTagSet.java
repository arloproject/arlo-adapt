package arlo;

import java.util.logging.Level; 
import java.util.logging.Logger;
import java.util.Vector;
import java.util.Iterator;
import java.util.HashMap;
import java.util.Set;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;


public class NesterTagSet {

	/** \brief Get a list of Tags in a TagSet.
	* 
	* @param tagSetID Database ID of the TagSet
	* @param connection An opened Database connection.
	* @return A list of NesterTag objects.
	*/

	public static Vector<NesterTag> getNesterTagSetTags(int tagSetID, Connection connection) {
		Vector<Integer> tagSetIds = new Vector<Integer>();
		tagSetIds.add( (Integer) tagSetID);
		return getNesterTagSetTags(tagSetIds, connection);
	}


	public static Vector<NesterTag> getNesterTagSetTags(Vector<Integer> tagSetIds, Connection connection) {
		return getNesterTagSetTags(tagSetIds, null, connection);
	}


	/** \brief Get a list of Tags in a list of TagSets.
	 * 
	 * @param tagSetIds A list of Database IDs of the TagSets
	 * @param mediaFileIds If not null, restrict returned tags to only UserTags and Tags from the specified MediaFiles
	 * @note This is kind of a weird overload. It will return User Tags from ANY mediaFile, and all Tags from specified MediaFiles.
	 * @param connection An opened Database connection.
	 * @return A list of NesterTag objects.
	 */

	public static Vector<NesterTag> getNesterTagSetTags(Vector<Integer> tagSetIds, Vector<Integer> mediaFileIds, Connection connection) {

		// build a comma-separated list of Ids
		String tagSetsCSV = "";
		{
			Iterator i = tagSetIds.iterator();
			while (i.hasNext()) {
				tagSetsCSV += i.next() + ",";
			}
		}
		tagSetsCSV = tagSetsCSV.substring(0,tagSetsCSV.length() - 1); // remove trailing comma

		// build a comma-separated list of MediaFile Ids
		String mediaFileIDsCSV = "";
		{
			if (mediaFileIds != null) {
				Iterator i = mediaFileIds.iterator();
				while (i.hasNext()) {
					mediaFileIDsCSV += i.next() + ",";
				}
				mediaFileIDsCSV = mediaFileIDsCSV.substring(0, mediaFileIDsCSV.length() - 1); // remove trailing comma
			}
		}

		Vector<NesterTag> tags = new Vector<NesterTag>();
		try {
			Statement s1 = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			String sql = 
			sql = "SELECT " +
				" id, user_id, tagSet_id, mediaFile_id, creationDate, startTime, endTime, minFrequency, maxFrequency, " +
				" tagClass_id, parentTag_id, randomlyChosen, machineTagged, userTagged, strength " + 
				" FROM tools_tag " + 
				" WHERE tagSet_id IN (" + tagSetsCSV + ")";
			if (mediaFileIds != null) {
				sql += " AND (userTagged = 1 OR mediaFile_id IN (" + mediaFileIDsCSV + "))";
			}

			s1.executeQuery(sql);
			ResultSet rs1 = s1.getResultSet();

			while (rs1.next()) {

				//// 
				// Add Tags

				NesterTag nesterTag = new NesterTag(
					rs1.getInt("id"),
					rs1.getInt("user_id"),
					rs1.getTimestamp("creationDate"),
					rs1.getInt("tagClass_id"),
					rs1.getInt("tagSet_id"),
					rs1.getInt("mediaFile_id"),
					rs1.getDouble("startTime"),
					rs1.getDouble("endTime"),
					rs1.getDouble("minFrequency"),
					rs1.getDouble("maxFrequency"),
					rs1.getInt("parentTag_id"),
					rs1.getBoolean("randomlyChosen"),
					rs1.getBoolean("machineTagged"),
					rs1.getBoolean("userTagged"),
					rs1.getDouble("strength"));

				// add to map
				tags.add(nesterTag);

			}
			rs1.close();
			s1.close();

			// convert map to Vector

			System.gc();

			return tags;

		} catch (Exception e) {
			Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
			logger.log(Level.INFO, "name not found");
			logger.log(Level.INFO, "Exception = " + e);

			return null;
		}
	}



}
