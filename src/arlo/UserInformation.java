package arlo;

import java.io.Serializable;

public class UserInformation implements Serializable {

	public String firstName;
	public String lastName;
	public String affiliation1;
	public String affiliation2;
	public String affiliation3;
	public String eMailAddress;
	public String officePhoneNumber;
	public String cellPhoneNumber;
	public String homePhoneNumber;

}
