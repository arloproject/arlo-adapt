package arlo;

import java.sql.Connection;
import java.sql.Timestamp;
import java.sql.ResultSet;
import java.sql.Statement;

class Heartbeat extends Thread {
	
	int restTimeInSeconds = 600;

	Connection connection = null;

	Heartbeat(Connection connection) {
		this.connection = connection;
		System.out.println("Heartbeat restTimeInSeconds =  "  + restTimeInSeconds);
	}

	public void run() {

		while (true) {

			try {

				Statement s = connection.createStatement();
				s.executeQuery("select now()");
				ResultSet rs = s.getResultSet();
				while (rs.next()) {

					Timestamp date = rs.getTimestamp(1);
					System.out.println("database alive at: "  + date);

				}
				Thread.sleep(restTimeInSeconds * 1000);

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

			}

		}

	}
}
