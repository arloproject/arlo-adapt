package arlo;

import java.util.Vector;
import java.sql.Connection;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.StringWriter;
import java.io.PrintWriter;
import java.io.OutputStream;

import arlo.QueueTask;
import arlo.NesterJob;
import arlo.ArloSettings;

import arlo.SVMModelRunner;

public class QueueTask_WekaJobWrapper extends QueueTask {

	static final private String taskName = "WekaJobWrapper";

	public boolean runQueue(Vector<NesterUser> restrictedUsers) {
		return super._runQueue(taskName, restrictedUsers);
	}

    void runJob(NesterJob job) {

		if (job == null) {
			System.out.println("QueueTask_WekaJobWrapper::runJob received null job");
			return;
		}

		System.out.println("Starting QueueTask_WekaJobWrapper::runJob jobId = " + job.id);

		Connection connection = QueueTask.openDatabaseConnection();

		if (RunJob(job, connection)) {
			setJobStatusComplete(job.id, connection);
		} else {
			setJobStatusError(job.id, connection);  // error occured
		}

		closeDatabaseConnection(connection);
	}


	/** \brief Run a 'WekaJobWrapper' task
	 *
	 * This is a wrapper around calling Weka with a Job. 
	 * @param job the NesterJob object of the job.
	 * @param connection Opened database connection.
	 * @return true if success, false if any error encountered. 
	 */
	
	boolean RunJob(NesterJob job, Connection connection) {
		Logger logger;
		logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	
		//////////////////////////
		//                      //
		//  Get Job Parameters  //
		//                      //
		//////////////////////////

logger.log(Level.INFO, "Retrieving Parameters");

		String value;

		String wekaFunction = null;
		wekaFunction = job.GetJobParameter("wekaFunction");
		if (wekaFunction == null) {
			job.SaveJobLogMessage("Missing Parameter 'wekaFunction'");
			return false;
		}

		int sourceTagSetId = -1;
		value = job.GetJobParameter("sourceTagSet");
		if (value != null) {
			sourceTagSetId = Integer.parseInt(value);
		} else {
			job.SaveJobLogMessage("Missing Parameter 'sourceTagSet'");
			return false;
		}
		
		int destinationTagSetId = -1;
		value = job.GetJobParameter("destinationTagSet");
		if (value != null) {
			destinationTagSetId = Integer.parseInt(value);
		} else {
			job.SaveJobLogMessage("Missing Parameter 'destinationTagSet'");
			return false;
		}
		
		////
		// Get Spectra Parameters

		int numFrequencyBands;
		int numTimeFramesPerSecond;
		double dampingRatio;
		double minFrequency;
		double maxFrequency;

		value = job.GetJobParameter("numFrequencyBands");
		if (value != null) {
			numFrequencyBands = Integer.parseInt(value);
		} else {
			job.SaveJobLogMessage("Missing Parameter 'numFrequencyBands'");
			return false;
		}
		
		value = job.GetJobParameter("numTimeFramesPerSecond");
		if (value != null) {
			numTimeFramesPerSecond = Integer.parseInt(value);
		} else {
			job.SaveJobLogMessage("Missing Parameter 'numTimeFramesPerSecond'");
			return false;
		}
		
		value = job.GetJobParameter("dampingRatio");
		if (value != null) {
			dampingRatio = Double.parseDouble(value);
		} else {
			job.SaveJobLogMessage("Missing Parameter 'dampingRatio'");
			return false;
		}
		
		value = job.GetJobParameter("minFrequency");
		if (value != null) {
			minFrequency = Double.parseDouble(value);
		} else {
			job.SaveJobLogMessage("Missing Parameter 'minFrequency'");
			return false;
		}
		
		value = job.GetJobParameter("maxFrequency");
		if (value != null) {
			maxFrequency = Double.parseDouble(value);
		} else {
			job.SaveJobLogMessage("Missing Parameter 'maxFrequency'");
			return false;
		}


		///////////////////////////////
		//                           //
		//         Call Weka         //
		//                           //
		///////////////////////////////

		// Currently only SVM supported
		if (! wekaFunction.equals("svm")) {
			job.SaveJobLogMessage("Only 'svm' wekaFunction supported. (Passed '" + wekaFunction + "')");
			return false;
		}
		
		int numFramesPerExample;
		double complexityConstant;
		
		value = job.GetJobParameter("numFramesPerExample");
		if (value != null) {
			numFramesPerExample = Integer.parseInt(value);
		} else {
			job.SaveJobLogMessage("Missing Parameter 'numFramesPerExample'");
			return false;
		}
		
		value = job.GetJobParameter("complexityConstant");
		if (value != null) {
			complexityConstant = Double.parseDouble(value);
		} else {
			job.SaveJobLogMessage("Missing Parameter 'complexityConstant'");
			return false;
		}

		try {

			SVMModelRunner modeler = new SVMModelRunner();
			// API Credentials
			modeler.setAccount(ArloSettings.QueueRunnerAPIUserName);
			modeler.setHost(ArloSettings.QueueRunnerAPIHost);
			modeler.setPassword(ArloSettings.QueueRunnerAPIPassword);
			modeler.setPort(ArloSettings.QueueRunnerAPIPort);
			// Weka Settings
			modeler.setComplexityConstant(complexityConstant);
			modeler.setFramesPerExample(numFramesPerExample);
			modeler.setFramesPerSecond(numTimeFramesPerSecond);
			modeler.setFrequencyBands(numFrequencyBands);
			modeler.setLabelTagSet(sourceTagSetId);
			modeler.setCalledTagSet(destinationTagSetId);
			modeler.setMinFrequency((int) minFrequency);
			modeler.setMaxFrequency((int) maxFrequency);
			modeler.setDampingFactor(dampingRatio);
			try {
				modeler.execute("TestThis", (OutputStream) null);
				logger.log(Level.INFO, "Success running Weka");
				return true;
			} catch (Exception e) {
				StringWriter writer = new StringWriter();
				PrintWriter printWriter = new PrintWriter( writer );
				e.printStackTrace( printWriter );
				printWriter.flush();
				String stackTrace = writer.toString();
		
				logger.log(Level.SEVERE, "QueueTask_WekaJobWrapper::Weka Exception Executing Weka " + e + " -- " + stackTrace);
				return false;
			}
	
		} catch (Exception e) {
			StringWriter writer = new StringWriter();
			PrintWriter printWriter = new PrintWriter( writer );
			e.printStackTrace( printWriter );
			printWriter.flush();
			String stackTrace = writer.toString();
	
			logger.log(Level.SEVERE, "QueueTask_WekaJobWrapper::Weka Failed Exception" + e + " -- " + stackTrace);
			return false;
		}
	}

}

