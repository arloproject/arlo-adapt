package arlo;

import java.util.Vector;

public class NesterPersystFile {
	
	public int MAX_NUM_CHANNELS = 4;

	// [NP_Configuration]
	public String NPConfigStr;

	// [NP_Patient]
	public String PatientInitials;
	public String PatientGender;
	public String PatientIdentifier;

	// [NP_FileInfo]
	public String LayFileVersion;
	public String LayoutFileTimeStampAsLocalTime;
	public String LayoutFileTimeStampTimeZone;
	public String LayoutFileTimeStampOffsetHoursFromGMT;
	public String LayoutFileTimeStampOffsetMinutesFromGMT;
	public long LayoutFileTimeStampAsUTC;

	public String ECoGTimeStampAsLocalTime;
	public String ECoGTimeStampTimeZone;
	public String ECoGTimeStampOffsetHoursFromGMT;
	public String ECoGTimeStampOffsetMinutesFromGMT;
	public long ECoGTimeStampAsUTC;

	public String ProgrammerSerialNumber;
	public String ProgrammerSoftwareRevision;
	public String AnnotatedProgrammerSerialNumber;
	public String AnnotatedProgrammerSoftwareRevision;

	public int DeviceSerialNumber = 107067;
	public String DeviceModelNumber;
	public String DeviceSoftwareVersion;
	public String DeviceHardwareVersion;
	public String DeviceROMVersion;
	public String DeviceHollyDogTag;
	public String DeviceListerDogTag;
	public String ProgrammingParameterFile;
	public String DATFile;
	public String NPFileType;
	public String DeviceRecordsUsed;

	public Vector<String> Annotations = new Vector<String>();

	public String UserComments;

	// [NP_Comments]
	public String FeatureLabels;

	public String TriggerReason;

	// [NP_Parameters]
	public String AmplifierChannel1SampleRate;
	public String AmplifierChannel2SampleRate;
	public String AmplifierChannel3SampleRate;
	public String AmplifierChannel4SampleRate;
	public String AmplifierChannel1ECoGStorageEnabled;
	public String AmplifierChannel2ECoGStorageEnabled;
	public String AmplifierChannel3ECoGStorageEnabled;
	public String AmplifierChannel4ECoGStorageEnabled;
	public String AmplifierChannel1StoredECoGSamplingInterval;
	public String AmplifierChannel2StoredECoGSamplingInterval;
	public String AmplifierChannel3StoredECoGSamplingInterval;
	public String AmplifierChannel4StoredECoGSamplingInterval;
	public String AmplifierChannel1RealTimeECogSamplingInterval;
	public String AmplifierChannel2RealTimeECogSamplingInterval;
	public String AmplifierChannel3RealTimeECogSamplingInterval;
	public String AmplifierChannel4RealTimeECogSamplingInterval;
	public String AmplifierChannel1Gain;
	public String AmplifierChannel2Gain;
	public String AmplifierChannel3Gain;
	public String AmplifierChannel4Gain;
	public String AmplifierChannel1LowPassFilterFrequency;
	public String AmplifierChannel2LowPassFilterFrequency;
	public String AmplifierChannel3LowPassFilterFrequency;
	public String AmplifierChannel4LowPassFilterFrequency;
	public String AmplifierChannel1HighPassFilterFrequency;
	public String AmplifierChannel2HighPassFilterFrequency;
	public String AmplifierChannel3HighPassFilterFrequency;
	public String AmplifierChannel4HighPassFilterFrequency;
	public String Amplifier1LineLengthTrendWindowSize;
	public String Amplifier2LineLengthTrendWindowSize;
	public String Amplifier1LineLengthTrendISInterval;
	public String Amplifier2LineLengthTrendISInterval;
	public String Amplifier1LineLengthTrendSampleCount;
	public String Amplifier2LineLengthTrendSampleCount;
	public String Amplifier1AreaTrendWindowSize;
	public String Amplifier2AreaTrendWindowSize;
	public String Amplifier1AreaTrendInterSampleInterval;
	public String Amplifier2AreaTrendInterSampleInterval;
	public String Amplifier1AreaTrendSampleCount;
	public String Amplifier2AreaTrendSampleCount;
	public String FirstAmplifierChannel;
	public String SecondAmplifierChannel;
	public String NPPreTriggerBufferLength;
	public String NPPostTriggerBufferLength;

	public String PinLabelInput1;
	public String PinLabelInput2;
	public String PinLabelInput3;
	public String PinLabelInput4;
	public String PinLabelInput5;
	public String PinLabelInput6;
	public String PinLabelInput7;
	public String PinLabelInput8;

	public String DetectionSetName;
	public String ResponsiveTherapySequence;
	public String ResponsiveTherapyEnable;

	public String CHANNEL_1_LL;
	public String CHANNEL_1_LL_SAMPLE_COUNT;
	public String CHANNEL_2_LL;
	public String CHANNEL_2_LL_SAMPLE_COUNT;
	public String CHANNEL_1_AREA;
	public String CHANNEL_1_AREA_SAMPLE_COUNT;
	public String CHANNEL_2_AREA;
	public String CHANNEL_2_AREA_SAMPLE_COUNT;

	// [FileInfo]
	public String File;
	public String FileType;
	public int SamplingRate;
	public String HeaderLength;
	public String Calibration;
	public int WaveformCount;
	public String DataType;

	// [ChannelMap]
	public String[] ChannelMap = new String[MAX_NUM_CHANNELS];

	public NesterPersystFile(String[] lines) {

		for (int i = 0; i < lines.length; i++) {

			String line = lines[i];
			// System.out.println(line);

			if (line.indexOf("=") == -1)
				continue;

			String[] parts = line.split("=");

			if (parts.length > 2) {
				System.out.println("ERROR:  (parts.length > 2) line = " + line);
				continue;
			}

			String variable = parts[0];
			String value = null;
			if (parts.length == 2)
				value = parts[1];
			else
				value = null;

			if (variable.equals("NPConfigStr"))
				NPConfigStr = value;

			else if (variable.equals("PatientInitials"))
				PatientInitials = value;
			else if (variable.equals("PatientGender"))
				PatientGender = value;
			else if (variable.equals("PatientIdentifier"))
				PatientIdentifier = value;

			else if (variable.equals("LayFileVersion"))
				LayFileVersion = value;
			else if (variable.equals("LayoutFileTimeStampAsLocalTime"))
				LayoutFileTimeStampAsLocalTime = value;
			else if (variable.equals("LayoutFileTimeStampTimeZone"))
				LayoutFileTimeStampTimeZone = value;
			else if (variable.equals("LayoutFileTimeStampOffsetHoursFromGMT"))
				LayoutFileTimeStampOffsetHoursFromGMT = value;
			else if (variable.equals("LayoutFileTimeStampOffsetMinutesFromGMT"))
				LayoutFileTimeStampOffsetMinutesFromGMT = value;
			else if (variable.equals("LayoutFileTimeStampAsUTC"))
				LayoutFileTimeStampAsUTC = Long.parseLong(value);

			else if (variable.equals("ECoGTimeStampAsLocalTime"))
				ECoGTimeStampAsLocalTime = value;
			else if (variable.equals("ECoGTimeStampTimeZone"))
				ECoGTimeStampTimeZone = value;
			else if (variable.equals("ECoGTimeStampOffsetHoursFromGMT"))
				ECoGTimeStampOffsetHoursFromGMT = value;
			else if (variable.equals("ECoGTimeStampOffsetMinutesFromGMT"))
				ECoGTimeStampOffsetMinutesFromGMT = value;
			else if (variable.equals("ECoGTimeStampAsUTC"))
				ECoGTimeStampAsUTC = Long.parseLong(value);

			else if (variable.equals("ProgrammerSerialNumber"))
				ProgrammerSerialNumber = value;
			else if (variable.equals("ProgrammerSoftwareRevision"))
				ProgrammerSoftwareRevision = value;
			else if (variable.equals("AnnotatedProgrammerSerialNumber"))
				AnnotatedProgrammerSerialNumber = value;
			else if (variable.equals("AnnotatedProgrammerSoftwareRevision"))
				AnnotatedProgrammerSoftwareRevision = value;

			else if (variable.equals("DeviceSerialNumber"))
				DeviceSerialNumber = Integer.parseInt(value);
			else if (variable.equals("DeviceModelNumber"))
				DeviceModelNumber = value;
			else if (variable.equals("DeviceSoftwareVersion"))
				DeviceSoftwareVersion = value;
			else if (variable.equals("DeviceHardwareVersion"))
				DeviceHardwareVersion = value;
			else if (variable.equals("DeviceROMVersion"))
				DeviceROMVersion = value;
			else if (variable.equals("DeviceHollyDogTag"))
				DeviceHollyDogTag = value;
			else if (variable.equals("DeviceListerDogTag"))
				DeviceListerDogTag = value;
			else if (variable.equals("ProgrammingParameterFile"))
				ProgrammingParameterFile = value;
			else if (variable.equals("DATFile"))
				DATFile = value;
			else if (variable.equals("NPFileType"))
				NPFileType = value;
			else if (variable.equals("DeviceRecordsUsed"))
				DeviceRecordsUsed = value;

			else if (variable.equals("Annotations"))
				Annotations.add(value);

			else if (variable.equals("UserComments"))
				UserComments = value;

			else if (variable.equals("FeatureLabels"))
				FeatureLabels = value;

			else if (variable.equals("TriggerReason"))
				TriggerReason = value;

			else if (variable.equals("AmplifierChannel1SampleRate"))
				AmplifierChannel1SampleRate = value;
			else if (variable.equals("AmplifierChannel2SampleRate"))
				AmplifierChannel2SampleRate = value;
			else if (variable.equals("AmplifierChannel3SampleRate"))
				AmplifierChannel3SampleRate = value;
			else if (variable.equals("AmplifierChannel4SampleRate"))
				AmplifierChannel4SampleRate = value;
			else if (variable.equals("AmplifierChannel1ECoGStorageEnabled"))
				AmplifierChannel1ECoGStorageEnabled = value;
			else if (variable.equals("AmplifierChannel2ECoGStorageEnabled"))
				AmplifierChannel2ECoGStorageEnabled = value;
			else if (variable.equals("AmplifierChannel3ECoGStorageEnabled"))
				AmplifierChannel3ECoGStorageEnabled = value;
			else if (variable.equals("AmplifierChannel4ECoGStorageEnabled"))
				AmplifierChannel4ECoGStorageEnabled = value;
			else if (variable.equals("AmplifierChannel1StoredECoGSamplingInterval"))
				AmplifierChannel1StoredECoGSamplingInterval = value;
			else if (variable.equals("AmplifierChannel2StoredECoGSamplingInterval"))
				AmplifierChannel2StoredECoGSamplingInterval = value;
			else if (variable.equals("AmplifierChannel3StoredECoGSamplingInterval"))
				AmplifierChannel3StoredECoGSamplingInterval = value;
			else if (variable.equals("AmplifierChannel4StoredECoGSamplingInterval"))
				AmplifierChannel4StoredECoGSamplingInterval = value;
			else if (variable.equals("AmplifierChannel1RealTimeECogSamplingInterval"))
				AmplifierChannel1RealTimeECogSamplingInterval = value;
			else if (variable.equals("AmplifierChannel2RealTimeECogSamplingInterval"))
				AmplifierChannel2RealTimeECogSamplingInterval = value;
			else if (variable.equals("AmplifierChannel3RealTimeECogSamplingInterval"))
				AmplifierChannel3RealTimeECogSamplingInterval = value;
			else if (variable.equals("AmplifierChannel4RealTimeECogSamplingInterval"))
				AmplifierChannel4RealTimeECogSamplingInterval = value;
			else if (variable.equals("AmplifierChannel1Gain"))
				AmplifierChannel1Gain = value;
			else if (variable.equals("AmplifierChannel2Gain"))
				AmplifierChannel2Gain = value;
			else if (variable.equals("AmplifierChannel3Gain"))
				AmplifierChannel3Gain = value;
			else if (variable.equals("AmplifierChannel4Gain"))
				AmplifierChannel4Gain = value;
			else if (variable.equals("AmplifierChannel1LowPassFilterFrequency"))
				AmplifierChannel1LowPassFilterFrequency = value;
			else if (variable.equals("AmplifierChannel2LowPassFilterFrequency"))
				AmplifierChannel2LowPassFilterFrequency = value;
			else if (variable.equals("AmplifierChannel3LowPassFilterFrequency"))
				AmplifierChannel3LowPassFilterFrequency = value;
			else if (variable.equals("AmplifierChannel4LowPassFilterFrequency"))
				AmplifierChannel4LowPassFilterFrequency = value;
			else if (variable.equals("AmplifierChannel1HighPassFilterFrequency"))
				AmplifierChannel1HighPassFilterFrequency = value;
			else if (variable.equals("AmplifierChannel2HighPassFilterFrequency"))
				AmplifierChannel2HighPassFilterFrequency = value;
			else if (variable.equals("AmplifierChannel3HighPassFilterFrequency"))
				AmplifierChannel3HighPassFilterFrequency = value;
			else if (variable.equals("AmplifierChannel4HighPassFilterFrequency"))
				AmplifierChannel4HighPassFilterFrequency = value;

			else if (variable.equals("Amplifier1LineLengthTrendWindowSize"))
				Amplifier1LineLengthTrendWindowSize = value;
			else if (variable.equals("Amplifier2LineLengthTrendWindowSize"))
				Amplifier2LineLengthTrendWindowSize = value;
			else if (variable.equals("Amplifier1LineLengthTrendISInterval"))
				Amplifier1LineLengthTrendISInterval = value;
			else if (variable.equals("Amplifier2LineLengthTrendISInterval"))
				Amplifier2LineLengthTrendISInterval = value;
			else if (variable.equals("Amplifier1AreaTrendWindowSize"))
				Amplifier1AreaTrendWindowSize = value;
			else if (variable.equals("Amplifier2AreaTrendWindowSize"))
				Amplifier2AreaTrendWindowSize = value;
			else if (variable.equals("Amplifier1LineLengthTrendSampleCount"))
				Amplifier1LineLengthTrendSampleCount = value;
			else if (variable.equals("Amplifier2LineLengthTrendSampleCount"))
				Amplifier2LineLengthTrendSampleCount = value;
			else if (variable.equals("Amplifier1AreaTrendInterSampleInterval"))
				Amplifier1AreaTrendInterSampleInterval = value;
			else if (variable.equals("Amplifier2AreaTrendInterSampleInterval"))
				Amplifier2AreaTrendInterSampleInterval = value;
			else if (variable.equals("Amplifier1AreaTrendSampleCount"))
				Amplifier1AreaTrendSampleCount = value;
			else if (variable.equals("Amplifier2AreaTrendSampleCount"))
				Amplifier2AreaTrendSampleCount = value;
			else if (variable.equals("FirstAmplifierChannel"))
				FirstAmplifierChannel = value;
			else if (variable.equals("SecondAmplifierChannel"))
				SecondAmplifierChannel = value;
			else if (variable.equals("PinLabelInput1"))
				PinLabelInput1 = value;
			else if (variable.equals("PinLabelInput1"))
				PinLabelInput1 = value;

			else if (variable.equals("NPPreTriggerBufferLength"))
				NPPreTriggerBufferLength = value;
			else if (variable.equals("NPPostTriggerBufferLength"))
				NPPostTriggerBufferLength = value;

			else if (variable.equals("PinLabelInput1"))
				PinLabelInput1 = value;
			else if (variable.equals("PinLabelInput2"))
				PinLabelInput2 = value;
			else if (variable.equals("PinLabelInput3"))
				PinLabelInput3 = value;
			else if (variable.equals("PinLabelInput4"))
				PinLabelInput4 = value;
			else if (variable.equals("PinLabelInput5"))
				PinLabelInput5 = value;
			else if (variable.equals("PinLabelInput6"))
				PinLabelInput6 = value;
			else if (variable.equals("PinLabelInput7"))
				PinLabelInput7 = value;
			else if (variable.equals("PinLabelInput8"))
				PinLabelInput8 = value;

			else if (variable.equals("DetectionSetName"))
				DetectionSetName = value;
			else if (variable.equals("ResponsiveTherapySequence"))
				ResponsiveTherapySequence = value;
			else if (variable.equals("ResponsiveTherapyEnable"))
				ResponsiveTherapyEnable = value;

			else if (variable.equals("CHANNEL_1_LL"))
				CHANNEL_1_LL = value;
			else if (variable.equals("CHANNEL_1_LL_SAMPLE_COUNT"))
				CHANNEL_1_LL_SAMPLE_COUNT = value;
			else if (variable.equals("CHANNEL_2_LL"))
				CHANNEL_2_LL = value;
			else if (variable.equals("CHANNEL_2_LL_SAMPLE_COUNT"))
				CHANNEL_2_LL_SAMPLE_COUNT = value;
			else if (variable.equals("CHANNEL_1_AREA"))
				CHANNEL_1_AREA = value;
			else if (variable.equals("CHANNEL_1_AREA_SAMPLE_COUNT"))
				CHANNEL_1_AREA_SAMPLE_COUNT = value;
			else if (variable.equals("CHANNEL_2_AREA"))
				CHANNEL_2_AREA = value;
			else if (variable.equals("CHANNEL_2_AREA_SAMPLE_COUNT"))
				CHANNEL_2_AREA_SAMPLE_COUNT = value;

			else if (variable.equals("File"))
				File = value;
			else if (variable.equals("FileType"))
				FileType = value;
			else if (variable.equals("SamplingRate"))
				SamplingRate = Integer.parseInt(value);
			else if (variable.equals("HeaderLength"))
				HeaderLength = value;
			else if (variable.equals("Calibration"))
				Calibration = value;
			else if (variable.equals("WaveformCount"))
				WaveformCount = Integer.parseInt(value);
			else if (variable.equals("DataType"))
				DataType = value;

			else if (variable.indexOf(" - ") != -1) {
				
				int channelIndex = Integer.parseInt(value) - 1;
				
				
//				System.out.println("channel map: " + variable + "=" + value);
			}

			else {

				System.out.println("ERROR:  varible not found line = " + line);
			}

		}

	}

}
