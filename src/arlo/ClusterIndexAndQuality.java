package arlo;

public class ClusterIndexAndQuality implements Comparable<ClusterIndexAndQuality>{
	
	int clusterIndex;
	double quality;
	
	
	public int compareTo(ClusterIndexAndQuality o) {
		
		
		if (this.quality > o.quality)
			return -1;
		if (this.quality == o.quality) 
			return 0;
		
		return 1;

	}
	
	ClusterIndexAndQuality (int clusterIndex, double population) {
		this.clusterIndex = clusterIndex;
		this.quality = population;
	}

}
