package arlo;

import java.util.Vector;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Time;
import java.sql.SQLException;

import arlo.NesterUser;
import arlo.ArloSettings;




public class QueueTask {


	public boolean runQueue(
		Vector<NesterUser> restrictedUsers)
	{
		System.out.println("QueueTask runQueue - proto - this one shouldn't be run");
		return false;
	}

	void runJob(NesterJob job) {
		System.out.println("runJob from BaseClass");
	}

	/** Get and execute a job from the queue. 
	 *
	 *  Originally, we executed the whole queue for this task before returning. However, now 
	 *  we execute one at a time to jump back to the beginning of the queue and look for 
	 *  higher priority tasks. 
	 *  \param taskName The job name stored in tools_jobtypes. This limits the search to 
	 *  only one job type at a time. 
	 *  \param restrictedUsers If null, search for jobs from any user; else, search from only specified users. NOTE: Not currently implemented.
	 *  \return true if a job was found and executed, false otherwise.
	 */

	protected boolean _runQueue(
		String taskName,
		Vector<NesterUser> restrictedUsers)
	{
//		System.out.println("QueueTask runQueue - Task: " + taskName);
		// TODO implement restrictedUsers

		NesterJob job = null;

// change to run 1 job, and return immediately - don't loop
//		do {
			job = getTaskFromQueue(taskName, null);
			if (job == null)
//				break;
				return false;
			runJob(job);
			return true;
//		} while (job != null);

//		return false;
	}


	//////////////////////////////////
	//                              //
	// Database Interface Functions //
	//                              //
	//////////////////////////////////


	/** Try to get a job from the queue. 
	 *
	 *  \param jobTypeString The job name stored in tools_jobtypes. This limits the search to 
	 *  only one job type at a time. 
	 *  \param user_id If null, search for jobs from any user; else, search from only specified user.
	 *  \return null if no task found. If a job is found and secured, return aNesterJob object
	 */

	static protected NesterJob getTaskFromQueue(String jobTypeString, Integer user_id) {

		int jobId = -1;

		// get Database Connection
		Statement stmt = null;
		Connection connection = openDatabaseConnection();
		if (connection == null) {
			System.out.println("Error!  Database connection failed in getTaskFromQueue");
			return null;
		}

		// we need a single atomic operation within the database to ensure that we alone grab the task

		// so, LOCK the TABLES
		try {
			stmt = connection.createStatement();
				stmt.executeUpdate("LOCK TABLES tools_jobs WRITE, tools_jobtypes WRITE, tools_jobstatustypes WRITE, tools_joblog WRITE;");
		} catch (SQLException e) {
			System.out.println("SQLState: " + e.getSQLState());
			System.out.println("Error Code: " + e.getErrorCode());
			System.out.println("Message: " + e.getMessage());
			closeDatabaseConnection(connection);
			return null;
		} catch (Exception e) {
			System.out.println("Exception = " + e);
			closeDatabaseConnection(connection);
			return null;
		}

		// check if there's a task
		String sql = 
			"SELECT id from tools_jobs WHERE status_id = " + 
				"(SELECT id FROM tools_jobstatustypes WHERE name = 'Queued') " + 
			" AND type_id = " + 
				"(SELECT id FROM tools_jobtypes WHERE name = '" + jobTypeString + 
			"')"; 
		if (user_id != null) {
			sql += " AND user_id = " + user_id;
		}
		sql += " ORDER BY priority ASC, id ASC LIMIT 1";  // order by priority, then by id (oldest first)

		ResultSet rs1 = null;
		try {
			stmt.executeQuery(sql);
			rs1 = stmt.getResultSet();

			if (rs1.next()) {
				jobId = rs1.getInt("id");

				rs1.last();
				if (rs1.getRow() > 1) {
					System.out.println("QueueTask::getTaskFromQueue() More than 1 row found when retrieving JobID.");
				}
			}
			rs1.close();
		} catch (SQLException e) {
			System.out.println("SQLState: " + e.getSQLState());
			System.out.println("Error Code: " + e.getErrorCode());
			System.out.println("Message: " + e.getMessage());
			closeDatabaseConnection(connection);
			return null;
		} catch (Exception e) {
			System.out.println("Exception = " + e);
			closeDatabaseConnection(connection);
			return null;
		}
			
		// if we got a job, make it ours
		if (jobId != -1) {
			if (! setJobStatusRunning(jobId, connection)) {
				closeDatabaseConnection(connection);
				return null;
			}
		}
		

		// UNLOCK TABLES
		try {
			stmt.executeUpdate("UNLOCK TABLES;");
		} catch (SQLException e) {
			System.out.println("SQLState: " + e.getSQLState());
			System.out.println("Error Code: " + e.getErrorCode());
			System.out.println("Message: " + e.getMessage());
			closeDatabaseConnection(connection);
			return null;
		} catch (Exception e) {
			System.out.println("Exception = " + e);
			closeDatabaseConnection(connection);
			return null;
		}

		if (jobId == -1) {
			closeDatabaseConnection(connection);
			return null;
		}

		// if we have a job, get it's info
		NesterJob job = new NesterJob();
		if (! job.getNesterJob(jobId, connection)) {
			// something went wrong, update the task to error status
			setJobStatusError(jobId, connection);
			closeDatabaseConnection(connection);
			return null;
		}

		closeDatabaseConnection(connection);
		return job;
	}



	// yeah, creating my own for the moment for lack of a current project-scope function...
	static Connection openDatabaseConnection() {
		String url = ArloSettings.dbUrl;
		String dbUserName = ArloSettings.dbUserName;
		String dbPassword = ArloSettings.dbPassword;
		Connection connection = null;

		try {
			connection = DriverManager.getConnection(url, dbUserName, dbPassword);
			return connection;
		} catch (SQLException e) {
			System.out.println("SQLState: " + e.getSQLState());
			System.out.println("Error Code: " + e.getErrorCode());
			System.out.println("Message: " + e.getMessage());
			return null;
		} catch (Exception e) {
			System.out.println("Exception = " + e);
			System.out.println("Error!  Database connection failed.");
			return null;
		}
	}

	static void closeDatabaseConnection(Connection connection) {
		if (connection == null) {
			return;
		}
		try {
			connection.close();
		} catch (SQLException e) {
			System.out.println("SQLState: " + e.getSQLState());
			System.out.println("Error Code: " + e.getErrorCode());
			System.out.println("Message: " + e.getMessage());
		} catch (Exception e) { 
			System.out.println("Exception = " + e);
			System.out.println("Error!  Database connection failed.");
		}
	}

	static boolean setJobStatus(String statusString, int jobId, Connection connection) {
		// change tools_jobs status and update lastStatusDate
		try {
			Statement stmt = connection.createStatement();
			String sql = 
				"UPDATE tools_jobs SET " + 
				"status_id = (SELECT id FROM tools_jobstatustypes WHERE name = '" + statusString + "'), " + 
				" lastStatusDate = NOW() " +
				"WHERE id = " + jobId; 
			stmt.executeUpdate(sql);
		} catch (SQLException e) {
			System.out.println("SQLState: " + e.getSQLState());
			System.out.println("Error Code: " + e.getErrorCode());
			System.out.println("Message: " + e.getMessage());
			return false;
		} catch (Exception e) {
			System.out.println("Exception = " + e);
			return false;
		}

		// log the status change
		try {
			NesterJob.SaveJobLogMessage(jobId, "Status Changed to '" + statusString + "' by '" + ArloSettings.hostname + "' (" + ArloSettings.processDescription + ")", connection);
		} catch (Exception e) {
			System.out.println("setJobStatus Failed Adding Log");
			System.out.println("Exception = " + e);
		}
		return true;
	}

	static boolean setJobStatusRunning(int jobId, Connection connection) {
		return setJobStatus("Running", jobId, connection);
	}

	static boolean setJobStatusComplete(int jobId, Connection connection) {
		return setJobStatus("Complete", jobId, connection);
	}

	static boolean setJobStatusError(int jobId, Connection connection) {
		return setJobStatus("Error", jobId, connection);
	}

	static boolean setJobStatusQueued(int jobId, Connection connection) {
		return setJobStatus("Queued", jobId, connection);
	}
}

