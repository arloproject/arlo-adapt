/**
 * University of Illinois/NCSA
 * Open Source License
 * 
 * Copyright (c) 2008, Board of Trustees-University of Illinois.  
 * All rights reserved.
 * 
 * Developed by: 
 * 
 * Automated Learning Group
 * National Center for Supercomputing Applications
 * http://www.seasr.org
 * 
 *  
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal with the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions: 
 * 
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimers. 
 * 
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimers in the 
 *    documentation and/or other materials provided with the distribution. 
 * 
 *  * Neither the names of Automated Learning Group, The National Center for
 *    Supercomputing Applications, or University of Illinois, nor the names of
 *    its contributors may be used to endorse or promote products derived from
 *    this Software without specific prior written permission. 
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 * CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * WITH THE SOFTWARE.
 */

package arlo;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.io.StringWriter;


import adapt.IO;
import adapt.SimpleTable;
import org.python.core.PyDictionary;


public class ServiceHead {

	String PROP_URL_PATH = "url_path";

	private Logger logger;

	private String executionInstanceId;

	public String test(PyDictionary dictionary) {

		int length = dictionary.__len__();
		System.out.println("dictionary = " + dictionary);
		System.out.println("length = " + length);

		return "success";
	}

	ConcurrentHashMap<String, WebServiceTask> taskNameToOutputConnector;
	Connection connection = null;

	QueueRunner queueRunner = null;

	public enum WebServiceTask {
		ANALYZE_AUDIO_FILE, CREATE_SPECTRA, CREATE_WAV_SEGMENT,
		//
		START_ADAPT_ANALYSIS, PAUSE_ADAPT_ANALYSIS, RESUME_ADAPT_ANALYSIS, STOP_ADAPT_ANALYSIS,
		//
		START_UNSUPERVISED_TAG_DISCOVERY, PAUSE_UNSUPERVISED_TAG_DISCOVERY, RESUME_UNSUPERVISED_TAG_DISCOVERY, STOP_UNSUPERVISED_TAG_DISCOVERY,
		//
		START_SUPERVISED_TAG_DISCOVERY, PAUSE_SUPERVISED_TAG_DISCOVERY, RESUME_SUPERVISED_TAG_DISCOVERY, STOP_SUPERVISED_TAG_DISCOVERY,
		//
		START_TRANSCRIPT, PAUSE_TRANSCRIPT, RESUME_TRANSCRIPT, STOP_TRANSCRIPT,
		//
		START_TAG_ANALYSIS, PAUSE_TAG_ANALYSIS, RESUME_TAG_ANALYSIS, STOP_TAG_ANALYSIS,
		//
		GET_AUDIO_SPECTRA_DATA, GET_AUDIO_PITCHTRACE_DATA,
		TEST_CALL,
	}

	static HashSet<Integer> stoppedJobIDs = new HashSet<Integer>();

	String hostname = null;

	static boolean initialized = false;
	
	public static void main(String[] args) {
		ServiceHead serviceHead = new ServiceHead();
		serviceHead.initialize();
		// serviceHead.startSupervisedTagDiscoveryJob(3071);
	}

	public void initialize() {

		if (initialized) {

			System.out.println("skipping initialize()");

		} else {

			System.out.println("running initialize()");

			try {
				hostname = java.net.InetAddress.getLocalHost().getHostName();
				System.out.println("hostname = " + hostname);
			} catch (UnknownHostException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}

			//// 
			// initialize global logger and override the defaults on the ConsoleHandler
			logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
			logger.setUseParentHandlers(false);

			// setup the console handler
			Handler consoleHandler = null;
			//see if there is already a console handler
			for (Handler handler : logger.getHandlers()) {
				if (handler instanceof ConsoleHandler) {
					//found the console handler
					consoleHandler = handler;
					break;
				}
			}

			if (consoleHandler == null) {
				//there was no console handler found, create a new one
				consoleHandler = new ConsoleHandler();
				logger.addHandler(consoleHandler);
			}   
			//set the console handler to fine:
			consoleHandler.setLevel(java.util.logging.Level.FINEST);

			////
			// Setup Configuration Parameters
			String configFileName = System.getProperty("ARLO_CONFIG_FILE");
			if (configFileName == null) {
				logger.log(Level.SEVERE, "Did Not Specifiy Config File. Set Environment Property on command line '-DARLO_CONFIG_FILE=ArloSettings.properties' (must be on ClassPath)");
				System.exit(1);
			}

			ArloSettings mySettings = new ArloSettings();
			//mySettings.Initialize("ArloSettings.properties");
			mySettings.Initialize( configFileName );



			openConnection();

			taskNameToOutputConnector = new ConcurrentHashMap<String, WebServiceTask>();

			System.out.println("taskNameToOutputConnector = " + taskNameToOutputConnector);

			taskNameToOutputConnector.put("analyzeAudioFile", WebServiceTask.ANALYZE_AUDIO_FILE);
			taskNameToOutputConnector.put("createSpectra", WebServiceTask.CREATE_SPECTRA);
			taskNameToOutputConnector.put("createWavSegment", WebServiceTask.CREATE_WAV_SEGMENT);

			taskNameToOutputConnector.put("startADAPTAnalysis", WebServiceTask.START_ADAPT_ANALYSIS);
			taskNameToOutputConnector.put("pauseADAPTAnalysis", WebServiceTask.PAUSE_ADAPT_ANALYSIS);
			taskNameToOutputConnector.put("resumeADAPTAnalysis", WebServiceTask.RESUME_ADAPT_ANALYSIS);
			taskNameToOutputConnector.put("stopADAPTAnalysis", WebServiceTask.STOP_ADAPT_ANALYSIS);

			taskNameToOutputConnector.put("startUnsupervisedTagDiscovery", WebServiceTask.START_UNSUPERVISED_TAG_DISCOVERY);
			taskNameToOutputConnector.put("pauseUnsupervisedTagDiscovery", WebServiceTask.PAUSE_UNSUPERVISED_TAG_DISCOVERY);
			taskNameToOutputConnector.put("resumeUnsupervisedTagDiscovery", WebServiceTask.RESUME_UNSUPERVISED_TAG_DISCOVERY);
			taskNameToOutputConnector.put("stopUnsupervisedTagDiscovery", WebServiceTask.STOP_UNSUPERVISED_TAG_DISCOVERY);

			taskNameToOutputConnector.put("startSupervisedTagDiscovery", WebServiceTask.START_SUPERVISED_TAG_DISCOVERY);
			taskNameToOutputConnector.put("pauseSupervisedTagDiscovery", WebServiceTask.PAUSE_SUPERVISED_TAG_DISCOVERY);
			taskNameToOutputConnector.put("resumeSupervisedTagDiscovery", WebServiceTask.RESUME_SUPERVISED_TAG_DISCOVERY);
			taskNameToOutputConnector.put("stopSupervisedTagDiscovery", WebServiceTask.STOP_SUPERVISED_TAG_DISCOVERY);

			taskNameToOutputConnector.put("startTranscript", WebServiceTask.START_TRANSCRIPT);
			taskNameToOutputConnector.put("pauseTranscript", WebServiceTask.PAUSE_TRANSCRIPT);
			taskNameToOutputConnector.put("resumeTranscript", WebServiceTask.RESUME_TRANSCRIPT);
			taskNameToOutputConnector.put("stopTranscript", WebServiceTask.STOP_TRANSCRIPT);

			taskNameToOutputConnector.put("startTagAnalysis", WebServiceTask.START_TAG_ANALYSIS);
			taskNameToOutputConnector.put("pauseTagAnalysis", WebServiceTask.PAUSE_TAG_ANALYSIS);
			taskNameToOutputConnector.put("resumeTagAnalysis", WebServiceTask.RESUME_TAG_ANALYSIS);
			taskNameToOutputConnector.put("stopTagAnalysis", WebServiceTask.STOP_TAG_ANALYSIS);

			taskNameToOutputConnector.put("getAudioSpectraData", WebServiceTask.GET_AUDIO_SPECTRA_DATA);
			taskNameToOutputConnector.put("getAudioPitchTraceData", WebServiceTask.GET_AUDIO_PITCHTRACE_DATA);

			taskNameToOutputConnector.put("meandreTestCall", WebServiceTask.TEST_CALL);

			initialized = true;


			////////
			// setup HttpCallMeAPI server
			HttpCallMeAPIServer httpCallMeAPIServer = null;

			if (ArloSettings.startHttpCallMeAPIServer) {
				httpCallMeAPIServer = new HttpCallMeAPIServer();
				
				if ( ! httpCallMeAPIServer.Initialize(this, ArloSettings.HttpCallMeAPIServerPort)) {
					// handle error

					httpCallMeAPIServer = null;
				}
			}

			////////
			// QueueRunner
			if (ArloSettings.RunQueueDaemonInterval_Seconds > 0) {
				// Vector<NesterUser> restrictedUsers = new Vector<NesterUser>();
				Vector<NesterUser> restrictedUsers = null;

				// register known tasks
				Vector<QueueTask> taskHandlers = new Vector<QueueTask>();
				if (ArloSettings.RunQueueEnableImportAudioFile) 
					taskHandlers.add( (QueueTask) new QueueTask_ImportAudioFile());
				if (ArloSettings.RunQueueEnableTest) 
					taskHandlers.add( (QueueTask) new QueueTask_Test());
				if (ArloSettings.RunQueueEnableSupervisedTagDiscoveryParent)
					taskHandlers.add( (QueueTask) new QueueTask_SupervisedTagDiscoveryParent());
				if (ArloSettings.RunQueueEnableSupervisedTagDiscoveryChild)
					taskHandlers.add( (QueueTask) new QueueTask_SupervisedTagDiscoveryChild());
				if (ArloSettings.RunQueueEnableUnsupervisedTagDiscovery)
					taskHandlers.add( (QueueTask) new QueueTask_UnsupervisedTagDiscovery());
				if (ArloSettings.RunQueueEnableExpertAgreementClassification)
					taskHandlers.add( (QueueTask) new QueueTask_ExpertAgreementClassification());
				if (ArloSettings.RunQueueEnableWekaJob)
					taskHandlers.add( (QueueTask) new QueueTask_WekaJobWrapper());
				if (ArloSettings.RunQueueEnableLibraryMediaFileValidation)
					taskHandlers.add( (QueueTask) new QueueTask_LibraryMediaFileValidation());

				queueRunner = new QueueRunner();
				if (ArloSettings.QueueRunnerOneShot) {
					logger.log(Level.INFO, "Starting OneShot");
					int numTasks;
					numTasks = QueueRunner.runQueue(taskHandlers, restrictedUsers, ArloSettings.QueueRunnerOneShotMaxTasks);
					logger.log(Level.INFO, "Exiting Adapt - Completed OneShot - Ran " + numTasks + " Tasks");
					System.exit(0);
				} else {
					queueRunner.startDaemon(
						ArloSettings.RunQueueDaemonInterval_Seconds, 
						taskHandlers, 
						restrictedUsers);
				}

			}

		}
	}

	public void dispose() {
		closeConnection();
	}

	static int count = 0;

	public PyDictionary callMe(PyDictionary request) {
		// wrapper around callMe when using PyDictionary (Jython)

		// convert PyDictionary to HashMap
		HashMap<String,String> hmRequest = new HashMap<String,String>();
		for (String k : (Set<String>) request.keySet()) {
			hmRequest.put(k, (String) request.get(k));
		}

		// pass on call
		HashMap<String, Object> hmResponse;
		hmResponse = callMe(hmRequest);

		// convert response back to PyDictionary
		PyDictionary pyResponse = new PyDictionary();
		for (String k : hmResponse.keySet()) {
			pyResponse.put(k, hmResponse.get(k));
		}   

		return pyResponse;
	}

	public HashMap<String, Object> callMe(HashMap<String, String> request) {

		HashMap<String, Object> response = null;
		response = new HashMap<String, Object>();

		StringBuffer writer = new StringBuffer();

		try {

			// String infoMessage = this.getClass().getSimpleName() + ":  handle v2;  executionInstanceId = " + executionInstanceId;
			// logger.log(Level.INFO, infoMessage);
			// System.out.println(infoMessage);

			// String userName = (String) request.get("userName");
			// String projectName = (String) request.get("projectName");
			String taskName = (String) request.get("taskName");
			String action = (String) request.get("action");
			logger.log(Level.INFO, "callMe - Action: " + action);


			// ////////////////////
			// validate request //
			// ////////////////////

			boolean validRequest = true;


			// validate taskName //

			System.out.println("2taskNameToOutputConnector = " + taskNameToOutputConnector);
			System.out.println("taskName = " + taskName);
			System.out.println("action = " + action);

			WebServiceTask webServriceTask = taskNameToOutputConnector.get(action);
			if (webServriceTask == null) {
				logger.log(Level.SEVERE, "webServriceTask == null  action: '" + action + "'");
				writer.append("<br>Error!  Invalid taskName = " + action);
				validRequest = false;
			}

			if (validRequest) {

				writer.append("<br>Service Head Started:" + "\n");
				writer.append("<br>&nbsp;&nbsp;taskName    = " + action + "\n");
				writer.append("<br>&nbsp;&nbsp;taskID      = " + webServriceTask + "\n");
			}

			switch (webServriceTask) {

			case ANALYZE_AUDIO_FILE: {

				// NesterProject project = getProject(userNameID, projectName);

				String audioFileIDString = (String) request.get("audioFileID");
				logger.log(Level.INFO, "analyzing Audio File: " + audioFileIDString);

				int audioFileID = Integer.parseInt(audioFileIDString);

				logger.log(Level.INFO, "Starting analyzeAudioFile - " + audioFileID);

				analyzeAudioFile(audioFileID);

				break;
			}

			case CREATE_SPECTRA: {

				// NesterProject project = getProject(userNameID, projectName);
				logger.log(Level.INFO, "Start CREATE_SPECTRA");

				/*                                                       */
				/*   Get Settings From the Parameters in the API Call    */
				/*                                                       */

				// ****
				// NOTE This changed in April 2013 - we are now no longer passing in the full file path, 
				// rather the file path relative to the Media Root

				// The file paths, relative to MEDIA_ROOT
				String audioFilePath = (String) request.get("audioFilePath");
				String imageFilePath = (String) request.get("imageFilePath");
				String audioSegmentFilePath = (String) request.get("audioSegmentFilePath");

				// convert to absolute
				audioFilePath = ArloSettings.mediaRootDirectoryPath + "/" + audioFilePath;
				imageFilePath = ArloSettings.mediaRootDirectoryPath + "/" + imageFilePath;
				audioSegmentFilePath = ArloSettings.mediaRootDirectoryPath + "/" + audioSegmentFilePath;


				int userSettingsID = Integer.valueOf((String) request.get("userSettingsID"));
				double startTime = Double.valueOf((String) request.get("startTime"));
				double endTime = Double.valueOf((String) request.get("endTime"));
				double gain = Double.valueOf((String) request.get("gain"));
				double spectraNumFramesPerSecond = Double.valueOf((String) request.get("spectraNumFramesPerSecond"));
				int spectraNumFrequencyBands = Integer.valueOf((String) request.get("spectraNumFrequencyBands"));
				double spectraDampingFactor = Double.valueOf((String) request.get("spectraDampingFactor"));
				double spectraMinimumBandFrequency = Double.valueOf((String) request.get("spectraMinimumBandFrequency"));
				double spectraMaximumBandFrequency = Double.valueOf((String) request.get("spectraMaximumBandFrequency"));
				@SuppressWarnings("unused")
				String spectraNormalizationMethod = (String) request.get("spectraNormalizationMethod");


				boolean showCatalog = false;
				boolean showTimeScale = true;
				boolean showFrequencyScale = true;
				
				
				logger.log(Level.INFO, "Start CREATE_SPECTRA  1");
				if ((String) request.get("showCatalog") != null)
					showCatalog = Boolean.valueOf((String) request.get("showCatalog"));
				if ((String) request.get("showTimeScale") != null)
					showTimeScale = Boolean.valueOf((String) request.get("showTimeScale"));
				if ((String) request.get("showFrequencyScale") != null)
					showFrequencyScale = Boolean.valueOf((String) request.get("showFrequencyScale"));
				logger.log(Level.INFO, "Start CREATE_SPECTRA 2");

				int spectraBorderWidth = Integer.valueOf((String) request.get("spectraBorderWidth"));
				int spectraTopBorderHeight = Integer.valueOf((String) request.get("spectraTopBorderHeight"));
				int timeScaleHeight = Integer.valueOf((String) request.get("timeScaleHeight"));
				int catalogSpectraNumFrequencyBands = Integer.valueOf((String) request.get("catalogSpectraNumFrequencyBands"));

				/* ************************ */
				/*   Pitch Trace Settings   */
				/* ************************ */

				CreateSpectraImage.SpectraPitchTraceType pitchTraceType = CreateSpectraImage.SpectraPitchTraceType.NONE; 

				String spectraPitchTraceTypeString = (String) request.get("spectraPitchTraceType");

				if (spectraPitchTraceTypeString.equals("None")) {
					pitchTraceType = CreateSpectraImage.SpectraPitchTraceType.NONE;
				} else if (spectraPitchTraceTypeString.equals("Fundamental")) {
					pitchTraceType = CreateSpectraImage.SpectraPitchTraceType.FUNDAMENTAL;
				} else if (spectraPitchTraceTypeString.equals("MaxEnergy")) {
					pitchTraceType = CreateSpectraImage.SpectraPitchTraceType.MAX_ENERGY;
				} else {
					logger.log(Level.SEVERE, "CREATE_SPECTRA - Invalid spectraPitchTraceType");
					writer.append("<br> CREATE_SPECTRA - Invalid spectraPitchTraceType" + "\n"); // writer = response text (defined at beginning of function)
				}

				CreateSpectraImage.SpectraPitchTraceColor pitchTraceColor = CreateSpectraImage.SpectraPitchTraceColor.BLACK;
				String pitchTraceColorString = request.get("spectraPitchTraceColor");
				if (pitchTraceColorString.equals("White")) {
					pitchTraceColor = CreateSpectraImage.SpectraPitchTraceColor.WHITE;
				}
				int pitchTraceNumSamplePoints = Integer.valueOf((String) request.get("spectraPitchTraceNumSamplePoints"));
				int pitchTraceWidth = Integer.valueOf((String) request.get("spectraPitchTraceWidth"));
				double pitchTraceMinCorrelation = Double.valueOf((String) request.get("spectraPitchTraceMinCorrelation"));
				double pitchTraceEntropyThreshold = Double.valueOf((String) request.get("spectraPitchTraceEntropyThreshold"));
				double pitchTraceStartFreq = Double.valueOf((String) request.get("spectraPitchTraceStartFreq"));
				double pitchTraceEndFreq = Double.valueOf((String) request.get("spectraPitchTraceEndFreq"));
				double spectraPitchTraceMinEnergyThreshold = Double.valueOf((String) request.get("spectraPitchTraceMinEnergyThreshold"));
				double spectraPitchTraceTolerance = Double.valueOf((String) request.get("spectraPitchTraceTolerance"));
				double spectraPitchTraceInverseFreqWeight = Double.valueOf((String) request.get("spectraPitchTraceInverseFreqWeight"));
				double spectraPitchTraceMaxPathLengthPerTransition = Double.valueOf((String) request.get("spectraPitchTraceMaxPathLengthPerTransition"));
				int spectraPitchTraceWindowSize = Integer.valueOf((String) request.get("spectraPitchTraceWindowSize"));
				double spectraPitchTraceExtendRangeFactor = Double.valueOf((String) request.get("spectraPitchTraceExtendRangeFactor"));


				/****************************************************/
				/* test to see if spectra has already been computed */
				/****************************************************/

				if (ArloSettings.cacheEnabled) {

					File imageFile = new File(imageFilePath);

					if (imageFile.exists()) {
						System.out.println("Found image file in cache " + imageFilePath);
						// return response;
						break;
					}
				}

				// ///////////////////////////
				// If here, compute Spectra

				logger.log(Level.INFO, "Start CREATE_SPECTRA 3");

				double windowSizeInSeconds = endTime - startTime;
				int windowNumTimeFrames = (int) (spectraNumFramesPerSecond * windowSizeInSeconds);

				
				if (startTime >= endTime || windowNumTimeFrames <= 0) {
					return null;
				}
				

				
				
				boolean simpleNormalization = true;

				CreateSpectraImage createSpectraImage = new CreateSpectraImage(logger, showCatalog, showTimeScale, showFrequencyScale, spectraBorderWidth, spectraTopBorderHeight,
						timeScaleHeight, catalogSpectraNumFrequencyBands, simpleNormalization, gain);

				writer.append("<br> executing createSpectra" + "\n"); // writer = response text (defined at beginning of function)

				Object[] intDataObject = new Object[1];

				// Open Audio File
				AudioFile audioFile = new AudioFile(audioFilePath);
				logger.log(Level.INFO, "Start CREATE_SPECTRA  5");
				if (audioFile == null) {
					writer.append("<br>&nbsp;&nbsp;CREATE_SPECTRA failed:  could not create/parse audio file" + "\n");
					logger.log(Level.INFO, "CREATE_SPECTRA failed:  could not create/parse audio file; audioFilePath = " + audioFilePath);
					//return response;
					break;
				}

				// compute Spectra
				NesterUserSettings nesterUserSettings = getNesterUserSettings(userSettingsID);
logger.log(Level.INFO, "Test1");
				createSpectraImage.createSpectralImageJPEG(nesterUserSettings.showSpectra, nesterUserSettings.showWaveform, pitchTraceType, startTime, endTime, audioFile, windowNumTimeFrames,
						spectraNumFrequencyBands, spectraNumFramesPerSecond, spectraDampingFactor, spectraMinimumBandFrequency, spectraMaximumBandFrequency,
						/* project.usePrecomputedSpectra */false, intDataObject, imageFilePath, 
						pitchTraceColor, pitchTraceNumSamplePoints, pitchTraceWidth, pitchTraceMinCorrelation, pitchTraceEntropyThreshold, pitchTraceStartFreq, pitchTraceEndFreq, 
						spectraPitchTraceMinEnergyThreshold, spectraPitchTraceTolerance, spectraPitchTraceInverseFreqWeight, spectraPitchTraceMaxPathLengthPerTransition, spectraPitchTraceWindowSize, spectraPitchTraceExtendRangeFactor);
logger.log(Level.INFO, "Test2");
				int[] intData = (int[]) intDataObject[0];
logger.log(Level.INFO, "Test3");
				WavFileCreator.createWavFile(audioFile, new File(audioSegmentFilePath), intData);
logger.log(Level.INFO, "Test4");
			}

				break;

			case CREATE_WAV_SEGMENT: {

				// NesterProject project = getProject(userNameID, projectName);

				String audioFilePath = (String) request.get("audioFilePath");

				// logger.log(Level.INFO, "audioFilePath = " + audioFilePath);

				// File file = new File(audioFilePath);
				// logger.log(Level.INFO, "file = " + file);
				// logger.log(Level.INFO, "file.exists() = " + file.exists());

				String audioSegmentFilePath = (String) request.get("audioSegmentFilePath");
				double startTime = Double.valueOf((String) request.get("startTime"));
				double endTime = Double.valueOf((String) request.get("endTime"));

				WavFileCreator.createWavFile(new File(audioFilePath), new File(audioSegmentFilePath), startTime, endTime, true);

			}
				break;

			case START_ADAPT_ANALYSIS:

			{
				int adaptAnalysisBiasID = Integer.parseInt((String) request.get("id"));

				// Search for ADAPTAnalysisDisabled
				logger.log(Level.SEVERE, "ERROR: ADAPTAnalysis Disabled");

				// startADAPTAnalysisJob(adaptAnalysisBiasID);

			}

				break;

			case PAUSE_ADAPT_ANALYSIS:

			{
				int adaptAnalysisBiasID = Integer.parseInt((String) request.get("id"));

				// Search for ADAPTAnalysisDisabled
				logger.log(Level.SEVERE, "ERROR: ADAPTAnalysis Disabled");

				// pauseADAPTAnalysisJob(adaptAnalysisBiasID);

			}

				break;

			case RESUME_ADAPT_ANALYSIS:

			{
				int adaptAnalysisBiasID = Integer.parseInt((String) request.get("id"));

				// Search for ADAPTAnalysisDisabled
				logger.log(Level.SEVERE, "ERROR: ADAPTAnalysis Disabled");

				// resumeADAPTAnalysisJob(adaptAnalysisBiasID);

			}

				break;

			case STOP_ADAPT_ANALYSIS:

			{
				int adaptAnalysisBiasID = Integer.parseInt((String) request.get("id"));

				// Search for ADAPTAnalysisDisabled
				logger.log(Level.SEVERE, "ERROR: ADAPTAnalysis Disabled");

				// stopADAPTAnalysisJob(adaptAnalysisBiasID);

			}

			case START_UNSUPERVISED_TAG_DISCOVERY:

			{
				int unsupervisedTagDiscoveryBiasID = Integer.parseInt((String) request.get("id"));

				startUnsupervisedTagDiscoveryJob(unsupervisedTagDiscoveryBiasID);

			}

				break;

			case PAUSE_UNSUPERVISED_TAG_DISCOVERY:

			{
				int unsupervisedTagDiscoveryBiasID = Integer.parseInt((String) request.get("id"));

				pauseUnsupervisedTagDiscoveryJob(unsupervisedTagDiscoveryBiasID);

			}

				break;

			case RESUME_UNSUPERVISED_TAG_DISCOVERY:

			{
				int unsupervisedTagDiscoveryBiasID = Integer.parseInt((String) request.get("id"));

				resumeUnsupervisedTagDiscoveryJob(unsupervisedTagDiscoveryBiasID);

			}

				break;

			case STOP_UNSUPERVISED_TAG_DISCOVERY:

			{
				int unsupervisedTagDiscoveryBiasID = Integer.parseInt((String) request.get("id"));

				stopUnsupervisedTagDiscoveryJob(unsupervisedTagDiscoveryBiasID);

			}

				break;

			case START_SUPERVISED_TAG_DISCOVERY:

			{
				int supervisedTagDiscoveryBiasID = Integer.parseInt((String) request.get("id"));

				startSupervisedTagDiscoveryJob(supervisedTagDiscoveryBiasID);

			}

				break;

			case PAUSE_SUPERVISED_TAG_DISCOVERY:

			{
				int supervisedTagDiscoveryBiasID = Integer.parseInt((String) request.get("id"));

				pauseSupervisedTagDiscoveryJob(supervisedTagDiscoveryBiasID);

			}

				break;

			case RESUME_SUPERVISED_TAG_DISCOVERY:

			{
				int supervisedTagDiscoveryBiasID = Integer.parseInt((String) request.get("id"));

				resumeSupervisedTagDiscoveryJob(supervisedTagDiscoveryBiasID);

			}

				break;

			case STOP_SUPERVISED_TAG_DISCOVERY:

			{
				int supervisedTagDiscoveryBiasID = Integer.parseInt((String) request.get("id"));

				stopSupervisedTagDiscoveryJob(supervisedTagDiscoveryBiasID);

			}

				break;

			case START_TAG_ANALYSIS:

			{
				int supervisedTagDiscoveryJobID = Integer.parseInt((String) request.get("id"));

				startTagAnalysisJob(supervisedTagDiscoveryJobID);

			}

				break;

			case PAUSE_TAG_ANALYSIS:

			{
				int tagAnalysisBiasID = Integer.parseInt((String) request.get("id"));

				pauseTagAnalysisJob(tagAnalysisBiasID);

			}

				break;

			case RESUME_TAG_ANALYSIS:

			{
				int tagAnalysisBiasID = Integer.parseInt((String) request.get("id"));

				resumeTagAnalysisJob(tagAnalysisBiasID);

			}

				break;

			case STOP_TAG_ANALYSIS:

			{
				int tagAnalysisBiasID = Integer.parseInt((String) request.get("id"));

				stopTagAnalysisJob(tagAnalysisBiasID);

			}

				break;

			case GET_AUDIO_SPECTRA_DATA:

			{
				logger.log(Level.INFO, "Start GET_AUDIO_SPECTRA_DATA");

				///
				// Request Parameters

				int mediaFileId = Integer.valueOf((String)request.get("mediaFileId"));
				double startTime = Double.valueOf((String) request.get("startTime"));
				double endTime = Double.valueOf((String) request.get("endTime"));
				int numTimeFramesPerSecond = Integer.valueOf((String) request.get("numTimeFramesPerSecond"));
				int numFrequencyBands = Integer.valueOf((String) request.get("numFrequencyBands"));
				double dampingRatio = Double.valueOf((String) request.get("dampingRatio"));
				double minFrequency = Double.valueOf((String) request.get("minFrequency"));
				double maxFrequency = Double.valueOf((String) request.get("maxFrequency"));


				// Get the MediaFile object
				NesterAudioFile nesterAudioFile = NesterAudioFile.getAudioFile(mediaFileId, connection, null);

				// Validate Params
				if (startTime < 0) {
					startTime = 0;
					logger.log(Level.SEVERE, "GET_AUDIO_SPECTRA_DATA: startTime < 0 - overriding");
				}

				if (endTime > nesterAudioFile.wavFileMetaData.durationInSeconds) {
					endTime = nesterAudioFile.wavFileMetaData.durationInSeconds;
					logger.log(Level.INFO, "GET_AUDIO_SPECTRA_DATA: endTime > file length - overriding");
				}
		
				///		
				// Get the Spectra Data
				int[][] spectraData = CreateSpectraImage.getNesterAudioFileSpectraData(nesterAudioFile, startTime, endTime, numTimeFramesPerSecond, numFrequencyBands, dampingRatio, minFrequency, maxFrequency);

				///
				// Get the corresponding frequency bands

				// Build an equivalent BandPass filter
				BandPass bandPass = new BandPass();

				bandPass.setDampingRatio(dampingRatio);
				bandPass.setMinBandFrequency(minFrequency);
				bandPass.setMaxBandFrequency(maxFrequency);
				bandPass.setNumBands(numFrequencyBands);
				bandPass.setRawSamplingRate(nesterAudioFile.wavFileMetaData.sampleRate);
				
				bandPass.initialize();


				///
				// Build Response Dictionary

				// Since we're going to JSON, we get a tad crazy with Vectors / Maps here

				// 'spectraData': { 
				//     'params': {
				//       'mediaFileId': mediaFileId,
				//       'startTime': startTime,
				//       'endTime': endTime,
				//       'numTimeFramesPerSecond': numTimeFramesPerSecond,
				//       'numFrequencyBands': numFrequencyBands,
				//       'dampingRatio': dampingRatio,
				//       'minFrequency': minFrequency,
				//       'maxFrequency': maxFrequency
				//     },
				//     'freqBands': [
				//                    freq0,
				//                    freq1,
				//                    ...
				//                  ],
				//     'frames': {
				//                 time0: [
				//                          sample0,
				//                          sample1,
				//                          ...
				//                        ],
				//                 time1: [
				//                          sample0,
				//                          sample1,
				//                          ...
				//                        ],
				//                 ...
				//               }
				// }

				// Parameters

				HashMap<String, Object> paramsDict = new HashMap<String, Object>();
				paramsDict.put("mediaFileId", mediaFileId);
				paramsDict.put("startTime", startTime);
				paramsDict.put("endTime", endTime);
				paramsDict.put("numTimeFramesPerSecond", numTimeFramesPerSecond);
				paramsDict.put("numFrequencyBands", numFrequencyBands);
				paramsDict.put("dampingRatio", dampingRatio);
				paramsDict.put("minFrequency", minFrequency);
				paramsDict.put("maxFrequency", maxFrequency);

				// freqBands
				Vector<Double> freqBands = new Vector<Double>();
				for (int frequencyIndex = 0; frequencyIndex < bandPass.bandFrequencies.length; frequencyIndex++) {
					freqBands.add(bandPass.bandFrequencies[frequencyIndex]);
				}


				// Frames
				HashMap<Double, Vector<Integer>> frames = new HashMap<Double, Vector<Integer>>();
				for (int sampleIndex = 0; sampleIndex < spectraData.length; sampleIndex++) {
					Vector<Integer> frameSamples = new Vector<Integer>();
					for (int frequencyIndex = 0; frequencyIndex < bandPass.bandFrequencies.length; frequencyIndex++) {
						frameSamples.add(spectraData[sampleIndex][frequencyIndex]);
					}
					double frameTime = startTime + ((double) sampleIndex / numTimeFramesPerSecond);
					frames.put(frameTime, frameSamples);
				}
					

				// spectraData
				HashMap<String, Object> responseSpectraData = new HashMap<String, Object>();
				responseSpectraData.put("freqBands", freqBands);
				responseSpectraData.put("frames", frames);
				responseSpectraData.put("params", paramsDict);
				response.put("spectraData", responseSpectraData);


			}

				break;

			case GET_AUDIO_PITCHTRACE_DATA:
			{

				logger.log(Level.INFO, "Start GET_AUDIO_PITCHTRACE_DATA");

				///
				// Request Parameters

				// PitchTraceType
				PitchTrace.PitchTraceType pitchTraceType = null;
				String sPitchTraceType = (String)request.get("pitchTraceType");
				if (sPitchTraceType.equals("MAX_ENERGY")) {
					pitchTraceType = PitchTrace.PitchTraceType.MAX_ENERGY;
				} else if (sPitchTraceType.equals("FUNDAMENTAL")) {
					pitchTraceType = PitchTrace.PitchTraceType.FUNDAMENTAL;
				} else {
					logger.log(Level.SEVERE, "GET_AUDIO_PITCHTRACE_DATA: Unknown PitchTraceType: " + sPitchTraceType);
					response.put("ErrorMsg:", "GET_AUDIO_PITCHTRACE_DATA: Unknown PitchTraceType: " + sPitchTraceType);
					return response;
				}

				int mediaFileId = Integer.valueOf((String)request.get("mediaFileId"));
				double startTime = Double.valueOf((String) request.get("startTime"));
				double endTime = Double.valueOf((String) request.get("endTime"));
				int numTimeFramesPerSecond = Integer.valueOf((String) request.get("numTimeFramesPerSecond"));
				int numFrequencyBands = Integer.valueOf((String) request.get("numFrequencyBands"));
				double dampingRatio = Double.valueOf((String) request.get("dampingRatio"));
				double spectraMinFrequency = Double.valueOf((String) request.get("spectraMinFrequency"));
				double spectraMaxFrequency = Double.valueOf((String) request.get("spectraMaxFrequency"));

				boolean simpleNormalization = true;

				// FUNDAMENTAL Params

				int pitchTraceNumSamplePoints = -1;
				double pitchTraceMinCorrelation = -1;
				double pitchTraceEntropyThreshold = -1;
				double pitchTraceStartFreq = -1;
				double pitchTraceEndFreq = -1;
				double spectraPitchTraceMinEnergyThreshold = -1;
				double spectraPitchTraceTolerance = -1;
				double spectraPitchTraceInverseFreqWeight = -1;
				double spectraPitchTraceMaxPathLengthPerTransition = -1;
				int spectraPitchTraceWindowSize = -1;
				double spectraPitchTraceExtendRangeFactor = -1;


				if (pitchTraceType == PitchTrace.PitchTraceType.FUNDAMENTAL) {
					pitchTraceNumSamplePoints = Integer.valueOf((String)request.get("numSamplePoints"));
					pitchTraceMinCorrelation = Double.valueOf((String) request.get("minCorrelation"));
					pitchTraceEntropyThreshold = Double.valueOf((String) request.get("entropyThreshold"));
					pitchTraceStartFreq = Double.valueOf((String) request.get("pitchTraceStartFreq"));
					pitchTraceEndFreq = Double.valueOf((String) request.get("pitchTraceEndFreq"));
					spectraPitchTraceMinEnergyThreshold = Double.valueOf((String) request.get("minEnergyThreshold"));
					spectraPitchTraceTolerance = Double.valueOf((String) request.get("tolerance"));
					spectraPitchTraceInverseFreqWeight = Double.valueOf((String) request.get("inverseFreqWeight"));
					spectraPitchTraceMaxPathLengthPerTransition = Double.valueOf((String) request.get("maxPathLengthPerTransition"));
					spectraPitchTraceWindowSize = Integer.valueOf((String)request.get("windowSize"));
					spectraPitchTraceExtendRangeFactor = Double.valueOf((String) request.get("extendRangeFactor"));
				}

				// Get the MediaFile object
				NesterAudioFile nesterAudioFile = NesterAudioFile.getAudioFile(mediaFileId, connection, null);
				AudioFile audioFile = new AudioFile(nesterAudioFile);

				// Validate Params
				if (startTime < 0) {
					startTime = 0;
					logger.log(Level.SEVERE, "GET_AUDIO_PITCHTRACE_DATA: startTime < 0 - overriding");
				}

				if (endTime > nesterAudioFile.wavFileMetaData.durationInSeconds) {
					endTime = nesterAudioFile.wavFileMetaData.durationInSeconds;
					logger.log(Level.INFO, "GET_AUDIO_PITCHTRACE_DATA: endTime > file length - overriding");
				}
		
				// After Validation
				int windowNumTimeFrames = (int) ((endTime - startTime) * numTimeFramesPerSecond);



				///		
				// Get the Spectra Data
				double[][] pitchTraceData = PitchTrace.getPitchTraceData(
					pitchTraceType,
					audioFile,
					startTime,
					windowNumTimeFrames,

					numFrequencyBands,
					numTimeFramesPerSecond,
					dampingRatio,
					// FUNDAMENTAL Params
					spectraMinFrequency,
					spectraMaxFrequency,
					pitchTraceNumSamplePoints,
					pitchTraceMinCorrelation,
					pitchTraceEntropyThreshold,
					pitchTraceStartFreq,
					pitchTraceEndFreq,
					spectraPitchTraceMinEnergyThreshold,
					spectraPitchTraceTolerance,
					spectraPitchTraceInverseFreqWeight,
					spectraPitchTraceMaxPathLengthPerTransition,
					spectraPitchTraceWindowSize,
					spectraPitchTraceExtendRangeFactor,
					simpleNormalization
				);




				///
				// Build Response Dictionary

				// Since we're going to JSON, we get a tad crazy with Vectors / Maps here

				// 'pitchTraceData': { 
				//     'params': {
				//       'mediaFileId': mediaFileId,
				//       'startTime': startTime,
				//       'endTime': endTime,
				//       'numTimeFramesPerSecond': numTimeFramesPerSecond,
				//       'numFrequencyBands': numFrequencyBands,
				//       'dampingRatio': dampingRatio,
				//       'minFrequency': minFrequency,
				//       'maxFrequency': maxFrequency,
				//       'pitchTraceType': "FUNDAMENTAL",
				//     },
				//     'freqBands': [
				//                    freq0,
				//                    freq1,
				//                    ...
				//                  ],
				//     'frames': {
				//                 time0: {
				//                          'frequency': peak-frequency,
				//                          'energy': energy-of-peak-band,
				//                          ...
				//                        },
				//                 time1: {
				//                          'frequency': peak-frequency,
				//                          'energy': energy-of-peak-band,
				//                          ...
				//                        },
				//                 ...
				//               }
				// }

				// Parameters

				HashMap<String, Object> paramsDict = new HashMap<String, Object>();
				paramsDict.put("mediaFileId", mediaFileId);
				paramsDict.put("startTime", startTime);
				paramsDict.put("endTime", endTime);
				paramsDict.put("numTimeFramesPerSecond", numTimeFramesPerSecond);
				paramsDict.put("numFrequencyBands", numFrequencyBands);
				paramsDict.put("dampingRatio", dampingRatio);
				paramsDict.put("spectraMinFrequency", spectraMinFrequency);
				paramsDict.put("spectraMaxFrequency", spectraMaxFrequency);
				switch (pitchTraceType) {
					case MAX_ENERGY:
					{
						paramsDict.put("pitchTraceType", "MAX_ENERGY");
					}
						break;
					case FUNDAMENTAL:
					{
						paramsDict.put("pitchTraceType", "FUNDAMENTAL");
						// FUNDAMENTAL Params
						paramsDict.put("numSamplePoints", pitchTraceNumSamplePoints);
						paramsDict.put("minCorrelation", pitchTraceMinCorrelation);
						paramsDict.put("entropyThreshold", pitchTraceEntropyThreshold);
						paramsDict.put("pitchTraceStartFreq", pitchTraceStartFreq);
						paramsDict.put("pitchTraceEndFreq", pitchTraceEndFreq);
						paramsDict.put("minEnergyThreshold", spectraPitchTraceMinEnergyThreshold);
						paramsDict.put("tolerance", spectraPitchTraceTolerance);
						paramsDict.put("inverseFreqWeight", spectraPitchTraceInverseFreqWeight);
						paramsDict.put("maxPathLengthPerTransition", spectraPitchTraceMaxPathLengthPerTransition);
						paramsDict.put("windowSize", spectraPitchTraceWindowSize);
						paramsDict.put("extendRangeFactor", spectraPitchTraceExtendRangeFactor);
					}
						break;
					default:
						logger.log(Level.SEVERE, "Unable to translate PitchTraceType");
						return null;
				}


				///
				// freqBands

				// Build an equivalent BandPass filter to get the corresponding frequency bands
				BandPass bandPass = new BandPass();

				bandPass.setDampingRatio(dampingRatio);
				bandPass.setMinBandFrequency(spectraMinFrequency);
				bandPass.setMaxBandFrequency(spectraMaxFrequency);
				bandPass.setNumBands(numFrequencyBands);
				bandPass.setRawSamplingRate(nesterAudioFile.wavFileMetaData.sampleRate);
				
				bandPass.initialize();

				Vector<Double> freqBands = new Vector<Double>();
				for (int frequencyIndex = 0; frequencyIndex < bandPass.bandFrequencies.length; frequencyIndex++) {
					freqBands.add(bandPass.bandFrequencies[frequencyIndex]);
				}

				// Frames
				HashMap<Double, HashMap<String, Double>> frames = new HashMap<Double, HashMap<String, Double>>();
				for (int sampleIndex = 0; sampleIndex < pitchTraceData.length; sampleIndex++) {
					HashMap<String, Double> frameData = new HashMap<String, Double>();
					frameData.put("frequency", pitchTraceData[sampleIndex][0]);
					frameData.put("energy", pitchTraceData[sampleIndex][1]);

					double frameTime = startTime + ((double) sampleIndex / numTimeFramesPerSecond);
					frames.put(frameTime, frameData);
				}


				// spectraData
				HashMap<String, Object> responseData = new HashMap<String, Object>();
				responseData.put("spectraFrequencyBands", freqBands);
				responseData.put("frames", frames);
				responseData.put("params", paramsDict);
				response.put("pitchTraceData", responseData);


			}

				break;
			case TEST_CALL:

			{
				logger.log(Level.INFO, "Meandre Test Call");
				writer.append("Meandre Test Call");
				response.put("Test Data", "This is Test Data");

			}
				break;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block

			StringWriter excWriter = new StringWriter();
			PrintWriter printWriter = new PrintWriter( excWriter );
			e.printStackTrace( printWriter );
			printWriter.flush();
			String stackTrace = excWriter.toString();
			logger.log(Level.SEVERE, "ServiceHead::callMe() Failed Exception" + e + " -- " + stackTrace);

			return null;
		}

		response.put("text", writer.toString());
		return response;

	}


	public void openConnection() {

		String url = ArloSettings.dbUrl;
		String dbUserName = ArloSettings.dbUserName;
		String dbPassword = ArloSettings.dbPassword;

		try {
			// Object object = Class.forName(ArloSettings.driveClassString).newInstance();

			connection = DriverManager.getConnection(url, dbUserName, dbPassword);

			logger.log(Level.INFO, "Database connection established - " + dbUserName + "@" + url);

			boolean useHeartbeat = true;

			if (useHeartbeat) {
				Heartbeat heartbeat = new Heartbeat(connection);
				heartbeat.start();

				logger.log(Level.INFO, "Database heartbeat established");
			}

			logger.log(Level.INFO, "connection = " + connection);

		} catch (Exception e) {
			logger.log(Level.INFO, "Exception = " + e);
			logger.log(Level.INFO, "Error!  Database connection failed.");
		}

	}

	public boolean closeConnection() {

		if (connection != null) {
			try {
				connection.close();
				logger.log(Level.INFO, "Database connection terminated");
				return true;
			} catch (Exception e) { /* ignore close errors */
				return false;
			}
		}

		return false;
	}


	public NesterUserSettings getNesterUserSettings(int id) {

		NesterUserSettings user = null;

		try {

			Statement s = connection.createStatement();
			s.executeQuery("select * from tools_usersettings where id = " + id);
			ResultSet rs = s.getResultSet();
			if (rs.next()) {

				user = new NesterUserSettings(rs.getInt("id"), rs.getString("name"), rs.getDouble("windowSizeInSeconds"), rs.getDouble("spectraMinimumBandFrequency"),
						rs.getDouble("spectraMaximumBandFrequency"), rs.getDouble("spectraDampingFactor"), rs.getInt("spectraNumFrequencyBands"),
						rs.getDouble("spectraNumFramesPerSecond"), rs.getBoolean("showSpectra"), rs.getBoolean("showWaveform"), rs.getBoolean("loadAudio"),
						rs.getInt("maxNumAudioFilesToList"));

			} else {
				System.out.println("Error!  Project not found.");
			}
			rs.close();
			s.close();

		} catch (Exception e) {
			logger.log(Level.INFO, "name not found");
			logger.log(Level.INFO, "Exception = " + e);
		}

		return user;
	}


	public Vector<NesterTagClass> getNesterTagSetTagClasses(int tagSetID) {

		Vector<NesterTagClass> tagClasses = new Vector<NesterTagClass>();

		try {

			// logger.log(Level.INFO, "connection = " + connection);

			Statement s1 = connection.createStatement();
			s1.executeQuery("select * from tools_tagclass where tagSet_id = " + tagSetID);
			ResultSet rs1 = s1.getResultSet();

			while (rs1.next()) {

				NesterTagClass nesterTagClass = null;

				nesterTagClass = new NesterTagClass(rs1.getInt("id"), rs1.getInt("user_id"), rs1.getInt("tagSet_id"), rs1.getString("className"), rs1.getString("displayName"));

				tagClasses.add(nesterTagClass);

			}
			rs1.close();
			s1.close();

			return tagClasses;

		} catch (Exception e) {
			logger.log(Level.INFO, "Exception = " + e);
		}

		return tagClasses;
	}


	public NesterADAPTAnalysisBias getNesterADAPTAnalysisBias(int adaptAnalysisBiasID) {

		// TODO *** Implement ADAPTAnalysisBias

		logger.log(Level.SEVERE, "**********************************************");
		logger.log(Level.SEVERE, "**                                          **");
		logger.log(Level.SEVERE, "**  AdaptAnalysis Job not implemented       **");
		logger.log(Level.SEVERE, "**                                          **");
		logger.log(Level.SEVERE, "**********************************************");
		return null;
		//
		// NesterADAPTAnalysisBias adaptAnalysisBias = null;
		//
		// try {
		//
		// Statement s1 = connection.createStatement();
		// s1.executeQuery("select * from tools_adaptanalysisbias where id = " + adaptAnalysisBiasID);
		// ResultSet rs1 = s1.getResultSet();
		//
		// if (rs1.next()) {
		//
		// adaptAnalysisBias = new NesterADAPTAnalysisBias(rs1.getInt("id"), rs1.getString("name"), rs1.getInt("user_id"), rs1.getInt("project_id"),
		// rs1.getDate("creationDate"), rs1.getInt("numToComplete"), rs1.getInt("numCompleted"), rs1.getDouble("fractionCompleted"), rs1.getDouble("elapsedRealTime"),
		// rs1.getDouble("timeToCompletion"), rs1.getBoolean("isRunning"), rs1.getBoolean("wasStopped"), rs1.getBoolean("isComplete"), rs1.getBoolean("wasDeleted"),
		//
		// rs1.getInt("maxNumTrials"), rs1.getDouble("maxTimeInSeconds"),
		//
		// rs1.getInt("numFrequencyBands"), rs1.getDouble("numTimeFramesPerSecond"), rs1.getDouble("dampingRatio"), rs1.getDouble("minFrequency"),
		// rs1.getDouble("maxFrequency"),
		//
		// rs1.getDouble("spectraWeight"), rs1.getDouble("pitchWeight"), rs1.getDouble("pitchEnergyWeight"), rs1.getDouble("averageEnergyWeight"),
		//
		// rs1.getInt("numRandomProbes"), rs1.getDouble("minPerformance"));
		//
		// }
		// rs1.close();
		// s1.close();
		//
		// } catch (Exception e) {
		// logger.log(Level.INFO, "name not found");
		// logger.log(Level.INFO, "Exception = " + e);
		// }
		//
		// return adaptAnalysisBias;
	}


	/** \brief Open, parse, and save to the database an audio file's header and metadata.
	 *
	 * @param audioFileID Database ID of the MediaFile to analyze.
	 * @return true on success, false otherwise
	 */

	public boolean analyzeAudioFile(int audioFileID) {

		logger.log(Level.INFO, "analyzeAudioFile audioFileID = " + audioFileID);
		System.out.println("analyzeAudioFile audioFileID = " + audioFileID);

		// Retrieve the existing object from the database. 
		NesterAudioFile nesterAudioFile = NesterAudioFile.getAudioFile(audioFileID, connection, null);

		try {

			// Open and parse the file
			AudioFile audioFile = new AudioFile(nesterAudioFile.GetAbsoluteFilePath());

			if (audioFile == null) {
				logger.log(Level.INFO, "audioFile == null");
				return false;
			}

			if (audioFile.fileSize > 0 && audioFile.dataSize > 0 && audioFile.validRIFFsize && audioFile.validDATAsize) {
				System.out.println(audioFile + "\n");
			} else {

				String reason = "";

				if (audioFile.dataSize <= 0)
					reason += "(audioFile.dataSize <= 0)";
				if (audioFile.validRIFFsize == false)
					reason += "(audioFile.validRIFFsize == false)";
				if (audioFile.validDATAsize == false)
					reason += "(audioFile.validDATAsize == false)";

				System.out.println("File rejected for reason(s):  " + reason + "\n" + audioFile + "\n");
				logger.log(Level.INFO, "File rejected for reason(s):  " + reason + "  " + audioFile);
			}

			nesterAudioFile.wavFileMetaData.fileSize = audioFile.fileSize;
			nesterAudioFile.wavFileMetaData.validRIFFsize = audioFile.validRIFFsize;
			nesterAudioFile.wavFileMetaData.validDATAsize = audioFile.validDATAsize;
			nesterAudioFile.wavFileMetaData.dataStartIndex = audioFile.dataStartIndex;
			nesterAudioFile.wavFileMetaData.wFormatTag = audioFile.wFormatTag;
			nesterAudioFile.wavFileMetaData.numChannels = audioFile.numChannels;
			nesterAudioFile.wavFileMetaData.sampleRate = audioFile.sampleRate;
			nesterAudioFile.wavFileMetaData.nAvgBytesPerSec = audioFile.nAvgBytesPerSec;
			nesterAudioFile.wavFileMetaData.nBlockAlign = audioFile.nBlockAlign;
			nesterAudioFile.wavFileMetaData.bitsPerSample = audioFile.bitsPerSample;
			nesterAudioFile.wavFileMetaData.numBytesToTailPad = audioFile.numBytesToTailPad;
			nesterAudioFile.wavFileMetaData.numFrames = audioFile.numFrames;
			nesterAudioFile.wavFileMetaData.durationInSeconds = audioFile.durationInSeconds;
			nesterAudioFile.wavFileMetaData.dataSize = audioFile.dataSize;

			return nesterAudioFile.wavFileMetaData.saveNesterWavFileMetaData(nesterAudioFile.id, connection);

		} catch (Exception e) {
			System.out.println("Exception = " + e);
			logger.log(Level.INFO, "Exception = " + e);
		}

		return false;
	}

	HashMap<Integer, ADAPTAnalysis> adaptAnalysisThreads = new HashMap<Integer, ADAPTAnalysis>();

	public void startADAPTAnalysisJob(int adaptAnalysisBiasID) {
// diabled with cassandra updates
//		ADAPTAnalysis adaptAnalysis = new ADAPTAnalysis(this, adaptAnalysisBiasID);
//
//		try {
//			Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
//			ResultSet uprs = stmt.executeQuery("SELECT * FROM tools_adaptanalysisbias where id=" + adaptAnalysisBiasID);
//			uprs.next();
//			uprs.updateBoolean("isRunning", true);
//			uprs.updateRow();
//			uprs.close();
//			stmt.close();
//		} catch (Exception e) {
//			logger.log(Level.INFO, "Exception = " + e);
//		}
//
//		adaptAnalysis.start();
//
//		// add thread to job hash map
//		adaptAnalysisThreads.put(adaptAnalysisBiasID, adaptAnalysis);

	}

	public void pauseADAPTAnalysisJob(int adaptAnalysisBiasID) {
// disabled with Cassandra updates
//		try {
//			Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
//			ResultSet uprs = stmt.executeQuery("SELECT * FROM tools_adaptanalysisbias where id=" + adaptAnalysisBiasID);
//			uprs.next();
//			uprs.updateBoolean("isRunning", false);
//			uprs.updateRow();
//			uprs.close();
//			stmt.close();
//		} catch (Exception e) {
//			logger.log(Level.INFO, "Exception = " + e);
//		}
//
//		// find thread and pause it
//		ADAPTAnalysis adaptAnalysisThread = adaptAnalysisThreads.get(adaptAnalysisBiasID);
//		if (adaptAnalysisThread != null)
//			adaptAnalysisThread.pauseThread();
//
	}

	public void resumeADAPTAnalysisJob(int adaptAnalysisBiasID) {
// disabled with cassandra updates
//		try {
//			Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
//			ResultSet uprs = stmt.executeQuery("SELECT * FROM tools_adaptanalysisbias where id=" + adaptAnalysisBiasID);
//			uprs.next();
//			uprs.updateBoolean("isRunning", true);
//			uprs.updateRow();
//			uprs.close();
//			stmt.close();
//		} catch (Exception e) {
//			logger.log(Level.INFO, "Exception = " + e);
//		}
//
//		// find thread and resume it
//		ADAPTAnalysis adaptAnalysisThread = adaptAnalysisThreads.get(adaptAnalysisBiasID);
//		if (adaptAnalysisThread != null) {
//			adaptAnalysisThreads.get(adaptAnalysisBiasID).resumeThread();
//		}
//
	}

	public void stopADAPTAnalysisJob(int adaptAnalysisBiasID) {
// disabled with Cassandra updates
//		try {
//			Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
//			ResultSet uprs = stmt.executeQuery("SELECT * FROM tools_adaptanalysisbias where id=" + adaptAnalysisBiasID);
//			uprs.next();
//			uprs.updateBoolean("wasStopped", true);
//			uprs.updateRow();
//			uprs.close();
//			stmt.close();
//		} catch (Exception e) {
//			logger.log(Level.INFO, "Exception = " + e);
//		}
//
//		// find thread and stop it
//
//		ADAPTAnalysis adaptAnalysisThread = adaptAnalysisThreads.get(adaptAnalysisBiasID);
//		if (adaptAnalysisThread != null) {
//			adaptAnalysisThread.stopThread();
//			adaptAnalysisThreads.remove(adaptAnalysisBiasID);
//		}
//
	}

	HashMap<Integer, UnsupervisedTagDiscovery> unsupervisedTagDiscoveryThreads = new HashMap<Integer, UnsupervisedTagDiscovery>();

	public void startUnsupervisedTagDiscoveryJob(int unsupervisedTagDiscoveryBiasID) {
		UnsupervisedTagDiscovery unsupervisedTagDiscovery = new UnsupervisedTagDiscovery(this, unsupervisedTagDiscoveryBiasID);

		try {
			Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			ResultSet uprs = stmt.executeQuery("SELECT * FROM tools_jobs where id=" + unsupervisedTagDiscoveryBiasID);
			uprs.next();
			uprs.updateBoolean("isRunning", true);
			uprs.updateRow();
			uprs.close();
			stmt.close();
		} catch (Exception e) {
			logger.log(Level.INFO, "Exception = " + e);
		}

		unsupervisedTagDiscovery.start();

		// add thread to job hash map
		unsupervisedTagDiscoveryThreads.put(unsupervisedTagDiscoveryBiasID, unsupervisedTagDiscovery);

	}

	public void pauseUnsupervisedTagDiscoveryJob(int unsupervisedTagDiscoveryBiasID) {
		try {
			Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			ResultSet uprs = stmt.executeQuery("SELECT * FROM tools_jobs where id=" + unsupervisedTagDiscoveryBiasID);
			uprs.next();
			uprs.updateBoolean("isRunning", false);
			uprs.updateRow();
			uprs.close();
			stmt.close();
		} catch (Exception e) {
			logger.log(Level.INFO, "Exception = " + e);
		}

		// find thread and pause it
		UnsupervisedTagDiscovery unsupervisedTagDiscoveryThread = unsupervisedTagDiscoveryThreads.get(unsupervisedTagDiscoveryBiasID);
		if (unsupervisedTagDiscoveryThread != null)
			unsupervisedTagDiscoveryThread.pauseThread();

	}

	public void resumeUnsupervisedTagDiscoveryJob(int unsupervisedTagDiscoveryBiasID) {
		try {
			Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			ResultSet uprs = stmt.executeQuery("SELECT * FROM tools_jobs where id=" + unsupervisedTagDiscoveryBiasID);
			uprs.next();
			uprs.updateBoolean("isRunning", true);
			uprs.updateRow();
			uprs.close();
			stmt.close();
		} catch (Exception e) {
			logger.log(Level.INFO, "Exception = " + e);
		}

		// find thread and resume it
		UnsupervisedTagDiscovery unsupervisedTagDiscoveryThread = unsupervisedTagDiscoveryThreads.get(unsupervisedTagDiscoveryBiasID);
		if (unsupervisedTagDiscoveryThread != null) {
			unsupervisedTagDiscoveryThreads.get(unsupervisedTagDiscoveryBiasID).resumeThread();
		}

	}

	public void stopUnsupervisedTagDiscoveryJob(int unsupervisedTagDiscoveryBiasID) {
		try {
			Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			ResultSet uprs = stmt.executeQuery("SELECT * FROM tools_jobs where id=" + unsupervisedTagDiscoveryBiasID);
			uprs.next();
			uprs.updateBoolean("wasStopped", true);
			uprs.updateRow();
			uprs.close();
			stmt.close();
		} catch (Exception e) {
			logger.log(Level.INFO, "Exception = " + e);
		}

		// find thread and stop it

		UnsupervisedTagDiscovery unsupervisedTagDiscoveryThread = unsupervisedTagDiscoveryThreads.get(unsupervisedTagDiscoveryBiasID);
		if (unsupervisedTagDiscoveryThread != null) {
			unsupervisedTagDiscoveryThread.stopThread();
			unsupervisedTagDiscoveryThreads.remove(unsupervisedTagDiscoveryBiasID);
		}

	}

	HashMap<Integer, SupervisedTagDiscovery> supervisedTagDiscoveryThreads = new HashMap<Integer, SupervisedTagDiscovery>();

	public void startSupervisedTagDiscoveryJob(int supervisedTagDiscoveryBiasID) {

		SupervisedTagDiscovery supervisedTagDiscovery = new SupervisedTagDiscovery(this, supervisedTagDiscoveryBiasID);

		try {
			Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			ResultSet uprs = stmt.executeQuery("SELECT * FROM tools_jobs where id=" + supervisedTagDiscoveryBiasID);
			uprs.next();
			uprs.updateBoolean("isRunning", true);
			uprs.updateRow();
			uprs.close();
			stmt.close();
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Exception = " + e);
		}

		supervisedTagDiscovery.start();

		// add thread to job hash map
		supervisedTagDiscoveryThreads.put(supervisedTagDiscoveryBiasID, supervisedTagDiscovery);

	}

	public void pauseSupervisedTagDiscoveryJob(int supervisedTagDiscoveryBiasID) {

		try {
			Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			ResultSet uprs = stmt.executeQuery("SELECT * FROM tools_jobs where id=" + supervisedTagDiscoveryBiasID);
			uprs.next();
			uprs.updateBoolean("isRunning", false);
			uprs.updateRow();
			uprs.close();
			stmt.close();
		} catch (Exception e) {
			logger.log(Level.INFO, "Exception = " + e);
		}

		// find thread and pause it
		SupervisedTagDiscovery supervisedTagDiscoveryThread = supervisedTagDiscoveryThreads.get(supervisedTagDiscoveryBiasID);
		if (supervisedTagDiscoveryThread != null)
			supervisedTagDiscoveryThread.pauseThread();

	}

	public void resumeSupervisedTagDiscoveryJob(int supervisedTagDiscoveryBiasID) {

		try {
			Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			ResultSet uprs = stmt.executeQuery("SELECT * FROM tools_jobs where id=" + supervisedTagDiscoveryBiasID);
			uprs.next();
			uprs.updateBoolean("isRunning", true);
			uprs.updateRow();
			uprs.close();
			stmt.close();
		} catch (Exception e) {
			logger.log(Level.INFO, "Exception = " + e);
		}

		// find thread and resume it
		SupervisedTagDiscovery supervisedTagDiscoveryThread = supervisedTagDiscoveryThreads.get(supervisedTagDiscoveryBiasID);
		if (supervisedTagDiscoveryThread != null) {
			supervisedTagDiscoveryThreads.get(supervisedTagDiscoveryBiasID).resumeThread();
		}

	}

	public void stopSupervisedTagDiscoveryJob(int supervisedTagDiscoveryBiasID) {

		stoppedJobIDs.add(supervisedTagDiscoveryBiasID);

		try {
			Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			ResultSet uprs = stmt.executeQuery("SELECT * FROM tools_jobs where id=" + supervisedTagDiscoveryBiasID);
			uprs.next();
			uprs.updateBoolean("wasStopped", true);
			uprs.updateRow();
			uprs.close();
			stmt.close();
		} catch (Exception e) {
			logger.log(Level.INFO, "Exception = " + e);
		}

		// find thread and stop it

		SupervisedTagDiscovery supervisedTagDiscoveryThread = supervisedTagDiscoveryThreads.get(supervisedTagDiscoveryBiasID);
		if (supervisedTagDiscoveryThread != null) {
			supervisedTagDiscoveryThread.stopThread();
			supervisedTagDiscoveryThreads.remove(supervisedTagDiscoveryBiasID);
		}

	}

//	HashMap<Integer, TagAnalysis> TagAnalysisThreads = new HashMap();

	public void startTagAnalysisJob(int TagAnalysisBiasID) {
// disabled with cassandra updates
//		TagAnalysis TagAnalysis = new TagAnalysis(this, TagAnalysisBiasID);
//
//		try {
//			Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
//			ResultSet uprs = stmt.executeQuery("SELECT * FROM tools_jobs where id=" + TagAnalysisBiasID);
//			uprs.next();
//			uprs.updateBoolean("isRunning", true);
//			uprs.updateRow();
//			uprs.close();
//			stmt.close();
//		} catch (Exception e) {
//			logger.log(Level.INFO, "Exception = " + e);
//		}
//
//		TagAnalysis.start();
//
//		// add thread to job hash map
//		TagAnalysisThreads.put(TagAnalysisBiasID, TagAnalysis);
//
	}

	public void pauseTagAnalysisJob(int TagAnalysisBiasID) {
// disabled with cassandra updates
//
//		try {
//			Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
//			ResultSet uprs = stmt.executeQuery("SELECT * FROM tools_taganalysisbias where id=" + TagAnalysisBiasID);
//			uprs.next();
//			uprs.updateBoolean("isRunning", false);
//			uprs.updateRow();
//			uprs.close();
//			stmt.close();
//		} catch (Exception e) {
//			logger.log(Level.INFO, "Exception = " + e);
//		}
//
//		// find thread and pause it
//		TagAnalysis TagAnalysisThread = TagAnalysisThreads.get(TagAnalysisBiasID);
//		if (TagAnalysisThread != null)
//			TagAnalysisThread.pauseThread();
//
	}

	public void resumeTagAnalysisJob(int TagAnalysisBiasID) {
// disabled with cassandra updates
//
//		try {
//			Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
//			ResultSet uprs = stmt.executeQuery("SELECT * FROM tools_taganalysisbias where id=" + TagAnalysisBiasID);
//			uprs.next();
//			uprs.updateBoolean("isRunning", true);
//			uprs.updateRow();
//			uprs.close();
//			stmt.close();
//		} catch (Exception e) {
//			logger.log(Level.INFO, "Exception = " + e);
//		}
//
//		// find thread and resume it
//		TagAnalysis TagAnalysisThread = TagAnalysisThreads.get(TagAnalysisBiasID);
//		if (TagAnalysisThread != null) {
//			TagAnalysisThreads.get(TagAnalysisBiasID).resumeThread();
//		}
//
	}

	public void stopTagAnalysisJob(int TagAnalysisBiasID) {
// disabled with cassandra updates
//
//		try {
//			Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
//			ResultSet uprs = stmt.executeQuery("SELECT * FROM tools_taganalysisbias where id=" + TagAnalysisBiasID);
//			uprs.next();
//			uprs.updateBoolean("wasStopped", true);
//			uprs.updateRow();
//			uprs.close();
//			stmt.close();
//		} catch (Exception e) {
//			logger.log(Level.INFO, "Exception = " + e);
//		}
//
//		// find thread and stop it
//
//		TagAnalysis TagAnalysisThread = TagAnalysisThreads.get(TagAnalysisBiasID);
//		if (TagAnalysisThread != null) {
//			TagAnalysisThread.stopThread();
//			TagAnalysisThreads.remove(TagAnalysisBiasID);
//		}
//
	}

	public void updateFractionCompletedForTagAnalysis(TagDiscoveryBias tagDiscoveryBias) {
		tagDiscoveryBias.updateFractionCompletedForTagAnalysis(this.connection);
	}

}
