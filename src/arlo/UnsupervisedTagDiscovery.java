package arlo;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.sql.Connection;

import adapt.IO;

public class UnsupervisedTagDiscovery extends TagDiscovery {

	ServiceHead serviceHead = null;

	UnsupervisedTagDiscovery(ServiceHead serviceHead, int tagDiscoveryBiasID) {
		super(tagDiscoveryBiasID);
		this.serviceHead = serviceHead;
	}

	UnsupervisedTagDiscovery(int tagDiscoveryBiasID) {
		super(tagDiscoveryBiasID);
	}

	public enum MutationMode {
		EXCHANGE, MOVE
	}

	double updateIntervalDuaration = 60.0;
	MutationMode mutationMode = null;

	/**
	 * Launch the job when running threaded from ServiceHead.
	 * 
	 * Used to start a thread when called by ServiceHead to start a job. Not used by QueueTask_UnsupervisedTagDiscovery. Note that this function is an override inherited from Thread.
	 */

	public void run() {
		runJob(serviceHead.connection);
	}

	/**
	 * Launch the job when running from QueueTask_UnsupervisedTagDiscovery.
	 * 
	 * @param connection
	 *            An opened database connection.
	 * @return True if success, false if any error occurred.
	 */

	public boolean QueueTaskRun(Connection connection) {
		return runJob(connection);
	}

	/**
	 * Run the Unsupervised Tag Discovery.
	 * 
	 * @param connection
	 *            An opened database connection.
	 * @return True if completed or stopped, False if any error encountered.
	 */

	public boolean runJob(Connection connection) {
		Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		System.gc();

		try {
			long startTimeInMS = System.currentTimeMillis();

			logger.log(Level.INFO, "### starting UnsupervisedTagDiscovery ###");

			NesterUnsupervisedTagDiscoveryBias nesterUnsupervisedTagDiscoveryBias = new NesterUnsupervisedTagDiscoveryBias();

			if (!nesterUnsupervisedTagDiscoveryBias.getNesterUnsupervisedTagDiscoveryBias(tagDiscoveryBias.id, connection)) {
				logger.log(Level.SEVERE, "UnsupervisedTagDiscovery.run - Failed getting nesterUnsupervisedTagDiscoveryBias");
				return false;
			}

			tagDiscoveryBias = (TagDiscoveryBias) nesterUnsupervisedTagDiscoveryBias;

			runTagDiscoveryStart(tagDiscoveryBias.project_id, connection);

			logger.log(Level.INFO, "### nesterUnsupervisedTagDiscoveryBias = " + nesterUnsupervisedTagDiscoveryBias);

			BandPass bandPass = new BandPass();
			bandPass.setNumBands(nesterUnsupervisedTagDiscoveryBias.numFrequencyBands);
			bandPass.setMinBandFrequency(nesterUnsupervisedTagDiscoveryBias.minFrequency);
			bandPass.setMaxBandFrequency(nesterUnsupervisedTagDiscoveryBias.maxFrequency);
			bandPass.initialize();

			if (nesterUnsupervisedTagDiscoveryBias.exchangeMode)
				mutationMode = MutationMode.EXCHANGE;

			if (nesterUnsupervisedTagDiscoveryBias.moveMode)
				mutationMode = MutationMode.MOVE;

			int numClusters = nesterUnsupervisedTagDiscoveryBias.numberOfClusters;
			int initialNumExamplesPerCluster = nesterUnsupervisedTagDiscoveryBias.numberOfExamplesPerCluster;

			logger.log(Level.INFO, "### numberOfClusters = " + numClusters);
			logger.log(Level.INFO, "### initialNumExamplesPerCluster = " + initialNumExamplesPerCluster);

			// Get Random Seed, if used
			boolean useUserRandomSeed = false;
			Long userRandomSeed = null;
			{
				String s = nesterUnsupervisedTagDiscoveryBias.GetJobParameter("forceRandomSeed");
				if (s != null) {
					useUserRandomSeed = true;
					userRandomSeed = Long.parseLong(s.trim());
					if (userRandomSeed == null) {
						logger.log(Level.SEVERE, "'searchWithinTagClasses' parent invalid");
						return false;
					}
				}
			}

			Random random = null;

			if (!useUserRandomSeed) {
				random = new Random();
			} else {
				random = new Random(userRandomSeed);
			}

			// Normalize Volume Settings
			boolean normalizeRank = false;
			boolean normalizeEqualEnergy = false;
			{
				String s = nesterUnsupervisedTagDiscoveryBias.GetJobParameter("normalize");
				if (s != null) {
					if (s == "Rank") {
						normalizeRank = true;
					}
					if (s == "EqualEnergy") {
						normalizeEqualEnergy = true;
					}
				}
			}

			int[] clusterTagClass_id = new int[numClusters];

			long endTimeInMS = System.currentTimeMillis();

			double duration = (endTimeInMS - startTimeInMS) / 1000.0;

			logger.log(Level.INFO, "### duration = " + duration);

			Vector<NesterTag> allTags = NesterTagSet.getNesterTagSetTags(nesterUnsupervisedTagDiscoveryBias.sourceTagSets, connection); // dkt 5-24-14

			int numExamples = allTags.size();

			// numClusters * initialNumExamplesPerCluster;

			int[] exampleAudioFileIndices = new int[numExamples];
			int[] exampleTagIDs = new int[numExamples];
			int[] exampleClassIDs = new int[numExamples];
			double[] exampleStartTimes = new double[numExamples];
			double[] exampleEndTimes = new double[numExamples];
			double[] exampleDurations = new double[numExamples];
			int[] exampleStartFreqIndex = new int[numExamples];
			int[] exampleEndFreqIndex = new int[numExamples];
			// int[] exampleStartFrameIndices = new int[numExamples];
			int[] exampleNumFrames = new int[numExamples];

			int[][] clusterExampleIndices = new int[numClusters][numExamples];
			int[] clusterNumExamples = new int[numClusters];

			HashMap<Integer, Integer> tagClasIDToIndex = new HashMap<Integer, Integer>();

			int exampleMinNumFrames = Integer.MAX_VALUE;

			int numClustersFound = 0;
			{

				// evenly distribute examples into N clusters

				int exampleIndex = 0;
				for (Iterator<NesterTag> iterator = allTags.iterator(); iterator.hasNext();) {

					NesterTag nesterTag = iterator.next();

					double exampleStartTime = (double) nesterTag.startTime;
					double exampleEndTime = (double) nesterTag.endTime;
					double exampleDuration = exampleEndTime - exampleStartTime;

					exampleTagIDs[exampleIndex] = nesterTag.id;
					exampleClassIDs[exampleIndex] = nesterTag.tagClass_id;
					

					// Integer clusterIndex = tagClasIDToIndex.get(tagClass_id);

					Integer clusterIndex = exampleIndex % numClusters;

					int audioFileID = nesterTag.audioFile_id;
					int audioFileIndex = -1;
					for (int i = 0; i < tagDiscoveryNumAudioFiles; i++) {
						NesterAudioFile nesterAudioFile = tagDiscoveryNesterAudioFiles.get(i);
						if (nesterAudioFile.id == audioFileID) {
							audioFileIndex = i;
							break;
						}
					}

					exampleAudioFileIndices[exampleIndex] = audioFileIndex;

					logger.log(Level.INFO, "### exampleStartTime = " + exampleStartTime);
					logger.log(Level.INFO, "### exampleDuration = " + exampleDuration);

					exampleDurations[exampleIndex] = exampleDuration;
					exampleStartTimes[exampleIndex] = exampleStartTime;
					exampleEndTimes[exampleIndex] = exampleStartTime + exampleDuration;

					exampleNumFrames[exampleIndex] = (int) (exampleDuration * nesterUnsupervisedTagDiscoveryBias.numTimeFramesPerSecond);

					if (exampleNumFrames[exampleIndex] < exampleMinNumFrames) {
						exampleMinNumFrames = exampleNumFrames[exampleIndex];
					}

					exampleStartFreqIndex[exampleIndex] = bandPass.getFrequencyIndex(nesterTag.minFrequency);
					exampleEndFreqIndex[exampleIndex] = bandPass.getFrequencyIndex(nesterTag.maxFrequency);

					clusterExampleIndices[clusterIndex][clusterNumExamples[clusterIndex]] = exampleIndex;
					clusterNumExamples[clusterIndex]++;

					exampleIndex++;
				}
			}

			// }
			// else {
			// for (int exampleIndex = 0; exampleIndex < numExamples; exampleIndex++) {
			//
			// int audioFileIndex = -1;
			// int audioFileNumFrames = -1;
			//
			// while (true) {
			// audioFileIndex = (int) (random.nextDouble() * tagDiscoveryNumAudioFiles);
			// audioFileNumFrames = (int) tagDiscoveryAudioFileSpectraNumFrames[audioFileIndex];
			// if (audioFileNumFrames >= windowNumTimeFrames)
			// break;
			// }
			//
			// int exampleStartFrameIndex = (int) (random.nextDouble() * (audioFileNumFrames - windowNumTimeFrames));
			//
			// int clusterIndex = exampleIndex % numClusters;
			//
			// exampleAudioFileIndices[exampleIndex] = audioFileIndex;
			//
			// System.out.println("### exampleStartFrameIndex = " + exampleStartFrameIndex);
			// System.out.println("### startTime = " + (double) exampleStartFrameIndex / nesterUnsupervisedTagDiscoveryBias.numTimeFramesPerSecond);
			//
			// exampleStartFrameIndices[exampleIndex] = exampleStartFrameIndex;
			// exampleNumFrames[exampleIndex] = windowNumTimeFrames;
			//
			// clusterExampleIndices[clusterIndex][exampleIndex / numClusters] = exampleIndex;
			// clusterNumExamples[clusterIndex]++;
			// }
			// }

			// ********************************************//
			// * load spectra data for each random window *//
			// ********************************************//

			// int numWindowValues = windowNumTimeFrames * tagDiscoveryNumFrequencyBands;

			computeExampleSpectraData(numExamples, exampleAudioFileIndices, exampleStartTimes, exampleEndTimes, exampleStartFreqIndex, exampleEndFreqIndex);


			// int numWindowValues = nesterUnsupervisedTagDiscoveryBias.minNumSamples;

			// int windowNumTimeFrames = nesterUnsupervisedTagDiscoveryBias.minNumSamples;

			int windowNumSpectraValuesToCompare = exampleMinNumFrames * tagDiscoveryBias.numFrequencyBands;
			System.out.println("### exampleMinNumFrames = " + exampleMinNumFrames);
			System.out.println("### windowNumSpectraValuesToCompare = " + windowNumSpectraValuesToCompare);


			boolean saveData = true;
			
			if (saveData) {
			StringBuffer spectralDataString = new StringBuffer();
			for (int exampleIndex = 0; exampleIndex < numExamples; exampleIndex++) {

				try {

					 spectralDataString.setLength(0);

					for (int valueIndex = 0; valueIndex < windowNumSpectraValuesToCompare; valueIndex++) {
						spectralDataString.append("\t" + tagDiscoveryAllExampleSpectra[exampleIndex][valueIndex]);
					}

					IO.appendStringLine("/data/example.dat", //
							exampleIndex + "\t" + //
									exampleTagIDs[exampleIndex] + "\t" + //
									exampleClassIDs[exampleIndex] + "\t" + //
									exampleAudioFileIndices[exampleIndex] + "\t" + //
									exampleStartTimes[exampleIndex] + "\t" + //
									exampleEndTimes[exampleIndex] + "\t" + //
									exampleDurations[exampleIndex] + "\t" + //
									exampleMinNumFrames + "\t" + //
									windowNumSpectraValuesToCompare + //
									spectralDataString);

				} catch (Exception e) {
					logger.log(Level.INFO, "Failed saving example data");
				}
			}
			}
			

			// *******************************//
			// * normalize spectra if needed *//
			// *******************************//

			// boolean equalEnergyNormalization = false;
			// boolean rankNormalization = true;

			if (normalizeRank || normalizeEqualEnergy) {

				if (normalizeEqualEnergy) {

					int maxNormalizedValue = 1000;

					for (int clusterIndex = 0; clusterIndex < numClusters; clusterIndex++) {

						for (int clusterExampleIndex = 0; clusterExampleIndex < clusterNumExamples[clusterIndex]; clusterExampleIndex++) {

							long intensitySum = 0;

							int exampleIndex = clusterExampleIndices[clusterIndex][clusterExampleIndex];

							// int minValue = Integer.MAX_VALUE;
							// int maxValue = Integer.MIN_VALUE;

							for (int valueIndex = 0; valueIndex < windowNumSpectraValuesToCompare; valueIndex++) {
								// if (tagDiscoveryAllExampleSpectra[exampleIndex][valueIndex] < minValue) {
								// minValue = tagDiscoveryAllExampleSpectra[exampleIndex][valueIndex];
								// }
								// if (tagDiscoveryAllExampleSpectra[exampleIndex][valueIndex] > maxValue) {
								// maxValue = tagDiscoveryAllExampleSpectra[exampleIndex][valueIndex];
								// }
								intensitySum += tagDiscoveryAllExampleSpectra[exampleIndex][valueIndex];
							}

							double averageIntensity = (double) intensitySum / windowNumSpectraValuesToCompare;

							// int range = maxValue - minValue;

							for (int valueIndex = 0; valueIndex < windowNumSpectraValuesToCompare; valueIndex++) {
								// tagDiscoveryAllExampleSpectra[exampleIndex][valueIndex] = (int) (((double) (tagDiscoveryAllExampleSpectra[exampleIndex][valueIndex] - minValue) / (double) range) *
								// maxNormalizedValue);
								tagDiscoveryAllExampleSpectra[exampleIndex][valueIndex] = (int) ((double) tagDiscoveryAllExampleSpectra[exampleIndex][valueIndex]
										/ averageIntensity * maxNormalizedValue);

							}
						}

					}
				}
				if (normalizeRank) {

					ClusterIndexAndQuality[] clusterIndexAndQaulities = new ClusterIndexAndQuality[windowNumSpectraValuesToCompare];

					for (int valueIndex = 0; valueIndex < windowNumSpectraValuesToCompare; valueIndex++) {
						clusterIndexAndQaulities[valueIndex] = new ClusterIndexAndQuality(-1, 0.0);
					}

					for (int clusterIndex = 0; clusterIndex < numClusters; clusterIndex++) {

						for (int clusterExampleIndex = 0; clusterExampleIndex < clusterNumExamples[clusterIndex]; clusterExampleIndex++) {

							int exampleIndex = clusterExampleIndices[clusterIndex][clusterExampleIndex];

							for (int valueIndex = 0; valueIndex < windowNumSpectraValuesToCompare; valueIndex++) {
								clusterIndexAndQaulities[valueIndex].clusterIndex = valueIndex;
								clusterIndexAndQaulities[valueIndex].quality = tagDiscoveryAllExampleSpectra[exampleIndex][valueIndex];
							}

							Arrays.sort(clusterIndexAndQaulities);

							for (int valueIndex = 0; valueIndex < windowNumSpectraValuesToCompare; valueIndex++) {
								tagDiscoveryAllExampleSpectra[exampleIndex][clusterIndexAndQaulities[valueIndex].clusterIndex] = valueIndex;

							}
						}

					}
				}
			}

			// String exampleDataPath = ArloSettings.bigDataCacheDirectoryPath + File.separatorChar + nesterUnsupervisedTagDiscoveryBias.getSpectraComputationBiasString() + ".examples";
			// saveExampleSpectraData(exampleDataPath);

			// *****************************//
			// * compute cluster centroids *//
			// *****************************//

			long[][] clusterCentroidSums = new long[numClusters][windowNumSpectraValuesToCompare];
			int[][] clusterCentroids = new int[numClusters][windowNumSpectraValuesToCompare];
			double[] clusterQuality = new double[numClusters];

			int[] lastClusterACentroid = new int[windowNumSpectraValuesToCompare];
			int[] lastClusterBCentroid = new int[windowNumSpectraValuesToCompare];

			for (int clusterIndex = 0; clusterIndex < numClusters; clusterIndex++) {

				for (int clusterExampleIndex = 0; clusterExampleIndex < clusterNumExamples[clusterIndex]; clusterExampleIndex++) {

					int exampleIndex = clusterExampleIndices[clusterIndex][clusterExampleIndex];

					// double factor = (double) exampleNumFrames[exampleIndex] / (double) windowNumTimeFrames;

					for (int valueIndex = 0; valueIndex < windowNumSpectraValuesToCompare; valueIndex++) {
						// clusterCentroidSums[clusterIndex][valueIndex] += tagDiscoveryAllExampleSpectra[exampleIndex][(int) (valueIndex * factor)];
						clusterCentroidSums[clusterIndex][valueIndex] += tagDiscoveryAllExampleSpectra[exampleIndex][valueIndex];
					}
				}

				for (int valueIndex = 0; valueIndex < windowNumSpectraValuesToCompare; valueIndex++) {
					clusterCentroids[clusterIndex][valueIndex] = (int) (clusterCentroidSums[clusterIndex][valueIndex] / clusterNumExamples[clusterIndex]);
				}

			}

			// ****************************//*
			// * report cluster centroids
			// ****************************//*

			boolean reportClusterCentroids = false;

			if (reportClusterCentroids) {
				for (int clusterIndex = 0; clusterIndex < numClusters; clusterIndex++) {
					logger.log(Level.INFO, "\n\nclusterIndex = " + clusterIndex);
					logger.log(Level.INFO, "values: ");

					for (int valueIndex = 0; valueIndex < windowNumSpectraValuesToCompare; valueIndex++) {
						logger.log(Level.INFO, "\t" + clusterCentroids[clusterIndex][valueIndex]);
					}
				}
			}

			// ***************************//
			// * compute cluster quality *//
			// ***************************//

			for (int clusterIndex = 0; clusterIndex < numClusters; clusterIndex++) {

				long absDiffSum = 0;
				for (int clusterExampleIndex = 0; clusterExampleIndex < clusterNumExamples[clusterIndex]; clusterExampleIndex++) {

					int exampleIndex = clusterExampleIndices[clusterIndex][clusterExampleIndex];

					// int audioFileIndex = exampleAudioFileIndices[exampleIndex];
					// int exampleStartFrameIndex = exampleStartFrameIndices[exampleIndex];

					// double factor = (double) exampleNumFrames[exampleIndex] / (double) windowNumTimeFrames;

					for (int valueIndex = 0; valueIndex < windowNumSpectraValuesToCompare; valueIndex++) {
						// absDiffSum += Math.abs(clusterCentroids[clusterIndex][valueIndex] - tagDiscoveryAllExampleSpectra[exampleIndex][(int) (valueIndex * factor)]);
						absDiffSum += Math.abs(clusterCentroids[clusterIndex][valueIndex] - tagDiscoveryAllExampleSpectra[exampleIndex][valueIndex]);
					}
				}

				clusterQuality[clusterIndex] = -(double) absDiffSum / (clusterNumExamples[clusterIndex] * windowNumSpectraValuesToCompare * tagDiscoveryNumFrequencyBands);
			}

			double overallClusterQualitySum = 0;
			for (int clusterIndex = 0; clusterIndex < numClusters; clusterIndex++) {
				overallClusterQualitySum += clusterQuality[clusterIndex] * clusterNumExamples[clusterIndex];
			}
			double overallClusterQuality = overallClusterQualitySum / numClusters;

			for (int clusterIndex = 0; clusterIndex < numClusters; clusterIndex++) {
				logger.log(Level.INFO, "clusterQuality[" + clusterIndex + "] = " + clusterQuality[clusterIndex]);
			}
			logger.log(Level.INFO, "overallClusterQuality = " + overallClusterQuality);

			// optimize configuration //

			int numOptimizationIterations = nesterUnsupervisedTagDiscoveryBias.numOptimizationIterations;

			long lastUpdateTimeInMS = System.currentTimeMillis();

			double lastOverallClusterQuality = overallClusterQuality;

			for (int optmizationIterationIndex = 0; optmizationIterationIndex < numOptimizationIterations; optmizationIterationIndex++) {

				if (optmizationIterationIndex % 1000 == 0) {
					logger.log(Level.INFO, "optmizationIterationIndex = " + optmizationIterationIndex);
					logger.log(Level.INFO, "lastOverallClusterQuality = " + lastOverallClusterQuality);
					try {
						IO.appendStringLine("/data/learning.dat", optmizationIterationIndex + "\t" + lastOverallClusterQuality);
					} catch (Exception e) {
						logger.log(Level.INFO, "Failed saving learning curve");
					}
				}

				//
				// pick two clusters, A and B, for movement/exchange
				//

				int clusterIndexA = -1;
				int clusterIndexB = -1;

				if (mutationMode == MutationMode.EXCHANGE) {
					clusterIndexA = (int) (random.nextDouble() * numClusters);
				}

				if (mutationMode == MutationMode.MOVE) {
					while (true) {
						clusterIndexA = (int) (random.nextDouble() * numClusters);
						if (clusterNumExamples[clusterIndexA] > nesterUnsupervisedTagDiscoveryBias.minNumSamples) // dkt 05-24-2014
							break;
					}
				}

				while (true) {

					clusterIndexB = (int) (random.nextDouble() * numClusters);

					if (clusterIndexA != clusterIndexB)
						break;
				}

				int clusterExampleIndexA = -1;
				int clusterExampleIndexB = -1;

				if (mutationMode == MutationMode.EXCHANGE) {

					//
					// pick two cluster examples for movement/exchange
					//

					clusterExampleIndexA = (int) (random.nextDouble() * clusterNumExamples[clusterIndexA]);
					clusterExampleIndexB = (int) (random.nextDouble() * clusterNumExamples[clusterIndexB]);
				}

				if (mutationMode == MutationMode.MOVE) {

					//
					// pick a cluster example to move from cluster A to cluster B
					//

					clusterExampleIndexA = (int) (random.nextDouble() * clusterNumExamples[clusterIndexA]);
				}

				// record last state

				double lastClusterQualityA = clusterQuality[clusterIndexA];
				double lastClusterQualityB = clusterQuality[clusterIndexB];

				// save last centroids for A and B
				for (int valueIndex = 0; valueIndex < windowNumSpectraValuesToCompare; valueIndex++) {
					lastClusterACentroid[valueIndex] = clusterCentroids[clusterIndexA][valueIndex];
					lastClusterBCentroid[valueIndex] = clusterCentroids[clusterIndexB][valueIndex];
				}

				int exampleIndexA = -1;
				int exampleIndexB = -1;

				if (mutationMode == MutationMode.EXCHANGE) {
					//
					// exchange A & B
					//

					exampleIndexA = clusterExampleIndices[clusterIndexA][clusterExampleIndexA];
					exampleIndexB = clusterExampleIndices[clusterIndexB][clusterExampleIndexB];

					clusterExampleIndices[clusterIndexA][clusterExampleIndexA] = exampleIndexB;
					clusterExampleIndices[clusterIndexB][clusterExampleIndexB] = exampleIndexA;
				}
				if (mutationMode == MutationMode.MOVE) {
					//
					// move example from A to B
					//

					exampleIndexA = clusterExampleIndices[clusterIndexA][clusterExampleIndexA];

					clusterExampleIndices[clusterIndexB][clusterNumExamples[clusterIndexB]] = exampleIndexA;
					clusterNumExamples[clusterIndexB]++;

					clusterExampleIndices[clusterIndexA][clusterExampleIndexA] = clusterExampleIndices[clusterIndexA][clusterNumExamples[clusterIndexA] - 1];
					clusterNumExamples[clusterIndexA]--;

				}

				// compute new centroids for A and B

				for (int i = 0; i < 2; i++) {

					int clusterIndex = -1;
					if (i == 0)
						clusterIndex = clusterIndexA;
					if (i == 1)
						clusterIndex = clusterIndexB;

					// clear array
					for (int valueIndex = 0; valueIndex < windowNumSpectraValuesToCompare; valueIndex++) {
						clusterCentroidSums[clusterIndex][valueIndex] = 0;
					}

					for (int clusterExampleIndex = 0; clusterExampleIndex < clusterNumExamples[clusterIndex]; clusterExampleIndex++) {

						int exampleIndex = clusterExampleIndices[clusterIndex][clusterExampleIndex];

						// int audioFileIndex = exampleAudioFileIndices[exampleIndex];
						// int exampleStartFrameIndex = exampleStartFrameIndices[exampleIndex];

						// double factor = (double) exampleNumFrames[exampleIndex] / (double) windowNumTimeFrames;

						for (int valueIndex = 0; valueIndex < windowNumSpectraValuesToCompare; valueIndex++) {
							// clusterCentroidSums[clusterIndex][valueIndex] += tagDiscoveryAllExampleSpectra[exampleIndex][(int) (valueIndex * factor)];
							clusterCentroidSums[clusterIndex][valueIndex] += tagDiscoveryAllExampleSpectra[exampleIndex][valueIndex];
						}
					}

					for (int valueIndex = 0; valueIndex < windowNumSpectraValuesToCompare; valueIndex++) {
						clusterCentroids[clusterIndex][valueIndex] = (int) (clusterCentroidSums[clusterIndex][valueIndex] / clusterNumExamples[clusterIndex]);
					}

					// compute cluster quality

					long absDiffSum = 0;
					for (int clusterExampleIndex = 0; clusterExampleIndex < clusterNumExamples[clusterIndex]; clusterExampleIndex++) {

						int exampleIndex = clusterExampleIndices[clusterIndex][clusterExampleIndex];

						// int audioFileIndex = exampleAudioFileIndices[exampleIndex];
						// int exampleStartFrameIndex = exampleStartFrameIndices[exampleIndex];

						// double factor = (double) exampleNumFrames[exampleIndex] / (double) windowNumTimeFrames;

						for (int valueIndex = 0; valueIndex < windowNumSpectraValuesToCompare; valueIndex++) {
							// absDiffSum += Math.abs(clusterCentroids[clusterIndex][valueIndex] - tagDiscoveryAllExampleSpectra[exampleIndex][(int) (valueIndex * factor)]);
							absDiffSum += Math.abs(clusterCentroids[clusterIndex][valueIndex] - tagDiscoveryAllExampleSpectra[exampleIndex][valueIndex]);
						}
					}

					clusterQuality[clusterIndex] = -(double) absDiffSum / (clusterNumExamples[clusterIndex] * windowNumSpectraValuesToCompare * tagDiscoveryNumFrequencyBands);

				}

				overallClusterQualitySum = 0;
				for (int clusterIndex = 0; clusterIndex < numClusters; clusterIndex++) {
					overallClusterQualitySum += clusterQuality[clusterIndex] * clusterNumExamples[clusterIndex];
				}
				overallClusterQuality = overallClusterQualitySum / numClusters;

				double deltaQualtity = overallClusterQuality - lastOverallClusterQuality;

				if (deltaQualtity < 0.0) {

					// System.out.println("BAD");
					// bad solution switch back

					clusterQuality[clusterIndexA] = lastClusterQualityA;
					clusterQuality[clusterIndexB] = lastClusterQualityB;

					if (mutationMode == MutationMode.EXCHANGE) {
						//
						// undo exchange A & B
						//
						clusterExampleIndices[clusterIndexA][clusterExampleIndexA] = exampleIndexA;
						clusterExampleIndices[clusterIndexB][clusterExampleIndexB] = exampleIndexB;
					}
					if (mutationMode == MutationMode.MOVE) {
						//
						// undo move example from A to B
						//

						clusterExampleIndices[clusterIndexA][clusterNumExamples[clusterIndexA]] = clusterExampleIndices[clusterIndexA][clusterExampleIndexA];
						clusterExampleIndices[clusterIndexA][clusterExampleIndexA] = clusterExampleIndices[clusterIndexB][clusterNumExamples[clusterIndexB] - 1];
						clusterNumExamples[clusterIndexA]++;
						clusterNumExamples[clusterIndexB]--;

					}

					for (int valueIndex = 0; valueIndex < windowNumSpectraValuesToCompare; valueIndex++) {
						clusterCentroids[clusterIndexA][valueIndex] = lastClusterACentroid[valueIndex];
						clusterCentroids[clusterIndexB][valueIndex] = lastClusterBCentroid[valueIndex];
					}

				} else {
					// System.out.println("GOOD");

					lastOverallClusterQuality = overallClusterQuality;
				}

			}
			
			

			/*******************************************************/
			/* analyze clusters to compute classification accuracy */
			/*******************************************************/

			boolean analyzeClusters = true;
			
			if (analyzeClusters) {
				
				for (int clusterIndex = 0; clusterIndex < numClusters; clusterIndex++) {

					for (int clusterExampleIndex = 0; clusterExampleIndex < clusterNumExamples[clusterIndex]; clusterExampleIndex++) {

						int exampleIndex = clusterExampleIndices[clusterIndex][clusterExampleIndex];

					}

				}
			}
			
			

			boolean updateCatalog = true;
			
			if (updateCatalog) {

				lastUpdateTimeInMS = System.currentTimeMillis();

				logger.log(Level.INFO, "UPDATING CATALOG");
				logger.log(Level.INFO, "overallClusterQuality = " + overallClusterQuality);

				// delete all tag Examples
				NesterTag.deleteAllTagsInTagSet(nesterUnsupervisedTagDiscoveryBias.destinationTagSetId, connection);

				logger.log(Level.INFO, "### allTags.size() = " + allTags.size());
				logger.log(Level.INFO, "### end getNesterTagSetTags ###");

				ArrayList<ClusterIndexAndQuality> clusterIndexAndQaulity = new ArrayList<ClusterIndexAndQuality>();

				for (int clusterIndex = 0; clusterIndex < numClusters; clusterIndex++) {
					clusterIndexAndQaulity.add(new ClusterIndexAndQuality(clusterIndex, clusterQuality[clusterIndex]));
				}

				Collections.sort(clusterIndexAndQaulity);

				for (int clusterIndex = 0; clusterIndex < numClusters; clusterIndex++) {
					logger.log(Level.INFO, "clusterIndexAndPopulation.quality = " + clusterIndexAndQaulity.get(clusterIndex).quality);
					logger.log(Level.INFO, "clusterIndexAndPopulation.clusterIndex = " + clusterIndexAndQaulity.get(clusterIndex).clusterIndex);
				}

				ArrayList<Integer> savedClusterIndices = new ArrayList<Integer>();
				for (int i = 0; i < nesterUnsupervisedTagDiscoveryBias.numberOfClusters; i++) {
					savedClusterIndices.add(clusterIndexAndQaulity.get(i).clusterIndex);
				}

				// create tag
				ArrayList<NesterTag> nesterTags = new ArrayList<NesterTag>();

				int maxClassNameLength = Integer.toString(nesterUnsupervisedTagDiscoveryBias.numberOfClusters).length();
				for (int savedClusterIndex = 0; savedClusterIndex < nesterUnsupervisedTagDiscoveryBias.numberOfClusters; savedClusterIndex++) {

					int clusterIndex = savedClusterIndices.get(savedClusterIndex);

					String numberFormat = "%0" + maxClassNameLength + "d";
					String clusterName = "Cluster" + String.format(numberFormat, (savedClusterIndex + 1));

					NesterTagClass nesterTagClass = NesterTagClass.getNesterTagClassByName(clusterName, nesterUnsupervisedTagDiscoveryBias.project_id, connection);
					if (nesterTagClass == null) {
						nesterTagClass = new NesterTagClass(-1, nesterUnsupervisedTagDiscoveryBias.user_id, nesterUnsupervisedTagDiscoveryBias.project_id, clusterName, clusterName);
						clusterTagClass_id[clusterIndex] = NesterTagClass.addTagClass(nesterTagClass, connection);
						if (clusterTagClass_id[clusterIndex] < 0) {
							logger.log(Level.INFO, "Failed adding TagClass - failed");
							return false;
						}
					} else {
						clusterTagClass_id[clusterIndex] = nesterTagClass.id;
					}
					// clusterTagClass_id[clusterIndex] = nesterTagClass.id;

					// int nesterTagClass = -1;
					// TODO
					// try {
					//
					// Statement s = serviceHead.connection.createStatement();
					// s.executeQuery("select * from tools_tagclass where user_id = " + project.user_id + " and tagSet_id = " + nesterUnsupervisedTagDiscoveryBias.destinationTagSetId
					// + " and className = \"unknown\"");
					// ResultSet rs = s.getResultSet();
					// if (rs.next()) {
					//
					// nesterTagClass = rs.getInt("id");
					//
					// }
					// rs.close();
					// s.close();
					//
					// } catch (Exception e) {
					//
					// Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
					// logger.log(Level.INFO, "name not found");
					// logger.log(Level.INFO, "Exception = " + e);
					// }
					//
					// clusterTagClass_id[clusterIndex] = nesterTagClass;

					logger.log(Level.INFO, "### clusterTagClass_id[" + clusterIndex + "] = " + clusterTagClass_id[clusterIndex]);

					for (int clusterExampleIndex = 0; clusterExampleIndex < clusterNumExamples[clusterIndex]; clusterExampleIndex++) {

						int exampleIndex = clusterExampleIndices[clusterIndex][clusterExampleIndex];

						int audioFileIndex = exampleAudioFileIndices[exampleIndex];

						NesterAudioFile nesterAudioFile = tagDiscoveryNesterAudioFiles.get(audioFileIndex);

						// compute example quality

						long absDiffSum = 0;

						for (int valueIndex = 0; valueIndex < windowNumSpectraValuesToCompare; valueIndex++) {
							absDiffSum += Math.abs(clusterCentroids[clusterIndex][valueIndex] - tagDiscoveryAllExampleSpectra[exampleIndex][valueIndex]);
						}

						NesterTag newTag = null;
						{
							int id = -1;
							int user_id = nesterUnsupervisedTagDiscoveryBias.user_id;
							int tagSet_id = nesterUnsupervisedTagDiscoveryBias.destinationTagSetId;

							int audioFile_id = nesterAudioFile.id;
							java.sql.Timestamp creationDate = new java.sql.Timestamp(System.currentTimeMillis());
							double startTime = exampleStartTimes[exampleIndex];
							double endTime = exampleEndTimes[exampleIndex];
							double minFrequency = nesterUnsupervisedTagDiscoveryBias.minFrequency;
							double maxFrequency = nesterUnsupervisedTagDiscoveryBias.maxFrequency;

							int tagClass_id = clusterTagClass_id[clusterIndex];
							int parentTag_id = -1;
							boolean randomlyChosen = false;
							boolean machineTagged = true;
							boolean userTagged = false;
							double strength = -absDiffSum;

							newTag = new NesterTag(
									id, user_id, creationDate, tagClass_id,
									tagSet_id, audioFile_id, startTime, endTime, minFrequency, maxFrequency,
									parentTag_id, randomlyChosen, machineTagged, userTagged, strength);

						}

						nesterTags.add(newTag);
					}

				}

				if (! NesterTag.addTagsToProject(nesterUnsupervisedTagDiscoveryBias.project_id, nesterTags, null)) {
					return false;
				}
			}

			logger.log(Level.INFO, "### finished discoverTags ###");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}

}
