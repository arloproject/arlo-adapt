package arlo;

public class Correlation {

    public static double correlation(double[] xValues, double[] yValues, int numObservations) {

        double correlation = Double.NaN;


        double N = numObservations;
        double sum_sq_x = 0;
        double sum_sq_y = 0;
        double sum_coproduct = 0;
        double mean_x = xValues[0];
        double mean_y = yValues[0];

        for (int i = 2; i <= N; i++) {

            double sweep = (i - 1.0) / i;
            double delta_x = xValues[i - 1] - mean_x;
            double delta_y = yValues[i - 1] - mean_y;
            sum_sq_x += delta_x * delta_x * sweep;
            sum_sq_y += delta_y * delta_y * sweep;
            sum_coproduct += delta_x * delta_y * sweep;
            mean_x += delta_x / i;
            mean_y += delta_y / i;
        }

        double pop_sd_x = Math.sqrt(sum_sq_x / N);
        double pop_sd_y = Math.sqrt(sum_sq_y / N);
        double cov_x_y = sum_coproduct / N;
        correlation = cov_x_y / (pop_sd_x * pop_sd_y);


        return correlation;

    }



    public static double correlation(double[] xValues, double[] yValues, int offsetX, int offsetY, int numObservations) {

        double correlation = Double.NaN;


        double N = numObservations;
        double sum_sq_x = 0;
        double sum_sq_y = 0;
        double sum_coproduct = 0;
        double mean_x = xValues[offsetX];
        double mean_y = yValues[offsetY];

        int iXOffset = -1 + offsetX;
        int iYOffset = -1 + offsetY;
        for (int i = 2; i <= N; i++) {

            double sweep = (i - 1.0) / i;
            double delta_x = xValues[i + iXOffset] - mean_x;
            double delta_y = yValues[i + iYOffset] - mean_y;
            sum_sq_x += delta_x * delta_x * sweep;
            sum_sq_y += delta_y * delta_y * sweep;
            sum_coproduct += delta_x * delta_y * sweep;
            mean_x += delta_x / i;
            mean_y += delta_y / i;
        }

        double pop_sd_x = Math.sqrt(sum_sq_x / N);
        double pop_sd_y = Math.sqrt(sum_sq_y / N);
        double cov_x_y = sum_coproduct / N;
        correlation = cov_x_y / (pop_sd_x * pop_sd_y);


        return correlation;

    }

    public static double correlation(int[] xValues, int[] yValues, int offsetX, int offsetY, int numObservations) {

        double correlation = Double.NaN;


        double N = numObservations;
        double sum_sq_x = 0;
        double sum_sq_y = 0;
        double sum_coproduct = 0;
        double mean_x = xValues[offsetX];
        double mean_y = yValues[offsetY];

        int iXOffset = -1 + offsetX;
        int iYOffset = -1 + offsetY;
        for (int i = 2; i <= N; i++) {

            double sweep = (i - 1.0) / i;
            double delta_x = xValues[i + iXOffset] - mean_x;
            double delta_y = yValues[i + iYOffset] - mean_y;
            sum_sq_x += delta_x * delta_x * sweep;
            sum_sq_y += delta_y * delta_y * sweep;
            sum_coproduct += delta_x * delta_y * sweep;
            mean_x += delta_x / i;
            mean_y += delta_y / i;
        }

        double pop_sd_x = Math.sqrt(sum_sq_x / N);
        double pop_sd_y = Math.sqrt(sum_sq_y / N);
        double cov_x_y = sum_coproduct / N;
        correlation = cov_x_y / (pop_sd_x * pop_sd_y);


        return correlation;

    }
    
    
}
