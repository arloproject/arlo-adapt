package arlo;

import java.io.File;
import java.io.Serializable;
import java.util.Vector;

import adapt.IO;

public class AudioFile implements Serializable {

	private static final long serialVersionUID = 5940631679177161666L;

	public String filePath;
	public long fileSize;
	public boolean isWAV;
	public boolean isMP3;
	public boolean isPersyst;

	// from wav meta data
	public boolean validRIFFsize;
	public boolean validDATAsize;
	public int dataStartIndex;
	public long dataSize;
	public int wFormatTag;
	public int numChannels;
	public int sampleRate;
	public int nAvgBytesPerSec;
	public int nBlockAlign;
	public int bitsPerSample;
	public int bytesPerSample;
	public int numBytesToTailPad;
	public int numFrames;
	public double durationInSeconds;

	public byte[] byteData;
	public int[] intData;

	public double[] timeEntropyFeatures;
	public int[] spectraFeatures;

	final static public String audioFileExtensionForWAV = ".wav".toLowerCase();
	final static public String audioFileExtensionForMP3 = ".mp3".toLowerCase();
	final static public String fileExtensionForTIF = ".tif".toLowerCase();
	final static public String fileExtensionForTIFF = ".tiff".toLowerCase();
	final static public String audioFileExtensionForPersyst = ".dat".toLowerCase();

	NesterPersystFile nesterPersystFile = null;

	public AudioFile(String sampleFilePath) {

		File file = new File(sampleFilePath);

		filePath = sampleFilePath;

		fileSize = file.length();

		// if (sampleFilePath.toLowerCase().endsWith(audioFileExtensionForPersyst)) {
		//
		// // load .lay file
		//
		// File layoutFile = new File(sampleFilePath.substring(0, sampleFilePath.length() - 4) + ".lay");
		//
		// if (layoutFile.exists()) {
		//
		// System.out.println("reading Persyst layout file");
		//
		// String[] lines = null;
		// try {
		// lines = IO.readStringLines(layoutFile.getAbsolutePath());
		// } catch (Exception e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		//
		// nesterPersystFile = new NesterPersystFile(lines);
		//
		// isPersyst = true;
		//
		// dataStartIndex = 0;
		//
		// dataSize = fileSize;
		//
		// numChannels = nesterPersystFile.WaveformCount;
		//
		// sampleRate = nesterPersystFile.SamplingRate;
		//
		// bitsPerSample = 16;
		//
		// bytesPerSample = bitsPerSample / 8;
		//
		// nBlockAlign = numChannels * bytesPerSample;
		//
		// nAvgBytesPerSec = sampleRate * nBlockAlign;
		//
		// numFrames = (int) (fileSize / nBlockAlign);
		//
		// durationInSeconds = (double) numFrames / sampleRate;
		//
		// }
		//
		// System.out.println("done reading Persyst layout file");
		//
		// } else

		if (sampleFilePath.toLowerCase().endsWith(audioFileExtensionForWAV)) {

			isWAV = true;

			try {
				BWAVinspector inspector = new BWAVinspector();

				arlo.BWAVinspector.BWAVinformation information = inspector.inspectBWAV(file, false);

				if (information != null) {
					validRIFFsize = information.validRIFFsize;

					// if (information.dataSize >= 2147483648L)
					// validDATAsize = false;
					// else
					// validDATAsize = information.validDATAsize;

					validDATAsize = true; // !!!

					sampleRate = information.nSamplesPerSec;
					wFormatTag = information.wFormatTag;
					numChannels = information.nChannels;
					dataStartIndex = information.dataStartIndex;
					dataSize = information.dataSize;
					nAvgBytesPerSec = information.nAvgBytesPerSec;
					nBlockAlign = information.nBlockAlign;
					bitsPerSample = information.bitsPerSample;
					numBytesToTailPad = information.numBytesToTailPad;

					numFrames = (int) (dataSize / nBlockAlign);

					durationInSeconds = (double) numFrames / sampleRate;

				} else {
					System.out.println("Failed to Extract WAV Info, skipping: " + file.getPath());
					return;
				}
			} catch (Exception e) {

				System.out.println("Failed to Extract WAV Info, skipping: " + file.getPath());
				return;
			}
			return;
		}

		// } else if (sampleFilePath.toLowerCase().endsWith(fileExtensionForTIF) || sampleFilePath.toLowerCase().endsWith(fileExtensionForTIFF)) {
		//
		// System.out.println("### working on TIF file ###");
		// }

		else {

			System.out.println("ERROR: file does not have a .WAV ");

		}

	}

	AudioFile(String filePath, long fileSize, boolean isWAV, boolean isMP3, boolean validRIFFsize, boolean validDATAsize, int dataStartIndex, long dataSize, int wFormatTag,
			int numChannels, int sampleRate, int nAvgBytesPerSec, int nBlockAlign, int bitsPerSample, int numBytesToTailPad) {

		this.filePath = filePath;
		this.fileSize = fileSize;
		this.isWAV = isWAV;
		this.isMP3 = isMP3;

		this.validRIFFsize = validRIFFsize;
		this.validDATAsize = validDATAsize;
		this.dataStartIndex = dataStartIndex;
		this.dataSize = dataSize;

		this.wFormatTag = wFormatTag;
		this.numChannels = numChannels;
		this.sampleRate = sampleRate;
		this.nAvgBytesPerSec = nAvgBytesPerSec;
		this.nBlockAlign = nBlockAlign;
		this.bitsPerSample = bitsPerSample;
		this.numBytesToTailPad = numBytesToTailPad;

		this.numFrames = (int) ((dataSize + numBytesToTailPad) / nBlockAlign);

		this.durationInSeconds = (double) dataSize / (double) nAvgBytesPerSec;

	}

	public AudioFile(NesterAudioFile nesterAudioFile) {

		this.filePath = nesterAudioFile.GetAbsoluteFilePath();
		this.fileSize = nesterAudioFile.wavFileMetaData.fileSize;

		this.validRIFFsize = nesterAudioFile.wavFileMetaData.validRIFFsize;
		this.validDATAsize = nesterAudioFile.wavFileMetaData.validDATAsize;
		this.dataStartIndex = nesterAudioFile.wavFileMetaData.dataStartIndex;
		this.dataSize = nesterAudioFile.wavFileMetaData.dataSize;

		this.wFormatTag = nesterAudioFile.wavFileMetaData.wFormatTag;
		this.numChannels = nesterAudioFile.wavFileMetaData.numChannels;
		this.sampleRate = nesterAudioFile.wavFileMetaData.sampleRate;
		this.nAvgBytesPerSec = nesterAudioFile.wavFileMetaData.nAvgBytesPerSec;
		this.nBlockAlign = nesterAudioFile.wavFileMetaData.nBlockAlign;
		this.bitsPerSample = nesterAudioFile.wavFileMetaData.bitsPerSample;
		this.numBytesToTailPad = nesterAudioFile.wavFileMetaData.numBytesToTailPad;

		this.numFrames = (int) ((nesterAudioFile.wavFileMetaData.dataSize + nesterAudioFile.wavFileMetaData.numBytesToTailPad) / nesterAudioFile.wavFileMetaData.nBlockAlign);

		this.durationInSeconds = (double) nesterAudioFile.wavFileMetaData.dataSize / (double) nesterAudioFile.wavFileMetaData.nAvgBytesPerSec;

	}

	static public String toHeader() {
		StringBuffer sb = new StringBuffer();

		sb.append("filePath\t");
		sb.append("fileSize\t");
		sb.append("isWAV\t");
		sb.append("validSize\t");
		sb.append("dataStartIndex\t");
		sb.append("dataSize\t");
		sb.append("wFormatTag\t");
		sb.append("numChannels\t");
		sb.append("sampleRate\t");
		sb.append("nAvgBytesPerSec\t");
		sb.append("nBlockAlign\t");
		sb.append("bitsPerSample\t");
		sb.append("numBytesToTailPad\t");
		sb.append("numFrames\t");
		sb.append("durationInSeconds");

		return sb.toString();

	}

	public String toString() {

		StringBuffer sb = new StringBuffer();

		sb.append(filePath + "\t");
		sb.append(fileSize + "\t");
		sb.append(isWAV + "\t");
		sb.append(validRIFFsize + "\t");
		sb.append(validDATAsize + "\t");
		sb.append(dataStartIndex + "\t");
		sb.append(dataSize + "\t");
		sb.append(wFormatTag + "\t");
		sb.append(numChannels + "\t");
		sb.append(sampleRate + "\t");
		sb.append(nAvgBytesPerSec + "\t");
		sb.append(nBlockAlign + "\t");
		sb.append(bitsPerSample + "\t");
		sb.append(numBytesToTailPad + "\t");
		sb.append(numFrames + "\t");
		sb.append(durationInSeconds);

		return sb.toString();

	}

	public static void main(String[] args) {
//		new AudioFile("/data/user-files/ThreeLives/ThreeLives.wav");
	}
}
