package arlo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Vector;

public class NesterSupervisedTagDiscoveryBias extends TagDiscoveryBias {

	Vector<Integer> sourceTagSets;
	int destinationTagSetId;

	public int numberOfTagsToDiscover;

	// REPRESENTATION BIAS
	public double spectraWeight;
	public double pitchWeight;
	public double averageEnergyWeight;
	public int numExemplars;

	// SELECTION BIAS
	public double maxOverlapFraction;
	public double minPerformance;
	
	NesterSupervisedTagDiscoveryBias() {
		this.id = 0;
	}
	
	NesterSupervisedTagDiscoveryBias(int id, int user_id, int project_id, boolean isRunning, boolean isComplete,  int numCompleted, double fractionCompleted, String name, 
			Vector<Integer> sourceTagSets, int destinationTagSetId, 
			int numberOfTagsToDiscover, int numFrequencyBands, double numTimeFramesPerSecond, double dampingRatio, double minFrequency, double maxFrequency, double spectraWeight,
			double pitchWeight, double averageEnergyWeight, int numExemplars, double maxOverlapFraction, double minPerformance) {

		this.id = id;
		this.user_id = user_id;
		this.project_id = project_id;
		this.sourceTagSets = sourceTagSets;
		this.destinationTagSetId = destinationTagSetId;
		this.isRunning = isRunning;
		this.isComplete = isComplete;
		this.numCompleted = numCompleted;
		this.fractionCompleted = fractionCompleted;
		this.name = name;

		this.numberOfTagsToDiscover = numberOfTagsToDiscover;

		this.numFrequencyBands = numFrequencyBands;
		this.numTimeFramesPerSecond = numTimeFramesPerSecond;
		this.dampingRatio = dampingRatio;
		this.minFrequency = minFrequency;
		this.maxFrequency = maxFrequency;

		this.spectraWeight = spectraWeight;
		this.pitchWeight = pitchWeight;
		this.averageEnergyWeight = averageEnergyWeight;
		this.numExemplars = numExemplars;

		this.maxOverlapFraction = maxOverlapFraction;
		this.minPerformance = minPerformance;

	}


	public boolean getNesterSupervisedTagDiscoveryBias(int id, Connection connection) {

		Logger logger;
		logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		/////////////////
		// get Job info

		if (! this.getNesterJob(id, connection)) {
			logger.log(Level.WARNING, "Failed to retrieve job");
			return false;
		}
		
		
		//////////////////////
		// Get Job Parameters
		
		logger.log(Level.INFO, "Get Job Params");

		boolean gotParameters = true;
		String value;

		value = this.GetJobParameter("sourceTagSets");
		if (value != null) {
			this.sourceTagSets = new Vector<Integer>();
			// sourceTagSets is a comma separated string
			// split and create a Vector of values
			for (String ts : value.split(",")) {
				this.sourceTagSets.add(Integer.parseInt(ts.trim()));
			}
		} else {
			gotParameters = false;
		}
		
		value = this.GetJobParameter("destinationTagSet");
		if (value != null) {
			this.destinationTagSetId = Integer.parseInt(value);
		} else {
			gotParameters = false;
		}

		value = this.GetJobParameter("numberOfTagsToDiscover");
		if (value != null) {
			this.numberOfTagsToDiscover = Integer.parseInt(value);
		} else {
			gotParameters = false;
		}
		
		value = this.GetJobParameter("numFrequencyBands");
		if (value != null) {
			this.numFrequencyBands = Integer.parseInt(value);
		} else {
			gotParameters = false;
		}
		
		value = this.GetJobParameter("numTimeFramesPerSecond");
		if (value != null) {
			this.numTimeFramesPerSecond = Double.parseDouble(value);
		} else {
			gotParameters = false;
		}
		
		value = this.GetJobParameter("dampingRatio");
		if (value != null) {
			this.dampingRatio = Double.parseDouble(value);
		} else {
			gotParameters = false;
		}
		
		value = this.GetJobParameter("minFrequency");
		if (value != null) {
			this.minFrequency = Double.parseDouble(value);
		} else {
			gotParameters = false;
		}
		
		value = this.GetJobParameter("maxFrequency");
		if (value != null) {
			this.maxFrequency = Double.parseDouble(value);
		} else {
			gotParameters = false;
		}
		
		value = this.GetJobParameter("spectraWeight");
		if (value != null) {
			this.spectraWeight = Double.parseDouble(value);
		} else {
			gotParameters = false;
		}
		
		value = this.GetJobParameter("pitchWeight");
		if (value != null) {
			this.pitchWeight = Double.parseDouble(value);
		} else {
			gotParameters = false;
		}
		
		value = this.GetJobParameter("averageEnergyWeight");
		if (value != null) {
			this.averageEnergyWeight = Double.parseDouble(value);
		} else {
			gotParameters = false;
		}

		value = this.GetJobParameter("numExemplars");
		if (value != null) {
			this.numExemplars = Integer.parseInt(value);
		} else {
			gotParameters = false;
		}
		
		value = this.GetJobParameter("maxOverlapFraction");
		if (value != null) {
			this.maxOverlapFraction = Double.parseDouble(value);
		} else {
			gotParameters = false;
		}

		value = this.GetJobParameter("minPerformance");
		if (value != null) {
			this.minPerformance = Double.parseDouble(value);
		} else {
			gotParameters = false;
		}
		
		if (gotParameters == false) {
			logger.log(Level.INFO, "Not all Job Parameters were found");
			return false;
		}

		return true;
		
	}

	/** \brief Construct a NesterSupervisedTagDiscoveryBias from a 'QueueRunner' task.
	* 
	* This version is for the QueueRunner version of NesterSupervisedTagDiscoveryBias. 
	* This pulls data from both the child Task and the parent Task from the QueueRunner entries.
	* 'this' is the child job, and it will pull in the settings from the parent.
	* @param childJob The NesterJob object that represents the  child task from the QueueRunner task.
	* @param connection Database connection
	* @return initialized NesterAudioFile
	*/

	public boolean getNesterSupervisedTagDiscoveryBias2(int id, Connection connection) {

		Logger logger;
		logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		/////////////////
		// get Job info

		// child job
		if (! this.getNesterJob(id, connection)) {
			logger.log(Level.WARNING, "Failed to retrieve child job");
			return false;
		}
		
		if (this.parentJob_id == null) {
			logger.log(Level.WARNING, "parentJob_id is null");
			return false;
		}

		// parent job
		NesterJob parentJob = new NesterJob();

		if (! parentJob.getNesterJob(this.parentJob_id, connection)) {
			logger.log(Level.WARNING, "Failed to retrieve parent job");
			return false;
		}
		
		//////////////////////
		// Get Job Parameters
		
		logger.log(Level.INFO, "Get Job Params");

		boolean gotParameters = true;
		String value;

		value = parentJob.GetJobParameter("sourceTagSets");
		if (value != null) {
			this.sourceTagSets = new Vector<Integer>();
			// sourceTagSets is a comma separated string
			// split and create a Vector of values
			for (String ts : value.split(",")) {
				this.sourceTagSets.add(Integer.parseInt(ts));
			}
		} else {
			gotParameters = false;
		}
		
		value = parentJob.GetJobParameter("destinationTagSet");
		if (value != null) {
			this.destinationTagSetId = Integer.parseInt(value);
		} else {
			gotParameters = false;
		}

		value = parentJob.GetJobParameter("numberOfTagsToDiscover");
		if (value != null) {
			this.numberOfTagsToDiscover = Integer.parseInt(value);
		} else {
			gotParameters = false;
		}
		
		value = parentJob.GetJobParameter("numFrequencyBands");
		if (value != null) {
			this.numFrequencyBands = Integer.parseInt(value);
		} else {
			gotParameters = false;
		}
		
		value = parentJob.GetJobParameter("numTimeFramesPerSecond");
		if (value != null) {
			this.numTimeFramesPerSecond = Double.parseDouble(value);
		} else {
			gotParameters = false;
		}
		
		value = parentJob.GetJobParameter("dampingRatio");
		if (value != null) {
			this.dampingRatio = Double.parseDouble(value);
		} else {
			gotParameters = false;
		}
		
		value = parentJob.GetJobParameter("minFrequency");
		if (value != null) {
			this.minFrequency = Double.parseDouble(value);
		} else {
			gotParameters = false;
		}
		
		value = parentJob.GetJobParameter("maxFrequency");
		if (value != null) {
			this.maxFrequency = Double.parseDouble(value);
		} else {
			gotParameters = false;
		}
		
		value = parentJob.GetJobParameter("spectraWeight");
		if (value != null) {
			this.spectraWeight = Double.parseDouble(value);
		} else {
			gotParameters = false;
		}
		
		value = parentJob.GetJobParameter("pitchWeight");
		if (value != null) {
			this.pitchWeight = Double.parseDouble(value);
		} else {
			gotParameters = false;
		}
		
		value = parentJob.GetJobParameter("averageEnergyWeight");
		if (value != null) {
			this.averageEnergyWeight = Double.parseDouble(value);
		} else {
			gotParameters = false;
		}

		value = parentJob.GetJobParameter("numExemplars");
		if (value != null) {
			this.numExemplars = Integer.parseInt(value);
		} else {
			gotParameters = false;
		}

		value = parentJob.GetJobParameter("maxOverlapFraction");
		if (value != null) {
			this.maxOverlapFraction = Double.parseDouble(value);
		} else {
			gotParameters = false;
		}

		value = parentJob.GetJobParameter("minPerformance");
		if (value != null) {
			this.minPerformance = Double.parseDouble(value);
		} else {
			gotParameters = false;
		}
		
		if (gotParameters == false) {
			logger.log(Level.INFO, "Not all Job Parameters were found");
			return false;
		}

		return true;
		
	}


}
