package arlo; 

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.StringWriter;
import java.io.PrintWriter;

import com.amd.aparapi.Kernel;
import com.amd.aparapi.Range;

import adapt.IO;
import adapt.Utility;

import java.sql.Connection;


public class SupervisedTagDiscovery extends TagDiscovery {

////
// Debug Job Launcher

//	public static void main(String[] args) {
//
//		ServiceHead serviceHead = new ServiceHead();
//
//		serviceHead.initialize();
//
//		if (false) {
//			SupervisedTagDiscovery upervisedTagDiscovery = new SupervisedTagDiscovery(serviceHead, 3421);
//
//			upervisedTagDiscovery.run();
//
//		}
//
//		PyDictionary pyDictionary = new PyDictionary();
//
//		pyDictionary.put("audioFilePath", "/data/workspace/nester/media/files/user-files/PennSound/wav/McClure-Michael_10_Intro-Ghost-Tantras_Ghost-Tantras_Rockdrill-9_2005.wav");
//		pyDictionary.put("showTimeScale", "False");
//		pyDictionary.put("timeScaleHeight", "0");
//		pyDictionary.put("catalogSpectraNumFrequencyBands", "256");
//		pyDictionary.put("endTime", "59.1379128729");
//		pyDictionary.put("spectraMaximumBandFrequency", "12000.0");
//		pyDictionary.put("spectraNumFramesPerSecond", "100.0");
//		pyDictionary.put("spectraNumFrequencyBands", "64");
//		pyDictionary.put("spectraSimpleNormalization", "True");
//		pyDictionary.put("spectraMinimumBandFrequency", "60.0");
//		pyDictionary
//				.put("audioSegmentFilePath",
//						"/data/workspace/nester/media/files/user-files/PennSound/wav/cache/segments/McClure-Michael_10_Intro-Ghost-Tantras_Ghost-Tantras_Rockdrill-9_2005.wavPoetryIs1459945.segment.wav");
//		pyDictionary.put("userSettingsID", "37");
//		pyDictionary.put("showFrequencyScale", "False");
//		pyDictionary.put("gain", "1.0");
//		pyDictionary.put("startTime", "52.0179128729");
//		pyDictionary.put("action", "createSpectra");
//		pyDictionary.put("spectraDampingFactor", "0.02");
//		pyDictionary.put("spectraBorderWidth", "0");
//		pyDictionary.put("spectraTopBorderHeight", "0");
//		pyDictionary
//				.put("imageFilePath",
//						"/data/workspace/nester/media/files/user-files/PennSound/wav/cache/jpgs/McClure-Michael_10_Intro-Ghost-Tantras_Ghost-Tantras_Rockdrill-9_2005.wavPoetryIs1459945.image-s52.0179128729e59.1379128729g1.0mi60.0ma12000.0fb64fps100.0df0.02ssTrueswFalse.jpg");
//
//		serviceHead.callMe(pyDictionary);
//
//	}

	final static boolean gpuDebugMode = false;

	// Random randomizedAudioFileIndicesRandom = new Random();
	Random randomizedAudioFileIndicesRandom = new Random(123);

	int cpuGPUThreshold = 0;
	// int cpuGPUThreshold = 999999999;
	boolean useDB = true;

	// boolean randomExemplars = true;
	boolean randomExemplars = false; // !!! true

	NesterTag[] userTagArray;
	int problemSolverAudioFileTimeIndex;
	NesterSupervisedTagDiscoveryBias nesterSupervisedTagDiscoveryBias;

	double allSeedTagScores[][];
	double seedTagScores[];

	ServiceHead serviceHead;

	/** If set, then override and search only through these file IDs */
	Vector<Integer> singleFileModeIdsVector; 

	/** If set, only search within existing Tags in these classes */
	Vector<Integer> searchWithinTagClassesVector;

	/** If set, only save the best 'N' Tags per file */
	Integer saveBestNTags;

	/** If set, only save the best 'N' Tags per TagClass per file */
	Integer saveBestNTagsPerClass;

	/** \brief Original callMe launched Constructor.
	* 
	* This is the original 'single job' style launch. The one job will process 
	* all mediaFiles in one go. This is the original version before the 
	* QueueTask_SupervisedTagDiscovery.
	* @param serviceHead Reference to the ServiceHead that launched this job. 
	* Contains an opened database 'connection'.
	* @param tagDiscoveryBiasID Database ID of the Job to launch.
	*/

	SupervisedTagDiscovery(ServiceHead serviceHead, int tagDiscoveryBiasID) {
		super(tagDiscoveryBiasID);
		this.serviceHead = serviceHead;
	}

	/** \brief Constructor for new QueueTask launcher.
	* 
	* This is for the new QueueTask_SupervisedTagDiscovery style jobs.
	* @param tagDiscoveryBiasID Database ID of the Job to launch.
	*/

	SupervisedTagDiscovery(int tagDiscoveryBiasID) {
		super(tagDiscoveryBiasID);
		this.serviceHead = null;
	}

	/** Check if this job has been queued to stop by the user. 
	 * 
	 * This is currently only implemented if the job is called by ServiceHead, 
	 * which maintains a list of jobs to stop.
	 * @return True if this Job should stop, False otherwise.
	 */

	public boolean shouldStop() {
		if (this.serviceHead != null) {
			if (this.serviceHead.stoppedJobIDs.contains(nesterSupervisedTagDiscoveryBias.id)) {
				return true;  // return gracefully
			}
		}
		return false;
	}

	/** Launch the job when running threaded from ServiceHead. 
	 * 
	 * Used to start a thread when called by ServiceHead to start 
	 * a job. Not used by QueueTask_SupervisedTagDiscovery. Note that 
	 * this function is an override inherited from Thread.
	 */

	public void run() {
		Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
		Connection connection = serviceHead.connection;

		// Get the job data from the database
		nesterSupervisedTagDiscoveryBias = new NesterSupervisedTagDiscoveryBias();
		if (!nesterSupervisedTagDiscoveryBias.getNesterSupervisedTagDiscoveryBias(tagDiscoveryBias.id, connection)) {
			logger.log(Level.SEVERE, "Could not get nesterSupervisedTagDiscoveryBias");
			return;
		}

		// get the Project data
		tagDiscoveryBias = (TagDiscoveryBias) nesterSupervisedTagDiscoveryBias;

		// get the files through which to search (inherited from TagDiscovery)
		runTagDiscoveryStart(tagDiscoveryBias.project_id, connection);

		// disable single-file mode
		singleFileModeIdsVector = null;

		// disable 'Advanced' options by default
		searchWithinTagClassesVector = null;
		saveBestNTags = null;
		saveBestNTagsPerClass = null;

		runJob(nesterSupervisedTagDiscoveryBias, connection);
	}

	/** Launch the job when running from QueueTask_SupervisedTagDiscovery.
	 * 
	 * @param connection An opened database connection.
	 * @return True if success, false if any error occurred. 
	*/

	public boolean QueueTaskRun(Connection connection) {
		Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		// Get the job data from the database
		nesterSupervisedTagDiscoveryBias = new NesterSupervisedTagDiscoveryBias();
		// use version '2' of the function, for separate parent and child jobs
		if (!nesterSupervisedTagDiscoveryBias.getNesterSupervisedTagDiscoveryBias2(tagDiscoveryBias.id, connection)) {
			logger.log(Level.SEVERE, "Could not get nesterSupervisedTagDiscoveryBias");
			return false;
		}

		// get the Project data
		tagDiscoveryBias = (TagDiscoveryBias) nesterSupervisedTagDiscoveryBias;

		////
		// get the parent Job, if it exists
		NesterJob parentJob = null;
		if (tagDiscoveryBias.parentJob_id != null) {
			parentJob = new NesterJob();
			if (! parentJob.getNesterJob(tagDiscoveryBias.parentJob_id, connection)) {
				logger.log(Level.SEVERE, "Could not get parent Job");
				nesterSupervisedTagDiscoveryBias.SaveJobLogMessage("ERROR: Could not get parent Job");
				return false;
			}
		} 

		// get the files through which to search (inherited from TagDiscovery)
//		runTagDiscoveryStart(tagDiscoveryBias.project_id, connection);

        // load all of the Project audio files (inherited from TagDiscovery)
		// note this loads all of the files, but we override which to search with singleFileModeIdsVector
		runTagDiscoveryStart(tagDiscoveryBias.project_id, connection);

		////
		// setup single file mode
		// pull IDs from "mediaFileIds" field - if not found, fall back to "mediaFileId", the 
		// deprecated field

		String value;
		value = nesterSupervisedTagDiscoveryBias.GetJobParameter("mediaFileIds");
		if (value != null) {
			singleFileModeIdsVector = new Vector<Integer>();
			for (String ts : value.split(",")) {
				Integer audioFileId = Integer.parseInt(ts.trim());
				if (audioFileId == null) {
					logger.log(Level.SEVERE, "'mediaFileId' invalid");
					nesterSupervisedTagDiscoveryBias.SaveJobLogMessage("ERROR: 'mediaFileId' invalid");
					return false;
				}
				singleFileModeIdsVector.add(audioFileId);
			}
		} else { // (value != null)
			// fall back to check "mediaFileId"
			logger.log(Level.WARNING, "Didn't find 'mediaFileIds' in childJob - falling back to check for 'mediaFileId'");

			value = nesterSupervisedTagDiscoveryBias.GetJobParameter("mediaFileId");
			if (value == null) {
				logger.log(Level.SEVERE, "Didn't find 'mediaFileId' in childJob");
				nesterSupervisedTagDiscoveryBias.SaveJobLogMessage("ERROR: Didn't find 'mediaFileId' in childJob");
				return false;
			}
			Integer audioFileId = Integer.parseInt(value.trim());
			if (audioFileId == null) {
				logger.log(Level.SEVERE, "'mediaFileId' invalid");
				nesterSupervisedTagDiscoveryBias.SaveJobLogMessage("ERROR 'mediaFileId' invalid");
				return false;
			}
			singleFileModeIdsVector = new Vector<Integer>();
			singleFileModeIdsVector.add(audioFileId);
		}

		/// 
		// Setup restriced searching, if enabled

		// search 'this' Job
		searchWithinTagClassesVector = null;
		value = nesterSupervisedTagDiscoveryBias.GetJobParameter("searchWithinTagClasses");
		if (value != null) {
			searchWithinTagClassesVector = new Vector<Integer>();
			for (String ts : value.split(",")) {
				Integer tagClassId = Integer.parseInt(ts.trim());
				if (tagClassId == null) {
					logger.log(Level.SEVERE, "'searchWithinTagClasses' invalid");
					nesterSupervisedTagDiscoveryBias.SaveJobLogMessage("ERROR: 'searchWithinTagClasses' invalid");
					return false;
				}
				searchWithinTagClassesVector.add(tagClassId);
			}
		}
		
		// search the parent Job ('this' Job has precedence over the Value)
		if ((parentJob) != null && (searchWithinTagClassesVector == null)) {
			value = parentJob.GetJobParameter("searchWithinTagClasses");
			if (value != null) {
				if (searchWithinTagClassesVector == null) 
					searchWithinTagClassesVector = new Vector<Integer>();
				for (String ts : value.split(",")) {
					Integer tagClassId = Integer.parseInt(ts.trim());
					if (tagClassId == null) {
						logger.log(Level.SEVERE, "'searchWithinTagClasses' parent invalid");
						nesterSupervisedTagDiscoveryBias.SaveJobLogMessage("ERROR: 'searchWithinTagClasses' parent invalid");
						return false;
					}
					searchWithinTagClassesVector.add(tagClassId);
				}
			} 
		}
		

		// //
		// Whether to enable "Save Best N" Tags

		// search 'this' Job
		saveBestNTags = null;
		value = nesterSupervisedTagDiscoveryBias.GetJobParameter("saveBestNTags");
		if (value != null) {
			saveBestNTags = Integer.parseInt(value.trim());
			if (saveBestNTags == null) {
				logger.log(Level.SEVERE, "'saveBestNTags' invalid");
				nesterSupervisedTagDiscoveryBias.SaveJobLogMessage("ERROR: 'saveBestNTags' invalid");
				return false;
			}
		}
		
		// search the parent Job ('this' Job has precedence over the Value)
		if ((parentJob != null) && (saveBestNTags == null)) {
			value = parentJob.GetJobParameter("saveBestNTags");
			if (value != null) {
				saveBestNTags = Integer.parseInt(value.trim());
				if (saveBestNTags == null) {
					logger.log(Level.SEVERE, "'saveBestNTags' invalid");
					nesterSupervisedTagDiscoveryBias.SaveJobLogMessage("ERROR: 'saveBestNTags' invalid");
					return false;
				}
			} 
		}

		// //
		// Whether to enable "Save Best N" Tags Per Class

		// search 'this' Job
		saveBestNTagsPerClass = null;
		value = nesterSupervisedTagDiscoveryBias.GetJobParameter("saveBestNTagsPerClass");
		if (value != null) {
			saveBestNTagsPerClass = Integer.parseInt(value.trim());
			if (saveBestNTagsPerClass == null) {
				logger.log(Level.SEVERE, "'saveBestNTagsPerClass' invalid");
				nesterSupervisedTagDiscoveryBias.SaveJobLogMessage("ERROR: 'saveBestNTagsPerClass' invalid");
				return false;
			}
		}
		
		// search the parent Job ('this' Job has precedence over the Value)
		if ((parentJob != null) && (saveBestNTagsPerClass == null)) {
			value = parentJob.GetJobParameter("saveBestNTagsPerClass");
			if (value != null) {
				saveBestNTagsPerClass = Integer.parseInt(value.trim());
				if (saveBestNTagsPerClass == null) {
					logger.log(Level.SEVERE, "'saveBestNTagsPerClass' invalid");
					nesterSupervisedTagDiscoveryBias.SaveJobLogMessage("ERROR: 'saveBestNTagsPerClass' invalid");
					return false;
				}
			} 
		}


		////
		// run forrest run

		try {
			return runJob(nesterSupervisedTagDiscoveryBias, connection);
		} catch (Exception e) {
			StringWriter writer = new StringWriter();
			PrintWriter printWriter = new PrintWriter( writer );
			e.printStackTrace( printWriter );
			printWriter.flush();
			String stackTrace = writer.toString();

			logger.log(Level.SEVERE, "Unknown error encountered running SupervisedTagDiscovery::runJob: " + e + "\n" + stackTrace);
			nesterSupervisedTagDiscoveryBias.SaveJobLogMessage("ERROR: Unknown error encountered running SupervisedTagDiscovery::runJob: " + e + "\n" + stackTrace);
			return false;
		}

	}

	/** Run the Supervised Tag Discovery.
	* 
	* Can be called from multiple locations. Removed from ServiceHead 
	* so that it can be called from QueueTask_SupervisedTagDiscovery.
	* @param nesterSupervisedTagDiscoveryBias 
	* @param connection An opened database connection.
	* @return True if completed or stopped, False if any error encountered.
	*/

	public boolean runJob(
				NesterSupervisedTagDiscoveryBias nesterSupervisedTagDiscoveryBias, 
				Connection connection) 
	{
		Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		logger.log(Level.INFO, "Starting " + this.getClass());
		logger.log(Level.INFO, "tagDiscoveryBiasID = " + tagDiscoveryBias.id);

		nesterSupervisedTagDiscoveryBias.SaveJobLogMessage("Starting SupervisedTagDiscovery on " + ArloSettings.hostname);

		/***********************************************************/
		/* Get Data From the Database and Build Up Data Structures */
		/***********************************************************/


		HashMap<Integer, String> tagClassIDToName = new HashMap<Integer, String>();
		HashMap<Integer, Double> tagClassIDToFocusFactor = new HashMap<Integer, Double>();
		HashMap<Integer, Double> tagClassIDToMatchQuality = new HashMap<Integer, Double>();

		// Get the TagClasses and params from the JobParameters

		// use this job, or a parent ?
		NesterJob paramsJob = null;
		if (nesterSupervisedTagDiscoveryBias.parentJob_id == null) {
			paramsJob = nesterSupervisedTagDiscoveryBias;
		} else {
			// Get the Parent Job
			NesterJob parentJob = new NesterJob();
			if (! parentJob.getNesterJob(nesterSupervisedTagDiscoveryBias.parentJob_id, connection)) {
				logger.log(Level.SEVERE, "Failed to retrieve parent job");
				nesterSupervisedTagDiscoveryBias.SaveJobLogMessage("ERROR: Failed to retrieve parent job");
				return false;
			}
			paramsJob = parentJob;
		}

		// get the TagClasses of the Project - we just use these to get the Class Names
		Vector<NesterTagClass> allTagSetClasses = NesterTagClass.getNesterProjectTagClasses(tagDiscoveryBias.project_id, connection);

		////
		// Get the TagClass Parameters

		Vector<String> tagClassParameters = paramsJob.GetJobParameterVector("searchTagClass");
		if (tagClassParameters.size() == 0) {
			logger.log(Level.SEVERE, "No TagClasses Found in JobParameters - bailing out");
			nesterSupervisedTagDiscoveryBias.SaveJobLogMessage("No TagClasses Found in JobParameters - bailing out");
			return false;
		}

		for (Iterator<String> iterator = tagClassParameters.iterator(); iterator.hasNext();) {
			String searchTagClassEntry = iterator.next();

			// split into tagClassId, focusFactor, minimumMatchQuality
			String[] splitArray = searchTagClassEntry.split(",");
			if (
					splitArray.length != 3 ||
					splitArray[0].isEmpty() ||
					splitArray[1].isEmpty() ||
					splitArray[2].isEmpty())
			{
				logger.log(Level.SEVERE, "Invalid Job Parameter Format: \"" + searchTagClassEntry + "\"");
				nesterSupervisedTagDiscoveryBias.SaveJobLogMessage("Invalid Job Parameter Format: \"" + searchTagClassEntry + "\"");
				return false;
			}

			String sTagClassId          = splitArray[0];
			String sFocusFactor         = splitArray[1];
			String sMinimumMatchQuality = splitArray[2];

			try {
				Integer tagClassId = Integer.parseInt(sTagClassId);
				tagClassIDToFocusFactor.put(tagClassId, Double.parseDouble(sFocusFactor));
				tagClassIDToMatchQuality.put(tagClassId, Double.parseDouble(sMinimumMatchQuality));

				// find the className
				// - iterate over all of the Projects classes looking for a match
				boolean classNameFound = false;
				for (Iterator<NesterTagClass> tagClassIterator = allTagSetClasses.iterator(); tagClassIterator.hasNext();) {
					NesterTagClass nesterTagClass = tagClassIterator.next();
					if (nesterTagClass.id == tagClassId) {
						tagClassIDToName.put(tagClassId, "");
						classNameFound = true;
						break;
					}
				}
				if (! classNameFound) {
					logger.log(Level.SEVERE, "Failure Parsing Job Parameter - Name Not Found: \"" + searchTagClassEntry + "\"");
				}

			} catch (Exception e) {
				logger.log(Level.SEVERE, "Failure Parsing Job Parameter Format: \"" + searchTagClassEntry + "\"");
				logger.log(Level.SEVERE, "Exception = " + e);
				nesterSupervisedTagDiscoveryBias.SaveJobLogMessage("Failure Parsing Job Parameter Format: \"" + searchTagClassEntry + "\" - Exception = " + e);
				return false;
			}

		}
			

		// verify all Maps are the same length
		if (tagClassIDToName.size() != tagClassIDToFocusFactor.size() || 
				tagClassIDToName.size() != tagClassIDToMatchQuality.size()) {
			String s = "Mismatch in Map Lengths -" + 
					" tagClassIDToName: " + tagClassIDToName.size() + 
					" tagClassIDToFocusFactor: " + tagClassIDToFocusFactor.size() + 
					" tagClassIDToMatchQuality: " + tagClassIDToMatchQuality.size();
			logger.log(Level.SEVERE, s);
			nesterSupervisedTagDiscoveryBias.SaveJobLogMessage(s);
			return false;
		}

		// //
		// report focus factors and match quality constraints

		boolean reportFocusFactorsAndMatchQuality = true;
		if (reportFocusFactorsAndMatchQuality) {

			logger.log(Level.INFO, "---------------------");
			logger.log(Level.INFO, "classID, className, focusFactor, matchQuality");

			Set<Integer> tagClassIdSet = tagClassIDToName.keySet();
			for (Iterator<Integer> iterator = tagClassIdSet.iterator(); iterator.hasNext();) {
				Integer classId = iterator.next();
				logger.log(Level.INFO,
					classId + ", " +                               // classId
					tagClassIDToName.get(classId) + ", " +         // className
					tagClassIDToFocusFactor.get(classId) + ", " +  // focusFactor
					tagClassIDToMatchQuality.get(classId));        // matchQuality
			}
			logger.log(Level.INFO, "---------------------");
		}


		// //
		// Audio Files

		tagDiscoveryNumAudioFiles = tagDiscoveryNesterAudioFiles.size();
		logger.log(Level.INFO, "### nesterAudioFiles.size() = " + tagDiscoveryNumAudioFiles);

		Hashtable<Integer, Integer> audioFileIDToIndex = new Hashtable<Integer, Integer>();

		for (int audioFileindex = 0; audioFileindex < tagDiscoveryNumAudioFiles; audioFileindex++) {
			NesterAudioFile nesterAudioFile = tagDiscoveryNesterAudioFiles.get(audioFileindex);
			audioFileIDToIndex.put(nesterAudioFile.id, audioFileindex);
		}

		// //
		// get all tag examples from DB

		Vector<NesterTag> originalAllTags = NesterTagSet.getNesterTagSetTags(nesterSupervisedTagDiscoveryBias.sourceTagSets, singleFileModeIdsVector, connection);
		Vector<NesterTag> allTags = originalAllTags;
		logger.log(Level.INFO, "### allTags.size() = " + allTags.size());
		logger.log(Level.INFO, "### end getNesterTagSetTags ###");

		// //
		// filter out tags that don't meet criteria
		// associate tag examples to audio file indices, to index into spectra memory map file

		Vector<NesterTag> newAllTags = new Vector<NesterTag>();
// TODO Filter out tags that aren't in the searchTagClass list
// perhaps do this via the DB load call?
		for (int userTagIndex = 0; userTagIndex < allTags.size(); userTagIndex++) {
			// this iteration's tag
			NesterTag nesterTag = allTags.get(userTagIndex);
			// audio File index this tag is in
			int audioFileindex = audioFileIDToIndex.get(nesterTag.audioFile_id);
			// audio file this tag is within
			NesterAudioFile nesterAudioFile = tagDiscoveryNesterAudioFiles.get(audioFileindex);

			// //
			// Run various checks against the tags


			// if (firstTag.strength != 1.0) {
			// continue;
			// }

			// check if invalid Class
			if (tagClassIDToFocusFactor.get(nesterTag.tagClass_id) == null)
				continue;

			// check Focus Factor strength
			if (tagClassIDToFocusFactor.get(nesterTag.tagClass_id) != 1.0)
				continue;

			// check if tag end time is invalid
			if (nesterTag.endTime > nesterAudioFile.wavFileMetaData.durationInSeconds) {
				if (false) // !!! the tag is compared against each audio file, so it may generate a lot of false warnings
					logger.log(Level.INFO, "(nesterTag.endTime > nesterAudioFile.wavFileMetaData.durationInSeconds)");
				continue;
			}

			// set Tag settings and add to search list, and
			// if (nesterTag.audioFile_id == nesterAudioFile.id) {

			nesterTag.audioFileIndex = audioFileindex;
			nesterTag.nesterAudioFile = nesterAudioFile;

			newAllTags.add(nesterTag);
		}

		allTags = newAllTags;

		logger.log(Level.INFO, "### (new) allTags.size() = " + allTags.size());

		// filter out tags that don't meet criteria
		// //

		// //
		// adjust tag start and end times based on context size //
		// (search a bit before and after the tag)
// TODO eventually make this a user selected parameter

		double timeContextSize = 0.0; // !!! was 0.1 previously

		if (timeContextSize != 0.0) {
			logger.log(Level.INFO, "adjusting tag start and end times based on context size");
			
			newAllTags = new Vector<NesterTag>();

			for (Iterator<NesterTag> iterator = allTags.iterator(); iterator.hasNext();) {

				NesterTag nesterTag = (NesterTag) iterator.next();
				NesterAudioFile  nesterAudioFile = tagDiscoveryNesterAudioFiles.get(nesterTag.audioFileIndex);

				if ((nesterTag.startTime - timeContextSize >= 0.0) && (nesterTag.endTime + timeContextSize <= nesterAudioFile.wavFileMetaData.durationInSeconds)) {

					nesterTag.startTime -= timeContextSize;
					nesterTag.endTime += timeContextSize;

					newAllTags.add(nesterTag);
				}
			}

			allTags = newAllTags;

			logger.log(Level.INFO, "### (new re-sized) allTags.size() = " + allTags.size());
		}


		/**********************************************/
		/* create vector of user tags for all classes */
		/**********************************************/

		HashMap<Integer, Vector<NesterTag>> tagClassToUserTagVector = new HashMap<Integer, Vector<NesterTag>>();
		HashMap<Integer, Vector<NesterTag>> tagClassToAllTagVector = new HashMap<Integer, Vector<NesterTag>>();
		ArrayList<Integer> allTagClassIDs = new ArrayList<Integer>();

		for (int userTagIndex = 0; userTagIndex < allTags.size(); userTagIndex++) {

			NesterTag nesterTag = allTags.get(userTagIndex);

			nesterTag.durationInSeconds = (nesterTag.endTime - nesterTag.startTime);

			{
				Vector<NesterTag> classAllTagVector = tagClassToAllTagVector.get(nesterTag.tagClass_id);
				if (classAllTagVector == null) {
					classAllTagVector = new Vector<NesterTag>();
					tagClassToAllTagVector.put(nesterTag.tagClass_id, classAllTagVector);
					allTagClassIDs.add(nesterTag.tagClass_id);
				}
				classAllTagVector.add(nesterTag);
			}

			if (nesterTag.machineTagged)
				continue;

			{
				Vector<NesterTag> classUserTagVector = tagClassToUserTagVector.get(nesterTag.tagClass_id);
				if (classUserTagVector == null) {
					classUserTagVector = new Vector<NesterTag>();
					tagClassToUserTagVector.put(nesterTag.tagClass_id, classUserTagVector);
					allTagClassIDs.add(nesterTag.tagClass_id);
				}
				classUserTagVector.add(nesterTag);
			}

		}

		/******************************************************************/
		/* count total number of user tags and report counts by tag class */
		/******************************************************************/

		{
			int numClasses = tagClassToAllTagVector.size();
			logger.log(Level.INFO, "### All numClasses = " + numClasses);

			Set<Integer> set = tagClassToAllTagVector.keySet();

			int numClassAllTags = 0;
			for (Iterator<Integer> iterator = set.iterator(); iterator.hasNext();) {

				Integer integer = iterator.next();

				Vector<NesterTag> classTagVector = tagClassToAllTagVector.get(integer);
				logger.log(Level.INFO, "### All classID = " + integer + " size = " + classTagVector.size());

				numClassAllTags += classTagVector.size();
			}

			logger.log(Level.INFO, "### All  numClassAllTags = " + numClassAllTags);

		}

		{
			int numClasses = tagClassToUserTagVector.size();
			logger.log(Level.INFO, "### user numClasses = " + numClasses);

			Set<Integer> set = tagClassToUserTagVector.keySet();

			int numClassUserTags = 0;
			for (Iterator<Integer> iterator = set.iterator(); iterator.hasNext();) {

				Integer integer = iterator.next();

				Vector<NesterTag> classTagVector = tagClassToUserTagVector.get(integer);
				logger.log(Level.INFO, "### user classID = " + integer + " size = " + classTagVector.size());

				numClassUserTags += classTagVector.size();
			}

			logger.log(Level.INFO, "### user  numClassUserTags = " + numClassUserTags);

		}


		/******************************************************************/
		/*              Build the SearchWithin Tag Vector                 */
		/******************************************************************/
		
		Vector<NesterTag> searchWithinTagVector = null;
		if (searchWithinTagClassesVector != null) {
			searchWithinTagVector = new Vector<NesterTag>();

			for (int tagIndex = 0; tagIndex < originalAllTags.size(); tagIndex++) {
				NesterTag nesterTag = originalAllTags.get(tagIndex);
				if (searchWithinTagClassesVector.contains(nesterTag.tagClass_id)) {
					searchWithinTagVector.add(nesterTag);
				}
			}
		}


		// #########################################################################################################################################################################################################################################################################################
		// #########################################################################################################################################################################################################################################################################################
		// #########################################################################################################################################################################################################################################################################################
		// #########################################################################################################################################################################################################################################################################################
		// #########################################################################################################################################################################################################################################################################################
		// #########################################################################################################################################################################################################################################################################################
		// #########################################################################################################################################################################################################################################################################################
		// #########################################################################################################################################################################################################################################################################################
		// #########################################################################################################################################################################################################################################################################################
		// #########################################################################################################################################################################################################################################################################################

		if (false) {
			int numClasses = tagClassToAllTagVector.size();
			logger.log(Level.INFO, "### numClasses = " + numClasses);

			int numExamples = 0;
			Set<Integer> tagClassIDKeySet = null;

			tagClassIDKeySet = tagClassToAllTagVector.keySet();
			logger.log(Level.INFO, "### tagClassToAllTagVector.size() = " + tagClassToAllTagVector.size());

			for (Iterator<Integer> iterator = tagClassIDKeySet.iterator(); iterator.hasNext();) {
				Integer tagClassID = iterator.next();

				Vector<NesterTag> classTagVector = tagClassToAllTagVector.get(tagClassID);
				logger.log(Level.INFO, "### classTagVector.size() = " + classTagVector.size());

				numExamples += classTagVector.size();
			}

			int[] exampleAudioFileIndices = new int[numExamples];

			NesterTag[] exampleTags = new NesterTag[numExamples];

			double[] exampleStartTimes = new double[numExamples];
			double[] exampleEndTimes = new double[numExamples];

			int[] exampleStartFrameIndices = new int[numExamples];
			int[] exampleNumFrames = new int[numExamples];
			int[] exampleStartFreqIndex = new int[numExamples];
			int[] exampleEndFreqIndex = new int[numExamples];

			double[] exampleSpectraAveragePitches = new double[numExamples];

			tagClassIDKeySet = tagClassToAllTagVector.keySet();

			BandPass bandPass = new BandPass();

			bandPass.setNumBands(nesterSupervisedTagDiscoveryBias.numFrequencyBands);
			bandPass.setMinBandFrequency(nesterSupervisedTagDiscoveryBias.minFrequency);
			bandPass.setMaxBandFrequency(nesterSupervisedTagDiscoveryBias.maxFrequency);
			bandPass.initialize();

			int exampleIndex = 0;
			for (Iterator<Integer> tagClassIterator = tagClassIDKeySet.iterator(); tagClassIterator.hasNext();) {
				Integer tagClassID = tagClassIterator.next();

				Vector<NesterTag> classTagVector = tagClassToAllTagVector.get(tagClassID);

				for (Iterator<NesterTag> tagIterator = classTagVector.iterator(); tagIterator.hasNext();) {

					NesterTag nesterTag = tagIterator.next();

					exampleStartTimes[exampleIndex] = nesterTag.startTime;
					exampleEndTimes[exampleIndex] = nesterTag.endTime;

					int tagStartFrame = (int) (nesterTag.startTime * nesterSupervisedTagDiscoveryBias.numTimeFramesPerSecond);
					int tagEndFrame = (int) (nesterTag.endTime * nesterSupervisedTagDiscoveryBias.numTimeFramesPerSecond);

					int numFrames = tagEndFrame - tagStartFrame + 1;

					exampleAudioFileIndices[exampleIndex] = nesterTag.audioFileIndex;
					exampleStartFrameIndices[exampleIndex] = tagStartFrame;
					exampleNumFrames[exampleIndex] = numFrames;

					exampleStartFreqIndex[exampleIndex] = bandPass.getFrequencyIndex(nesterTag.minFrequency);
					exampleEndFreqIndex[exampleIndex] = bandPass.getFrequencyIndex(nesterTag.maxFrequency);

					nesterTag.exampleIndex = exampleIndex;

					exampleTags[exampleIndex] = nesterTag;

					exampleIndex++;
				}
			}

			logger.log(Level.INFO, "### numExamples " + numExamples + "  exampleAudioFileIndices  " + exampleAudioFileIndices + "  exampleStartFrameIndices  "
					+ exampleStartFrameIndices + "  exampleNumFrames  " + exampleNumFrames + "  exampleStartFreqIndex  " + exampleStartFreqIndex + "  exampleEndFreqIndex  "
					+ exampleEndFreqIndex);

			// !!! loadExampleSpectraData(numExamples, exampleAudioFileIndices, exampleStartFrameIndices, exampleNumFrames, exampleStartFreqIndex, exampleEndFreqIndex);
			try {
				computeExampleSpectraAveragePitch(numExamples, exampleAudioFileIndices, exampleStartTimes, exampleEndTimes, exampleStartFreqIndex, exampleEndFreqIndex,
						exampleTags, tagClassIDToName);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		// System.exit(0); // !!!

		// #########################################################################################################################################################################################################################################################################################
		// #########################################################################################################################################################################################################################################################################################
		// #########################################################################################################################################################################################################################################################################################
		// #########################################################################################################################################################################################################################################################################################
		// #########################################################################################################################################################################################################################################################################################
		// #########################################################################################################################################################################################################################################################################################
		// #########################################################################################################################################################################################################################################################################################
		// #########################################################################################################################################################################################################################################################################################
		// #########################################################################################################################################################################################################################################################################################
		// #########################################################################################################################################################################################################################################################################################
		// #########################################################################################################################################################################################################################################################################################

// unused - guess we should remove this
//		HashSet<Integer> tagClassIDsToUseHashSet = new HashSet<Integer>();
//		ArrayList<Integer> tagClassIDsToUseArrayList = new ArrayList<Integer>();
//		{
//
//			Set<Integer> set = tagClassToUserTagVector.keySet();
//
//			for (Iterator<Integer> iterator = set.iterator(); iterator.hasNext();) {
//
//				Integer tagClassID = iterator.next();
//
//				tagClassIDsToUseHashSet.add(tagClassID);
//				tagClassIDsToUseArrayList.add(tagClassID);
//
//			}
//
//		}

		/********************************/
		/* identify tag class exemplars */
		/********************************/

		HashMap<Integer, Vector<NesterTag>> tagClassToUserExemplarTagVector = new HashMap<Integer, Vector<NesterTag>>();

		/* for centroid calculation */

		HashMap<Integer, double[]> tagClassCentroidSpectraValues = new HashMap<Integer, double[]>();
		HashMap<Integer, int[]> tagClassCentroidSpectraCounts = new HashMap<Integer, int[]>();

		int numClassUserTags = 0;

		Vector<NesterTag> userTagVector = new Vector<NesterTag>();

		{
			int numClasses = tagClassToUserTagVector.size();
			logger.log(Level.INFO, "### numClasses = " + numClasses);

			/* load all example window spectra */

			int numExamples = 0;
			Set<Integer> tagClassIDKeySet = null;

			tagClassIDKeySet = tagClassToUserTagVector.keySet();
			logger.log(Level.INFO, "### tagClassToUserTagVector.size() = " + tagClassToUserTagVector.size());

			for (Iterator<Integer> iterator = tagClassIDKeySet.iterator(); iterator.hasNext();) {
				Integer tagClassID = iterator.next();

				Vector<NesterTag> classTagVector = tagClassToUserTagVector.get(tagClassID);
				logger.log(Level.INFO, "### classTagVector.size() = " + classTagVector.size());

				numExamples += classTagVector.size();
			}

			int[] exampleAudioFileIndices = new int[numExamples];

			double[] exampleStartTimes = new double[numExamples];
			double[] exampleEndTimes = new double[numExamples];

			int[] exampleStartFrameIndices = new int[numExamples];
			int[] exampleNumFrames = new int[numExamples];
			int[] exampleStartFreqIndex = new int[numExamples];
			int[] exampleEndFreqIndex = new int[numExamples];

			tagClassIDKeySet = tagClassToUserTagVector.keySet();

			BandPass bandPass = new BandPass();

			bandPass.setNumBands(nesterSupervisedTagDiscoveryBias.numFrequencyBands);
			bandPass.setMinBandFrequency(nesterSupervisedTagDiscoveryBias.minFrequency);
			bandPass.setMaxBandFrequency(nesterSupervisedTagDiscoveryBias.maxFrequency);
			bandPass.initialize();

			int exampleIndex = 0;
			for (Iterator<Integer> iterator = tagClassIDKeySet.iterator(); iterator.hasNext();) {
				Integer tagClassID = iterator.next();

				Vector<NesterTag> classTagVector = tagClassToUserTagVector.get(tagClassID);
				for (Iterator<NesterTag> iterator2 = classTagVector.iterator(); iterator2.hasNext();) {

					NesterTag nesterTag = iterator2.next();

					exampleStartTimes[exampleIndex] = nesterTag.startTime;
					exampleEndTimes[exampleIndex] = nesterTag.endTime;

					int tagStartFrame = (int) (nesterTag.startTime * nesterSupervisedTagDiscoveryBias.numTimeFramesPerSecond);
					int tagEndFrame = (int) (nesterTag.endTime * nesterSupervisedTagDiscoveryBias.numTimeFramesPerSecond);

					// int midpointFrame = (tagStartFrame + tagEndFrame) / 2;
					//
					// double exampleDuration = 1.0;
					// int numFrames = (int) (exampleDuration * nesterSupervisedTagDiscoveryBias.numTimeFramesPerSecond);
					//
					// tagStartFrame = midpointFrame - numFrames / 2;
					//
					// if (tagStartFrame < 0) {
					// tagStartFrame = 0;
					// }
					int numFrames = tagEndFrame - tagStartFrame + 1;

					exampleAudioFileIndices[exampleIndex] = nesterTag.audioFileIndex;
					exampleStartFrameIndices[exampleIndex] = tagStartFrame;
					exampleNumFrames[exampleIndex] = numFrames;
					
					exampleStartFreqIndex[exampleIndex] = bandPass.getFrequencyIndex(nesterTag.minFrequency);
					exampleEndFreqIndex[exampleIndex] = bandPass.getFrequencyIndex(nesterTag.maxFrequency);

					nesterTag.exampleIndex = exampleIndex;

					exampleIndex++;
				}
			}

			logger.log(Level.INFO, "### numExamples " + numExamples + "  exampleAudioFileIndices  " + exampleAudioFileIndices + "  exampleStartFrameIndices  "
					+ exampleStartFrameIndices + "  exampleNumFrames  " + exampleNumFrames + "  exampleStartFreqIndex  " + exampleStartFreqIndex + "  exampleEndFreqIndex  "
					+ exampleEndFreqIndex);

			// !!! loadExampleSpectraData(numExamples, exampleAudioFileIndices, exampleStartFrameIndices, exampleNumFrames, exampleStartFreqIndex, exampleEndFreqIndex);
			computeExampleSpectraData(numExamples, exampleAudioFileIndices, exampleStartTimes, exampleEndTimes, exampleStartFreqIndex, exampleEndFreqIndex);

			// if (true) { !!!
			//
			// IO.writeObject("/data/allExampleSpectra.ser", allExampleSpectra);
			//
			// System.out.println("Finished " + this.getClass());
			// return;
			// }

			/*****************************/
			/* loop over all tag classes */
			/*****************************/

			numClassUserTags = 0;
			for (Iterator<Integer> tagClassIDIterator = tagClassIDKeySet.iterator(); tagClassIDIterator.hasNext();) {

				Integer tagClassID = tagClassIDIterator.next();

				Vector<NesterTag> classTagVector = tagClassToUserTagVector.get(tagClassID);

				/* if tag class has less than requested number of examplars, use all examples as exemplars */
				if (classTagVector.size() <= nesterSupervisedTagDiscoveryBias.numExemplars) {
					// use all available examples as exemplars
					tagClassToUserExemplarTagVector.put(tagClassID, classTagVector);

					for (Iterator<NesterTag> iterator = classTagVector.iterator(); iterator.hasNext();) {
						NesterTag nesterTag = (NesterTag) iterator.next();
						userTagVector.add(nesterTag);
					}

				} else {
					// compute centroid

					double durationSum = 0.0;

					int maxNumFrames = Integer.MIN_VALUE;
					for (Iterator<NesterTag> iterator2 = classTagVector.iterator(); iterator2.hasNext();) {
						NesterTag nesterTag = iterator2.next();

						double duration = nesterTag.endTime - nesterTag.startTime;

						durationSum += duration;

						int numFrames = (int) (duration * nesterSupervisedTagDiscoveryBias.numTimeFramesPerSecond);

						if (numFrames > maxNumFrames) {
							maxNumFrames = numFrames;
						}

					}

					double centroidDuration = durationSum / classTagVector.size();

					logger.log(Level.INFO, "### centroidDuration = " + centroidDuration);

					int centroidNumFrames = (int) (centroidDuration * nesterSupervisedTagDiscoveryBias.numTimeFramesPerSecond);

					logger.log(Level.INFO, "### maxNumFrames = " + maxNumFrames);
					logger.log(Level.INFO, "### centroidNumFrames = " + centroidNumFrames);

					int centroidNumSpectralValues = centroidNumFrames * tagDiscoveryNumFrequencyBands;

					double[] tempSpectralValues = new double[centroidNumSpectralValues];

					double[] centroidSpectraValues = new double[centroidNumSpectralValues];
					int[] centroidSpectraCounts = new int[centroidNumSpectralValues];

					tagClassCentroidSpectraValues.put(tagClassID, centroidSpectraValues);
					tagClassCentroidSpectraCounts.put(tagClassID, centroidSpectraCounts);

					for (Iterator<NesterTag> iterator2 = classTagVector.iterator(); iterator2.hasNext();) {

						NesterTag nesterTag = iterator2.next();

						int[] tagAudioFileData = tagDiscoveryAllExampleSpectra[nesterTag.exampleIndex];
						int numFrames = tagDiscoveryAllExampleSpectraNumFrames[nesterTag.exampleIndex];

						if (centroidNumFrames < numFrames) {
							numFrames = centroidNumFrames;
						}

						int numValues = numFrames * tagDiscoveryNumFrequencyBands;

						for (int valueIndex = 0; valueIndex < numValues; valueIndex++) {

							centroidSpectraValues[valueIndex] += tagAudioFileData[valueIndex];
							centroidSpectraCounts[valueIndex]++;

						}

					}

					/* normalize centroid sums to form centroid */
					{

						int numValues = centroidNumFrames * tagDiscoveryNumFrequencyBands;

						for (int valueIndex = 0; valueIndex < numValues; valueIndex++) {

							centroidSpectraValues[valueIndex] /= centroidSpectraCounts[valueIndex];
							valueIndex++;

						}

					}

					// find the 'numExemplars' closest examples and use them as exemplars

					ArrayList<NesterTagAndDistance> userTags = new ArrayList<NesterTagAndDistance>();

					for (Iterator<NesterTag> iterator2 = classTagVector.iterator(); iterator2.hasNext();) {

						NesterTag nesterTag = iterator2.next();

						// System.out.println("### nesterTag.id = " + nesterTag.id);

						int[] tagAudioFileData = tagDiscoveryAllExampleSpectra[nesterTag.exampleIndex];

						int numTagFrames = tagDiscoveryAllExampleSpectraNumFrames[nesterTag.exampleIndex];

						if (numTagFrames > centroidNumFrames)
							numTagFrames = centroidNumFrames;

						int tagNumSpectralValues = numTagFrames * tagDiscoveryNumFrequencyBands;

						{
							for (int valueIndex = 0; valueIndex < tagNumSpectralValues; valueIndex++) {

								tempSpectralValues[valueIndex] = tagAudioFileData[valueIndex];
							}

							int numCorrelationValues = Math.min(centroidNumSpectralValues, tagNumSpectralValues);

							double distance = 1.0 - Correlation.correlation(centroidSpectraValues, tempSpectralValues, numCorrelationValues);

							if (randomExemplars)
								userTags.add(new NesterTagAndDistance(nesterTag, Math.random()));
							else
								userTags.add(new NesterTagAndDistance(nesterTag, distance));
						}

					}

					Collections.sort(userTags);

					Vector<NesterTag> exemplarTagVector = new Vector<NesterTag>();

					int exemplarCount = 0;
					for (Iterator<NesterTagAndDistance> iterator2 = userTags.iterator(); iterator2.hasNext();) {

						NesterTagAndDistance nesterTagAndDistance = (NesterTagAndDistance) iterator2.next();

						logger.log(Level.INFO, "### distance = " + nesterTagAndDistance.distance + " id = " + nesterTagAndDistance.tag.id);

						exemplarTagVector.add(nesterTagAndDistance.tag);
						userTagVector.add(nesterTagAndDistance.tag);

						exemplarCount++;

						if (exemplarCount == nesterSupervisedTagDiscoveryBias.numExemplars)
							break;

					}

					tagClassToUserExemplarTagVector.put(tagClassID, exemplarTagVector);

				}

				numClassUserTags += classTagVector.size();
			}

			logger.log(Level.INFO, "### numClassUserTags = " + numClassUserTags);

		}

		userTagArray = new NesterTag[userTagVector.size()];
		userTagVector.toArray(userTagArray);
		Arrays.sort(userTagArray);

		int maxExampleNumFrames = Integer.MIN_VALUE;
		for (int i = 0; i < tagDiscoveryAllExampleSpectraNumFrames.length; i++) {

			if (tagDiscoveryAllExampleSpectraNumFrames[i] > maxExampleNumFrames)
				maxExampleNumFrames = tagDiscoveryAllExampleSpectraNumFrames[i];

		}
		logger.log(Level.INFO, "### maxExampleNumFrames = " + maxExampleNumFrames);

		/*****************/
		/* discover tags */
		/*****************/

		int candidateAudioFileIndex = -1;
		NesterAudioFile candidateNesterAudioFile = null;

		int numAudioFilesToProcess = tagDiscoveryNumAudioFiles;

//		BufferedWriter bufferedWriter = null;
//		try {
//			// TODO need to move this file location into the ArloSettings settings class if we keep it
//			bufferedWriter = new BufferedWriter(new FileWriter(new File("/data/tags.csv")));
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

		int numSeedTags = userTagArray.length;
		logger.log(Level.INFO, "numSeedTags = " + numSeedTags);

		if (numSeedTags == 0) {
			logger.log(Level.WARNING, "SupervisedTagDiscovery() - numSeedTags = 0");
			nesterSupervisedTagDiscoveryBias.SaveJobLogMessage("SupervisedTagDiscovery() - numSeedTags = 0");
			return true;
		}

		nesterSupervisedTagDiscoveryBias.SaveJobLogMessage("Examples Loaded (" + numSeedTags + ") - Running Search");


		double seedBestScores[] = new double[numSeedTags];

		for (int i = 0; i < seedBestScores.length; i++) {
			seedBestScores[i] = Double.NEGATIVE_INFINITY;
		}

		int maxSeedNumFrames = Integer.MIN_VALUE;
		for (int seedTagIndex = 0; seedTagIndex < numSeedTags; seedTagIndex++) {
			NesterTag seedTag = this.userTagArray[seedTagIndex];

			int seedExampleIndex = seedTag.exampleIndex;

			int[] seedExampleSpectra = this.tagDiscoveryAllExampleSpectra[seedExampleIndex];
			int seedNumFrames = this.tagDiscoveryAllExampleSpectraNumFrames[seedExampleIndex];

			if (seedNumFrames > maxSeedNumFrames) {
				maxSeedNumFrames = seedNumFrames;
			}

		}

		boolean randomizeAudioFileOrder = true;

		int audioFileIndex = -1;

		int[] randomizedAudioFileIndices = null;

		if (randomizeAudioFileOrder)
			randomizedAudioFileIndices = Utility.randomIntArray(randomizedAudioFileIndicesRandom, numAudioFilesToProcess);

		for (int audioFileIndexIndex = 0 /* !!! */; audioFileIndexIndex < numAudioFilesToProcess; audioFileIndexIndex++) {

			// // //
			// Performance Considerations
			// --------------------------
			// For large Projects (e.g., the current 27,000 file Project) most iterations through this 
			// loop while running in QueueRunner mode will be skipped - therefore the work done before 
			// the tests should be minimal. 

			// // //
			// Executed for every file

			if (randomizeAudioFileOrder)
				audioFileIndex = randomizedAudioFileIndices[audioFileIndexIndex];
			else
				audioFileIndex = audioFileIndexIndex;

			// if (!singleFileMode) {
			candidateAudioFileIndex = audioFileIndex;
			// }
			candidateNesterAudioFile = tagDiscoveryNesterAudioFiles.get(candidateAudioFileIndex);
			// candidateAudioFileNumFrames = (int) tagDiscoveryAudioFileSpectraNumFrames[candidateAudioFileIndex];

			// single file mode for QueueTask_SupervisedTagDiscovery
			if (singleFileModeIdsVector != null) {
				if ( ! singleFileModeIdsVector.contains(candidateNesterAudioFile.id)) {
					continue;
				}
			}

			if (!candidateNesterAudioFile.active) {
				continue;
			}

			if (shouldStop()) {
				nesterSupervisedTagDiscoveryBias.SaveJobLogMessage("Stopping at Request");
				return true;  // return gracefully
			}

			// ///
			// House keeping tasks

			// update job status

			double percentCompleted = 0.0;
			percentCompleted = (double) audioFileIndexIndex / (double) tagDiscoveryNumAudioFiles * 100.0;
			this.tagDiscoveryBias.fractionCompleted = percentCompleted;
			// System.out.println("### tagDiscoveryBias.id  = " + this.tagDiscoveryBias.id);
			// System.out.println("### fractionCompleted    = " + this.tagDiscoveryBias.fractionCompleted);
			this.tagDiscoveryBias.updateFractionCompletedForTagAnalysis(connection);

			logger.log(Level.INFO,
				"### tagDiscoveryBias.id  = " + this.tagDiscoveryBias.id +
				"    audioFileIndexIndex = " + audioFileIndexIndex +
				"    fractionCompleted: " + this.tagDiscoveryBias.fractionCompleted);
			logger.log(Level.INFO, "### candidateAudioFileIndex = " + candidateAudioFileIndex);
			logger.log(Level.INFO, "### candidateNesterAudioFile.mediaRelativeFilePath = " + candidateNesterAudioFile.mediaRelativeFilePath);
			nesterSupervisedTagDiscoveryBias.SaveJobLogMessage(
				"Searching File " + candidateNesterAudioFile.id + " - " + candidateNesterAudioFile.mediaRelativeFilePath + 
				"(FractionCompleted: " + this.tagDiscoveryBias.fractionCompleted + ")");


			// ////
			// Run the actual search...


			if (searchWithinTagVector != null) {
				
				////
				// If we are restricting the search with searchWithinTagClasses, see if there's any in this file

				for (Iterator<NesterTag> iterator = searchWithinTagVector.iterator(); iterator.hasNext();) {
					NesterTag tag = iterator.next();

					if (tag.audioFile_id == candidateNesterAudioFile.id) {
						
						if (! this.searchWithinAudioFile(
								candidateNesterAudioFile,
								tag.startTime,
								tag.endTime,
								nesterSupervisedTagDiscoveryBias,
								connection,
								searchWithinTagVector,
								timeContextSize,
								originalAllTags,
								audioFileIndex,
								tagClassIDToMatchQuality,
								allTags
								)) 
							{
								nesterSupervisedTagDiscoveryBias.SaveJobLogMessage("ERROR: Failed Searching Within File");
								return false;
							}

					}
				}

			} else {

				////
				// We are NOT restricting the search with searchWithinTagClasses

				if (! this.searchWithinAudioFile(
						candidateNesterAudioFile,
						0,
						candidateNesterAudioFile.wavFileMetaData.durationInSeconds,
						nesterSupervisedTagDiscoveryBias,
						connection,
						searchWithinTagVector,
						timeContextSize,
						originalAllTags,
						audioFileIndex,
						tagClassIDToMatchQuality,
						allTags
					))
				{
					nesterSupervisedTagDiscoveryBias.SaveJobLogMessage("ERROR: Failed Searching Within File");
					return false;
				}

			}



			if (shouldStop()) {
				nesterSupervisedTagDiscoveryBias.SaveJobLogMessage("Stopping at Request");
				return true;  // return gracefully
			}

		}

		this.tagDiscoveryBias.fractionCompleted = 1.0;
		logger.log(Level.INFO, "### fractionCompleted    = " + this.tagDiscoveryBias.fractionCompleted);
		this.tagDiscoveryBias.updateFractionCompletedForTagAnalysis(connection);

//		try {
//			bufferedWriter.close();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

		System.gc();

		logger.log(Level.INFO, "### finished discoverTags ###");
		nesterSupervisedTagDiscoveryBias.SaveJobLogMessage("Search Completed");
		return true;
	}






	/** Search for Tags within a file, or a portion thereof.
	*
	* Can be called from multiple locations. Removed from ServiceHead
	* so that it can be called from QueueTask_SupervisedTagDiscovery.
	* @param nesterSupervisedTagDiscoveryBias
	* @param connection An opened database connection.
	* @return True if completed or stopped, False if any error encountered.
	*/

	private boolean searchWithinAudioFile(
				NesterAudioFile candidateNesterAudioFile,
				double searchWithinFileStartTime,
				double searchWithinFileEndTime,
				NesterSupervisedTagDiscoveryBias nesterSupervisedTagDiscoveryBias,
				Connection connection,
				
				Vector<NesterTag> searchWithinTagVector,
				double timeContextSize,
				Vector<NesterTag> originalAllTags,
				int audioFileIndex,
				HashMap<Integer, Double> tagClassIDToMatchQuality,
				Vector<NesterTag> allTags
				)
	{
		Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
		logger.log(Level.INFO, "Starting searchWithinAudioFile");

		double realStartTime = System.currentTimeMillis() / 1000.0;
		boolean firstTime = true;
		int numberOfTagsDiscovered = 0;

		// ///
		// Check Search Times

		if (searchWithinFileEndTime > candidateNesterAudioFile.wavFileMetaData.durationInSeconds) {
			logger.log(Level.WARNING, "searchWithinFileEndTime > candidateNesterAudioFile.wavFileMetaData.durationInSeconds");
			searchWithinFileEndTime = candidateNesterAudioFile.wavFileMetaData.durationInSeconds;
		}
		if (searchWithinFileStartTime >= searchWithinFileEndTime) {
			logger.log(Level.SEVERE, "searchWithinFileStartTime >= searchWithinFileEndTime");
			return false;
		}

		double searchWithinFileDuration = searchWithinFileEndTime - searchWithinFileStartTime;


		// ///////
		//
		// Build up seed Tag data
		//

		double spectraWeight = nesterSupervisedTagDiscoveryBias.spectraWeight;
		double volumeWeight = nesterSupervisedTagDiscoveryBias.averageEnergyWeight;
		double pitchWeight = nesterSupervisedTagDiscoveryBias.pitchWeight;

		// loadAudioFileSpectraData(candidateAudioFileIndex, 0, candidateAudioFileNumFrames, false);

		int numSeedTags = this.userTagArray.length;

		int[] seedSpectraSizes = new int[numSeedTags];
		int[] seedSpectraOffsets = new int[numSeedTags];

		int offset = 0;
		int allSeedsNumSpectraFrames = 0;

		for (int seedTagIndex = 0; seedTagIndex < numSeedTags; seedTagIndex++) {

			NesterTag seedTag = this.userTagArray[seedTagIndex];

			int seedExampleIndex = seedTag.exampleIndex;

			int[] currentSeedExampleSpectra = this.tagDiscoveryAllExampleSpectra[seedExampleIndex];
			int currentSeedNumFrames = this.tagDiscoveryAllExampleSpectraNumFrames[seedExampleIndex];

			allSeedsNumSpectraFrames += currentSeedNumFrames;

			seedSpectraSizes[seedTagIndex] = currentSeedNumFrames * tagDiscoveryNumFrequencyBands;
			seedSpectraOffsets[seedTagIndex] = offset;
			offset += seedSpectraSizes[seedTagIndex];
		}

		int allSeedsNumSpectraValues = allSeedsNumSpectraFrames * tagDiscoveryNumFrequencyBands;

		logger.log(Level.INFO, "allSeedsNumSpectraFrames = " + allSeedsNumSpectraFrames);
		logger.log(Level.INFO, "allSeedsNumSpectraValues = " + allSeedsNumSpectraValues);

		int[] seedExampleSpectra = new int[allSeedsNumSpectraFrames * tagDiscoveryNumFrequencyBands];
		int[] seedVolumes = new int[allSeedsNumSpectraFrames];
		int[] seedPitches = new int[allSeedsNumSpectraFrames];

		int seedSpectraIndex = 0;
		int seedVolumeIndex = 0;
		int seedPitchIndex = 0;

		for (int seedTagIndex = 0; seedTagIndex < numSeedTags; seedTagIndex++) {

			NesterTag seedTag = this.userTagArray[seedTagIndex];

			int seedExampleIndex = seedTag.exampleIndex;

			int[] currentSeedExampleSpectra = this.tagDiscoveryAllExampleSpectra[seedExampleIndex];
			int currentSeedNumFrames = this.tagDiscoveryAllExampleSpectraNumFrames[seedExampleIndex];

			for (int j = 0; j < currentSeedExampleSpectra.length; j++) {
				seedExampleSpectra[seedSpectraIndex++] = currentSeedExampleSpectra[j];
			}

			{
				int index = 0;
				for (int i = 0; i < currentSeedNumFrames; i++) {
					long sum = 0;
					for (int j = 0; j < tagDiscoveryNumFrequencyBands; j++) {
						sum += currentSeedExampleSpectra[index++];
					}
					seedVolumes[seedVolumeIndex++] = (int) (sum / tagDiscoveryNumFrequencyBands);
				}
			}

			{
				int index = 0;
				for (int i = 0; i < currentSeedNumFrames; i++) {

					double weightedPitchSum = 0.0;
					double weightsum = 0.0;

					// int maxValue = Integer.MIN_VALUE;
					// int maxJ = Integer.MIN_VALUE;
					for (int j = 0; j < tagDiscoveryNumFrequencyBands; j++) {
						int value = currentSeedExampleSpectra[index++];

						weightedPitchSum += value * j;
						weightsum += value;

						// if (value > maxValue) {
						// maxValue = value;
						// maxJ = j;
						// }
					}
					// seedPitches[seedPitchIndex++] = maxJ;
					seedPitches[seedPitchIndex++] = (int) (weightedPitchSum / weightsum * 1000);
				}

			}
		}


		// ////
		// Break the AudioFile into segments
		// compute spectra/correlations on each segment individually
		// When done, we'll be left with "allSeedTagScores"


		double audioFileSegmentSizeInSeconds = 1024.0;

		int numAudioFileSegments = (int) (searchWithinFileDuration / audioFileSegmentSizeInSeconds);

		if (numAudioFileSegments * audioFileSegmentSizeInSeconds < searchWithinFileDuration) {
			numAudioFileSegments++;
		}
		logger.log(Level.INFO, "numAudioFileSegments = " + numAudioFileSegments);

		int candidateAudioFileNumFrames = (int) (searchWithinFileDuration * nesterSupervisedTagDiscoveryBias.numTimeFramesPerSecond);

		// '0' index of this is the searchWithinFileStartTime
		// be sure to offset this when access absolute time parameters
		allSeedTagScores = new double[candidateAudioFileNumFrames][numSeedTags]; 


		for (int audioSegmentIndex = 0; audioSegmentIndex < numAudioFileSegments; audioSegmentIndex++) {

			logger.log(Level.INFO, "working on audioSegmentIndex = " + audioSegmentIndex);

			double audioFileOffsetStartTime = audioSegmentIndex * audioFileSegmentSizeInSeconds;
			double audioFileOffsetEndTime = (audioSegmentIndex + 1) * audioFileSegmentSizeInSeconds;

			if (audioFileOffsetEndTime > searchWithinFileDuration) {
				audioFileOffsetEndTime = searchWithinFileDuration;
			}

			double audioFileSegmentDuration = audioFileOffsetEndTime - audioFileOffsetStartTime;
			logger.log(Level.INFO, "audioFileSegmentDuration = " + audioFileSegmentDuration);


			// ///
			//         COMPUTE SPECTRA
			//


			logger.log(Level.INFO, "### COMPUTING SPECTRA ###");

			boolean useGPUForSpectraComputation = false; // !!! wtf??? true DOES NOT WORK!!!

			int[] spectra = null;

			if (!useGPUForSpectraComputation) {

				spectra = computeAudioFileSpectraData(candidateNesterAudioFile, audioFileOffsetStartTime + searchWithinFileStartTime, audioFileOffsetEndTime + searchWithinFileStartTime);

			} else {

				// load file audio data //

				long startTimeInMS = System.currentTimeMillis();

				logger.log(Level.INFO, "starting BytesToWavData.getWavDataAsInt");

				int[] audioSamples = BytesToWavData.getWavDataAsInt(new AudioFile(candidateNesterAudioFile), audioFileOffsetStartTime + searchWithinFileStartTime, audioFileOffsetEndTime + searchWithinFileStartTime, 0, true);

				logger.log(Level.INFO, "audioSamples.length = " + audioSamples.length);

				int samplesPerFrame = (int) (candidateNesterAudioFile.wavFileMetaData.sampleRate / nesterSupervisedTagDiscoveryBias.numTimeFramesPerSecond);

				logger.log(Level.INFO, "samplesPerFrame = " + samplesPerFrame);

				int audioSamplesNumFrames = audioSamples.length / samplesPerFrame;

				logger.log(Level.INFO, "audioSamplesNumFrames = " + audioSamplesNumFrames);

				long endTimeInMS = System.currentTimeMillis();
				logger.log(Level.INFO, "done with BytesToWavData.getWavDataAsInt");

				logger.log(Level.INFO, "dataSize = " + candidateNesterAudioFile.wavFileMetaData.dataSize);

				double readDuration = (endTimeInMS - startTimeInMS) / 1000.0;
				logger.log(Level.INFO, "BytesToWavData.getWavDataAsInt readDuration = " + readDuration);

				double samplesPerSecond = (double) audioSamples.length / readDuration;
				logger.log(Level.INFO, "samplesPerSecond = " + samplesPerSecond);

				double chunkSizeInSeconds = 8.0;
				double preRollSizeInSeconds = 1.0;
				int numChunks = (int) (audioFileSegmentDuration / chunkSizeInSeconds);
				if (numChunks * chunkSizeInSeconds < audioFileSegmentDuration) {
					numChunks++;
				}

				int numProblems = numChunks * tagDiscoveryNumFrequencyBands;

				logger.log(Level.INFO, "numChunks   = " + numChunks);
				logger.log(Level.INFO, "numBands    = " + tagDiscoveryNumFrequencyBands);
				logger.log(Level.INFO, "numProblems = " + numProblems);

				int framesPerChunk = (int) (chunkSizeInSeconds * nesterSupervisedTagDiscoveryBias.numTimeFramesPerSecond);
				logger.log(Level.INFO, "framesPerChunk = " + framesPerChunk);

				int spectraNumValues = numProblems * framesPerChunk;
				logger.log(Level.INFO, "spectraNumValues = " + spectraNumValues);

				spectra = new int[spectraNumValues];

				// int samplesPerFrame = (int) (candidateNesterAudioFile.wavFileMetaData.sampleRate / nesterSupervisedTagDiscoveryBias.numTimeFramesPerSecond);
				int numSamplesPerChunk = (int) (chunkSizeInSeconds * candidateNesterAudioFile.wavFileMetaData.sampleRate);

				logger.log(Level.INFO, "samplesPerFrame = " + samplesPerFrame);
				logger.log(Level.INFO, "numSamplesPerChunk = " + numSamplesPerChunk);

				// public BandPassKernel(int _numProblems, double _dampingFactor, double _minFrequency, double _maxFrequency, int _numFrequencyBands, int _numSamplesPerFrame,
				// int _numSamplesPerChunk, int _numChunks, int[] _audioSamples, int[] _audioSpectra) {

				int numSamplesForConditioning = (int) (preRollSizeInSeconds * candidateNesterAudioFile.wavFileMetaData.sampleRate);

				logger.log(Level.INFO, "audioFileSegmentDuration                  = " + audioFileSegmentDuration);

				boolean debugMode = false;
				final BandPassGPU.BandPassKernel kernel = new BandPassGPU.BandPassKernel(debugMode, numProblems, nesterSupervisedTagDiscoveryBias.dampingRatio,
						nesterSupervisedTagDiscoveryBias.minFrequency, nesterSupervisedTagDiscoveryBias.maxFrequency, nesterSupervisedTagDiscoveryBias.numFrequencyBands,
						candidateNesterAudioFile.wavFileMetaData.sampleRate, samplesPerFrame, numSamplesPerChunk, numChunks, numSamplesForConditioning, framesPerChunk,
						audioSamples, spectra);

				// public BandPassKernel(boolean _debugMode, int _numProblems, double _dampingRatio, double _minFrequency, double _maxFrequency, int _numFrequencyBands, double _samplingRate,
				// int _numSamplesPerFrame, int _numSamplesPerChunk, int _numChunks, int _numSamplesForConditioning, int _audioNumTimeSlices, int[] _audioSamples, int[] _audioSpectra) {

				kernel.putAudioFileDataOnGPU();

				if (gpuDebugMode)
					kernel.setExecutionMode(Kernel.EXECUTION_MODE.SEQ);
				else
					kernel.setExecutionMode(Kernel.EXECUTION_MODE.GPU);

				if (numProblems > 0)
					kernel.nextGeneration(numProblems);

				// for (int i = 0; i < spectra.length; i++) {
				//
				// System.out.println("spectra[" + i + "] = " + spectra[i]);
				//
				// }

				kernel.dispose();

			}

			//

			if (shouldStop()) {
				// if (this.serviceHead.stoppedJobIDs.contains(nesterSupervisedTagDiscoveryBias.id)) {
				nesterSupervisedTagDiscoveryBias.SaveJobLogMessage("Stopping at Request");
				return true;  // return gracefully
			}

			// ///
			//        SEARCH
			//


			logger.log(Level.INFO, "### SEARCHING SPECTRA ###");

			// tagDiscoveryAudioFileSpectra = new int[(int) (candidateAudioFileNumFrames * tagDiscoveryNumFrequencyBands)];

			int candidateAudioFileSegmentNumFrames = (int) (audioFileSegmentDuration * nesterSupervisedTagDiscoveryBias.numTimeFramesPerSecond);
			logger.log(Level.INFO, "candidateAudioFileNumFrames = " + candidateAudioFileSegmentNumFrames);

			int[] audioFileVolumes = new int[candidateAudioFileSegmentNumFrames];
			int[] audioFilePitches = new int[candidateAudioFileSegmentNumFrames];

			logger.log(Level.INFO, "numSeedTags = " + numSeedTags);
			logger.log(Level.INFO, "spectra.length = " + spectra.length);

			{
				int index = 0;
				for (int i = 0; i < candidateAudioFileSegmentNumFrames; i++) {
					long sum = 0;
					for (int j = 0; j < tagDiscoveryNumFrequencyBands; j++) {
						sum += spectra[index++];
					}
					audioFileVolumes[i] = (int) (sum / tagDiscoveryNumFrequencyBands);
				}
			}

			{
				int index = 0;
				for (int i = 0; i < candidateAudioFileSegmentNumFrames; i++) {

					double weightedPitchSum = 0.0;
					double weightsum = 0.0;

					// int maxValue = Integer.MIN_VALUE;
					// int maxJ = Integer.MIN_VALUE;

					for (int j = 0; j < tagDiscoveryNumFrequencyBands; j++) {

						int value = spectra[index++];

						weightedPitchSum += value * j;
						weightsum += value;

						// if (value > maxValue) {
						// maxValue = value;
						// maxJ = j;
						// }
					}
					// audioFilePitches[i] = maxJ;
					audioFilePitches[i] = (int) (weightedPitchSum / weightsum * 1000);
				}
			}

			int numProblems = numSeedTags * candidateAudioFileSegmentNumFrames;

			float[] correlations = new float[numProblems];

			for (int i = 0; i < correlations.length; i++) {
				correlations[i] = Float.NEGATIVE_INFINITY;
			}

			final SupervisedTagDiscoveryGPU.SupervisedTagDiscoveryKernel kernel = new SupervisedTagDiscoveryGPU.SupervisedTagDiscoveryKernel(numProblems, spectraWeight,
					volumeWeight, pitchWeight, tagDiscoveryNumFrequencyBands, numSeedTags, seedSpectraSizes, seedSpectraOffsets, seedExampleSpectra,
					candidateAudioFileSegmentNumFrames, spectra, seedVolumes, audioFileVolumes, seedPitches, audioFilePitches, correlations);

			// if (tagDiscoveryAudioFileNumFrames >= cpuGPUThreshold) {
			// kernel.putAudioFileDataOnGPU();
			// }

			// kernel.setExecutionMode(Kernel.EXECUTION_MODE.SEQ);

			// if (audioFileNumFrames < cpuGPUThreshold) {
			kernel.setExecutionMode(Kernel.EXECUTION_MODE.CPU);
			// } else {
			// kernel.setExecutionMode(Kernel.EXECUTION_MODE.GPU);
			// }

			if (numProblems > 0)
				kernel.nextGeneration(numProblems);

			// for (int i = 0; i < numProblems; i++) {
			//
			// if (Float.isInfinite(correlations[i]))
			// System.out.println("correlations[" + i + "] = " + correlations[i]);
			// }

			{
				int audioFileSegmentTimeOffset = (int) (audioSegmentIndex * audioFileSegmentSizeInSeconds * nesterSupervisedTagDiscoveryBias.numTimeFramesPerSecond);
				int index = 0;
				for (int seedTagIndex = 0; seedTagIndex < numSeedTags; seedTagIndex++) {
					for (int audioFileOffset = 0; audioFileOffset < candidateAudioFileSegmentNumFrames; audioFileOffset++) {
						allSeedTagScores[audioFileSegmentTimeOffset + audioFileOffset][seedTagIndex] = correlations[index++];
					}
				}
			}

			kernel.dispose();

			double realElapsedTime = System.currentTimeMillis() / 1000.0 - realStartTime;

			double windowElapsedTime = candidateAudioFileSegmentNumFrames / nesterSupervisedTagDiscoveryBias.numTimeFramesPerSecond;

			double rate = numSeedTags * windowElapsedTime / realElapsedTime;

			logger.log(Level.INFO, "rate = " + rate + " (X realtime)");

			long numComparisons = (long) numProblems
					* ((long) allSeedsNumSpectraFrames / numSeedTags * tagDiscoveryNumFrequencyBands + (long) allSeedsNumSpectraFrames / numSeedTags + (long) allSeedsNumSpectraFrames
							/ numSeedTags);
			double comparisonsPerSecond = numComparisons / realElapsedTime;

			logger.log(Level.INFO, "comparisonsPerSecond = " + comparisonsPerSecond);
		}

		/*****************************************************/
		/*****************************************************/
		/*****************************************************/
		/*                                                   */
		/* inspect match profiles to create new machine tags */
		/*                                                   */
		/*****************************************************/
		/*****************************************************/
		/*****************************************************/

		////
		// restrict system from creating machine tags overlapping existing user tags for this audio file

		for (int tagIndex = 0; tagIndex < originalAllTags.size(); tagIndex++) {

			NesterTag nesterTag = originalAllTags.elementAt(tagIndex);

			if (nesterTag.audioFileIndex == audioFileIndex) {

				// these are the absolute times !!!
				double startTime = nesterTag.startTime - timeContextSize;
				double endTime = nesterTag.endTime + timeContextSize;
				double duration = endTime - startTime;

				double overlap = nesterSupervisedTagDiscoveryBias.maxOverlapFraction * duration;

				logger.log(Level.INFO, "overlap = " + overlap);

				double restrictedStartTime = startTime - duration / 2.0 + overlap;
				double restrictedEndTime = startTime + duration / 2.0 - overlap;

				// relative to start of 'allSeedTagScores'
				double offsetRestrictedStartTime = restrictedStartTime - searchWithinFileStartTime;
				double offsetRestrictedEndTime   = restrictedEndTime   - searchWithinFileStartTime;

				int startTimeIndex = (int) (offsetRestrictedStartTime * nesterSupervisedTagDiscoveryBias.numTimeFramesPerSecond);
				int endTimeIndex = (int) (offsetRestrictedEndTime * nesterSupervisedTagDiscoveryBias.numTimeFramesPerSecond);

				if (startTimeIndex < 0)
					startTimeIndex = 0;

				if (endTimeIndex > allSeedTagScores.length - 1)
					endTimeIndex = allSeedTagScores.length - 1;

				for (int restrictedSeedTagIndex = 0; restrictedSeedTagIndex < numSeedTags; restrictedSeedTagIndex++) {

					for (int timeIndex = startTimeIndex; timeIndex <= endTimeIndex; timeIndex++) {

						allSeedTagScores[timeIndex][restrictedSeedTagIndex] = Double.NEGATIVE_INFINITY;

					}
				}

			}

		}


		int numRounds = nesterSupervisedTagDiscoveryBias.numberOfTagsToDiscover;

		// System.out.println("#############################################");
		// System.out.println("### numRounds = " + numRounds);
		// System.out.println("### numSeedTags = " + numSeedTags);

		ArrayList<NesterTag> newTags = new ArrayList<NesterTag>();

		boolean[] seedTagExhausted = new boolean[numSeedTags];

		for (int roundIndex = 0; roundIndex < numRounds; roundIndex++) {

			for (int seedTagIndex = 0; seedTagIndex < numSeedTags; seedTagIndex++) {

				if (seedTagExhausted[seedTagIndex])
					continue;

				double bestScore = Double.NEGATIVE_INFINITY;
				int bestAudioFileTimeIndex = -1;

				int numSeedFrames = seedSpectraSizes[seedTagIndex] / tagDiscoveryNumFrequencyBands;

				for (int audioFileTimeIndex = 0; audioFileTimeIndex < candidateAudioFileNumFrames - numSeedFrames; audioFileTimeIndex++) {

					if (allSeedTagScores[audioFileTimeIndex][seedTagIndex] > bestScore) {
						bestScore = allSeedTagScores[audioFileTimeIndex][seedTagIndex];
						bestAudioFileTimeIndex = audioFileTimeIndex;
					}

				}
				// System.out.println("### bestScore              = " + bestScore);
				// System.out.println("### bestAudioFileTimeIndex = " + bestAudioFileTimeIndex);

				double minScore = tagClassIDToMatchQuality.get(userTagArray[seedTagIndex].tagClass_id);

				// !!!
				if (bestScore < minScore) {
					seedTagExhausted[seedTagIndex] = true;
					continue;
				}

				if (bestAudioFileTimeIndex != -1) {

					/***********************************/
					/* add new machine tag to database */
					/***********************************/

					NesterTag bestSeedTag = userTagArray[seedTagIndex];

					// System.out.println("### found  tagClass_id = " + bestSeedTag.tagClass_id);

					int bestSeedExampleIndex = bestSeedTag.exampleIndex;
					int bestSeedNumFrames = tagDiscoveryAllExampleSpectraNumFrames[bestSeedExampleIndex];
					long bestCandidateExampleStartFrame = bestAudioFileTimeIndex;
					long bestCandidateExampleEndFrame = bestCandidateExampleStartFrame + bestSeedNumFrames - 1;
				//	double peakIntensityAverageIntensity = Double.NaN;
					double highestFrequencyAverageIntensity = Double.NaN;
				//	double highestFrequencyNoiseValue = Double.NaN;
				//	double peakIntensityNoiseValue = Double.NaN;
				//	int peakIntensityFreqIndex = -1;
					double peakIntensityFrequency = Double.NaN;
				//	double highestFrequency = Double.NaN;

				//	double contextStartTime = bestCandidateExampleStartFrame / nesterSupervisedTagDiscoveryBias.numTimeFramesPerSecond + timeContextSize;
				//	double contextEndTime = (bestCandidateExampleEndFrame + 1) / nesterSupervisedTagDiscoveryBias.numTimeFramesPerSecond - timeContextSize;
					double trueStartTime = bestCandidateExampleStartFrame / nesterSupervisedTagDiscoveryBias.numTimeFramesPerSecond + timeContextSize + searchWithinFileStartTime;
					double trueEndTime = (bestCandidateExampleEndFrame + 1) / nesterSupervisedTagDiscoveryBias.numTimeFramesPerSecond - timeContextSize + searchWithinFileStartTime;

					NesterTag newTag = null;
					{
						int id = -1;
						int user_id = nesterSupervisedTagDiscoveryBias.user_id;
						int tagSet_id = nesterSupervisedTagDiscoveryBias.destinationTagSetId;
						int audioFile_id = candidateNesterAudioFile.id;
						java.sql.Timestamp creationDate = new java.sql.Timestamp(System.currentTimeMillis());
						int tagClass_id = bestSeedTag.tagClass_id;

						// double minFrequency = nesterSupervisedTagDiscoveryBias.minFrequency;
						// double maxFrequency = nesterSupervisedTagDiscoveryBias.maxFrequency;
						double minFrequency = bestSeedTag.minFrequency;
						double maxFrequency = bestSeedTag.maxFrequency;

						int parentTag_id = bestSeedTag.id;
						boolean randomlyChosen = false;
						boolean machineTagged = true;
						boolean userTagged = false;

						double strength = bestScore;
						logger.log(Level.INFO, "### strength = " + strength);

						// ///
						// restrict future picks //

						{
							double startTime = userTagArray[seedTagIndex].startTime;
							double endTime = userTagArray[seedTagIndex].endTime;
							double duration = endTime - startTime;
							int durationNumFrames = (int) (duration * tagDiscoveryBias.numTimeFramesPerSecond);

							// int startTimeIndex = bestAudioFileTimeIndex - durationNumFrames / 2;
							// int endTimeIndex = bestAudioFileTimeIndex + durationNumFrames / 2;

							int overlapInFrames = (int) (nesterSupervisedTagDiscoveryBias.maxOverlapFraction * duration * tagDiscoveryBias.numTimeFramesPerSecond);

							logger.log(Level.INFO, "overlapInFrames = " + overlapInFrames);

							// double restrictedStartTime = startTime - duration + overlap;
							// double restrictedEndTime = restrictedStartTime + 2 * duration - overlap;

							// these remain relative to 'allSeedTagScores' since bestAudioFileTimeIndex is relative
							int startTimeIndex = bestAudioFileTimeIndex - durationNumFrames / 2 + overlapInFrames;
							int endTimeIndex = bestAudioFileTimeIndex + durationNumFrames / 2 - overlapInFrames;

							if (startTimeIndex < 0)
								startTimeIndex = 0;

							if (endTimeIndex > allSeedTagScores.length - 1)
								endTimeIndex = allSeedTagScores.length - 1;

							for (int restrictedSeedTagIndex = 0; restrictedSeedTagIndex < numSeedTags; restrictedSeedTagIndex++) {
								for (int timeIndex = startTimeIndex; timeIndex <= endTimeIndex; timeIndex++) {
									allSeedTagScores[timeIndex][restrictedSeedTagIndex] = Double.NEGATIVE_INFINITY;
								}
							}
						}


						newTag = new NesterTag(
								id, user_id, creationDate, tagClass_id,
								tagSet_id, audioFile_id, trueStartTime, trueEndTime, minFrequency, maxFrequency,
								parentTag_id, randomlyChosen, machineTagged, userTagged, strength
								);
					}



					// trueLastTagStartTime = trueStartTime;
					// trueLastTagEndTime = trueEndTime;

					allTags.add(newTag);

					if (useDB) {

						// if (bestScore > seedBestScores[seedTagIndex]) {
						//
						// seedBestScores[seedTagIndex] = bestScore;

						newTags.add(newTag);
						// }
					}

					if (true) {

						if (firstTime) {

							String textString = "TAGFOUND,user,tagID,startTime,endTime,centerTime,centerFreq,intensity,minFrequency,maxFrequency,tagClass,confidence,machine?,user?,parentTagID,audioFileID,audioFileAlias,mediaRelativeFilePath";
							textString += "\n";

//							try {
//								bufferedWriter.write(textString);
//							} catch (IOException e) {
//								// TODO Auto-generated catch block
//								e.printStackTrace();
//							}

							firstTime = false;
						}

						double middleTime = (newTag.startTime + newTag.endTime) / 2.0;

						String textString = "TAGFOUND" + "," + newTag.user_id + "," + newTag.id + "," + newTag.startTime + "," + newTag.endTime + ","
								+ middleTime + "," + highestFrequencyAverageIntensity + "," + peakIntensityFrequency + "," + newTag.minFrequency + ","
								+ newTag.maxFrequency + "," + newTag.tagClass_id + "," + newTag.strength + "," + newTag.machineTagged + "," + newTag.userTagged + ","
								+ newTag.parentTag_id + "," + candidateNesterAudioFile.id + "," + candidateNesterAudioFile.alias + ","
								+ candidateNesterAudioFile.mediaRelativeFilePath;
						textString += "\n";

//						try {
//							bufferedWriter.write(textString);
//							bufferedWriter.flush();
//						} catch (IOException e) {
//							// TODO Auto-generated catch block
//							e.printStackTrace();
//						}

					}

					numberOfTagsDiscovered++;

				}

			}

		} // round


		// ///
		// Save only the best 'N' tags per file (regardless of Class)

		if (( saveBestNTags != null) && ( saveBestNTags > 0)) {
			ArrayList<NesterTag> filteredTags = new ArrayList<NesterTag>();
			NesterTag bestTag = null;
			int bestIndex;

			for (int round = 0; round < saveBestNTags; round++) {
				if (newTags.size() > 0) {
					bestTag = newTags.get(0);
					bestIndex = 0;

					for (int i = 1; i < newTags.size(); i++) {
						if (bestTag.strength < newTags.get(i).strength) {
							bestTag = newTags.get(i);
							bestIndex = i;
						}
					}
					filteredTags.add(newTags.get(bestIndex));
					newTags.remove(bestIndex);
				}
			}

			newTags = filteredTags;
		}
			
		// ///
		// Save only the best 'n' tags per class
		
		if (( saveBestNTagsPerClass != null) && ( saveBestNTagsPerClass > 0)) {

			ArrayList<NesterTag> filteredTags = new ArrayList<NesterTag>();

			// loop over TagClasses we are searching for
			Set<Integer> tagClassIdSet = tagClassIDToMatchQuality.keySet();
			for (Iterator<Integer> iterator = tagClassIdSet.iterator(); iterator.hasNext();) {
				Integer classId = iterator.next();

				for (int round = 0; round < saveBestNTagsPerClass; round++) {
					NesterTag bestTag = null;
					int bestIndex = -1;

					for (int i = 0; i < newTags.size(); i++) {
						NesterTag t = newTags.get(i);

						if (t.tagClass_id == classId) {
							if (bestTag == null) {
								bestTag = t;
								bestIndex = i;
							}

							if (bestTag.strength < t.strength) {
								bestTag = t;
								bestIndex = i;
							}
						}
					}

					if (bestTag != null) {
						filteredTags.add(newTags.get(bestIndex));
						newTags.remove(bestIndex);
					}
				}
			}

			newTags = filteredTags;
		}
			
		// ///

		// ///
		// Save Tags to DB

		int numTagsSaved = newTags.size();
		// either save via DB connection...
		// NesterTag.addTagsToProject(nesterSupervisedTagDiscoveryBias.project_id, newTags, connection);
		// ...or via API
		NesterTag.addTagsToProject(nesterSupervisedTagDiscoveryBias.project_id, newTags, null);
		newTags.clear();

		logger.log(Level.INFO, "### numberOfTagsDiscovered this file = " + numberOfTagsDiscovered + " (Saved " + numTagsSaved + ")");
		nesterSupervisedTagDiscoveryBias.SaveJobLogMessage("numberOfTagsDiscovered this file = " + numberOfTagsDiscovered + " (Saved " + numTagsSaved + ")");

		return true;

	}
}

class SupervisedTagDiscoveryGPU {

	public static class SupervisedTagDiscoveryKernel extends Kernel {
		private final int numProblems;
		private final int[] seedSpectra;
		private final int[] audioFileSpectra;
		private final int[] seedVolumes;
		private final int[] audioFileVolumes;
		private final int[] seedPitches;
		private final int[] audioFilePitches;
		private final float[] correlations;

		private final int numFrequencyBands;
		private final int numSeeds;
		private final int seedSpectraSizes[];
		private final int seedSpectraOffsets[];
		private final int numAudioFileFrames;
		// private final int size2[];

		private final double spectraWeight;
		private final double volumeWeight;
		private final double pitchWeight;

		private Range range;

		public SupervisedTagDiscoveryKernel(int _numProblems, double _spectraWeight, double _volumeWeight, double _pitchWeight, int _numFrequencyBands, int _numSeeds,
				int[] _seedSpectraSizes, int[] _seedSpectraOffsets, int[] _seedSpectra, int _numAudioFileFrames, int[] _audioFileSpectra, int[] _seedVolumes,
				int[] _audioFileVolumes, int[] _seedPitches, int[] _audioFilePitches, float[] _correlations) {

			numProblems = _numProblems;
			spectraWeight = _spectraWeight;
			volumeWeight = _volumeWeight;
			pitchWeight = _pitchWeight;

			Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
			logger.log(Level.INFO, "spectraWeight = " + spectraWeight);
			logger.log(Level.INFO, "volumeWeight  = " + volumeWeight);
			logger.log(Level.INFO, "pitchWeight   = " + pitchWeight);

			numFrequencyBands = _numFrequencyBands;

			numSeeds = _numSeeds;
			seedSpectraSizes = _seedSpectraSizes;
			seedSpectraOffsets = _seedSpectraOffsets;
			seedSpectra = _seedSpectra;

			numAudioFileFrames = _numAudioFileFrames;
			audioFileSpectra = _audioFileSpectra;

			seedVolumes = _seedVolumes;
			audioFileVolumes = _audioFileVolumes;

			seedPitches = _seedPitches;
			audioFilePitches = _audioFilePitches;

			correlations = _correlations;

			setExplicit(true); // This gives us a performance boost

		}

		@Override
		public void run() {

			int problemID = getGlobalId();

			// System.out.println("problemId = " + problemID);

			if (problemID >= numProblems) {
				return;
			}

			int seedIndex = problemID / numAudioFileFrames;

			// System.out.println("seedIndex = " + seedIndex);
			// System.out.println("seedSpectra.length = " + seedSpectra.length);

			int audioFileFrameOffset = problemID % numAudioFileFrames;

			if (numAudioFileFrames - audioFileFrameOffset < seedSpectraSizes[seedIndex] / numFrequencyBands) {
				return;
			}

			// local variables //

			double correlationSum = 0.0f;
			int offsetY;
			double N;
			double sum_sq_x;
			double sum_sq_y;
			double sum_coproduct;
			double mean_x;
			double mean_y;
			int iXOffset;
			int iYOffset;
			double pop_sd_x;
			double pop_sd_y;
			double cov_x_y;
			double correlation;
			int seedSpectraOffset;

			int seedVolumeAndPitchOffset;

			seedSpectraOffset = seedSpectraOffsets[seedIndex];
			seedVolumeAndPitchOffset = seedSpectraOffset / numFrequencyBands;

			// spectra match //

			if (spectraWeight > 0.0) {

				offsetY = audioFileFrameOffset * numFrequencyBands;

				N = seedSpectraSizes[seedIndex];


				int numValid = 0;

				int I = -1;
				mean_x = Double.NaN;
				mean_y = Double.NaN;
				for (int i = 0; (i < N) && (numValid == 0); i++) {
					if (seedSpectra[seedSpectraOffset + i] != Integer.MIN_VALUE) {
						numValid++;
						mean_x = seedSpectra[seedSpectraOffset + i];
						mean_y = audioFileSpectra[offsetY + i];
						I = i + 1;
						// break; // APARAPI doesn't support 'break'
					}

				}

				iXOffset = -1 + seedSpectraOffset;
				iYOffset = -1 + offsetY;

				sum_sq_x = 0;
				sum_sq_y = 0;
				sum_coproduct = 0;
				
				for (int i = I + 1; i <= N; i++) {

					if (seedSpectra[i + iXOffset] != Integer.MIN_VALUE) {

						numValid++;

						double sweep = (i - 1.0f) / i;
						double delta_x = seedSpectra[i + iXOffset] - mean_x;
						double delta_y = audioFileSpectra[i + iYOffset] - mean_y;
						sum_sq_x += delta_x * delta_x * sweep;
						sum_sq_y += delta_y * delta_y * sweep;
						sum_coproduct += delta_x * delta_y * sweep;
						mean_x += delta_x / i;
						mean_y += delta_y / i;

					}

				}

				pop_sd_x = (double) Math.sqrt(sum_sq_x / numValid);
				pop_sd_y = (double) Math.sqrt(sum_sq_y / numValid);
				cov_x_y = sum_coproduct / numValid;
				correlation = cov_x_y / (pop_sd_x * pop_sd_y);

				// System.out.println("spectraWeight correlation = " + correlation);

				correlationSum += spectraWeight * correlation;
			}

			// volume match //

			if (volumeWeight > 0.0) {

				offsetY = audioFileFrameOffset;

				N = seedSpectraSizes[seedIndex] / numFrequencyBands;

				sum_sq_x = 0;
				sum_sq_y = 0;
				sum_coproduct = 0;

				mean_x = seedVolumes[seedVolumeAndPitchOffset];
				mean_y = audioFileVolumes[offsetY];

				iXOffset = -1 + seedVolumeAndPitchOffset;
				iYOffset = -1 + offsetY;
				for (int i = 2; i <= N; i++) {

					double sweep = (i - 1.0f) / i;
					double delta_x = seedVolumes[i + iXOffset] - mean_x;
					double delta_y = audioFileVolumes[i + iYOffset] - mean_y;
					sum_sq_x += delta_x * delta_x * sweep;
					sum_sq_y += delta_y * delta_y * sweep;
					sum_coproduct += delta_x * delta_y * sweep;
					mean_x += delta_x / i;
					mean_y += delta_y / i;
				}

				pop_sd_x = (double) Math.sqrt(sum_sq_x / N);
				pop_sd_y = (double) Math.sqrt(sum_sq_y / N);
				cov_x_y = sum_coproduct / N;
				correlation = cov_x_y / (pop_sd_x * pop_sd_y);

				// System.out.println("volumeWeight correlation = " + correlation);

				correlationSum += volumeWeight * correlation;
			}

			// pitch match //

			if (pitchWeight > 0.0) {

				offsetY = audioFileFrameOffset;

				N = seedSpectraSizes[seedIndex] / numFrequencyBands;

				sum_sq_x = 0;
				sum_sq_y = 0;
				sum_coproduct = 0;

				mean_x = seedPitches[seedVolumeAndPitchOffset];
				mean_y = audioFilePitches[offsetY];

				iXOffset = -1 + seedVolumeAndPitchOffset;
				iYOffset = -1 + offsetY;
				for (int i = 2; i <= N; i++) {

					double sweep = (i - 1.0f) / i;
					double delta_x = seedPitches[i + iXOffset] - mean_x;
					double delta_y = audioFilePitches[i + iYOffset] - mean_y;
					sum_sq_x += delta_x * delta_x * sweep;
					sum_sq_y += delta_y * delta_y * sweep;
					sum_coproduct += delta_x * delta_y * sweep;
					mean_x += delta_x / i;
					mean_y += delta_y / i;
				}

				pop_sd_x = (double) Math.sqrt(sum_sq_x / N);
				pop_sd_y = (double) Math.sqrt(sum_sq_y / N);
				cov_x_y = sum_coproduct / N;
				correlation = cov_x_y / (pop_sd_x * pop_sd_y);

				// System.out.println("pitchWeight correlation = " + correlation);

				correlationSum += pitchWeight * correlation;
			}

			correlations[seedIndex * numAudioFileFrames + audioFileFrameOffset] = (float) correlationSum;

		}

		public void putAudioFileDataOnGPU() {

			if (spectraWeight > 0.0)
				put(audioFileSpectra); // Because we are using explicit buffer management
			if (volumeWeight > 0.0)
				put(audioFileVolumes); // Because we are using explicit buffer management
			if (pitchWeight > 0.0)
				put(audioFilePitches); // Because we are using explicit buffer management

		}

		public void nextGeneration(int numProblems) {

			Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

			// // int numProblems = (size2[0] - size1[0]) + 1;
			// int numProblems = (size2[0] - size1[0]) + 1;

			logger.log(Level.INFO, "numProblems = " + numProblems);

			// int groupSize = 1; // for SEQ debugging
			int groupSize = 256;
			int expandedNumProblems = numProblems / groupSize * groupSize;
			if (expandedNumProblems < numProblems) {
				expandedNumProblems += groupSize;
			}

			logger.log(Level.INFO, "expandedNumProblems = " + expandedNumProblems);

			range = Range.create(expandedNumProblems, groupSize);

			logger.log(Level.INFO, "range = " + range);
			logger.log(Level.INFO, "range.getWorkGroupSize() = " + range.getWorkGroupSize());

			put(seedSpectraOffsets);
			if (spectraWeight > 0.0)
				put(seedSpectra);

			if (volumeWeight > 0.0)
				put(seedVolumes);

			if (pitchWeight > 0.0)
				put(seedPitches);

			execute(range);
			get(correlations); // Because we are using explicit buffer management

		}

	}

	static boolean running = false;

}

class BandPassGPU {

	public static class BandPassKernel extends Kernel {

		private final static boolean debugMode = BandPass.gpuDebugMode;
		private final int numProblems;
		private final double dampingRatio;
		private final double minFrequency;
		private final double maxFrequency;
		private final int numFrequencyBands;
		private final double samplingRate;
		private final int numSamplesPerFrame;
		private final int numSamplesPerChunk;
		private final int numChunks;
		private final int numSamplesForConditioning;
		private final int audioNumTimeSlices;
		private final int[] audioSamples;
		private final int[] audioSpectra;

		private Range range;

		public BandPassKernel(boolean _debugMode, int _numProblems, double _dampingRatio, double _minFrequency, double _maxFrequency, int _numFrequencyBands, double _samplingRate,
				int _numSamplesPerFrame, int _numSamplesPerChunk, int _numChunks, int _numSamplesForConditioning, int _audioNumTimeSlices, int[] _audioSamples, int[] _audioSpectra) {

			Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

			// debugMode = _debugMode;
			numProblems = _numProblems;
			dampingRatio = _dampingRatio;
			minFrequency = _minFrequency;
			maxFrequency = _maxFrequency;
			samplingRate = _samplingRate;
			numFrequencyBands = _numFrequencyBands;
			numSamplesPerFrame = _numSamplesPerFrame;
			numSamplesPerChunk = _numSamplesPerChunk;
			numChunks = _numChunks;
			numSamplesForConditioning = _numSamplesForConditioning;
			audioNumTimeSlices = _audioNumTimeSlices;
			audioSamples = _audioSamples;
			audioSpectra = _audioSpectra;

			logger.log(Level.INFO, "numProblems               = " + numProblems);
			logger.log(Level.INFO, "dampingRatio              = " + dampingRatio);
			logger.log(Level.INFO, "minFrequency              = " + minFrequency);
			logger.log(Level.INFO, "maxFrequency              = " + maxFrequency);
			logger.log(Level.INFO, "numFrequencyBands         = " + numFrequencyBands);
			logger.log(Level.INFO, "numSamplesPerFrame        = " + numSamplesPerFrame);
			logger.log(Level.INFO, "numSamplesPerChunk        = " + numSamplesPerChunk);
			logger.log(Level.INFO, "numChunks                 = " + numChunks);
			logger.log(Level.INFO, "numSamplesForConditioning = " + numSamplesForConditioning);
			logger.log(Level.INFO, "audioSpectra              = " + audioSpectra);

			setExplicit(true); // This gives us a performance boost

		}

		@Override
		public void run() {

			int problemID = getGlobalId();

			// System.out.println("problemId = " + problemID);

			if (problemID >= numProblems) {
				return;
			}

			int problemChunkIndex = problemID / numFrequencyBands;
			int problemBandIndex = problemID % numFrequencyBands;

			if (debugMode) {
				Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
				logger.log(Level.INFO, "problemBandIndex = " + problemBandIndex);
				logger.log(Level.INFO, "problemChunkIndex = " + problemChunkIndex);
			}

			/**************/
			/* INITIALIZE */
			/**************/

			double bandToBandFactor = Math.exp(Math.log(maxFrequency / minFrequency) / (numFrequencyBands - 1));

			double startBandFrequency = 0.0;

			if (numFrequencyBands > 1) {
				startBandFrequency = minFrequency;
			} else {
				startBandFrequency = (minFrequency + maxFrequency) / 2.0;
			}

			double bandFrequency = 0.0;

			double a1 = 0.0;
			double a2 = 0.0;
			double b1 = 0.0;
			double b2 = 0.0;
			double n0 = 0.0;
			double n1 = 0.0;
			double n2 = 0.0;

			double lastBandSignal = 0.0;

			// keneticEnergySums = new double[numBands];
			// potentialEnergySums = new double[numBands];

			double wn = 0.0;
			double wd = 0.0;
			double y1 = 0.0;
			double y2 = 0.0;
			double y3 = 0.0;
			double a = 0.0;

			for (int b = 0; b < numFrequencyBands; b++) {

				if (b == problemBandIndex) {

					bandFrequency = startBandFrequency;

					wn = (2.0 * Math.PI * bandFrequency);
					wd = (wn * Math.sqrt(1.0 - dampingRatio * dampingRatio));
					a = (dampingRatio * wn);

					double t = 0.0;

					t = (1.0 / samplingRate);
					y1 = ((2.0 * a / wd) * Math.exp(-a * t) * Math.sin(wd * t));
					t = (2.0 / samplingRate);
					y2 = ((2.0 * a / wd) * Math.exp(-a * t) * Math.sin(wd * t));
					t = (3.0 / samplingRate);
					y3 = ((2.0 * a / wd) * Math.exp(-a * t) * Math.sin(wd * t));

					a1 = (y2 / y1);
					a2 = ((y1 * y3 - y2 * y2) / (y1 * y1));
					b1 = (y1);
					b2 = (-y1);
					n0 = (0);
					n1 = (0);
					n2 = (0);
				}

				// System.out.println("bandFrequency = " + bandFrequency);

				startBandFrequency *= bandToBandFactor;

			}

			lastBandSignal = 0.0;

			/***********/
			/* COMPUTE */
			/***********/

			int numTimeSlicesPerChunk = numSamplesPerChunk / numSamplesPerFrame;

			int problemNumTimeSlices = -1;

			if (problemChunkIndex < numChunks - 1) {
				problemNumTimeSlices = numTimeSlicesPerChunk;
			} else {
				problemNumTimeSlices = audioNumTimeSlices % numTimeSlicesPerChunk;
				if (problemNumTimeSlices == 0)
					problemNumTimeSlices = numTimeSlicesPerChunk;

			}

			int offset = problemChunkIndex * numSamplesPerChunk;
			// int offset = 0; /* !!! */

			for (int t = 0; t < problemNumTimeSlices; t++) {

				int i;
				// double n0, n1, n2, a1, a2, b1, b2;
				double bandSignal, deltaBandSignal;
				// double twoLogKeneticEnergySum, twoLogPotentialEnergySum;

				double twoLogEnergySum = 0.0;

				bandSignal = 0.0f;

				int start_i = t * numSamplesPerFrame + offset;
				int end_i = start_i + numSamplesPerFrame;
				for (i = start_i; i < end_i; i++) {
					n2 = n1;
					n1 = n0;

					// try {
					n0 = audioSamples[i] + a1 * n1 + a2 * n2;
					// } catch (Exception e) {
					// // TODO Auto-generated catch block
					// e.printStackTrace();
					// System.exit(1);
					// }

					bandSignal = n1 * b1 + n2 * b2;
					deltaBandSignal = lastBandSignal - bandSignal;
					lastBandSignal = bandSignal;

					if (deltaBandSignal < 0)
						twoLogEnergySum -= deltaBandSignal;
					else
						twoLogEnergySum += deltaBandSignal;

					if (bandSignal < 0)
						twoLogEnergySum -= bandSignal;
					else
						twoLogEnergySum += bandSignal;

				}

				lastBandSignal = bandSignal;

				audioSpectra[problemBandIndex * audioNumTimeSlices + problemChunkIndex * (numTimeSlicesPerChunk) + t] = (int) (twoLogEnergySum);

			}

		}

		public void putAudioFileDataOnGPU() {

			put(audioSamples); // Because we are using explicit buffer management

		}

		public void nextGeneration(int numProblems) {

			Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

			logger.log(Level.INFO, "numProblems = " + numProblems);

			int groupSize = -1;

			if (this.getExecutionMode() == EXECUTION_MODE.SEQ) {
				groupSize = 1;
			} else {
				groupSize = 256;
			}

			// int groupSize = 1; // for SEQ debugging
			int expandedNumProblems = numProblems / groupSize * groupSize;
			if (expandedNumProblems < numProblems) {
				expandedNumProblems += groupSize;
			}

			logger.log(Level.INFO, "expandedNumProblems = " + expandedNumProblems);

			range = Range.create(expandedNumProblems, groupSize);

			logger.log(Level.INFO, "range = " + range);
			logger.log(Level.INFO, "range.getWorkGroupSize() = " + range.getWorkGroupSize());

			put(audioSamples); // Because we are using explicit buffer management
			execute(range);
			get(audioSpectra); // Because we are using explicit buffer management

		}

	}

	static boolean running = false;

}
