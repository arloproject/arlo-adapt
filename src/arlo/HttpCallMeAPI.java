package arlo;

import arlo.ServiceHead;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import java.io.IOException;
import java.io.StringWriter;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.util.ajax.JSON;


/** Jetty HTTP Server for providing a simple API to the callMe function. 
 *  This class sets up an HTTP server to receive simple POST requests on a 
 *  single URL, callMe/. It passes these requests to the HttpCallMeAPIHandler 
 *  for processing.
 */

class HttpCallMeAPIServer {
	int _serverPort;          //!< TCP port of this server 
	boolean _initialized;     //!< Has this server been initialized 
	ServiceHead _servicehead; //!< Used to provide a callback to the callMe function 
	Server _server;           //!< The Jetty server 
	
	/** Default Constructor */
	public void HttpCallMeAPIServer() {
		_initialized = false;
		_servicehead = null;
		_server = null;
	}

	/** Initialize (start) the Server. 
	 *  Create a Jetty server, build the handler, start it, and 
	 *  let it run in the background.
	 *  \param ServiceHead A reference to the ServiceHead instance that started
	 *  the server. Used to call back to callMe().
	 *  \param port The TCP port on which to start the server. 
	 *  \return true on success, false if an error
	 */
	
	public boolean Initialize(ServiceHead serviceHead, int port) {
		_servicehead = serviceHead;
		_serverPort = port;

		try {
			_server = new Server(port);

			//ContextHandler context = new ContextHandler("/");
			// context.setContextPath("/");
			//context.setHandler(new HelloHandler());

			ContextHandler contextApi = new ContextHandler("/callMe");
			contextApi.setHandler(new HttpCallMeAPIHandler(serviceHead));

			ContextHandlerCollection contexts = new ContextHandlerCollection();
			contexts.setHandlers(new Handler[] { contextApi});
			//contexts.setHandlers(new Handler[] { context, contextApi});
			_server.setHandler(contexts);

			_server.start();
			//server.join();  
			return true;
		} catch (InterruptedException e) {
			System.out.println("JETTY InterruptedException HANDLER HIT - HttpCallMeAPIServer");
			return false;
		} catch (Exception e) {
			System.out.println("JETTY EXCEPTION HANDLER HIT - HttpCallMeAPIServer");
			return false;
		}
	}

}

///////
//Generic example handler 

//class HelloHandler extends AbstractHandler {
//    public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
//        response.setContentType("text/html;charset=utf-8");
//        response.setStatus(HttpServletResponse.SC_OK);
//        baseRequest.setHandled(true);
//        response.getWriter().println("HTTP ServiceHead saying Hello");
//    }
//}

/** Jetty Response Handler for the callMe API. 
 *  Parse an HTTP request from the Jetty server to the callMe function. 
 *  Build a dictionary of request parameters, pass them to callMe(), and 
 *  return the response to the client. 
 */

class HttpCallMeAPIHandler extends AbstractHandler {
    ServiceHead _servicehead;

	/** Default Constructor */
    public HttpCallMeAPIHandler() {
        _servicehead = null;
    }

	/** Constructor. 
	 *  \param servicehead Reference the the ServiceHead instance that 
	 *  started the server. Used to call back to callMe().
	 */
    public HttpCallMeAPIHandler(ServiceHead servicehead) {
        _servicehead = servicehead;
    }

	/** Handler for the callMe API request. 
	 *  Parse an HTTP request from the Jetty server to the callMe function.
	 *  Build a dictionary of request parameters, pass them to callMe(), and
	 *  return the response to the client.
	 *  \param target The target of the request - either a URI or a name (from Jetty docs).
	 *  \param baseRequest The original unwrapped request object (from Jetty docs).
	 *  \param request The request either as the Request object or a wrapper of that request (from Jetty docs).
	 *  \param response The response as the Response object or a wrapper of that request (from Jetty docs).
	 */
    public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

        // Handler for /callMe/ HTTP api

        // only allow the URL '/callMe/', not /callMe/*'
        if ( ! (request.getRequestURI().equals("/callMe/"))) {
            return;
        }

        // must be a POST
        if (request.getMethod() != "POST") {
            response.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
            baseRequest.setHandled(true);
            return;
        }

        if (_servicehead == null) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR );
            response.getWriter().println("ServiceHead null");
            baseRequest.setHandled(true);
            return;
        }

        // response.setContentType("text/html;charset=utf-8");
        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_OK);
        baseRequest.setHandled(true);

        // build request dictionary
        HashMap<String, String> req = new HashMap<String, String>();
        Map<String, String[]> params = request.getParameterMap();
        for (String k : params.keySet()) {
            String [] values = params.get(k);
            if (values.length != 1) {
                response.getWriter().println("Paratmeters must have one value per key");
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }
            req.put(k, values[0]);
        }

        // we have all of the request parameters and a valid ServiceHead
        // ...so callMe() maybe...
        HashMap<String, Object> resp;
        try {
            resp = _servicehead.callMe(req);
        } catch (Exception e) {

			StringWriter writer = new StringWriter();
			PrintWriter printWriter = new PrintWriter( writer );
			e.printStackTrace( printWriter );
			printWriter.flush();
			String stackTrace = writer.toString();
			logger.log(Level.SEVERE, "HttpCallMeAPI::handle() Failed Exception" + e + " -- " + stackTrace);

            response.getWriter().println("Error in Java CallMe!");
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return;
        }

		if (resp != null) { 
	        // convert response to JSON
    	    JSON conv = new JSON();
        	response.getWriter().println(conv.toJSON(resp));
		} else {
            System.out.println("Null Response in Java CallMe!");
            response.getWriter().println("Null Response in Java CallMe!");
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return;
		}
    }

}

