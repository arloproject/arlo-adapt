package arlo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Vector;

import com.kenai.jffi.Array;


import java.sql.Connection;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.StringWriter;
import java.io.PrintWriter;

import arlo.QueueTask;
import arlo.NesterUser;
import arlo.NesterJob;
import arlo.NesterAudioFile;
import arlo.NesterTagSet;
import arlo.NesterTag;

import arlo.ExpertAgreement;

public class QueueTask_ExpertAgreementClassification extends QueueTask {

	static final private String taskName = "ExpertAgreementClassification";

	public boolean runQueue(Vector<NesterUser> restrictedUsers) {
		return super._runQueue(taskName, restrictedUsers);
	}

    void runJob(NesterJob job) {

		if (job == null) {
			System.out.println("QueueTask_ExpertAgreementClassification::runJob received null job");
			return;
		}

		System.out.println("Starting QueueTask_ExpertAgreementClassification::runJob jobId = " + job.id);

		Connection connection = QueueTask.openDatabaseConnection();

		if (RunJob(job, connection)) {
			setJobStatusComplete(job.id, connection);
		} else {
			setJobStatusError(job.id, connection);  // error occured
		}

		closeDatabaseConnection(connection);
	}


	/** \brief Run an 'ExpertAgreementClassification' task
	 *
	 * @param job the NesterJob object of the job.
	 * @param connection Opened database connection.
	 * @return true if success, false if any error encountered. 
	 */
	
	boolean RunJob(NesterJob job, Connection connection) {
		Logger logger;
		logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	
		//////////////////////////
		//                      //
		//  Get Job Parameters  //
		//                      //
		//////////////////////////

logger.log(Level.INFO, "Retrieving MediaFiles");
		// Get the Audio Files to Search
		Vector<NesterAudioFile> audioFilesToClassify;

		HashMap<String, Boolean> fieldNames = new HashMap<String, Boolean>();
		fieldNames.put("active", true);
		fieldNames.put("durationInSeconds", true);
		fieldNames.put("bitsPerSample", true);
		fieldNames.put("nBlockAlign", true);
		fieldNames.put("dataStartIndex", true);
		fieldNames.put("sampleRate", true);
		fieldNames.put("numChannels", true);
		fieldNames.put("dataSize", true);

		audioFilesToClassify = NesterProject.getNesterProjectAudioFiles(job.project_id, false, fieldNames, connection);

logger.log(Level.INFO, "Retrieving Soure TagSets");
		// Get the Source TagSets
		String value;
		Vector<Integer> sourceTagSetIds = null;
		value = job.GetJobParameter("sourceTagSets");
		if (value != null) {
			sourceTagSetIds = new Vector<Integer>();
			// sourceTagSets is a comma separated string
			// split and create a Vector of values
			for (String ts : value.split(",")) {
				sourceTagSetIds.add(Integer.parseInt(ts));
			}
		} else {
			job.SaveJobLogMessage("Missing Parameter 'sourceTagSets'");
			return false;
		}
		

logger.log(Level.INFO, "Retrieving Known Tags");
		// Get the known Tags
		Vector<NesterTag> knownTags = NesterTagSet.getNesterTagSetTags(sourceTagSetIds, null, connection);

		// Get the distanceWeightingPower
		double distanceWeightingPower;
		value = job.GetJobParameter("distanceWeightingPower");
		if (value != null) {
			distanceWeightingPower = Double.parseDouble(value);
		} else {
			job.SaveJobLogMessage("Missing Parameter 'distanceWeightingPower'");
			return false;
		}


		// Get the classificationProbabilityThreshold
		double classificationProbabilityThreshold;
		value = job.GetJobParameter("classificationProbabilityThreshold");
		if (value != null) {
			classificationProbabilityThreshold = Double.parseDouble(value);
		} else {
			job.SaveJobLogMessage("Missing Parameter 'classificationProbabilityThreshold'");
			return false;
		}


		////
		// Get Spectra Parameters

logger.log(Level.INFO, "Retrieving Known Spectra Details");

		int numFrequencyBands;
		double numTimeFramesPerSecond;
		double dampingRatio;
		double minFrequency;
		double maxFrequency;

		value = job.GetJobParameter("numFrequencyBands");
		if (value != null) {
			numFrequencyBands = Integer.parseInt(value);
		} else {
			job.SaveJobLogMessage("Missing Parameter 'numFrequencyBands'");
			return false;
		}
		
		value = job.GetJobParameter("numTimeFramesPerSecond");
		if (value != null) {
			numTimeFramesPerSecond = Double.parseDouble(value);
		} else {
			job.SaveJobLogMessage("Missing Parameter 'numTimeFramesPerSecond'");
			return false;
		}
		
		value = job.GetJobParameter("dampingRatio");
		if (value != null) {
			dampingRatio = Double.parseDouble(value);
		} else {
			job.SaveJobLogMessage("Missing Parameter 'dampingRatio'");
			return false;
		}
		
		value = job.GetJobParameter("minFrequency");
		if (value != null) {
			minFrequency = Double.parseDouble(value);
		} else {
			job.SaveJobLogMessage("Missing Parameter 'minFrequency'");
			return false;
		}
		
		value = job.GetJobParameter("maxFrequency");
		if (value != null) {
			maxFrequency = Double.parseDouble(value);
		} else {
			job.SaveJobLogMessage("Missing Parameter 'maxFrequency'");
			return false;
		}


		//////////////////////////////
		//                          //
		//  Run the Classification  //
		//                          //
		//////////////////////////////

		try {
	
			ExpertAgreement ea = new ExpertAgreement();

			String[] args = new String[] { "learn", "1", "10.0", "0.4" };	

			if (ea.run(
					args, 
					job.id,
					knownTags, 
					audioFilesToClassify, 
					distanceWeightingPower,
					classificationProbabilityThreshold,
					numFrequencyBands, 
					numTimeFramesPerSecond, 
					dampingRatio, 
					minFrequency, 
					maxFrequency,
					connection
			)) {
				logger.log(Level.INFO, "Success running QueueTaskRun()");
				return true;
			} else {
				logger.log(Level.SEVERE, "Failed running QueueTaskRun()");
				return false;
			}
		} catch (Exception e) {
			StringWriter writer = new StringWriter();
			PrintWriter printWriter = new PrintWriter( writer );
			e.printStackTrace( printWriter );
			printWriter.flush();
			String stackTrace = writer.toString();
	
			logger.log(Level.SEVERE, "QueueTask_ExpertAgreement::runJob Failed Exception" + e + " -- " + stackTrace);
			return false;
		}
	}

}

