package arlo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Iterator;

public class NesterUnsupervisedTagDiscoveryBias extends TagDiscoveryBias {


	Vector<Integer> sourceTagSets;
	int destinationTagSetId;

	public int numberOfClusters;
	public int numberOfClustersToSave;
	public int numberOfExamplesPerCluster;
	public double windowSizeInSeconds;

	// SELECTION BIAS
	public boolean exchangeMode;
	public boolean moveMode;
	public int numOptimizationIterations;
	
	public int minNumSamples;

	public NesterUnsupervisedTagDiscoveryBias() {
		
		this.destinationTagSetId = 0;
		this.sourceTagSets = null;

		this.id = 0;
		this.user_id = 0;
		this.isRunning = false;
		this.isComplete = false;
		this.numCompleted = 0;
		this.fractionCompleted = 0;
		this.name = "";

		this.numberOfClusters = 0;
		this.numberOfClustersToSave = 0;
		this.numberOfExamplesPerCluster = 0;
		this.windowSizeInSeconds = 0;

		this.numFrequencyBands = 0;
		this.numTimeFramesPerSecond = 0;
		this.dampingRatio = 0;
		this.minFrequency = 0;
		this.maxFrequency = 0;

		this.exchangeMode = false;
		this.moveMode = false;
		this.numOptimizationIterations = 0;
		
		this.minNumSamples = 0;
	}

	NesterUnsupervisedTagDiscoveryBias(int id, int user_id, boolean isRunning, boolean isComplete, int numCompleted, double fractionCompleted, String name,
			int numberOfClusters, int numberOfClustersToSave, int numberOfExamplesPerCluster, double windowSizeInSeconds, int numFrequencyBands, double numTimeFramesPerSecond, double dampingRatio, double minFrequency, double maxFrequency,
			boolean exchangeMode, boolean moveMode, int numOptimizationIterations, int minNumSamples) {

		this.id = id;
		this.user_id = user_id;
		this.isRunning = isRunning;
		this.isComplete = isComplete;
		this.numCompleted = numCompleted;
		this.fractionCompleted = fractionCompleted;
		this.name = name;

		this.numberOfClusters = numberOfClusters;
		this.numberOfClustersToSave = numberOfClustersToSave;
		this.numberOfExamplesPerCluster = numberOfExamplesPerCluster;
		this.windowSizeInSeconds = windowSizeInSeconds;

		this.numFrequencyBands = numFrequencyBands;
		this.numTimeFramesPerSecond = numTimeFramesPerSecond;
		this.dampingRatio = dampingRatio;
		this.minFrequency = minFrequency;
		this.maxFrequency = maxFrequency;

		this.exchangeMode = exchangeMode;
		this.moveMode = moveMode;
		this.numOptimizationIterations = numOptimizationIterations;
		this.minNumSamples = minNumSamples;

	}


// 	public NesterUnsupervisedTagDiscoveryBias getNesterUnsupervisedTagDiscoveryBias(int tagDiscoveryBiasID) {
// 
// 		NesterUnsupervisedTagDiscoveryBias tagDiscoveryBias = null;
// 
// 		try {
// 
// 			Statement s1 = connection.createStatement();
// 			s1.executeQuery("select * from tools_unsupervisedtagdiscoverybias where id = " + tagDiscoveryBiasID);
// 			ResultSet rs1 = s1.getResultSet();
// 
// 			if (rs1.next()) {
// 
// 				tagDiscoveryBias = new NesterUnsupervisedTagDiscoveryBias(rs1.getInt("id"), rs1.getInt("user_id"), rs1.getInt("project_id"), rs1.getBoolean("isRunning"),
// 						rs1.getBoolean("isComplete"), rs1.getInt("numCompleted"), rs1.getDouble("fractionCompleted"), rs1.getString("name"), rs1.getInt("numberOfClusters"),
// 						rs1.getInt("numberOfClustersToSave"), rs1.getInt("numberOfExamplesPerCluster"), rs1.getDouble("windowSizeInSeconds"), rs1.getInt("numFrequencyBands"),
// 						rs1.getDouble("numTimeFramesPerSecond"), rs1.getDouble("dampingRatio"), rs1.getDouble("minFrequency"), rs1.getDouble("maxFrequency"),
// 						rs1.getBoolean("exchangeMode"), rs1.getBoolean("moveMode"), rs1.getInt("numOptimizationIterations"));
// 
// 			}
// 			rs1.close();
// 			s1.close();
// 
// 		} catch (Exception e) {
// 			logger.log(Level.INFO, "name not found");
// 			logger.log(Level.INFO, "Exception = " + e);
// 		}
// 
// 		return tagDiscoveryBias;
// 	}




	public boolean getNesterUnsupervisedTagDiscoveryBias(int id, Connection connection) {

		Logger logger;
		logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		/////////////////
		// get Job info

		if (! this.getNesterJob(id, connection)) {
			logger.log(Level.WARNING, "Failed to retrieve job");
			return false;
		}
		
		
		//////////////////////
		// Get Job Parameters
		
		logger.log(Level.INFO, "Get Job Params");	

		boolean gotParameters = true;
		String value;
		
		value = this.GetJobParameter("sourceTagSets");
		if (value != null) {
			this.sourceTagSets = new Vector<Integer>();
			// sourceTagSets is a comma separated string
			// split and create a Vector of values
			for (String ts : value.split(",")) {
				this.sourceTagSets.add(Integer.parseInt(ts));
			}
		} else {
			gotParameters = false;
		}
		
		value = this.GetJobParameter("destinationTagSet");
		if (value != null) {
			this.destinationTagSetId = Integer.parseInt(value);
		} else {
			gotParameters = false;
		}
		
		value = this.GetJobParameter("numberOfClusters");
		if (value != null) {
			this.numberOfClusters = Integer.parseInt(value);
		} else {
			gotParameters = false;
		}

//		value = this.GetJobParameter("numberOfClustersToSave");
//		if (value != null) {
//			this.numberOfClustersToSave = Integer.parseInt(value);
//		} else {
//			gotParameters = false;
//		}

//		value = this.GetJobParameter("numberOfExamplesPerCluster");
//		if (value != null) {
//			this.numberOfExamplesPerCluster = Integer.parseInt(value);
//		} else {
//			gotParameters = false;
//		}

//		value = this.GetJobParameter("windowSizeInSeconds");
//		if (value != null) {
//			this.windowSizeInSeconds = Double.parseDouble(value);
//		} else {
//			gotParameters = false;
//		}
	
		value = this.GetJobParameter("numFrequencyBands");
		if (value != null) {
			this.numFrequencyBands = Integer.parseInt(value);
		} else {
			gotParameters = false;
		}
		
		value = this.GetJobParameter("numTimeFramesPerSecond");
		if (value != null) {
			this.numTimeFramesPerSecond = Double.parseDouble(value);
		} else {
			gotParameters = false;
		}
		
		value = this.GetJobParameter("dampingRatio");
		if (value != null) {
			this.dampingRatio = Double.parseDouble(value);
		} else {
			gotParameters = false;
		}
		
		value = this.GetJobParameter("minFrequency");
		if (value != null) {
			this.minFrequency = Double.parseDouble(value);
		} else {
			gotParameters = false;
		}
		
		value = this.GetJobParameter("maxFrequency");
		if (value != null) {
			this.maxFrequency = Double.parseDouble(value);
		} else {
			gotParameters = false;
		}

		value = this.GetJobParameter("exchangeMode");
		if (value != null) {
			this.exchangeMode = Boolean.parseBoolean(value);
		} else {
			gotParameters = false;
		}
		
		value = this.GetJobParameter("moveMode");
		if (value != null) {
			this.moveMode = Boolean.parseBoolean(value);
		} else {
			gotParameters = false;
		}

        // looks like parseInt fails for strings that have a '.', e.g., 2.0 
		value = this.GetJobParameter("numOptimizationIterations");
		if (value != null) {
			this.numOptimizationIterations = Integer.parseInt(value);
		} else {
			gotParameters = false;
		}
		
		if (gotParameters == false) {
			logger.log(Level.INFO, "Not all Job Parameters were found");
			return false;
		}

		value = this.GetJobParameter("minNumSamples");
		if (value != null) {
			this.minNumSamples = Integer.parseInt(value);
		} else {
			gotParameters = false;
		}

		
		
		
		/*                       */
		/* Temp Debug Code       */
		/* Dump all read values  */
		/*                       */
		
		value = this.GetJobParameter("sourceTagSets");
		if (value != null) {
			this.sourceTagSets = new Vector<Integer>();
			// sourceTagSets is a comma separated string
			// split and create a Vector of values
			for (String ts : value.split(",")) {
				this.sourceTagSets.add(Integer.parseInt(ts));
			}
		} else {
			gotParameters = false;
		}
		
		System.out.print("\n\tsourceTagSets:");
		Iterator<Integer> it = sourceTagSets.iterator();
		while (it.hasNext())
			System.out.print(it.next() + ", ");
		
		System.out.println("" + 
				"\n\tdestinationTagSet: " + destinationTagSetId + 
				"\n\tnumberOfClusters: " + numberOfClusters + 
//				"\n\tnumberOfExamplesPerCluster: " + numberOfExamplesPerCluster + 
				"\n\tnumFrequencyBands: " + numFrequencyBands + 
				"\n\tnumTimeFramesPerSecond: " + numTimeFramesPerSecond + 
				"\n\tdampingRatio: " + dampingRatio + 
				"\n\tminFrequency: " + minFrequency + 
				"\n\tmaxFrequency: " + maxFrequency + 
				"\n\texchangeMode: " + exchangeMode + 
				"\n\tmoveMode: " + moveMode + 
				"\n\tnumOptimizationIterations: " + numOptimizationIterations +
				"\n\tminNumSamples: " + minNumSamples +
				"\n");
		
		return true;
		
	}







}
