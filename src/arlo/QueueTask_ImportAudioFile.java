package arlo;

import java.util.Vector;
import java.io.File;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Time;
import java.sql.SQLException;


import arlo.NesterUser;
import arlo.NesterAudioFile;

import arlo.QueueTask;

public class QueueTask_ImportAudioFile extends QueueTask {

	static final private String taskName = "ImportAudioFile";

	public boolean runQueue(
		Vector<NesterUser> restrictedUsers)
	{
		return super._runQueue(taskName, restrictedUsers);
	}

	void runJob(NesterJob job) {

		if (job == null) {
			System.out.println("QueueTask_ImportAudioFile::runJob received null job");
			return;
		}

		System.out.println("Starting QueueTask_ImportAudioFile::runJob jobId = " + job.id);

		Connection connection = QueueTask.openDatabaseConnection();

		try {

			////////
			// get JobParameters

			// get user
			NesterUser nesterUser = NesterUser.getNesterUser(job.user_id, connection);
			if (nesterUser == null) {
				System.out.println("Error retrieving NesterUser");
				job.SaveJobLogMessage("Error retrieving NesterUser");
				setJobStatusError(job.id, connection);
				closeDatabaseConnection(connection);
				return;
			}

			// job parameters

			// relative to the mediaRootDirectoryPath.
			// Note: if the file is being converted, this is the destination

			String mediaRelativeFilePath = job.GetJobParameter("mediaRelativeFilePath");
			if (mediaRelativeFilePath == null) {
				System.out.println("Missing mediaRelativeFilePath");
				job.SaveJobLogMessage("Missing mediaRelativeFilePath");
				setJobStatusError(job.id, connection);
				closeDatabaseConnection(connection);
				return;
			}

			// relative to the user's media directory. 
			// If provided, this source file is converted to a new file, at the mediaRelativeFilePath
			String sourceRelativeFilePath = job.GetJobParameter("sourceRelativeFilePath");
			String alias            = job.GetJobParameter("alias");
			String outputBits       = job.GetJobParameter("outputBits");
			String outputSampleRate = job.GetJobParameter("outputSampleRate");

			// Project ID
			String sProjectId = job.GetJobParameter("projectId");
			if (sProjectId == null) {
				System.out.println("Missing projectId");
				job.SaveJobLogMessage("Missing projectId");
				setJobStatusError(job.id, connection);
				closeDatabaseConnection(connection);
				return;
			}
			
			int projectId = Integer.parseInt(sProjectId);
			if (projectId <= 0) {
				System.out.println("projectId <= 0");
				job.SaveJobLogMessage("projectId <= 0");
				setJobStatusError(job.id, connection);
				closeDatabaseConnection(connection);
				return;
			}
			
			// Library ID
			String sLibraryId = job.GetJobParameter("libraryId");
			if (sLibraryId == null) {
				System.out.println("Missing libraryId");
				job.SaveJobLogMessage("Missing libraryId");
				setJobStatusError(job.id, connection);
				closeDatabaseConnection(connection);
				return;
			}
			
			int libraryId = Integer.parseInt(sLibraryId);
			if (libraryId <= 0) {
				System.out.println("libraryId <= 0");
				job.SaveJobLogMessage("libraryId <= 0");
				setJobStatusError(job.id, connection);
				closeDatabaseConnection(connection);
				return;
			}
			

			// build absolute file paths
			String absoluteFilePath = ArloSettings.mediaRootDirectoryPath + "/" + mediaRelativeFilePath;
			String sourceAbsoluteFilePath = null;
			if (sourceRelativeFilePath != null) {
				sourceAbsoluteFilePath = ArloSettings.mediaRootDirectoryPath + "/" + sourceRelativeFilePath;
			}


			///////
			// convert file with SoX

			if (sourceAbsoluteFilePath != null) {

				// Test if file already exists, fail if it does
				if ( (new File(absoluteFilePath)).exists() ) {
					System.out.println("File already exists - bailing out");
					job.SaveJobLogMessage("Destination file already exists - bailing out");
					setJobStatusError(job.id, connection);
					closeDatabaseConnection(connection);
					return;
				}

				Vector<String> soxCommand = new Vector<String>();

				soxCommand.add("sox");
				soxCommand.add(sourceAbsoluteFilePath);
				if (outputBits != null) {
					soxCommand.add("-b");
					soxCommand.add(outputBits);
				}
				if (outputSampleRate != null) {
					soxCommand.add("-r");
					soxCommand.add(outputSampleRate);
				}
				soxCommand.add(absoluteFilePath);

				ProcessBuilder pb = new ProcessBuilder(soxCommand);

				System.out.println(pb.command());
				job.SaveJobLogMessage("SoX Command: " + pb.command());

				Process p = pb.start();
				p.waitFor();
				int exitValue = p.exitValue();
				System.out.println("SoX Exit Value: " + exitValue);
				job.SaveJobLogMessage("SoX Exit Value: " + exitValue);

				if (exitValue != 0) {
					setJobStatusError(job.id, connection);
					closeDatabaseConnection(connection);
					return;
				}
			}

			/////
			// import file

			if (! (new File(absoluteFilePath)).exists() ) {
				System.out.println("absoluteFilePath does not exist - bailing out");
				job.SaveJobLogMessage("absoluteFilePath does not exist - bailing out");
				setJobStatusError(job.id, connection);
				closeDatabaseConnection(connection);
				return;
			}
			
			if (! NesterAudioFile.importAudioFile(nesterUser.name, nesterUser.id, projectId, libraryId, alias, mediaRelativeFilePath, connection) ) {
				System.out.println("Failed importing audio file.");
				job.SaveJobLogMessage("Failed importing audio file.");
				setJobStatusError(job.id, connection);
				closeDatabaseConnection(connection);
				return;
			}
			
			////
			// delete source file if this was a conversion
			if (sourceAbsoluteFilePath != null) {
				File file = new File(sourceAbsoluteFilePath);
				if (! file.delete()) {
					System.out.println("Failed deleting source file.");
					job.SaveJobLogMessage("Failed deleting source file.");
					setJobStatusError(job.id, connection);
					closeDatabaseConnection(connection);
					return;
				}
			}


			setJobStatusComplete(job.id, connection);

		} catch (Exception e) {

			setJobStatusError(job.id, connection);

		}

		closeDatabaseConnection(connection);

	}
}

