package arlo;

import java.util.Vector;
import java.util.Iterator;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;


import arlo.NesterUser;
import arlo.NesterAudioFile;
import arlo.NesterJob;

import arlo.QueueTask;

public class QueueTask_LibraryMediaFileValidation extends QueueTask {

	static final private String taskName = "LibraryMediaFileValidation";

	public boolean runQueue(Vector<NesterUser> restrictedUsers) {
		return super._runQueue(taskName, restrictedUsers);
	}

	void runJob(NesterJob job) {
		Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		if (job == null) {
			logger.log(Level.SEVERE, "QueueTask_LibraryMediaFileValidation::runJob received null job");
			return;
		}

		logger.log(Level.INFO, "Starting QueueTask_LibraryMediaFileValidation::runJob jobId = " + job.id);

		Connection connection = QueueTask.openDatabaseConnection();

		if (RunValidation(job, connection)) {
			setJobStatusComplete(job.id, connection);
		} else {
			setJobStatusError(job.id, connection);  // error occured
		}

		closeDatabaseConnection(connection);
	}

	/** \brief Run a LibraryMediaFileValidation job.
	*
	* @param job the NesterJob object of the 'parent' job.
	* @param connection Opened database connection.
	* @return true if success, false if any error encountered. 
	*/

	static boolean RunValidation(NesterJob job, Connection connection) {
		Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	
		try {
			if (job.project_id == 0) // 0 returned if null in database
				return false;

			// Only allow jobs from admin users
			NesterUser nesterUser = NesterUser.getNesterUser(job.user_id, connection);
			if (! nesterUser.is_staff) {
				job.SaveJobLogMessage("Not an Admin User");
				return false;
			}

			// Should we skip md5 checks?
			Boolean skipMd5 = false;
			if (job.GetJobParameter("skipMd5") != null)
				skipMd5 = true;

			// Should we update null md5's?
			Boolean updateNullMd5 = false;
			if (job.GetJobParameter("updateNullChecksum") != null)
				updateNullMd5 = true;

			// Get the Library ID paramter
			Integer libraryId = null;
			{
				String value = job.GetJobParameter("libraryId");
				if (value != null)
					libraryId = Integer.parseInt(value);
			}

			// Should we update the stored MetaData?
			Boolean updateMetadata = false;
			if (job.GetJobParameter("updateMetadata") != null)
				updateMetadata = true;

			// Name Maps
			HashMap<Integer, String> userIdNameMap = new HashMap<Integer, String>();
			HashMap<Integer, String> libraryIdNameMap = new HashMap<Integer, String>();



			// Get the MediaFiles
			Vector<Integer> mediaFileIds = null;
			if (libraryId == null) {
				// validate ALL MediaFiles in ARLO
				mediaFileIds = NesterAudioFile.getAllMediaFileIds(connection);
			} else {
				// Get a list of MediaFiles in the Library
				mediaFileIds = NesterLibrary.getMediaFileIds(libraryId, connection);
			}
			if (mediaFileIds == null) {
				job.SaveJobLogMessage("Error retrieving Library MediaFiles list");
				return false;
			}


			///
			// Loop over the MediaFiles

			String results = "File ID,type,user_id,username,library_id,library,Alias,Path,md5 In DB,Pass?,Comments\n";
			int num_files = mediaFileIds.size();
			int files_processed = 0;

			for (Iterator<Integer> iterator = mediaFileIds.iterator(); iterator.hasNext();) {
				int fileId = iterator.next();

				logger.log(Level.INFO, "Audited " + files_processed++ + " of " + num_files + " Files - Auditing ID: " + fileId);

				boolean pass = true;
				boolean safeToUpdate = true;
				String comments = "";

				NesterAudioFile file = null;
				try {
					file = NesterAudioFile.getAudioFile(fileId, connection, null);
				} catch (Exception e) {
					results += fileId + ",Exception Retrieving NesterAudioFile\n";
					continue;
				}
				if (file == null) {
					results += fileId + ",Failed Retrieving NesterAudioFile\n";
					continue;
				}

				///
				// Map IDs to Names

				// Map User IDs to Names
				String username = userIdNameMap.get(file.user_id);
				if (username == null) {
					NesterUser user = NesterUser.getNesterUser(file.user_id, connection);
					if (user == null) {
						username = "";
					} else {
						username = user.name;
					}
					userIdNameMap.put(file.user_id, username);
				}

				// Map Library IDs to Names
				String libraryName = libraryIdNameMap.get(file.library_id);
				if (libraryName == null) {
					NesterLibrary lib = NesterLibrary.retrieveLibrary(file.library_id);
					if (lib == null) {
						libraryName = "";
					} else {
						libraryName = lib.name;
					}
					libraryIdNameMap.put(file.library_id, libraryName);
				}


				// Validations

				// Ensure the file exists
				if (pass) {
					File f = new File(file.GetAbsoluteFilePath());
					if(! (f.exists() && !f.isDirectory())) {
						comments += "File Does Not Exist on System";
						pass = false;
						safeToUpdate = false;
					}
				}

				// Validate the MD5 sum
				if (! skipMd5) {
					if (pass) {
						String md5_stored = file.md5;
						if (md5_stored == null) {
							if (updateNullMd5) {
								comments += "Updating Empty Checksum";
								file.md5 = NesterAudioFile.ComputeFileMD5Digest(file.GetAbsoluteFilePath());;
								file.saveMD5(connection);
							} else {
								comments += "No MD5 Stored";
							}
							
						} else {
							String md5_computed = NesterAudioFile.ComputeFileMD5Digest(file.GetAbsoluteFilePath());
							if (! md5_stored.equals(md5_computed)) {
								comments += "MD5s don't match - Stored: " + md5_stored + " Computed: " + md5_computed;
								pass = false;
								safeToUpdate = false;
							}
						}
					}
				}

				///
				// Check the file MetaData stored in the DB

				if (pass) {
					if (file.mediaFileType == NesterAudioFile.MediaFileType.AUDIO) {
						AudioFile compareAudioFile = new AudioFile(file.GetAbsoluteFilePath());

						if (!compareAudioFile.validDATAsize) {
							comments += "Failed Opening File: validDATAsize";
							pass = false;
							safeToUpdate = false;
						}
						if (!compareAudioFile.validRIFFsize) {
							comments += "Failed Opening File: validRIFFsize";
							pass = false;
							safeToUpdate = false;
						}
						if (compareAudioFile.durationInSeconds == 0.0) {
							comments += "Failed Opening File: durationInSeconds";
							pass = false;
							safeToUpdate = false;
						}

						if (file.wavFileMetaData.fileSize != compareAudioFile.fileSize) {
							comments += "Mismatched fileSize(" + file.wavFileMetaData.fileSize + " vs " + compareAudioFile.fileSize + ") ";
							pass = false;
						}
						if (file.wavFileMetaData.dataSize != compareAudioFile.dataSize) {
							comments += "Mismatched dataSize(" + file.wavFileMetaData.dataSize + " vs " + compareAudioFile.dataSize + ") ";
							pass = false;
						}
						if (file.wavFileMetaData.validRIFFsize != (Boolean) compareAudioFile.validRIFFsize) {
							comments += "Mismatched validRIFFsize(" + file.wavFileMetaData.validRIFFsize + " vs " + compareAudioFile.validRIFFsize + ") ";
							pass = false;
						}
						if (file.wavFileMetaData.validDATAsize != (Boolean) compareAudioFile.validDATAsize) {
							comments += "Mismatched validDATAsize(" + file.wavFileMetaData.validDATAsize + " vs " + compareAudioFile.validDATAsize + ") ";
							pass = false;
						}
						if (file.wavFileMetaData.dataStartIndex != compareAudioFile.dataStartIndex) {
							comments += "Mismatched dataStartIndex(" + file.wavFileMetaData.dataStartIndex + " vs " + compareAudioFile.dataStartIndex + ") ";
							pass = false;
						}
						if (file.wavFileMetaData.wFormatTag != compareAudioFile.wFormatTag) {
							comments += "Mismatched wFormatTag(" + file.wavFileMetaData.wFormatTag + " vs " + compareAudioFile.wFormatTag + ") ";
							pass = false;
						}
						if (file.wavFileMetaData.numChannels != compareAudioFile.numChannels) {
							comments += "Mismatched numChannels(" + file.wavFileMetaData.numChannels + " vs " + compareAudioFile.numChannels + ") ";
							pass = false;
						}
						if (file.wavFileMetaData.sampleRate != compareAudioFile.sampleRate) {
							comments += "Mismatched sampleRate(" + file.wavFileMetaData.sampleRate + " vs " + compareAudioFile.sampleRate + ") ";
							pass = false;
						}
						if (file.wavFileMetaData.nAvgBytesPerSec != compareAudioFile.nAvgBytesPerSec) {
							comments += "Mismatched nAvgBytesPerSec(" + file.wavFileMetaData.nAvgBytesPerSec + " vs " + compareAudioFile.nAvgBytesPerSec + ") ";
							pass = false;
						}
						if (file.wavFileMetaData.nBlockAlign != compareAudioFile.nBlockAlign) {
							comments += "Mismatched nBlockAlign(" + file.wavFileMetaData.nBlockAlign + " vs " + compareAudioFile.nBlockAlign + ") ";
							pass = false;
						}
						if (file.wavFileMetaData.bitsPerSample != compareAudioFile.bitsPerSample) {
							comments += "Mismatched bitsPerSample(" + file.wavFileMetaData.bitsPerSample + " vs " + compareAudioFile.bitsPerSample + ") ";
							pass = false;
						}
						if (file.wavFileMetaData.numBytesToTailPad != compareAudioFile.numBytesToTailPad) {
							comments += "Mismatched numBytesToTailPad(" + file.wavFileMetaData.numBytesToTailPad + " vs " + compareAudioFile.numBytesToTailPad + ") ";
							pass = false;
						}
						if (file.wavFileMetaData.numFrames != compareAudioFile.numFrames) {
							comments += "Mismatched numFrames(" + file.wavFileMetaData.numFrames + " vs " + compareAudioFile.numFrames + ") ";
							pass = false;
						}
						if (file.wavFileMetaData.durationInSeconds != compareAudioFile.durationInSeconds) {
							comments += "Mismatched durationInSeconds(" + file.wavFileMetaData.durationInSeconds + " vs " + compareAudioFile.durationInSeconds + ") ";
							pass = false;
						}


						///
						// Update stored MetaData ?

						if (!pass && updateMetadata && safeToUpdate) {
							if ((file.wavFileMetaData.fileSize == 0) && (compareAudioFile.fileSize > 0)) {
								comments += "Updating FileSize: " +
									NesterAudioFile.SaveMetaData(fileId, "fileSize", String.valueOf(compareAudioFile.fileSize), false, connection);
							}
							if ((file.wavFileMetaData.dataSize == 0) && (compareAudioFile.dataSize > 0)) {
								comments += "Updating dataSize: " +
									NesterAudioFile.SaveMetaData(fileId, "dataSize", String.valueOf(compareAudioFile.dataSize), false, connection);
							}
							if ((file.wavFileMetaData.validRIFFsize == false) && (compareAudioFile.validRIFFsize == true)) {
								comments += "Updating validRIFFsize: " +
									NesterAudioFile.SaveMetaData(fileId, "validRIFFsize", String.valueOf(compareAudioFile.validRIFFsize), false, connection);
							}
							if ((file.wavFileMetaData.validDATAsize == false) && (compareAudioFile.validDATAsize == true)) {
								comments += "Updating validDATAsize: " +
									NesterAudioFile.SaveMetaData(fileId, "validDATAsize", String.valueOf(compareAudioFile.validDATAsize), false, connection);
							}
							if ((file.wavFileMetaData.dataStartIndex == 0) && (compareAudioFile.dataStartIndex > 0)) {
								comments += "Updating dataStartIndex: " +
									NesterAudioFile.SaveMetaData(fileId, "dataStartIndex", String.valueOf(compareAudioFile.dataStartIndex), false, connection);
							}
							if ((file.wavFileMetaData.numChannels == 0) && (compareAudioFile.numChannels > 0)) {
								comments += "Updating numChannels: " +
									NesterAudioFile.SaveMetaData(fileId, "numChannels", String.valueOf(compareAudioFile.numChannels), false, connection);
							}
							if ((file.wavFileMetaData.sampleRate == 0) && (compareAudioFile.sampleRate > 0)) {
								comments += "Updating sampleRate: " +
									NesterAudioFile.SaveMetaData(fileId, "sampleRate", String.valueOf(compareAudioFile.sampleRate), false, connection);
							}
							if ((file.wavFileMetaData.nAvgBytesPerSec == 0) && (compareAudioFile.nAvgBytesPerSec > 0)) {
								comments += "Updating nAvgBytesPerSec: " +
									NesterAudioFile.SaveMetaData(fileId, "nAvgBytesPerSec", String.valueOf(compareAudioFile.nAvgBytesPerSec), false, connection);
							}
							if ((file.wavFileMetaData.bitsPerSample == 0) && (compareAudioFile.bitsPerSample > 0)) {
								comments += "Updating bitsPerSample: " +
									NesterAudioFile.SaveMetaData(fileId, "bitsPerSample", String.valueOf(compareAudioFile.bitsPerSample), false, connection);
							}
							if ((file.wavFileMetaData.numBytesToTailPad == 0) && (compareAudioFile.numBytesToTailPad > 0)) {
								comments += "Updating numBytesToTailPad: " +
									NesterAudioFile.SaveMetaData(fileId, "numBytesToTailPad", String.valueOf(compareAudioFile.numBytesToTailPad), false, connection);
							}
							if ((file.wavFileMetaData.numFrames == 0) && (compareAudioFile.numFrames > 0)) {
								comments += "Updating numFrames: " +
									NesterAudioFile.SaveMetaData(fileId, "numFrames", String.valueOf(compareAudioFile.numFrames), false, connection);
							}
							if ((file.wavFileMetaData.durationInSeconds == 0) && (compareAudioFile.durationInSeconds > 0)) {
								comments += "Updating durationInSeconds: " +
									NesterAudioFile.SaveMetaData(fileId, "durationInSeconds", String.valueOf(compareAudioFile.durationInSeconds), false, connection);
							}
						}

					}
				}


				///
				// Store results
				results += file.id + "," +
					file.mediaFileType.name() + "," +
					file.user_id + "," +
					username + "," +
					file.library_id + "," +
					libraryName + ",\"" +
					file.alias + "\",\"" +
					file.mediaRelativeFilePath + "\"," +
					file.md5 + "," +
					pass + "," +
					comments + "\n";
			}


			///
			// Upload Results File

			if (! job.UploadJobResultFile("audit_result.csv", results.getBytes(), "Audit Results", connection)) {
				job.SaveJobLogMessage("Error Uploading Results File");
				return false;
			}


			return true;

		} catch (Exception e) {
			logger.log(Level.SEVERE, "Exception = " + e);
			StringWriter excWriter = new StringWriter();
			PrintWriter printWriter = new PrintWriter( excWriter );
			e.printStackTrace( printWriter );
			printWriter.flush();
			String stackTrace = excWriter.toString();
			logger.log(Level.SEVERE, stackTrace);
			return false;
		}

	}

}

