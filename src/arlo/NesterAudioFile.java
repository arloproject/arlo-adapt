package arlo;

import java.io.Serializable;
import java.sql.Timestamp;
import java.io.File;
import java.io.InputStream;
import java.io.FileInputStream;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.util.HashMap;
import java.util.Vector;
import java.util.Iterator;

import java.security.MessageDigest;

import adapt.IO;

/* TODO 
 *
 * Eventually Generalize to NesterMediaFile and make this an inherited class
 */

public class NesterAudioFile implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5787612004200650730L;

	// Defined in Database in tools_mediatypes
	public enum MediaFileType {
		AUDIO(1),
		IMAGE (2),
		MOVEMENT (3),
		TEXT (4),
		VIDEO (5);

		private MediaFileType(final Integer id) {
			this.id = id;
		}

		private Integer id;
		private static final HashMap<Integer, MediaFileType> idMap = new HashMap<Integer, MediaFileType>();  // Lookup table

		public Integer getId() {
			return this.id;
		}

 
		// Build the lookup table
		static {
			for (final MediaFileType x : MediaFileType.values())
				idMap.put(x.getId(), x);
		}
 
		// Reverse Lookpu
		public static MediaFileType getFromId(Integer id) {
			return idMap.get(id);
		}
	}

	public int id; /*!< \brief database id; if -1, indicates a new object, not in db */
	public int user_id;
	public boolean active;
	public String alias;
	public Timestamp uploadDate;
	public String mediaRelativeFilePath;  // Relative to MEDIA_ROOT, this maps to the 'file' field in the database
	public int library_id;
	public Timestamp realStartTime;
	public NesterWavFileMetaData wavFileMetaData;
	public NesterStaticUserMetaData staticUserMetaData;
	public String md5;
	public MediaFileType mediaFileType = null;

	// public int dynamicUserMetaData_id;
	// public NesterDynamicUserMetaData dynamicUserMetaData;

	@Override
	public String toString() {

		String s = "";

		s += "<";
		s += "id:" + id;
		s += ",user_id:" + user_id;
		s += ",alias:" + alias;
		s += ",active:" + active;
		s += ",uploadDate:" + uploadDate;
		s += ",mediaRelativeFilePath:" + mediaRelativeFilePath;
		s += ",library_id:" + library_id;
		s += ",realStartTime:" + realStartTime;
		s += ">";

		// TODO Auto-generated method stub
		return s;
	}


	/** \brief Default Constructor
	*
	*/

	public NesterAudioFile() {
		
	}

	/** \brief Construct a New NesterAudioFile from an AudioFile. 
	* 
	* Initializes a new NesterAudioFile with default settings/MetaData. 
	* Does not save the data to the Database itself, this just initializes it into 
	* memory. 
	* @param audioFile An initialize AudioFile with WAV data
	* @param username The username who owns this file, used for generating the paths
	* @param userID The database ID of the user who owns this file
	* @param libraryID The database ID of the Library in which this will be stored.
	* @return initialized NesterAudioFile
	*/

	public NesterAudioFile(AudioFile audioFile, String username, int userID, int libraryID) {
		
		// username and userID should be the same user, and in fact for consistency
		// we could check the database. However, that is an expensive operation, so we'll 
		// trust the caller to keep these valid

		// default settings
		id = -1;   // new, not in Db
		user_id = userID;
		active = true;  // active by default
		alias = new File(audioFile.filePath).getName();  // by default, just the filename
		uploadDate = new java.sql.Timestamp(System.currentTimeMillis());  // current Time
		library_id = libraryID;
		realStartTime = null;
		
		// relative path, bit complicated - have to take out the relative directory
		mediaRelativeFilePath = audioFile.filePath.substring(ArloSettings.mediaRootDirectoryPath.length()+1);  // remove the media_root + '/'
		md5 = null;  // this is expensive - only compute during import / audit operations

		// WavFileMetaData
		wavFileMetaData = new NesterWavFileMetaData(audioFile);  // constuct from audioFile settings

		// StaticUserMetaData
		staticUserMetaData = new NesterStaticUserMetaData();  // default blank settings
	}


	/** \brief Get a NeterAudioFile from the database, along with the associated MetaData
	* 
	* @param audioFileID The NesterAudioFile to retrieve.
	* @param connection The initialized connection to the database.
	* @param fieldNames A list of various fields (MetaData) to retrieve.
	* @return initialized NesterAudioFile.
	*/

	static public NesterAudioFile getAudioFile(int audioFileID, Connection connection, HashMap<String, Boolean> fieldNames) {

		NesterAudioFile audioFile = null;

		Logger logger;
		logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		try {

			// /////////////////////
			// Get Media File Data
			Statement s1 = connection.createStatement();
			s1.executeQuery("select * from tools_mediafile where id = " + audioFileID);
			ResultSet rs1 = s1.getResultSet();

			if (rs1.next()) {

				audioFile = new NesterAudioFile();

				audioFile.id = rs1.getInt("id");
				audioFile.user_id = rs1.getInt("user_id");
				audioFile.active = rs1.getBoolean("active");
				audioFile.alias = rs1.getString("alias");
				audioFile.uploadDate = rs1.getTimestamp("uploadDate");
				audioFile.mediaRelativeFilePath = rs1.getString("file");
				audioFile.md5 = rs1.getString("md5");
				audioFile.mediaFileType = MediaFileType.getFromId(rs1.getInt("type_id"));

				String username = NesterUser.getNesterUser(audioFile.user_id, connection).name;
				audioFile.library_id = rs1.getInt("library_id");
				audioFile.realStartTime = rs1.getTimestamp("realStartTime");
			} else {
				return null;
			}

			rs1.close();
			s1.close();

			// //////////////////////
			// Get wavFile MetaData

			audioFile.wavFileMetaData = new NesterWavFileMetaData();
			if (!audioFile.wavFileMetaData.getNesterWavFileMetaData(audioFile.id, connection, fieldNames)) {
				logger.log(Level.SEVERE, "GetNesterWavFileMetaData(audioFile() Failed");
				return null;
			}

			// /////////////////////////
			// Get StaticUser metaData

			audioFile.staticUserMetaData = new NesterStaticUserMetaData();
			if (!audioFile.staticUserMetaData.getNesterStaticUserMetaData(audioFile.id, connection, fieldNames)) {
				logger.log(Level.SEVERE, "Get getNesterStaticUserMetaData Failed");
				return null;
			}


			return audioFile;

		} catch (Exception e) {
			logger.log(Level.SEVERE, "Exception = " + e);
			return null;
		}

	}

	/** \brief Get a list of all MediaFile objects in a Project.
	 *
	 * This is an optimized version of the original NesterProject.getNesterProjectAudioFiles. 
	 * This version reduces the number of database calls by requesting all files from the database at once. 
	 * @param projectId The database ID of the Project.
	 * @param activeOnly Only return files set as 'active'
	 * @param fieldNames
	 * @param connection Opened Database Connection
	 * @return List of NesterAudioFile objects
	 */

	public static Vector<NesterAudioFile> getNesterProjectAudioFiles(int projectID, boolean activeOnly, HashMap<String, Boolean> fieldNames, Connection connection) {
		Logger logger;
		logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

// TODO 
// eventually refactor all of NesterMediaFile to use metadata in such a way
// also TODO
// support multiple entires with HashMap<String, Vector<String>>


		/// 
		// Get all mediaFileMetadata

		//    fileId       meta Name       meta Value
		HashMap<Integer, HashMap<String, Vector<String>>> fileMetadataMaps = new HashMap<Integer, HashMap<String, Vector<String>>>();

		// build the metadata into maps per file
		try {
			Statement s1 = connection.createStatement();
			s1.executeQuery(
				"SELECT * FROM tools_mediafilemetadata WHERE mediaFile_id IN " + 
				"(SELECT mediaFile_id FROM tools_project_mediaFiles where project_id = " + projectID + ")");
			ResultSet rs1 = s1.getResultSet();

			while (rs1.next()) {
				Integer mediaFileId = rs1.getInt("mediaFile_id");
				String name  = rs1.getString("name");
				String value = rs1.getString("value");

				HashMap<String, Vector<String>> metadataMap = fileMetadataMaps.get(mediaFileId);
				if (metadataMap == null) {
					metadataMap = new HashMap<String, Vector<String>>();
					fileMetadataMaps.put(mediaFileId, metadataMap);
				}

				Vector<String> metadataValuesVector = metadataMap.get(name);
				if (metadataValuesVector == null) {
					metadataValuesVector = new Vector<String>();
					metadataMap.put(name, metadataValuesVector);
				}

				metadataValuesVector.add(value);
			}
		
			rs1.close();
			s1.close();

		} catch (Exception e) {
			logger.log(Level.INFO, "Error Retrieving Metadata");
			logger.log(Level.INFO, "Exception = " + e);
			return null;
		}



		///
		// Get all mediaFiles
		Vector<NesterAudioFile> audioFiles = new Vector<NesterAudioFile>();

		try {
			Statement s1 = connection.createStatement();
			s1.executeQuery(
				"SELECT tools_mediafile.*, auth_user.username " + 
				"FROM tools_mediafile, auth_user " + 
				" WHERE tools_mediafile.user_id = auth_user.id " + 
				" AND tools_mediafile.id IN " +
					"(SELECT mediaFile_id FROM tools_project_mediaFiles WHERE project_id = " + projectID + ")");
			ResultSet rs1 = s1.getResultSet();

			while (rs1.next()) {

				// NesterAudioFile audioFile = NesterAudioFile.getAudioFile(audioFileID, connection, fieldNames);
				// don't retrieve the file separately, rather build it in-place here

				NesterAudioFile audioFile = new NesterAudioFile();

				audioFile.id = rs1.getInt("id");
				audioFile.user_id = rs1.getInt("user_id");
				audioFile.active = rs1.getBoolean("active");
				audioFile.alias = rs1.getString("alias");
				audioFile.uploadDate = rs1.getTimestamp("uploadDate");
				audioFile.mediaRelativeFilePath = rs1.getString("file");
				audioFile.library_id = rs1.getInt("library_id");
				audioFile.realStartTime = rs1.getTimestamp("realStartTime");
				audioFile.md5 = rs1.getString("md5");

				String username = rs1.getString("username");

				// Metadata
				HashMap<String, Vector<String>> metadataSourceData = fileMetadataMaps.get(audioFile.id);
				if (metadataSourceData == null) {
					logger.log(Level.SEVERE, "Missing Metadata");
					return null;
				}

				// Get wavFile MetaData

				audioFile.wavFileMetaData = new NesterWavFileMetaData();
				if (!audioFile.wavFileMetaData.getNesterWavFileMetaData(metadataSourceData, fieldNames)) {
					logger.log(Level.SEVERE, "Get getNesterStaticUserMetaData Failed");
					return null;
				}

				// Get StaticUser metaData

				audioFile.staticUserMetaData = new NesterStaticUserMetaData();
				if (!audioFile.staticUserMetaData.getNesterStaticUserMetaData(metadataSourceData, fieldNames)) {
					logger.log(Level.SEVERE, "Get getNesterStaticUserMetaData Failed");
					return null;
				}


				if ((activeOnly && audioFile.active) || (!activeOnly))
					audioFiles.add(audioFile);
			}

			rs1.close();
			s1.close();

			return audioFiles;

		} catch (Exception e) {
			logger.log(Level.INFO, "name not found");
			logger.log(Level.INFO, "Exception = " + e);
		}

		return audioFiles;
	}





	// //////
	// Lookup MetaData entry in database. If not found, return null

	public String GetMetaData(String name, Connection connection) {
		return GetMetaData(this.id, name, connection);
	}

	public String GetMetaData(String name, Connection connection, boolean AllowNullValue) {
		return GetMetaData(this.id, name, connection, AllowNullValue);
	}

	static public String GetMetaData(int mediaFileID, String name, Connection connection) {
		return GetMetaData(mediaFileID, name, connection, false);
	}

	static public String GetMetaData(int mediaFileID, String name, Connection connection, boolean AllowNullValue) {
		// AllowNullValue - If true, forces NULL from the database into an empty string.
		// return NULL on error

		String value = null;

		Logger logger;
		logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		try {

			Statement s1 = connection.createStatement();
			s1.executeQuery("select * from tools_mediafilemetadata where mediafile_id = " + mediaFileID + " AND name = '" + name + "'");
			ResultSet rs1 = s1.getResultSet();

			if (rs1.next()) {
				value = rs1.getString("value");
			}

			if ((value == null) && AllowNullValue) {
				value = "";
			}

			rs1.last();
			int rows = rs1.getRow();
			if (rows != 1) {
				// logger.log(Level.INFO, "Warning: " + rows + " rows found for MediaFileMetaData '" + name + "', mediaFileID=" + mediaFileID);
				value = null;
			}
			rs1.close();
			s1.close();

		} catch (Exception e) {
			logger.log(Level.SEVERE, "GetMetaData() - name not found");
			logger.log(Level.SEVERE, "Exception = " + e);
			value = null;
		}

		if (value != null) {
			// logger.log(Level.INFO, "Found MetaData  " + name + ":" + value);
		}
		return value;
	}

	// ////////////////
	// Save MetaData

	 static public Boolean SaveMetaData(int mediaFileID, String name, String value, Boolean userEditable, Connection connection) {
	
		 Logger logger;
		 logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	
		 System.out.println("Save Meta Data name='" + name + "' value='" + value + "' userEditable=" + userEditable);
	
		 try {
			 // First, check to see if the entry already exists
	
			 Statement s1 = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			 s1.executeQuery("select * from tools_mediafilemetadata where mediafile_id = " + mediaFileID + " AND name = '" + name + "'");
			 ResultSet rs1 = s1.getResultSet();
			 rs1.last();
			 int rows = rs1.getRow();
	
			 if (rows > 1) {
				 logger.log(Level.SEVERE, rows + " rows found for MediaFileMetaData '" + name + "', mediaFileID=" + mediaFileID + "... Bailing out");
				 return false;
			 }
			 if (rows == 1) {
				 // Data already exists, update.
				 logger.log(Level.INFO, rows + " rows found for MediaFileMetaData '" + name + "', mediaFileID=" + mediaFileID + "... Updating");
				 rs1.updateInt("mediafile_id", mediaFileID);
				 rs1.updateString("name", name);
				 rs1.updateString("value", value);
				 rs1.updateBoolean("userEditable", userEditable);
				
				 rs1.updateRow();
				 rs1.close();
				 s1.close();
				 return true;
			 }
	
			 if (rs1 != null)
				 rs1.close();
			 if (s1 != null)
				 s1.close();
	
			 // if here, add new row
	
			 s1 = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			 rs1 = s1.executeQuery("SELECT * FROM tools_mediafilemetadata LIMIT 0");
			 rs1.moveToInsertRow();
	
			 rs1.updateInt("mediafile_id", mediaFileID);
			 rs1.updateString("name", name);
			 rs1.updateString("value", value);
			 rs1.updateBoolean("userEditable", userEditable);
	
			 rs1.insertRow();
	
			 if (rs1 != null)
				 rs1.close();
			 if (s1 != null)
				 s1.close();
	
			 return true;
	
		 } catch (Exception e) {
			 logger.log(Level.WARNING, "Error in SaveMetaData - Exception = " + e);
			 return false;
		 }
	
	}


	// /////
	// Find the AudioFile ID from the provided path

	static public int getAudioFileID(int userID, String mediaRelativeFilePath, Connection connection) {
		Logger logger;
		logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		try {
			// logger.log(Level.INFO, "1");
			Statement s = connection.createStatement();
			s.executeQuery("SELECT * FROM tools_mediafile where user_id = " + userID + " && " + " file = \"" + mediaRelativeFilePath + "\"");
			ResultSet rs = s.getResultSet();
			// logger.log(Level.INFO, "2");
			if (rs.next()) {

				int id = rs.getInt("id");

				return id;
			}
			// logger.log(Level.INFO, "3");
			rs.close();
			s.close();
			logger.log(Level.SEVERE, "name not found");
			// logger.log(Level.INFO, "4");
			return -1;

		} catch (Exception e) {
			logger.log(Level.SEVERE, "Exception = " + e);
		}

		return -1;
	}


////////////////////////////////////////////////////////////////////////////////////////

// NEW

////////////////////////////////////////////////////////////////////////////////////////



	
	// ////////////////////
	// Import Audio Files


	/**
	* Adds the files to the indicated Project in the DB.
	* @param nesterAudioFiles The list of NesterAudioFile objects from which to save the metadata
	* @param projectId The DB id of the Project for which to add these files
	* @param connection The database connection, already opened and active
	* @return Boolean - true if success, false on error
	*/

	static public Boolean AddFilesToProject(Vector<NesterAudioFile> nesterAudioFiles, int projectId, Connection connection) {
	
		try {
			String sInsertStatement = "INSERT INTO tools_project_mediaFiles " + 
				"(project_id, mediafile_id) " +
				"VALUES (?, ?)";
			connection.setAutoCommit(false);
			PreparedStatement stmtInsertProject = connection.prepareStatement(sInsertStatement);

			stmtInsertProject.setInt(1, projectId);

			for (Iterator<NesterAudioFile> iterator = nesterAudioFiles.iterator(); iterator.hasNext();) {
				NesterAudioFile nesterAudioFile = (NesterAudioFile) iterator.next();
				
				if (nesterAudioFile.id != -1) {
					stmtInsertProject.setInt(2, nesterAudioFile.id);
					stmtInsertProject.addBatch();
				}
				
			} // for (Iterator<NesterAudioFile> iterator ...
		
System.out.println("Save Project Batch...");
			stmtInsertProject.executeBatch();
			stmtInsertProject.close();
System.out.println("Commit... ");
			connection.commit();
			connection.setAutoCommit(true);
			
		} catch (Exception e) {
			System.out.println("Exception = " + e);
 			return false;
		}
		
		return true;
	}



	/**
	* Deletes any existing MetaData from the DB from the associated files. Existing data is removed
	* so that we can run straight inserts rather that insert / updates when importing new files.
	* \note Expects that the id of the NesterAudioFile is set to the correct id. 
	* \warning Deletes all existing MetaData for the associated files.
	* @param nesterAudioFiles The list of NesterAudioFile objects from which to save the metadata
	* @param connection The database connection, already opened and active
	* @return Boolean - true if success, false on error
	*/

	static public Boolean DeleteMetaDataFromDB(Vector<NesterAudioFile> nesterAudioFiles, Connection connection) {
		
		try {
			String sInsertStatement = "DELETE FROM tools_mediafilemetadata " + 
				"WHERE mediaFile_id = ?";
			connection.setAutoCommit(false);
			PreparedStatement stmt = connection.prepareStatement(sInsertStatement);

			for (Iterator<NesterAudioFile> iterator = nesterAudioFiles.iterator(); iterator.hasNext();) {
				NesterAudioFile nesterAudioFile = (NesterAudioFile) iterator.next();

				if (nesterAudioFile.id != -1) {
					stmt.setInt(1, nesterAudioFile.id);
					stmt.addBatch();
				}
			} // for (Iterator<NesterAudioFile> iterator ...
		
System.out.println("Delete Batch...");
			stmt.executeBatch();
			stmt.close();
System.out.println("Commit... ");
			connection.commit();
			connection.setAutoCommit(true);
			
		} catch (Exception e) {
			System.out.println("Exception = " + e);
 			return false;
		}
		
		return true;

	}

	/**
	* Saves all MetaData from the files in the list. Batches up all MetaData from the provided 
	* nesterAudioFiles, and saves them to the Db at once. 
	* \warning This deletes any existing MetaData for these files. 
	* @param nesterAudioFiles The list of NesterAudioFile objects from which to save the metadata
	* @param connection The database connection, already opened and active
	* @return Boolean - true if success, false on error
	*/

	static public Boolean SaveMetaDataToDb(Vector<NesterAudioFile> nesterAudioFiles, Connection connection) {

		// this funtion will likely only be called for new files. However, in case it is being 
		// used to modify existing files (future use) delete all existing MetaData
		DeleteMetaDataFromDB(nesterAudioFiles, connection);
	
		try {
			String sInsertStatement = "INSERT INTO tools_mediafilemetadata " + 
				"(mediaFile_id, name, value, userEditable) " +
				"VALUES (?, ?, ?, ?)";
			connection.setAutoCommit(false);
			PreparedStatement stmtInsertMetadata = connection.prepareStatement(sInsertStatement);

			for (Iterator<NesterAudioFile> iterator = nesterAudioFiles.iterator(); iterator.hasNext();) {
				NesterAudioFile nesterAudioFile = (NesterAudioFile) iterator.next();
				
				nesterAudioFile.wavFileMetaData.BatchSaveMetaData(nesterAudioFile.id, stmtInsertMetadata);
				nesterAudioFile.staticUserMetaData.BatchSaveMetaData(nesterAudioFile.id, stmtInsertMetadata);
			} // for (Iterator<NesterAudioFile> iterator ...
		
System.out.println("Save Batch...");
			stmtInsertMetadata.executeBatch();
			stmtInsertMetadata.close();
System.out.println("Commit... ");
			connection.commit();
			connection.setAutoCommit(true);
			
		} catch (Exception e) {
			System.out.println("Exception = " + e);
 			return false;
		}
		
		return true;
	}

	/**
	* Saves a list of audioFiles. Save a batch of NesterAudioFiles and associated MetaData 
	* at once to the database to optimize time accessing the DB. Uses default/empty values 
	* for unknown MetaData/values. 
	* \warning Deletes any existing 'import' Project for \a userID. 
	* @param nesterAudioFiles The list of NesterAudioFile objects to save
	* @param projectId The DB id of the project for which to add these files
	* @param connection The database connection, already opened and active
	* @return Boolean - true if success, false on error
	*/

	static public Boolean saveNewAudioFilesToDb(Vector<NesterAudioFile> nesterAudioFiles, int projectId, Connection connection) {
		
		// not sure adding the files to the Project should happen here. Would we add files that
		// we don't want/need in a Project? Easy to change in the future...

		Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		try {
			connection.setAutoCommit(false);
			Statement stmt1 = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

			// some good Java / MySql notes: http://dev.mysql.com/doc/refman/4.1/en/connector-j-usagenotes-basic.html#connector-j-examples-execute-select

			// /////////////////////
			// setup prepared statement
			// type_id 1 = Audio File
			String sInsertStatement = "INSERT INTO tools_mediafile " + 
				"(user_id, type_id, active, alias, uploadDate, file, library_id, realStartTime, md5) " + 
				"VALUES (?, 1, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			PreparedStatement stmtInsertMediaFile = connection.prepareStatement(sInsertStatement, Statement.RETURN_GENERATED_KEYS);
			

			for (Iterator<NesterAudioFile> iterator = nesterAudioFiles.iterator(); iterator.hasNext();) {
				NesterAudioFile nesterAudioFile = (NesterAudioFile) iterator.next();
				
				// //////////////////////
				// for the NesterAudioFiles we have to insert them individually to get the ID back

				stmtInsertMediaFile.setInt(      1, nesterAudioFile.user_id);
				stmtInsertMediaFile.setBoolean(  2, nesterAudioFile.active);
				stmtInsertMediaFile.setString(   3, nesterAudioFile.alias);
				stmtInsertMediaFile.setTimestamp(4, nesterAudioFile.uploadDate);
				stmtInsertMediaFile.setString(   5, nesterAudioFile.mediaRelativeFilePath);
				stmtInsertMediaFile.setInt(      6, nesterAudioFile.library_id);
				stmtInsertMediaFile.setTimestamp(7, nesterAudioFile.realStartTime);
				stmtInsertMediaFile.setString(   8, nesterAudioFile.md5);

				// execute statement, and get ID
				if (stmtInsertMediaFile.executeUpdate() != 1) {
					logger.log(Level.SEVERE, "Modified Rows != 1");
					return false;
				}
				ResultSet rs = stmtInsertMediaFile.getGeneratedKeys();
				if (rs.next()) {
					nesterAudioFile.id = rs.getInt(1);
				} else {
					logger.log(Level.SEVERE, "No ResultSet returned for nesterAudioFile.id");
				}

				rs.close();

			} // for (iterator = audioFiles.iterator)
		
			stmtInsertMediaFile.close();
			connection.commit();
			connection.setAutoCommit(true);

		} catch (Exception e) {
			System.out.println("Exception = " + e);
			return false;
		}

		// ///////////////////////////////////////////
		// for the MetaData, we can insert as a batch

		logger.log(Level.INFO, "Batch Saving MetaData");
		if (! SaveMetaDataToDb(nesterAudioFiles, connection)) {
			logger.log(Level.SEVERE, "Failed SaveMetaDataToDb()");
			return false;
		}
		
		// ////////////////////////
		// add files to Project
		if (! AddFilesToProject(nesterAudioFiles, projectId, connection)) {
			logger.log(Level.SEVERE, "Failed AddFilesToProject()");
			return false;
		}

		return true;

	} // saveAudioFilesToDb()


	/** Import an individual audio file. 
	 *  Other versions delete and recreate the 'import' Project, whereas this one includes 
	 *  the Project as an argument.
	 *  @note This version is a wrapper that excludes the 'alias' parameter (for backward compatibility)
	 *  @param username 
	 *  @param userID
	 *  @param projectId
	 *  @param libraryId
	 *  @param mediaRelativeFilePath
	 *  @param connection
	 *  @return True on success, False on error
	 */

	static public Boolean importAudioFile(String username, int userID, int projectID, int libraryID, String mediaRelativeFilePath, Connection connection) {
		return importAudioFile(username, userID, projectID, libraryID, null, mediaRelativeFilePath, connection);
	}


	/** Import an individual audio file. 
	 *  Other versions delete and recreate the 'import' Project, whereas this one includes 
	 *  the Project as an argument.
	 *  @param username 
	 *  @param userID
	 *  @param projectID
	 *  @param libraryID Database ID of the Library in which to store this file. 
	 *  @param alias If not null, stores this value as the 'alias' for the mediaFile, else uses the file name.
	 *  @param mediaRelativeFilePath Used for the 'file' field, path relative to the Django MEDIA_ROOT (e.g., 'files/user-files/mediaFiles/library-1/test.wav')
	 *  @param connection
	 *  @return True on success, False on error
	 */

	static public Boolean importAudioFile(String username, int userID, int projectID, int libraryID, String alias, String mediaRelativeFilePath, Connection connection) {
		Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

 		String absoluteFilePath = ArloSettings.mediaRootDirectoryPath + "/" + mediaRelativeFilePath;
		try {

			if (! mediaRelativeFilePath.toLowerCase().endsWith(".wav")) { 
				logger.log(Level.SEVERE, "Not a wav file - failing");
				return false;
			}
			
			// ///////////////
			// Open AudioFile

			AudioFile audioFile = new AudioFile(absoluteFilePath);

			if (!audioFile.validDATAsize) {
				logger.log(Level.SEVERE, "!audioFile.validDATAsize");
				return false;
			}
			if (!audioFile.validRIFFsize) {
				logger.log(Level.SEVERE, "!validRIFFsize");
				return false;
			}
			if (audioFile.durationInSeconds == 0.0) {
				logger.log(Level.SEVERE, "durationInSeconds == 0.0");
				return false;
			}

			///
			// Compute File MD5

			String md5digest = ComputeFileMD5Digest(absoluteFilePath);
			logger.log(Level.INFO, "Importing File Hash: " + md5digest);

			// ///////////
			// Build NesterAudioFile from audioFile

// NesterAudioFile nesterAudioFile = new NesterAudioFile(audioFile, username, userID);


			Statement stmt1 = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			ResultSet uprs1 = null;


			// /////////////////////////
			// Add to database //
			// /////////////////////////

			// ////////////////
			// Add MediaFile

			stmt1 = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			uprs1 = stmt1.executeQuery("SELECT * FROM tools_mediafile LIMIT 0");
			uprs1.moveToInsertRow();

			uprs1.updateInt("user_id", userID);
			uprs1.updateInt("type_id", MediaFileType.AUDIO.getId());
			uprs1.updateBoolean("active", true);
			if (alias == null || alias.length() == 0) {
				uprs1.updateString("alias", new File(audioFile.filePath).getName());
			} else {
				uprs1.updateString("alias", alias);
			}
			uprs1.updateTimestamp("uploadDate", new java.sql.Timestamp(System.currentTimeMillis()));

			uprs1.updateString("file", mediaRelativeFilePath);
			uprs1.updateString("md5", md5digest);

			uprs1.updateInt("library_id", libraryID);
			uprs1.updateTimestamp("realStartTime", null);

			uprs1.insertRow();

			int audioFileID = -1;
			uprs1 = stmt1.executeQuery("SELECT LAST_INSERT_ID()");
			if (uprs1.next()) {
				audioFileID = uprs1.getInt(1);
				System.out.println("audioFileID = " + audioFileID);
			} else {
				logger.log(Level.SEVERE, "Failed adding file to DB - didn't get an ID back");
				return false;
			}

			// //////
			// wavFileMetaData

			NesterWavFileMetaData wavFileMetaData = new NesterWavFileMetaData();

			wavFileMetaData.fileSize = audioFile.fileSize;
			wavFileMetaData.dataSize = audioFile.dataSize;
			wavFileMetaData.validRIFFsize = (Boolean) audioFile.validRIFFsize;
			wavFileMetaData.validDATAsize = (Boolean) audioFile.validDATAsize;
			wavFileMetaData.dataStartIndex = audioFile.dataStartIndex;
			wavFileMetaData.wFormatTag = audioFile.wFormatTag;
			wavFileMetaData.numChannels = audioFile.numChannels;
			wavFileMetaData.sampleRate = audioFile.sampleRate;
			wavFileMetaData.nAvgBytesPerSec = audioFile.nAvgBytesPerSec;
			wavFileMetaData.nBlockAlign = audioFile.nBlockAlign;
			wavFileMetaData.bitsPerSample = audioFile.bitsPerSample;
			wavFileMetaData.numBytesToTailPad = audioFile.numBytesToTailPad;
			wavFileMetaData.numFrames = audioFile.numFrames;
			wavFileMetaData.durationInSeconds = audioFile.durationInSeconds;

			if (!wavFileMetaData.saveNesterWavFileMetaData(audioFileID, connection)) {
				logger.log(Level.SEVERE, "audioFileID = " + audioFileID + " filePath = " + audioFile.filePath);
				logger.log(Level.SEVERE, "Failed saving wavFileMetaData");
				return false;
			}

			// //////////
			// staticUserMetaData

			if (!NesterStaticUserMetaData.saveEmptyNesterStaticUserMetaData(audioFileID, connection)) {
				logger.log(Level.SEVERE, "audioFileID = " + audioFileID + " filePath = " + audioFile.filePath);
				logger.log(Level.SEVERE, "Failed saving wavFileMetaData");
				return false;
			}

			// /////////////////
			// Add to Project

			stmt1 = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			uprs1 = stmt1.executeQuery("SELECT * FROM tools_project_mediaFiles");
			uprs1.moveToInsertRow();

			uprs1.updateInt("project_id", projectID);
			uprs1.updateInt("mediaFile_id", audioFileID);

			uprs1.insertRow();

			return true;

		} catch (Exception e) {
			System.out.println("Exception = " + e);

			return false;
		}

	}

	/** Get the absoluteFilePath of a NesterAudioFile. 
	 * 
	 * @return The absolute file path for this file. 
	 */

	public String GetAbsoluteFilePath() {
		String absoluteFilePath = ArloSettings.mediaRootDirectoryPath + "/" + this.mediaRelativeFilePath;
		return absoluteFilePath;
	}


	/** Compute the MD5 Hash of a File.
	 *
	 * @param filepath Path to the file to hash.
	 * @return The 32 character hexadecimal hash as a String, null upon Error.
	 */

	public static String ComputeFileMD5Digest(String filepath) {
		Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		// Setup Digest
		MessageDigest md5digest;
		try {
			md5digest = MessageDigest.getInstance("MD5");
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Unable to initialize MD5 Digest");
			logger.log(Level.SEVERE, "Exception = " + e);
			return null;
		}

		// Setup file and buffers
		InputStream f;
		try {
			f =  new FileInputStream(filepath);
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Unable to open file for Digest");
			logger.log(Level.SEVERE, "Exception = " + e);
			return null;
		}

		int buffer_size = 4096;
		byte[] buffer = new byte[buffer_size];

		// Loop over the file, hash the contents
		// the first iteration is pretty much useless, with 0 bytes in buffer
		int bytes_read = 0;
		while (bytes_read != -1) {
			md5digest.update(buffer, 0, bytes_read);
			try {
				bytes_read = f.read(buffer);
			} catch (Exception e) {
				logger.log(Level.SEVERE, "Unable to read for MD5 Digest");
				logger.log(Level.SEVERE, "Exception = " + e);
				return null;
			}
		}

		// close file
		try {
			f.close();
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Unable to close file during MD5 Digest");
			logger.log(Level.SEVERE, "Exception = " + e);
			return null;
		}

		// generate the HEX string
		byte[] digest_bytes = md5digest.digest();
		String digest_hex = "";
		for (int i = 0; i < digest_bytes.length; i++) {
			// convert byte to Hex, adding 0x100 to ensure 3 byte String
			String h = Integer.toString( ( digest_bytes[i] & 0xff ) + 0x100, 16);
			// skip the MSB, 0x100
			digest_hex += h.substring(1);
		}

		return digest_hex;
	}

	/** \brief Get a list of all MediaFile IDs in the Database.
	*
	* @param connection Opened Database Connection
	* @return List of database IDs of the MediaFiles, null on any error
	*/

	static public Vector<Integer> getAllMediaFileIds(Connection connection) {
		Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		Vector<Integer> mediaFileIds = new Vector<Integer>();

		if (connection == null) {
			return null;
		} else {
			try {

				Statement s1 = connection.createStatement();
				s1.executeQuery("select id from tools_mediafile");
				ResultSet rs1 = s1.getResultSet();

				while (rs1.next()) {
					int mediaFileId = rs1.getInt("id");
					mediaFileIds.add(mediaFileId);
				}

				rs1.close();
				s1.close();

			} catch (Exception e) {
				logger.log(Level.INFO, "Exception = " + e);
				return null;
			}
		}

		return mediaFileIds;
	}


	/** Save the MD5 Hash of a file back to the database.
	 *
	 * @param connection The database connection, already opened and active
	 * @return true on Success, false on error
	 */

	public Boolean saveMD5(Connection connection) {
		Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		if (this.id == -1) {
			logger.log(Level.SEVERE, "Not an initialized file");
			return false;
		}

		try {


			Statement s1 = connection.createStatement();
			String sSql = "UPDATE tools_mediafile SET md5 = ? WHERE id = " + this.id + " LIMIT 1";
			PreparedStatement stmt = connection.prepareStatement(sSql, Statement.RETURN_GENERATED_KEYS);
	
			////
			// set fields
	
			stmt.setString(   1, this.md5);
	
			////
			// Save to Database - execute statement, and get ID
			if (stmt.executeUpdate() != 1) {
				logger.log(Level.SEVERE, "Modified Rows != 1");
				return false;
			}

			stmt.close();

			return true;

		} catch (Exception e) {
			System.out.println("Exception = " + e);

			return false;
		}

	}


}
