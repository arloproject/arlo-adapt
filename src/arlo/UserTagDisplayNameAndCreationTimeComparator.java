package arlo;

import java.util.Comparator;

public class UserTagDisplayNameAndCreationTimeComparator implements Comparator <UserTag> {

	public int compare(UserTag o1, UserTag o2) {
		

		int nameCompareResult = o1.displayName.compareTo(o2.displayName);

		if (nameCompareResult == 0) {

			if (o1.tagCreateTimeInMS == o2.tagCreateTimeInMS) {
				return 0;
			}

			if (o1.tagCreateTimeInMS < o2.tagCreateTimeInMS) {
				return -1;
			}

			else {
				return 1;
			}

		} else {
			return nameCompareResult;
		}

		
	}
	
}
