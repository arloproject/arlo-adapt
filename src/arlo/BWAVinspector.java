package arlo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.DecimalFormat;


public class BWAVinspector {

	DecimalFormat format = new DecimalFormat("###,###,###,###,###");
	DecimalFormat format2digits = new DecimalFormat("00");

	IO1L io = null;

	// for monitoring

	public BWAVinspector() {
		io = new IO1L();
	};

	public BWAVinformation inspectBWAV(File originalFile, boolean verbose) {
		
		verbose = false;

		boolean validRIFFsize = false; // assume true until proven otherwise
		boolean validDATAsize = false; // assume true until proven otherwise

		int dataStartIndex = -1;

		// //////////////////////
		// open original file //
		// //////////////////////

		if (verbose)
			System.out.println("originalFilePath = " + originalFile.getPath());

		RandomAccessFile originalRandomAccessFile = null;
		long orginalFileSize = -1;
		try {
			originalRandomAccessFile = new RandomAccessFile(originalFile, "r");
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		}
		try {
			orginalFileSize = originalRandomAccessFile.length();
		} catch (IOException ex) {
			ex.printStackTrace();
		}

		if (verbose)
			System.out.println("orginalFileSize = " + orginalFileSize);

		int maxHeaderBufferSize = (int) Math.min(orginalFileSize, 10000L);

		// /////////////////////////////////////
		// open and fully read original file //
		// /////////////////////////////////////

		byte[] headerBuffer = new byte[maxHeaderBufferSize];

		try {
			originalRandomAccessFile.readFully(headerBuffer);
		} catch (IOException ex) {
			ex.printStackTrace();
		}

		// ///////////////////////
		// close original file //
		// ///////////////////////

		try {
			originalRandomAccessFile.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}

		if (headerBuffer[0] != 'R' || headerBuffer[1] != 'I' || headerBuffer[2] != 'F' || headerBuffer[3] != 'F') {
			return null;
		}

		// //////////////////////////////
		// initialize buffer pointers //
		// //////////////////////////////

		int originalIndex = 0;

		if (verbose)
			System.out.println("parsing RIFF header");

		int riffLength = 4;
		for (int i = 0; i < riffLength; i++) {

			if (verbose)
				System.out.println(originalIndex + "," + headerBuffer[originalIndex] + "," + (char) headerBuffer[originalIndex]);
			originalIndex++;

		}

		if (verbose)
			System.out.println("parsing RIFF size");

		int riffSize = IO1L.parseDWORD(headerBuffer, originalIndex);
		originalIndex += 4;

		if (verbose)
			System.out.println("riffSize = " + riffSize);

		if (verbose)
			System.out.println("parsing WAVE header");
		int waveLength = 4;
		for (int i = 0; i < waveLength; i++) {

			if (verbose)
				System.out.println(originalIndex + "," + headerBuffer[originalIndex] + "," + (char) headerBuffer[originalIndex]);
			originalIndex++;

		}

		long dataSize = -1;

		int wFormatTag = -1;
		int nChannels = -1;
		int nSamplesPerSec = -1;
		int nAvgBytesPerSec = -1;
		int nBlockAlign = -1;
		int bitsPerSample = -1;

		if (verbose)
			System.out.println("parsing WAVE chunks");
		boolean stopParsing = false;
		while (!stopParsing) {

			// if (originalIndex % 2 == 1) { // why???
			// originalIndex++;
			// }

			if (originalIndex == maxHeaderBufferSize) {
				break;
			}

			int chunkIDSize = 4;

			if (originalIndex == orginalFileSize || originalIndex == maxHeaderBufferSize) {
				break;
			}

			char[] chunkType = new char[4];
			for (int i = 0; i < chunkIDSize; i++) {

				if (verbose)
					System.out.println(originalIndex + "," + headerBuffer[originalIndex] + "," + (char) headerBuffer[originalIndex]);
				chunkType[i] = (char) headerBuffer[originalIndex];
				originalIndex++;

			}

			if (chunkType[0] == 'b' && chunkType[1] == 'e' && chunkType[2] == 'x' && chunkType[3] == 't') {
				if (verbose)
					System.out.println("found bext chunk");

				int bextSize = io.parseDWORD(headerBuffer, originalIndex);
				originalIndex += 4;
				if (verbose)
					System.out.println("bextSize = " + bextSize);

				int bextStartIndex = originalIndex;

				byte[] bextDescription = new byte[256];
				for (int i = 0; i < 256; i++) {
					bextDescription[i] = headerBuffer[originalIndex];
					if (verbose)
						System.out.println(bextDescription[i]);
					originalIndex++;
				}

				String bextDescriptionString = new String(bextDescription);
				if (bextDescriptionString.indexOf(0) != -1)
					bextDescriptionString = bextDescriptionString.substring(0, bextDescriptionString.indexOf(0));
				if (verbose)
					System.out.println("bextDescription = " + bextDescriptionString);

				byte[] bextOriginator = new byte[32];
				for (int i = 0; i < 32; i++) {
					bextOriginator[i] = headerBuffer[originalIndex];
					originalIndex++;
				}
				String bextOriginatorString = new String(bextOriginator);
				if (bextOriginatorString.indexOf(0) != -1)
					bextOriginatorString = bextOriginatorString.substring(0, bextOriginatorString.indexOf(0));
				if (verbose)
					System.out.println("bextOriginatorString = " + bextOriginatorString);

				byte[] bextOriginatorReference = new byte[32];
				for (int i = 0; i < 32; i++) {
					bextOriginatorReference[i] = headerBuffer[originalIndex];
					originalIndex++;
				}
				String bextOriginatorReferenceString = new String(bextOriginatorReference);
				if (bextOriginatorReferenceString.indexOf(0) != -1)
					bextOriginatorReferenceString = bextOriginatorReferenceString.substring(0, bextOriginatorReferenceString.indexOf(0));
				if (verbose)
					System.out.println("bextOriginatorReferenceString = " + bextOriginatorReferenceString);

				byte[] bextOriginatorDate = new byte[10];
				for (int i = 0; i < 10; i++) {
					bextOriginatorDate[i] = headerBuffer[originalIndex];
					originalIndex++;
				}
				String bextOriginatorDateString = new String(bextOriginatorDate);
				if (bextOriginatorDateString.indexOf(0) != -1)
					bextOriginatorDateString = bextOriginatorDateString.substring(0, bextOriginatorDateString.indexOf(0));
				if (verbose)
					System.out.println("bextOriginatorDateString = " + bextOriginatorDateString);

				byte[] bextOriginatorTime = new byte[8];
				for (int i = 0; i < 8; i++) {
					bextOriginatorTime[i] = headerBuffer[originalIndex];
					originalIndex++;
				}
				String bextOriginatorTimeString = new String(bextOriginatorTime);
				if (bextOriginatorTimeString.indexOf(0) != -1)
					bextOriginatorTimeString = bextOriginatorTimeString.substring(0, bextOriginatorTimeString.indexOf(0));
				if (verbose)
					System.out.println("bextOriginatorTimeString = " + bextOriginatorTimeString);

				byte[] bextIgnore1 = new byte[4 + 4 + 2 + 64 + 190];
				for (int i = 0; i < bextIgnore1.length; i++) {
					bextIgnore1[i] = headerBuffer[originalIndex];
				}

				int numBextBytesParsed = originalIndex - bextStartIndex;
				int codingHistorySize = bextSize - numBextBytesParsed;

				byte[] bextCodingHistory = new byte[codingHistorySize];
				for (int i = 0; i < codingHistorySize; i++) {
					bextCodingHistory[i] = headerBuffer[originalIndex];
					originalIndex++;
				}
				String bextCodingHistoryString = new String(bextCodingHistory);
				if (bextCodingHistoryString.indexOf(0) != -1)
					bextCodingHistoryString = bextCodingHistoryString.substring(0, bextCodingHistoryString.indexOf(0));
				if (verbose)
					System.out.println("bextCodingHistoryString = " + bextCodingHistoryString);

			} else if (chunkType[0] == 'f' && chunkType[1] == 'm' && chunkType[2] == 't' && chunkType[3] == ' ') {
				if (verbose)
					System.out.println("found fmt chunk");
				int fmtSize = -1;
				{
					fmtSize = IO1L.parseDWORD(headerBuffer, originalIndex);
					originalIndex += 4;

					// if (fmtSize != 16 && fmtSize != 18 && fmtSize != 20) {
					// System.out.println("Error!  wrong fmtSize(" + fmtSize + ") -- should be 16 or 18 or 20");
					// return null;
					// }
					if (fmtSize < 16) {
						System.out.println("Error!  wrong fmtSize(" + fmtSize + ") -- should be at least 16");
						return null;
					}

					// Format
					//   1 is PCM, what we recognize
					//   common ones we can not handle:
					//     0x0002 Microsoft ADPCM
					//     0x0003 WAVE_FORMAT_IEEE_FLOAT IEEE float
					//     0x0006 WAVE_FORMAT_ALAW 8-bit ITU-T G.711 A-law
					//     0x0007 WAVE_FORMAT_MULAW 8-bit ITU-T G.711 µ-law
					//     0xFFFE WAVE_FORMAT_EXTENSIBLE Determined by SubFormat

					wFormatTag = IO1L.parseWORD(headerBuffer, originalIndex);
					originalIndex += 2;

					if (wFormatTag != 1) {
						System.out.println("Error!  wFormatTag != 1  (found " + wFormatTag + ")");
						return null;
					}

					nChannels = IO1L.parseWORD(headerBuffer, originalIndex);
					originalIndex += 2;

					nSamplesPerSec = IO1L.parseDWORD(headerBuffer, originalIndex);
					originalIndex += 4;

					nAvgBytesPerSec = IO1L.parseDWORD(headerBuffer, originalIndex);
					originalIndex += 4;

					nBlockAlign = IO1L.parseWORD(headerBuffer, originalIndex);
					originalIndex += 2;

					bitsPerSample = IO1L.parseWORD(headerBuffer, originalIndex);
					originalIndex += 2;

					int numPads = fmtSize - 16;

					originalIndex += numPads;

					if (verbose) {
						System.out.println("fmtSize         = " + fmtSize);
						System.out.println("wFormatTag      = " + wFormatTag);
						System.out.println("nChannels       = " + nChannels);
						System.out.println("nSamplesPerSec  = " + nSamplesPerSec);
						System.out.println("nAvgBytesPerSec = " + nAvgBytesPerSec);
						System.out.println("nBlockAlign     = " + nBlockAlign);
						System.out.println("bitsPerSample   = " + bitsPerSample);
					}

				}

			} else if (chunkType[0] == 'd' && chunkType[1] == 'a' && chunkType[2] == 't' && chunkType[3] == 'a') {

				if (verbose)
					System.out.println("found data chunk");
				dataSize = IO1L.parseDWORDLong(headerBuffer, originalIndex);
				if (verbose)
					System.out.println("dataSize = " + dataSize);
				originalIndex += 4;
				dataStartIndex = originalIndex;

				if (orginalFileSize - dataStartIndex >= dataSize) {
					validDATAsize = true;
				}

				// stop parsing after data chunk is found
				break;

			} else /* if chunk type is unknown */ {

				if (verbose)
					System.out.println("found unknown chunk");
				long unknownChunkSize = IO1L.parseDWORDLong(headerBuffer, originalIndex);
				if (verbose)
					System.out.println("unknownChunkSize = " + unknownChunkSize);
				originalIndex += 4;
				originalIndex += unknownChunkSize;
			}

		}
		if (verbose)
			System.out.println("originalIndex = " + originalIndex);

		int maxNumBytesToTailPad = 2;
		long numBytesToTailPad = -(orginalFileSize - dataStartIndex - dataSize);

		if (numBytesToTailPad <= maxNumBytesToTailPad) {
			validRIFFsize = true;
			validDATAsize = true;
		}

		BWAVinformation result = new BWAVinformation();

		result.validRIFFsize = validRIFFsize;
		result.validDATAsize = validDATAsize;

		result.dataStartIndex = dataStartIndex;
		result.dataSize = dataSize;

		result.wFormatTag = wFormatTag;
		result.nChannels = nChannels;
		result.nSamplesPerSec = nSamplesPerSec;
		result.nAvgBytesPerSec = nAvgBytesPerSec;
		result.nBlockAlign = nBlockAlign;
		result.bitsPerSample = bitsPerSample;
		result.numBytesToTailPad = (int) numBytesToTailPad;

		return result;
	}

	public class BWAVinformation {

		public final static long serialVersionUID = 79837222346732L;

		public boolean validRIFFsize;
		public boolean validDATAsize;

		// from data chunk
		public int dataStartIndex;
		public long dataSize;

		// from bwav fmt chunk
		public int wFormatTag;
		public int nChannels;
		public int nSamplesPerSec;
		public int nAvgBytesPerSec;
		public int nBlockAlign;
		public int bitsPerSample;

		// fixup data
		public int numBytesToTailPad;

	}

}
