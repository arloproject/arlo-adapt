/**
 * 
 */

package arlo;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;

import org.dataflow.execution.ExecutionEngine;
import org.dataflow.execution.ResultsCollector;
import org.dataflow.wiring.BasicDataflow;
import org.dataflow.wiring.ComponentDefinition;
import org.dataflow.wiring.formats.RDFDataflowHandler;


/**
 * This class wraps the Arlo SVM model builder and applier, and provides access to the 
 * exposed properties. There is also a convenient execute method will run the workflow,
 * and return when done. This method is provided an input stream for logging, or null
 * for no output.
 * @author redman
 */
public class SVMModelRunner {
    
    /** the name of the label data generator component. */
    final private String LABEL_DATA_GEN = "arlo-label-data-generator/0";
    
    /** the name of the data generator to get the application data set. */
    final private String APPLY_DATA_GEN = "arlo-data-generator/0";
    
    /** the name of the label data generator component. */
    final private String NFOLD_SVM_MODEL_PRODUCER = "weka-svm-producer/0";
    
    /** the name of the label data generator component. */
    final private String FINAL_SVM_MODEL_PRODUCER = "weka-svm-producer/2";
    
    /** the name of the label data generator component. */
    final private String TAG_WRITER = "arlo-tag-writer/0";
    
    /** the dataflow. */
    private BasicDataflow df;
    
    /** defines the machine tagged data. */
    private int calledTagSet;
    
    /** This tag set provides label data against the example project media files. */
    private int labelTagSet;
    
    /** number of frames per second. */
    private int framesPerSecond = 1;
    
    /** number of frames per example. */
    private int framesPerExample = 1;
    
    /** the frequency bands. */
    private int frequencyBands = 256;
    
    /** the complexity constant. */
    private double complexityConstant = 1.0;
    
    /** the user account. */
    private String account = "";
    
    /** the password. */
    private String password = "";
    
    /** the host name. */
    private String host = "live.arloproject.com";
    
    /** the port. */
    private int port = 81;
    
	private double dampingFactor = 0.5D;

	private int minFrequency = 600;

	private int maxFrequency = 5000;

    /**
     * the runner will load the dataflow from the resource fork of the jar file wrapping all required data.
     */
    public SVMModelRunner() {
        InputStream is = this.getClass().getClassLoader().getResourceAsStream("dataflow.rdf");

        if (is == null) {
            throw new IllegalArgumentException("NULL Returned when loading dataflow.rdf resource");
        }

        df = RDFDataflowHandler.getDataflowFromRDFStream(is);
        if (df == null) {
            throw new IllegalArgumentException("NULL Returned when loading dataflow.rdf resource Stream");
        }
    }

    /**
     * @return the framesPerSecond
     */
    public int getFramesPerSecond() {
        return framesPerSecond;
    }

    /**
     * @param framesPerSecond the framesPerSecond to set
     */
    public void setFramesPerSecond(int framesPerSecond) {
        final String KEY = "Slices Per Second";
        this.framesPerSecond = framesPerSecond;
        ComponentDefinition dc = df.getComponents().get(LABEL_DATA_GEN);
        try {
            dc.getComponent().setProperty(KEY, Integer.toString(framesPerSecond));
            dc = df.getComponents().get(APPLY_DATA_GEN);
            if (dc != null)
                dc.getComponent().setProperty(KEY, Integer.toString(framesPerSecond));
            dc = df.getComponents().get(TAG_WRITER);
            dc.getComponent().setProperty(KEY, Integer.toString(framesPerSecond));
        } catch (SecurityException | IllegalArgumentException | IllegalAccessException e) {
            throw new IllegalArgumentException("The property, \"Project\", could not be set!",e);
        }
    }


    /**
     * @return the framesPerExample
     */
    public int getFramesPerExample() {
        return framesPerExample;
    }

    /**
     * @param framesPerExample the framesPerExample to set
     */
    public void setFramesPerExample(int framesPerExample) {
        final String KEY = "Slices Per Example";
        this.framesPerExample = framesPerExample;
        ComponentDefinition dc = df.getComponents().get(LABEL_DATA_GEN);
        try {
            dc.getComponent().setProperty(KEY, Integer.toString(framesPerExample));
            dc = df.getComponents().get(APPLY_DATA_GEN);
            if (dc != null)
                dc.getComponent().setProperty(KEY, Integer.toString(framesPerExample));
            dc = df.getComponents().get(TAG_WRITER);
            dc.getComponent().setProperty(KEY, Integer.toString(framesPerExample));
        } catch (SecurityException | IllegalArgumentException | IllegalAccessException e) {
            throw new IllegalArgumentException("The property, \"Project\", could not be set!",e);
        }
    }

	public void setDampingFactor(double damping) {
		String KEY = "Damping Factor";
		dampingFactor = damping;
		ComponentDefinition dc = (ComponentDefinition)df.getComponents().get("arlo-label-data-generator/0");
		try {
			dc.getComponent().setProperty("Damping Factor", Double.toString(dampingFactor));
			dc = (ComponentDefinition)df.getComponents().get("arlo-data-generator/0");
			if (dc != null)
				dc.getComponent().setProperty("Damping Factor", Double.toString(dampingFactor));
		} catch (SecurityException|IllegalArgumentException|IllegalAccessException e) {
			throw new IllegalArgumentException("The property, \"Project\", could not be set!", e);
		}
	}
  

	public void setMinFrequency(int mF) {
		String KEY = "Min Frequency";
		minFrequency = mF;
		ComponentDefinition dc = (ComponentDefinition)df.getComponents().get("arlo-label-data-generator/0");
		try {
			dc.getComponent().setProperty("Min Frequency", Integer.toString(minFrequency));
			dc = (ComponentDefinition)df.getComponents().get("arlo-data-generator/0");
			if (dc != null)
				dc.getComponent().setProperty("Min Frequency", Integer.toString(minFrequency));
		} catch (SecurityException|IllegalArgumentException|IllegalAccessException e) {
			throw new IllegalArgumentException("The property, \"Project\", could not be set!", e);
		}
	}


	public void setMaxFrequency(int mxF) {
		String KEY = "Max Frequency";
		maxFrequency = mxF;
		ComponentDefinition dc = (ComponentDefinition)df.getComponents().get("arlo-label-data-generator/0");
		try {
			dc.getComponent().setProperty("Max Frequency", Integer.toString(maxFrequency));
			dc = (ComponentDefinition)df.getComponents().get("arlo-data-generator/0");
			if (dc != null)
			dc.getComponent().setProperty("Max Frequency", Integer.toString(maxFrequency));
		} catch (SecurityException|IllegalArgumentException|IllegalAccessException e) {
			throw new IllegalArgumentException("The property, \"Project\", could not be set!", e);
		}
	}


    /**
     * @return the frequencyBands
     */
    public int getFrequencyBands() {
        return frequencyBands;
    }

    /**
     * @param frequencyBands the frequencyBands to set
     */
    public void setFrequencyBands(int frequencyBands) {
        final String KEY = "Frequency Bands";
        this.frequencyBands = frequencyBands;
        ComponentDefinition dc = df.getComponents().get(LABEL_DATA_GEN);
        try {
            dc.getComponent().setProperty(KEY, Integer.toString(frequencyBands));
            dc = df.getComponents().get(APPLY_DATA_GEN);
            if (dc != null)
                dc.getComponent().setProperty(KEY, Integer.toString(frequencyBands));
            dc = df.getComponents().get(TAG_WRITER);
            dc.getComponent().setProperty(KEY, Integer.toString(frequencyBands));
        } catch (SecurityException | IllegalArgumentException | IllegalAccessException e) {
            throw new IllegalArgumentException("The property, \"Project\", could not be set!",e);
        }
    }
    
    /**
     * @return the complexityConstant
     */
    public double getComplexityConstant() {
        return complexityConstant;
    }

    /**
     * @param complexityConstant the complexityConstant to set
     */
    public void setComplexityConstant(double complexityConstant) {
        final String KEY = "complexity constant";
        this.complexityConstant = complexityConstant;
        ComponentDefinition dc = df.getComponents().get(NFOLD_SVM_MODEL_PRODUCER);
        try {
            dc.getComponent().setProperty(KEY, Double.toString(complexityConstant));
            dc = df.getComponents().get(FINAL_SVM_MODEL_PRODUCER);
            dc.getComponent().setProperty(KEY, Double.toString(complexityConstant));
        } catch (SecurityException | IllegalArgumentException | IllegalAccessException e) {
            throw new IllegalArgumentException("The property, \"Project\", could not be set!",e);
        }
    }

    /**
     * @return the account
     */
    public String getAccount() {
        return account;
    }

    /**
     * @param account the account to set
     */
    public void setAccount(String account) {
        final String KEY = "Account";
        this.account = account;
        ComponentDefinition dc = df.getComponents().get(LABEL_DATA_GEN);
        try {
            dc.getComponent().setProperty(KEY, this.account);
            dc = df.getComponents().get(APPLY_DATA_GEN);
            if (dc != null)
                dc.getComponent().setProperty(KEY, this.account);
            dc = df.getComponents().get(TAG_WRITER);
            dc.getComponent().setProperty(KEY, account);
        } catch (SecurityException | IllegalArgumentException | IllegalAccessException e) {
            throw new IllegalArgumentException("The property, \"Project\", could not be set!",e);
        }
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        final String KEY = "Password";
        this.password = password;
        ComponentDefinition dc = df.getComponents().get(LABEL_DATA_GEN);
        try {
            dc.getComponent().setProperty(KEY, this.password);
            dc = df.getComponents().get(APPLY_DATA_GEN);
            if (dc != null)
                dc.getComponent().setProperty(KEY, this.password);
            dc = df.getComponents().get(TAG_WRITER);
            dc.getComponent().setProperty(KEY, password);
        } catch (SecurityException | IllegalArgumentException | IllegalAccessException e) {
            throw new IllegalArgumentException("The property, \"Project\", could not be set!",e);
        }
    }

    /**
     * @return the host
     */
    public String getHost() {
        return host;
    }

    /**
     * @param host the host to set
     */
    public void setHost(String host) {
        final String KEY = "Host";
        this.host = host;
        ComponentDefinition dc = df.getComponents().get(LABEL_DATA_GEN);
        try {
            dc.getComponent().setProperty(KEY, this.host);
            dc = df.getComponents().get(APPLY_DATA_GEN);
            if (dc != null)
                dc.getComponent().setProperty(KEY, this.host);
            dc = df.getComponents().get(TAG_WRITER);
            dc.getComponent().setProperty(KEY, this.host);
        } catch (SecurityException | IllegalArgumentException | IllegalAccessException e) {
            throw new IllegalArgumentException("The property, \"Project\", could not be set!",e);
        }
    }

    /**
     * @return the port
     */
    public int getPort() {
        return port;
    }

    /**
     * @param port the port to set
     */
    public void setPort(int port) {
        final String KEY = "Port";
        this.port = port;
        ComponentDefinition dc = df.getComponents().get(LABEL_DATA_GEN);
        try {
            dc.getComponent().setProperty(KEY, Integer.toString(this.port));
            dc = df.getComponents().get(APPLY_DATA_GEN);
            if (dc != null)
                dc.getComponent().setProperty(KEY, Integer.toString(this.port));
            dc = df.getComponents().get(TAG_WRITER);
            dc.getComponent().setProperty(KEY, Integer.toString(this.port));
        } catch (SecurityException | IllegalArgumentException | IllegalAccessException e) {
            throw new IllegalArgumentException("The property, \"Project\", could not be set!",e);
        }
    }
    
    /**
     * @return the labelTagSet
     */
    public int getLabelTagSet() {
        return labelTagSet;
    }

    /**
     * @param labelTagSet the labelTagSet to set
     */
    public void setLabelTagSet(int labelTagSet) {
        final String KEY = "TagSet";
        this.labelTagSet = labelTagSet;
        ComponentDefinition dc = df.getComponents().get(LABEL_DATA_GEN);
        try {
            dc.getComponent().setProperty(KEY, Integer.toString(labelTagSet));
        } catch (SecurityException | IllegalArgumentException | IllegalAccessException e) {
            throw new IllegalArgumentException("The property, \"TagSet\", could not be set!",e);
        }
    }

    /**
     * @return the calledTagSet
     */
    public int getCalledTagSet() {
        return calledTagSet;
    }

    /**
     * @param calledTagSet the calledTagSet to set
     */
    public void setCalledTagSet(int calledTagSet) {
        final String KEY = "TagSet";
        this.calledTagSet = calledTagSet;
        ComponentDefinition dc = df.getComponents().get(APPLY_DATA_GEN);
        try {
            dc.getComponent().setProperty(KEY, Integer.toString(calledTagSet));
            dc = df.getComponents().get(TAG_WRITER);
            dc.getComponent().setProperty(KEY, Integer.toString(calledTagSet));
        } catch (SecurityException | IllegalArgumentException | IllegalAccessException e) {
            throw new IllegalArgumentException("The property, \"Project\", could not be set!",e);
        }
    }

    /**
     * this method will execute the dataflow.
     * @param name a name for the flow.
     * @param os the output stream to send logging to.
     * @return a list of results each entry keyed on module name (sort of), containing a list of named results.
     * @throws Exception 
     */
    public HashMap<String, HashMap<String, Object>> execute(String name, OutputStream os) throws Exception {
        final ResultsCollector collector = new ResultsCollector(this.df);
        ExecutionEngine ee = new ExecutionEngine(this.df);
        ee.getThrottle().addActivityInterface(collector);
        
        // start up the workflow.
        ee.start(name, os);

        // just wait to finish.
        Thread.yield();
        while (!ee.isDone()) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        
        // Save all the resulting good stuff, models, tables, really anything any modules wants to save
        // as a result.
        return collector.getResults();
    }
    
    /**
     * Test and exemplify the use of this class. For our testing, the TagSet id for the tags 
     * used to provide labeled data is 863. The tag set we will produce with machine labeling is
     * tagset 876, in project PS-2kSubset with id 363.
     * @param args
     */
    static public void main(String [] args) {
        SVMModelRunner modeler = new SVMModelRunner();
        modeler.setAccount("");
        modeler.setComplexityConstant(1.0);
        modeler.setFramesPerExample(1);
        modeler.setFramesPerSecond(1);
        modeler.setFrequencyBands(256);
        modeler.setHost("live.arloproject.com");
        modeler.setPassword("");
        modeler.setPort(81);
        modeler.setLabelTagSet(863);
        modeler.setCalledTagSet(876);
        try {
            modeler.execute("TestThis", (OutputStream) null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
