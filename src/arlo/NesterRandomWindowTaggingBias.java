//package arlo;

//import java.sql.Connection;
//import java.sql.ResultSet;
//import java.sql.Statement;
//import java.util.logging.Level;
//import java.util.logging.Logger;

//
// This class is now unused, but it will probably be making a return someday.
//

// Eventually, we need to factor out the Static method Job.GetJobParameter()
// below. This should init the SuperClass and use it's method.

//public class NesterRandomWindowTaggingBias  extends NesterJob {
//
//	public double windowSizeInSeconds = Double.NaN;
//	public int numWindowsToTag = -1;
//
//	// REPRESENTATION BIAS
//	public int numFrequencyBands = -1;
//	public double numTimeFramesPerSecond = Double.NaN;
//	public double dampingRatio = Double.NaN;
//	public double minFrequency = Double.NaN;
//	public double maxFrequency = Double.NaN;
//
//	NesterRandomWindowTaggingBias(int id, int user_id, int project_id, String name, int numberOfWindowsToTag, double windowSizeInSeconds, int numFrequencyBands,
//			double numTimeFramesPerSecond, double dampingRatio, double minFrequency, double maxFrequency) {
//
//		this.id = id;
//		this.user_id = user_id;
//		this.project_id = project_id;
//		this.name = name;
//
//		this.windowSizeInSeconds = windowSizeInSeconds;
//
//		this.numWindowsToTag = numberOfWindowsToTag;
//
//		this.numFrequencyBands = numFrequencyBands;
//		this.numTimeFramesPerSecond = numTimeFramesPerSecond;
//		this.dampingRatio = dampingRatio;
//		this.minFrequency = minFrequency;
//		this.maxFrequency = maxFrequency;
//
//	}
//
//	public String getSpectraComputationBiasString() {
//		String string = "numFrequencyBands=" + numFrequencyBands + "numTimeFramesPerSecond=" + numTimeFramesPerSecond + "dampingRatio=" + dampingRatio
//				+ "minFrequency=" + minFrequency + "maxFrequency=" + maxFrequency;
//		return string;
//
//	}
//
//	////
//	// Get a NesterRandomWindowTaggingBias from the database, return null if fail
//
//	static public NesterRandomWindowTaggingBias getNesterRandomWindowTaggingBias(int id, Connection connection) {
//
//		NesterRandomWindowTaggingBias bias = null;
//
//		Logger logger;
//		logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
//
//		// Job Info
//		boolean gotJobData = false;
//		int user_id = 0; int project_id = 0; String name = "";
//
//		// Parameters
//		boolean gotParameters = false;
//		int numWindowsToTag = 0; double windowSizeInSeconds = 0; int numFrequencyBands = 0;
//		double numTimeFramesPerSecond = 0; double dampingRatio = 0; double minFrequency = 0; double maxFrequency = 0;
//
//		/////////////////
//		// get Job info
//		try {
//
//			Statement s1 = connection.createStatement();
//			s1.executeQuery("select * from tools_jobs where id = " + id);
//			ResultSet rs1 = s1.getResultSet();
//
//			if (rs1.next()) {
//				user_id = 			rs1.getInt("user_id");
//				project_id = rs1.getInt("project_id");
//				name = rs1.getString("name");
//
//				gotJobData = true;
//			}
//			rs1.close();
//			s1.close();
//
//		} catch (Exception e) {
//			logger.log(Level.INFO, "Job not found");
//			logger.log(Level.INFO, "Exception = " + e);
//		}
//
//		//////////////////////
//		// Get Job Parameters
//logger.log(Level.INFO, "Get Job Params");
//		gotJobData = true;
//		String value = "";
//
//		value = NesterJob.GetJobParameter(id, "numWindowsToTag", connection);
//		if (value != null) {
//			numWindowsToTag = Integer.parseInt(value);
//		} else {
//			gotJobData = false;
//		}
//
//		value = NesterJob.GetJobParameter(id, "windowSizeInSeconds", connection);
//		if (value != null) {
//			windowSizeInSeconds = Double.parseDouble(value);
//		} else {
//			gotJobData = false;
//		}
//
//		value = NesterJob.GetJobParameter(id, "numFrequencyBands", connection);
//		if (value != null) {
//			numFrequencyBands = Integer.parseInt(value);
//		} else {
//			gotJobData = false;
//		}
//
//		value = NesterJob.GetJobParameter(id, "numTimeFramesPerSecond", connection);
//		if (value != null) {
//			numTimeFramesPerSecond = Double.parseDouble(value);
//		} else {
//			gotJobData = false;
//		}
//
//		value = NesterJob.GetJobParameter(id, "dampingRatio", connection);
//		if (value != null) {
//			dampingRatio = Double.parseDouble(value);
//		} else {
//			gotJobData = false;
//		}
//
//		value = NesterJob.GetJobParameter(id, "minFrequency", connection);
//		if (value != null) {
//			minFrequency = Double.parseDouble(value);
//		} else {
//			gotJobData = false;
//		}
//
//		value = NesterJob.GetJobParameter(id, "maxFrequency", connection);
//		if (value != null) {
//			maxFrequency = Double.parseDouble(value);
//		} else {
//			gotJobData = false;
//		}
//
//		if (gotJobData == false) {
//			logger.log(Level.INFO, "Not all Job Parameters were found");
//			return null;
//		}
//
//		////////////////////
//		// initialize Bias
//		bias = new NesterRandomWindowTaggingBias(
//			id,
//			user_id,
//			project_id,
//			name,
//			numWindowsToTag,
//			windowSizeInSeconds,
//			numFrequencyBands,
//			numTimeFramesPerSecond,
//			dampingRatio,
//			minFrequency,
//			maxFrequency);
//
//		return bias;
//	}
//
//
//}
