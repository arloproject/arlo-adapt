package arlo;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;


import adapt.IO;
import adapt.SimpleTable;

public class UserState implements Serializable {

	public final static long serialVersionUID = 729385610392L;

	transient public Logger logger;
	transient public CreateSpectraImage createSpectraImage;

	public String userDefinedTagsFilePath = null;
	public SimpleTable userDefinedTagTable = null;
	public ArrayList<String> tagNames = null;

	public int spectraBorderWidth = -1;
	public int spectraTopBorderHeight = -1;
	public int timeScaleHeight = -1;

	public boolean showSpectra;

	public boolean showCatalog;
	public boolean showTimeScale;
	public boolean showFrequencyScale;
	public int catalogTypeIndex;  // 
	public final static int CATALOG_TYPE_ALL_TAG_CLASSES = 0;
	public final static int CATALOG_TYPE_TAG_CLASS_EXAMPLES = 1;
	public int catalogTagClassIndex;  // 
	public int catalogBorderHeight = -1;
	public int catalogBorderWidth = -1;
	public int catalogSpectraNumFrequencyBands = -1;
	public int catalogSpectraNumFramesPerSecond = -1;
	public ArrayList<UserTag> catalogFirstTags = new ArrayList<UserTag>();
	public ArrayList<UserTag> catalogClassExampleTags = new ArrayList<UserTag>();

	public String loginName;
	public String password;
	public String filePath;
	public long currentLoginTime;
	public long lastLoginTime;
	public int loginNumTimes;
	//
	// data uploading
	//
	public String userDataDirectoryPath;
	public String dataUploadLoginName;
	public String dataUploadPassword;
	public String dataUploadMachineIP;
	public String dataUploadSourceDirectory;
	//
	//
	public String lastMainMenuChoice;

	public UserInformation userInformation;

	// example definition
	public double windowSizeInSeconds;
	public int windowNumTimeFrames;
	public double windowFractionOverlap;
	public double spectraMinimumBandFrequency;
	public double spectraMaximumBandFrequency;
	public double spectraDampingFactor;
	public int spectraNumFrequencyBands;
	public int spectraNumFramesPerSecond;
	public boolean subwindowTagging;
	public boolean ignoreFrequencyBoundaries;
	public boolean simpleNormalization;
	public double gain;
	public boolean frequencyBandNormalization;

	public boolean normalizeAudioVolume;

	public boolean activeLearning;
	public String activeLearningModelName;
	public int activeLearningNumProbes;
	public double activeLearningTargetOutput;

	public int learningNumProbes;
	public int learningBestBiasSpaceProbeIndex;
	public double learningBestAccuracy;
	public int learningNumRepresentationRepeats;
	public double learningBestFalseNegativeRate;
	public double learningBestFalsePositiveRate;
	public double learningBestDuration;
	public int learningBestNumTrueNegatives;
	public int learningBestNumTruePositives;
	public int learningBestNumFalseNegatives;
	public int learningBestNumFalsePositives;

	// example set definition
	public int exampleSetNumExamples;

	// audio file collection

	public Vector<AudioFile> audioFileVector;

	// tagged examples
	public ArrayList<UserTag> taggedExamples;

	// learning algorithm definition
	public int numTrainingExamples;
	public int numNearestNeighbors;

	//
	public Model modelParameterMinValues;
	public Model modelParameterMaxValues;
	public Model currentModel;
	public Model bestModel;
	public ConcurrentHashMap<String, Model> modelSpace;
	public String newModelName;
	public Model active;

	// apply model
	public int maxTimeInSeconds;

	public UserState() {
	
	}
	
	public UserState(Logger logger, String loginName, String password, String filePath, String userDataDirectoryPath) {

		this.logger = logger;
		this.createSpectraImage = new CreateSpectraImage(this);

		this.loginName = loginName;
		this.filePath = filePath;
		this.password = password;
		this.userDataDirectoryPath = userDataDirectoryPath;

		userInformation = new UserInformation();

		if (userDataDirectoryPath != null) {
			File userDataDirectory = new File(userDataDirectoryPath);
			logger.info("###### UserState : userDataDirectory = " + userDataDirectory);

			boolean mkdirResult = userDataDirectory.mkdir();
			logger.info("###### UserState : mkdirResult = " + mkdirResult);
		}

		spectraMinimumBandFrequency = 40.0;
		spectraMaximumBandFrequency = 16000.0;
		spectraDampingFactor = 0.02;
		spectraNumFrequencyBands = 256;
		spectraNumFramesPerSecond = 128;

		windowSizeInSeconds = 4.0;
		windowFractionOverlap = 0.0;

		subwindowTagging = true;
		ignoreFrequencyBoundaries = true;
		simpleNormalization = true;
		frequencyBandNormalization = false;

		normalizeAudioVolume = true;
		learningNumProbes = 20;

		modelParameterMinValues = new Model();
		modelParameterMaxValues = new Model();
		currentModel = new Model();
		bestModel = new Model();
		active = new Model();

		audioFileVector = null;
		taggedExamples = null;

		learningBestAccuracy = Double.NEGATIVE_INFINITY;

		modelSpace = new ConcurrentHashMap<String, Model>();
		newModelName = "";

		activeLearningNumProbes = 100;
		activeLearningTargetOutput = 1.0;

		showSpectra = true;

		showCatalog = true;

	}

	public void setDefaults(Logger logger) {

		this.logger = logger;
		this.createSpectraImage = new CreateSpectraImage(this);

		if (learningNumRepresentationRepeats <= 0)
			learningNumRepresentationRepeats = 1;

		spectraBorderWidth = 50;

		spectraTopBorderHeight = 30;

		catalogBorderWidth = 1;

		catalogBorderHeight = 20;
		timeScaleHeight = 22;

		catalogSpectraNumFrequencyBands = spectraNumFrequencyBands;

		catalogSpectraNumFramesPerSecond = spectraNumFramesPerSecond;

		if (modelParameterMinValues == null) {
			modelParameterMinValues = new Model();
		}
		if (modelParameterMaxValues == null) {
			modelParameterMaxValues = new Model();
		}
		if (currentModel == null) {
			currentModel = new Model();
		}
		if (bestModel == null) {
			bestModel = new Model();
		}
		if (active == null) {
			active = new Model();
		}
		if (modelSpace == null) {
			modelSpace = new ConcurrentHashMap<String, Model>();
		}

	}

	public void saveToDisk() {
		IO.writeObject(new File(filePath), this);
	}

	public String toString() {

		Date currentLoginDate = new Date(currentLoginTime);
		Date lastLoginDate = new Date(lastLoginTime);

		String string = "";

		string += "UserState:  \n";

		string += "  Login Name            = " + loginName + "\n";
		string += "  Password              = " + password + "\n";
		string += "  objectFilePath        = " + filePath + "\n";
		string += "  Current Login Time    = " + currentLoginDate + "\n";
		if (lastLoginTime != 0)
			string += "  Last Login Time       = " + lastLoginDate + "\n";
		string += "  Login Number of Times = " + loginNumTimes + "\n";

		return string;

	}

	public void renameTag(Logger logger, String oldTagName, String newTagName) {

		logger.info("### starting renameTag ###");

		String tagDirectoryPath = SharedData.userStateDirectory + loginName + "/tags/";

		logger.info("### oldTagName = " + oldTagName);
		logger.info("### newTagName = " + newTagName);

		int numTaggedExamples = taggedExamples.size();
		logger.info("### numTaggedExamples = " + numTaggedExamples);

		for (Iterator<UserTag> iterator = taggedExamples.iterator(); iterator.hasNext();) {

			UserTag userTag = (UserTag) iterator.next();

			int numTags = userTag.tagStrings.size();

			// get old tag strength

			boolean oldTagFound = false;
			int oldTagNameIndex = -1;
			int oldTagNameStrength = -1;

			for (int i = 0; i < numTags; i++) {

				String tagName = userTag.tagStrings.get(i);

				if (tagName.equals(oldTagName)) {
					oldTagFound = true;
					oldTagNameIndex = i;
					oldTagNameStrength = userTag.tagStrengths.get(i);
					break;

				}

			}

			if (oldTagFound) {

				// erase old tag name

				userTag.tagStrings.remove(oldTagNameIndex);
				userTag.tagStrengths.remove(oldTagNameIndex);

				// get new tag strength

				numTags = userTag.tagStrings.size();

				boolean newTagFound = false;
				int newTagNameIndex = -1;
				int newTagNameStrength = -1;

				for (int i = 0; i < numTags; i++) {

					String tagName = userTag.tagStrings.get(i);

					if (tagName.equals(newTagName)) {
						newTagFound = true;
						newTagNameIndex = i;
						newTagNameStrength = userTag.tagStrengths.get(i);
						break;

					}

				}

				if (newTagFound) {
					if (oldTagNameStrength > newTagNameStrength) {
						userTag.tagStrengths.set(newTagNameIndex, oldTagNameStrength);
					}
				} else {
					userTag.tagStrings.add(newTagName);
					userTag.tagStrengths.add(oldTagNameStrength);
				}

				String tagFilePath = SharedData.userStateDirectory + loginName + "/tags/" + userTag.tagCreateTimeInMS;

				if (false) {
					
					String newTagFilePath = tagFilePath + ".old";

					IO.rename(tagFilePath, newTagFilePath);
				}

				IO.writeObject(tagFilePath, userTag);
			}

		}

		// adjust tags file

		int numTagNames = -1;

		// remove old tag

		while (tagNames.remove(oldTagName)) {

		}

		// add new tag

		if (!tagNames.contains(newTagName))
			tagNames.add(newTagName);

		numTagNames = tagNames.size();

		userDefinedTagTable.numDataRows = numTagNames;
		userDefinedTagTable.stringMatrix = new String[numTagNames][1];
		for (int i = 0; i < numTagNames; i++) {
			userDefinedTagTable.stringMatrix[i][0] = tagNames.get(i);
		}

		userDefinedTagTable.writeToFile(userDefinedTagsFilePath);

		logger.info("### finished renameTag ###");

	}
}
