package arlo;

import java.io.Serializable;
import java.util.ArrayList;

public class UserTag implements Serializable {

	public final static long serialVersionUID = 6201428756378L;

	public boolean randomlyChosen;

	public String displayName;
	public int tagClassIndex;

	public String audioFilePath;
	public AudioFile audioFile;
	public long tagCreateTimeInMS;
	public double startTime;
	public double endTime;
	public double startFrequency;
	public double endFrequency;
	public ArrayList<String> tagStrings;
	public ArrayList<Integer> tagStrengths;

	public UserTag(boolean randomlyChosen, long userTagCreationTimeInMS, String audioFilePath, AudioFile audioFile, double startTime, double endTime, double startFrequency,
			double endFrequency, ArrayList<String> tagStrings, ArrayList<Integer> tagStrengths) {

		this.randomlyChosen = randomlyChosen;

		this.tagCreateTimeInMS = userTagCreationTimeInMS;

		this.audioFilePath = audioFilePath;
		this.audioFile = audioFile;
		this.startTime = startTime;
		this.endTime = endTime;
		this.startFrequency = startFrequency;
		this.endFrequency = endFrequency;
		this.tagStrings = tagStrings;
		this.tagStrengths = tagStrengths;
	}

	public boolean hasTag(String tagName) {

		boolean value = false;

		int index = tagStrings.indexOf(tagName);

		if (index != -1) {
			int strength = tagStrengths.get(index);
			if (strength > 0)
				value = true;
		}

		return value;

	}
	// public int creationTimeCompareTo(UserTag o) {
	//
	//
	// if (o.tagCreateTimeInMS == this.tagCreateTimeInMS) {
	// return 0;
	// }
	//
	// if (o.tagCreateTimeInMS < this.tagCreateTimeInMS) {
	// return -1;
	// }
	//
	// else {
	// return 1;
	// }
	//
	//
	// }
	//
	//
	// public int displayNameCompareTo(UserTag o) {
	//
	// int nameCompareResult = this.displayName.compareTo(o.displayName);
	//
	// if (nameCompareResult == 0) {
	//
	// if (o.tagCreateTimeInMS == this.tagCreateTimeInMS) {
	// return 0;
	// }
	//
	// if (o.tagCreateTimeInMS < this.tagCreateTimeInMS) {
	// return -1;
	// }
	//
	// else {
	// return 1;
	// }
	//
	// } else {
	// return nameCompareResult;
	//
	// }
	//
	// }

}
