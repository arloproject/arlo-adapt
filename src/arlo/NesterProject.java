package arlo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Vector;
import java.util.HashMap;

import java.util.Iterator;

public class NesterProject {

	static public int getProjectID(int userID, String projectName, Connection connection) {
		Logger logger;
		logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		try {

			Statement s = connection.createStatement();
			s.executeQuery("SELECT * FROM tools_project where user_id = " + userID + " && " + " name = \"" + projectName + "\"");
			ResultSet rs = s.getResultSet();
			if (rs.next()) {

				int id = rs.getInt("id");

				return id;

			}
			rs.close();
			s.close();
			logger.log(Level.INFO, "name not found");

			return -1;

		} catch (Exception e) {
			logger.log(Level.INFO, "Exception = " + e);
		}

		return -1;
	}

	/** \brief Get a list of all MediaFile IDs in a Project.
	*
	* @param projectId The database ID of the Project. 
	* @param connection Opened Database Connection
	* @return List of database IDs of the MediaFiles
	*/

	static public Vector<Integer> getProjectMediaFileIds(int projectId, Connection connection) {
		Logger logger;
		logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);


		Vector<Integer> mediaFileIds = new Vector<Integer>();

		try {

			Statement s1 = connection.createStatement();
			s1.executeQuery("select * from tools_project_mediaFiles where project_id = " + projectId);
			ResultSet rs1 = s1.getResultSet();

			while (rs1.next()) {
				int mediaFileId = rs1.getInt("mediaFile_id");
				mediaFileIds.add(mediaFileId);
			}

			rs1.close();
			s1.close();

			return mediaFileIds;

		} catch (Exception e) {
			logger.log(Level.INFO, "name not found");
			logger.log(Level.INFO, "Exception = " + e);
		}

		return mediaFileIds;
	}


	/** \brief Get a list of all MediaFile objects in a Project.
	*
	* @param projectId The database ID of the Project.
	* @param activeOnly Only return files set as 'active'
	* @param fieldNames
	* @param connection Opened Database Connection
	* @return List of NesterAudioFile objects
	*/

	public static Vector<NesterAudioFile> getNesterProjectAudioFiles(int projectID, boolean activeOnly, HashMap<String, Boolean> fieldNames, Connection connection) {
		return NesterAudioFile.getNesterProjectAudioFiles(projectID, activeOnly, fieldNames, connection);
	}


}
