package arlo;

import java.util.Vector;
import java.util.Iterator;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.File;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;


import arlo.NesterUser;
import arlo.NesterAudioFile;
import arlo.NesterJob;

import arlo.QueueTask;

public class QueueTask_SupervisedTagDiscoveryParent extends QueueTask {

	static final private String taskName = "SupervisedTagDiscoveryParent";

	public boolean runQueue(Vector<NesterUser> restrictedUsers) {
		return super._runQueue(taskName, restrictedUsers);
	}

	void runJob(NesterJob job) {

		if (job == null) {
			System.out.println("QueueTask_SupervisedTagDiscoveryParent::runJob received null job");
			return;
		}

		System.out.println("Starting QueueTask_SupervisedTagDiscoveryParent::runJob jobId = " + job.id);

		Connection connection = QueueTask.openDatabaseConnection();

		// Do we have a 'parent' job, or a 'child'
		if (job.parentJob_id == null) {
			// parent job
			if (RunParentJob(job, connection)) {
				setJobStatusComplete(job.id, connection);
			} else {
				setJobStatusError(job.id, connection);  // error occured
			}
		} else {
			// parent task should not have parent task ID
			setJobStatusError(job.id, connection);  // error occured
		}

		closeDatabaseConnection(connection);
	}

	/** \brief Run a SupervisedTagDiscovery 'parent' job.
	*
	* Little actual processing is done for a 'parent' job. This pulls in the 
	* list of mediaFiles in the Project, and creates a 'child' job for each. 
	* We do this here in the Java since there may be tens of thousands of 
	* files, and it can be time consuming to add them all. As each is added 
	* to the database, other QueueRunner nodes can then pick them up.
	* @param parentJob the NesterJob object of the 'parent' job.
	* @param connection Opened database connection.
	* @return true if success, false if any error encountered. 
	*/

	static boolean RunParentJob(NesterJob parentJob, Connection connection) {
		Logger logger;
		logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	
		try {
			if (parentJob.project_id == 0) // 0 returned if null in database
				return false;

			Integer projectId = parentJob.project_id;

			Vector<Integer> mediaFileIds = NesterProject.getProjectMediaFileIds(projectId, connection);
			logger.log(Level.INFO, "Found " + mediaFileIds.size() + " files");
			
			Integer statusId_Unknown = NesterJob.GetJobStatusIdFromName("Unknown", connection);
			Integer statusId_Queued = NesterJob.GetJobStatusIdFromName("Queued", connection);
			Integer typeId_SupervisedTagDiscoveryChild = NesterJob.GetJobTypeIdFromName("SupervisedTagDiscoveryChild", connection);
			if (statusId_Unknown == 0) return false;
			if (statusId_Queued == 0) return false;
			if (typeId_SupervisedTagDiscoveryChild == null) return false;

			////
			// build up a basic NesterJob, we'll tweak settings for each addition on the fly
			NesterJob childJob = new NesterJob(parentJob);
			childJob.parentJob_id = parentJob.id;
			childJob.status_id = statusId_Unknown;
			childJob.type_id = typeId_SupervisedTagDiscoveryChild;
			childJob.priority = mediaFileIds.size();  // default priority to the number of files to search

			// Previously we added one ChildTask per media file to search. 
			// Due to the overhead of setting up each task, now search for multiple media files 
			// per child task.

HashMap<String, Boolean> fieldNames = new HashMap<String, Boolean>();
fieldNames.put("active", true);
fieldNames.put("durationInSeconds", true);
fieldNames.put("bitsPerSample", true);
fieldNames.put("nBlockAlign", true);
fieldNames.put("dataStartIndex", true);
fieldNames.put("sampleRate", true);
fieldNames.put("numChannels", true);
fieldNames.put("dataSize", true);

			final int NumFilesPerTask = 3;
			Vector<Integer> idsToAdd = new Vector<Integer>();

			// loop over each media file, add a new job and parameters
			for (Iterator<Integer> iterator = mediaFileIds.iterator(); iterator.hasNext();) {
				int fileId = iterator.next();

				NesterAudioFile file = NesterAudioFile.getAudioFile(fileId, connection, fieldNames);

				if ( file.active ) {
					idsToAdd.add( fileId );
				}
				
				if ((idsToAdd.size() >= NumFilesPerTask) || (! iterator.hasNext()) ) {
					String sIds = "";
					for (Integer id : idsToAdd) {
						sIds += id + ",";
					}

					if (idsToAdd.size() > 0) {

						// update per-job settings
						childJob.id = -1;
						childJob.creationDate = new java.sql.Timestamp(System.currentTimeMillis());
						childJob.name = "Supervised Tag Discovery Child - mediaFileIds: " + sIds;
	
						// add job parameters (mediafile ids)
						childJob.jobParameters = new HashMap<String, Vector<String>> ();
						if (! childJob.AddJobParameter("mediaFileIds", sIds)) {
							logger.log(Level.SEVERE, "Failed adding job parameters");
							return false;
						}
	
						// save 'child' job to DB
						childJob.SaveJob(connection);
	
						// set to Queued
						QueueTask.setJobStatusQueued(childJob.id, connection);

						idsToAdd.clear();
					}
				}
			}

			return true;

		} catch (Exception e) {
			logger.log(Level.SEVERE, "Exception = " + e);
			return false;
		}

	}

}

