package arlo;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Random;
import java.util.Vector;

import adapt.IO;
import adapt.SimpleTable;
import adapt.Utility;

// Search for ADAPTAnalysisDisabled
// NOTE: This is currently unused code
// There's a few TODO tags that need to be looked into before using
// - TonyB Feb 2012

// disabled with cassandra updates
public class ADAPTAnalysis extends Thread {
//
//	// v3
//	
//	// int targetNumTrainWindows = 999;
//	int targetNumTrainWindows = Integer.MAX_VALUE;
//	// long exampleSetPartitionRandomSeed = 123456;
//	// long exampleSetPartitionRandomSeed = 1234567;
//	long testWindowRandomSeed = 12345678;
//	long trainWindowRandomSeed = 23456789;
//	// double offset = 0.15;
//	// int numTestWindows = 1;
//
//	ServiceHead serviceHead;
//	int adaptAnalysisBiasID;
//	NesterProject project;
//
//	ADAPTAnalysis(ServiceHead serviceHead, int adaptAnalysisBiasID) {
//
//		this.serviceHead = serviceHead;
//		this.adaptAnalysisBiasID = adaptAnalysisBiasID;
//
//	}
//
//	public void run() {
//
//		boolean clusteringMode = true;
//		boolean supervisedLearningMode = false;
//
//		System.out.println("###############################");
//		System.out.println("### starting ADAPT Analysis ###");
//		System.out.println("###############################");
//
//		// get bias from DB using bias ID
//		NesterADAPTAnalysisBias nesterADAPTAnalysisBias = serviceHead.getNesterADAPTAnalysisBias(adaptAnalysisBiasID);
//
//		NesterUser nesterUser = serviceHead.getNesterUser(nesterADAPTAnalysisBias.user_id);
//
//		// get project from DB using project ID from bias
//		project = serviceHead.getNesterProject(nesterADAPTAnalysisBias.project_id);
//
//		System.out.println("### nesterADAPTAnalysisBias = " + nesterADAPTAnalysisBias);
//		System.out.println("### nesterUser.name = " + nesterUser.name);
//		System.out.println("### project.user_id = " + project.user_id);
//		System.out.println("### project.library_id = " + project.library_id);
//		System.out.println("### project.tagSet_id = " + project.tagSet_id);
//
//		boolean findClusters = true;
//
//		boolean usedCachedSpectraResults = false;
//
//		boolean de_mode = false;
//		boolean nc_mode = false;
//		boolean np_mode = false;
//
//		double randomWindowDuration = Double.NaN;
//
//		boolean writeSpectraForGPU = false;
//
//		boolean makeRandomWindowsFromTags = np_mode || de_mode;
//		boolean randomWindowSizeEqualsTagSize = np_mode;
//
//		String commandFilePath = "/home/dtcheng/NVIDIA_GPU_Computing_SDK/C/src/cardinal/control/commands.txt";
//		boolean useCPU = false;
//		boolean useGPU = true;
//
//		int numClasses = -1;
//		double[] correlationThresholds = null;
//		String[] classNames = null;
//		Hashtable<String, Integer> classNameToClassIndex = null;
//		Hashtable<Integer, Integer> classIDToClassIndex = null;
//		if (supervisedLearningMode) {
//
//			boolean importNPTags = false;
//			if (np_mode && importNPTags)
//// disabled with Cassandra updates
////				serviceHead.importNPExampleTagsFromFile(project, nesterADAPTAnalysisBias, "/home/dtcheng/workspace/nester/media/files/user-files/dtcheng/np/data.tab");
//
//			classNameToClassIndex = new Hashtable<String, Integer>();
//			classIDToClassIndex = new Hashtable<Integer, Integer>();
//
//			if (de_mode) {
//
//				{
//					classNames = new String[] { "a", "b", "c", "d", "e", "f", "g", "g2", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "r2", "t", "t2", "u", "v", "y", "z" };
//					numClasses = classNames.length;
//					for (int index = 0; index < numClasses; index++) {
//						classNameToClassIndex.put(classNames[index], index);
//					}
//				}
//
//				correlationThresholds = new double[numClasses];
//
//				correlationThresholds[classNameToClassIndex.get("a")] = 0.5;
//				correlationThresholds[classNameToClassIndex.get("b")] = 0.5;
//				correlationThresholds[classNameToClassIndex.get("c")] = 0.5;
//				correlationThresholds[classNameToClassIndex.get("d")] = 0.5;
//				correlationThresholds[classNameToClassIndex.get("e")] = 0.5;
//				correlationThresholds[classNameToClassIndex.get("f")] = 0.5;
//				correlationThresholds[classNameToClassIndex.get("g")] = 0.5;
//				correlationThresholds[classNameToClassIndex.get("g2")] = 0.5;
//				correlationThresholds[classNameToClassIndex.get("i")] = 0.5;
//				correlationThresholds[classNameToClassIndex.get("j")] = 0.5;
//				correlationThresholds[classNameToClassIndex.get("k")] = 0.5;
//				correlationThresholds[classNameToClassIndex.get("l")] = 0.5;
//				correlationThresholds[classNameToClassIndex.get("m")] = 0.5;
//				correlationThresholds[classNameToClassIndex.get("n")] = 0.5;
//				correlationThresholds[classNameToClassIndex.get("o")] = 0.5;
//				correlationThresholds[classNameToClassIndex.get("p")] = 0.5;
//				correlationThresholds[classNameToClassIndex.get("q")] = 0.5;
//				correlationThresholds[classNameToClassIndex.get("r")] = 0.5;
//				correlationThresholds[classNameToClassIndex.get("r2")] = 0.5;
//				correlationThresholds[classNameToClassIndex.get("t")] = 0.5;
//				correlationThresholds[classNameToClassIndex.get("t2")] = 0.5;
//				correlationThresholds[classNameToClassIndex.get("u")] = 0.5;
//				correlationThresholds[classNameToClassIndex.get("v")] = 0.5;
//				correlationThresholds[classNameToClassIndex.get("y")] = 0.5;
//				correlationThresholds[classNameToClassIndex.get("z")] = 0.5;
//
//			}
//			if (nc_mode) {
//				{
//					int index = 0;
//					classNameToClassIndex.put("acfl", index++);
//					classNameToClassIndex.put("baww", index++);
//					classNameToClassIndex.put("btbw", index++);
//					classNameToClassIndex.put("btgn", index++);
//					classNameToClassIndex.put("howa", index++);
//					classNameToClassIndex.put("oven", index++);
//					classNameToClassIndex.put("scta", index++);
//					classNameToClassIndex.put("wewa", index++);
//					classNameToClassIndex.put("ytwa", index++);
//					numClasses = classNameToClassIndex.size();
//				}
//
//				correlationThresholds = new double[numClasses];
//
//				classNames = new String[numClasses];
//
//				classNames[classNameToClassIndex.get("acfl")] = "acfl";
//				classNames[classNameToClassIndex.get("baww")] = "baww";
//				classNames[classNameToClassIndex.get("btbw")] = "btbw";
//				classNames[classNameToClassIndex.get("btgn")] = "btgn";
//				classNames[classNameToClassIndex.get("howa")] = "howa";
//				classNames[classNameToClassIndex.get("oven")] = "oven";
//				classNames[classNameToClassIndex.get("scta")] = "scta";
//				classNames[classNameToClassIndex.get("wewa")] = "wewa";
//				classNames[classNameToClassIndex.get("ytwa")] = "ytwa";
//
//				double offset = 0.20;
//
//				correlationThresholds[classNameToClassIndex.get("acfl")] = 0.8607 - offset;
//				correlationThresholds[classNameToClassIndex.get("baww")] = 0.7399 - offset;
//				correlationThresholds[classNameToClassIndex.get("btbw")] = 0.7785 - offset;
//				correlationThresholds[classNameToClassIndex.get("btgn")] = 0.7007 - offset;
//				correlationThresholds[classNameToClassIndex.get("howa")] = 0.7067 - offset;
//				correlationThresholds[classNameToClassIndex.get("oven")] = 0.6975 - offset;
//				correlationThresholds[classNameToClassIndex.get("scta")] = 0.5562 - offset;
//				correlationThresholds[classNameToClassIndex.get("wewa")] = 0.6706 - offset;
//				correlationThresholds[classNameToClassIndex.get("ytwa")] = 0.7391 - offset;
//
//			}
//			if (np_mode) {
//				{
//					int index = 0;
//					classNameToClassIndex.put("seizure", index++);
//					numClasses = classNameToClassIndex.size();
//				}
//
//				correlationThresholds = new double[numClasses];
//
//				classNames = new String[numClasses];
//
//				classNames[classNameToClassIndex.get("seizure")] = "seizure";
//
//				correlationThresholds[classNameToClassIndex.get("seizure")] = 0.10; // 0.27, 0.17
//
//			}
//
//			// get all class definitions for project tagSet //
//
//			Vector<NesterTagClass> allTagClasses = serviceHead.getNesterTagSetTagClasses(project.tagSet_id);
//
//			// create mapping between tag class ID and class index //
//
//			for (Iterator<NesterTagClass> iterator = allTagClasses.iterator(); iterator.hasNext();) {
//
//				NesterTagClass nesterTagClass = (NesterTagClass) iterator.next();
//
//				Integer classIndex = classNameToClassIndex.get(nesterTagClass.className);
//
//				if (classIndex != null) {
//					classIDToClassIndex.put(nesterTagClass.id, classIndex);
//				}
//
//			}
//
//			// report tag class information //
//
//			for (Iterator<NesterTagClass> iterator = allTagClasses.iterator(); iterator.hasNext();) {
//				NesterTagClass nesterTagClass = (NesterTagClass) iterator.next();
//				if (true) {
//					System.out.println(nesterTagClass.className);
//				}
//
//			}
//
//			// find the maximum tag class index //
//
//			int maxTagClassIndex = -1;
//			for (Iterator<NesterTagClass> iterator = allTagClasses.iterator(); iterator.hasNext();) {
//				NesterTagClass nesterTagClass = (NesterTagClass) iterator.next();
//				int tagClassIndex = nesterTagClass.id;
//				if (tagClassIndex > maxTagClassIndex) {
//					maxTagClassIndex = tagClassIndex;
//				}
//			}
//		}
//
//		// get all project library audio files //
//
//		Vector<NesterAudioFile> nesterAudioFiles = serviceHead.getNesterLibraryAudioFiles(project.library_id, false, null);
//
//		Vector<NesterRandomWindow> allWindows = null;
//		Vector<NesterTagExample> allTagExamples = null;
//
//		if (supervisedLearningMode) {
//
//			// get all project tag set tag examples //
//
//			allTagExamples = serviceHead.getNesterTagSetTagExamples(project.tagSet_id);
//
//			if (makeRandomWindowsFromTags) {
//
//				allWindows = new Vector<NesterRandomWindow>();
//
//				for (NesterTagExample nesterTagExample : allTagExamples) {
//
//					System.out.println("### nesterTagExample.audioFile_id = " + nesterTagExample.audioFile_id);
//
//					NesterRandomWindow nesterRandomWindow = null;
//
//					if (randomWindowSizeEqualsTagSize) {
//						nesterRandomWindow = new NesterRandomWindow(-1, -1, nesterTagExample.audioFile_id, null, nesterTagExample.startTime, nesterTagExample.endTime);
//					} else {
//
//						double tagDuration = nesterTagExample.endTime - nesterTagExample.startTime;
//						double padding = (randomWindowDuration - tagDuration) / 2.0;
//						double startTime = nesterTagExample.startTime - padding;
//						double endTime = nesterTagExample.endTime + padding;
//						nesterRandomWindow = new NesterRandomWindow(-1, -1, nesterTagExample.audioFile_id, null, startTime, endTime);
//					}
//
//					NesterAudioFile nesterAudioFile = nesterAudioFiles.get(nesterTagExample.audioFileIndex);
//
//					if (nesterRandomWindow.startTime >= 0.0 && nesterTagExample.endTime <= nesterAudioFile.wavFileMetaData.durationInSeconds)
//						allWindows.add(nesterRandomWindow);
//				}
//
//			} else {
//// TODO why does this have a constant.. temp test code? 
//				if (nc_mode)
//					allWindows = NesterRandomWindow.getNesterRandomWindows(14, serviceHead.connection); // !!!
//			}
//
//			System.out.println("### allTagExamples.size() = " + allTagExamples.size());
//		}
//
//		if (clusteringMode) {
//
//			allWindows = new Vector<NesterRandomWindow>();
//
//			for (int i = 0; i < nesterAudioFiles.size(); i++) {
//
//				NesterAudioFile nesterAudioFile = nesterAudioFiles.get(i);
//
//				double duration = nesterAudioFile.wavFileMetaData.durationInSeconds;
//
//				double startTime = 0;
//				double endTime = duration;
//
//				NesterRandomWindow nesterRandomWindow = null;
//
//				nesterRandomWindow = new NesterRandomWindow(-1, -1, nesterAudioFile.id, null, startTime, endTime);
//
//				allWindows.add(nesterRandomWindow);
//			}
//
//		}
//
//		System.out.println("### allWindows.size() = " + allWindows.size());
//
//		int numAudioFiles = nesterAudioFiles.size();
//
//		System.out.println("### numAudioFiles = " + numAudioFiles);
//
//		Hashtable<Integer, Integer> audioFileIDToAudioFileIndex = new Hashtable<Integer, Integer>();
//		for (int audioFileIndex = 0; audioFileIndex < numAudioFiles; audioFileIndex++) {
//
//			System.out.println("### nesterAudioFiles.get(audioFileIndex).id = " + nesterAudioFiles.get(audioFileIndex).id);
//
//			audioFileIDToAudioFileIndex.put(nesterAudioFiles.get(audioFileIndex).id, audioFileIndex);
//
//		}
//
//		/**************************************/
//		/* create spectra for for each window */
//		/**************************************/
//
//		int numWindows = allWindows.size();
//
//		double totalDuration = 0.0;
//
//		if (!usedCachedSpectraResults) {
//
//			CreateSpectraImage createSpectraImage = new CreateSpectraImage();
//
//			for (int windowIndex = 0; windowIndex < numWindows; windowIndex++) {
//
//				NesterRandomWindow nesterRandomWindow = allWindows.get(windowIndex);
//
//				System.out.println("### audioFile_id = " + nesterRandomWindow.audioFile_id);
//
//				int audioFileIndex = audioFileIDToAudioFileIndex.get(nesterRandomWindow.audioFile_id);
//
//				System.out.println("### windowIndex = " + windowIndex + " of " + numWindows);
//
//				NesterAudioFile nesterAudioFile = nesterAudioFiles.get(audioFileIndex);
//
//				double duration = nesterRandomWindow.endTime - nesterRandomWindow.startTime;
//
//				totalDuration += duration;
//
//				String[] parts = nesterAudioFile.absoluteFilePath.split("/");
//				String fileName = parts[parts.length - 1];
//				int fileNameLength = fileName.length();
//
//				String pathPart1 = nesterAudioFile.absoluteFilePath.substring(0, nesterAudioFile.absoluteFilePath.length() - fileNameLength);
//				String pathPart2 = "features/";
//				String pathPart3 = nesterAudioFile.alias + "." + nesterADAPTAnalysisBias.getSpectraComputationBiasString() + ".isd";
//
//				int[][] isdData = createSpectraImage.getNesterAudioFileSpectraData(nesterAudioFile, nesterRandomWindow.startTime, nesterRandomWindow.endTime,
//						nesterADAPTAnalysisBias);
//
//				String windowSpectraFilePath = pathPart1 + pathPart2 + pathPart3;
//
//				System.out.println("### windowSpectraFilePath = " + windowSpectraFilePath);
//
//				IO.writeObject(windowSpectraFilePath, isdData);
//
//			}
//
//			System.out.println("### totalDuration = " + totalDuration);
//
//		}
//
//		/*************************************/
//		/* load all spectra data into memory */
//		/*************************************/
//
//		int[][] allWindowSpectra1D = null;
//		int[] allWindowOffsets = null;
//
//		{
//			long startTime = System.currentTimeMillis();
//
//			String nesterTagDiscoveryBiasString = nesterADAPTAnalysisBias.getSpectraComputationBiasString();
//
//			System.out.println("nesterTagDiscoveryBiasString = " + nesterTagDiscoveryBiasString);
//
//			{
//				allWindowSpectra1D = new int[numWindows][];
//				allWindowOffsets = new int[numWindows];
//
//				// compute offsets //
//
//				int allSpectraDataNumSamples = 0;
//				for (int windowIndex = 0; windowIndex < numWindows; windowIndex++) {
//
//					NesterRandomWindow nesterWindow = allWindows.get(windowIndex);
//
//					System.out.println("### audioFile_id = " + nesterWindow.audioFile_id);
//
//					int audioFileIndex = audioFileIDToAudioFileIndex.get(nesterWindow.audioFile_id);
//
//					System.out.println("### windowIndex = " + windowIndex + " of " + numWindows);
//
//					NesterAudioFile nesterAudioFile = nesterAudioFiles.get(audioFileIndex);
//
//					String[] parts = nesterAudioFile.absoluteFilePath.split("/");
//					String fileName = parts[parts.length - 1];
//					int fileNameLength = fileName.length();
//
//					String pathPart1 = nesterAudioFile.absoluteFilePath.substring(0, nesterAudioFile.absoluteFilePath.length() - fileNameLength);
//					String pathPart2 = "features/";
//					String pathPart3 = windowIndex + ".isd";
//
//					String windowSpectraFilePath = pathPart1 + pathPart2 + pathPart3;
//
//					System.out.println("### windowSpectraFilePath = " + windowSpectraFilePath);
//
//					int[][] windowSpectra2D = (int[][]) IO.readObject(windowSpectraFilePath);
//
//					int numRows = windowSpectra2D.length;
//					int numCols = windowSpectra2D[0].length;
//
//					int numSamples = numRows * numCols;
//					allWindowSpectra1D[windowIndex] = new int[numSamples];
//					int dataIndex = 0;
//					for (int rowIndex = 0; rowIndex < numRows; rowIndex++) {
//						for (int colIndex = 0; colIndex < numCols; colIndex++) {
//							allWindowSpectra1D[windowIndex][dataIndex++] = windowSpectra2D[rowIndex][colIndex];
//						}
//					}
//
//					allWindowOffsets[windowIndex] = allSpectraDataNumSamples;
//
//					allSpectraDataNumSamples += numSamples;
//				}
//
//				System.out.println("allSpectraDataNumSamples = " + allSpectraDataNumSamples);
//
//				/************************************/
//				/* WRITE SPECTRA DATA FILE FOR GPU */
//				/************************************/
//				if (writeSpectraForGPU) {
//
//					String spectraFilePathString = "/home/dtcheng/NVIDIA_GPU_Computing_SDK/C/src/cardinal/data/data." + nesterTagDiscoveryBiasString + ".bin";
//					IO.delete(spectraFilePathString);
//					try {
//						RandomAccessFile file = new RandomAccessFile(spectraFilePathString, "rw");
//
//						System.out.println("allSpectraDataNumSamples = " + allSpectraDataNumSamples);
//
//						file.writeLong(Long.reverseBytes(allSpectraDataNumSamples));
//
//						int count = 0;
//						for (int windowIndex = 0; windowIndex < numWindows; windowIndex++) {
//							int dataSize = allWindowSpectra1D[windowIndex].length;
//							for (int i = 0; i < dataSize; i++) {
//
//								float value = allWindowSpectra1D[windowIndex][i];
//
//								if (count++ < 20)
//									System.out.println("value = " + value);
//
//								file.writeInt(Integer.reverseBytes(Float.floatToIntBits(value)));
//							}
//						}
//						System.out.println("count = " + count);
//						file.close();
//					} catch (FileNotFoundException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					} catch (IOException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//
//				}
//			}
//			long endTime = System.currentTimeMillis();
//
//			double duration = (endTime - startTime) / 1000.0;
//
//			System.out.println("### duration = " + duration);
//		}
//
//		if (useGPU) {
//
//			// Command GPU To Load Spectra Data //
//
//			String dataDirectoryPath = "/home/dtcheng/NVIDIA_GPU_Computing_SDK/C/src/cardinal/data/";
//
//			String dataFilePath = dataDirectoryPath + "data." + nesterADAPTAnalysisBias.getSpectraComputationBiasString() + ".bin";
//
//			IO.writeTextFileFromString(commandFilePath + ".part", "loadData " + dataFilePath);
//			IO.rename(commandFilePath + ".part", commandFilePath);
//
//		}
//
//		int maxNumTrials = nesterADAPTAnalysisBias.maxNumTrials;
//		int numToComplete = nesterADAPTAnalysisBias.numToComplete;
//
//		System.out.println("### maxNumTrials = " + maxNumTrials);
//		System.out.println("### numToComplete = " + numToComplete);
//
//		// closeConnection();
//
//		int allWindowsTagCount = 0;
//		for (NesterRandomWindow window : allWindows) {
//
//			window.classCounts = new int[numClasses];
//
//			double windowStartTime = window.startTime;
//			double windowEndTime = window.endTime;
//
//			// System.out.println("### windowStartTime = " + windowStartTime);
//			// System.out.println("### windowEndTime   = " + windowEndTime);
//
//			int windowTagCount = 0;
//			for (NesterTagExample tagExample : allTagExamples) {
//// TODO userTagged moved from TagExample to Tag
//// uncomment this and resolve
//System.out.println("ERROR: ADAPTAnalysis Disabled !!!!!!!!!");
////				if (tagExample.userTagged && tagExample.audioFile_id == window.audioFile_id && (tagExample.startTime >= windowStartTime && tagExample.startTime <= windowEndTime)
////						&& (tagExample.endTime >= windowStartTime && tagExample.endTime <= windowEndTime)) 
//				{
//
//					windowTagCount++;
//
//					// System.out.println("### tagExample.id   = " +
//					// tagExample.id);
//					// System.out.println("### tagExample.audioFile_id   = " +
//					// tagExample.audioFile_id);
//					// System.out.println("### tagExample.tagVector.size()   = "
//					// + tagExample.tagVector.size());
//					Integer classIndex = classIDToClassIndex.get(tagExample.tagVector.get(0).tagClass_id);
//					if (classIndex != null) {
//						window.classCounts[classIndex]++;
//					}
//				}
//
//			}
//			// System.out.println("### windowTagCount   = " + windowTagCount);
//
//			allWindowsTagCount += windowTagCount;
//
//		}
//
//		double averageNumTagsPerWindow = (double) allWindowsTagCount / numWindows;
//
//		System.out.println("### numWindows   = " + numWindows);
//		System.out.println("### averageNumTagsPerWindow   = " + averageNumTagsPerWindow);
//
//		int[][] groupdOccupancyCounts = null;
//		int[] groupOccupancyNumWindows = null;
//		Hashtable<Integer, Integer> audioFileIDToGroupIndex = null;
//
//		int numExampleGroups = -1;
//
//		if (nc_mode) {
//			numExampleGroups = 25;
//			int numMicrophones = 3;
//			groupdOccupancyCounts = new int[numExampleGroups][numClasses];
//			groupOccupancyNumWindows = new int[numExampleGroups];
//			audioFileIDToGroupIndex = new Hashtable<Integer, Integer>();
//
//			SimpleTable table = null;
//			try {
//				table = IO.readDelimitedTable("assigments.xls");
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//
//			int numRows = table.getNumRows();
//
//			for (int rowIndex = 0; rowIndex < numRows; rowIndex++) {
//
//				int microphoneIndex = table.getInt(rowIndex, 0) - 1;
//				int groupIndex = table.getInt(rowIndex, 1) - 1;
//				String relativeFilePath = table.getString(rowIndex, 2);
//
//				for (NesterAudioFile nesterAudioFile : nesterAudioFiles) {
//
//					if (nesterAudioFile.relativeFilePath.equals(relativeFilePath)) {
//						audioFileIDToGroupIndex.put(nesterAudioFile.id, groupIndex);
//					}
//				}
//
//				// System.out.format("%d %d %s\n", microphoneIndex, trialIndex,
//				// relativeFilePath);
//
//			}
//		}
//
//		/**************************************/
//		/* OBSERVATION LEVEL CROSS VALIDATION */
//		/***************************************/
//
//		int numTrainWindows = -1;
//
//		int[] classPredictionCounts = new int[numClasses];
//		int[] classTruePositiveCounts = new int[numClasses];
//		int[] classTrueNegativeCounts = new int[numClasses];
//		int[] classFalsePositiveCounts = new int[numClasses];
//		int[] classFalseNegativeCounts = new int[numClasses];
//		int[] classErrorCounts = new int[numClasses];
//
//		Random testWindowRandom = new Random(testWindowRandomSeed);
//		Random trainWindowRandom = new Random(trainWindowRandomSeed);
//
//		// randomize order of windows //
//		int[] testWindowRandomIntArray = Utility.randomIntArray(testWindowRandom, numWindows);
//
//		long startTime = System.currentTimeMillis();
//
//		// leave one out cross validation
//		for (int observationLevelTrialIndex = 0; observationLevelTrialIndex < numWindows; observationLevelTrialIndex++) {
//
//			System.out.println("");
//			System.out.println("");
//			System.out.println("##################################################");
//			System.out.println("##################################################");
//			System.out.println("");
//			System.out.println("observationLevelTrialIndex   = " + observationLevelTrialIndex);
//
//			// randomly pick test windows //
//
//			NesterRandomWindow testWindow = null;
//
//			testWindow = allWindows.get(testWindowRandomIntArray[observationLevelTrialIndex]);
//
//			// select non overlapping training windows //
//
//			int[] trainWindowRandomIntArray = Utility.randomIntArray(trainWindowRandom, numWindows);
//
//			Vector<NesterRandomWindow> trainWindowVector = new Vector<NesterRandomWindow>();
//
//			for (int candidateTrainWindowIndex = 0; candidateTrainWindowIndex < numWindows; candidateTrainWindowIndex++) {
//
//				NesterRandomWindow candidateTrainWindow = allWindows.get(trainWindowRandomIntArray[candidateTrainWindowIndex]);
//
//				// do not include self
//				if (testWindow == candidateTrainWindow)
//					continue;
//
//				// if in the same file and with overlapping start and stop times
//				// reject
//				if ((candidateTrainWindow.audioFile_id == testWindow.audioFile_id)
//						&& ((candidateTrainWindow.startTime >= testWindow.startTime && candidateTrainWindow.startTime <= testWindow.endTime) || (candidateTrainWindow.endTime >= testWindow.startTime && candidateTrainWindow.endTime <= testWindow.endTime))) {
//					continue;
//				}
//
//				if (nc_mode) { // !!!
//
//					// if in the same group reject
//					int candidateTrainWindowGroupIndex = audioFileIDToGroupIndex.get(candidateTrainWindow.audioFile_id);
//					int testWindowGroupIndex = audioFileIDToGroupIndex.get(testWindow.audioFile_id);
//					if (candidateTrainWindowGroupIndex == testWindowGroupIndex) {
//						continue;
//					}
//				}
//
//				trainWindowVector.add(candidateTrainWindow);
//
//				if (trainWindowVector.size() == targetNumTrainWindows)
//					break;
//
//			}
//
//			numTrainWindows = trainWindowVector.size();
//			System.out.println();
//			System.out.println("targetNumTrainWindows = " + targetNumTrainWindows);
//			System.out.println("numTrainWindows       = " + numTrainWindows);
//
//			NesterRandomWindow[] trainWindows = new NesterRandomWindow[numTrainWindows];
//			for (int trainWindowIndex = 0; trainWindowIndex < numTrainWindows; trainWindowIndex++) {
//				trainWindows[trainWindowIndex] = trainWindowVector.get(trainWindowIndex);
//			}
//
//			// ---------------------------------- //
//			// find all train window tag examples //
//			// ---------------------------------- //
//
//			Vector<NesterTagExample> trainTagExamples = new Vector<NesterTagExample>();
//
//			for (NesterTagExample tagExample : allTagExamples) {
//// TODO userTagged moved from TagExample to Tag
//// uncomment this and resolve
//System.out.println("ERROR: ADAPTAnalysis Disabled !!!!!!!!!");
//
////				if (tagExample.userTagged) 
//				{
//
//					if (false) {
//						System.out.println("tagExample = " + tagExample);
//						System.out.println("tagExample.id = " + tagExample.id);
//						System.out.println("tagExample.audioFile_id = " + tagExample.audioFile_id);
//						System.out.println("tagExample.tagVector = " + tagExample.tagVector);
//						System.out.println("tagExample.firstTag = " + tagExample.tag);
//						System.out.println("tagExample.tagVector.size() = " + tagExample.tagVector.size());
//					}
//
//					int tagExampleClass_id = tagExample.tagVector.get(0).tagClass_id;
//					// System.out.println("tagExampleClass_id = " +
//					// tagExampleClass_id);
//
//					Integer tagExampleClassIndex = classIDToClassIndex.get(tagExampleClass_id);
//
//					if (tagExampleClassIndex != null) {
//
//						boolean tagInTrainWindow = false;
//
//						for (NesterRandomWindow window : trainWindowVector) {
//
//							double windowStartTime = window.startTime;
//							double windowEndTime = window.endTime;
//
//							if (tagExample.audioFile_id == window.audioFile_id && (tagExample.startTime >= windowStartTime && tagExample.startTime <= windowEndTime)
//									&& (tagExample.endTime >= windowStartTime && tagExample.endTime <= windowEndTime)) {
//
//								tagInTrainWindow = true;
//
//								break;
//
//							}
//
//						}
//
//						if (tagInTrainWindow) {
//							trainTagExamples.add(tagExample);
//						}
//
//					}
//				}
//
//			}
//
//			int numTrainTags = trainTagExamples.size();
//
//			System.out.println("numTrainTags   = " + numTrainTags);
//
//			// double[] trainTagSpectraValues = new double[0];
//			// double[] testWindowTagSpectraValues = new double[0];
//
//			// ------------------- //
//			// Predict Test Window //
//			// ------------------- //
//
//			int testWindowAudioFileIndex = audioFileIDToAudioFileIndex.get(testWindow.audioFile_id);
//
//			int testWindowTrialIndex = -1;
//
//			if (nc_mode)
//				testWindowTrialIndex = audioFileIDToGroupIndex.get(testWindow.audioFile_id);
//
//			int[] testWindowAudioFileSpectra = allWindowSpectra1D[testWindowAudioFileIndex];
//
//			int testWindowStartFrame = (int) (testWindow.startTime * nesterADAPTAnalysisBias.numTimeFramesPerSecond);
//			int testWindowEndFrame = (int) (testWindow.endTime * nesterADAPTAnalysisBias.numTimeFramesPerSecond);
//
//			// double testWindowAudioFileSpectraDuration =
//			// testWindowAudioFileSpectra.length /
//			// nesterADAPTAnalysisBias.numFrequencyBands
//			// / nesterADAPTAnalysisBias.numTimeFramesPerSecond;
//			// System.out.println("Debug! testWindowAudioFileSpectraDuration = "
//			// + testWindowAudioFileSpectraDuration);
//
//			if (testWindowEndFrame > testWindowAudioFileSpectra.length / nesterADAPTAnalysisBias.numFrequencyBands) {
//				testWindowEndFrame = testWindowAudioFileSpectra.length / nesterADAPTAnalysisBias.numFrequencyBands;
//			}
//
//			// int testWindowNumFrames = testWindowEndFrame -
//			// testWindowStartFrame + 1; //!!!
//			int testWindowNumFrames = testWindowEndFrame - testWindowStartFrame;
//			System.out.println("testWindowNumFrames = " + testWindowNumFrames);
//
//			// System.out.println("Debug! testWindowNumFrames = " +
//			// testWindowNumFrames);
//
//			// for each train tag //
//
//			int numTrainTagExamples = trainTagExamples.size();
//
//			int[] trainTagClassCountsCPU = new int[numClasses];
//			double[] trainTagClassPerformanceSumCPU = new double[numClasses];
//
//			int[] trainTagClassCountsGPU = new int[numClasses];
//			double[] trainTagClassPerformanceSumGPU = new double[numClasses];
//
//			// ----------------------------------- //
//			// Loop Over Each Training Tag Example //
//			// ----------------------------------- //
//
//			if (useCPU) {
//
//				for (int trainTagExampleIndex = 0; trainTagExampleIndex < numTrainTagExamples; trainTagExampleIndex++) {
//
//					NesterTagExample trainTagExample = trainTagExamples.get(trainTagExampleIndex);
//
//					if (false) {
//						System.out.println("trainTagExample = " + trainTagExample);
//					}
//
//					// -------------------------- //
//					// Measure Spectra Similarity //
//					// -------------------------- //
//
//					int trainTagExampleClass_id = trainTagExample.tagVector.get(0).tagClass_id;
//					Integer trainTagExampleClassIndex = classIDToClassIndex.get(trainTagExampleClass_id);
//
//					int trainTagAudioFileIndex = audioFileIDToAudioFileIndex.get(trainTagExample.audioFile_id);
//
//					int[] trainTagAudioFileSpectra = allWindowSpectra1D[trainTagAudioFileIndex];
//
//					int trainTagStartFrame = (int) (trainTagExample.startTime * nesterADAPTAnalysisBias.numTimeFramesPerSecond);
//					int trainTagEndFrame = (int) (trainTagExample.endTime * nesterADAPTAnalysisBias.numTimeFramesPerSecond);
//
//					if (trainTagEndFrame > trainTagAudioFileSpectra.length / nesterADAPTAnalysisBias.numFrequencyBands) {
//						trainTagEndFrame = trainTagAudioFileSpectra.length / nesterADAPTAnalysisBias.numFrequencyBands;
//					}
//
//					// int trainTagNumFrames = trainTagEndFrame -
//					// trainTagStartFrame + 1; //!!!
//					int trainTagNumFrames = trainTagEndFrame - trainTagStartFrame;
//
//					if (np_mode)
//						if (trainTagNumFrames != testWindowNumFrames) {
//
//							System.out.format("trainTagNumFrames : %d\n", trainTagNumFrames);
//							System.out.format("testWindowNumFrames : %d\n", testWindowNumFrames);
//						}
//
//					int numSpectraObservations = trainTagNumFrames * nesterADAPTAnalysisBias.numFrequencyBands;
//
//					int trainTagSpectraValueIndex = trainTagStartFrame * nesterADAPTAnalysisBias.numFrequencyBands;
//
//					int lastTestWindowTagStartFrame = testWindowStartFrame + (testWindowNumFrames - trainTagNumFrames) + 1;
//
//					double bestPerformance = Double.NEGATIVE_INFINITY;
//					for (int frameIndex = testWindowStartFrame; frameIndex < lastTestWindowTagStartFrame; frameIndex++) {
//
//						int testWindowTagSectraValueIndex = frameIndex * nesterADAPTAnalysisBias.numFrequencyBands;
//
//						double meandreCorrelation = Double.NaN;
//						try {
//							meandreCorrelation = Correlation.correlation(trainTagAudioFileSpectra, testWindowAudioFileSpectra, trainTagSpectraValueIndex,
//									testWindowTagSectraValueIndex, numSpectraObservations);
//							if (false)
//								System.out.format("ob%d\ttt%d\tfi%d\twc%f\n", observationLevelTrialIndex, trainTagExampleIndex, frameIndex, meandreCorrelation);
//
//						} catch (Exception e) {
//							System.out.format("Error! correlation failed:\n");
//							// TODO Auto-generated catch block
//							e.printStackTrace();
//							System.exit(1);
//						}
//
//						double performance = Double.NaN;
//
//						if (false)
//							performance = meandreCorrelation - correlationThresholds[trainTagExampleClassIndex];
//						else
//							performance = meandreCorrelation;
//
//						if (performance > bestPerformance) {
//							bestPerformance = performance;
//						}
//
//					}
//
//					if (false)
//						System.out.format("ob%d\ttt%d\tbp%f\n", observationLevelTrialIndex, trainTagExampleIndex, bestPerformance);
//
//					trainTagClassPerformanceSumCPU[trainTagExampleClassIndex] += bestPerformance;
//					trainTagClassCountsCPU[trainTagExampleClassIndex]++;
//
//				}
//			}
//
//			// ----------------------------------- //
//			// Loop Over Each Training Tag Example //
//			// ----------------------------------- //
//
//			if (useGPU) {
//
//				String queryFilePathString = "/home/dtcheng/NVIDIA_GPU_Computing_SDK/C/src/cardinal/data/query.bin";
//				IO.delete(queryFilePathString);
//				String resultFilePathString = "/home/dtcheng/NVIDIA_GPU_Computing_SDK/C/src/cardinal/data/result.bin";
//				IO.delete(resultFilePathString);
//
//				int[] trainTagExampleNumQueries = new int[numTrainTagExamples];
//				System.out.format("starting to write query file\n");
//				try {
//					RandomAccessFile queryFile = new RandomAccessFile(queryFilePathString, "rw");
//
//					// write dummy value to be fixed later;
//					queryFile.writeLong(Long.reverseBytes(numTrainTagExamples));
//
//					for (int trainTagExampleIndex = 0; trainTagExampleIndex < numTrainTagExamples; trainTagExampleIndex++) {
//
//						NesterTagExample trainTagExample = trainTagExamples.get(trainTagExampleIndex);
//
//						if (false) {
//							System.out.println("trainTagExample = " + trainTagExample);
//						}
//
//						// -------------------------- //
//						// Measure Spectra Similarity //
//						// -------------------------- //
//
//						int trainTagExampleClass_id = trainTagExample.tagVector.get(0).tagClass_id;
//						Integer trainTagExampleClassIndex = classIDToClassIndex.get(trainTagExampleClass_id);
//
//						int trainTagAudioFileIndex = audioFileIDToAudioFileIndex.get(trainTagExample.audioFile_id);
//
//						int[] trainTagAudioFileSpectra = allWindowSpectra1D[trainTagAudioFileIndex];
//
//						int trainTagStartFrame = (int) (trainTagExample.startTime * nesterADAPTAnalysisBias.numTimeFramesPerSecond);
//						int trainTagEndFrame = (int) (trainTagExample.endTime * nesterADAPTAnalysisBias.numTimeFramesPerSecond);
//
//						if (trainTagEndFrame > trainTagAudioFileSpectra.length / nesterADAPTAnalysisBias.numFrequencyBands) {
//							trainTagEndFrame = trainTagAudioFileSpectra.length / nesterADAPTAnalysisBias.numFrequencyBands;
//						}
//
//						int trainTagNumFrames = trainTagEndFrame - trainTagStartFrame + 1;
//
//						int numSpectraObservations = trainTagNumFrames * nesterADAPTAnalysisBias.numFrequencyBands;
//
//						int trainTagSpectraValueIndex = trainTagStartFrame * nesterADAPTAnalysisBias.numFrequencyBands;
//
//						int lastTestWindowTagStartFrame = testWindowStartFrame + (testWindowNumFrames - trainTagNumFrames) + 1;
//
//						double bestPerformance = Double.NEGATIVE_INFINITY;
//
//						// long numQueries = lastTestWindowTagStartFrame -
//						// testWindowStartFrame;
//
//						long sourceStartIndex = allWindowOffsets[trainTagAudioFileIndex] + trainTagSpectraValueIndex;
//						long sourceEndIndex = sourceStartIndex + numSpectraObservations;
//						// System.out.println("writing sourceStartIndex = " + sourceStartIndex);
//						// System.out.println("writing sourceEndIndex = " + sourceEndIndex);
//						queryFile.writeLong(Long.reverseBytes(sourceStartIndex));
//						queryFile.writeLong(Long.reverseBytes(sourceEndIndex));
//
//						long targetStartIndex = allWindowOffsets[testWindowAudioFileIndex] + testWindowStartFrame * nesterADAPTAnalysisBias.numFrequencyBands;
//						// long targetEndIndex = targetStartIndex +
//						// numSpectraObservations;
//						long numIncrements = testWindowNumFrames - trainTagNumFrames + 1;
//
//						if (numIncrements < 0) {
//							System.out.println("Error! (numIncrements < 0)");
//
//						}
//
//						// System.out.println("writing targetStartIndex = " + targetStartIndex);
//						// System.out.println("writing numIncrements = " + numIncrements);
//						// System.out.println("writing nesterADAPTAnalysisBias.numFrequencyBands = " + nesterADAPTAnalysisBias.numFrequencyBands);
//						queryFile.writeLong(Long.reverseBytes(targetStartIndex));
//						queryFile.writeLong(Long.reverseBytes(numIncrements));
//						queryFile.writeLong(Long.reverseBytes(nesterADAPTAnalysisBias.numFrequencyBands));
//
//						// totalNumQueries++;
//						//
//						// for (int frameIndex = testWindowStartFrame;
//						// frameIndex < lastTestWindowTagStartFrame;
//						// frameIndex++) {
//						//
//						// int testWindowTagSectraValueIndex = frameIndex *
//						// nesterADAPTAnalysisBias.numFrequencyBands;
//						//
//						// long targetStartIndex =
//						// allDataSpectraFileOffsets[testWindowAudioFileIndex] +
//						// testWindowTagSectraValueIndex;
//						// long targetEndIndex = targetStartIndex +
//						// numSpectraObservations;
//						//
//						// queryFile.writeLong(Long.reverseBytes(targetStartIndex));
//						// queryFile.writeLong(Long.reverseBytes(targetEndIndex));
//						//
//						//
//						// trainTagExampleNumQueries[trainTagExampleIndex]++;
//						//
//						// totalNumQueries++;
//						// }
//
//					}
//
//					// queryFile.seek(0L);
//					// queryFile.writeLong(Long.reverseBytes(totalNumQueries));
//
//					queryFile.close();
//				} catch (FileNotFoundException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				} catch (IOException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//
//				System.out.format("done writing query file.\n");
//				System.out.format("calling GPU to do work...\n");
//				/* COMMAND GPU TO COMPUTE CORRELATIONS */
//
//				{
//
//					// Command GPU To Load Spectra Data //
//
//					// System.out.format("sending GPU query...\n");
//
//					/* WAIT FOR RESPONSE */
//
//					// System.out.format("waiting for command file to dissappear...\n");
//					while (true) {
//
//						if (!IO.fileExists(commandFilePath)) {
//							break;
//						}
//
//						// System.out.format("sleeping...\n");
//
//						try {
//							Thread.sleep(1L);
//						} catch (InterruptedException e) {
//							// TODO Auto-generated catch block
//							e.printStackTrace();
//						}
//					}
//
//					IO.writeTextFileFromString(commandFilePath + ".part", "findBestMatches" + " " + queryFilePathString + " " + resultFilePathString);
//					IO.rename(commandFilePath + ".part", commandFilePath);
//
//				}
//
//				/* WAIT FOR RESPONSE */
//
//				// System.out.format("waiting for response...\n");
//				while (true) {
//
//					if (IO.fileExists(resultFilePathString)) {
//						break;
//					}
//
//					// System.out.format("sleeping...\n");
//
//					try {
//						Thread.sleep(1L);
//					} catch (InterruptedException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//				}
//
//				System.out.format("GPU finished work\n");
//
//				/* parse result file */
//
//				{
//
//					System.out.format("starting to read result file...\n");
//					try {
//						RandomAccessFile resultFile = new RandomAccessFile(resultFilePathString, "rw");
//
//						for (int trainTagExampleIndex = 0; trainTagExampleIndex < numTrainTagExamples; trainTagExampleIndex++) {
//
//							NesterTagExample trainTagExample = trainTagExamples.get(trainTagExampleIndex);
//
//							int trainTagExampleClass_id = trainTagExample.tagVector.get(0).tagClass_id;
//							Integer trainTagExampleClassIndex = classIDToClassIndex.get(trainTagExampleClass_id);
//
//							double bestPerformance = Float.intBitsToFloat(Integer.reverseBytes(resultFile.readInt()));
//
//							if (false)
//								System.out.format("ob%d\ttt%d\tbp%f\n", observationLevelTrialIndex, trainTagExampleIndex, bestPerformance);
//
//							trainTagClassPerformanceSumGPU[trainTagExampleClassIndex] += bestPerformance;
//							trainTagClassCountsGPU[trainTagExampleClassIndex]++;
//
//						}
//
//						resultFile.close();
//					} catch (FileNotFoundException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					} catch (IOException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//
//					System.out.format("done reading result file.\n");
//				}
//
//			}
//
//			if (useCPU && useGPU)
//				for (int i = 0; i < numClasses; i++) {
//					if (Math.abs(trainTagClassPerformanceSumGPU[i] - trainTagClassPerformanceSumCPU[i]) > 0.0001) {
//						System.out.format("Error!:  Math.abs(trainTagClassPerformanceSumGPU[%d]:%f - trainTagClassPerformanceSumCPU[%d]:%f) > 0.0001)\n", i,
//								trainTagClassPerformanceSumGPU[i], i, trainTagClassPerformanceSumCPU[i]);
//						// System.exit(1);
//					}
//
//				}
//
//			int[] trainTagClassCounts;
//			double[] trainTagClassPerformanceSum;
//			if (useCPU) {
//				trainTagClassCounts = trainTagClassCountsCPU;
//				trainTagClassPerformanceSum = trainTagClassPerformanceSumCPU;
//			} else {
//				trainTagClassCounts = trainTagClassCountsGPU;
//				trainTagClassPerformanceSum = trainTagClassPerformanceSumGPU;
//			}
//
//			int[] predictedClassCounts = new int[numClasses];
//			int[] actualClassCounts = new int[numClasses];
//
//			for (int i = 0; i < numClasses; i++) {
//				predictedClassCounts[i] = trainTagClassCounts[i];
//				actualClassCounts[i] = testWindow.classCounts[i];
//			}
//
//			boolean[] classPredicted = new boolean[numClasses];
//			double[] averageBestPerformanceValues = new double[numClasses];
//
//			for (int classIndex = 0; classIndex < numClasses; classIndex++) {
//				averageBestPerformanceValues[classIndex] = Double.NaN;
//			}
//
//			// determine predicted classifications //
//
//			for (int classIndex = 0; classIndex < numClasses; classIndex++) {
//
//				if (false) {
//
//					if (predictedClassCounts[classIndex] > 0)
//						classPredicted[classIndex] = true;
//
//				} else {
//
//					if (trainTagClassCounts[classIndex] > 0) {
//
//						double averageBestPerformanceValue = trainTagClassPerformanceSum[classIndex] / trainTagClassCounts[classIndex];
//						averageBestPerformanceValues[classIndex] = averageBestPerformanceValue;
//
//						if (averageBestPerformanceValue >= correlationThresholds[classIndex])
//							classPredicted[classIndex] = true;
//					}
//
//				}
//			}
//
//			// determine actual classification //
//
//			boolean[] classActual = new boolean[numClasses];
//			for (int i = 0; i < numClasses; i++) {
//
//				if (actualClassCounts[i] > 0)
//					classActual[i] = true;
//			}
//
//			System.out.println("###  PREDICTED VS ACTUAL COUNTS  ###");
//			System.out.println("index\tclass\tcount\tavgBP\tthresh\tdelta\tactCnt\tpredict\tactual");
//			for (int classIndex = 0; classIndex < numClasses; classIndex++) {
//
//				double delta = averageBestPerformanceValues[classIndex] - correlationThresholds[classIndex];
//
//				System.out.format("%5d\t%7s\t%7d\t%7.3f\t%7.3f\t%7.3f\t%7d\t%7s\t%7s\n", classIndex, classNames[classIndex], predictedClassCounts[classIndex],
//						averageBestPerformanceValues[classIndex], correlationThresholds[classIndex], delta, actualClassCounts[classIndex], classPredicted[classIndex],
//						classActual[classIndex]);
//				//
//				// System.out.println(classIndex + "\t" +
//				// classNames[classIndex] + "\t" +
//				// predictedClassCounts[classIndex] + "\t" +
//				// averageBestPerformanceValues[classIndex] +
//				// "\t" + correlationThresholds[classIndex] + "\t"
//				// + actualClassCounts[classIndex] + "\t" +
//				// classPredicted[classIndex] + "\t" +
//				// classActual[classIndex]);
//
//			}
//
//			System.out.println("### CLASSIFICATION PERFORMANCE RATES ###");
//			System.out.println("index\tclass\tE Rate\tTP Rate\tTN Rate\tFP Rate\tFN Rate");
//
//			boolean perfectPrediction = true;
//
//			double errorRateSum = 0;
//			double truePositiveRateSum = 0;
//			double trueNegativeRateSum = 0;
//			double falsePositiveRateSum = 0;
//			double falseNegativeRateSum = 0;
//			for (int i = 0; i < numClasses; i++) {
//
//				classPredictionCounts[i]++;
//
//				if (classPredicted[i] == true && classActual[i] == true) {
//					classTruePositiveCounts[i]++;
//				}
//				if (classPredicted[i] == false && classActual[i] == false) {
//					classTrueNegativeCounts[i]++;
//				}
//
//				if (classPredicted[i] == true && classActual[i] == false) {
//					classFalsePositiveCounts[i]++;
//					classErrorCounts[i]++;
//					perfectPrediction = false;
//				}
//				if (classPredicted[i] == false && classActual[i] == true) {
//					classFalseNegativeCounts[i]++;
//					classErrorCounts[i]++;
//					perfectPrediction = false;
//				}
//
//				double errorRate = (double) classErrorCounts[i] / classPredictionCounts[i];
//				double truePositiveRate = (double) classTruePositiveCounts[i] / classPredictionCounts[i];
//				double trueNegativeRate = (double) classTrueNegativeCounts[i] / classPredictionCounts[i];
//				double falsePositiveRate = (double) classFalsePositiveCounts[i] / classPredictionCounts[i];
//				double falseNegativeRate = (double) classFalseNegativeCounts[i] / classPredictionCounts[i];
//
//				System.out.format("%5d\t%7s\t%7.3f\t%7.3f\t%7.3f\t%7.3f\t%7.3f\n", i, classNames[i], errorRate, truePositiveRate, trueNegativeRate, falsePositiveRate,
//						falseNegativeRate);
//
//				errorRateSum += errorRate;
//				truePositiveRateSum += truePositiveRate;
//				trueNegativeRateSum += trueNegativeRate;
//				falsePositiveRateSum += falsePositiveRate;
//				falseNegativeRateSum += falseNegativeRate;
//			}
//
//			double overallErrorRate = errorRateSum / numClasses;
//			double overallTruePositiveRate = truePositiveRateSum / numClasses;
//			double overallTrueNegativeRate = trueNegativeRateSum / numClasses;
//			double overallFalsePositiveRate = falsePositiveRateSum / numClasses;
//			double overallFalseNegativeRate = falseNegativeRateSum / numClasses;
//
//			System.out.println("### OVERALL CLASSIFICATION PERFORMANCE RATE ###");
//			System.out.println("E Rate\tTP Rate\tTN Rate\tFP Rate\tFN Rate");
//
//			System.out.format("%7.3f\t%7.3f\t%7.3f\t%7.3f\t%7.3f\n", overallErrorRate, overallTruePositiveRate, overallTrueNegativeRate, overallFalsePositiveRate,
//					overallFalseNegativeRate);
//
//			if (nc_mode) {
//				for (int i = 0; i < numClasses; i++) {
//					if (classPredicted[i])
//						groupdOccupancyCounts[testWindowTrialIndex][i]++;
//				}
//				groupOccupancyNumWindows[testWindowTrialIndex]++;
//
//				System.out.println("### TRIAL OCCUPANCY RATES ###");
//				System.out.format("trial\tcount");
//				for (int classIndex = 0; classIndex < numClasses; classIndex++) {
//					System.out.format("\t%s", classNames[classIndex]);
//				}
//				System.out.format("\n");
//
//				for (int trialIndex = 0; trialIndex < numExampleGroups; trialIndex++) {
//
//					System.out.format("%d\t%d", trialIndex + 1, groupOccupancyNumWindows[trialIndex]);
//
//					for (int classIndex = 0; classIndex < numClasses; classIndex++) {
//
//						double rate = Double.NaN;
//						if (groupOccupancyNumWindows[trialIndex] > 0)
//							rate = (double) groupdOccupancyCounts[trialIndex][classIndex] / groupOccupancyNumWindows[trialIndex];
//
//						System.out.format("\t%7.3f", rate);
//					}
//					System.out.format("\n");
//				}
//			}
//
//			long endTime = System.currentTimeMillis();
//
//			double duration = (endTime - startTime) / 1000.0;
//
//			System.out.println("### duration = " + duration);
//			System.out.println("### average trial duration = " + duration / (observationLevelTrialIndex + 1));
//
//		}
//
//		System.out.println("### finished ADAPT Analysis ###");
//
//	}
//
//	private void loadAudioFileData(NesterADAPTAnalysisBias nesterADAPTAnalysisBias, Vector<NesterAudioFile> nesterAudioFiles, int[][] allDataSpectra1D, int[][] allDataPitch,
//			int[][] allDataPitchEnergy, int[][] allDataAverageEnergy, int audioFileIndex) {
//
//
//		NesterAudioFile nesterAudioFile = nesterAudioFiles.get(audioFileIndex);
//
//		String[] parts = nesterAudioFile.absoluteFilePath.split("/");
//		String fileName = parts[parts.length - 1];
//		int fileNameLength = fileName.length();
//
//		String pathPart1 = nesterAudioFile.absoluteFilePath.substring(0, nesterAudioFile.absoluteFilePath.length() - fileNameLength);
//		String pathPart2 = "features/";
//		String pathPart3 = fileName + "." + nesterADAPTAnalysisBias.getSpectraComputationBiasString() + ".isd";
//
//		String isdFileAbsoluteDirectory = pathPart1 + pathPart2;
//
//		File isdDirectory = new File(isdFileAbsoluteDirectory);
//		if (!isdDirectory.isDirectory()) {
//			isdDirectory.mkdir();
//		}
//
//		String isdFileAbsolutePath = pathPart1 + pathPart2 + pathPart3;
//
//		// System.out.println("### reading spectra for isdFilePath = " +
//		// isdFileAbsolutePath);
//
//		// int[][] data =
//		// createSpectraImage.getNesterAudioFileSpectraData(nesterAudioFile,
//		// nesterTagDiscoveryBias);
//
//		String pitchPath = isdFileAbsolutePath + ".pitch";
//		String pitchEnergyPath = isdFileAbsolutePath + ".pitchEnergy";
//		String averageEnergyPath = isdFileAbsolutePath + ".averageEnergy";
//
//		int[][] data = null;
//
//		if (nesterADAPTAnalysisBias.spectraWeight > 0.0) {
//
//			data = (int[][]) IO.readObject(isdFileAbsolutePath);
//
//			int numRows = data.length;
//			int numCols = data[0].length;
//			int numValues = numRows * numCols;
//			double isdDurationInSeconds = numRows / nesterADAPTAnalysisBias.numTimeFramesPerSecond;
//
//			// System.out.println("nesterAudioFile.id = " + nesterAudioFile.id);
//			// System.out.println("nesterAudioFile.wavFileMetaData.durationInSeconds = "
//			// + nesterAudioFile.wavFileMetaData.durationInSeconds);
//			// System.out.println("isdDurationInSeconds = " +
//			// isdDurationInSeconds);
//
//			int[] data1D = new int[numValues];
//
//			int valueIndex = 0;
//			for (int i = 0; i < numRows; i++) {
//				for (int j = 0; j < numCols; j++) {
//					data1D[valueIndex++] = data[i][j];
//				}
//			}
//
//			allDataSpectra1D[audioFileIndex] = data1D;
//		}
//		// System.out.println("### reading profile data");
//
//		if (nesterADAPTAnalysisBias.pitchWeight > 0.0)
//			allDataPitch[audioFileIndex] = (int[]) IO.readObject(pitchPath);
//		if (nesterADAPTAnalysisBias.pitchEnergyWeight > 0.0)
//			allDataPitchEnergy[audioFileIndex] = (int[]) IO.readObject(pitchEnergyPath);
//		if (nesterADAPTAnalysisBias.averageEnergyWeight > 0.0)
//			allDataAverageEnergy[audioFileIndex] = (int[]) IO.readObject(averageEnergyPath);
//	}
//
//	void pauseIfNecessary() {
//		while (pauseThread) {
//			try {
//				Thread.sleep(1000);
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//	}
//
//	boolean pauseThread = false;
//	boolean stopThread = false;
//	boolean threadStopped = false;
//
//	public void pauseThread() {
//		pauseThread = true;
//	}
//
//	public void resumeThread() {
//		pauseThread = false;
//	}
//
//	public void stopThread() {
//		stopThread = true;
//		threadStopped = false;
//		while (!threadStopped) {
//			try {
//				Thread.sleep(1000);
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//	}
}
