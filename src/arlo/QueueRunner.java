package arlo;

import java.util.Vector;
import java.util.Iterator;

import arlo.NesterUser;
import arlo.QueueTask;


/** Main class for running through the Nester Database work queue and executing 
 *  jobs. This class doesn't actually execute any work, or even access the database. 
 *  This class is only a wrapper around all of the QueueTask handlers. It calls each 
 *  handler, having them check for and execute work. 
 */

public class QueueRunner extends Thread {

	int _checkIntervalSeconds;
	Vector<QueueTask> _taskHandlers = null;
	Vector<NesterUser> _restrictedUsers = null;

	/** Loop over a list of QueueTask handlers, having them check and execute work. 
	 *  This is a one-shot function. It calls all handlers in the provided list, 
	 *  having them perform the actual database check and work. 
	 *  \param taskHandlers A Vector of QueueTask subclasses, each checking a specific task.
	 *  \param restrictedUsers A Vector of users to check work from. Useful if the QueueRunner 
	 *  is running under a specific user's machine or account, and shouldn't execute 
	 *  other users' tasks. If this list is null, then all users' tasks will be run.
	 *  \param maxTasks If > 0, the maximum number of tasks to run before 
	                    returning from this function. 
	 *  \note The queue is processed first in the order of the handlers, then in the order of the 
	 *  users in the Vectors... keep this in mind if prioritizing tasks or users.
	 *  \return The number of tasks found and executed.
	 */

	static public int runQueue(
		Vector<QueueTask> taskHandlers,
		Vector<NesterUser> restrictedUsers,
		int maxTasks) 
	{
		int numTasksRan = 0;

		Iterator<QueueTask> i = taskHandlers.iterator();
		while (i.hasNext()) {
			QueueTask qt = i.next();
			if (qt.runQueue( restrictedUsers )) {
				// found and executed a job
				numTasksRan++;

				if (maxTasks > 0) {
					if (numTasksRan >= maxTasks) {
						return numTasksRan;
					}
				}

				// restart from the beginning of the task
				// list to look for higher priority tasks
				i = taskHandlers.iterator();
			}
		}

		return numTasksRan;
	}

	public boolean startDaemon(
		int checkIntervalSeconds,
		Vector<QueueTask> taskHandlers,
		Vector<NesterUser> restrictedUsers) 
	{
		_checkIntervalSeconds = checkIntervalSeconds;
		_taskHandlers = taskHandlers;
		_restrictedUsers = restrictedUsers;

		if (checkIntervalSeconds <= 0)
			return false;

		this.start();

		return true;
	}


	public void run() {
	
		while (true) {
			try {
				QueueRunner.runQueue(_taskHandlers, _restrictedUsers, -1);
				Thread.sleep(_checkIntervalSeconds * 1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}

