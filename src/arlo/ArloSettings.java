package arlo;

import java.util.Properties;
import java.io.InputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.HashMap;
import java.lang.StringBuilder;
import java.lang.NumberFormatException;

/** Configuration management for the ADAPT package.
 *
 * This class provides configuration parameters for the program. Parameters 
 * are sourced from multiple locations, searching for the parameter name in 
 * multiple locations, in order of precedence:
 *   - System Property, prefixed with "ADAPT."; specify on the commandline with -D, e.g., -DADAPT.dbUrl=localhost
 *   - Properties File (which is likewise specified with -DARLO_CONFIG_FILE=ArloSettings.properties
 *   - Default settings, specified in this class in HashMap defaultValues
 *
 */

public class ArloSettings {
	
	Properties config;

	public static Boolean startHttpCallMeAPIServer = null;
	public static Integer HttpCallMeAPIServerPort = null;
	public static String hostname = null;
	public static String processDescription = null;

	///
	// QueueRunner

	public static Integer RunQueueDaemonInterval_Seconds = null; // 0 = disabled
	public static Boolean QueueRunnerOneShot = null;  // if true, run through the queue once, then die
	public static String QueueRunnerAPIUserName = null;
	public static String QueueRunnerAPIPassword = null;
	public static String QueueRunnerAPIHost = null;
	public static Integer QueueRunnerAPIPort = null;
	public static Integer QueueRunnerOneShotMaxTasks = null;  // If > 0, kill this instance after number of completed Tasks

	// Queue Tasks
	public static Boolean RunQueueEnableImportAudioFile = null;
	public static Boolean RunQueueEnableTest = null;
	public static Boolean RunQueueEnableSupervisedTagDiscoveryParent = null;
	public static Boolean RunQueueEnableSupervisedTagDiscoveryChild = null;
	public static Boolean RunQueueEnableUnsupervisedTagDiscovery = null;
	public static Boolean RunQueueEnableExpertAgreementClassification = null;
	public static Boolean RunQueueEnableWekaJob = null;
	public static Boolean RunQueueEnableLibraryMediaFileValidation = null;

	public static long sizeOfInt = 4;
	public static long sizeOfLong = 8;

	// database
	public static String dbUserName = null;
	public static String dbPassword = null;
	public static String dbUrl = null;
	public static String dbDriveClassString = null;

	public static Integer numThreads = null;

	public static String bigDataCacheDirectoryPath = null;
	public static Boolean cacheEnabled = null;
	public static Boolean advancedTagging = null;
	public static String userFilesDirectoryPath = null;
	public static String mediaRootDirectoryPath = null;

	// API
	public static String NesterAPIRootURL = null;
	public static String NesterAPIUser = null;
	public static String NesterAPIToken = null;
	public static Boolean NesterAPINoVerfySSL = false;


	// Default Values
	private static HashMap<String, String> defaultValues;
	static {
		defaultValues = new HashMap<String, String>();
		defaultValues.put("startHttpCallMeAPIServer", "false");
		defaultValues.put("HttpCallMeAPIServerPort",  "0");
		defaultValues.put("processDescription", "");

		defaultValues.put("dbUserName", null);
		defaultValues.put("dbPassword", null);
		defaultValues.put("dbUrl", null);
		defaultValues.put("dbDriveClassString", "com.mysql.jdbc.Driver");
		
		defaultValues.put("numThreads", "1");
		
		defaultValues.put("bigDataCacheDirectoryPath", null);
		defaultValues.put("cacheEnabled", "false");
		defaultValues.put("advancedTagging", "false");
		defaultValues.put("userFilesDirectoryPath", null);
		defaultValues.put("mediaRootDirectoryPath", null);

		defaultValues.put("RunQueueDaemonInterval_Seconds", "0");  // 0 = disabled
		defaultValues.put("QueueRunnerOneShot", "false");
		defaultValues.put("QueueRunnerAPIUserName", null);
		defaultValues.put("QueueRunnerAPIPassword", null);
		defaultValues.put("QueueRunnerAPIHost", null);
		defaultValues.put("QueueRunnerAPIPort", null);
		defaultValues.put("QueueRunnerOneShotMaxTasks", "-1");

		defaultValues.put("RunQueueEnableImportAudioFile",               "false");
		defaultValues.put("RunQueueEnableTest",                          "false");
		defaultValues.put("RunQueueEnableSupervisedTagDiscoveryParent",  "false");
		defaultValues.put("RunQueueEnableSupervisedTagDiscoveryChild",   "false");
		defaultValues.put("RunQueueEnableUnsupervisedTagDiscovery",      "false");
		defaultValues.put("RunQueueEnableExpertAgreementClassification", "false");
		defaultValues.put("RunQueueEnableWekaJob", "false");
		defaultValues.put("RunQueueEnableLibraryMediaFileValidation", "false");

		defaultValues.put("NesterAPIRootURL", null);
		defaultValues.put("NesterAPIUser", null);
		defaultValues.put("NesterAPIToken", null);
		defaultValues.put("NesterAPINoVerfySSL", "false");
	}


	public boolean Initialize(String resourceName) {

		///
		// Get the logger
		Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		///
		// Get the local hostname
		{
			try {
				hostname = java.net.InetAddress.getLocalHost().getHostName();
			} catch (Exception e) {
				hostname = "ERROR_RETRIEVING_HOSTNAME";
			}
		}

		///
		// Read in all settings from resource file
		InputStream inputFile = this.getClass().getClassLoader().getResourceAsStream(resourceName);
		if (inputFile == null) {
			logger.log(Level.SEVERE, "Failed opening resource file for ArloSettings. Attempted to open resource name: " + resourceName);
			return false;
		}
		config = new Properties();
		try {
			config.load( inputFile );
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Failed Parsing resource file for ArloSettings. Attempted to open resource name: " + resourceName);
			return false;
		}

		/// 
		// Display Raw Settings
		logger.log(Level.CONFIG, "======== Raw Settings ========");
		for(String key : config.stringPropertyNames()) {
			String value = config.getProperty(key);
			logger.log(Level.CONFIG, key + " => " + value);
		}
		logger.log(Level.CONFIG, "==============================");


		///
		// Parse each setting

		startHttpCallMeAPIServer = getBoolean("startHttpCallMeAPIServer", logger);
		HttpCallMeAPIServerPort  = getInteger("HttpCallMeAPIServerPort",  logger);

		dbUserName = getString("dbUserName", logger);
		dbPassword = getString("dbPassword", logger);
		dbUrl = getString("dbUrl", logger);
		dbDriveClassString = getString("dbDriveClassString", logger);

		numThreads = getInteger("numThreads", logger);

		bigDataCacheDirectoryPath = getString("bigDataCacheDirectoryPath", logger);
		cacheEnabled = getBoolean("cacheEnabled", logger);
		advancedTagging = getBoolean("advancedTagging", logger);
		userFilesDirectoryPath = getString("userFilesDirectoryPath", logger);
		mediaRootDirectoryPath = getString("mediaRootDirectoryPath", logger);

		RunQueueDaemonInterval_Seconds = getInteger("RunQueueDaemonInterval_Seconds", logger);
		QueueRunnerOneShot = getBoolean("QueueRunnerOneShot", logger);
		QueueRunnerAPIUserName = getString("QueueRunnerAPIUserName", logger);
		QueueRunnerAPIPassword = getString("QueueRunnerAPIPassword", logger);
		QueueRunnerAPIHost = getString("QueueRunnerAPIHost", logger);
		QueueRunnerAPIPort = getInteger("QueueRunnerAPIPort", logger);
		QueueRunnerOneShotMaxTasks = getInteger("QueueRunnerOneShotMaxTasks", logger);

		RunQueueEnableImportAudioFile = getBoolean("RunQueueEnableImportAudioFile", logger);
		RunQueueEnableTest = getBoolean("RunQueueEnableTest", logger);
		RunQueueEnableSupervisedTagDiscoveryParent = getBoolean("RunQueueEnableSupervisedTagDiscoveryParent", logger);
		RunQueueEnableSupervisedTagDiscoveryChild = getBoolean("RunQueueEnableSupervisedTagDiscoveryChild", logger);
		RunQueueEnableUnsupervisedTagDiscovery = getBoolean("RunQueueEnableUnsupervisedTagDiscovery", logger);
		RunQueueEnableExpertAgreementClassification = getBoolean("RunQueueEnableExpertAgreementClassification", logger);
		RunQueueEnableWekaJob = getBoolean("RunQueueEnableWekaJob", logger);
		RunQueueEnableLibraryMediaFileValidation = getBoolean("RunQueueEnableLibraryMediaFileValidation", logger);

		NesterAPIRootURL = getString("NesterAPIRootURL", logger);
		NesterAPIUser = getString("NesterAPIUser", logger);
		NesterAPIToken = getString("NesterAPIToken", logger);
		NesterAPINoVerfySSL = getBoolean("NesterAPINoVerfySSL", logger);
		processDescription = getString("processDescription", logger);

		///
		// Ensure required parameters are provided
		StringBuilder sErr = new StringBuilder();
		boolean missing = false;
		sErr.append("Missing Configuration Parameters: ");

		if (startHttpCallMeAPIServer == null) { sErr.append("startHttpCallMeAPIServer, "); missing = true;}
		if (HttpCallMeAPIServerPort == null) { sErr.append("HttpCallMeAPIServerPort, "); missing = true;}
		if (dbUserName == null) { sErr.append("dbUserName, "); missing = true;}
		if (dbPassword == null) { sErr.append("dbPassword, "); missing = true;}
		if (dbUrl == null) { sErr.append("dbUrl, "); missing = true;}
		if (dbDriveClassString == null) { sErr.append("dbDriveClassString, "); missing = true;}
		if (numThreads == null) { sErr.append("numThreads, "); missing = true;}
		if (bigDataCacheDirectoryPath == null) { sErr.append("bigDataCacheDirectoryPath, "); missing = true;}
		if (cacheEnabled == null) { sErr.append("cacheEnabled, "); missing = true;}
		if (advancedTagging == null) { sErr.append("advancedTagging, "); missing = true;}
		if (userFilesDirectoryPath == null) { sErr.append("userFilesDirectoryPath, "); missing = true;}
		if (mediaRootDirectoryPath == null) { sErr.append("mediaRootDirectoryPath, "); missing = true;}
		if (RunQueueDaemonInterval_Seconds == null) { sErr.append("RunQueueDaemonInterval_Seconds, "); missing = true;}
		if (QueueRunnerOneShot == null) { sErr.append("QueueRunnerOneShot, "); missing = true;}
		if (QueueRunnerAPIUserName == null) {sErr.append("QueueRunnerAPIUserName, "); missing = true;}
		if (QueueRunnerAPIPassword == null) {sErr.append("QueueRunnerAPIPassword, "); missing = true;}
		if (QueueRunnerAPIHost == null) {sErr.append("QueueRunnerAPIHost, "); missing = true;}
		if (QueueRunnerAPIPort == null) {sErr.append("QueueRunnerAPIPort, "); missing = true;}
		if (QueueRunnerOneShotMaxTasks == null) {sErr.append("QueueRunnerOneShotMaxTasks, "); missing = true;}
		if (RunQueueEnableImportAudioFile == null) { sErr.append("RunQueueEnableImportAudioFile, "); missing = true;}
		if (RunQueueEnableTest == null) { sErr.append("RunQueueEnableTest, "); missing = true;}
		if (RunQueueEnableSupervisedTagDiscoveryParent == null) { sErr.append("RunQueueEnableSupervisedTagDiscoveryParent, "); missing = true;}
		if (RunQueueEnableSupervisedTagDiscoveryChild == null) { sErr.append("RunQueueEnableSupervisedTagDiscoveryChild, "); missing = true;}
		if (RunQueueEnableUnsupervisedTagDiscovery == null) { sErr.append("RunQueueEnableUnsupervisedTagDiscovery, "); missing = true;}
		if (RunQueueEnableExpertAgreementClassification == null) { sErr.append("RunQueueEnableExpertAgreementClassification, "); missing = true;}
		if (RunQueueEnableWekaJob == null) { sErr.append("RunQueueEnableWekaJob, "); missing = true;}
		if (RunQueueEnableLibraryMediaFileValidation == null) { sErr.append("RunQueueEnableLibraryMediaFileValidation, "); missing = true;}
		if (NesterAPIRootURL == null) { sErr.append("NesterAPIRootURL, "); missing = true;}
		if (NesterAPIUser == null) { sErr.append("NesterAPIUser, "); missing = true;}
		if (NesterAPIToken == null) { sErr.append("NesterAPIToken, "); missing = true;}

		if (missing == true) {
			logger.log(Level.SEVERE, sErr.toString());
			return false;
		}

		return true;
	}

	private Integer getInteger(String name, Logger logger) {
		Integer value = null;
		String sValue;
		
		sValue = System.getProperty("ADAPT." + name);
		if (sValue != null) {
logger.log(Level.SEVERE, "Found in system prop: " + name + " - " + sValue);
			try {
				value = Integer.valueOf(sValue);
			} catch (NumberFormatException e) {
				logger.log(Level.SEVERE, "Failed Parsing Value of '" + name + "' ('" + sValue + "')");
				return null;
			}
		}

		if (value == null) {
			sValue = config.getProperty(name);
			if (sValue != null) {
				try {
					value = Integer.valueOf(sValue);
				} catch (NumberFormatException e) {
					logger.log(Level.SEVERE, "Failed Parsing Value of '" + name + "' ('" + sValue + "')");
					return null;
				}
			}
		}

		// Fall back to default
		if (value == null) {
			logger.log(Level.WARNING, "Did not find parameter '" + name + "' in config file - using default");
			sValue = defaultValues.get(name);
			try {
				value = Integer.valueOf(sValue);
			} catch (NumberFormatException e) {
				logger.log(Level.SEVERE, "Failed Parsing Default Value of '" + name + "' ('" + sValue + "')");
				return null;
			}
		}

		logger.log(Level.CONFIG, "Found Value '" + Integer.toString(value) + "' for '" + name + "'");
		return value;
	}

	private Boolean getBoolean(String name, Logger logger) {
		Boolean value = null;
		String sValue;

		sValue = System.getProperty("ADAPT." + name);
		if (sValue != null) {
			value = Boolean.valueOf(sValue);
		}

		if (value == null) {
			sValue = config.getProperty(name);
			if (sValue != null) {
				value = Boolean.valueOf(sValue);
			}
		}

		// Fall back to default
		if (value == null) {
			logger.log(Level.WARNING, "Did not find parameter '" + name + "' in config file - using default");
			sValue = defaultValues.get(name);
			value = Boolean.valueOf(sValue);
		}

		logger.log(Level.CONFIG, "Found Value '" + Boolean.toString(value) + "' for '" + name + "'");
		return value;
	}

	private String getString(String name, Logger logger) {
		String value;
		
		value = System.getProperty("ADAPT." + name);

		if (value == null) {
			value = config.getProperty(name);
		}

		// Fall back to default
		if (value == null) {
			logger.log(Level.WARNING, "Did not find parameter '" + name + "' in config file - using default");
			value = defaultValues.get(name);
		}

		logger.log(Level.CONFIG, "Found Value '" + value + "' for '" + name + "'");
		return value;
	}

}
