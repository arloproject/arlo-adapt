package arlo;

public class NesterTagDiscoveryBias extends NesterJob {

	public enum TagDiscoveryType {
		UNSUPERVISED, SUPERVISED
	}
	

	public TagDiscoveryType tagDiscoveryType;


	public String name;
	public int numberOfTagsToDiscover;

	// REPRESENTATION BIAS
	public int numFrequencyBands;
	public double numTimeFramesPerSecond;
	public double dampingRatio;
	public double minFrequency;
	public double maxFrequency;
	public double spectraWeight;
	public double pitchWeight;
	public double pitchEnergyWeight;
	public double averageEnergyWeight;


	NesterTagDiscoveryBias(NesterUnsupervisedTagDiscoveryBias nesterUnsupervisedTagDiscoveryBias) {
		
		this.tagDiscoveryType = TagDiscoveryType.UNSUPERVISED;

		this.id = nesterUnsupervisedTagDiscoveryBias.id;
		this.user_id = nesterUnsupervisedTagDiscoveryBias.user_id;
		this.isRunning = nesterUnsupervisedTagDiscoveryBias.isRunning;
		this.isComplete = nesterUnsupervisedTagDiscoveryBias.isComplete;
		this.numCompleted = nesterUnsupervisedTagDiscoveryBias.numCompleted;
		this.fractionCompleted = nesterUnsupervisedTagDiscoveryBias.fractionCompleted;

		this.name = nesterUnsupervisedTagDiscoveryBias.name;

		this.numberOfTagsToDiscover = nesterUnsupervisedTagDiscoveryBias.numberOfClusters;

		this.numFrequencyBands = nesterUnsupervisedTagDiscoveryBias.numFrequencyBands;
		this.numTimeFramesPerSecond = nesterUnsupervisedTagDiscoveryBias.numTimeFramesPerSecond;
		this.dampingRatio = nesterUnsupervisedTagDiscoveryBias.dampingRatio;
		this.minFrequency = nesterUnsupervisedTagDiscoveryBias.minFrequency;
		this.maxFrequency = nesterUnsupervisedTagDiscoveryBias.maxFrequency;


	}

	NesterTagDiscoveryBias(NesterSupervisedTagDiscoveryBias nesterSupervisedTagDiscoveryBias) {
		
		this.tagDiscoveryType = TagDiscoveryType.SUPERVISED;

		this.id = nesterSupervisedTagDiscoveryBias.id;
		this.user_id = nesterSupervisedTagDiscoveryBias.user_id;
		this.isRunning = nesterSupervisedTagDiscoveryBias.isRunning;
		this.isComplete = nesterSupervisedTagDiscoveryBias.isComplete;
		this.fractionCompleted = nesterSupervisedTagDiscoveryBias.fractionCompleted;

		this.name = nesterSupervisedTagDiscoveryBias.name;

		this.numberOfTagsToDiscover = nesterSupervisedTagDiscoveryBias.numberOfTagsToDiscover;

		this.numFrequencyBands = nesterSupervisedTagDiscoveryBias.numFrequencyBands;
		this.numTimeFramesPerSecond = nesterSupervisedTagDiscoveryBias.numTimeFramesPerSecond;
		this.dampingRatio = nesterSupervisedTagDiscoveryBias.dampingRatio;
		this.minFrequency = nesterSupervisedTagDiscoveryBias.minFrequency;
		this.maxFrequency = nesterSupervisedTagDiscoveryBias.maxFrequency;


	}

	// NOTE: ADAPTAnalysis Disabled
	// Search for ADAPTAnalysisDisabled
	
	// NesterTagDiscoveryBias(NesterADAPTAnalysisBias nesterADAPTAnalysisBias) {
	// 	
	// 	this.tagDiscoveryType = TagDiscoveryType.SUPERVISED;

	// 	this.id = nesterADAPTAnalysisBias.id;
	// 	this.user_id = nesterADAPTAnalysisBias.user_id;
	// 	this.project_id = nesterADAPTAnalysisBias.project_id;
	// 	this.isRunning = nesterADAPTAnalysisBias.isRunning;
	// 	this.isComplete = nesterADAPTAnalysisBias.isComplete;
	// 	this.fractionCompleted = nesterADAPTAnalysisBias.fractionCompleted;

	// 	this.name = nesterADAPTAnalysisBias.name;

	// 	this.numFrequencyBands = nesterADAPTAnalysisBias.numFrequencyBands;
	// 	this.numTimeFramesPerSecond = nesterADAPTAnalysisBias.numTimeFramesPerSecond;
	// 	this.dampingRatio = nesterADAPTAnalysisBias.dampingRatio;
	// 	this.minFrequency = nesterADAPTAnalysisBias.minFrequency;
	// 	this.maxFrequency = nesterADAPTAnalysisBias.maxFrequency;


	// }

	public String getSpectraComputationBiasString() {
		String string = "numFrequencyBands=" + numFrequencyBands + "numTimeFramesPerSecond=" + numTimeFramesPerSecond + "dampingRatio=" + dampingRatio + "minFrequency=" + minFrequency
				+ "maxFrequency=" + maxFrequency;
		return string;

	}

}
