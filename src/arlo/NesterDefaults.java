package arlo;

public class NesterDefaults {

	double sindowSizeInSeconds;
	double spectraMinimumBandFrequency;
	double spectraMaximumBandFrequency;
	double spectraDampingFactor;
	int spectraNumFrequencyBands;
	double spectraNumFramesPerSecond;

	int spectraBorderWidth;
	int spectraTopBorderHeight;
	int timeScaleHeight;

	boolean showSpectra;
	boolean showCatalog;

	final static int CATALOG_TYPE_ALL_TAG_CLASSES = 0;
	final static int CATALOG_TYPE_TAG_CLASS_EXAMPLES = 1;
	int catalogTypeIndex;

	int catalogTagClassIndex;
	int catalogBorderHeight;
	int catalogBorderWidth;
	int catalogSpectraNumFrequencyBands;
	double catalogSpectraNumFramesPerSecond;

	// example definition
	boolean subwindowTagging;
	boolean ignoreFrequencyBoundaries;
	boolean simpleNormalization;
	boolean frequencyBandNormalization;

	boolean normalizeAudioVolume;


}
