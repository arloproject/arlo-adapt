package arlo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.Connection;

import com.kenai.jffi.Array;


import adapt.IO;
import adapt.Utility;
import arlo.NesterTag;
import arlo.NesterAudioFile;
import arlo.NesterTagClass;


/** \brief ExpertAgreement Classification
 *
 * @param args 
 * @param projectId The ProjectID in which this task is running. 
 * @param knownTags 'Expert' tagged Examples
 * @param audioFilesToClassify A list of audio Files to classify
 * @param connection An opened Database connection
 * @return true on a Successful run, false or an Exception otherwise
 */

public class ExpertAgreement {

	public static boolean run(
			String[] args, 
			int projectId,
			Vector<NesterTag> knownTags, 
			Vector<NesterAudioFile> audioFilesToClassify,
			double distanceWeightingPower,
			double classificationProbabilityThreshold,
			// Spectra Details
			int numFrequencyBands,
			double numTimeFramesPerSecond,
			double dampingRatio,
			double minFrequency,
			double maxFrequency,
			Connection connection
	) throws Exception {

        Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
		logger.log(Level.INFO, "===  Starting Expert Agreement Classification  ===");

// Things we need: 
// PARAMS
// - framesPerSecond
// - averaging?
// - 


		////////////////////////////////
		//                            //
		//          Settings          //
		//                            //
		////////////////////////////////


		int averagingWindowSizeInFrames = 1;
		
		/*
		 * instr d17 t0.3 0.7966133
		 * spoken d10.0 t0.9 0.81258154
		 * Sung d10.0 t0.4 0.7367247
		 */

		String randomWindowFilePath = "/data/environments/amanda/temp-data/random.windows.dat";
		String groundTruthTagFilePath = "/data/environments/amanda/temp-data/folklore.groundtruth.dat";
		String allSpectraFilePath = null;
//		double distanceWeightingPower = Double.NaN;  // Moved to Function Arguments

		boolean classifyAllFiles = false;

		if (args.length == 0) {
			args = new String[] { "learn", "1", "10.0", "0.4" };
// args = new String[] { "predict", "29", "1","2", "10.0", "0.4", "999999999" };
		}

		String mode = args[0];

		int numThreads = -1;
		int threadNumber = -1;
		int threadIndex = -1;
		int maxClassifyTimeInSeconds = -1;

		int numBands = -1;

//		double classificationProbabilityThreshold = Double.NaN;  // Moved to Function Arguments

		if (mode.equals("learn")) {
			numBands = Integer.parseInt(args[1]);
//			distanceWeightingPower = Double.parseDouble(args[2]);  // Moved to Function Arguments
//			classificationProbabilityThreshold = Double.parseDouble(args[3]);  // Moved to Function Arguments
			classifyAllFiles = false;
		}
		if (mode.equals("predict")) {
			numThreads = Integer.parseInt(args[1]);
			threadNumber = Integer.parseInt(args[2]);
			threadIndex = threadNumber - 1;
			numBands = Integer.parseInt(args[3]);
//			distanceWeightingPower = Double.parseDouble(args[4]);  // Moved to Function Arguments
//			classificationProbabilityThreshold = Double.parseDouble(args[5]);  // Moved to Function Arguments
			maxClassifyTimeInSeconds = Integer.parseInt(args[6]);
			classifyAllFiles = true;

		}

		// Some Logging
		logger.log(Level.INFO, "numThreads  = " + numThreads);
		logger.log(Level.INFO, "threadNumber = " + threadNumber);
		logger.log(Level.INFO, "numBands  = " + numBands);
		logger.log(Level.INFO, "distanceWeightingPower  = " + distanceWeightingPower);
		logger.log(Level.INFO, "classificationProbabilityThreshold  = " + classificationProbabilityThreshold);
		logger.log(Level.INFO, "classifyAllFiles  = " + classifyAllFiles);


		allSpectraFilePath = "examples." + numBands + "band.dat";

		int exampleWindowSizeInFrames = 1; // 1.0s at 32fps

		Random testExampleOrderRandom = new Random(333333333L);
// int numNeighbors = 127;

		////
		// Get a list of all of the ClassNames in the source Tags

		String[] classNames = null;
		HashMap<Integer, String> tagClassIdToName = new HashMap<Integer, String>();

		{
String sDEBUG = "Found these TagClasses: ";
			Vector<NesterTagClass> allTagSetClasses = NesterTagClass.getNesterProjectTagClasses(projectId, connection);

			// loop over all of the source Tags, and find the TagClasses included
			Vector<Integer> foundTagClassIds = new Vector<Integer>();
			for (NesterTag tag : knownTags) {
				if ( ! foundTagClassIds.contains(tag.tagClass_id)) {
					foundTagClassIds.add(tag.tagClass_id);
				}
			}

			// convert the included TagClasses into an array
			classNames = new String[foundTagClassIds.size()];
			int x = 0;
			for (NesterTagClass tagClass : allTagSetClasses) {
				if (foundTagClassIds.contains(tagClass.id)) {
sDEBUG = sDEBUG + tagClass.className + ", ";
					tagClassIdToName.put(tagClass.id, tagClass.className);
					classNames[x++] = tagClass.className;
				}
			}
logger.log(Level.INFO, sDEBUG);
		}
		
		int numClasses = classNames.length;


		// String[] classNames = new String[] { "Instrumental", "Spoken", "Sung" };
//		String[] classNames = new String[] { "instr", "Spoken", "Sung" };

		int subWindowSize = 1;
		int subWindowSizeMinusOne = subWindowSize - 1;

// int numExampleWindows = 195272;
		int numExampleWindows = -1;
// int maxNumExampleWindows = 257830; // exampleWindowSizeInFrames = 1
		int maxNumExampleWindows = 257830;


		////
		// File Column Offsets

		int exampleIndexOffset = 0;
		int exampleAudioFileIDOffset = 1;
		int exampleIDOffset = 2;
		int exampleTagIDOffset = 3;
		int exampleParentTagIDOffset = 4;
		int exampleClassIDOffset = 5;
		int exampleStartTimeOffset = 6;
		int exampleEndTimeOffset = 7;
		int exampleDurationOffset = 8;
		int dampingOffset = 9;
		int lowFreqOffset = 10;
		int highFreqOffset = 11;
		int numFrequencyBandsOffset = 12;
		int numSpectraPerSecondOffset = 13;
		int exampleNumFramesOffset = 14;
		int numValuesOffset = 15;

		int numRows = 2;
		String[] lines = IO.readStringLines(allSpectraFilePath, numRows);

// int numBands = Integer.parseInt(lines[1].split("\t")[numFrequencyBandsOffset]);
		double spectraFramesPerSecond = Double.parseDouble(lines[1].split("\t")[numSpectraPerSecondOffset]);

		System.out.println("numBands         = " + numBands);
		System.out.println("spectraPerSecond = " + spectraFramesPerSecond);

		boolean useCorelationDifference = false;
		boolean useAbsolutedDifference = true;

		boolean alwaysPredictFalse = false;
		boolean usedCompiledFeatures = false;
		boolean writeCompiledFeatures = false;
		int numExampleTags = 6929;
		int maxNumFrames = 128;

		String compiledExamplesFeaturesPath = allSpectraFilePath + ".bin";

		boolean reportTestExampleIndex = true;

		////
		// RandomWindow Variables

		int numRandomWindows = 3988;
		int[] randomWindowTagAudioFileIDs = new int[numRandomWindows];
		int[] randomWindowTagStartFrames = new int[numRandomWindows];
		int[] randomWindowTagEndFrames = new int[numRandomWindows];
		int[] randomWindowTagNumFrames = new int[numRandomWindows];

		int maxNumFeatures = numBands * maxNumFrames;

		System.out.println("maxNumFeatures = " + maxNumFeatures);
		System.out.println("maxNumFrames   = " + maxNumFrames);

		////
		// Tag Vairables

		int[] exampleTagAudioFileIDs = new int[numExampleTags];
		int[] exampleTagStartFrames = new int[numExampleTags];
		int[] exampleTagEndFrames = new int[numExampleTags];
		int[] exampleTagClassValues = new int[numExampleTags];


		int[] exampleWindowAudioFileIDs = new int[maxNumExampleWindows];
		int exampleWindowNumFeatures = numBands * exampleWindowSizeInFrames;
		int[][] exampleWindowFeatureValues = new int[maxNumExampleWindows][exampleWindowNumFeatures];
		int[] singleExampleWindowFeatureValues = new int[exampleWindowNumFeatures];
		boolean[][][] exampleWindowSpecificClassValues = new boolean[maxNumExampleWindows][exampleWindowSizeInFrames][numClasses];
		boolean[][] exampleWindowAbstractClassValues = new boolean[maxNumExampleWindows][numClasses];


		/********************************************/
		/********************************************/
		/***                                      ***/
		/*** READ GROUND TRUTH RANDOM WINDOW FILE ***/
		/***                                      ***/
		/********************************************/
		/********************************************/

		{

			BufferedReader bufferedReader = null;
			bufferedReader = new BufferedReader(new FileReader(new File(randomWindowFilePath)));

			// skip header line //
			String line = bufferedReader.readLine();

			for (int exampleIndex = 0; exampleIndex < numRandomWindows; exampleIndex++) {

				line = bufferedReader.readLine();

				String[] columnStrings = line.split("\t");
				int exampleTagStartFrame = (int) (Double.parseDouble(columnStrings[exampleStartTimeOffset]) * spectraFramesPerSecond);
				int exampleTagEndFrame = (int) (Double.parseDouble(columnStrings[exampleEndTimeOffset]) * spectraFramesPerSecond);

				int audioFileID = Integer.parseInt(columnStrings[exampleAudioFileIDOffset]);

				int numFrames = exampleTagEndFrame - exampleTagStartFrame + 1;

				randomWindowTagAudioFileIDs[exampleIndex] = audioFileID;
				randomWindowTagStartFrames[exampleIndex] = exampleTagStartFrame;
				randomWindowTagEndFrames[exampleIndex] = exampleTagEndFrame;
				randomWindowTagNumFrames[exampleIndex] = numFrames;

			}

			bufferedReader.close();

		}

		/**********************************/
		/**********************************/
		/***                            ***/
		/*** READ GROUND TRUTH TAG FILE ***/
		/***                            ***/
		/**********************************/
		/**********************************/

		exampleTagClassValues = new int[numExampleTags];

// This now comes from reading in the Tags Directly

//		{
//			BufferedReader bufferedReader = null;
//			bufferedReader = new BufferedReader(new FileReader(new File(groundTruthTagFilePath)));
//
//			// skip header line //
//			String line = bufferedReader.readLine();
//
//			for (int exampleIndex = 0; exampleIndex < numExampleTags; exampleIndex++) {
//
//				line = bufferedReader.readLine();
//
//				String[] columnStrings = line.split("\t");
//
//				int exampleTagStartFrame = (int) (Double.parseDouble(columnStrings[exampleStartTimeOffset]) * spectraFramesPerSecond);
//				int exampleTagEndFrame = (int) (Double.parseDouble(columnStrings[exampleEndTimeOffset]) * spectraFramesPerSecond);
//
//				int audioFileID = Integer.parseInt(columnStrings[exampleAudioFileIDOffset]);
//				String className = columnStrings[exampleClassIDOffset];
//
//				int classIndex = -1;
//				for (int i = 0; i < numClasses; i++) {
//					if (className.startsWith(classNames[i])) {
//						classIndex = i;
//						break;
//					}
//				}
//
//				exampleTagAudioFileIDs[exampleIndex] = audioFileID;
//				exampleTagStartFrames[exampleIndex] = exampleTagStartFrame;
//				exampleTagEndFrames[exampleIndex] = exampleTagEndFrame;
//
//				exampleTagClassValues[exampleIndex] = classIndex;
//
//			}
//
//			bufferedReader.close();
//
//		}


		{
			int exampleIndex = 0;
			for (NesterTag tag : knownTags) {
				int exampleTagStartFrame = (int) (tag.startTime * spectraFramesPerSecond);
				int exampleTagEndFrame = (int) (tag.endTime * spectraFramesPerSecond);

				int tagClass_id = tag.tagClass_id;
				String tagClassName = tagClassIdToName.get(tagClass_id);

				int classIndex = -1;
				for (int i = 0; i < numClasses; i++) {
					if (tagClassName == classNames[i]) {
						classIndex = i;
						break;
					}
				}

				exampleTagAudioFileIDs[exampleIndex] = tag.audioFile_id;
				exampleTagStartFrames[exampleIndex] = exampleTagStartFrame;
				exampleTagEndFrames[exampleIndex] = exampleTagEndFrame;
				exampleTagClassValues[exampleIndex] = classIndex;

				exampleIndex++;
			}
		}

		/*****************************/
		/*****************************/
		/***                       ***/
		/*** READ ALL SPECTRA FILE ***/
		/***                       ***/
		/*****************************/
		/*****************************/

		String line = null;
		int allSpectraNumFrames = 0;
		int exampleWindowIndex = 0;
		{
			BufferedReader bufferedReader = null;

			{
				bufferedReader = new BufferedReader(new FileReader(new File(allSpectraFilePath)));

				// skip header line //
				line = bufferedReader.readLine();

				String[] columnStrings = null;
				int[] abstractClassCounts = new int[numClasses];
				while (true) {

					line = bufferedReader.readLine();
					if (line == null)
						break;

					columnStrings = line.split("\t");

					int audioFileNumFrames = Integer.parseInt(columnStrings[exampleNumFramesOffset]);

					int audioFileID = Integer.parseInt(columnStrings[exampleAudioFileIDOffset]);

					System.out.println("audioFileID = " + audioFileID);

					int[][] spectraData = new int[audioFileNumFrames][numBands];
					for (int frameIndex = 0; frameIndex < audioFileNumFrames; frameIndex++) {
						line = bufferedReader.readLine();
						columnStrings = line.split("\t");
						for (int bandIndex = 0; bandIndex < numBands; bandIndex++) {
							spectraData[frameIndex][bandIndex] = Integer.parseInt(columnStrings[bandIndex]);
						}

						allSpectraNumFrames++;
					}

					boolean[] audioFileFrameInRandomWindow = new boolean[audioFileNumFrames];
					boolean[][] audioFileFrameClassValues = new boolean[audioFileNumFrames][numClasses];

					// mark random window frames
					for (int randomWindowIndex = 0; randomWindowIndex < numRandomWindows; randomWindowIndex++) {
						if (randomWindowTagAudioFileIDs[randomWindowIndex] == audioFileID) {
							for (int frameIndex = randomWindowTagStartFrames[randomWindowIndex]; frameIndex <= randomWindowTagEndFrames[randomWindowIndex]; frameIndex++) {
								audioFileFrameInRandomWindow[frameIndex] = true;
							}
						}
					}

					// mark frame classes
					for (int tagIndex = 0; tagIndex < numExampleTags; tagIndex++) {
						if (exampleTagAudioFileIDs[tagIndex] == audioFileID) {
							for (int frameIndex = exampleTagStartFrames[tagIndex]; frameIndex <= exampleTagEndFrames[tagIndex]; frameIndex++) {
								audioFileFrameClassValues[frameIndex][exampleTagClassValues[tagIndex]] = true;
							}
						}
					}

					/*******************/
					/* create examples */
					/*******************/

					for (int audioFileFrameIndex = 0; audioFileFrameIndex < audioFileNumFrames - (exampleWindowSizeInFrames - 1); audioFileFrameIndex++) {

						boolean fullyMarkedWindow = true;

						for (int exampleFrameIndex = 0; exampleFrameIndex < exampleWindowSizeInFrames; exampleFrameIndex++) {

							if (!audioFileFrameInRandomWindow[audioFileFrameIndex + exampleFrameIndex]) {
								fullyMarkedWindow = false;
							}
						}

						if (fullyMarkedWindow) {

							exampleWindowAudioFileIDs[exampleWindowIndex] = audioFileID;
							int featureIndex = 0;
							for (int exampleFrameIndex = 0; exampleFrameIndex < exampleWindowSizeInFrames; exampleFrameIndex++) {

								for (int classIndex = 0; classIndex < numClasses; classIndex++) {
									exampleWindowSpecificClassValues[exampleWindowIndex][exampleFrameIndex][classIndex] = audioFileFrameClassValues[audioFileFrameIndex + exampleFrameIndex][classIndex];
								}

								for (int bandIndex = 0; bandIndex < numBands; bandIndex++) {
									exampleWindowFeatureValues[exampleWindowIndex][featureIndex++] = spectraData[audioFileFrameIndex + exampleFrameIndex][bandIndex];
								}
							}

							/*******************************************/
							/* determine abstract example window class */
							/*******************************************/

							double[] classPositiveCounts = new double[numClasses];
							double[] classNegativeCounts = new double[numClasses];
							for (int classIndex = 0; classIndex < numClasses; classIndex++) {
								classPositiveCounts[classIndex] = (Math.random() - 0.5) * 0.001;
								classNegativeCounts[classIndex] = (Math.random() - 0.5) * 0.001;
							}

							for (int exampleFrameIndex = 0; exampleFrameIndex < exampleWindowSizeInFrames; exampleFrameIndex++) {

								for (int classIndex = 0; classIndex < numClasses; classIndex++) {
									if (audioFileFrameClassValues[audioFileFrameIndex + exampleFrameIndex][classIndex]) {
										classPositiveCounts[classIndex]++;
									} else {
										classNegativeCounts[classIndex]++;
									}
								}

							}

							for (int classIndex = 0; classIndex < numClasses; classIndex++) {

								if (classPositiveCounts[classIndex] > classNegativeCounts[classIndex]) {
									exampleWindowAbstractClassValues[exampleWindowIndex][classIndex] = true;
									abstractClassCounts[classIndex]++;
								}

							}

							exampleWindowIndex++;
						}

					}
				}

				bufferedReader.close();
				for (int classIndex = 0; classIndex < numClasses; classIndex++) {
					System.out.println("abstractClassCounts[" + classIndex + "] = " + abstractClassCounts[classIndex]);

				}

			}
		}
		System.out.println("allSpectraNumFrames = " + allSpectraNumFrames);
		System.out.println("exampleWindowIndex = " + exampleWindowIndex);

		numExampleWindows = exampleWindowIndex;

		if (writeCompiledFeatures) {
			Object[] objects = new Object[] { exampleWindowFeatureValues, exampleTagClassValues, exampleTagAudioFileIDs };
			IO.writeObject(compiledExamplesFeaturesPath, objects);
		}

		if (classifyAllFiles) {

			System.out.println("####    Classifying All Files    ####");

			line = null;
			allSpectraNumFrames = 0;
			exampleWindowIndex = 0;

			{
				BufferedReader bufferedReader = null;

				{
					bufferedReader = new BufferedReader(new FileReader(new File(allSpectraFilePath)));

					// skip header line //
					line = bufferedReader.readLine();

					String[] columnStrings = null;
					int fileIndex = 0;
					while (true) {

						line = bufferedReader.readLine();
						if (line == null)
							break;

						columnStrings = line.split("\t");

						int audioFileNumFrames = Integer.parseInt(columnStrings[exampleNumFramesOffset]);

						int audioFileID = Integer.parseInt(columnStrings[exampleAudioFileIDOffset]);

						System.out.println("classifying audioFileID = " + audioFileID);

						int[][] spectraData = new int[audioFileNumFrames][numBands];
						for (int frameIndex = 0; frameIndex < audioFileNumFrames; frameIndex++) {
							line = bufferedReader.readLine();
							columnStrings = line.split("\t");
							for (int bandIndex = 0; bandIndex < numBands; bandIndex++) {
								spectraData[frameIndex][bandIndex] = Integer.parseInt(columnStrings[bandIndex]);
							}

							allSpectraNumFrames++;
						}

						if (fileIndex % numThreads == threadIndex) {

							double[][] spectraClassProbabilities = new double[audioFileNumFrames][numClasses];

							/*******************/
							/* create examples */
							/*******************/

							for (int audioFileFrameIndex = 0; audioFileFrameIndex < audioFileNumFrames - (exampleWindowSizeInFrames - 1); audioFileFrameIndex++) {

								double frameTime = (double) audioFileFrameIndex / (double) spectraFramesPerSecond;

								if (frameTime > maxClassifyTimeInSeconds) {
									continue;
								}

								exampleWindowAudioFileIDs[exampleWindowIndex] = audioFileID;
								int featureIndex = 0;
								for (int exampleFrameIndex = 0; exampleFrameIndex < exampleWindowSizeInFrames; exampleFrameIndex++) {
									for (int bandIndex = 0; bandIndex < numBands; bandIndex++) {
										singleExampleWindowFeatureValues[featureIndex++] = spectraData[audioFileFrameIndex + exampleFrameIndex][bandIndex];
									}
								}

								// classify example //

								double[] classWeights = new double[numClasses];
								double weightSum = 0.0;

								for (int testSubwindowIndex = 0; testSubwindowIndex < exampleWindowSizeInFrames - subWindowSizeMinusOne; testSubwindowIndex++) {

									int testSubwindowStartIndex = testSubwindowIndex;

									for (int trainExampleIndex = 0; trainExampleIndex < numExampleWindows; trainExampleIndex++) {

										int[] trainFeatureValues = exampleWindowFeatureValues[trainExampleIndex];

										for (int trainSubwindowStartIndex = 0; trainSubwindowStartIndex < exampleWindowSizeInFrames - subWindowSizeMinusOne; trainSubwindowStartIndex++) {

											// compute difference //

											int testFeatureIndex = numBands * testSubwindowStartIndex;
											int trainFeatureIndex = numBands * trainSubwindowStartIndex;
											double distance = Double.NaN;

											if (useCorelationDifference) {

												double correlation = Utility.correlation(singleExampleWindowFeatureValues, trainFeatureValues, testFeatureIndex, trainFeatureIndex, numBands * subWindowSize);
												distance = 1.0 - correlation;
											}

											if (useAbsolutedDifference) {
												double distanceSum = 0;
												for (int bandIndex = 0; bandIndex < numBands * subWindowSize; bandIndex++) {
													distanceSum += Math.abs(singleExampleWindowFeatureValues[testFeatureIndex++] - trainFeatureValues[trainFeatureIndex++]);
												}
												distance = distanceSum / numBands;
											}

											double weight = 1.0 / Math.pow(distance, distanceWeightingPower);

											weightSum += weight;

											for (int classIndex = 0; classIndex < numClasses; classIndex++) {

												if (exampleWindowSpecificClassValues[trainExampleIndex][testSubwindowIndex][classIndex]) {
													// predict true
													classWeights[classIndex] += weight;
												}
											}

										}
									}

								}
								for (int classIndex = 0; classIndex < numClasses; classIndex++) {
									spectraClassProbabilities[audioFileFrameIndex][classIndex] = classWeights[classIndex] / weightSum;
								}

								if (audioFileFrameIndex > averagingWindowSizeInFrames) {

									double[] classCounts = new double[numClasses];
									for (int i = 0; i < averagingWindowSizeInFrames; i++) {
										for (int classIndex = 0; classIndex < numClasses; classIndex++) {
											classCounts[classIndex] += spectraClassProbabilities[audioFileFrameIndex - i][classIndex];
										}
									}
									double[] classProbabilities = new double[numClasses];
									for (int classIndex = 0; classIndex < numClasses; classIndex++) {
										classProbabilities[classIndex] = classCounts[classIndex] / averagingWindowSizeInFrames;
									}

									float time = (float) ((int) ((audioFileFrameIndex - averagingWindowSizeInFrames / 2) / spectraFramesPerSecond * 1000) / 1000.0);
									System.out.print("PREDICT\t" + audioFileID + "\t" + time + "\t" + audioFileFrameIndex);

									for (int classIndex = 0; classIndex < numClasses; classIndex++) {
										System.out.print("\t" + classProbabilities[classIndex]);
									}

									System.out.println();

								}

							}
						}

						fileIndex++;
					}

					bufferedReader.close();
				}

			}
			System.out.println("allSpectraNumFrames = " + allSpectraNumFrames);
			System.out.println("exampleWindowIndex = " + exampleWindowIndex);

			return true;

		}

		System.out.println("####    STARTING EXPERIMMENT    ####");

		System.out.println("numExamples = " + numExampleWindows);

		boolean[][] classPredictions = new boolean[numExampleWindows][numClasses];

		int[] randomTestExampleIndicies = Utility.randomIntArray(testExampleOrderRandom, numExampleWindows);

		for (int randomTestExampleIndex = 0; randomTestExampleIndex < numExampleWindows; randomTestExampleIndex++) {

			int testExampleIndex = randomTestExampleIndicies[randomTestExampleIndex];

			if (reportTestExampleIndex) {
				System.out.println("randomTestExampleIndex = " + randomTestExampleIndex);
				System.out.println("testExampleIndex       = " + testExampleIndex);
			}

			int[] testFeatureValues = exampleWindowFeatureValues[testExampleIndex];

			double[] classWeights = new double[numClasses];
			double weightSum = 0.0;

			for (int testSubwindowIndex = 0; testSubwindowIndex < exampleWindowSizeInFrames - subWindowSizeMinusOne; testSubwindowIndex++) {

				int testSubwindowStartIndex = testSubwindowIndex;

				for (int trainExampleIndex = 0; trainExampleIndex < numExampleWindows; trainExampleIndex++) {

					if (exampleWindowAudioFileIDs[trainExampleIndex] == exampleWindowAudioFileIDs[testExampleIndex]) {
						continue;
					}

					int[] trainFeatureValues = exampleWindowFeatureValues[trainExampleIndex];

					for (int trainSubwindowStartIndex = 0; trainSubwindowStartIndex < exampleWindowSizeInFrames - subWindowSizeMinusOne; trainSubwindowStartIndex++) {

						// compute difference //

						int testFeatureIndex = numBands * testSubwindowStartIndex;
						int trainFeatureIndex = numBands * trainSubwindowStartIndex;
						double distance = Double.NaN;

						if (useCorelationDifference) {

							double correlation = Utility.correlation(testFeatureValues, trainFeatureValues, testFeatureIndex, trainFeatureIndex, numBands * subWindowSize);
							distance = 1.0 - correlation;
						}

						if (useAbsolutedDifference) {
							double distanceSum = 0;
							for (int bandIndex = 0; bandIndex < numBands * subWindowSize; bandIndex++) {
								distanceSum += Math.abs(testFeatureValues[testFeatureIndex++] - trainFeatureValues[trainFeatureIndex++]);
							}
							distance = distanceSum / numBands;
						}

						double weight = 1.0 / Math.pow(distance, distanceWeightingPower);

						weightSum += weight;

						for (int classIndex = 0; classIndex < numClasses; classIndex++) {

							if (exampleWindowSpecificClassValues[trainExampleIndex][testSubwindowIndex][classIndex]) {
								// predict true
								classWeights[classIndex] += weight;
							}
						}

					}
				}

			}

			for (int classIndex = 0; classIndex < numClasses; classIndex++) {
				if (classWeights[classIndex] / weightSum > classificationProbabilityThreshold)
					classPredictions[testExampleIndex][classIndex] = true;
				else
					classPredictions[testExampleIndex][classIndex] = false;
			}

			int numExamplesPredicted = randomTestExampleIndex + 1;

			// compare class predictions to expert predictions //

			int[] numAgreementsByClass = new int[numClasses];
			int[] numComparisionsByClass = new int[numClasses];

// int numComparisionsCurrentClass = 0;
// int numAgreementsCurrentClass = 0;

			HashMap<String, Integer> predictedActualStringToCount = new HashMap<String, Integer>();

			for (int randomExampleIndex = 0; randomExampleIndex < numExamplesPredicted; randomExampleIndex++) {

				String predictionString = "";
				String actualString = "";

				for (int classIndex = 0; classIndex < numClasses; classIndex++) {

					int exampleIndex = randomTestExampleIndicies[randomExampleIndex];

					// System.out.println("classPredictions[" + exampleIndex + "][" + classIndex + "] = " + classPredictions[exampleIndex][classIndex]);

					if (classPredictions[exampleIndex][classIndex] == exampleWindowAbstractClassValues[exampleIndex][classIndex]) {
// if (classPredictions[exampleIndex][classIndex] == exampleWindowSpecificClassValues[exampleIndex][exampleWindowSizeInFrames/2][classIndex]) {
						numAgreementsByClass[classIndex]++;
					}

					numComparisionsByClass[classIndex]++;

					if (classPredictions[exampleIndex][classIndex]) {
						predictionString += "1";
					} else {
						predictionString += "0";
					}

					if (exampleWindowAbstractClassValues[exampleIndex][classIndex]) {
						actualString += "1";
					} else {
						actualString += "0";
					}

				}

// System.out.println("predictionString = " + predictionString);
// System.out.println("actualString     = " + actualString);

				String predictedActualString = predictionString + actualString;
				Integer count = predictedActualStringToCount.get(predictedActualString);
				if (count == null) {
					count = 0;
				}
				count = count + 1;
				predictedActualStringToCount.put(predictedActualString, count);
			}

			int numAgreementsAllClasses = 0;
			int numComparisionsAllClasses = 0;
			for (int classIndex = 0; classIndex < numClasses; classIndex++) {

				double accuracyRate = (double) numAgreementsByClass[classIndex] / (double) numComparisionsByClass[classIndex];
				System.out.println(classNames[classIndex] + "\t" + "accuracy = " + (float) accuracyRate);

				numAgreementsAllClasses += numAgreementsByClass[classIndex];
				numComparisionsAllClasses += numComparisionsByClass[classIndex];
			}

			double accuracyRateAllClasses = (double) numAgreementsAllClasses / (double) numComparisionsAllClasses;
			System.out.println("*********accuracyRateAllClasses = " + (float) accuracyRateAllClasses);

			SortedSet<String> keys = new TreeSet<String>(predictedActualStringToCount.keySet());

			int totalCount = 0;
			for (Iterator<String> iterator = keys.iterator(); iterator.hasNext();) {
				String key = (String) iterator.next();
				Integer count = predictedActualStringToCount.get(key);
				totalCount += count;

			}

			for (Iterator<String> iterator = keys.iterator(); iterator.hasNext();) {
				String key = (String) iterator.next();
				Integer count = predictedActualStringToCount.get(key);
				System.out.println(key + "\t" + count + "\t" + totalCount + "\t" + (float) count / totalCount * 100f);

			}

		}

		return true;
	}
}

/*
 * 
 * ### 128 bands ###
 * 
 * numBiasEvaluationsCompleted = 65.0
 * duration = 20862.18
 * rate = 0.003115686
 * averageErrorRateAllClasses = 0.38726196
 * bestErrorRateAllClasses = 0.3848611
 * NumTestSubWindows 291.0
 * NumTrainSubWindows 794.0
 * CenterFreqBandIndex 493.0
 * FrequencyWindowRadius 74.0
 * 
 * SCORE -0.0346030003760981 -0.019165608995756946 -0.030429467372957365
 * SCORE -0.03161325876306632 -0.0040361519643764285 -0.02968242738618765
 * 
 * 
 * 4096 examples sws=1
 * 
 * Instrumental errorRate = 0.18328764
 * Spoken errorRate = 0.22268726
 * Sung errorRate = 0.29831144
 * errorRateAllClasses = 0.23476212
 * 
 * 
 * 
 * 4096 examples sws=2
 * 
 * Instrumental errorRate = 0.17783773
 * Spoken errorRate = 0.22349036
 * Sung errorRate = 0.29611954
 * errorRateAllClasses = 0.23248254
 * 
 * 
 * 6800 examples sws=1
 * 
 * Instrumental errorRate = 0.18143375
 * Spoken errorRate = 0.21103504
 * Sung errorRate = 0.28554168
 * errorRateAllClasses = 0.2260035
 * randomTestExampleIndex = 4966
 * testExampleIndex = 5553
 * 
 * 6800 examples sws=2
 * 
 * Instrumental errorRate = 0.17527221
 * Spoken errorRate = 0.2170706
 * Sung errorRate = 0.29399368
 * errorRateAllClasses = 0.22877882
 * randomTestExampleIndex = 2847
 * testExampleIndex = 2434
 * 
 * 
 * 
 * #11
 * Instrumental errorRate = 0.1728763
 * Spoken errorRate = 0.21162444
 * Sung errorRate = 0.2876304
 * errorRateAllClasses = 0.22404371
 * randomTestExampleIndex = 671
 * testExampleIndex = 1099
 * 
 * #12
 * Instrumental errorRate = 0.15935673
 * Spoken errorRate = 0.20760234
 * Sung errorRate = 0.28070176
 * errorRateAllClasses = 0.21588694
 * randomTestExampleIndex = 684
 * testExampleIndex = 6274
 * #13
 * Instrumental errorRate = 0.1724138
 * Spoken errorRate = 0.28035983
 * Sung errorRate = 0.31784108
 * errorRateAllClasses = 0.25687155
 * randomTestExampleIndex = 667
 * testExampleIndex = 5154
 * #14
 * Instrumental errorRate = 0.23688394
 * Spoken errorRate = 0.3227345
 * Sung errorRate = 0.30683625
 * errorRateAllClasses = 0.28881824
 * randomTestExampleIndex = 629
 * testExampleIndex = 5911
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * accumulateSpecificGroundTruth = false
 * 
 * numBands=32 exampleWindowSize=1
 * randomTestExampleIndex = 31187
 * testExampleIndex = 115589
 * instr accuracy = 0.72085416
 * Spoken accuracy = 0.7290945
 * Sung accuracy = 0.6685584
 * ********accuracyRateAllClasses = 0.70616907
 * 
 * numBands=64 exampleWindowSize=1
 * randomTestExampleIndex = 31205
 * testExampleIndex = 160285
 * instr accuracy = 0.72370696
 * Spoken accuracy = 0.72969943
 * Sung accuracy = 0.67384475
 * ********accuracyRateAllClasses = 0.70908374
 * 
 * 
 * numBands=32 exampleWindowSize=3
 * randomTestExampleIndex = 22626
 * testExampleIndex = 103953
 * instr accuracy = 0.7188757
 * Spoken accuracy = 0.75869536
 * Sung accuracy = 0.6720732
 * ********accuracyRateAllClasses = 0.7165481
 * 
 * numBands=32 exampleWindowSize=5
 * randomTestExampleIndex = 12990
 * testExampleIndex = 34508
 * instr accuracy = 0.72165346
 * Spoken accuracy = 0.76953274
 * Sung accuracy = 0.6844739
 * ********accuracyRateAllClasses = 0.72522
 * 
 * numBands=32 exampleWindowSize=7
 * 
 * randomTestExampleIndex = 30186
 * testExampleIndex = 84415
 * instr accuracy = 0.73130816
 * Spoken accuracy = 0.77434
 * Sung accuracy = 0.6860238
 * ********accuracyRateAllClasses = 0.7305573
 * 
 * numBands=32 exampleWindowSize=9
 * randomTestExampleIndex = 25036
 * testExampleIndex = 162201
 * instr accuracy = 0.7326756
 * Spoken accuracy = 0.77653074
 * Sung accuracy = 0.68626434
 * ********accuracyRateAllClasses = 0.73182356
 * 
 * numBands=32 exampleWindowSize=11
 * randomTestExampleIndex = 24396
 * testExampleIndex = 169578
 * instr accuracy = 0.73525435
 * Spoken accuracy = 0.77874327
 * Sung accuracy = 0.69041276
 * ********accuracyRateAllClasses = 0.73480344
 * 
 * numBands=32 exampleWindowSize=13
 * randomTestExampleIndex = 16381
 * testExampleIndex = 170536
 * instr accuracy = 0.7309852
 * Spoken accuracy = 0.7778049
 * Sung accuracy = 0.696252
 * ********accuracyRateAllClasses = 0.735014
 * 
 * numBands=32 exampleWindowSize=15
 * randomTestExampleIndex = 21815
 * testExampleIndex = 151228
 * instr accuracy = 0.7344151
 * Spoken accuracy = 0.7792446
 * Sung accuracy = 0.6940319
 * ********accuracyRateAllClasses = 0.7358972
 * 
 * numBands=32 exampleWindowSize=17
 * randomTestExampleIndex = 18693
 * testExampleIndex = 182998
 * instr accuracy = 0.7339788
 * Spoken accuracy = 0.7808388
 * Sung accuracy = 0.6912378
 * ********accuracyRateAllClasses = 0.7353518
 * 
 * numBands=32 exampleWindowSize=19
 * randomTestExampleIndex = 9794
 * testExampleIndex = 180070
 * instr accuracy = 0.7349668
 * Spoken accuracy = 0.7798877
 * Sung accuracy = 0.7064829
 * ********accuracyRateAllClasses = 0.7404458
 * 
 * numBands=32 exampleWindowSize=21
 * randomTestExampleIndex = 20324
 * testExampleIndex = 157803
 * instr accuracy = 0.73441577
 * Spoken accuracy = 0.7785486
 * Sung accuracy = 0.70209104
 * ********accuracyRateAllClasses = 0.73835176
 * 
 * numBands=32 exampleWindowSize=23
 * 
 * 
 * randomTestExampleIndex = 23883
 * testExampleIndex = 49756
 * instr accuracy = 0.7317032
 * Spoken accuracy = 0.7799782
 * Sung accuracy = 0.6960727
 * ********accuracyRateAllClasses = 0.73591805
 * 
 * 
 * accumulateSpecificGroundTruth = true
 * 
 * all false baseline
 * randomTestExampleIndex = 44037
 * testExampleIndex = 42950
 * instr accuracy = 0.6367682
 * Spoken accuracy = 0.81163996
 * Sung accuracy = 0.6816613
 * ********accuracyRateAllClasses = 0.71002316
 * 
 * numBands=16 exampleWindowSize=1
 * 
 * randomTestExampleIndex = 6256
 * testExampleIndex = 94101
 * instr accuracy = 0.70001596
 * Spoken accuracy = 0.72718555
 * Sung accuracy = 0.643759
 * ********accuracyRateAllClasses = 0.6903202
 * 
 * 
 * numBands=32 exampleWindowSize=1
 * randomTestExampleIndex = 6256
 * testExampleIndex = 94101
 * instr accuracy = 0.74316764
 * Spoken accuracy = 0.7375739
 * Sung accuracy = 0.6701295
 * ********accuracyRateAllClasses = 0.71695703
 * 
 * 
 * numBands=64 exampleWindowSize=1
 * 
 * randomTestExampleIndex = 6256
 * testExampleIndex = 94101
 * instr accuracy = 0.75083905
 * Spoken accuracy = 0.7335784
 * Sung accuracy = 0.6734857
 * ********accuracyRateAllClasses = 0.71930104
 * 
 * numBands=64 exampleWindowSize=1
 * 
 * randomTestExampleIndex = 6256
 * testExampleIndex = 94101
 * instr accuracy = 0.7570721
 * Spoken accuracy = 0.7313409
 * Sung accuracy = 0.68227583
 * ********accuracyRateAllClasses = 0.72356296
 * 
 * 
 * numBands=128 exampleWindowSize=1
 * 
 * randomTestExampleIndex = 6256
 * testExampleIndex = 94101
 * instr accuracy = 0.75675243
 * Spoken accuracy = 0.73022217
 * Sung accuracy = 0.6824357
 * ********accuracyRateAllClasses = 0.7231368
 * 
 * numBands=256 exampleWindowSize=1
 * 
 * randomTestExampleIndex = 6256
 * testExampleIndex = 94101
 * instr accuracy = 0.7525971
 * Spoken accuracy = 0.72958285
 * Sung accuracy = 0.6833946
 * ********accuracyRateAllClasses = 0.7218582
 * randomTestExampleIndex = 35904
 * testExampleIndex = 229279
 * instr accuracy = 0.75939286
 * Spoken accuracy = 0.7364434
 * Sung accuracy = 0.6882607
 * ********accuracyRateAllClasses = 0.7280323
 * 
 * 
 * 
 * 
 * 
 * numBands=32 exampleWindowSize=9
 * 
 * randomTestExampleIndex = 4448
 * testExampleIndex = 156239
 * instr accuracy = 0.794111
 * Spoken accuracy = 0.7848955
 * Sung accuracy = 0.71544164
 * ********accuracyRateAllClasses = 0.76481605
 * 
 * 
 * 
 * 
 * numBands=32 exampleWindowSize=16
 * randomTestExampleIndex = 928
 * testExampleIndex = 28987
 * instr accuracy = 0.795479
 * Spoken accuracy = 0.7911733
 * Sung accuracy = 0.7513455
 * ********accuracyRateAllClasses = 0.77933264
 * 
 * numBands=32 exampleWindowSize=32
 * randomTestExampleIndex = 740
 * testExampleIndex = 129455
 * instr accuracy = 0.82321185
 * Spoken accuracy = 0.79217273
 * Sung accuracy = 0.7341431
 * ********accuracyRateAllClasses = 0.7831759
 * 
 * numBands=32 exampleWindowSize=64
 * randomTestExampleIndex = 202
 * testExampleIndex = 4434
 * instr accuracy = 0.79310346
 * Spoken accuracy = 0.8719212
 * Sung accuracy = 0.67487687
 * ********accuracyRateAllClasses = 0.7799672
 * 
 * 
 * 
 * baseline all negative predictions
 * 
 * randomTestExampleIndex = 1601
 * testExampleIndex = 63613
 * instr accuracy = 0.6260924
 * Spoken accuracy = 0.79962546
 * Sung accuracy = 0.6622971
 * ********accuracyRateAllClasses = 0.696005
 * 
 * 64 bands
 * randomTestExampleIndex = 3044
 * testExampleIndex = 38199
 * instr accuracy = 0.8407225
 * Spoken accuracy = 0.8049261
 * Sung accuracy = 0.7599343
 * ********accuracyRateAllClasses = 0.801861
 * 
 * 128 bands
 * randomTestExampleIndex = 1518
 * testExampleIndex = 117019
 * instr accuracy = 0.8222515
 * Spoken accuracy = 0.80908495
 * Sung accuracy = 0.7702436
 * ********accuracyRateAllClasses = 0.8005267
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 1 neighbor
 * 
 * randomTestExampleIndex = 4655
 * testExampleIndex = 164287
 * instr accuracy = 0.7600945
 * Spoken accuracy = 0.73088485
 * Sung accuracy = 0.6810567
 * ********accuracyRateAllClasses = 0.724012
 * 
 * 5 neighbors
 * randomTestExampleIndex = 6556
 * testExampleIndex = 251583
 * instr accuracy = 0.77611715
 * Spoken accuracy = 0.78191245
 * Sung accuracy = 0.7076407
 * ********accuracyRateAllClasses = 0.75522345
 * 
 * 17 neighbors
 * randomTestExampleIndex = 2020
 * testExampleIndex = 222127
 * instr accuracy = 0.7817912
 * Spoken accuracy = 0.7981197
 * Sung accuracy = 0.73676395
 * ********accuracyRateAllClasses = 0.77222496
 * 
 * 65 neighbors
 * randomTestExampleIndex = 495
 * testExampleIndex = 110261
 * instr accuracy = 0.7822581
 * Spoken accuracy = 0.8266129
 * Sung accuracy = 0.7520161
 * ********accuracyRateAllClasses = 0.7869624
 * 
 * 
 * distWeightPower = 15.0; 256 bands
 * randomTestExampleIndex = 12065
 * testExampleIndex = 91533
 * instr accuracy = 0.7716725
 * Spoken accuracy = 0.8054865
 * Sung accuracy = 0.73379743
 * ********accuracyRateAllClasses = 0.7703188
 * 
 * 
 * 
 * 128 bands windowSize=1
 * 
 * randomTestExampleIndex = 6723
 * testExampleIndex = 89079
 * instr accuracy = 0.7813801
 * Spoken accuracy = 0.7932778
 * Sung accuracy = 0.734533
 * ********accuracyRateAllClasses = 0.7697303
 * 
 * 128 bands windowSize=8
 * 
 * 128 bands windowSize=32
 * 
 * 32 bands windowSize=1
 * randomTestExampleIndex = 9732
 * testExampleIndex = 230863
 * instr accuracy = 0.76101923
 * Spoken accuracy = 0.7699579
 * Sung accuracy = 0.70060617
 * ********accuracyRateAllClasses = 0.7438611
 * 
 * 32 bands windowSize=9
 * 
 * 
 * 
 * 
 * 
 * 12.5
 * randomTestExampleIndex = 27250
 * testExampleIndex = 242975
 * instr accuracy = 0.77028364
 * Spoken accuracy = 0.80892444
 * Sung accuracy = 0.73564273
 * ********accuracyRateAllClasses = 0.77161694
 * 
 * 15.0
 * 
 * randomTestExampleIndex = 27096
 * testExampleIndex = 111701
 * instr accuracy = 0.7772447
 * Spoken accuracy = 0.8038528
 * Sung accuracy = 0.7355796
 * ********accuracyRateAllClasses = 0.7722257
 * 17.5
 * randomTestExampleIndex = 27370
 * testExampleIndex = 98136
 * instr accuracy = 0.78053415
 * Spoken accuracy = 0.7990939
 * Sung accuracy = 0.73197913
 * ********accuracyRateAllClasses = 0.7705357
 */
