package arlo;

import java.util.Vector;
import java.util.Iterator;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.File;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Time;
import java.sql.SQLException;


import arlo.NesterUser;
import arlo.NesterAudioFile;
import arlo.NesterJob;

import arlo.QueueTask;

public class QueueTask_UnsupervisedTagDiscovery extends QueueTask {

	static final private String taskName = "UnsupervisedTagDiscovery";

	public boolean runQueue(Vector<NesterUser> restrictedUsers) {
		return super._runQueue(taskName, restrictedUsers);
	}

	void runJob(NesterJob job) {

		if (job == null) {
			System.out.println("QueueTask_UnsupervisedTagDiscovery::runJob received null job");
			return;
		}

		System.out.println("Starting QueueTask_UnsupervisedTagDiscovery::runJob jobId = " + job.id);

		Connection connection = QueueTask.openDatabaseConnection();

		if (RunJob(job, connection)) {
			setJobStatusComplete(job.id, connection);
		} else {
			setJobStatusError(job.id, connection);  // error occured
		}

		closeDatabaseConnection(connection);
	}

	/** \brief Run an UnsupervisedTagDiscovery job.
	*
	* @param job the NesterJob object of the 'parent' job.
	* @param connection Opened database connection.
	* @return true if success, false if any error encountered. 
	*/

	static boolean RunJob(NesterJob job, Connection connection) {
		Logger logger;
		logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	
		try {

            UnsupervisedTagDiscovery unsupervisedTagDiscovery = new UnsupervisedTagDiscovery(job.id);

            if (unsupervisedTagDiscovery.QueueTaskRun(connection)) {
                logger.log(Level.INFO, "Success running QueueTaskRun()");
                return true;
            } else {
                logger.log(Level.SEVERE, "Failed running QueueTaskRun()");
                return false;
            }

		} catch (Exception e) {
			logger.log(Level.SEVERE, "Exception = " + e);
			return false;
		}

	}

}

