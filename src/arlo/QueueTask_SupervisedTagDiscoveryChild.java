package arlo;

import java.util.Vector;
import java.util.Iterator;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.File;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Time;
import java.sql.SQLException;
import java.io.StringWriter;
import java.io.PrintWriter;

import arlo.NesterUser;
import arlo.NesterAudioFile;
import arlo.NesterJob;

import arlo.QueueTask;

public class QueueTask_SupervisedTagDiscoveryChild extends QueueTask {

	static final private String taskName = "SupervisedTagDiscoveryChild";

	public boolean runQueue(Vector<NesterUser> restrictedUsers) {
		return super._runQueue(taskName, restrictedUsers);
	}

	void runJob(NesterJob job) {

		if (job == null) {
			System.out.println("QueueTask_SupervisedTagDiscoveryChild::runJob received null job");
			return;
		}

		System.out.println("Starting QueueTask_SupervisedTagDiscoveryChild::runJob jobId = " + job.id);

		Connection connection = QueueTask.openDatabaseConnection();

		if (job.parentJob_id == null) {
			setJobStatusError(job.id, connection);  // error occured
		} else {
			if (RunChildJob(job, connection)) {
				setJobStatusComplete(job.id, connection);
			} else {
				setJobStatusError(job.id, connection);  // error occured
			}
		}

		closeDatabaseConnection(connection);
	}

	/** \brief Run a SupervisedTagDiscovery 'child' job.
	*
	* @param childJob the NesterJob object of the 'child' job.
	* @param connection Opened database connection.
	* @return true if success, false if any error encountered. 
	*/

	boolean RunChildJob(NesterJob childJob, Connection connection) {
		Logger logger;
		logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		try {

			SupervisedTagDiscovery supervisedTagDiscovery = new SupervisedTagDiscovery(childJob.id);

			if (supervisedTagDiscovery.QueueTaskRun(connection)) {
				logger.log(Level.INFO, "Success running QueueTaskRun()");
				return true;
			} else {
				logger.log(Level.SEVERE, "Failed running QueueTaskRun()");
				return false;
			}
		} catch (Exception e) {
			StringWriter writer = new StringWriter();
			PrintWriter printWriter = new PrintWriter( writer );
			e.printStackTrace( printWriter );
			printWriter.flush();
			String stackTrace = writer.toString();

			logger.log(Level.SEVERE, "QueueTask_SupervisedTagDiscovery::runJob Failed Exception" + e + " -- " + stackTrace);
			return false;
		}


			/////
			// we have the 'child' job, now get the parent

//			if (childJob.parentJob_id == null) {
//				logger.log(Level.SEVERE, "QueueTask_SupervisedTagDiscovery::runJob null parentJob");
//				return false;
//			}
//
//			NesterJob parentJob = new NesterJob();
//			if (! parentJob.getNesterJob(childJob.parentJob_id, connection)) {
//				logger.log(Level.SEVERE, "QueueTask_SupervisedTagDiscovery::runJob Failed retrieving parentJob");
//				return false;
//			}
//
//			// get user
//			NesterUser nesterUser = NesterUser.getNesterUser(childJob.user_id, connection);
//			if (nesterUser == null) {
//				logger.log(Level.SEVERE, "Error retrieving NesterUser");
//				return false;
//			}
//
//			////////
//			// get JobParameters
//
//			int mediaFileId, numberOfTagsToDiscover, numFrequencyBands;
//			double numTimeFramesPerSecond, dampingRatio, minFrequency, maxFrequency, spectraWeight, pitchWeight; 
//			double averageEnergyWeight;
//			int numExemplars;
//			double  maxOverlapFraction, minPerformance;
//			Vector<Integer> sourceTagSets; 
//			int destinationTagSet;
//
//			try {
//				mediaFileId = Integer.parseInt(childJob.GetJobParameter("mediaFileId", connection));
//				numberOfTagsToDiscover = Integer.parseInt(parentJob.GetJobParameter("numberOfTagsToDiscover", connection));
//				numFrequencyBands = Integer.parseInt(parentJob.GetJobParameter("numFrequencyBands", connection));
//				numTimeFramesPerSecond = Double.parseDouble(parentJob.GetJobParameter("numTimeFramesPerSecond", connection));
//				dampingRatio = Double.parseDouble(parentJob.GetJobParameter("dampingRatio", connection));
//				minFrequency = Double.parseDouble(parentJob.GetJobParameter("minFrequency", connection));
//				maxFrequency = Double.parseDouble(parentJob.GetJobParameter("maxFrequency", connection));
//				spectraWeight = Double.parseDouble(parentJob.GetJobParameter("spectraWeight", connection));
//				pitchWeight = Double.parseDouble(parentJob.GetJobParameter("pitchWeight", connection));
//				averageEnergyWeight = Double.parseDouble(parentJob.GetJobParameter("averageEnergyWeight", connection));
//				
//				numExemplars = Integer.parseInt(parentJob.GetJobParameter("numExemplars", connection));
//
//				maxOverlapFraction = Double.parseDouble(parentJob.GetJobParameter("maxOverlapFraction", connection));
//				minPerformance = Double.parseDouble(parentJob.GetJobParameter("maxOverlapFraction", connection));
//				
//				destinationTagSet = Integer.parseInt(parentJob.GetJobParameter("destinationTagSet", connection));
//
//				String sourceTagSetsCSV = parentJob.GetJobParameter("sourceTagSets", connection);
//				sourceTagSets = new Vector<Integer>();
//				
//				for(String sourceTagSet : sourceTagSetsCSV.split(",")) {
//					sourceTagSets.add(Integer.valueOf(sourceTagSet.trim()));
//				}
//
//			} catch (NumberFormatException e) {
//				logger.log(Level.SEVERE, "QueueTask_SupervisedTagDiscovery::runJob Failed Parsing Parameters");
//				return false;
//			}

			////
			// Build TagDiscoveryBias

			////
			// build SupervisedTagDiscovery

// SupervisedTagDiscovery supervisedTagDiscovery = new SupervisedTagDiscovery(this, supervisedTagDiscoveryBiasID);
// supervisedTagDiscovery.start();



			


//		} catch (Exception e) {
//			logger.log(Level.SEVERE, "Exception = " + e);
//			return false;
//		}

//		return true;

	}

}

