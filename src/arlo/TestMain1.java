package arlo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.Date;
import java.util.HashSet;
import java.util.Random;
import java.util.Vector;


import adapt.IO;

public class TestMain1 {

	public static void main1(String[] args) {

		String ROOT_CREATION_DIRECTORY = "/media/BigDisk/";
		String ROOT_DATA_DIRECTORY = "/media/c/data/";
		String META_DATA_FILE_PATH = "/media/c/metadata.xls";
		//

		HashSet<Long> assignedRandomIDs = new HashSet<Long>();

		//

		Vector<File> searchDirectoryFileVector = new Vector<File>();
		Vector<File> resultFileVector = new Vector<File>();

		searchDirectoryFileVector.add(new File(ROOT_CREATION_DIRECTORY));

		IO.getPathStrings(searchDirectoryFileVector, resultFileVector);

		System.out.println("resultFileVector.size() = " + resultFileVector.size());

		int count = 0;
		for (File file : resultFileVector) {

			String absolutePath = file.getAbsolutePath();

			if (absolutePath.endsWith(".rar")) {

				boolean success = unrarFileInPlace(absolutePath);

				if (success) {
					count++;
				}
			}

		}
		System.out.println("count = " + count);
		System.out.println("done!");

	}

	public static void main2(String[] args) {

		String ROOT_CREATION_DIRECTORY = "/media/BigDisk/";
		String META_DATA_FILE_PATH = "/media/BigDisk/metadata.xls";

		//

		HashSet<Long> assignedRandomIDs = new HashSet<Long>();

		//

		Vector<File> searchDirectoryFileVector = new Vector<File>();
		Vector<File> resultFileVector = new Vector<File>();

		searchDirectoryFileVector.add(new File(ROOT_CREATION_DIRECTORY));

		IO.getPathStrings(searchDirectoryFileVector, resultFileVector);

		System.out.println("resultFileVector.size() = " + resultFileVector.size());

		File outfile = new File(META_DATA_FILE_PATH);

		PrintStream stream = null;
		try {
			stream = new PrintStream(outfile);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		stream.println("row\trandomID\tlastModified\tlength\textension\trootDirectory\trelativePath");

		int row = 0;
		int numLinked = 0;
		Random random = new Random(123);
		for (File file : resultFileVector) {


			String absolutePath = file.getAbsolutePath();

			String path = absolutePath;

			int index = path.lastIndexOf(".");

			if (index == -1)
				continue;

			String extension = path.substring(index + 1).toLowerCase();
			System.out.println("extension = " + extension);

			if (!(extension.equalsIgnoreCase("wav") || extension.equalsIgnoreCase("mp3") || extension.equalsIgnoreCase("flac") || extension
					.equalsIgnoreCase("wma")))
				continue;

			String relativePath = absolutePath.substring(ROOT_CREATION_DIRECTORY.length());

			long length = file.length();

			long lastModified = file.lastModified();

			long randomID = -1;

			while (true) {
				randomID = Math.abs(random.nextLong());

				if (assignedRandomIDs.contains(randomID)) {
					continue;
				}

				break;

			}

			assignedRandomIDs.add(randomID);

			row++;
			stream.println(row + "\t" + randomID + "\t" + lastModified + "\t" + length + "\t" + extension + "\t"
					+ ROOT_CREATION_DIRECTORY + "\t" + relativePath);

			boolean success = linkFileToRandomID(absolutePath, randomID);

			if (success) {
				numLinked++;
			}

		}

		stream.close();

		int numMissed = resultFileVector.size() - numLinked;
		System.out.println("numLinked = " + numLinked);
		System.out.println("numMissed = " + numMissed);
		System.out.println("done!");

	}

	static boolean linkFileToRandomID(String absolutePath, long randomID) {

		String ROOT_DATA_DIRECTORY = "/media/BigDisk/data";

		// start the ls command running

		// String path = absolutePath;
		// path = path.replaceAll("\\ ", "\\\\ ");
		// path = path.replaceAll("\\.", "\\\\.");
		// path = path.replaceAll("\\'", "\\\\'");
		// path = path.replaceAll("\\(", "\\\\(");
		// path = path.replaceAll("\\)", "\\\\)");
		String newPath = ROOT_DATA_DIRECTORY + "/" + randomID;

		// String commandString = "ln" + " " + "/media/c/t1 /media/c/t2";
		String commandString = "ln -v" + " " + absolutePath + " " + newPath;

		Runtime runtime = Runtime.getRuntime();
		Process proc = null;
		try {

			String[] args = new String[] { "ln", "-v", absolutePath, newPath };

			proc = runtime.exec(args);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		InputStream inputstream = proc.getInputStream();
		InputStreamReader inputstreamreader = new InputStreamReader(inputstream);
		BufferedReader bufferedreader = new BufferedReader(inputstreamreader);

		// read the ls output

		boolean success = false;
		String line;
		try {
			while ((line = bufferedreader.readLine()) != null) {
				System.out.println(line);
				success = true;
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			proc.waitFor();

			proc.getInputStream().close();
			proc.getOutputStream().close();
			proc.getErrorStream().close(); 
		} catch (InterruptedException e) {
			System.err.println(e);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		if (!success) {

			System.out.println("FAILED: " + commandString);
		}

		return success;

	}

	static boolean unrarFileInPlace(String absolutePath) {

		int index = absolutePath.lastIndexOf('/');

		String directory = absolutePath.substring(0, index);

		System.out.println("absolutePath = " + absolutePath);
		System.out.println("directory = " + directory);
		System.out.println("index = " + index);

		Runtime runtime = Runtime.getRuntime();
		Process proc = null;
		try {

			String[] args = new String[] { "unrar", "x", "-y", "-p-", absolutePath };

			proc = runtime.exec(args, null, new File(directory));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		InputStream inputstream = proc.getInputStream();
		InputStreamReader inputstreamreader = new InputStreamReader(inputstream);
		BufferedReader bufferedreader = new BufferedReader(inputstreamreader);

		boolean success = false;
		String line;
		try {
			while ((line = bufferedreader.readLine()) != null) {
				System.out.println(line);
				success = true;
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			if (proc.waitFor() != 0) {
				if (proc.exitValue() != 1)
					System.err.println("exit value = " + proc.exitValue());
			}
		} catch (InterruptedException e) {
			System.err.println(e);
		}

		return success;

	}
}
