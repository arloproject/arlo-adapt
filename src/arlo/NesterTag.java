package arlo;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Vector;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.HashMap;

import java.io.PrintWriter;
import java.io.StringWriter;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;

import com.goebl.david.WebbException;
import org.json.JSONObject;


public class NesterTag implements Comparable<NesterTag> {

	int id;

	int user_id;
	
	Timestamp creationDate;

	int tagClass_id;

	int tagSet_id;
	int audioFile_id;
	double startTime;
	double endTime;
	double minFrequency;
	double maxFrequency;

	int parentTag_id;

	boolean randomlyChosen;
	boolean machineTagged;
	boolean userTagged;

	double strength; // 0.0 to 1.0 scale

	/* added for meandre internal use only */
	/* TODO Refactor these out at some point */

	int audioFileIndex;
	NesterAudioFile nesterAudioFile;
	int exampleIndex;
	double durationInSeconds;



	NesterTag(
			int id, int user_id, Timestamp creationDate, int tagClass_id,
			int tagSet_id, int audioFile_id, double startTime, double endTime, double minFrequency, double maxFrequency,
			int parentTag_id, boolean randomlyChosen, boolean machineTagged, boolean userTagged, double strength)
	{
		this.id = id;
		this.user_id = user_id;
		this.creationDate = creationDate;
		this.tagClass_id = tagClass_id;
		this.tagSet_id = tagSet_id;
		this.audioFile_id = audioFile_id;
		this.startTime = startTime;
		this.endTime = endTime;
		this.minFrequency = minFrequency;
		this.maxFrequency = maxFrequency;
		this.parentTag_id = parentTag_id;
		this.randomlyChosen = randomlyChosen;
		this.machineTagged = machineTagged;
		this.userTagged = userTagged;
		this.strength = strength;
	}

	/** \brief Sort according to Tag time duration.
	 *
	 * TODO I'm not sure I like default using duration as the sort. 
	 * Maybe remove this here and place into individual sorts.
	 */
	public int compareTo(NesterTag o) {
		double this_duration = this.endTime - this.startTime;
		double o_duration = o.endTime - o.startTime;

		if (o_duration < this_duration)
			return -1;
		if (o_duration > this_duration)
			return 1;
		// TODO Auto-generated method stub
		return 0;
	}


	/** \brief Internal Function to Save a list of Tags via the API
	 *
	 * @param logger An initialized Logger object for error reporting.
	 * @param projectId The database ID of the project to which we are saving Tags.
	 * @param dictsToAdd Vector of Tag Dicts to save
	 * @return True on Success, False if any error encountered.
	 */

	private static boolean saveTagDictsViaAPI(Logger logger, int projectId, Vector<HashMap<String, Object>> dictsToAdd) {
		// convert response to JSON
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("tags", dictsToAdd);

		///
		// Save Tags

		try {

			NesterWebbClient.getNesterWebbClient()
				.post("/bulkTagCreate/" + projectId + "/")
				.body(jsonObject)  // auto sets Content-Type to Json
				.ensureSuccess()
				.asVoid();

		} catch (WebbException e) {
			StringWriter excWriter = new StringWriter();
			PrintWriter printWriter = new PrintWriter( excWriter );
			e.printStackTrace( printWriter );
			printWriter.flush();
			String stackTrace = excWriter.toString();
			logger.log(Level.SEVERE,
				"NesterJob::SaveJobLogMessage() Failed Status: " + e.getResponse().getStatusCode() +
				" Message: " + e.getResponse().getResponseMessage() +
				"\nBody:\n" + e.getResponse().getBody() +
				"\nErrorBody:\n" +  e.getResponse().getErrorBody() +
				"\nException " + e + " -- " + stackTrace);

			return false;
		}

		return true;
	}


	/** \brief Add a list of Tags.
	 *
	 * @param projectId The database ID of the project to which we are saving Tags.
	 * @param nesterTags List of Tags to add.
	 * @param connection An opened database connection, or 'null' to use the API save.
	 * @return True on Success, False if any error encountered. 
	 */

	public static boolean addTagsToProject(
			int projectId,
			ArrayList<NesterTag> nesterTags, 
			Connection connection)
	{

		Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
		logger.log(Level.INFO, "addTagsToProject");

long startTime = System.nanoTime();


		int numToAdd = nesterTags.size();
		if (numToAdd == 0) {
			return true;
		}

		if (connection == null) {

			// Use the API to add new Tags

			long start = System.nanoTime();

			///
			// Build JSON Dictionaries
			Vector<HashMap<String, Object>> dictsToAdd = new Vector<HashMap<String, Object>>();
			HashMap<String, Object> dictObject;

			for (int i = 0; i < numToAdd; i++) {
				NesterTag nesterTag = nesterTags.get(i);
				dictObject = new HashMap<String, Object>();

				dictObject.put("user", nesterTag.user_id);
				dictObject.put("tagSet", nesterTag.tagSet_id);
				dictObject.put("mediaFile", nesterTag.audioFile_id);
				dictObject.put("startTime", nesterTag.startTime);
				dictObject.put("endTime", nesterTag.endTime);
				dictObject.put("minFrequency", nesterTag.minFrequency);
				dictObject.put("maxFrequency", nesterTag.maxFrequency);
				dictObject.put("tagClass", nesterTag.tagClass_id);
				if (nesterTag.parentTag_id < 0) {
					dictObject.put("parentTag", null);
				} else {
					dictObject.put("parentTag", nesterTag.parentTag_id);
				}

				dictObject.put("randomlyChosen", nesterTag.randomlyChosen);
				dictObject.put("machineTagged", nesterTag.machineTagged);
				dictObject.put("userTagged", nesterTag.userTagged);
				dictObject.put("strength", nesterTag.strength);

				dictsToAdd.add(dictObject);

				// save in smaller batches
				if (dictsToAdd.size() >= 100) {
					if (! saveTagDictsViaAPI(logger, projectId, dictsToAdd)) {
						logger.log(Level.SEVERE, "Failed Saving Tags via API");
						return false;
					}
					dictsToAdd.clear();
				}
			}

			if (dictsToAdd.size() > 0) {
				if (! saveTagDictsViaAPI(logger, projectId, dictsToAdd)) {
					logger.log(Level.SEVERE, "Failed Saving Tags via API");
					return false;
				}
			}


		} else {

			// Use the Database connection


			Statement stmt1 = null;
			ResultSet uprs1 = null;

			Statement stmt2 = null;
			ResultSet uprs2 = null;

			Statement stmt3 = null;
			ResultSet uprs3 = null;

			logger.log(Level.INFO, "addTagToProject numToAdd = " + numToAdd);

			try {
// TODO optimize these... we're returning all the records apparently
				stmt1 = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
				uprs1 = stmt1.executeQuery("SELECT * FROM tools_tag LIMIT 1");
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Exception = " + e);
				return false;
			}

			for (int i = 0; i < numToAdd; i++) {

				try {

					NesterTag nesterTag = nesterTags.get(i);

					/////
					// Handle TagClass

					int tagClass_id = -1;
					if (nesterTag.tagClass_id == -1) {
						try {

							Statement s = connection.createStatement();
							s.executeQuery("select * from tools_tagclass where user_id = " + nesterTag.user_id + " and tagSet_id = " + nesterTag.tagSet_id
							+ " and className = \"unknown\"");
							ResultSet rs = s.getResultSet();
							if (rs.next()) {
								tagClass_id = rs.getInt("id");
							}
							rs.close();
							s.close();
						} catch (Exception e) {
							logger.log(Level.INFO, "name not found");
							logger.log(Level.INFO, "Exception = " + e);
						}

					} else {
						tagClass_id = nesterTag.tagClass_id;
					}

					uprs1.moveToInsertRow();
					uprs1.updateInt("user_id", nesterTag.user_id);
					uprs1.updateInt("tagSet_id", nesterTag.tagSet_id);
					uprs1.updateInt("mediaFile_id", nesterTag.audioFile_id);

					uprs1.updateTimestamp("creationDate", nesterTag.creationDate);
					uprs1.updateDouble("startTime", nesterTag.startTime);
					uprs1.updateDouble("endTime", nesterTag.endTime);
					uprs1.updateDouble("minFrequency", nesterTag.minFrequency);
					uprs1.updateDouble("maxFrequency", nesterTag.maxFrequency);

					uprs1.updateInt("tagClass_id", tagClass_id);
					if (nesterTag.parentTag_id < 0) {
						uprs1.updateNull("parentTag_id");
					} else {
						uprs1.updateInt("parentTag_id", nesterTag.parentTag_id);
					}
					uprs1.updateBoolean("randomlyChosen", nesterTag.randomlyChosen);
					uprs1.updateBoolean("machineTagged", nesterTag.machineTagged);
					uprs1.updateBoolean("userTagged", nesterTag.userTagged);
					uprs1.updateDouble("strength", nesterTag.strength);
					logger.log(Level.INFO, "addTagToProject strength = " + nesterTag.strength);

					uprs1.insertRow();

				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("Exception = " + e);
					return false;
				}
			}

			try {
				uprs1.close();
				stmt1.close();
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Exception = " + e);
				return false;
			}
		}

long diff = System.nanoTime() - startTime;
logger.log(Level.INFO, "PerfMon - addTagsToProject - Added " + numToAdd + " tags in " + (float) (diff / 1000000000) + " (s)");

		return true;
	}


	/** \brief Delete all Tags from a given TagSet.
	*
	* @param tagSetId Database id of the TagSet to empty.
	* @param connection An opened database connection.
	* @return true if successful, false if any error occurred.
	*/

	static public boolean deleteAllTagsInTagSet(int tagSetId, Connection connection) {
		Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		try {

			// first find any tags whose parent is within the TagSet to empty, and unset their parent
			Statement s1 = connection.createStatement();
			s1.executeUpdate(
				"UPDATE tools_tag " + 
				"SET parentTag_id = NULL " + 
				"WHERE tagSet_id = " + tagSetId
			);
			ResultSet rs1 = s1.getResultSet();
			if (rs1 != null)
				rs1.close();
			s1.close();

			// now, delete all Tags within the TagSet to empty
			s1 = connection.createStatement();
			s1.executeUpdate(
				"DELETE FROM tools_tag WHERE tagSet_id = " + tagSetId
			);
			rs1 = s1.getResultSet();
			if (rs1 != null)
				rs1.close();
			s1.close();

		} catch (Exception e) {
			logger.log(Level.INFO, "Exception = " + e);
			return false;
		}

		return true;
	}


	public static boolean runAddTagTests(int tagSet1, int tagSet2, Connection connection) {
		Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		String results = "\n";
		int user_id = -1;
		int mediaFile_id = -1;
		int tagClass_id = -1;
		int projectId = -1;

		int numTagsToAdd = 1000;

		java.sql.Timestamp creationDate = new java.sql.Timestamp(System.currentTimeMillis());
		long startTime;
		long diff;


		// First ensure these TagSets are empty
		if (NesterTagSet.getNesterTagSetTags(tagSet1, connection).size() > 0) {
			logger.log(Level.SEVERE, "TagSet ID " + tagSet1 + " Not Empty");
			return false;
		}
		if (NesterTagSet.getNesterTagSetTags(tagSet2, connection).size() > 0) {
			logger.log(Level.SEVERE, "TagSet ID " + tagSet2 + " Not Empty");
			return false;
		}

		///
		// Build Tags

		// For DB
		ArrayList<NesterTag> dbTags = new ArrayList<NesterTag>();
		for (int x = 0; x < numTagsToAdd; x++) {
			dbTags.add(new NesterTag(-1, user_id, creationDate, tagClass_id, tagSet1, mediaFile_id, x, x + 0.5, 100, 1000 + x, -1, false, true, false, x));
		}
		// Add one more with irrational values
		dbTags.add(new NesterTag(-1, user_id, creationDate, tagClass_id, tagSet1, mediaFile_id, 1/17.0, 2/17.0, 100 + 3/17.0, 1000 + 4/17.0, -1, false, true, false, 5/17.0));

		// For API
		ArrayList<NesterTag> apiTags = new ArrayList<NesterTag>();
		for (int x = 0; x < numTagsToAdd; x++) {
			apiTags.add(new NesterTag(-1, user_id, creationDate, tagClass_id, tagSet2, mediaFile_id, x, x + 0.5, 100, 1000 + x, -1, false, true, false, x));
		}
		// Add one more with irrational values
		apiTags.add(new NesterTag(-1, user_id, creationDate, tagClass_id, tagSet2, mediaFile_id, 1/17.0, 2/17.0, 100 + 3/17.0, 1000 + 4/17.0, -1, false, true, false, 5/17.0));

		for (int run = 0; run < 5; run++) {

			///
			// Load Tags

			// Load with DB
			startTime = System.nanoTime();
			if (! addTagsToProject(projectId, dbTags, connection)) {
				logger.log(Level.SEVERE, "Failed Saving Tags via Database");
				return false;
			}
			diff = System.nanoTime() - startTime;
			results += "Time For DB - " + (float) (diff / 1000000000) + " (s)\n";

			// Load with API
			startTime = System.nanoTime();
			if (! addTagsToProject(projectId, apiTags, null)) {
				logger.log(Level.SEVERE, "Failed Saving Tags via API");
				return false;
			}
			diff = System.nanoTime() - startTime;
			results += "Time For API - " + (float) (diff / 1000000000) + " (s)\n";
		
		
		
			// Retrieve both with DB
			Vector<NesterTag> dbSavedTags = NesterTagSet.getNesterTagSetTags(tagSet1, connection);
			Vector<NesterTag> apiSavedTags = NesterTagSet.getNesterTagSetTags(tagSet2, connection);

			// compare
			if (dbSavedTags.size() != apiSavedTags.size()) {
				logger.log(Level.SEVERE, "Returned TagSets different size.");
				logger.log(Level.SEVERE, "DB Tags Count: " + dbSavedTags.size());
				logger.log(Level.SEVERE, "API Tags Count: " + apiSavedTags.size());
				return false;
			}

			for (NesterTag dbTag : dbSavedTags) {
				NesterTag apiTag = null;
				// find the matching Tag from the API
				for (NesterTag z : apiSavedTags) {
					if (z.startTime == dbTag.startTime) {
						apiTag = z;
						break;
					}
				}
				if (apiTag == null) {
					logger.log(Level.SEVERE, "Could not Find matching Tags.");
					return false;
				}
				boolean fail = false;
				if (dbTag.endTime != apiTag.endTime) {fail = true;}
				if (dbTag.user_id != apiTag.user_id) {fail = true;}
				if (dbTag.audioFile_id != apiTag.audioFile_id) {fail = true;}
				if (dbTag.minFrequency != apiTag.minFrequency) {fail = true;}
				if (dbTag.maxFrequency != apiTag.maxFrequency) {fail = true;}
				if (dbTag.tagClass_id != apiTag.tagClass_id) {fail = true;}
				if (dbTag.parentTag_id != apiTag.parentTag_id) {fail = true;}
				if (dbTag.randomlyChosen != apiTag.randomlyChosen) {fail = true;}
				if (dbTag.machineTagged != apiTag.machineTagged) {fail = true;}
				if (dbTag.userTagged != apiTag.userTagged) {fail = true;}
				if (dbTag.strength != apiTag.strength) {fail = true;}

				if (fail) {
					logger.log(Level.SEVERE, "Tags Don't Match.");
					return false;
				}

			}

			// Retrieve both with API
		
			// compare
		
			///
			// empty tagsets
			if (! deleteAllTagsInTagSet(tagSet1, connection)) {
				logger.log(Level.SEVERE, "Failed Emptying TagSet1");
				return false;
			}
			if (! deleteAllTagsInTagSet(tagSet2, connection)) {
				logger.log(Level.SEVERE, "Failed Emptying TagSet2");
				return false;
			}

		}

		logger.log(Level.INFO, results);
		return true;

	}







}
