package arlo;

public class NesterTagAndDistance implements Comparable<NesterTagAndDistance>{
	
	NesterTag tag;
	double distance;
	
	
	public int compareTo(NesterTagAndDistance o) {
		
		
		if (this.distance < o.distance)
			return -1;
		if (this.distance == o.distance) 
			return 0;
		
		return 1;

	}
	
	NesterTagAndDistance (NesterTag tag, double distance) {
		this.tag = tag;
		this.distance = distance;
	}

}
