package arlo;

import java.io.*;

import com.amd.aparapi.Kernel;

public class BandPass implements Serializable {

	final static long serialVersionUID = -2547450855909917698L;

	final static boolean gpuDebugMode = false;

	private int numBands = 20;

	public void setNumBands(int value) {
		this.numBands = value;
	}

	public int getNumBands() {
		return this.numBands;
	}

	private int numTimeSlices = 4;

	public void setNumSegments(int value) {
		this.numTimeSlices = value;
	}

	public int getNumTimeSlices() {
		return this.numTimeSlices;
	}

	private double rawSamplingRate = 44100.0;

	public void setRawSamplingRate(double value) {
		this.rawSamplingRate = value;
	}

	public double getRawSamplingRate() {
		return this.rawSamplingRate;
	}

	private double minBandFrequency = 0.1;

	public void setMinBandFrequency(double value) {
		this.minBandFrequency = value;
	}

	public double getMinBandFrequency() {
		return this.minBandFrequency;
	}

	private double maxBandFrequency = 44100.0;

	public void setMaxBandFrequency(double value) {
		this.maxBandFrequency = value;
	}

	public double getMaxBandFrequency() {
		return this.maxBandFrequency;
	}

	private double dampingRatio = 0.03;

	public void setDampingRatio(double value) {
		this.dampingRatio = value;
	}

	public double getDampingRatio() {
		return this.dampingRatio;
	}

	public double[] bandFrequencies = null;
	double[] bandA1s = null;
	double[] bandA2s = null;
	double[] bandB0s = null;
	double[] bandB1s = null;
	double[] bandB2s = null;
	double[] bandN0s = null;
	double[] bandN1s = null;
	double[] bandN2s = null;
	double[] lastBandSignals = null;
	double[] keneticEnergySums = null;
	double[] potentialEnergySums = null;

	double bandToBandFactor;
	double bandFrequency;
	int exampleIndex = 0;

	int numThreads = -1;
	WorkerThread[] workers = null;

	int samplesPerSlice = -1;

	int numValues = -1;
	int[] intensityValues = null;
	int[] audioBuffer = null;
	int offset = -1;
	double controllerSleepInterval = 1.0;

	/** Creates a new instance of BandPass */
	public BandPass() {
	}

	public int getFrequencyIndex(double freq) {
		double bestDistance = Double.POSITIVE_INFINITY;
		int bestIndex = -1;
		for (int frequencyIndex = 0; frequencyIndex < bandFrequencies.length; frequencyIndex++) {
			double distance = Math.abs(bandFrequencies[frequencyIndex] - freq);
			if (distance < bestDistance) {
				bestDistance = distance;
				bestIndex = frequencyIndex;
			}
		}
		return bestIndex;
	}

	public void initialize() {

		bandToBandFactor = Math.exp(Math.log(maxBandFrequency / minBandFrequency) / (numBands - 1));

		if (numBands > 1) {
			bandFrequency = minBandFrequency;
		} else {
			bandFrequency = (minBandFrequency + maxBandFrequency) / 2.0;
		}

		bandFrequencies = new double[numBands];

		bandA1s = new double[numBands];
		bandA2s = new double[numBands];
		bandB1s = new double[numBands];
		bandB2s = new double[numBands];
		bandN0s = new double[numBands];
		bandN1s = new double[numBands];
		bandN2s = new double[numBands];

		lastBandSignals = new double[numBands];

		// keneticEnergySums = new double[numBands];
		// potentialEnergySums = new double[numBands];

		for (int b = 0; b < numBands; b++) {

			double wn, wd, y1, y2, y3, a, t;

			bandFrequencies[b] = bandFrequency;

			wn = (2.0 * Math.PI * bandFrequencies[b]);
			wd = (wn * Math.sqrt(1.0 - dampingRatio * dampingRatio));
			a = (dampingRatio * wn);

			t = (1.0 / rawSamplingRate);
			y1 = ((2.0 * a / wd) * Math.exp(-a * t) * Math.sin(wd * t));
			t = (2.0 / rawSamplingRate);
			y2 = ((2.0 * a / wd) * Math.exp(-a * t) * Math.sin(wd * t));
			t = (3.0 / rawSamplingRate);
			y3 = ((2.0 * a / wd) * Math.exp(-a * t) * Math.sin(wd * t));

			bandA1s[b] = (y2 / y1);
			bandA2s[b] = ((y1 * y3 - y2 * y2) / (y1 * y1));
			bandB1s[b] = (y1);
			bandB2s[b] = (-y1);
			bandN0s[b] = (0);
			bandN1s[b] = (0);
			bandN2s[b] = (0);

			// System.out.println("bandFrequency = " + bandFrequency);

			bandFrequency *= bandToBandFactor;

		}

		for (int b = 0; b < numBands; b++) {
			lastBandSignals[b] = 0.0;
		}
	}

	public int[] calculateBandPassSpectrogram(int[] audioBuffer, int offset, int numSamples, boolean reset) {
		return calculateBandPassSpectrogram(audioBuffer, offset, numSamples, reset, 1);
	}

	public int[] calculateBandPassSpectrogram(int[] audioSamples, int offset, int numSamples, boolean reset, int numThreads) {

		if (numSamples > audioSamples.length) {
			System.out.println("Error!  (numSamples > audioBuffer.length)");
			System.out.println("numSamples = " + numSamples);
			System.out.println("audioBuffer.length = " + audioSamples.length);
			Thread.dumpStack();
			System.exit(1);
		}

		//

		samplesPerSlice = numSamples / numTimeSlices;

//		double samplesPerSliceDouble = (double) numSamples / (double) numTimeSlices;

//		System.out.println("samplesPerSlice = " + samplesPerSlice);
//
//		if (samplesPerSlice != samplesPerSliceDouble) {
//			System.out.println("Error!  (samplesPerSlice(" + samplesPerSlice + ") != samplesPerSliceDouble(" + samplesPerSliceDouble + "))");
//		}

		numValues = numTimeSlices * numBands;
		intensityValues = new int[numValues];

		//

		boolean useGPU = false;

		if (false) // !!!
			if (numSamples > 44100 * 10.0) {
				useGPU = true;
			}

//		System.out.println("calculateBandPassSpectrogram: numSamples = " + numSamples);
//		System.out.println("calculateBandPassSpectrogram: samplesPerSlice = " + samplesPerSlice);
//		System.out.println("calculateBandPassSpectrogram: numTimeSlices = " + numTimeSlices);
//		System.out.println("calculateBandPassSpectrogram: useGPU     = " + useGPU);

		if (useGPU) {

			System.out.println("starting calculateBandPassSpectrogram with GPU");

			int audioSamplesNumFrames = numSamples / samplesPerSlice;

			System.out.println("audioSamplesNumFrames = " + audioSamplesNumFrames);

			//

			double chunkSizeInSeconds = 8.0;
			double preRollSizeInSeconds = 1.0;

			int chunkSizeInSamples = (int) (chunkSizeInSeconds * rawSamplingRate);
			System.out.println("chunkSizeInSamples = " + chunkSizeInSamples);

			int numFramesPerChunk = chunkSizeInSamples / samplesPerSlice;
			System.out.println("numFramesPerChunk   = " + numFramesPerChunk);

			int numChunks = (int) (numTimeSlices / numFramesPerChunk);
			if (numTimeSlices * numFramesPerChunk < samplesPerSlice) {
				numChunks++;
			}

			int numProblems = numChunks * numBands;

			System.out.println("numChunks   = " + numChunks);
			System.out.println("numBands    = " + numBands);
			System.out.println("numProblems = " + numProblems);

			int framesPerChunk = (int) (chunkSizeInSamples / samplesPerSlice);
			System.out.println("framesPerChunk = " + framesPerChunk);

			int spectraNumValues = numChunks * numBands * framesPerChunk;
			System.out.println("spectraNumValues = " + spectraNumValues);

			// spectra = new int[spectraNumValues];

			// int samplesPerFrame = (int) (candidateNesterAudioFile.wavFileMetaData.sampleRate / nesterSupervisedTagDiscoveryBias.numTimeFramesPerSecond);
			int numSamplesPerChunk = (int) (chunkSizeInSeconds * rawSamplingRate);

			System.out.println("numSamplesPerChunk = " + numSamplesPerChunk);

			// public BandPassKernel(int _numProblems, double _dampingFactor, double _minFrequency, double _maxFrequency, int _numFrequencyBands, int _numSamplesPerFrame,
			// int _numSamplesPerChunk, int _numChunks, int[] _audioSamples, int[] _audioSpectra) {

			int numSamplesForConditioning = (int) (preRollSizeInSeconds * rawSamplingRate);

			boolean debugMode = false;
			final BandPassGPU.BandPassKernel kernel = new BandPassGPU.BandPassKernel(debugMode, numProblems, dampingRatio, minBandFrequency, maxBandFrequency, numBands,
					rawSamplingRate, samplesPerSlice, numSamplesPerChunk, numChunks, numSamplesForConditioning, numTimeSlices, audioSamples, intensityValues);

			kernel.putAudioFileDataOnGPU();

			if (gpuDebugMode)
				kernel.setExecutionMode(Kernel.EXECUTION_MODE.SEQ);
			else
				kernel.setExecutionMode(Kernel.EXECUTION_MODE.GPU);

			if (numProblems > 0)
				kernel.nextGeneration(numProblems);

			// for (int i = 0; i < spectra.length; i++) {
			//
			// System.out.println("spectra[" + i + "] = " + spectra[i]);
			//
			// }

			kernel.dispose();

		} else {

			this.numThreads = numThreads;

			if (reset) {

				for (int b = 0; b < numBands; b++) {
					bandN0s[b] = (0);
					bandN1s[b] = (0);
					bandN2s[b] = (0);
				}

			}

			this.audioBuffer = audioSamples;
			this.offset = offset;

			if (numThreads == 1) {

				WorkerThread workerThread = new WorkerThread(0);
				workerThread.run();

			} else {
				startWorkers(numThreads);

				waitForSolution();
			}
		}

		return intensityValues;

	}

	private void waitForSolution() {
		boolean alive = true;
		while (alive) {
			alive = false;
			for (int i = 0; i < workers.length; i++) {
				if (workers[i].isAlive()) {
					alive = true;
				}
			}
			// System.out.println("Thread.sleep(1);Thread.yield();");
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// System.out.println("Thread.yield();");
			// Thread.yield();
		}
		// System.out.println("waitForSolution finished");
	}

	public void startWorkers(int numThreads) {

		this.numThreads = numThreads;

		this.workers = new WorkerThread[numThreads];

		for (int i = 0; i < workers.length; i++) {
			workers[i] = new WorkerThread(i);
			workers[i].start();
		}

	}

	private class WorkerThread extends Thread {

		int threadIndex = -1;

		WorkerThread(int threadIndex) {
			this.threadIndex = threadIndex;
		}

		public void run() {

			for (int t = 0; t < numTimeSlices; t++) {

				for (int f = 0; f < numBands; f++) {

					if (f % numThreads == threadIndex) {

						int i;
						double n0, n1, n2, a1, a2, b1, b2;
						double bandSignal, lastBandSignal, deltaBandSignal;
						// double twoLogKeneticEnergySum, twoLogPotentialEnergySum;

						double twoLogEnergySum = 0.0;

						a1 = bandA1s[f];
						a2 = bandA2s[f];
						b1 = bandB1s[f];
						b2 = bandB2s[f];
						n0 = bandN0s[f];
						n1 = bandN1s[f];
						n2 = bandN2s[f];

						lastBandSignal = lastBandSignals[f];
						bandSignal = 0.0f;

						int start_i = t * samplesPerSlice + offset;
						int end_i = start_i + samplesPerSlice;
						for (i = start_i; i < end_i; i++) {
							n2 = n1;
							n1 = n0;

							// try {
							n0 = audioBuffer[i] + a1 * n1 + a2 * n2;
							// } catch (Exception e) {
							// // TODO Auto-generated catch block
							// e.printStackTrace();
							// System.exit(1);
							// }

							bandSignal = n1 * b1 + n2 * b2;
							deltaBandSignal = lastBandSignal - bandSignal;
							lastBandSignal = bandSignal;

							if (deltaBandSignal < 0)
								twoLogEnergySum -= deltaBandSignal;
							else
								twoLogEnergySum += deltaBandSignal;

							if (bandSignal < 0)
								twoLogEnergySum -= bandSignal;
							else
								twoLogEnergySum += bandSignal;

						}

						bandN0s[f] = n0;
						bandN1s[f] = n1;
						bandN2s[f] = n2;

						lastBandSignals[f] = bandSignal;

						// keneticEnergySums[f] = twoLogKeneticEnergySum;
						// potentialEnergySums[f] = twoLogPotentialEnergySum;

						// intensityValues[f * numTimeSlices + t] = (int) (keneticEnergySums[f] + potentialEnergySums[f]);
						intensityValues[f * numTimeSlices + t] = (int) (twoLogEnergySum);

					}

				}
			}

		}

		public void runOld() {

			for (int t = 0; t < numTimeSlices; t++) {

				for (int f = 0; f < numBands; f++) {

					if (f % numThreads == threadIndex) {

						int i;
						double n0, n1, n2, a1, a2, b1, b2;
						double bandSignal, lastBandSignal, deltaBandSignal;
						double twoLogKeneticEnergySum, twoLogPotentialEnergySum;

						keneticEnergySums[f] = 0.0f;
						potentialEnergySums[f] = 0.0f;

						twoLogKeneticEnergySum = keneticEnergySums[f];
						twoLogPotentialEnergySum = potentialEnergySums[f];

						a1 = bandA1s[f];
						a2 = bandA2s[f];
						b1 = bandB1s[f];
						b2 = bandB2s[f];
						n0 = bandN0s[f];
						n1 = bandN1s[f];
						n2 = bandN2s[f];

						lastBandSignal = lastBandSignals[f];
						bandSignal = 0.0f;

						int start_i = t * samplesPerSlice + offset;
						int end_i = start_i + samplesPerSlice;
						for (i = start_i; i < end_i; i++) {
							n2 = n1;
							n1 = n0;

							// try {
							n0 = audioBuffer[i] + a1 * n1 + a2 * n2;
							// } catch (Exception e) {
							// // TODO Auto-generated catch block
							// e.printStackTrace();
							// System.exit(1);
							// }

							bandSignal = n1 * b1 + n2 * b2;
							deltaBandSignal = lastBandSignal - bandSignal;
							lastBandSignal = bandSignal;

							if (deltaBandSignal < 0)
								twoLogKeneticEnergySum -= deltaBandSignal;
							else
								twoLogKeneticEnergySum += deltaBandSignal;

							if (bandSignal < 0)
								twoLogPotentialEnergySum -= bandSignal;
							else
								twoLogPotentialEnergySum += bandSignal;

						}

						bandN0s[f] = n0;
						bandN1s[f] = n1;
						bandN2s[f] = n2;

						lastBandSignals[f] = bandSignal;

						keneticEnergySums[f] = twoLogKeneticEnergySum;
						potentialEnergySums[f] = twoLogPotentialEnergySum;

						intensityValues[f * numTimeSlices + t] = (int) (keneticEnergySums[f] + potentialEnergySums[f]);

					}

				}
			}

		}

	}

}
