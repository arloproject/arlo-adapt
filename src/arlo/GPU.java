package arlo;

/*
 Copyright (c) 2010-2011, Advanced Micro Devices, Inc.
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 following conditions are met:

 Redistributions of source code must retain the above copyright notice, this list of conditions and the following
 disclaimer. 

 Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
 disclaimer in the documentation and/or other materials provided with the distribution. 

 Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission. 

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 If you use the software (in whole or in part), you shall adhere to all applicable U.S., European, and other export
 laws, including but not limited to the U.S. Export Administration Regulations ("EAR"), (15 C.F.R. Sections 730 through
 774), and E.U. Council Regulation (EC) No 1334/2000 of 22 June 2000.  Further, pursuant to Section 740.6 of the EAR,
 you hereby certify that, except pursuant to a license granted by the United States Department of Commerce Bureau of 
 Industry and Security or as otherwise permitted pursuant to a License Exception under the U.S. Export Administration 
 Regulations ("EAR"), you will not (1) export, re-export or release to a national of a country in Country Groups D:1,
 E:1 or E:2 any restricted technology, software, or source code you receive hereunder, or (2) export to Country Groups
 D:1, E:1 or E:2 the direct product of such technology or software, if such foreign produced direct product is subject
 to national security controls as identified on the Commerce Control List (currently found in Supplement 1 to Part 774
 of EAR).  For the most current Country Group listings, or for additional information about the EAR or your obligations
 under those regulations, please refer to the U.S. Bureau of Industry and Security's website at http://www.bis.doc.gov/. 

 */

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import com.amd.aparapi.Kernel;
import com.amd.aparapi.ProfileInfo;
import com.amd.aparapi.Range;

/**
 * An example Aparapi application which demonstrates Conways 'Game Of Life'.
 * 
 * Original code from Witold Bolt's site https://github.com/houp/aparapi/tree/master/samples/gameoflife.
 * 
 * Converted to use int buffer and some performance tweaks by Gary Frost
 * 
 * @author Wiltold Bolt
 * @author Gary Frost
 */
public class GPU {

	public static class LifeKernel extends Kernel {

		private static final int ALIVE = 0xffffff;
		private static final int DEAD = 0;

		private final float[] spectra_1;
		private final float[] spectra_2;
		private final float[] correlations;

		private final int size1;
		private final int size2;

		private final Range range;

		public LifeKernel(int _size1, int _size2, float[] _spectra_1, float[] _spectra_2, float[] _correlations) {

			size1 = _size1;
			size2 = _size2;

			spectra_1 = _spectra_1;
			spectra_2 = _spectra_2;
			correlations = _correlations;

			int numProblems = size1 * size2;

			range = Range.create(numProblems, 256);
			System.out.println("range = " + range);
			
			
//			setExplicit(true); // This gives us a performance boost
//			put(_spectra_1); // Because we are using explicit buffer management we must put the imageData array
//			put(_spectra_2); // Because we are using explicit buffer management we must put the imageData array
//			put(correlations); // Because we are using explicit buffer management we must put the imageData array

		}

		@Override
		public void run() {

			int gid = getGlobalId();

			int position_1 = gid / size2;
			int position_2 = gid % size2;
			
			correlations[gid] = Math.abs(spectra_1[position_1] - spectra_2[position_2]);
			

		}

		public void nextGeneration() {
			// swap fromBase and toBase

			execute(range);

		}

	}

	static boolean running = false;

	public static void main(String[] _args) {

		final int size_1 = 2048;
		final int size_2 = 4096;

		final float[] spectra_1 = new float[size_1];
		final float[] spectra_2 = new float[size_2];
		final float[] correlations = new float[size_1 * size_2];

		for (int i = 0; i < size_1; i++) {
			spectra_1[i] = (float) Math.random();
		}
		for (int i = 0; i < size_2; i++) {
			spectra_2[i] = (float) Math.random();
		}

		final LifeKernel lifeKernel = new LifeKernel(size_1, size_2, spectra_1, spectra_2, correlations);

		System.out.println("lifeKernel.getExecutionMode() = " + lifeKernel.getExecutionMode());

		long start = System.currentTimeMillis();
		long generations = 0;
		while (true) {

			lifeKernel.nextGeneration(); // Work is performed here
			generations++;
			long now = System.currentTimeMillis();
			if (now - start > 1000) {
				System.out.println("Generations/Second = " + (generations * 1000.0) / (now - start));
				System.out.println("BIPS = " + (generations * 1000.0) / (now - start) * size_1 * size_2 * 1 / 1000000000);
				start = now;
				generations = 0;
			}
		}

	}
}
