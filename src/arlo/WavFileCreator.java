package arlo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.DecimalFormat;


public class WavFileCreator {

	DecimalFormat format = new DecimalFormat("###,###,###,###,###");
	DecimalFormat format2digits = new DecimalFormat("00");

	public static void createWavFile(File originalFile, File newFile, double startTime, double endTime, boolean verbose) {

		int dataStartIndex = -1;

		// 
		// open original file
		// 

		if (verbose)
			System.out.println("originalFilePath = " + originalFile.getPath());

		RandomAccessFile originalRandomAccessFile = null;
		long orginalFileSize = -1;
		try {
			originalRandomAccessFile = new RandomAccessFile(originalFile, "r");
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		}
		try {
			orginalFileSize = originalRandomAccessFile.length();
		} catch (IOException ex) {
			ex.printStackTrace();
		}

		if (verbose)
			System.out.println("orginalFileSize = " + orginalFileSize);

		int maxHeaderBufferSize = (int) Math.min(orginalFileSize, 10000L);

		// 
		// open and fully read original file header
		// 

		byte[] headerBuffer = new byte[maxHeaderBufferSize];

		try {
			originalRandomAccessFile.readFully(headerBuffer);
		} catch (IOException ex) {
			ex.printStackTrace();
		}

		// //////////////////////////////
		// initialize buffer pointers //
		// //////////////////////////////

		int originalIndex = 0;

		if (verbose)
			System.out.println("parsing RIFF header");

		int riffLength = 4;
		for (int i = 0; i < riffLength; i++) {

			if (verbose)
				System.out.println(originalIndex + "," + headerBuffer[originalIndex] + "," + (char) headerBuffer[originalIndex]);
			originalIndex++;

		}

		if (verbose)
			System.out.println("parsing RIFF size");

		int riffSize = IO1L.parseDWORD(headerBuffer, originalIndex);
		originalIndex += 4;

		if (verbose)
			System.out.println("riffSize = " + riffSize);

		if (verbose)
			System.out.println("parsing WAVE header");
		int waveLength = 4;
		for (int i = 0; i < waveLength; i++) {

			if (verbose)
				System.out.println(originalIndex + "," + headerBuffer[originalIndex] + "," + (char) headerBuffer[originalIndex]);
			originalIndex++;

		}

		int fmtSize = -1;
		long dataSize = -1;

		int wFormatTag = -1;
		int nChannels = -1;
		int nSamplesPerSec = -1;
		int nAvgBytesPerSec = -1;
		int nBlockAlign = -1;
		int bitsPerSample = -1;

		if (verbose)
			System.out.println("parsing WAVE chunks");
		boolean stopParsing = false;
		while (!stopParsing) {

			// if (originalIndex % 2 == 1) { // why
			// originalIndex++;
			// }

			if (originalIndex == maxHeaderBufferSize) {
				break;
			}

			int chunkIDSize = 4;

			if (originalIndex == orginalFileSize || originalIndex == maxHeaderBufferSize) {
				break;
			}

			char[] chunkType = new char[4];
			for (int i = 0; i < chunkIDSize; i++) {

				if (verbose)
					System.out.println(originalIndex + "," + headerBuffer[originalIndex] + "," + (char) headerBuffer[originalIndex]);
				chunkType[i] = (char) headerBuffer[originalIndex];
				originalIndex++;

			}

			if (chunkType[0] == 'b' && chunkType[1] == 'e' && chunkType[2] == 'x' && chunkType[3] == 't') {
				if (verbose)
					System.out.println("found bext chunk");

				int bextSize = IO1L.parseDWORD(headerBuffer, originalIndex);
				originalIndex += 4;
				if (verbose)
					System.out.println("bextSize = " + bextSize);

				int bextStartIndex = originalIndex;

				byte[] bextDescription = new byte[256];
				for (int i = 0; i < 256; i++) {
					bextDescription[i] = headerBuffer[originalIndex];
					if (verbose)
						System.out.println(bextDescription[i]);
					originalIndex++;
				}

				String bextDescriptionString = new String(bextDescription);
				if (bextDescriptionString.indexOf(0) != -1)
					bextDescriptionString = bextDescriptionString.substring(0, bextDescriptionString.indexOf(0));
				if (verbose)
					System.out.println("bextDescription = " + bextDescriptionString);

				byte[] bextOriginator = new byte[32];
				for (int i = 0; i < 32; i++) {
					bextOriginator[i] = headerBuffer[originalIndex];
					originalIndex++;
				}
				String bextOriginatorString = new String(bextOriginator);
				if (bextOriginatorString.indexOf(0) != -1)
					bextOriginatorString = bextOriginatorString.substring(0, bextOriginatorString.indexOf(0));
				if (verbose)
					System.out.println("bextOriginatorString = " + bextOriginatorString);

				byte[] bextOriginatorReference = new byte[32];
				for (int i = 0; i < 32; i++) {
					bextOriginatorReference[i] = headerBuffer[originalIndex];
					originalIndex++;
				}
				String bextOriginatorReferenceString = new String(bextOriginatorReference);
				if (bextOriginatorReferenceString.indexOf(0) != -1)
					bextOriginatorReferenceString = bextOriginatorReferenceString.substring(0, bextOriginatorReferenceString.indexOf(0));
				if (verbose)
					System.out.println("bextOriginatorReferenceString = " + bextOriginatorReferenceString);

				byte[] bextOriginatorDate = new byte[10];
				for (int i = 0; i < 10; i++) {
					bextOriginatorDate[i] = headerBuffer[originalIndex];
					originalIndex++;
				}
				String bextOriginatorDateString = new String(bextOriginatorDate);
				if (bextOriginatorDateString.indexOf(0) != -1)
					bextOriginatorDateString = bextOriginatorDateString.substring(0, bextOriginatorDateString.indexOf(0));
				if (verbose)
					System.out.println("bextOriginatorDateString = " + bextOriginatorDateString);

				byte[] bextOriginatorTime = new byte[8];
				for (int i = 0; i < 8; i++) {
					bextOriginatorTime[i] = headerBuffer[originalIndex];
					originalIndex++;
				}
				String bextOriginatorTimeString = new String(bextOriginatorTime);
				if (bextOriginatorTimeString.indexOf(0) != -1)
					bextOriginatorTimeString = bextOriginatorTimeString.substring(0, bextOriginatorTimeString.indexOf(0));
				if (verbose)
					System.out.println("bextOriginatorTimeString = " + bextOriginatorTimeString);

				byte[] bextIgnore1 = new byte[4 + 4 + 2 + 64 + 190];
				for (int i = 0; i < bextIgnore1.length; i++) {
					bextIgnore1[i] = headerBuffer[originalIndex];
				}

				int numBextBytesParsed = originalIndex - bextStartIndex;
				int codingHistorySize = bextSize - numBextBytesParsed;

				byte[] bextCodingHistory = new byte[codingHistorySize];
				for (int i = 0; i < codingHistorySize; i++) {
					bextCodingHistory[i] = headerBuffer[originalIndex];
					originalIndex++;
				}
				String bextCodingHistoryString = new String(bextCodingHistory);
				if (bextCodingHistoryString.indexOf(0) != -1)
					bextCodingHistoryString = bextCodingHistoryString.substring(0, bextCodingHistoryString.indexOf(0));
				if (verbose)
					System.out.println("bextCodingHistoryString = " + bextCodingHistoryString);

			} else if (chunkType[0] == 'f' && chunkType[1] == 'm' && chunkType[2] == 't' && chunkType[3] == ' ') {
				if (verbose)
					System.out.println("found fmt chunk");
				{
					fmtSize = IO1L.parseDWORD(headerBuffer, originalIndex);
					originalIndex += 4;

					if (fmtSize != 16 && fmtSize != 18) {
						System.out.println("Error!  wrong fmtSize(" + fmtSize + ") -- should be 16 or 18");
						System.exit(0);
					}

					wFormatTag = IO1L.parseWORD(headerBuffer, originalIndex);
					originalIndex += 2;

					nChannels = IO1L.parseWORD(headerBuffer, originalIndex);
					originalIndex += 2;

					nSamplesPerSec = IO1L.parseDWORD(headerBuffer, originalIndex);
					originalIndex += 4;

					nAvgBytesPerSec = IO1L.parseDWORD(headerBuffer, originalIndex);
					originalIndex += 4;

					nBlockAlign = IO1L.parseWORD(headerBuffer, originalIndex);
					originalIndex += 2;

					bitsPerSample = IO1L.parseWORD(headerBuffer, originalIndex);
					originalIndex += 2;

					int numPads = fmtSize - 16;

					originalIndex += numPads;

					if (verbose) {
						System.out.println("fmtSize         = " + fmtSize);
						System.out.println("wFormatTag      = " + wFormatTag);
						System.out.println("nChannels       = " + nChannels);
						System.out.println("nSamplesPerSec  = " + nSamplesPerSec);
						System.out.println("nAvgBytesPerSec = " + nAvgBytesPerSec);
						System.out.println("nBlockAlign     = " + nBlockAlign);
						System.out.println("bitsPerSample   = " + bitsPerSample);
					}

				}

			} else if (chunkType[0] == 'd' && chunkType[1] == 'a' && chunkType[2] == 't' && chunkType[3] == 'a') {

				if (verbose)
					System.out.println("found data chunk");
				dataSize = IO1L.parseDWORD(headerBuffer, originalIndex);
				if (verbose)
					System.out.println("dataSize = " + dataSize);
				originalIndex += 4;
				dataStartIndex = originalIndex;

				// stop parsing after data chunk is found
				break;

			}

		}
		if (verbose)
			System.out.println("originalIndex = " + originalIndex);

		//
		//
		// CREATE NEW WAV FILE FROM ORIGINAL WAV FILE
		//
		//

		double durationInSeconds = endTime - startTime;
		long durationInFrames = (long) (durationInSeconds * nSamplesPerSec);
		long durationInBytes = durationInFrames * nBlockAlign;

		long newDataSize = durationInBytes;

		long dataSizeDelta = newDataSize - dataSize;

		if (verbose)
			System.out.println("newFile.getPath() = " + newFile.getPath());

		RandomAccessFile newRandomAccessFile = null;
		try {
			newRandomAccessFile = new RandomAccessFile(newFile, "rw");
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		}

		int maxFileSize = 10000000; // 10M bytes
		byte[] byteBuffer = new byte[maxFileSize];

		int newIndex = 0;

		byteBuffer[newIndex++] = 'R';
		byteBuffer[newIndex++] = 'I';
		byteBuffer[newIndex++] = 'F';
		byteBuffer[newIndex++] = 'F';

		long newRiffSize = riffSize + dataSizeDelta;

		if (verbose)
			System.out.println("newRiffSize = " + newRiffSize);

		newIndex = IO1L.encodeDWORDlong(byteBuffer, newIndex, newRiffSize);

		byteBuffer[newIndex++] = 'W';
		byteBuffer[newIndex++] = 'A';
		byteBuffer[newIndex++] = 'V';
		byteBuffer[newIndex++] = 'E';

		byteBuffer[newIndex++] = 'f';
		byteBuffer[newIndex++] = 'm';
		byteBuffer[newIndex++] = 't';
		byteBuffer[newIndex++] = ' ';

		fmtSize = 16;
		newIndex = IO1L.encodeDWORD(byteBuffer, newIndex, fmtSize);
		newIndex = IO1L.encodeWORD(byteBuffer, newIndex, wFormatTag);
		newIndex = IO1L.encodeWORD(byteBuffer, newIndex, nChannels);
		newIndex = IO1L.encodeDWORD(byteBuffer, newIndex, nSamplesPerSec);
		newIndex = IO1L.encodeDWORD(byteBuffer, newIndex, nAvgBytesPerSec);
		newIndex = IO1L.encodeWORD(byteBuffer, newIndex, nBlockAlign);
		newIndex = IO1L.encodeWORD(byteBuffer, newIndex, bitsPerSample);

		byteBuffer[newIndex++] = 'd';
		byteBuffer[newIndex++] = 'a';
		byteBuffer[newIndex++] = 't';
		byteBuffer[newIndex++] = 'a';

		newIndex = IO1L.encodeDWORDlong(byteBuffer, newIndex, newDataSize);

		// read segment of original file

		byte[] segmentData = new byte[(int) durationInBytes];

		int segmentOffset = ((int) (startTime * nSamplesPerSec)) * nBlockAlign;
		int newDataStartIndex = dataStartIndex + segmentOffset;
		try {
			originalRandomAccessFile.seek(newDataStartIndex);
			originalRandomAccessFile.readFully(segmentData);
			for (int i = 0; i < segmentData.length; i++) {
				byteBuffer[newIndex++] = segmentData[i];
			}
			newRandomAccessFile.write(byteBuffer, 0, newIndex);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			newRandomAccessFile.close();
			originalRandomAccessFile.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}

	}

	public static void createWavFile(AudioFile audioFile, File newFile, int[] intData) {

		int numFrames = intData.length;

		int numBytesPerSample = 2;
		long dataSize = numFrames * numBytesPerSample;
		int headerSize = 44;

		long riffSize = dataSize + headerSize;

//		int maxFileSize = 10000000; // 10M bytes
//		byte[] byteBuffer = new byte[maxFileSize];
		
		byte[] byteBuffer = new byte[(int) riffSize];

		boolean normalize = true;
		if (normalize) {
			int maxValue = Integer.MIN_VALUE;
			int minValue = Integer.MAX_VALUE;
			for (int frameIndex = 0; frameIndex < numFrames; frameIndex++) {

				if (intData[frameIndex] > maxValue)
					maxValue = intData[frameIndex];
				if (intData[frameIndex] < minValue)
					minValue = intData[frameIndex];

			}

			int maxMagnitude = Math.max(-minValue, maxValue);

			double factor = (double) Short.MAX_VALUE / (double) (maxMagnitude + 1);
			for (int frameIndex = 0; frameIndex < numFrames; frameIndex++) {

				intData[frameIndex] = (int) (intData[frameIndex] * factor);

			}
		}

		RandomAccessFile newRandomAccessFile = null;
		try {
			newRandomAccessFile = new RandomAccessFile(newFile, "rw");
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		}

		int newIndex = 0;

		byteBuffer[newIndex++] = 'R';
		byteBuffer[newIndex++] = 'I';
		byteBuffer[newIndex++] = 'F';
		byteBuffer[newIndex++] = 'F';

		newIndex = IO1L.encodeDWORDlong(byteBuffer, newIndex, riffSize);

		byteBuffer[newIndex++] = 'W';
		byteBuffer[newIndex++] = 'A';
		byteBuffer[newIndex++] = 'V';
		byteBuffer[newIndex++] = 'E';

		byteBuffer[newIndex++] = 'f';
		byteBuffer[newIndex++] = 'm';
		byteBuffer[newIndex++] = 't';
		byteBuffer[newIndex++] = ' ';

		int fmtSize = 16;
		int wFormatTag = 1;
		int nChannels = 1;
		int nSamplesPerSec = audioFile.sampleRate;
		int nAvgBytesPerSec = audioFile.sampleRate * numBytesPerSample;
		int nBlockAlign = numBytesPerSample;
		int bitsPerSample = 16;

		newIndex = IO1L.encodeDWORD(byteBuffer, newIndex, fmtSize);
		newIndex = IO1L.encodeWORD(byteBuffer, newIndex, wFormatTag);
		newIndex = IO1L.encodeWORD(byteBuffer, newIndex, nChannels);
		newIndex = IO1L.encodeDWORD(byteBuffer, newIndex, nSamplesPerSec);
		newIndex = IO1L.encodeDWORD(byteBuffer, newIndex, nAvgBytesPerSec);
		newIndex = IO1L.encodeWORD(byteBuffer, newIndex, nBlockAlign);
		newIndex = IO1L.encodeWORD(byteBuffer, newIndex, bitsPerSample);

		byteBuffer[newIndex++] = 'd';
		byteBuffer[newIndex++] = 'a';
		byteBuffer[newIndex++] = 't';
		byteBuffer[newIndex++] = 'a';

		newIndex = IO1L.encodeDWORDlong(byteBuffer, newIndex, dataSize);

		for (int i = 0; i < numFrames; i++) {
			newIndex = IO1L.encodeWORD(byteBuffer, newIndex, intData[i]);
		}

		// read segment of original file

		try {
			newRandomAccessFile.write(byteBuffer, 0, newIndex);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			newRandomAccessFile.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}

	}

	public static void main(String[] args) {
		System.out.println("Hi");
		WavFileCreator.createWavFile(new File("/home/dtcheng/workspace/Meandre-1.3.2rc2/published_resources/bigSample.wav"), new File(
				"/home/dtcheng/workspace/Meandre-1.3.2rc2/published_resources/sample_1.wav"), 1000.0, 1010.0, true);
	}

}
