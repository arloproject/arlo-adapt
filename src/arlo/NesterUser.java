package arlo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Time;
import java.sql.SQLException;

public class NesterUser {

	int id;
	String name;
	boolean is_staff;

	NesterUser(int id, String name, boolean is_staff) {
		
		this.id = id;
		this.name = name;
		this.is_staff = is_staff;
		
	}

	public static NesterUser getNesterUser(int userID, Connection connection) {
		NesterUser user = null;

		try {
			Statement s = connection.createStatement();
			s.executeQuery("select * from auth_user where id = " + userID);
			ResultSet rs = s.getResultSet();
			if (rs.next()) {
				user = new NesterUser(rs.getInt("id"), rs.getString("username"), rs.getBoolean("is_staff"));
			} else {
				System.out.println("Error! User not found.");
			}
			rs.close();
			s.close();
		} catch (Exception e) {
			System.out.println("name not found");
			System.out.println("Exception = " + e);
		}

		return user;
	}


}
