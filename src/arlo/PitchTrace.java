//
// Create a PitchTrace from an AudioFile

package arlo;

import java.util.Arrays;
import java.util.logging.Logger;
import java.util.Iterator;

import adapt.IO;
import adapt.Utility;


public class PitchTrace{
	public enum PitchTraceType {
		NONE, FUNDAMENTAL, MAX_ENERGY
	}
	 

	public PitchTrace() {}

	/// Compute a PitchTrace for an AudioFile. 
	// 
	// @return double[x][y]
	//           x = timeIndex
	//           y = 0 - Pitch Frequency Band
	//             = 1 - energy value

	static public double[][] getPitchTraceData(
		PitchTraceType pitchTraceType,
		AudioFile audioFile, 
		double startTime,
		int windowNumTimeFrames, 

		int numFrequencyBands,
		double numTimeFramesPerSecond, 
		double dampingRatio, 
		double spectraMinFrequency, 
		double spectraMaxFrequency, 
		int pitchTraceNumSamplePoints, // Only for FUNDAMENTAL
		double pitchTraceMinCorrelation, // Only for FUNDAMENTAL
		double pitchTraceEntropyThreshold, // Only for FUNDAMENTAL
		double pitchTraceStartFreq, // Only for FUNDAMENTAL
		double pitchTraceEndFreq, // Only for FUNDAMENTAL
		double spectraPitchTraceMinEnergyThreshold, // Only for FUNDAMENTAL
		double spectraPitchTraceTolerance, // Only for FUNDAMENTAL
		double spectraPitchTraceInverseFreqWeight, // Only for FUNDAMENTAL
		double spectraPitchTraceMaxPathLengthPerTransition, // Only for FUNDAMENTAL
		int spectraPitchTraceWindowSize, // Only for FUNDAMENTAL
		double spectraPitchTraceExtendRangeFactor, // Only for FUNDAMENTAL
		boolean simpleNormalization
	) {

		Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
		double gain = 1;
		double scalingFactor = 0.2 * gain;
		boolean frequencyBandNormalization = false;

		int averagingWindowSize = 2;
		BandPass bandPass = null;
		double[][] imageSpectraData = new double[windowNumTimeFrames][numFrequencyBands];



		// read bytes from wav file
		// convert bytes to mono samples

		double windowDuration = (double) windowNumTimeFrames / numTimeFramesPerSecond;
		double endTime = startTime + windowDuration;

		int[] audioDataAsInt = null;
		int padNumFrames = 1;
		audioDataAsInt = BytesToWavData.getWavDataAsInt(audioFile, startTime, endTime, padNumFrames, true);



		double baseLineIntensity = Double.NaN;

		int[][] imageBandValues = new int[windowNumTimeFrames][numFrequencyBands];
		// imageSpectraData = new double[windowNumTimeFrames][numFrequencyBands];

		int timeSegmentOffset = (int) (startTime * numTimeFramesPerSecond);


		logger.info("###### getPitchTraceData:audioFile = " + audioFile);
		logger.info("###### getPitchTraceData:startTime = " + startTime);
		logger.info("###### getPitchTraceData:windowDuration = " + windowDuration);
		logger.info("###### getPitchTraceData:endTime = " + endTime);
		logger.info("###### getPitchTraceData:windowNumTimeFrames = " + windowNumTimeFrames);
		logger.info("###### getPitchTraceData:numTimeFramesPerSecond = " + numTimeFramesPerSecond);

		bandPass = new BandPass();

		bandPass.setDampingRatio(dampingRatio);
		bandPass.setMinBandFrequency(spectraMinFrequency);
		bandPass.setMaxBandFrequency(spectraMaxFrequency);
		bandPass.setNumBands(numFrequencyBands);
		bandPass.setRawSamplingRate(audioFile.sampleRate);
		bandPass.setNumSegments((int) (windowNumTimeFrames));

		bandPass.initialize();

		int numThreads = 8;
		int[] spectraFeatures = bandPass.calculateBandPassSpectrogram(audioDataAsInt, 0, (int) (windowDuration * audioFile.sampleRate), true, numThreads);

		// logger.info("spectraFeatures.length = " + spectraFeatures.length);

		for (int t = 0; t < windowNumTimeFrames; t++) {
			for (int f = 0; f < numFrequencyBands; f++) {
				int index = f * windowNumTimeFrames + t;
				imageBandValues[t][f] = spectraFeatures[index];
			}
		}

		if (simpleNormalization) {

			long intensitySum = 0;

			intensitySum = 0;

			for (int t = 0; t < windowNumTimeFrames; t++) {

				for (int f = 0; f < numFrequencyBands; f++) {

					int value1 = imageBandValues[t][f];

					intensitySum += value1;

				}
			}

			double averageIntensity = (double) intensitySum / windowNumTimeFrames / numFrequencyBands;

			baseLineIntensity = averageIntensity;

			for (int t = 0; t < windowNumTimeFrames; t++) {
				for (int f = 0; f < numFrequencyBands; f++) {
					imageSpectraData[t][f] = imageBandValues[t][f] / baseLineIntensity * scalingFactor;
				}
			}
		}

		if (frequencyBandNormalization) {

			for (int f = 0; f < numFrequencyBands; f++) {

				long intensitySum = 0;

				for (int t = 0; t < windowNumTimeFrames; t++) {

					int value1 = imageBandValues[t][f];

					intensitySum += value1;

				}

				double averageIntensity = (double) intensitySum / windowNumTimeFrames;

				baseLineIntensity = averageIntensity;

				for (int t = 0; t < windowNumTimeFrames; t++) {
					imageSpectraData[t][f] = imageBandValues[t][f] / baseLineIntensity * scalingFactor;
				}
			}
		}

		//
		//
		//  MAX_ENERGY
		//
		//		

		if (pitchTraceType == PitchTraceType.MAX_ENERGY) {

			double overallMinValue = Double.POSITIVE_INFINITY;
			double overallMaxValue = Double.NEGATIVE_INFINITY;
			for (int t = 0; t < windowNumTimeFrames; t++) {
				for (int f = 0; f < numFrequencyBands; f++) {
					if (overallMinValue > imageSpectraData[t][f]) {
						overallMinValue = imageSpectraData[t][f];
					}
					if (overallMaxValue < imageSpectraData[t][f]) {
						overallMaxValue = imageSpectraData[t][f];
					}
				}
			}


			int[] maxEnergyBandIndexData = new int[windowNumTimeFrames];
			
			for (int timeFrameIndex = 0; timeFrameIndex < windowNumTimeFrames; timeFrameIndex++) {
				
				double maxValue = Double.NEGATIVE_INFINITY;
				int maxEnergyBandIndex = -1;

				for (int f = 0; f < numFrequencyBands; f++) {
					double value = imageSpectraData[timeFrameIndex][f];
					if (value > maxValue) {
						maxValue = value;
						maxEnergyBandIndex = f;
					}
				}

				if (maxEnergyBandIndex == -1) {
					// all values are NaN, likely from an audio file of all 0's
					maxEnergyBandIndex = 0;
				}

				maxEnergyBandIndexData[timeFrameIndex] = maxEnergyBandIndex;

			}
			

			// @return double[x][y]
			//           x = timeIndex
			//           y = 0 - Pitch Frequency Band Index
			//             = 1 - energy value

			double[][] returnPitchTraceData = new double[windowNumTimeFrames][2];
			
			for (int timeFrameIndex = 0; timeFrameIndex < windowNumTimeFrames; timeFrameIndex++) {

				int maxEnergyBandIndex = maxEnergyBandIndexData[timeFrameIndex];

				returnPitchTraceData[timeFrameIndex][0] = bandPass.bandFrequencies[maxEnergyBandIndex];
				returnPitchTraceData[timeFrameIndex][1] = imageSpectraData[timeFrameIndex][maxEnergyBandIndex];
			}

			return returnPitchTraceData;
			
		}


		/// 
		//
		//  FUNDAMENTAL
		//
		//

		if (pitchTraceType == PitchTraceType.FUNDAMENTAL) {

			double startFreqForAnalysis = pitchTraceStartFreq;
			double endFreqForAnalysis = pitchTraceEndFreq;
			int numSamplePoints = pitchTraceNumSamplePoints;
//			double minCorrelationThreshold = pitchTraceMinCorrelation;
//			double maxEntropyThreshold = pitchTraceEntropyThreshold;

			// double tolerance = 0.15;
			double tolerance = spectraPitchTraceTolerance;
			// double inverseFreqWeightingPower = 0.75; // Dream Speech
			double inverseFreqWeightingPower = spectraPitchTraceInverseFreqWeight;
			// double tolerance = 0.15;
			// double inverseFreqWeightingPower = 0.0; // TEH comparison

			int fundamentalEnergyDistributionDownSampleFactor = 16;

			// int startHarmonic = 0; // Howel
			int startHarmonic = 1; // Hollywood

			// double minEnergyThreshold = 0.00;
//			double minEnergyThreshold = spectraPitchTraceMinEnergyThreshold;
			double minP = 1e-100;

			double distanceDivisor = 1.0;
			double distancePower = 0.1;

			double freqChangeFactor = Math.exp((Math.log(endFreqForAnalysis) - Math.log(startFreqForAnalysis)) / (numSamplePoints - 1));

			/******************************************/
			/* create harmonic templates for matching */
			/******************************************/

			double[] sampleFreqs = new double[numSamplePoints];
			double[][] harmonicWeightTemplates = new double[numSamplePoints][numFrequencyBands];

			double sampleFreq = startFreqForAnalysis;

			for (int sampleFreqIndex = 0; sampleFreqIndex < numSamplePoints; sampleFreqIndex++) {

				sampleFreq *= freqChangeFactor;
				sampleFreqs[sampleFreqIndex] = sampleFreq;

				for (int bandIndex = 0; bandIndex < numFrequencyBands; bandIndex++) {

					double bandFreq = bandPass.bandFrequencies[bandIndex];

					/* find distance to nearest Harmonic */

					if (false) {

						int freqMultiple = (int) (bandFreq / sampleFreq + 0.5);

						double difference = bandFreq - ((int) (bandFreq / sampleFreq) * sampleFreq);

						double distance = Math.min(sampleFreq - difference, difference);

						double proximityWeight = 1.0 / Math.pow(distance / distanceDivisor, distancePower);

						double freqMultipleWeight = Double.NaN;

						if (freqMultiple > startHarmonic) {
							freqMultipleWeight = 1.0;
						} else {
							freqMultipleWeight = 0.0;
						}

						harmonicWeightTemplates[sampleFreqIndex][bandIndex] = proximityWeight * freqMultipleWeight;
					}

					double freqMultiple = bandFreq / sampleFreq;

					double fractionRemainder = freqMultiple - (int) freqMultiple;

					boolean nearHarmonic = false;
					boolean farHarmonic = false;
					if (fractionRemainder < tolerance || fractionRemainder > (1.0 - tolerance)) {
						nearHarmonic = true;
					}
					if (fractionRemainder < (0.5 + tolerance) && fractionRemainder > (0.5 - tolerance)) {
						farHarmonic = true;
					}

					if (freqMultiple < (1.0 - tolerance)) {
						harmonicWeightTemplates[sampleFreqIndex][bandIndex] = -1.0;
					} else {
						if (nearHarmonic) {
							harmonicWeightTemplates[sampleFreqIndex][bandIndex] = 1.0;
						}
						if (farHarmonic) {
							harmonicWeightTemplates[sampleFreqIndex][bandIndex] = -1.0;
						}
					}
				}

			}

			double[] harmonicEnergies = new double[numSamplePoints];

			double f0 = startFreqForAnalysis;

			boolean showPitchTraceOnly = false;

			double overallMinValue = Double.POSITIVE_INFINITY;
			double overallMaxValue = Double.NEGATIVE_INFINITY;
			for (int t = 0; t < windowNumTimeFrames; t++) {
				for (int f = 0; f < numFrequencyBands; f++) {
					if (overallMinValue > imageSpectraData[t][f]) {
						overallMinValue = imageSpectraData[t][f];
					}
					if (overallMaxValue < imageSpectraData[t][f]) {
						overallMaxValue = imageSpectraData[t][f];
					}
				}
			}

			double[] fundamentalEnergyDistribution = new double[numSamplePoints];

			boolean outputDataToFile = false;

			String traceFileName = null;
			String distributionFileName = null;

			double windowMaxEnergyFrequencySum = 0;
			double windowMaxHarmonicCorrelationSum = 0;
			double windowEntropySum = 0;
			int windowCount = 0;

			double[] pitchData = new double[windowNumTimeFrames];
			double[] correlationData = new double[windowNumTimeFrames];
			double[] entropyData = new double[windowNumTimeFrames];
			double[] energyData = new double[windowNumTimeFrames];
			int[] maxEnergyBandIndexData = new int[windowNumTimeFrames];
			int[] maxEnergySampleIndexData = new int[windowNumTimeFrames];
			
			for (int timeFrameIndex = 0; timeFrameIndex < windowNumTimeFrames; timeFrameIndex++) {

				
				// boolean trackMaxEnergyPitch = false;
				// boolean trackFundamentalPitch = true;

				double maxValue = Double.NEGATIVE_INFINITY;
				int maxEnergyBandIndex = -1;
				int maxEnergySampleIndex = -1;
				double maxHarmonicCorrelation = Double.NEGATIVE_INFINITY;


				double maxEnergyFrequency = Double.NaN;

				IO.p("finding fundamental: t = " + timeFrameIndex / numTimeFramesPerSecond);

				/***************************************************************/
				/* use harmonic template to compute fundamental pitch energies */
				/***************************************************************/

				double harmonicsEnergyWeightedSum;
				double harmonicWeight;

				f0 = startFreqForAnalysis;
				for (int sampleFreqIndex = 0; sampleFreqIndex < numSamplePoints; sampleFreqIndex++) {
					harmonicEnergies[sampleFreqIndex] = Utility.correlation(imageSpectraData[timeFrameIndex], harmonicWeightTemplates[sampleFreqIndex], numFrequencyBands)
							/ Math.pow(f0, inverseFreqWeightingPower);
					f0 *= freqChangeFactor;
				}

				maxEnergyBandIndex = -1;
				maxHarmonicCorrelation = Double.NEGATIVE_INFINITY;

				f0 = startFreqForAnalysis;
				for (int candidateFreqIndex = 0; candidateFreqIndex < numSamplePoints; candidateFreqIndex++) {
//	logger.info("harmonicEnergies[candidateFreqIndex]: " + harmonicEnergies[candidateFreqIndex]);

					if (harmonicEnergies[candidateFreqIndex] > maxHarmonicCorrelation) {
						maxHarmonicCorrelation = harmonicEnergies[candidateFreqIndex];
						maxEnergyFrequency = f0;
						maxEnergyBandIndex = bandPass.getFrequencyIndex(maxEnergyFrequency);
						maxEnergySampleIndex = candidateFreqIndex;
					}

					f0 *= freqChangeFactor;

				}

				if (maxEnergyBandIndex == -1) {
					// all values are NaN, likely from an audio file of all 0's
					logger.info("maxEnergyBandIndex == -1");
					maxEnergyBandIndex = 0;
				}

				maxEnergyBandIndexData[timeFrameIndex] = maxEnergyBandIndex;
				maxEnergySampleIndexData[timeFrameIndex] = maxEnergySampleIndex;

				int startIndex = bandPass.getFrequencyIndex(startFreqForAnalysis);
				int endIndex = bandPass.getFrequencyIndex(endFreqForAnalysis * spectraPitchTraceExtendRangeFactor);
				int numBandsAnalyzed = endIndex - startIndex + 1;
				double energySum = 0.0;
				//				for (int bandIndex = 0; bandIndex < numFrequencyBands; bandIndex++) {
				for (int bandIndex = startIndex; bandIndex <= endIndex; bandIndex++) {
					energySum += imageSpectraData[timeFrameIndex][bandIndex];
				}

				double entropySum = 0.0;
				//				for (int bandIndex = 0; bandIndex < numFrequencyBands; bandIndex++) {
				for (int bandIndex = startIndex; bandIndex <= endIndex; bandIndex++) {
					double p = imageSpectraData[timeFrameIndex][bandIndex] / energySum;
					if (p < minP)
						p = minP;
					entropySum += p * Math.log(p);
				}

				double entropy = entropySum / numBandsAnalyzed;
				double energy = energySum / numBandsAnalyzed;

				pitchData[timeFrameIndex] = maxEnergyFrequency;
				correlationData[timeFrameIndex] = maxHarmonicCorrelation;
				entropyData[timeFrameIndex] = entropy;
				energyData[timeFrameIndex] = energy;

			}
			
			
			
			
			// sort values for filtering //
			
//			double[] pitchDataSorted = new double[windowNumTimeFrames];
//			double[] correlationDataSorted = new double[windowNumTimeFrames];
//			double[] entropyDataSorted = new double[windowNumTimeFrames];
//			double[] energyDataSorted = new double[windowNumTimeFrames];
//			int[] maxEnergyBandIndexDataSorted = new int[windowNumTimeFrames];
//			int[] maxEnergySampleIndexDataSorted = new int[windowNumTimeFrames];

			
//			System.arraycopy(correlationData, 0, correlationDataSorted, 0, windowNumTimeFrames);
//			System.arraycopy(entropyData, 0, entropyDataSorted, 0, windowNumTimeFrames);
//			System.arraycopy(energyData, 0, energyDataSorted, 0, windowNumTimeFrames);

//			Arrays.sort(correlationDataSorted);
//			Arrays.sort(entropyDataSorted);
//			Arrays.sort(energyDataSorted);
			
			double minCorrelation = pitchTraceMinCorrelation;
			// This was for using 'pitchTraceMinCorrelation' as a percentile selector (0 - 1) to specify 
			// relative values rather than absolute. For repeatability, we'll keep this absolute for the moment. 
			// double minCorrelationThreshold = correlationDataSorted[(int) (minCorrelation * (windowNumTimeFrames - 1))];
			double minCorrelationThreshold = minCorrelation;

			//double maxEntropy = 1.0 - pitchTraceEntropyThreshold;
			//double maxEntropyThreshold = entropyDataSorted[(int) (maxEntropy * (windowNumTimeFrames - 1))];
			double maxEntropyThreshold = pitchTraceEntropyThreshold;
			
			//double minEnergyFraction = spectraPitchTraceMinEnergyThreshold;
			//double minEnergyThreshold = energyDataSorted[(int) (minEnergyFraction * (windowNumTimeFrames - 1))];
			double minEnergyThreshold = spectraPitchTraceMinEnergyThreshold;
			

			
//			minCorrelationThreshold = -1.0;
////			minCorrelationThreshold = 0.00285;
////			maxEntropyThreshold = -0.0180;
//			maxEntropyThreshold = 0.0;
////			minEnergyThreshold = 0.04;
			

			IO.p("minCorrelationThreshold = " + minCorrelationThreshold);
			IO.p("maxEntropyThreshold = " + maxEntropyThreshold);
			IO.p("minEnergyThreshold = " + minEnergyThreshold);
			
			
			
			
			
			// analyze and pitch traces to provide data for filtering //
			
			// apply sliding window to path length //

			int halfWindowSizeMinusOne = (spectraPitchTraceWindowSize - 1) / 2;
			
			int windowSum = 0;
			
			int startTimeFrame = halfWindowSizeMinusOne + 1;
			int endTimeFrame = (windowNumTimeFrames - 1) -  halfWindowSizeMinusOne - 1;
			
			
			double [] pathLengths = new double[windowNumTimeFrames];
			boolean [] useTimeFrame = new boolean[windowNumTimeFrames];

			for (int centerTimeFrameIndex = startTimeFrame; centerTimeFrameIndex <= endTimeFrame; centerTimeFrameIndex++) {
				
				
				int startIndex = centerTimeFrameIndex - halfWindowSizeMinusOne;
				int endIndex = centerTimeFrameIndex + halfWindowSizeMinusOne;
				for (int i = startIndex; i <= endIndex; i++) {
					pathLengths[centerTimeFrameIndex] += Math.abs(pitchData[i] - ((pitchData[i - 1] + pitchData[i + 1]) / 2.0));
				}
				
				
				
				//
				// Apply Filters to Data
				//

				if ((pathLengths[centerTimeFrameIndex] / spectraPitchTraceWindowSize < spectraPitchTraceMaxPathLengthPerTransition) && //
						(energyData[centerTimeFrameIndex] > minEnergyThreshold) && //
						(entropyData[centerTimeFrameIndex] < maxEntropyThreshold) && //
						(correlationData[centerTimeFrameIndex] > minCorrelationThreshold)) {
					useTimeFrame[centerTimeFrameIndex] = true;
				} else {
					useTimeFrame[centerTimeFrameIndex] = false;
				}
				

//				if ((maxHarmonicCorrelation > minCorrelationThreshold) &&
//						(entropy < maxEntropyThreshold) &&
//						(energy > minEnergyThreshold)) 
				
				
				
			}
			
			
			
			// @return double[x][y]
			//           x = timeIndex
			//           y = 0 - Pitch Frequency Band
			//             = 1 - energy value

			double[][] returnPitchTraceData = new double[windowNumTimeFrames][2];

			for (int timeFrameIndex = 0; timeFrameIndex < windowNumTimeFrames; timeFrameIndex++) {

				int maxEnergyBandIndex = maxEnergyBandIndexData[timeFrameIndex];
				if (useTimeFrame[timeFrameIndex]) {
					returnPitchTraceData[timeFrameIndex][0] = bandPass.bandFrequencies[maxEnergyBandIndex];
				} else {
					returnPitchTraceData[timeFrameIndex][0] = 0;
				}
				returnPitchTraceData[timeFrameIndex][1] = imageSpectraData[timeFrameIndex][maxEnergyBandIndex];
			}

			return returnPitchTraceData;

		}

		return null;


	}

}
