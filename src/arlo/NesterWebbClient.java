package arlo;

import com.goebl.david.Webb;
import com.goebl.david.Response;
import org.json.JSONObject;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.security.cert.X509Certificate;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.PrintWriter;


// Builds a Webb client with default settings for Nester API

public class NesterWebbClient {

	public static Webb getNesterWebbClient() {

		Webb webb = Webb.create();
		webb.setBaseUri(ArloSettings.NesterAPIRootURL);
		webb.setDefaultHeader(Webb.HDR_USER_AGENT, "Adapt Webb Client");
		webb.setDefaultHeader("Authorization", "Token " + ArloSettings.NesterAPIToken);
		webb.setDefaultHeader("Accept", "*/*");
		webb.setDefaultHeader("Connection", "close");

		if (ArloSettings.NesterAPINoVerfySSL) {
			try {
				TrustManager[] trustAllCerts = new TrustManager[] { new AlwaysTrustManager() };
				SSLContext sslContext = SSLContext.getInstance("TLS");
				sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

				webb.setSSLSocketFactory(sslContext.getSocketFactory());
				webb.setHostnameVerifier(new TrustingHostnameVerifier());
			} catch (Exception e) {
				Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
				StringWriter excWriter = new StringWriter();
				PrintWriter printWriter = new PrintWriter( excWriter );
				e.printStackTrace( printWriter );
				printWriter.flush();
				String stackTrace = excWriter.toString();
				logger.log(Level.SEVERE, "NesterWebbClient::getNesterWebbClient() Failed Exception" + e + " -- " + stackTrace);

				// may be fine yet if SSL fails, let's let it try...
				//return null;
			}
		}

		return webb;
	}

	private static class TrustingHostnameVerifier implements HostnameVerifier {
		public boolean verify(String hostname, SSLSession session) {
			return true;
		}
	}

	private static class AlwaysTrustManager implements X509TrustManager {
		public void checkClientTrusted(X509Certificate[] arg0, String arg1) { }
		public void checkServerTrusted(X509Certificate[] arg0, String arg1) { }
		public X509Certificate[] getAcceptedIssuers() { return null; }
	}

}


