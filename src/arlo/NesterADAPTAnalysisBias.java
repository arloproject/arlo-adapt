package arlo;

import java.sql.Timestamp;

public class NesterADAPTAnalysisBias extends NesterJob {

	public int maxNumTrials;
	public double maxTimeInSeconds;

	// REPRESENTATION BIAS
	public int numFrequencyBands;
	public double numTimeFramesPerSecond;
	public double dampingRatio;
	public double minFrequency;
	public double maxFrequency;
	public double spectraWeight;
	public double pitchWeight;
	public double pitchEnergyWeight;
	public double averageEnergyWeight;

	// SELECTION BIAS
	public int numRandomProbes;
	public double minPerformance;

	NesterADAPTAnalysisBias(
			
			int id, 
			String name,
			int user_id, 
			Timestamp creationDate,
			int numToComplete, 
			int numCompleted, 
			double fractionCompleted, 
			double elapsedRealTime, 
			double timeToCompletion, 
			boolean isRunning,
			boolean wasStopped,
			boolean isComplete,
			boolean wasDeleted,
			
			int maxNumTrials,
			double maxTimeInSeconds,
			
			int numFrequencyBands, 
			double numTimeFramesPerSecond, 
			double dampingRatio, 
			double minFrequency,
			double maxFrequency, 
			
			double spectraWeight,
			double pitchWeight, 
			double pitchEnergyWeight, 
			double averageEnergyWeight,
			
			int numRandomProbes, 
			double minPerformance) {

		
		this.id = id;
		this.name = name;
		this.user_id = user_id;
		this.creationDate = creationDate;
		this.numToComplete = numToComplete;
		this.numCompleted = numCompleted;
		this.fractionCompleted = fractionCompleted;
		this.elapsedRealTime = elapsedRealTime;
		this.timeToCompletion = timeToCompletion;
		this.isRunning = isRunning;
		this.wasStopped = wasStopped;
		this.isComplete = isComplete;
		this.wasDeleted = wasDeleted;

		this.maxNumTrials = maxNumTrials;
		this.maxTimeInSeconds = maxTimeInSeconds;

		this.numFrequencyBands = numFrequencyBands;
		this.numTimeFramesPerSecond = numTimeFramesPerSecond;
		this.dampingRatio = dampingRatio;
		this.minFrequency = minFrequency;
		this.maxFrequency = maxFrequency;

		this.spectraWeight = spectraWeight;
		this.pitchWeight = pitchWeight;
		this.pitchEnergyWeight = pitchEnergyWeight;
		this.averageEnergyWeight = averageEnergyWeight;

		this.numRandomProbes = numRandomProbes;
		this.minPerformance = minPerformance;

	}

	public String getSpectraComputationBiasString() {
		String string = "numFrequencyBands=" + numFrequencyBands + "numTimeFramesPerSecond=" + numTimeFramesPerSecond + "dampingRatio=" + dampingRatio + "minFrequency="
				+ minFrequency + "maxFrequency=" + maxFrequency;
		return string;

	}
	//	
	// String toString() {
	// String s = "";
	//		
	// s += ""
	// }

}
