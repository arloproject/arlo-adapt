package arlo;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;
import java.util.logging.Level;

import java.sql.Connection;

import adapt.IO;

public class TagDiscovery extends Thread {

	/*                  */
	/* Class Properties */
	/*                  */

	boolean useAudioFilesCache = false;

	// TODO These settings should be moved out to the ArloSettings settings class
	String audioFilesFilePath = "/data/nesterAudioFiles.ser";
	String tagDiscoveryAllDataSpectraFilePath = null;
	String tagDiscoveryAllDataAudioFilesFilePath = null;

	// Database Objects
//	ServiceHead serviceHead;  // moved into SupervisedTagDiscovery to break dependence on ServiceHead
//	int tagDiscoveryBiasID;  // TODO this is redundant with tagDiscoveryBias below
	TagDiscoveryBias tagDiscoveryBias;

	// Job parameters
	int tagDiscoveryNumFrequencyBands;

	// Audio Files and data
	int tagDiscoveryNumAudioFiles;
	public Vector<NesterAudioFile> tagDiscoveryNesterAudioFiles;

	// int[][][] allDataSpectra = null;

	static String lastAllDataSpectraFilePath = null;
	static int[][][] lastAllDataSpectra = null;

	long[] tagDiscoveryAudioFileSpectraByteOffsets = null;
	long[] tagDiscoveryAudioFileSpectraNumBytes = null;
	long[] tagDiscoveryAudioFileSpectraNumFrames = null;

	int[][] tagDiscoveryAllExampleSpectra = null;
	int[] tagDiscoveryAllExampleSpectraNumFrames = null;
	int[] tagDiscoveryAllExampleSpectraStartFreqIndex = null;
	int[] tagDiscoveryAllExampleSpectraEndFreqIndex = null;

	int[] tagDiscoveryAudioFileSpectra = null;
	long tagDiscoveryAudioFileNumFrames;


	/*              */
	/* Constructors */
	/*              */

	public TagDiscovery(TagDiscoveryBias tagDiscoveryBias) {
		this.tagDiscoveryBias = tagDiscoveryBias;

		this.tagDiscoveryNumFrequencyBands = tagDiscoveryBias.numFrequencyBands;

	}

	TagDiscovery(int tagDiscoveryBiasID) {
		this.tagDiscoveryBias = new TagDiscoveryBias();
		this.tagDiscoveryBias.id = tagDiscoveryBiasID;

	}

	/*                   */
	/* Thread Management */
	/*                   */

	boolean threadStopped;

	void pauseIfNecessary() {
		while (pauseThread) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	boolean pauseThread = false;
	boolean stopThread = false;

	public void pauseThread() {
		pauseThread = true;
	}

	public void resumeThread() {
		pauseThread = false;
	}

	public void stopThread() {
		stopThread = true;
	}

	/*                              */
	/* Class Methods for Processing */
	/*                              */

	/** Initialize some data to prepare for running.
	*
	* Namely loads the audio files. 
	* @param project_id Database ID of the Project in which we're running.
	* @param connection An opened database connection.
	*/

	public void runTagDiscoveryStart(int project_id, Connection connection) {
		System.out.println("###  runTagDiscoveryStart  ============");

		tagDiscoveryNumFrequencyBands = tagDiscoveryBias.numFrequencyBands;

		// get project audio files from DB
//		if (!useAudioFilesCache) {

			// which fields (metadata) to retrieve from the AudioFiles
			HashMap<String, Boolean> fieldNames = new HashMap<String, Boolean>();
			fieldNames.put("active", true);
			fieldNames.put("durationInSeconds", true);
			fieldNames.put("bitsPerSample", true);
			fieldNames.put("nBlockAlign", true);
			fieldNames.put("dataStartIndex", true);
			fieldNames.put("sampleRate", true);
			fieldNames.put("numChannels", true);
			fieldNames.put("dataSize", true);

			tagDiscoveryNesterAudioFiles = NesterProject.getNesterProjectAudioFiles(project_id, false, fieldNames, connection);
//			IO.writeObject(audioFilesFilePath, tagDiscoveryNesterAudioFiles);
//		} else {
//			tagDiscoveryNesterAudioFiles = (Vector<NesterAudioFile>) IO.readObject(audioFilesFilePath);
//		}

		tagDiscoveryNumAudioFiles = tagDiscoveryNesterAudioFiles.size();

		// setupSpectralDataMemoryMaps();
	}

	public void runEnd() {
	}


	public void setupSpectralDataMemoryMaps() {
		System.out.println("###  setupSpectralDataMemoryMaps  ============");
		tagDiscoveryAllDataSpectraFilePath = tagDiscoveryBias.getSpectraMemoryMapFilePath();

		System.out.println("### lastAllDataSpectraFilePath = " + lastAllDataSpectraFilePath);
		System.out.println("### allDataSpectraFilePath = " + tagDiscoveryAllDataSpectraFilePath);

		tagDiscoveryNesterAudioFiles = (Vector<NesterAudioFile>) IO.readObject(tagDiscoveryBias.getSpectraMemoryMapFilePath() + ".nesterAudioFiles");

		// if (project == null)
		// project = serviceHead.getNesterProject(tagDiscoveryBias.project_id);
		//
		// nesterAudioFiles = serviceHead.getNesterProjectAudioFiles(project.project_id);

		tagDiscoveryNumAudioFiles = tagDiscoveryNesterAudioFiles.size();

		try {

			long byteIndex = 0;
			// Get a new FileChannel object for reading and writing the existing file
			FileChannel rwCh = new RandomAccessFile(tagDiscoveryAllDataSpectraFilePath, "r").getChannel();
			// Map entire file to memory and close the channel
			long fileSize = rwCh.size();
			// System.out.println("fileSize = " + fileSize);
			ByteBuffer buf = rwCh.map(FileChannel.MapMode.READ_ONLY, 0, ArloSettings.sizeOfInt);
			tagDiscoveryNumAudioFiles = buf.getInt();
			byteIndex += ArloSettings.sizeOfInt;
			System.out.println("### Creating audioFileSpectraByteOffsets - numAudioFiles: " + tagDiscoveryNumAudioFiles);
			tagDiscoveryAudioFileSpectraByteOffsets = new long[tagDiscoveryNumAudioFiles];
			tagDiscoveryAudioFileSpectraNumBytes = new long[tagDiscoveryNumAudioFiles];
			tagDiscoveryAudioFileSpectraNumFrames = new long[tagDiscoveryNumAudioFiles];

			buf = rwCh.map(FileChannel.MapMode.READ_ONLY, byteIndex, tagDiscoveryNumAudioFiles * ArloSettings.sizeOfLong);

			buf.position(0);
			for (int i = 0; i < tagDiscoveryNumAudioFiles; i++) {
				tagDiscoveryAudioFileSpectraByteOffsets[i] = buf.getLong();
				byteIndex += ArloSettings.sizeOfLong;
			}
			buf.position(0);

			buf = rwCh.map(FileChannel.MapMode.READ_ONLY, byteIndex, tagDiscoveryNumAudioFiles * ArloSettings.sizeOfLong);

			buf.position(0);
			for (int i = 0; i < tagDiscoveryNumAudioFiles; i++) {
				tagDiscoveryAudioFileSpectraNumBytes[i] = buf.getLong();
				tagDiscoveryAudioFileSpectraNumFrames[i] = tagDiscoveryAudioFileSpectraNumBytes[i] / ArloSettings.sizeOfInt / tagDiscoveryNumFrequencyBands;
				byteIndex += ArloSettings.sizeOfLong;
			}
			buf.position(0);

		} catch (Exception e) {

			e.printStackTrace();

		}

		if (false) {
			System.out.println("numAudioFiles = " + tagDiscoveryNumAudioFiles);
			for (int i = 0; i < tagDiscoveryNumAudioFiles; i++) {
				System.out.println(tagDiscoveryAudioFileSpectraByteOffsets[i] + "\t" + tagDiscoveryAudioFileSpectraNumBytes[i]);
			}
		}

	}

//	void loadExampleSpectraData(int numExamples, int[] exampleAudioFileIndices, int[] exampleStartFrameIndices, int[] exampleNumFrames, int[] exampleSpectraStartFreqIndex,
//			int[] exampleSpectraEndFreqIndex) {
//		System.out.println("###  loadExampleSpectraData  ============");
//		/********************************************/
//		/* load spectra data for each random window */
//		/********************************************/
//		tagDiscoveryAllExampleSpectra = new int[numExamples][];
//		tagDiscoveryAllExampleSpectraNumFrames = new int[numExamples];
//		tagDiscoveryAllExampleSpectraStartFreqIndex = new int[numExamples];
//		tagDiscoveryAllExampleSpectraEndFreqIndex = new int[numExamples];
//		System.out.println("### numExamples " + numExamples);
//		try {
//			FileChannel rwCh = new RandomAccessFile(tagDiscoveryAllDataSpectraFilePath, "r").getChannel();
//			long fileSize = rwCh.size();
//			// System.out.println("fileSize = " + fileSize);
//
//			for (int exampleIndex = 0; exampleIndex < numExamples; exampleIndex++) {
//
//				int windowNumFrames = exampleNumFrames[exampleIndex];
//
//				int numExampleValues = windowNumFrames * tagDiscoveryNumFrequencyBands;
//				int bytesPerFrame = (int) (tagDiscoveryNumFrequencyBands * ArloSettings.sizeOfInt);
//
//				tagDiscoveryAllExampleSpectra[exampleIndex] = new int[numExampleValues];
//
//				int audioFileIndex = exampleAudioFileIndices[exampleIndex];
//				long exampleStartFrameIndex = exampleStartFrameIndices[exampleIndex];
//
//				System.out.println("### audioFileIndex " + audioFileIndex + " exampleStart " + exampleStartFrameIndex + "bytesPerFrame" + bytesPerFrame);
//
//				long offsetSize = tagDiscoveryAudioFileSpectraByteOffsets[audioFileIndex] + exampleStartFrameIndex * bytesPerFrame;
//
//				System.out.println();
//				System.out.println("loadWindowSpectraData audioFileIndex         = " + audioFileIndex);
//				System.out.println("loadWindowSpectraData exampleIndex           = " + exampleIndex);
//				System.out.println("loadWindowSpectraData exampleStartFrameIndex = " + exampleStartFrameIndex);
//				System.out.println("loadWindowSpectraData offsetSize             = " + offsetSize);
//				System.out.println("loadWindowSpectraData fileSize               = " + fileSize);
//				System.out.println("loadWindowSpectraData numExampleValues       = " + numExampleValues);
//
//				ByteBuffer buf = null;
//				try {
//					buf = rwCh.map(FileChannel.MapMode.READ_ONLY, offsetSize, numExampleValues * ArloSettings.sizeOfInt);
//				} catch (Exception e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//					System.exit(1);
//				}
//
//				for (int i = 0; i < numExampleValues; i++) {
//					tagDiscoveryAllExampleSpectra[exampleIndex][i] = buf.getInt();
//				}
//				tagDiscoveryAllExampleSpectraNumFrames[exampleIndex] = windowNumFrames;
//				tagDiscoveryAllExampleSpectraStartFreqIndex[exampleIndex] = exampleSpectraStartFreqIndex[exampleIndex];
//				tagDiscoveryAllExampleSpectraEndFreqIndex[exampleIndex] = exampleSpectraEndFreqIndex[exampleIndex];
//
//			}
//			rwCh.close();
//		} catch (FileNotFoundException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		} catch (IOException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//	}

	void computeExampleSpectraData(int numExamples, int[] exampleAudioFileIndices, double[] exampleStartTimes, double[] exampleEndTimes, int[] exampleSpectraStartFreqIndex,
			int[] exampleSpectraEndFreqIndex) {

		CreateSpectraImage createSpectraImage = new CreateSpectraImage();

		System.out.println("###  computeExampleSpectraData  ============");

		/********************************************/
		/* load spectra data for each random window */
		/********************************************/
		tagDiscoveryAllExampleSpectra = new int[numExamples][];
		tagDiscoveryAllExampleSpectraNumFrames = new int[numExamples];
		tagDiscoveryAllExampleSpectraStartFreqIndex = new int[numExamples];
		tagDiscoveryAllExampleSpectraEndFreqIndex = new int[numExamples];
		System.out.println("### numExamples " + numExamples);
		// try {
		// FileChannel rwCh = new RandomAccessFile(tagDiscoveryAllDataSpectraFilePath, "r").getChannel();
		// long fileSize = rwCh.size();
		// System.out.println("fileSize = " + fileSize);

		for (int exampleIndex = 0; exampleIndex < numExamples; exampleIndex++) {
			
			System.out.println("computeExampleSpectraData:  exampleIndex = " + exampleIndex);
			System.out.println("computeExampleSpectraData:  exampleStartTimes[exampleIndex] = " + exampleStartTimes[exampleIndex]);
			System.out.println("computeExampleSpectraData:  exampleEndTimes[exampleIndex]   = " + exampleEndTimes[exampleIndex]);

			// public int[][] getNesterAudioFileSpectraData(NesterAudioFile nesterAudioFile, double startTime, double endTime, int numTimeFramesPerSecond, int numFrequencyBands,
			// double dampingRatio, double minFrequency, double maxFrequency)

			{

				int[][] spectra = createSpectraImage.getNesterAudioFileSpectraData(tagDiscoveryNesterAudioFiles.get(exampleAudioFileIndices[exampleIndex]),
						exampleStartTimes[exampleIndex], exampleEndTimes[exampleIndex], (int) tagDiscoveryBias.numTimeFramesPerSecond, tagDiscoveryBias.numFrequencyBands,
						tagDiscoveryBias.dampingRatio, tagDiscoveryBias.minFrequency, tagDiscoveryBias.maxFrequency);

				int numFrames = spectra.length;
				System.out.println("computeExampleSpectraData:  numFrames = " + numFrames);

				int numExampleValues = numFrames * tagDiscoveryNumFrequencyBands;

				tagDiscoveryAllExampleSpectra[exampleIndex] = new int[numExampleValues];
				tagDiscoveryAllExampleSpectraNumFrames[exampleIndex] = numFrames;
				tagDiscoveryAllExampleSpectraStartFreqIndex[exampleIndex] = exampleSpectraStartFreqIndex[exampleIndex];
				tagDiscoveryAllExampleSpectraEndFreqIndex[exampleIndex] = exampleSpectraEndFreqIndex[exampleIndex];

				int index = 0;
				for (int frameIndex = 0; frameIndex < numFrames; frameIndex++) {
					for (int frequencyBandIndex = 0; frequencyBandIndex < tagDiscoveryNumFrequencyBands; frequencyBandIndex++) {

						if ((frequencyBandIndex >= exampleSpectraStartFreqIndex[exampleIndex]) && (frequencyBandIndex <= exampleSpectraEndFreqIndex[exampleIndex])) {
							tagDiscoveryAllExampleSpectra[exampleIndex][index++] = spectra[frameIndex][frequencyBandIndex];
						} else {
							tagDiscoveryAllExampleSpectra[exampleIndex][index++] = Integer.MIN_VALUE;
						}
					}
				}

			}

			// int windowNumFrames = exampleNumFrames[exampleIndex];
			//
			// int numExampleValues = windowNumFrames * tagDiscoveryNumFrequencyBands;
			//
			//
			// // createSpectraImage.getNesterAudioFileSpectraData(tagDiscoveryNesterAudioFiles.get(exampleAudioFileIndices[exampleIndex]),
			// //
			// // startTime, endTime,
			// // numTimeFramesPerSecond, numFrequencyBands, dampingRatio, minFrequency, maxFrequency);
			//
			// int bytesPerFrame = (int) (tagDiscoveryNumFrequencyBands * ArloSettings.sizeOfInt);
			//
			// tagDiscoveryAllExampleSpectra[exampleIndex] = new int[numExampleValues];
			//
			// int audioFileIndex = exampleAudioFileIndices[exampleIndex];
			// long exampleStartFrameIndex = exampleStartFrameIndices[exampleIndex];
			//
			// System.out.println("### audioFileIndex " + audioFileIndex + " exampleStart " + exampleStartFrameIndex + "bytesPerFrame" + bytesPerFrame);
			//
			// long offsetSize = tagDiscoveryAudioFileSpectraByteOffsets[audioFileIndex] + exampleStartFrameIndex * bytesPerFrame;
			//
			// System.out.println();
			// System.out.println("loadWindowSpectraData audioFileIndex         = " + audioFileIndex);
			// System.out.println("loadWindowSpectraData exampleIndex           = " + exampleIndex);
			// System.out.println("loadWindowSpectraData exampleStartFrameIndex = " + exampleStartFrameIndex);
			// System.out.println("loadWindowSpectraData offsetSize             = " + offsetSize);
			// System.out.println("loadWindowSpectraData fileSize               = " + fileSize);
			// System.out.println("loadWindowSpectraData numExampleValues       = " + numExampleValues);
			//
			// ByteBuffer buf = null;
			// try {
			// buf = rwCh.map(FileChannel.MapMode.READ_ONLY, offsetSize, numExampleValues * ArloSettings.sizeOfInt);
			// } catch (Exception e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// System.exit(1);
			// }
			//
			// for (int i = 0; i < numExampleValues; i++) {
			// tagDiscoveryAllExampleSpectra[exampleIndex][i] = buf.getInt();
			// }
			// tagDiscoveryAllExampleSpectraNumFrames[exampleIndex] = windowNumFrames;
			// tagDiscoveryAllExampleSpectraStartFreqIndex[exampleIndex] = exampleSpectraStartFreqIndex[exampleIndex];
			// tagDiscoveryAllExampleSpectraEndFreqIndex[exampleIndex] = exampleSpectraEndFreqIndex[exampleIndex];

		}
		// rwCh.close();
		// } catch (FileNotFoundException e1) {
		// // TODO Auto-generated catch block
		// e1.printStackTrace();
		// } catch (IOException e1) {
		// // TODO Auto-generated catch block
		// e1.printStackTrace();
		// }
	}

	void computeExampleSpectraAveragePitch(int numExamples, int[] exampleAudioFileIndices, double[] exampleStartTimes, double[] exampleEndTimes,
			int[] exampleSpectraStartFreqIndex, int[] exampleSpectraEndFreqIndex, NesterTag[] exampleTags, HashMap<Integer, String> tagClassIDToName)
			throws IOException {

		CreateSpectraImage createSpectraImage = new CreateSpectraImage();

		BandPass bandPass = new BandPass();
		bandPass.setNumBands(tagDiscoveryBias.numFrequencyBands);
		bandPass.setMinBandFrequency(tagDiscoveryBias.minFrequency);
		bandPass.setMaxBandFrequency(tagDiscoveryBias.maxFrequency);
		bandPass.initialize();

		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(new File("/home/arlo/pitch.xls")));

		String string1 = "" + //
				"tag.audioFile_id" + "\t" + //
				"tag.id" + "\t" + //
				"tag.startTime" + "\t" + //
				"tag.endTime" + "\t" + //
				"tag.parentTag_id" + "\t" + //
				"tag.tagClass_id" + "\t" + //
				"tag.machineTagged" + "\t" + //
				"tag.userTagged" + "\t" + //
				"tag.strength" + "\t" + //
				"averageMaxEnergyFrequency" + "\t" +//
				"tag.creationDate" + "\t" + //
				"tag.audioFileAlias";

		bufferedWriter.write(string1);
		bufferedWriter.write('\n');

		for (int exampleIndex = 0; exampleIndex < numExamples; exampleIndex++) {

			System.out.println("#################################################### exampleIndex = " + exampleIndex);

			// public int[][] getNesterAudioFileSpectraData(NesterAudioFile nesterAudioFile, double startTime, double endTime, int numTimeFramesPerSecond, int numFrequencyBands,
			// double dampingRatio, double minFrequency, double maxFrequency)

			{

				int[][] spectra = createSpectraImage.getNesterAudioFileSpectraData(tagDiscoveryNesterAudioFiles.get(exampleAudioFileIndices[exampleIndex]),
						exampleStartTimes[exampleIndex], exampleEndTimes[exampleIndex], (int) tagDiscoveryBias.numTimeFramesPerSecond, tagDiscoveryBias.numFrequencyBands,
						tagDiscoveryBias.dampingRatio, tagDiscoveryBias.minFrequency, tagDiscoveryBias.maxFrequency);

				int numFrames = spectra.length;
				System.out.println("computeExampleSpectraData:  numFrames = " + numFrames);

				int numExampleValues = numFrames * tagDiscoveryNumFrequencyBands;

				double maxEnergyFrequencyIndexSum = 0;
				for (int frameIndex = 0; frameIndex < numFrames; frameIndex++) {

					int maxEnergy = Integer.MIN_VALUE;
					int maxEnergyFrequencyIndex = -1;

					for (int frequencyBandIndex = 0; frequencyBandIndex < tagDiscoveryNumFrequencyBands; frequencyBandIndex++) {
						int energy = spectra[frameIndex][frequencyBandIndex];
						if (energy > maxEnergy) {
							maxEnergy = energy;
							maxEnergyFrequencyIndex = frequencyBandIndex;
						}
					}

					// System.out.println("#################################################### maxEnergyFrequencyIndex = " + maxEnergyFrequencyIndex);

					maxEnergyFrequencyIndexSum += maxEnergyFrequencyIndex;

				}

				double averageMaxEnergyFrequencyIndex = maxEnergyFrequencyIndexSum / numFrames;

				float averageMaxEnergyFrequency = (float) bandPass.bandFrequencies[(int) averageMaxEnergyFrequencyIndex];

				NesterTag tag = exampleTags[exampleIndex];

				// System.out.println("#################################################### averageMaxEnergyFrequencyIndex = " + averageMaxEnergyFrequencyIndex);
				// System.out.println("#################################################### averageMaxEnergyFrequency      = " + averageMaxEnergyFrequency);
				// System.out.println("#################################################### tag.id           = " + tag.id);
				// System.out.println("#################################################### tag.parentTag_id = " + tag.parentTag_id);
				// System.out.println("#################################################### tag.Class_id     = " + tag.tagClass_id);
				// System.out.println("#################################################### tag.strength     = " + tag.strength);

				String string2 = "" + //
						tag.audioFile_id + "\t" + //
						tag.id + "\t" + //
						(float) tag.startTime + "\t" + //
						(float) tag.endTime + "\t" + //
						tag.parentTag_id + "\t" + //
						tagClassIDToName.get(tag.tagClass_id) + "\t" + //
						tag.machineTagged + "\t" + //
						tag.userTagged + "\t" + //
						(float) tag.strength + "\t" + //
						averageMaxEnergyFrequency + "\t" + //
						tag.creationDate + "\t" + //
						tag.nesterAudioFile.alias;

				bufferedWriter.write(string2);
				bufferedWriter.write('\n');
				bufferedWriter.flush();

			}

		}
	}

	void saveExampleSpectraData(String dataPathName) {
		System.out.println("###  saveExampleSpectraData  ============");
		ArrayList<Object> data = new ArrayList<Object>();

		data.add(tagDiscoveryAllExampleSpectra);
		data.add(tagDiscoveryAllExampleSpectraNumFrames);

		IO.writeObject(dataPathName, data);

	}

	public int[] loadAudioFileSpectraData(int audioFileIndex, long startFrame, long numFrames, boolean allocateMemory) {
		System.out.println("###  loadAudioFileSpectraData  ============");
		try {
			FileChannel rwCh = new RandomAccessFile(tagDiscoveryAllDataSpectraFilePath, "r").getChannel();
			long fileSize = rwCh.size();
			// System.out.println("fileSize = " + fileSize);

			long numExampleValues = numFrames * tagDiscoveryNumFrequencyBands;
			int bytesPerFrame = (int) (tagDiscoveryNumFrequencyBands * ArloSettings.sizeOfInt);

			if (allocateMemory)
				tagDiscoveryAudioFileSpectra = new int[(int) numExampleValues];

			long offsetSize = tagDiscoveryAudioFileSpectraByteOffsets[audioFileIndex] + startFrame * bytesPerFrame;

			ByteBuffer buf = rwCh.map(FileChannel.MapMode.READ_ONLY, offsetSize, numExampleValues * ArloSettings.sizeOfInt);

			for (int i = 0; i < numExampleValues; i++) {
				tagDiscoveryAudioFileSpectra[i] = buf.getInt();
			}
			tagDiscoveryAudioFileNumFrames = numFrames;

			rwCh.close();
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		return tagDiscoveryAudioFileSpectra;
	}

	public int[] computeAudioFileSpectraData(NesterAudioFile audioFile, double startTime, double endTime) {

		System.out.println("###  computeAudioFileSpectraData  ============");

		CreateSpectraImage createSpectraImage = new CreateSpectraImage();

		int[][] spectra = createSpectraImage.getNesterAudioFileSpectraData(audioFile, startTime, endTime, (int) tagDiscoveryBias.numTimeFramesPerSecond,
				tagDiscoveryBias.numFrequencyBands, tagDiscoveryBias.dampingRatio, tagDiscoveryBias.minFrequency, tagDiscoveryBias.maxFrequency);

		int numFrames = spectra.length;
		System.out.println("computeExampleSpectraData:  numFrames = " + numFrames);

		tagDiscoveryAudioFileNumFrames = numFrames;
		int numExampleValues = (int) tagDiscoveryAudioFileNumFrames * tagDiscoveryNumFrequencyBands;
		tagDiscoveryAudioFileSpectra = new int[numExampleValues];
		int index = 0;
		for (int frameIndex = 0; frameIndex < numFrames; frameIndex++) {
			for (int frequencyBandIndex = 0; frequencyBandIndex < tagDiscoveryNumFrequencyBands; frequencyBandIndex++) {
				tagDiscoveryAudioFileSpectra[index++] = spectra[frameIndex][frequencyBandIndex];
			}
		}

		return tagDiscoveryAudioFileSpectra;
	}

}
