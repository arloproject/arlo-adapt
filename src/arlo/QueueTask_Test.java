package arlo;

import java.util.Vector;

import arlo.NesterUser;
import arlo.QueueTask;
import arlo.NesterJob;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Time;
import java.sql.SQLException;
import java.util.HashMap;


public class QueueTask_Test extends QueueTask {
	static final private String taskName = "Test";

	public boolean runQueue(
		Vector<NesterUser> restrictedUsers)
	{
		return super._runQueue(taskName, restrictedUsers);
	}

	void runJob(NesterJob job) {
		System.out.println("QueueTask_Test Found Job: " + job.id + " - sleeping 1 second");

		Connection connection = QueueTask.openDatabaseConnection();

		try {

			System.out.println("====== Got Job");

			// print out parameter list:
			System.out.println("=== All Job Parameters ===");
			for (String k : job.jobParameters.keySet()) {
				System.out.println("  key = " + k);
				Vector<String> values = new Vector<String>();
				for (String value : job.jobParameters.get(k)) {
					System.out.println("    " + value);
				}

			}


			// Get Params

			// Print Params
			
//			HashMap<String, String> params = new HashMap<String, String>();
//			params.put("1", "val1");
//			params.put("2", "val2");
//			params.put("3", "val3");
//
//			if (job.SaveJobParameters(params, connection)) {
//				System.out.println("====== Params :)");
//			} else {
//				System.out.println("====== Params :(");
//			}

		} catch (Exception e) {
			e.printStackTrace();

		}

		setJobStatusComplete(job.id, connection);
		
	}
}

