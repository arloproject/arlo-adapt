package arlo;

import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Random;
import java.util.Vector;


import adapt.IO;
import adapt.SimpleTable;

// disabled with Cassandra updates
//public class TagAnalysis extends TagDiscovery {
//
//	TagAnalysis(ServiceHead serviceHead, int tagAnalysisBiasID) {
//
//		super(serviceHead, tagAnalysisBiasID);
//
//	}
//
//	public void run() {
//
//		BufferedWriter bufferedWriter = null;
//		try {
//			bufferedWriter = new BufferedWriter(new FileWriter(new File("/data/tags.csv")));
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		Boolean shortExportFormat = true;
//
//		System.out.println("#############################");
//		System.out.println("### starting Tag Analysis ###");
//		System.out.println("#############################");
//
//		// TODO *** serviceHead.connection  <-- This is probably REAL BAD, using the serviceHead connection
//
//		// get bias from DB using bias ID
//		NesterTagAnalysisBias nesterTagAnalysisBias = new NesterTagAnalysisBias();
//		nesterTagAnalysisBias.getNesterTagAnalysisBias(this.tagDiscoveryBiasID, this.serviceHead.connection);
//// TODO HERE  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//		tagDiscoveryBias = nesterTagAnalysisBias;
//
//		NesterUser nesterUser = serviceHead.getNesterUser(nesterTagAnalysisBias.user_id);
//
//		// get project from DB using project ID from bias
//		project = serviceHead.getNesterProject(nesterTagAnalysisBias.project_id);
//
//		System.out.println("### nesterTagAnalysisBias = " + nesterTagAnalysisBias);
//		System.out.println("### nesterUser.name = " + nesterUser.name);
//		System.out.println("### project.user_id = " + project.user_id);
//		System.out.println("### project.library_id = " + nesterTagAnalysisBias.library_id);
//		System.out.println("### nesterTagAnalysisBias.freqAveragingWindowWidth = " + nesterTagAnalysisBias.freqAveragingWindowWidth);
//		System.out.println("### nesterTagAnalysisBias.numFramesToAverageNoise = " + nesterTagAnalysisBias.numFramesToAverageNoise);
//		System.out.println("### nesterTagAnalysisBias.updateTagPositions = " + nesterTagAnalysisBias.updateTagPositions);
//
//		runTagDiscoveryStart(nesterTagAnalysisBias.library_id);
//
//		// get project audio files from DB
//		tagDiscoveryNesterAudioFiles = serviceHead.getNesterLibraryAudioFiles(nesterTagAnalysisBias.library_id, true, null);
//		System.out.println("### nesterAudioFiles.size() = " + tagDiscoveryNesterAudioFiles.size());
//		tagDiscoveryNumAudioFiles = tagDiscoveryNesterAudioFiles.size();
//
//		Vector<NesterTagClass> allTagSetClasses = serviceHead.getNesterTagSetTagClasses(project.tagSet_id);
//
//		HashMap<Integer, String> tagClassIDToName = new HashMap<Integer, String>();
//
//		for (Iterator<NesterTagClass> iterator = allTagSetClasses.iterator(); iterator.hasNext();) {
//			NesterTagClass nesterTagClass = iterator.next();
//			tagClassIDToName.put(nesterTagClass.id, nesterTagClass.className);
//		}
//
//		// get all tag examples from DB
//
//		Vector<NesterTagExample> allTagExamples = serviceHead.getNesterTagSetTagExamples(project.tagSet_id);
//		System.out.println("### allTagExamples.size() = " + allTagExamples.size());
//
//		System.out.println("### end getNesterTagSetTagExamples ###");
//
//		System.out.println("### (new) allTagExamples.size() = " + allTagExamples.size());
//
//		Vector<NesterTagExample> newAllTagExamples = new Vector<NesterTagExample>();
//
//		for (int audioFileindex = 0; audioFileindex < tagDiscoveryNumAudioFiles; audioFileindex++) {
//
//			NesterAudioFile nesterAudioFile = tagDiscoveryNesterAudioFiles.get(audioFileindex);
//
//			for (int userTagIndex = 0; userTagIndex < allTagExamples.size(); userTagIndex++) {
//
//				NesterTagExample nesterTagExample = allTagExamples.get(userTagIndex);
//
//				// filter out tag examples with no class tags
//				if (nesterTagExample.tagVector.size() == 0) {
//					if (false) { // !!!
//						if (true)
//							System.out.println("Warning!  (nesterTagExample.tagVector.size() == 0), nesterTagExample:" + nesterTagExample);
//						// System.exit(1);
//						if (true)
//							System.out.println("Deleting nesterTagExample:" + nesterTagExample);
//						serviceHead.deleteTagExample(nesterTagExample);
//					}
//					continue;
//				}
//				if (nesterTagExample.tagVector.size() != 1) {
//					System.out.println("Warning!  (nesterTagExample.tagVector.size() != 1), nesterTagExample:" + nesterTagExample);
//					// System.exit(1);
//					// if (false)
//					// System.out.println("Deleting nesterTagExample:" + nesterTagExample);
//					// serviceHead.deleteTagExample(nesterTagExample);
//					// continue;
//				}
//
//				NesterTag firstTag = nesterTagExample.tagVector.get(0);
//
//				// if (firstTag.strength != 1.0) {
//				// continue;
//				// }
//
//				if (nesterTagExample.endTime > nesterAudioFile.wavFileMetaData.durationInSeconds) {
//					System.out.println("Warning!  (nesterTagExample.endTime >  nesterAudioFile.wavFileMetaData.durationInSeconds)");
//					continue;
//				}
//
//				if (nesterTagExample.audioFile_id == nesterAudioFile.id) {
//
//					nesterTagExample.tag = firstTag;
//					nesterTagExample.audioFileIndex = audioFileindex;
//					nesterTagExample.nesterAudioFile = nesterAudioFile;
//
//					newAllTagExamples.add(nesterTagExample);
//				}
//
//			}
//
//		}
//
//		allTagExamples = newAllTagExamples;
//
//        // TODO Waring 'local variable hides a field'
//		long audioFileNumFrames = -1;
//		NesterAudioFile nesterAudioFile = null;
//
//		boolean firstTime = true;
//
//		BandPass bandPass = new BandPass();
//
//		bandPass.setNumBands(nesterTagAnalysisBias.numFrequencyBands);
//		bandPass.setMinBandFrequency(nesterTagAnalysisBias.minFrequency);
//		bandPass.setMaxBandFrequency(nesterTagAnalysisBias.maxFrequency);
//		bandPass.initialize();
//
//		for (int userTagIndex = 0; userTagIndex < allTagExamples.size(); userTagIndex++) {
//
//			if (stopThread) {
//				threadStopped = true;
//				return;
//			}
//
//			pauseIfNecessary();
//
//			NesterTagExample nesterTagExample = allTagExamples.get(userTagIndex);
//
//			int audioFileIndex = nesterTagExample.audioFileIndex;
//
//			int peakIntensitySum = Integer.MIN_VALUE;
//			int peakIntensityTimeIndex = -1;
//
//			int freqAveragingWindowWidth = (int) nesterTagAnalysisBias.freqAveragingWindowWidth;
//
//			int overallHighestFrequencyIntensitySum = Integer.MIN_VALUE;
//			int overallHighestFrequencyIndex = -1;
//
//			nesterAudioFile = tagDiscoveryNesterAudioFiles.get(audioFileIndex);
//			audioFileNumFrames = tagDiscoveryAudioFileSpectraNumFrames[audioFileIndex];
//
//			int tagExampleStartFrame = (int) (nesterTagExample.startTime * nesterTagAnalysisBias.numTimeFramesPerSecond);
//			int tagExampleEndFrame = (int) (nesterTagExample.endTime * nesterTagAnalysisBias.numTimeFramesPerSecond);
//			int tagExampleNumFrames = tagExampleEndFrame - tagExampleStartFrame + 1;
//
//			int peakIntensityFreqIndex = -1;
//
//			loadAudioFileSpectraData(audioFileIndex, tagExampleStartFrame, tagExampleNumFrames, true);
//
//			for (int tagTimeIndex = 0; tagTimeIndex < tagExampleNumFrames; tagTimeIndex++) {
//
//				int timeSliceHighestFrequencyIntensitySum = Integer.MIN_VALUE;
//				int timeSliceHighestFrequencyIndex = -1;
//
//				for (int frequencyIndex = 0; frequencyIndex < tagDiscoveryNumFrequencyBands - (freqAveragingWindowWidth - 1); frequencyIndex++) {
//
//					int intensitySum = 0;
//					for (int frequencyIndex2 = frequencyIndex; frequencyIndex2 < frequencyIndex + freqAveragingWindowWidth; frequencyIndex2++) {
//						intensitySum += tagDiscoveryAudioFileSpectra[tagTimeIndex * tagDiscoveryNumFrequencyBands + frequencyIndex2];
//					}
//
//					if (intensitySum > peakIntensitySum) {
//						peakIntensitySum = intensitySum;
//						peakIntensityFreqIndex = frequencyIndex + freqAveragingWindowWidth / 2;
//						peakIntensityTimeIndex = tagTimeIndex;
//					}
//
//					if (intensitySum > timeSliceHighestFrequencyIntensitySum) {
//						timeSliceHighestFrequencyIntensitySum = intensitySum;
//						timeSliceHighestFrequencyIndex = frequencyIndex + freqAveragingWindowWidth / 2;
//					}
//				}
//
//				if (timeSliceHighestFrequencyIndex > overallHighestFrequencyIndex) {
//					overallHighestFrequencyIndex = timeSliceHighestFrequencyIndex;
//					overallHighestFrequencyIntensitySum = timeSliceHighestFrequencyIntensitySum;
//				}
//
//			}
//
//			int peakIntensityAverageIntensity = peakIntensitySum / freqAveragingWindowWidth;
//			int highestFrequencyAverageIntensity = overallHighestFrequencyIntensitySum / freqAveragingWindowWidth;
//
//			double peakIntensityFrequency = bandPass.bandFrequencies[peakIntensityFreqIndex];
//			double highestFrequency = bandPass.bandFrequencies[overallHighestFrequencyIndex];
//
//			int numFramesToAverageNoise = nesterTagAnalysisBias.numFramesToAverageNoise;
//			int startTimeIndex = -1;
//
//			int offset = peakIntensityTimeIndex - tagExampleNumFrames / 2;
//			int newTagExampleStartFrame = tagExampleStartFrame + offset;
//			int newTagExampleEndFrame = tagExampleEndFrame + offset;
//
//			if (newTagExampleStartFrame < 0) {
//				System.out.println("Warning!  (newTagExampleStartFrame < 0) , nesterTagExample:" + nesterTagExample);
//				continue;
//			}
//
//			startTimeIndex = (int) tagExampleStartFrame - numFramesToAverageNoise;
//
//			if (startTimeIndex < 0) {
//				startTimeIndex = 0;
//			}
//
//			int noiseStartFrame = newTagExampleStartFrame - numFramesToAverageNoise;
//
//			if (noiseStartFrame < 0) {
//				System.out.println("Warning! (noiseStartFrame < 0), nesterTagExample:" + nesterTagExample);
//				continue;
//			}
//
//			loadAudioFileSpectraData(audioFileIndex, noiseStartFrame, numFramesToAverageNoise, true);
//
//			double highestFrequencyNoiseValue = Double.NaN;
//			double peakIntensityNoiseValue = Double.NaN;
//
//			{
//				long valueSum = 0;
//
//				for (int timeIndex = 0; timeIndex < numFramesToAverageNoise; timeIndex++) {
//
//					int startFrequencyIndex = overallHighestFrequencyIndex - freqAveragingWindowWidth / 2;
//					int endFrequencyIndex = startFrequencyIndex + freqAveragingWindowWidth;
//
//					for (int frequencyIndex2 = startFrequencyIndex; frequencyIndex2 < endFrequencyIndex; frequencyIndex2++) {
//						valueSum += tagDiscoveryAudioFileSpectra[timeIndex * tagDiscoveryNumFrequencyBands + frequencyIndex2];
//					}
//				}
//				highestFrequencyNoiseValue = (double) valueSum / freqAveragingWindowWidth / numFramesToAverageNoise;
//			}
//			{
//				long valueSum = 0;
//
//				for (int timeIndex = 0; timeIndex < numFramesToAverageNoise; timeIndex++) {
//
//					int startFrequencyIndex = peakIntensityFreqIndex - freqAveragingWindowWidth / 2;
//					int endFrequencyIndex = startFrequencyIndex + freqAveragingWindowWidth;
//
//					for (int frequencyIndex2 = startFrequencyIndex; frequencyIndex2 < endFrequencyIndex; frequencyIndex2++) {
//						valueSum += tagDiscoveryAudioFileSpectra[timeIndex * tagDiscoveryNumFrequencyBands + frequencyIndex2];
//					}
//				}
//				peakIntensityNoiseValue = (double) valueSum / freqAveragingWindowWidth / numFramesToAverageNoise;
//			}
//
//			NesterTagExample newTagExample = nesterTagExample;
//
//			double newStartTime = newTagExampleStartFrame / tagDiscoveryBias.numTimeFramesPerSecond;
//			double newEndTime = newTagExampleEndFrame / tagDiscoveryBias.numTimeFramesPerSecond;
//
//			if (firstTime) {
//
//				String textString;
//				if (shortExportFormat) {
//					textString = "TT,UT,F2,I2,N2,I2-N2,tagClassName,tagClassID,tagID,parentTagID,audioFilePath";
//				} else {
//					textString = "TT,UT,T1,Ts,Te,Td,F1,I1,N1,I1-N1,F2,I2,N2,I2-N2,tagClassName,tagClassID,tagID,parentTagID,audioFilePath";
//				}
//				textString += "\n";
//
//				try {
//					bufferedWriter.write(textString);
//				} catch (IOException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//
//				System.out.println();
//
//				firstTime = false;
//			}
//
//			double middleTime = (newStartTime + newEndTime) / 2.0;
//
//			long TT_long = nesterAudioFile.staticUserMetaData.startTime.getTime() + (int) (middleTime * 1000);
//			DateFormat TT_DateFormat = new SimpleDateFormat("HH:mm:ss.SSS");
//			String TT_string = TT_DateFormat.format(TT_long);
//
//			Date date = new Date(TT_long);
//
//			int hh = date.getHours();
//			int mm = date.getMinutes();
//			int ss = date.getSeconds();
//
//			double newTimeInSeconds = (hh * 3600 + mm * 60 + ss + 3600 * 6) % (24 * 3600) + (TT_long % 1000) / 1000.0;
//
//			String UT_string = String.format("%f", newTimeInSeconds);
//
//			String T1_string = String.format("%f", middleTime);
//
//			double Ts = newStartTime;
//			String Ts_string = String.format("%f", Ts);
//
//			double Te = newEndTime;
//			String Te_string = String.format("%f", Te);
//
//			double Td = Te - Ts;
//			String Td_string = String.format("%f", Td);
//
//			double F1 = highestFrequency;
//			String F1_string = String.format("%f", F1);
//
//			double I1 = 10 * Math.log10(highestFrequencyAverageIntensity);
//			String I1_string = String.format("%f", I1);
//
//			double N1 = 10 * Math.log10(highestFrequencyNoiseValue);
//			String N1_string = String.format("%f", N1);
//
//			double D1 = I1 - N1;
//			String D1_string = String.format("%f", D1);
//
//			double F2 = peakIntensityFrequency;
//			String F2_string = String.format("%f", F2);
//
//			double I2 = 10 * Math.log10(peakIntensityAverageIntensity);
//			String I2_string = String.format("%f", I2);
//
//			double N2 = 10 * Math.log10(peakIntensityNoiseValue);
//			String N2_string = String.format("%f", N2);
//
//			double D2 = I2 - N2;
//			String D2_string = String.format("%f", D2);
//
//			String tagClassName_string = String.format("%s", tagClassIDToName.get(newTagExample.tag.tagClass_id));
//
//			String tagClassID_string = String.format("%d", newTagExample.tag.tagClass_id);
//
//			String tagID_string = String.format("%d", newTagExample.tag.id);
//
//			// parentTag_id moved to NesterTag
//			// don't anticipate this code being used anymore
//			// so taking the lazy way out here... -TonyB
//			// String parentTagID_string = String.format("%d", newTagExample.parentTag_id);
//			String parentTagID_string = "error";
//
//			if (nesterTagAnalysisBias.updateTagPositions) {
//
//				nesterTagExample.startTime = newStartTime;
//				nesterTagExample.endTime = newEndTime;
//
//			}
//
//
//			serviceHead.updateExampleForTagAnalysis(nesterTagExample);
//
//			String textString = "";
//			textString += TT_string + ",";
//			textString += UT_string + ",";
//			if (!(shortExportFormat)) {
//
//				textString += T1_string + ",";
//				textString += Ts_string + ",";
//				textString += Te_string + ",";
//				textString += Td_string + ",";
//				textString += F1_string + ",";
//				textString += I1_string + ",";
//				textString += N1_string + ",";
//				textString += D1_string + ",";
//			}
//			textString += F2_string + ",";
//			textString += I2_string + ",";
//			textString += N2_string + ",";
//			textString += D2_string + ",";
//			textString += tagClassName_string + ",";
//			textString += tagClassID_string + ",";
//			textString += tagID_string + ",";
//			textString += parentTagID_string + ",";
//			textString += nesterAudioFile.relativeFilePath;
//			textString += "\n";
//
//			// String textString = "TAGFOUND" + "," + newTag.user_id + "," + newTag.id + "," + newTagExample.startTime + "," + newTagExample.endTime + "," + middleTime + ","
//			// + bestIntensityValue + "," + bestIntensityFrequency + "," + newTagExample.minFrequency + "," + newTagExample.maxFrequency + "," + newTag.tagClass_id + ","
//			// + candidateNesterAudioFile.alias + "," + candidateNesterAudioFile.relativeFilePath;
//			// textString += "\n";
//
//			try {
//				bufferedWriter.write(textString);
//				bufferedWriter.flush();
//
//				System.out.print(textString);
//
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//
//		}
//
//		try {
//			bufferedWriter.close();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//	}
//
//	void pauseIfNecessary() {
//		while (pauseThread) {
//			try {
//				Thread.sleep(1000);
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//	}
//
//	boolean pauseThread = false;
//	boolean stopThread = false;
//	boolean threadStopped = false;
//
//	public void pauseThread() {
//		pauseThread = true;
//	}
//
//	public void resumeThread() {
//		pauseThread = false;
//	}
//
//	public void stopThread() {
//		stopThread = true;
//		threadStopped = false;
//		while (!threadStopped) {
//			try {
//				Thread.sleep(1000);
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//	}
//}
