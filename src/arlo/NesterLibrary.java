package arlo;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Vector;
import java.util.HashMap;
import java.util.Iterator;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import com.goebl.david.Response;
import com.goebl.david.WebbException;
import org.json.JSONObject;


public class NesterLibrary {

	public int id;
	public int userId;
	public String name;


	NesterLibrary(int id, int userId, String name) {
		this.id = id;
		this.userId = userId;
		this.name = name;
	}


	/** \brief Get a list of all MediaFile IDs in a Library.
	*
	* @param libraryId The database ID of the Library. 
	* @param connection Opened Database Connection
	* @return List of database IDs of the MediaFiles, null on any error
	*/

	static public Vector<Integer> getMediaFileIds(int libraryId, Connection connection) {
		Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		Vector<Integer> mediaFileIds = new Vector<Integer>();

		if (connection == null) {
			return null;
		} else {
			try {

				Statement s1 = connection.createStatement();
				s1.executeQuery("select id from tools_mediafile where library_id = " + libraryId);
				ResultSet rs1 = s1.getResultSet();

				while (rs1.next()) {
					int mediaFileId = rs1.getInt("id");
					mediaFileIds.add(mediaFileId);
				}

				rs1.close();
				s1.close();

			} catch (Exception e) {
				logger.log(Level.INFO, "Exception = " + e);
				return null;
			}
		}

		return mediaFileIds;
	}


	/** \brief Get a Library Object via the API.
	*
	* @param libraryId The database ID of the Library. 
	* @return Initialized NesterLibrary object on Success, null otherwise
	*/

	static public NesterLibrary retrieveLibrary(int libraryId) {

		Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		try {

			long start = System.nanoTime();

			Response<JSONObject> response = NesterWebbClient.getNesterWebbClient()
				.get("/library/" + libraryId + "/")
				.ensureSuccess()
				.asJsonObject();

			long diff = System.nanoTime() - start;
			logger.log(Level.INFO, "PerfMon NesterJob::retrieveLibrary() API: " + (float) (diff / 1000000));

			JSONObject jo = response.getBody();

			return new NesterLibrary(jo.getInt("id"), jo.getInt("user"), jo.getString("name"));

		} catch (WebbException e) {
			StringWriter excWriter = new StringWriter();
			PrintWriter printWriter = new PrintWriter( excWriter );
			e.printStackTrace( printWriter );
			printWriter.flush();
			String stackTrace = excWriter.toString();
			logger.log(Level.SEVERE, "NesterJob::retrieveLibrary() Failed: " + e + "--" + stackTrace);
			logger.log(Level.SEVERE, 
				"NesterJob::retrieveLibrary() Failed Status: " + e.getResponse().getStatusCode() +
				" Message: " + e.getResponse().getResponseMessage() +
				"\nBody:\n" + e.getResponse().getBody() +
				"\nErrorBody:\n" +  e.getResponse().getErrorBody());
		
			return null;
		}

	}


	/** \brief Get a list of all Library IDs in the Database.
	*
	* @param connection Opened Database Connection
	* @return List of database IDs of the MediaFiles, null on any error
	*/

	static public Vector<Integer> retrieveAllLibraryIds(Connection connection) {
		Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		Vector<Integer> libraryIds = new Vector<Integer>();

		if (connection == null) {
			return null;
		} else {
			try {

				Statement s1 = connection.createStatement();
				s1.executeQuery("select id from tools_library");
				ResultSet rs1 = s1.getResultSet();

				while (rs1.next()) {
					int libraryId = rs1.getInt("id");
					libraryIds.add(libraryId);
				}

				rs1.close();
				s1.close();

			} catch (Exception e) {
				logger.log(Level.INFO, "Exception = " + e);
				return null;
			}
		}

		return libraryIds;
	}


}
