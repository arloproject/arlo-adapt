package arlo;

import java.io.Serializable;
import java.sql.Time;
import java.sql.Timestamp;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.util.HashMap;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import arlo.NesterWavFileMetaData;

/* 
 * 
 * no longer in database, only used to store data for the NesterAudioFile
 */

public class NesterStaticUserMetaData implements Serializable {

	private static final long serialVersionUID = 577780640008360184L;

	// Null values indicate not set, null in Db

	public String sex = null;


	public boolean getNesterStaticUserMetaData(int audioFileID, Connection connection, HashMap<String, Boolean> fieldNames) {
		// now split into separate parts - call the database to return all metadata records in 
		// one call, then parse them up.

		HashMap<String, Vector<String>> sourceData = NesterWavFileMetaData.getSourceData(audioFileID, connection);
		if (sourceData == null) {
			Logger logger;
			logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
			logger.log(Level.SEVERE, "Error Retrieving Metadata SourceData");
			return false;
		}

		return getNesterStaticUserMetaData(sourceData, fieldNames);
	}

	/** Parse the Metadata from a Map of values.
	 *
	 * @param sourceData A key/value map of raw metadata values, where 'value' is a Vector 
	 * where each entry represent a record in the database (there will usually be only one 
	 * record per key in the database, so this Vector will usually be length = 1.
	 * @param fieldNames If not null, then require and parse only these keys.
	 * @return True on success, False if any errors occur, including missing required keys.
	 */

	public boolean getNesterStaticUserMetaData(HashMap<String, Vector<String>> sourceData, HashMap<String, Boolean> fieldNames) {

		Logger logger;
		logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		try {
			// logger.log(Level.INFO, "Get StaticUserMetaData");

			// boolean gotMetaData = true;
			Vector<String> valueVector;
			String value;

			if (fieldNames == null || null != fieldNames.get("sex")) {
				valueVector = sourceData.get("sex");
				if (valueVector != null) {
					value = valueVector.isEmpty() ? "" : valueVector.firstElement();
					if (value != null) {
						this.sex = value;
					}
				}
			}

			return true;

		} catch (Exception e) {
			logger.log(Level.INFO, "Exception = " + e);
			return false;
		}

	}

	// ///////////////////////////////////
	// Save out blank metaData entries

	static public boolean saveEmptyNesterStaticUserMetaData(int mediaFileID, Connection connection) {

		Logger logger;
		logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		try {
			logger.log(Level.INFO, "saveEmptyNesterStaticUserMetaData");

			// if (! NesterAudioFile.SaveMetaData(mediaFileID, "sex", "", true, connection)) {
			// logger.log(Level.WARNING, "Failed saving sex");
			// return false;
			// }

			return true;

		} catch (Exception e) {
			logger.log(Level.INFO, "Exception = " + e);
			return false;
		}
	}


////////////////////////////////////////////////////////////

// NEW

//////////////////////////////////////////////////////////////

	/** \brief Save MetaData as a 'Batch'
	*
	* This saves MetaData in a batch using an SQL Prepared Statement. Note that this does
	* not execute the Insert, merely adds to an existing batch.
	* 
	* @param mediaFileID The Db id of the mediafile corresponding to this Metadata
	* @param stmtInsertMetadata A prepared INSERT statement. See arlo.NesterAudioFile.saveNewAudioFilesToDb  
	* Of the form "INSERT INTO tools_mediafilemetadata (mediaFile_id, name, value, userEditable) 
	* VALUES (?, ?, ?, ?)"
	* @return Boolean - true if success, false on error
	*/

	public boolean BatchSaveMetaData(int mediaFileID, PreparedStatement stmtInsertMetadata) {
		Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		// Prepared Statement Columns
		// 1 - mediaFile_id
		// 2 - name
		// 3 - value
		// 4 - userEditable

		try {
			// these stay the same for all MetaData, set them up front
			// confirmed this is legal from http://docs.oracle.com/javase/tutorial/jdbc/basics/prepared.html
			// (see Supplying Values for PreparedStatement Parameters)
			stmtInsertMetadata.setInt(    1, mediaFileID);
			stmtInsertMetadata.setBoolean(4, true);

			if (this.sex != null) {
				stmtInsertMetadata.setString(2, "sex");
				stmtInsertMetadata.setString(3, this.sex);
				stmtInsertMetadata.addBatch();
			}
			
			return true;

		} catch (Exception e) {
			logger.log(Level.INFO, "Exception = " + e);
			return false;
		}
		
	}

}

