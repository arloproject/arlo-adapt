package arlo;

import java.io.Serializable;

public class Model implements Serializable {

	public final static long serialVersionUID = 4826233974983L;
	
	String name;

	public String[] positiveClassTagNames;
	public String[] negativeClassTagNames;

	static final int numParameters = 8;
	public String[] ParameterNames = { "MinimumBandFrequency", "MaximumBandFrequency", "DampingFactor", "NumFrequencyBands", "NumFramesPerSecond",
			"NeighborhoodSize", "DistanceWeightingPower", "ClassificationThreshold" };

	public double minimumBandFrequency;
	public double maximumBandFrequency;
	public double dampingFactor;
	public int numFrequencyBands;
	public int numFramesPerSecond;
	public int neighborhoodSize;
	public double distanceWeightingPower;
	public double classificationThreshold;
	

	public double[][] exampleInputs;
	public double[][] exampleOutputs;
	public int[] exampleGroups;

	
	

	public Model() {
		name = "InstanceBased1";
		positiveClassTagNames = new String[] { "positive" };
		negativeClassTagNames = new String[] { "negative" };
		minimumBandFrequency = 40.0;
		maximumBandFrequency = 4000;
		dampingFactor = 0.02;
		numFrequencyBands = 32;
		numFramesPerSecond = 1;
		neighborhoodSize = 1;
		distanceWeightingPower = 0.0;
		classificationThreshold = 0.5;
	}

	public Model(Model model) {
		
		this.name = model.name;
		this.positiveClassTagNames = model.positiveClassTagNames;
		this.negativeClassTagNames = model.negativeClassTagNames;
		
		this.setParameters(model);
		
		this.exampleInputs = model.exampleInputs;
		this.exampleOutputs = model.exampleOutputs;
		
		
	}

	public void setParameters(double[] currentBiasSpacePoint) {
		int index = 0;
		minimumBandFrequency = currentBiasSpacePoint[index++];
		maximumBandFrequency = currentBiasSpacePoint[index++];
		dampingFactor = currentBiasSpacePoint[index++];
		numFrequencyBands = (int) currentBiasSpacePoint[index++];
		numFramesPerSecond = (int) currentBiasSpacePoint[index++];
		neighborhoodSize = (int) currentBiasSpacePoint[index++];
		distanceWeightingPower = currentBiasSpacePoint[index++];
		classificationThreshold = currentBiasSpacePoint[index++];
	}

	public void setParameters(Model model) {
		int index = 0;
		minimumBandFrequency = model.minimumBandFrequency;
		maximumBandFrequency = model.maximumBandFrequency;
		dampingFactor = model.dampingFactor;
		numFrequencyBands = model.numFrequencyBands;
		numFramesPerSecond = model.numFramesPerSecond;
		neighborhoodSize = model.neighborhoodSize;
		distanceWeightingPower = model.distanceWeightingPower;
		classificationThreshold = model.classificationThreshold;
	}

	public String getParameterName(int index) {
		return ParameterNames[index];
	}

	public double getParameterValue(int index) {

		double value = Double.NaN;

		switch (index) {
		case 0:
			value = minimumBandFrequency;
			break;
		case 1:
			value = maximumBandFrequency;
			break;
		case 2:
			value = dampingFactor;
			break;
		case 3:
			value = numFrequencyBands;
			break;
		case 4:
			value = numFramesPerSecond;
			break;
		case 5:
			value = neighborhoodSize;
			break;
		case 6:
			value = distanceWeightingPower;
			break;
		case 7:
			value = classificationThreshold;
			break;
		}

		return value;
	}
	
	public void setParameterValue(int index, double value) {

		switch (index) {
		case 0:
			 minimumBandFrequency = value;
			break;
		case 1:
			 maximumBandFrequency = value;
			break;
		case 2:
			 dampingFactor = value;
			break;
		case 3:
			 numFrequencyBands = (int) value;
			break;
		case 4:
			 numFramesPerSecond = (int) value;
			break;
		case 5:
			 neighborhoodSize = (int) value;
			break;
		case 6:
			 distanceWeightingPower = value;
			break;
		case 7:
			 classificationThreshold = value;
			break;
		}
	}

}
