package arlo;

import java.util.Vector;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NesterRandomWindow {

	public int id = -1;
	public int randomWindowTaggingJob_id = -1;
	public int audioFile_id = -1;
	public double startTime = Double.NaN;
	public double endTime = Double.NaN;
	
	// convenience
	
	public int [] classCounts = null;

	NesterRandomWindow(int id, int randomWindowTaggingJob_id, int audioFile_id, double startTime, double endTime) {

		this.id = id;
		this.randomWindowTaggingJob_id = randomWindowTaggingJob_id;
		this.audioFile_id = audioFile_id;

		this.startTime = startTime;
		this.endTime = endTime;
	}

	
	/////
	// Adds a window to the database
	static public boolean addRandomWindow(NesterRandomWindow nesterRandomWindow, Connection connection) {

		Logger logger;
		logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		boolean success = false;
		try {

			Statement stmt1 = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			ResultSet uprs1 = stmt1.executeQuery("SELECT * FROM tools_randomwindow");

			uprs1.moveToInsertRow();

			uprs1.updateInt("randomWindowTaggingJob_id", nesterRandomWindow.randomWindowTaggingJob_id);
			uprs1.updateInt("mediaFile_id", nesterRandomWindow.audioFile_id);
			uprs1.updateDouble("startTime", nesterRandomWindow.startTime);
			uprs1.updateDouble("endTime", nesterRandomWindow.endTime);
			uprs1.insertRow();

			uprs1.close();

			stmt1.close();

			success = true;

		} catch (Exception e) {
			System.out.println("Exception = " + e);
		}

		return success;
	}

	//////
	// Get a window from the db
	static public Vector<NesterRandomWindow> getNesterRandomWindows(int randomWindowTaggingJob_id, Connection connection) {

		Logger logger;
		logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		Vector<NesterRandomWindow> randomWindows = new Vector<NesterRandomWindow>();

		try {

			Statement s1 = connection.createStatement();
			s1.executeQuery("select * from tools_randomwindow where randomWindowTaggingJob_id = " + randomWindowTaggingJob_id);
			ResultSet rs1 = s1.getResultSet();

			while (rs1.next()) {

				NesterRandomWindow nesterRandomWindow = null;

				nesterRandomWindow = new NesterRandomWindow(rs1.getInt("id"), rs1.getInt("randomWindowTaggingJob_id"), rs1.getInt("audioFile_id"),
						rs1.getDouble("startTime"), rs1.getDouble("endTime"));

				randomWindows.add(nesterRandomWindow);

				// System.out.format("%d\t%f\n", nesterRandomWindow.id,
				// (nesterRandomWindow.endTime - nesterRandomWindow.startTime));

			}
			rs1.close();
			s1.close();

			return randomWindows;

		} catch (Exception e) {
			logger.log(Level.INFO, "name not found");
			logger.log(Level.INFO, "Exception = " + e);
		}

		return randomWindows;
	}




}
