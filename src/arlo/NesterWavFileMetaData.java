package arlo;

import java.io.Serializable;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.util.HashMap;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.StringWriter;
import java.io.PrintWriter;

/* 
 * 
 * no longer in database, only used to store data for the NesterAudioFile
 */

public class NesterWavFileMetaData implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7938171648762895537L;
	public long fileSize;
	public boolean validRIFFsize = false;
	public boolean validDATAsize = false;
	public int dataStartIndex = -1;
	public long dataSize = -1;
	public int wFormatTag = -1;
	public int numChannels = -1;
	public int sampleRate = -1;
	public int nAvgBytesPerSec = -1;
	public int nBlockAlign = -1;
	public int bitsPerSample = -1;
	public int numBytesToTailPad = -1;
	public int numFrames = -1;
	public double durationInSeconds = Double.NaN;

	/** \brief Default Constructor */

	public NesterWavFileMetaData() {
		
	}

	/** \brief Constructor from an initialized AudioFile
	*
	* @param audioFile An initialized AudioFile
	* @return initialized NesterWavFileMetaData
	*/
	
	public NesterWavFileMetaData(AudioFile audioFile) {
		fileSize = audioFile.fileSize;
		validRIFFsize = audioFile.validRIFFsize;
		validDATAsize = audioFile.validDATAsize;
		dataStartIndex = audioFile.dataStartIndex;
		dataSize = audioFile.dataSize;
		wFormatTag = audioFile.wFormatTag;
		numChannels = audioFile.numChannels;
		sampleRate = audioFile.sampleRate;
		nAvgBytesPerSec = audioFile.nAvgBytesPerSec;
		nBlockAlign = audioFile.nBlockAlign;
		bitsPerSample = audioFile.bitsPerSample;
		numBytesToTailPad = audioFile.numBytesToTailPad;
		numFrames = audioFile.numFrames;
		durationInSeconds = audioFile.durationInSeconds;
	}

	public boolean getNesterWavFileMetaData(int audioFileID, Connection connection, HashMap<String, Boolean> fieldNames) {
		// now split into separate parts - call the database to return all metadata records in 
		// one call, then parse them up.

		HashMap<String, Vector<String>> sourceData = getSourceData(audioFileID, connection);
		if (sourceData == null) {
			Logger logger;
			logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
			logger.log(Level.SEVERE, "Error Retrieving Metadata SourceData");
			return false;
		}

		return getNesterWavFileMetaData(sourceData, fieldNames);
	}

	/** Get all of the raw metadata entries from the database for an Audio File
	 *
	 * @param audioFileID Database Id of the AudioFile for which to retrieve data.
	 * @param connection An opened database connection.
	 * @return null if any error, otherwise a key/value map, where keys are the metadata 
	 * names and values are Vectors, where each entry represents a database record for that key.
	 */

	public static HashMap<String, Vector<String>> getSourceData(int audioFileID, Connection connection) {
		Logger logger;
		logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		HashMap<String, Vector<String>> metadataMap = new HashMap<String, Vector<String>>();

		try {
			Statement s1 = connection.createStatement();
			s1.executeQuery("SELECT * FROM tools_mediafilemetadata WHERE mediaFile_id = " + audioFileID);
			ResultSet rs1 = s1.getResultSet();

			while (rs1.next()) {
				String name  = rs1.getString("name");
				String value = rs1.getString("value");

				Vector<String> metadataValuesVector = metadataMap.get(name);
				if (metadataValuesVector == null) {
					metadataValuesVector = new Vector<String>();
					metadataMap.put(name, metadataValuesVector);
				}

				metadataValuesVector.add(value);
			}

			rs1.close();
			s1.close();

			return metadataMap;

		} catch (Exception e) {
			logger.log(Level.INFO, "Error Retrieving Metadata");
			logger.log(Level.INFO, "Exception = " + e);
			return null;
		}

	}

	/** Parse the Metadata from a Map of values.
	 *
	 * @param sourceData A key/value map of raw metadata values, where 'value' is a Vector 
	 * where each entry represent a record in the database (there will usually be only one 
	 * record per key in the database, so this Vector will usually be length = 1.
	 * @param fieldNames If not null, then require and parse only these keys.
	 * @return True on success, False if any errors occur, including missing required keys.
	 */

	public boolean getNesterWavFileMetaData(HashMap<String, Vector<String>> sourceData, HashMap<String, Boolean> fieldNames) {
		Logger logger;
		logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		try {

			boolean gotMetaData = true;
			Vector<String> valueVector;
			String value;

			// if (fieldNames == null || null != fieldNames.get("fileSize"))
			// if fieldNames is null, then get all metadata
			// if fieldNames is NOT null, get only specified metadata

			if (fieldNames == null || null != fieldNames.get("fileSize")) {
				valueVector = sourceData.get("fileSize");
				value = valueVector.isEmpty() ? null : valueVector.firstElement();
				if (value != null) {
					if (value.isEmpty()) {
						value = "0";
					}
					this.fileSize = Long.parseLong(value);
				} else {
					logger.log(Level.WARNING, "Did not find fileSize");
					gotMetaData = false;
				}
			}

			if (fieldNames == null || null != fieldNames.get("validRIFFsize")) {
				valueVector = sourceData.get("validRIFFsize");
				value = valueVector.isEmpty() ? null : valueVector.firstElement();
				if (value != null) {
					this.validRIFFsize = Boolean.parseBoolean(value);
				} else {
					logger.log(Level.WARNING, "Did not find validRIFFsize");
					gotMetaData = false;
				}
			}

			if (fieldNames == null || null != fieldNames.get("validDATAsize")) {
				valueVector = sourceData.get("validDATAsize");
				value = valueVector.isEmpty() ? null : valueVector.firstElement();
				if (value != null) {
					this.validDATAsize = Boolean.parseBoolean(value);
				} else {
					logger.log(Level.WARNING, "Did not find validDATAsize");
					gotMetaData = false;
				}
			}

			if (fieldNames == null || null != fieldNames.get("dataStartIndex")) {
				valueVector = sourceData.get("dataStartIndex");
				value = valueVector.isEmpty() ? null : valueVector.firstElement();
				if (value != null) {
					if (value.isEmpty()) {
						value = "0";
					}
					this.dataStartIndex = Integer.parseInt(value);
				} else {
					logger.log(Level.WARNING, "Did not find dataStartIndex");
					gotMetaData = false;
				}
			}

			if (fieldNames == null || null != fieldNames.get("dataSize")) {
				valueVector = sourceData.get("dataSize");
				value = valueVector.isEmpty() ? null : valueVector.firstElement();
				if (value != null) {
					if (value.isEmpty()) {
						value = "0";
					}
					this.dataSize = Long.parseLong(value);
				} else {
					logger.log(Level.WARNING, "Did not find dataSize");
					gotMetaData = false;
				}
			}

			if (fieldNames == null || null != fieldNames.get("wFormatTag")) {
				valueVector = sourceData.get("wFormatTag");
				value = valueVector.isEmpty() ? null : valueVector.firstElement();
				if (value != null) {
					this.wFormatTag = Integer.parseInt(value);
				} else {
					logger.log(Level.WARNING, "Did not find wFormatTag");
					gotMetaData = false;
				}
			}

			if (fieldNames == null || null != fieldNames.get("numChannels")) {
				valueVector = sourceData.get("numChannels");
				value = valueVector.isEmpty() ? null : valueVector.firstElement();
				if (value != null) {
					if (value.isEmpty()) {
						value = "0";
					}
					this.numChannels = Integer.parseInt(value);
				} else {
					logger.log(Level.WARNING, "Did not find numChannels");
					gotMetaData = false;
				}
			}

			if (fieldNames == null || null != fieldNames.get("sampleRate")) {
				valueVector = sourceData.get("sampleRate");
				value = valueVector.isEmpty() ? null : valueVector.firstElement();
				if (value != null) {
					if (value.isEmpty()) {
						value = "0";
					}
					this.sampleRate = Integer.parseInt(value);
				} else {
					logger.log(Level.WARNING, "Did not find sampleRate");
					gotMetaData = false;
				}
			}

			if (fieldNames == null || null != fieldNames.get("nAvgBytesPerSec")) {
				valueVector = sourceData.get("nAvgBytesPerSec");
				value = valueVector.isEmpty() ? null : valueVector.firstElement();
				if (value != null) {
					if (value.isEmpty()) {
						value = "0";
					}
					this.nAvgBytesPerSec = Integer.parseInt(value);
				} else {
					logger.log(Level.WARNING, "Did not find nAvgBytesPerSec");
					gotMetaData = false;
				}
			}

			if (fieldNames == null || null != fieldNames.get("nBlockAlign")) {
				valueVector = sourceData.get("nBlockAlign");
				value = valueVector.isEmpty() ? null : valueVector.firstElement();
				if (value != null) {
					if (value.isEmpty()) {
						value = "0";
					}
					this.nBlockAlign = Integer.parseInt(value);
				} else {
					logger.log(Level.WARNING, "Did not find nBlockAlign");
					gotMetaData = false;
				}
			}

			if (fieldNames == null || null != fieldNames.get("bitsPerSample")) {
				valueVector = sourceData.get("bitsPerSample");
				value = valueVector.isEmpty() ? null : valueVector.firstElement();
				if (value != null) {
					if (value.isEmpty()) {
						value = "0";
					}
					this.bitsPerSample = Integer.parseInt(value);
				} else {
					logger.log(Level.WARNING, "Did not find bitsPerSample");
					gotMetaData = false;
				}
			}

			if (fieldNames == null || null != fieldNames.get("numBytesToTailPad")) {
				valueVector = sourceData.get("numBytesToTailPad");
				value = valueVector.isEmpty() ? null : valueVector.firstElement();
				if (value != null) {
					if (value.isEmpty()) {
						value = "0";
					}
					this.numBytesToTailPad = Integer.parseInt(value);
				} else {
					logger.log(Level.WARNING, "Did not find numBytesToTailPad");
					gotMetaData = false;
				}
			}

			if (fieldNames == null || null != fieldNames.get("numFrames")) {
				valueVector = sourceData.get("numFrames");
				value = valueVector.isEmpty() ? null : valueVector.firstElement();
				if (value != null) {
					if (value.isEmpty()) {
						value = "0";
					}
					this.numFrames = Integer.parseInt(value);
				} else {
					logger.log(Level.WARNING, "Did not find numFrames");
					gotMetaData = false;
				}
			}

			if (fieldNames == null || null != fieldNames.get("durationInSeconds")) {
				valueVector = sourceData.get("durationInSeconds");
				value = valueVector.isEmpty() ? null : valueVector.firstElement();
				if (value != null) {
					if (value.isEmpty()) {
						value = "0";
					}
					this.durationInSeconds = Double.parseDouble(value);
				} else {
					logger.log(Level.WARNING, "Did not find durationInSeconds");
					gotMetaData = false;
				}
			}

			if (gotMetaData == false) {
				logger.log(Level.WARNING, "Not all MetaData was found");
				return false;
			}

			return true;

		} catch (Exception e) {
			logger.log(Level.INFO, "Exception = " + e);

			StringWriter writer = new StringWriter();
			PrintWriter printWriter = new PrintWriter( writer );
			e.printStackTrace( printWriter );
			printWriter.flush();
			String stackTrace = writer.toString();
			logger.log(Level.INFO, stackTrace);

			return false;
		}

	}

	public boolean saveNesterWavFileMetaData(int mediaFileID, Connection connection) {

		Logger logger;
		logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		// return true;

		try {
			logger.log(Level.INFO, "saveNesterWavFileMetaData");

			if (! NesterAudioFile.SaveMetaData(mediaFileID, "fileSize", Long.toString(this.fileSize), false, connection)) {
				logger.log(Level.WARNING, "Failed saving fileSize");	
				return false;
			}

			if (!NesterAudioFile.SaveMetaData(mediaFileID, "validRIFFsize", Boolean.toString(this.validRIFFsize), false, connection)) {
				logger.log(Level.WARNING, "Failed saving validRIFFsize");	
				return false;
			}

			if (!NesterAudioFile.SaveMetaData(mediaFileID, "validDATAsize", Boolean.toString(this.validDATAsize), false, connection)) {
				logger.log(Level.WARNING, "Failed saving validDATAsize");
				return false;
			}

			if (!NesterAudioFile.SaveMetaData(mediaFileID, "dataStartIndex", Integer.toString(this.dataStartIndex), false, connection)) {
				logger.log(Level.WARNING, "Failed saving dataStartIndex");
				return false;
			}

			if (! NesterAudioFile.SaveMetaData(mediaFileID, "dataSize", Long.toString(this.dataSize), false, connection)) {
				logger.log(Level.WARNING, "Failed saving dataSize");	
				return false;
			}

			if (!NesterAudioFile.SaveMetaData(mediaFileID, "wFormatTag", Integer.toString(this.wFormatTag), false, connection)) {
				logger.log(Level.WARNING, "Failed saving wFormatTag");
				return false;
			}

			if (!NesterAudioFile.SaveMetaData(mediaFileID, "numChannels", Integer.toString(this.numChannels), false, connection)) {
				logger.log(Level.WARNING, "Failed saving numChannels");
				return false;
			}

			if (!NesterAudioFile.SaveMetaData(mediaFileID, "sampleRate", Integer.toString(this.sampleRate), false, connection)) {
				logger.log(Level.WARNING, "Failed saving sampleRate");
				return false;
			}

			if (!NesterAudioFile.SaveMetaData(mediaFileID, "nAvgBytesPerSec", Integer.toString(this.nAvgBytesPerSec), false, connection)) {
				logger.log(Level.WARNING, "Failed saving nAvgBytesPerSec");
				return false;
			}

			if (!NesterAudioFile.SaveMetaData(mediaFileID, "nBlockAlign", Integer.toString(this.nBlockAlign), false, connection)) {
				logger.log(Level.WARNING, "Failed saving nBlockAlign");
				return false;
			}

			if (!NesterAudioFile.SaveMetaData(mediaFileID, "bitsPerSample", Integer.toString(this.bitsPerSample), false, connection)) {
				logger.log(Level.WARNING, "Failed saving bitsPerSample");
				return false;
			}

			if (!NesterAudioFile.SaveMetaData(mediaFileID, "numBytesToTailPad", Integer.toString(this.numBytesToTailPad), false, connection)) {
				logger.log(Level.WARNING, "Failed saving numBytesToTailPad");
				return false;
			}

			if (!NesterAudioFile.SaveMetaData(mediaFileID, "numFrames", Integer.toString(this.numFrames), false, connection)) {
				logger.log(Level.WARNING, "Failed saving numFrames");
				return false;
			}

			if (!NesterAudioFile.SaveMetaData(mediaFileID, "durationInSeconds", Double.toString(this.durationInSeconds), false, connection)) {
				logger.log(Level.WARNING, "Failed saving durationInSeconds");
				return false;
			}

			return true;

		} catch (Exception e) {
			logger.log(Level.INFO, "Exception = " + e);
			return false;
		}
		
	}


////////////////////////////////////////////////////////////

// NEW

//////////////////////////////////////////////////////////////

	/** \brief Save MetaData as a 'Batch'
	*
	* This saves MetaData in a batch using an SQL Prepared Statement. 
	* 
	* @param mediaFileID The Db id of the mediafile corresponding to this Metadata
	* @param stmtInsertMetadata A prepared INSERT statement. See arlo.NesterAudioFile.saveNewAudioFilesToDb  
	* Of the form "INSERT INTO tools_mediafilemetadata (mediaFile_id, name, value, userEditable) 
	* VALUES (?, ?, ?, ?)"
	* @return Boolean - true if success, false on error
	*/

	public boolean BatchSaveMetaData(int mediaFileID, PreparedStatement stmtInsertMetadata) {
		Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		// Prepared Statement Columns
		// 1 - mediaFile_id
		// 2 - name
		// 3 - value
		// 4 - userEditable

		try {
			// these stay the same for all MetaData, set them up front
			// confirmed this is legal from http://docs.oracle.com/javase/tutorial/jdbc/basics/prepared.html
			// (see Supplying Values for PreparedStatement Parameters)
			stmtInsertMetadata.setInt(    1, mediaFileID);
			stmtInsertMetadata.setBoolean(4, false);


			stmtInsertMetadata.setString( 2, "fileSize");
			stmtInsertMetadata.setString( 3, Long.toString(this.fileSize));
			stmtInsertMetadata.addBatch();

			stmtInsertMetadata.setString( 2, "validRIFFsize");
			stmtInsertMetadata.setString( 3, Boolean.toString(this.validRIFFsize));
			stmtInsertMetadata.addBatch();

			stmtInsertMetadata.setString( 2, "validDATAsize");
			stmtInsertMetadata.setString( 3, Boolean.toString(this.validDATAsize));
			stmtInsertMetadata.addBatch();

			stmtInsertMetadata.setString( 2, "dataStartIndex");
			stmtInsertMetadata.setString( 3, Integer.toString(this.dataStartIndex));
			stmtInsertMetadata.addBatch();

			stmtInsertMetadata.setString( 2, "dataSize");
			stmtInsertMetadata.setString( 3, Long.toString(this.dataSize));
			stmtInsertMetadata.addBatch();

			stmtInsertMetadata.setString( 2, "wFormatTag");
			stmtInsertMetadata.setString( 3, Integer.toString(this.wFormatTag));
			stmtInsertMetadata.addBatch();

			stmtInsertMetadata.setString( 2, "numChannels");
			stmtInsertMetadata.setString( 3, Integer.toString(this.numChannels));
			stmtInsertMetadata.addBatch();

			stmtInsertMetadata.setString( 2, "sampleRate");
			stmtInsertMetadata.setString( 3, Integer.toString(this.sampleRate));
			stmtInsertMetadata.addBatch();

			stmtInsertMetadata.setString( 2, "nAvgBytesPerSec");
			stmtInsertMetadata.setString( 3, Integer.toString(this.nAvgBytesPerSec));
			stmtInsertMetadata.addBatch();

			stmtInsertMetadata.setString( 2, "nBlockAlign");
			stmtInsertMetadata.setString( 3, Integer.toString(this.nBlockAlign));
			stmtInsertMetadata.addBatch();

			stmtInsertMetadata.setString( 2, "bitsPerSample");
			stmtInsertMetadata.setString( 3, Integer.toString(this.bitsPerSample));
			stmtInsertMetadata.addBatch();

			stmtInsertMetadata.setString( 2, "numBytesToTailPad");
			stmtInsertMetadata.setString( 3, Integer.toString(this.numBytesToTailPad));
			stmtInsertMetadata.addBatch();

			stmtInsertMetadata.setString( 2, "numFrames");
			stmtInsertMetadata.setString( 3, Integer.toString(this.numFrames));
			stmtInsertMetadata.addBatch();

			stmtInsertMetadata.setString( 2, "durationInSeconds");
			stmtInsertMetadata.setString( 3, Double.toString(this.durationInSeconds));
			stmtInsertMetadata.addBatch();


			return true;

		} catch (Exception e) {
			logger.log(Level.INFO, "Exception = " + e);
			return false;
		}
		
	}





}
