package arlo;

import java.sql.Timestamp;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.sql.PreparedStatement;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.HashMap;
import java.util.Set;
import java.util.Vector;
import java.util.Random;
import java.lang.StringBuffer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.DataOutputStream;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.PrintWriter;

import com.goebl.david.WebbException;
import org.json.JSONObject;


public class NesterJob {

	public int id;
	public String name;
	public int user_id;
	public int project_id;
	
	public Timestamp creationDate;

	public int numToComplete;
	public int numCompleted;
	public double fractionCompleted;

	public double elapsedRealTime;
	
	public double timeToCompletion;

	public boolean isRunning;
	public boolean wasStopped;
	public boolean isComplete;
	public boolean wasDeleted;

	public int type_id;
	public Integer status_id;  // Integer so that it can be null
	public Integer requestedStatus_id;  
	public Integer parentJob_id;
	public Integer priority;

	// key is the Parameter name, and Value is a Vector of entries for that value
	public HashMap<String, Vector<String>> jobParameters;

	/** 
	 * Default Constructor.
	 */

// TODO remove this, and force initization with getNesterJob() or the copy.
	
	public NesterJob() {
		jobParameters = new HashMap<String, Vector<String>> ();
	}

	/** 
	 * Copy Constructor.
	 */
	public NesterJob(NesterJob j) {
		id = j.id;
		name = j.name;
		user_id = j.user_id;
		project_id = j.project_id;
		creationDate = j.creationDate;
		numToComplete = j.numToComplete;
		numCompleted = j.numCompleted;
		fractionCompleted = j.fractionCompleted;
		elapsedRealTime = j.elapsedRealTime;
		timeToCompletion = j.timeToCompletion;
		isRunning = j.isRunning;
		wasStopped = j.wasStopped;
		isComplete = j.isComplete;
		wasDeleted = j.wasDeleted;
		type_id = j.type_id;
		status_id = j.status_id;
		requestedStatus_id = j.requestedStatus_id;
		parentJob_id = j.parentJob_id;

		// copy the jobParameters
		// I'm not instantly familiar with Java's deep copy procedures,
		// so I'll just loop over the entire thing
		for (String k : (Set<String>) j.jobParameters.keySet()) {
			Vector<String> values = new Vector<String>();
			for (String value : j.jobParameters.get(k)) {
				values.add(value);
			}		jobParameters = new HashMap<String, Vector<String>>();

			jobParameters.put(k, values);
		}
	}

	/** \brief Get a Job from the database. 
	 *
	 * Initializes 'this' with the Job.
	 * @param id Database Id of the Job for which to grab.
	 * @param connection Opened database Connection.
	 * @return true if success, false if errors.
	 */

	public boolean getNesterJob(int id, Connection connection) {

		Logger logger;
		logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		/////////////////
		// get Job info
		try {

			Statement s1 = connection.createStatement();
			s1.executeQuery("select * from tools_jobs where id = " + id);
			ResultSet rs1 = s1.getResultSet();
			
			if (rs1.next()) {
				this.id = id;
				this.user_id = rs1.getInt("user_id");
				this.project_id = rs1.getInt("project_id");  // 0 returned if null in database
				this.name = rs1.getString("name");

				this.numToComplete = rs1.getInt("numToComplete");
				this.numCompleted = rs1.getInt("numCompleted");

				this.creationDate = rs1.getTimestamp("creationDate");

				this.fractionCompleted = rs1.getDouble("fractionCompleted");
				this.elapsedRealTime = rs1.getDouble("elapsedRealTime");
				this.timeToCompletion = rs1.getDouble("timeToCompletion");

				this.isRunning = rs1.getBoolean("isRunning");
				this.wasStopped = rs1.getBoolean("wasStopped");
				this.isComplete = rs1.getBoolean("isComplete");
				this.wasDeleted = rs1.getBoolean("wasDeleted");

				this.type_id = rs1.getInt("type_id");
				this.status_id = rs1.getInt("status_id");
					if (rs1.wasNull()) {this.status_id = null;}
				this.requestedStatus_id = rs1.getInt("requestedStatus_id");
					if (rs1.wasNull()) {this.requestedStatus_id = null;}
				this.parentJob_id = rs1.getInt("parentJob_id");
					if (rs1.wasNull()) {this.parentJob_id = null;}

				this.priority = rs1.getInt("priority");

			}
			rs1.close();
			s1.close();

			jobParameters = GetJobParameters(this.id, connection);
			if (jobParameters == null) {
				logger.log(Level.SEVERE, "Failed Retriving JobParameters");
				return false;
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error opening Job");
			logger.log(Level.SEVERE, "Exception = " + e);
			return false;
		}

		return true;
	}
	
	/** \brief Read in a Job's the JobParameters from the database.
	 *
	 * @param jobId Database Id of the Job for which to grab parameters.
	 * @param connection Opened database Connection.
	 * @return null if error, otherwise HashMap<String key, Vector<String> values>.
	 */
	
	private static HashMap<String, Vector<String>> GetJobParameters(int jobId, Connection connection) {
		Logger logger;
		logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		HashMap<String, Vector<String>> jobParameters = new HashMap<String, Vector<String>>();

		try {

			Statement s1 = connection.createStatement();
			s1.executeQuery("SELECT * FROM tools_jobparameters WHERE job_id = " + jobId);
			ResultSet rs1 = s1.getResultSet();

			int rows = 0;
			while (rs1.next()) {
				rows++;
				String name  = rs1.getString("name");
				String value = rs1.getString("value");

				if (jobParameters.containsKey(name)) {
					jobParameters.get(name).add(value);
				} else {
					Vector<String> values = new Vector<String>();
					values.add(value);
					jobParameters.put(name, values);
				}

			}

			logger.log(Level.INFO, "Found " + rows + " JobParameter Entries for id: " +  jobId);
			rs1.close();
			s1.close();

		} catch (Exception e) {
			logger.log(Level.SEVERE, "GetJobParameters() Failed");
			logger.log(Level.SEVERE, "Exception = " + e);
			return null;
		}

		return jobParameters;
	}

	/** \brief Lookup a JobParameter from the Job.
	 *
	 * This expects a single value entry for the paramteter in the database.
	 * @note This routine will fail, returning a null, if multiple entries
	 * exist for the parameter.
	 *
	 * @param name Name of the Parameter to retrieve.
	 * @return null if not found, String value otherwise
	 */

	public String GetJobParameter(String name) {
		Logger logger;
		logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		if (! jobParameters.containsKey(name)) {
			logger.log(Level.INFO, "GetJobParameter() Key not found - job: " + this.id + " key: '" + name + "'");
			return null;
		}
		
		if (jobParameters.get(name).isEmpty()) {
			logger.log(Level.INFO, "GetJobParameter() Value list empty - job: " + this.id + " key: '" + name + "'");
			return null;
		}

		if (jobParameters.get(name).size() > 1) {
			logger.log(Level.SEVERE, "GetJobParameter() Multiple Values - job: " + this.id + " key: '" + name + "'");
			return null;
		}
		return jobParameters.get(name).firstElement();
	}

	/** \brief Lookup a JobParameter from the Job.
	 *
	 * Returns a list of all values for the Parameter.
	 * @note This routine will return an empty Vector if no values exist,
	 * or are not found.
	 *
	 * @param name Name of the Parameter to retrieve.
	 * @return Vector of Strings, empty if not found
	 */

	public Vector<String> GetJobParameterVector(String name) {
		Logger logger;
		logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		if (! jobParameters.containsKey(name)) {
			logger.log(Level.INFO, "GetJobParameter() Key not found - job: " + this.id + " key: '" + name + "'");
			return new Vector<String>();
		}

		return jobParameters.get(name);
	}

	/** \brief Add a JobParameter to a Job object in memory. 
	*
	* @note Does not automatically save to the database.
	* @note Enforces the current database limit of 255 characters for name or value.
	*
	* @param name Name of the Parameter to add.
	* @param value Value of the Parameter to add. 
	* @return true if success, false otherwise.
	*/
	
	public boolean AddJobParameter(String name, String value) {
		Logger logger;
		logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
		
		// enforce the database limits
		if (name.length() > 255) {
			logger.log(Level.SEVERE, "AddJobParameter() - name length > 255");
			return false;
		}
		
		if (value.length() > 255) {
			logger.log(Level.SEVERE, "AddJobParameter() - value length > 255");
			return false;
		}

		if (jobParameters.containsKey(name)) {
			jobParameters.get(name).add(value);
		} else {
			Vector<String> values = new Vector<String>();
			values.add(value);
			jobParameters.put(name, values);
		}

		return true;
	}

	
	/** \brief Save all of the Job's Parameters to the database.
	*
	* Only adds parameters that don't already exist, in the case that we're 
	* updating an existing job.
	* @param connection Opened database Connection.
	* @return True on success, False if any errors
	*/

	private boolean SaveJobParameters(Connection connection) {
		Logger logger;
		logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		// First delete all existing parameters from Database.
		if (! DeleteJobParametersFromDatabase(connection)) {
			logger.log(Level.SEVERE, "Failed DeleteJobParametersFromDatabase - bailing out of SaveJobParameters");
			return false;
		}

		try {

			Statement s1 = connection.createStatement();

			String sSql = "INSERT INTO tools_jobparameters (job_id, name, value) VALUES (?, ?, ?)";
			
			PreparedStatement stmt = connection.prepareStatement(sSql);

			stmt.setInt(1, this.id);

			// loop over all parameters
			for (String k : (Set<String>) jobParameters.keySet()) {
				stmt.setString(2, k);

				Vector<String> values = new Vector<String>();
				for (String value : jobParameters.get(k)) {
					stmt.setString(3, value);

					if (stmt.executeUpdate() != 1) {
						logger.log(Level.SEVERE, "Failed to save JobParamter() - != 1");
						return false;
					}
				}
			}

			stmt.close();
			
			return true;

		} catch (Exception e) {
			logger.log(Level.SEVERE, "Exception = " + e);
			return false;
		}
	}

	/** \brief Delete all JobParamters for a Job from the database.
	*
	* In anticipation of writing out the updated paramters.
	* @param connection Opened database Connection.
	* @return True on success, False if any errors
	*/

	private boolean DeleteJobParametersFromDatabase(Connection connection) {
		Logger logger;
		logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
		
		try {
			Statement s1 = connection.createStatement();
			s1.executeUpdate("DELETE FROM tools_jobparameters WHERE job_id = " + this.id);
			s1.close();

			return true;

		} catch (Exception e) {
			logger.log(Level.SEVERE, "Failed DeleteJobParametersFromDatabase");
			logger.log(Level.SEVERE, "Exception = " + e);
			return false;
		}
	}


	/** \brief Given a Job Status Name, lookup the database Id. 
	* 
	* @param statusName The status name to lookup.
	* @param connection An opened database Connection
	* @return null if error / not found, the database ID otherwise
	*/

	public static Integer GetJobStatusIdFromName(String statusName, Connection connection) {
		Logger logger;
		logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		try {
			Integer id;
			Statement s1 = connection.createStatement();
			s1.executeQuery("SELECT id FROM tools_jobstatustypes WHERE name = '" + statusName + "'");
            ResultSet rs1 = s1.getResultSet();

			if (rs1.next()) {
				id = rs1.getInt("id");
            } else {
				// no rows returned
				logger.log(Level.SEVERE, "Did not find status name '" + statusName + "'");
				id = null;
			}
			
			// make sure there was only one row
			rs1.last();
			int rows = rs1.getRow();
			if (rows != 1) {
				logger.log(Level.SEVERE, "Warning: " + rows + " rows found for StatusName '" + statusName + "'");
				id = null;
			}

			rs1.close();
			s1.close();

			return id;

		} catch (Exception e) {
            logger.log(Level.SEVERE, "Exception = " + e);
            return null;
        }
	}

	/** \brief Given a Job Type Name, lookup the database Id. 
	* 
	* @param typeName The type name to lookup.
	* @param connection An opened database Connection
	* @return null if error / not found, the database ID otherwise
	*/

	public static Integer GetJobTypeIdFromName(String typeName, Connection connection) {
		Logger logger;
		logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		try {
			Integer id;
			Statement s1 = connection.createStatement();
			s1.executeQuery("SELECT id FROM tools_jobtypes WHERE name = '" + typeName + "'");
            ResultSet rs1 = s1.getResultSet();

			if (rs1.next()) {
				id = rs1.getInt("id");
            } else {
				// no rows returned
				logger.log(Level.SEVERE, "Did not find type name '" + typeName + "'");
				id = null;
			}
			
			// make sure there was only one row
			rs1.last();
			int rows = rs1.getRow();
			if (rows != 1) {
				logger.log(Level.SEVERE, "Warning: " + rows + " rows found for TypeName '" + typeName + "'");
				id = null;
			}

			rs1.close();
			s1.close();

			return id;

		} catch (Exception e) {
            logger.log(Level.SEVERE, "Exception = " + e);
            return null;
        }
	}


	/** \brief Save a NesterJob object to the database.
	*
	* @note If this.id == -1, this will create a new job, and set this objects 
	* id accordingly. Otherwise, this will overwrite an existing job's id.
	* @note Currently this does nothing with JobParameters
	* @param connection An opened database Connection
	* @return true if success, false if any errors
	*/

	public boolean SaveJob(Connection connection) {

		Logger logger;
		logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		try {
			////
			// create a statuement, based on whether we're adding or updating
	
			Statement s1 = connection.createStatement();
			String sSql;
	
			if (this.id == -1) {
				// inserting a new Job
				sSql = "INSERT INTO tools_jobs " +
				"(name, user_id, project_id, type_id, creationDate, " + 
				"numToComplete, numCompleted, fractionCompleted, elapsedRealTime, timeToCompletion, " + 
				"isRunning, wasStopped, isComplete, wasDeleted, " + 
				"status_id, requestedStatus_id, parentJob_id, priority)" +
				"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			} else {
				// update an existing job
				sSql = "UPDATE tools_jobs SET " +
				"name = ?, user_id = ?, project_id = ?, type_id = ?, creationDate = ?, " + 
				"numToComplete = ?, numCompleted = ?, fractionCompleted = ?, elapsedRealTime = ?, timeToCompletion = ?, " + 
				"isRunning = ?, wasStopped = ?, isComplete = ?, wasDeleted = ?, " + 
				"status_id = ?, requestedStatus_id = ?, parentJob_id = ?, priority = ? " +
				"WHERE id = " + this.id + " LIMIT 1";
			}
	
			PreparedStatement stmt = connection.prepareStatement(sSql, Statement.RETURN_GENERATED_KEYS);
	
			////
			// set fields
	
			stmt.setString(   1, this.name);
			stmt.setInt(      2, this.user_id);
			stmt.setInt(      3, this.project_id);
			stmt.setInt(      4, this.type_id);
			stmt.setTimestamp(5, this.creationDate);
			stmt.setInt(      6, this.numToComplete);
			stmt.setInt(      7, this.numCompleted);
			stmt.setDouble(   8, this.fractionCompleted);
			stmt.setDouble(   9, this.elapsedRealTime);
			stmt.setDouble(  10, this.timeToCompletion);
			stmt.setBoolean( 11, this.isRunning);
			stmt.setBoolean( 12, this.wasStopped);
			stmt.setBoolean( 13, this.isComplete);
			stmt.setBoolean( 14, this.wasDeleted);
			if (this.status_id == null) {
				stmt.setNull(15, java.sql.Types.INTEGER);
			} else {
				stmt.setInt(15, this.status_id);
			}
			if (this.requestedStatus_id == null) {
				stmt.setNull(16, java.sql.Types.INTEGER);
			} else {
				stmt.setInt(16, this.requestedStatus_id);
			}
			if (this.parentJob_id == null) {
				stmt.setNull(17, java.sql.Types.INTEGER);
			} else {
				stmt.setInt(17, this.parentJob_id);
			}
			stmt.setInt(    18, this.priority);
	
			////
			// Save to Database - execute statement, and get ID
			if (stmt.executeUpdate() != 1) {
				logger.log(Level.SEVERE, "Modified Rows != 1");
				return false;
			}

			ResultSet rs = stmt.getGeneratedKeys();
			if (rs.next()) {
				if (this.id == -1) {
					// get the new id
					this.id = rs.getInt(1);
				} else {
					// shouldn't have generated any keys... uh-oh
					logger.log(Level.SEVERE, "Should not have created new keys in update");
					return false;
//					// verify the id's match
//					if (this.id != rs.getInt(1)) {
//						logger.log(Level.SEVERE, "Modified IDs don't match");
//						return false;
//					}
				}
			} else {
				if (this.id == -1) {
					// should have generated keys
					logger.log(Level.SEVERE, "No new keys generated for insert");
					return false;
				}
			}
	
			rs.close();
			stmt.close();

		} catch (Exception e) {
			logger.log(Level.SEVERE, "Failed saving Job");
			logger.log(Level.SEVERE, "Exception = " + e);
			return false;
		}

		// Save the JobParameters
		if (! SaveJobParameters(connection)) {
			logger.log(Level.SEVERE, "Failed saving Job while adding parameters");
			return false;
		}

		return true;
	}

	public void PrintJobParameters() {
		// print out parameter list:
		System.out.println("=== All Job Parameters ===");
		for (String k : jobParameters.keySet()) {
			System.out.println("  key = " + k);
			Vector<String> values = new Vector<String>();
			for (String value : jobParameters.get(k)) {
				System.out.println("    " + value);
			}

		}
	}

	/** \brief Save a log entry for a Job.
	*
	* @note If this.id == -1, return false
	* @param message The Message to save for the log entry.
	* @return true if success, false if any errors
	*/

	public boolean SaveJobLogMessage(String message) {
		return SaveJobLogMessage(this.id, message);
	}
	
	/** \brief Save a log entry for a Job.
	*
	* @note If jobId == -1, return false
	* @param jobId Database ID of the Job to which we are adding the log entry.
	* @param message The Message to save for the log entry.
	* @return true if success, false if any errors
	*/

	public static boolean SaveJobLogMessage(int jobId, String message) {
		return SaveJobLogMessage(jobId, message, null);
	}

	public boolean SaveJobLogMessage(String message, Connection connection) {
		return SaveJobLogMessage(this.id, message, connection);
	}
	

	// Keep the MySQL Version currently for some compatibility where we're doing table locking. 

	public static boolean SaveJobLogMessage(int jobId, String message, Connection connection) {

		Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		if (connection == null) {

			// Use the API to add JobLogMessage

			try {

				long start = System.nanoTime();

				NesterWebbClient.getNesterWebbClient()
					.post("/jobLog/")
					.param("job", jobId)
					.param("message", message)
					.ensureSuccess()
					.asVoid();
				
				long diff = System.nanoTime() - start;
				logger.log(Level.INFO, "PerfMon NesterJob::SaveJobLogMessage() API: " + (float) (diff / 1000000));

				return true;
			} catch (WebbException e) {
				StringWriter excWriter = new StringWriter();
				PrintWriter printWriter = new PrintWriter( excWriter );
				e.printStackTrace( printWriter );
				printWriter.flush();
				String stackTrace = excWriter.toString();
				logger.log(Level.SEVERE, 
					"NesterJob::SaveJobLogMessage() Failed Status: " + e.getResponse().getStatusCode() +
					" Message: " + e.getResponse().getResponseMessage() +
					"\nBody:\n" + e.getResponse().getBody() +
					"\nErrorBody:\n" +  e.getResponse().getErrorBody() +
					"\nException " + e + " -- " + stackTrace);
			
				return false;
			}


		} else {

			// Use the MySQL Connection 

			if (jobId == -1) {
				logger.log(Level.SEVERE, "SaveJobLogMessage called with id == -1");
				return false;
			}

			try {

				String sSql;

				// inserting a new JobLog
				sSql = "INSERT INTO tools_joblog " +
					"(job_id, messageDate, message) " + 
					"VALUES (?, NOW(), ?)";

				PreparedStatement stmt = connection.prepareStatement(sSql, Statement.RETURN_GENERATED_KEYS);

				stmt.setInt(1, jobId);
				stmt.setString(2, message);

				if (stmt.executeUpdate() != 1) {
					logger.log(Level.SEVERE, "Modified Rows != 1");
					return false;
				}

				stmt.close();

			} catch (Exception e) {
				logger.log(Level.SEVERE, "Failed saving JobLog");
				logger.log(Level.SEVERE, "Exception = " + e);
				return false;
			}

			return true;
		}

	}

	/** \brief Upload a Job Result File back to the webserver.
	*
	* @note If this.id == -1, return false
	* @note Error handling is rather weak here, so calling functions should 
	*       not expect this to reliably complete. 
	* @param fileName Name of the file to upload.
	* @param resultFileBytes A array of bytes that we are uploading as the file.
	* @param notes Notes to store with the file.
	* @param connection An opened database Connection
	* @return true if success, false if any errors
	*/

	public boolean UploadJobResultFile(String fileName, byte[] resultFileBytes, String notes, Connection connection) {

		Logger logger;
		logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		if (this.id == -1) {
			logger.log(Level.SEVERE, "UploadJobResultFile called with id == -1");
			return false;
		}

		//// 
		// Now we upload the file to the webserver API

		// this is gonna get crazy...

		String crlf = "\r\n";
		String doubleHyphen = "--";
		String boundary = getRandomString(20);  // we set this up randomly... not perfect, but meh
		String uploadUrl = ArloSettings.NesterAPIRootURL + "/jobResultFile/";

		try {
			// Setup the Request
			HttpURLConnection httpUrlConnection = null;
			URL url = new URL(uploadUrl);
			httpUrlConnection = (HttpURLConnection) url.openConnection();
			httpUrlConnection.setUseCaches(false);
			httpUrlConnection.setDoOutput(true);

			httpUrlConnection.setRequestMethod("POST");
			httpUrlConnection.setRequestProperty("Connection", "Keep-Alive");
			httpUrlConnection.setRequestProperty("Cache-Control", "no-cache");
			httpUrlConnection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
				httpUrlConnection.setRequestProperty("Authorization", "Token " + ArloSettings.NesterAPIToken);

			////
			// Send Parameters

			DataOutputStream request = new DataOutputStream(httpUrlConnection.getOutputStream());

			// job
			request.writeBytes(doubleHyphen + boundary + crlf);
			request.writeBytes("Content-Disposition: form-data; name=\"job\"" + crlf);
			request.writeBytes("Content-Type: text/plain; charset=UTF-8" + crlf + crlf);
			request.writeBytes(Integer.toString(this.id) + crlf);

			// notes
			request.writeBytes(doubleHyphen + boundary + crlf);
			request.writeBytes("Content-Disposition: form-data; name=\"notes\"" + crlf);
			request.writeBytes("Content-Type: text/plain; charset=UTF-8" + crlf + crlf);
			request.writeBytes(notes + crlf);


			// The actual file contents

			request.writeBytes(doubleHyphen + boundary + crlf);
			request.writeBytes("Content-Disposition: form-data; name=\"resultFile\";filename=\"" + fileName + "\"" + crlf);
			request.writeBytes(crlf);

			// File Content

			request.write(resultFileBytes);

			// End File

			request.writeBytes(crlf);
			request.writeBytes(doubleHyphen + boundary + doubleHyphen + crlf);

			// Close request

			request.flush();
			request.close();

			// Get Response
			if (httpUrlConnection.getResponseCode() != 201) {
				logger.log(Level.SEVERE, "Did not receive 201 from API");
				logger.log(Level.SEVERE, httpUrlConnection.getResponseMessage());
				String response_message = "";
				byte[] b = new byte[100];
				int num_read = 0;
				while (num_read != -1) {
					num_read = httpUrlConnection.getErrorStream().read(b);
					if (num_read > 0)
						response_message += new String(b, 0, num_read);
				}
				logger.log(Level.SEVERE, response_message);
				return false;
			}
//			InputStream responseStream = new BufferedInputStream(httpUrlConnection.getInputStream());
//
//			BufferedReader responseStreamReader = new BufferedReader(new InputStreamReader(responseStream));
//			String line = "";
//			StringBuilder stringBuilder = new StringBuilder();
//			while ((line = responseStreamReader.readLine()) != null)
//			{
//				stringBuilder.append(line).append("\n");
//			}
//			responseStreamReader.close();
//
//			String response = stringBuilder.toString();
//			logger.log(Level.SEVERE, response);

		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error Communicating with API");
			e.printStackTrace();
			return false;
		}

		return true;
	}


	private String getRandomString(int length) {
        ////
        // Generate a not-so-secure Nonce to use as the Upload Authorization Key

		String uploadNonce;
		String characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < length; i++) {
			double index = Math.random() * characters.length();
			buffer.append(characters.charAt((int) index));
		}

		return buffer.toString();
	}

}

