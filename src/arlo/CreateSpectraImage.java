package arlo;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;
import java.util.logging.Logger;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

import adapt.IO;
import adapt.Utility;

public class CreateSpectraImage {

	public enum SpectraPitchTraceType {
		NONE, FUNDAMENTAL, MAX_ENERGY
	}

	public enum SpectraPitchTraceColor {
		BLACK, WHITE
	}

	UserState userState = null;
	boolean monochrome = false;
	public boolean constantIntensity = false;
	public boolean logValues = false;
	Color[] colorPoints = null;
	public int numColorPoints = -1;

	public CreateSpectraImage() {
	}

	public CreateSpectraImage(Logger logger, boolean showCatalog, boolean showTimeScale, boolean showFrequencyScale, int spectraBorderWidth, int spectraTopBorderHeight,
			int timeScaleHeight, int catalogSpectraNumFrequencyBands, boolean simpleNormalization, double gain) {

		this.userState = new UserState();
		userState.logger = logger;
		userState.showCatalog = showCatalog;
		userState.showTimeScale = showTimeScale;
		userState.showFrequencyScale = showFrequencyScale;
		userState.spectraBorderWidth = spectraBorderWidth;
		userState.spectraTopBorderHeight = spectraTopBorderHeight;
		userState.timeScaleHeight = timeScaleHeight;
		userState.catalogSpectraNumFrequencyBands = catalogSpectraNumFrequencyBands;
		userState.simpleNormalization = simpleNormalization;
		userState.gain = gain;
	}

	public CreateSpectraImage(UserState userState) {
		this.userState = userState;
	}

	public void initDisplay(int numColorPoints_, int width_, int height_, boolean linear) {
		initDisplay(numColorPoints_, linear);
	}

	// TODO: get rid of public void initDisplay(int numColorPoints_, int width_, int height_, boolean linear)
	public void initDisplay(int numColorPoints_, boolean linear) {

		int numPrimaryColors = -1;
		Color[] primaryColorPoints = null;

		if (monochrome) {
			float[][] colorPoint = new float[][] { { 1.0f, 1.0f, 1.0f }, { 0.0f, 0.0f, 0.0f }, };

			int numColorPoints = colorPoint.length;
			int stepsPerTransition = 256;
			int numTransitions = numColorPoints - 1;

			numPrimaryColors = numColorPoints + stepsPerTransition * numTransitions;

			primaryColorPoints = new Color[numPrimaryColors];

			int colorIndex = 0;

			float intensity = -1.0f;

			for (int i = 0; i < numColorPoints; i++) {

				float unitPos = (float) colorIndex / (numPrimaryColors - 1);

				if (constantIntensity)
					intensity = 1.0f;
				else
					intensity = (float) Math.pow(unitPos, 0.1);

				float red = colorPoint[i][0];
				float green = colorPoint[i][1];
				float blue = colorPoint[i][2];

				double squaredSum = red * red + green * green + blue * blue;
				double length = Math.sqrt(squaredSum) / Math.sqrt(3.0);

				red /= length;
				green /= length;
				blue /= length;

				if (red > 1.0f)
					red = 1.0f;
				if (green > 1.0f)
					green = 1.0f;
				if (blue > 1.0f)
					blue = 1.0f;

				primaryColorPoints[colorIndex++] = new Color(red * intensity, green * intensity, blue * intensity);

				if (i < numColorPoints - 1) {
					for (int j = 1; j <= stepsPerTransition; j++) {

						unitPos = (float) colorIndex / (numPrimaryColors - 1);

						if (constantIntensity)
							intensity = 1.0f;
						else
							intensity = (float) Math.pow(unitPos, 0.1);

						float transPos = (float) j / (stepsPerTransition + 1);
						float w1 = 1.0f - transPos;
						float w2 = transPos;

						red = w1 * colorPoint[i][0] + w2 * colorPoint[i + 1][0];
						green = w1 * colorPoint[i][1] + w2 * colorPoint[i + 1][1];
						blue = w1 * colorPoint[i][2] + w2 * colorPoint[i + 1][2];

						squaredSum = red * red + +green * green + blue * blue;
						length = Math.sqrt(squaredSum) / Math.sqrt(3.0);

						red /= length;
						green /= length;
						blue /= length;

						if (red > 1.0f)
							red = 1.0f;
						if (green > 1.0f)
							green = 1.0f;
						if (blue > 1.0f)
							blue = 1.0f;

						primaryColorPoints[colorIndex++] = new Color(red * intensity, green * intensity, blue * intensity);

					}
				}

			}

			System.out.println("colorIndex = " + colorIndex);
		}

		else {

			numPrimaryColors = 11;

			primaryColorPoints = new Color[numPrimaryColors];

			int colorIndex = 0;
			primaryColorPoints[colorIndex++] = new Color(0.0f, 0.0f, 0.0f);
			primaryColorPoints[colorIndex++] = new Color(0.0f, 0.0f, 0.5f);
			primaryColorPoints[colorIndex++] = new Color(0.0f, 0.0f, 1.0f);
			primaryColorPoints[colorIndex++] = new Color(0.0f, 0.5f, 0.5f);
			primaryColorPoints[colorIndex++] = new Color(0.0f, 1.0f, 0.0f);
			primaryColorPoints[colorIndex++] = new Color(0.5f, 0.5f, 0.0f);
			primaryColorPoints[colorIndex++] = new Color(1.0f, 0.0f, 0.0f);
			primaryColorPoints[colorIndex++] = new Color(1.0f, 0.5f, 0.0f);
			primaryColorPoints[colorIndex++] = new Color(1.0f, 1.0f, 0.0f);
			primaryColorPoints[colorIndex++] = new Color(1.0f, 1.0f, 0.5f);
			primaryColorPoints[colorIndex++] = new Color(1.0f, 1.0f, 1.0f);
		}

		// count number of color point to create

		int factor = 2;

		int sum = 0;
		int count = 1;
		for (int i = 0; i < numPrimaryColors; i++) {
			if (linear) {
				sum += 1;
			} else {

				sum += count;
				count *= factor;
			}
		}

		System.out.println("sum = " + sum);

		numColorPoints = sum;

		colorPoints = new Color[numColorPoints];

		int colorIndex = 0;
		sum = 0;
		count = 1;
		for (int i = 0; i < numPrimaryColors; i++) {

			if (linear) {
				colorPoints[colorIndex++] = primaryColorPoints[i];
			} else {
				for (int j = 0; j < count; j++) {
					colorPoints[numColorPoints - 1 - colorIndex++] = primaryColorPoints[numPrimaryColors - 1 - i];
				}
				sum += count;
				count *= factor;
			}
		}

	}

	public void updateDisplayIncremental(Graphics g, double[][] data, int xi, int xOffset, int yOffset, int height) {

		int numXBins = data.length;
		int numYBins = height;

		int numDataXBins = data.length;
		int numDataYBins = data[0].length;

		double rectWidth = 1;
		double rectHeight = 1;
		double dataValue = -1.0;

		for (int yi = 0; yi < numYBins; yi++) {
			double y = ((double) yi / (double) numYBins);
			double userY = y * height;
			int dataYi = (int) (numDataYBins * y);

			Color color = null;

			if (Double.isNaN(data[xi][dataYi])) {
				color = Color.GRAY;
			} else {

				if (logValues)
					dataValue = (float) Math.log1p(data[xi][dataYi]);
				else
					dataValue = (data[xi][dataYi]);

				int colorIndex = (int) (dataValue * numColorPoints - 1);

				if (colorIndex < 0) {
					colorIndex = 0;
				}
				if (colorIndex >= numColorPoints) {
					colorIndex = numColorPoints - 1;
				}

				color = colorPoints[colorIndex];
			}

			// if (xi >= leftMarker && xi <= rightMarker) {
			// int red = color.getRed();
			// int green = color.getGreen();
			// int blue = color.getBlue();
			// color = new Color(255 - red, 255 - green, 255 - blue);
			// }

			g.setColor(color);
			// g.fillRect((int) xi, (height - 1) - (int) (userY + yOffset),
			// (int) rectWidth, (int) rectHeight);
			g.drawLine((int) xi + xOffset, (height - 1) - (int) (userY) + yOffset, (int) xi + xOffset, (height - 1) - (int) (userY) + yOffset);
		}
	}

	public void updateDisplayPadding(Graphics g, int xStart, int xEnd, int xOffset, int yOffset, int height) {

		Color color = new Color(0.5f, 0.5f, 0.5f);
		g.setColor(color);

		// fillRect(int x, int y, int width, int height)
		g.fillRect((int) xStart + xOffset, yOffset - 1, xEnd - xStart, height);
	}

	public void updateWaveformDisplay(Graphics g, int[] waveformData, int xOffset, int yOffset, int height, int width) {

		int minValue = Integer.MAX_VALUE;
		int maxValue = Integer.MIN_VALUE;
		for (int i = 0; i < waveformData.length; i++) {

			if (waveformData[i] < minValue) {
				minValue = waveformData[i];
			}
			if (waveformData[i] > maxValue) {
				maxValue = waveformData[i];
			}

		}

		double range = maxValue - minValue;
		double[] scaledData = new double[waveformData.length];
		for (int i = 0; i < scaledData.length; i++) {
			scaledData[i] = (((double) waveformData[i]) - minValue) / range;
		}

		g.setColor(Color.BLACK);

		for (int i = 0; i < width - 1; i++) {

			int xDataIndex1 = (int) ((double) i / (double) width * waveformData.length);
			int xDataIndex2 = (int) ((double) (i + 1) / (double) width * waveformData.length);
			double yScaledValue1 = scaledData[xDataIndex1];
			double yScaledValue2 = scaledData[xDataIndex2];
			double userY1 = yScaledValue1 * height;
			double userY2 = yScaledValue2 * height;

			g.drawLine((int) i + xOffset, (height - 1) - (int) (userY1) + yOffset, (int) (i + 1) + xOffset, (height - 1) - (int) (userY2) + yOffset);

		}

	}

	public void createScales(Logger logger, Graphics g, int windowNumTimeFrames, int numFrequencyBands, int width, int height, int borderWidth, int borderHeight, double startTime,
			double endTime, double minFrequency, double maxFrequency, boolean showTimeScale, boolean showFrequencyScale) {

		Font font = new Font("Verdana", Font.PLAIN, 10);
		g.setFont(font);
		g.setColor(Color.BLACK);

		// logger.info("###### createScales");
		int textHeight = g.getFontMetrics().getHeight();
		int scaleLength = 8;
		int minorTickLength = 4;
		int majorTickLength = 8;
		// logger.info("###### createScales: textHeight = " + textHeight);
		// logger.info("###### createScales: scaleHeight = " + scaleLength);

		// create freqScale

		double startFrequency = minFrequency;
		double endFrequency = maxFrequency;

		// create timeScale

		// String text1 = decimalFormat.format(startTime) + "s";
		// int stringWidth1 = g.getFontMetrics().stringWidth(text1);
		// g.drawString(text1, borderWidth, borderHeight + numFrequencyBands + scaleHeight + textHeight);
		// String text2 = decimalFormat.format(endTime) + "s";
		// int stringWidth2 = g.getFontMetrics().stringWidth(text2);
		// g.drawString(text2, borderWidth + windowNumTimeFrames - stringWidth2, borderHeight + numFrequencyBands + scaleHeight + textHeight);

		if (showTimeScale) {
			double timeSpan = endTime - startTime;
			int targetNumMinorTicks = 40;
			double targetMinorTickDuration = timeSpan / targetNumMinorTicks;
			double actualMinorTickDuration = Math.pow(10.0, Math.round(Math.log10(targetMinorTickDuration)));
			// logger.info("###### createScales: actualMinorTickDuration = " + actualMinorTickDuration);

			int startTimeNumMinorTicks = (int) (startTime / actualMinorTickDuration);
			double minorTickStartTime = startTimeNumMinorTicks * actualMinorTickDuration;
			if (minorTickStartTime < startTime) {
				minorTickStartTime += actualMinorTickDuration;
			}

			DecimalFormat decimalFormat = null;
			if (actualMinorTickDuration >= 1.0) {

				decimalFormat = new DecimalFormat("#####0");

			} else {
				int numFractionDigits = -(int) (Math.log10(actualMinorTickDuration));

				String decimalFormatString = "#####0.";
				for (int i = 0; i < numFractionDigits; i++) {
					decimalFormatString += "0";
				}

				decimalFormat = new DecimalFormat(decimalFormatString);
			}

			double minorTickTime = minorTickStartTime;
			while (true) {
				if (minorTickTime > endTime) {
					break;
				}
				double timeOffset = minorTickTime - startTime;
				int xCoord = (int) (borderWidth + timeOffset / timeSpan * windowNumTimeFrames);

				boolean majorTick = false;
				int tickLength = -1;

				if (((int) (Math.round(minorTickTime / actualMinorTickDuration))) % 10 == 0) {
					majorTick = true;
					tickLength = majorTickLength;
				} else {
					majorTick = false;
					tickLength = minorTickLength;
				}

				g.drawLine(xCoord, borderHeight + numFrequencyBands, xCoord, borderHeight + numFrequencyBands + tickLength);

				if (majorTick) {
					String text1 = decimalFormat.format(minorTickTime) + "s";
					int stringWidth1 = g.getFontMetrics().stringWidth(text1);
					g.drawString(text1, xCoord - stringWidth1 / 2, borderHeight + numFrequencyBands + scaleLength + textHeight);
				}

				minorTickTime += actualMinorTickDuration;
			}
		}

		if (showFrequencyScale) {
			double startLogFrequency = Math.log(startFrequency);
			double endLogFrequency = Math.log(endFrequency);
			double frequencyLogSpan = endLogFrequency - startLogFrequency;
			int numTicks = 11;
			double tickLogFrequencySpan = frequencyLogSpan / (numTicks - 1);
			// logger.info("###### createScales: tickLogFrequencySpan = " + tickLogFrequencySpan);

			DecimalFormat decimalFormat = new DecimalFormat("#####0");

			double majorTickLogFrequency = startLogFrequency;

			for (int i = 0; i < numTicks; i++) {

				double frequencyOffset = majorTickLogFrequency - startLogFrequency;
				int yBand = (int) Math.round((numFrequencyBands - frequencyOffset / frequencyLogSpan * numFrequencyBands));
				if (yBand == numFrequencyBands)
					yBand--;
				int yCoord = (int) (borderHeight + yBand);

				g.drawLine(borderWidth - majorTickLength, yCoord, borderWidth, yCoord);

				String text = decimalFormat.format(Math.round(Math.exp(majorTickLogFrequency))) + "hz";
				int stringWidth = g.getFontMetrics().stringWidth(text);
				g.drawString(text, borderWidth - majorTickLength - stringWidth - 1, yCoord + textHeight / 2 - 2);

				majorTickLogFrequency += tickLogFrequencySpan;
			}
		}
	}

	public void calculateFirstTags() {

		ArrayList<UserTag> allTags = new ArrayList<UserTag>();

		for (int i = 0; i < userState.taggedExamples.size(); i++) {

			UserTag userTag = userState.taggedExamples.get(i);

			ArrayList<String> tagStrings = userTag.tagStrings;
			ArrayList<Integer> tagStrengths = userTag.tagStrengths;
			int maxStrength = Integer.MIN_VALUE;
			int maxStrengthIndex = 0;
			for (int j = 0; j < tagStrings.size(); j++) {

				int stength = tagStrengths.get(j);

				if (stength > maxStrength) {
					maxStrength = stength;
					maxStrengthIndex = j;
				}

			}

			userTag.displayName = tagStrings.get(maxStrengthIndex);

			allTags.add(userTag);
		}

		userState.catalogFirstTags = new ArrayList<UserTag>();

		HashMap<String, UserTag> classNameToUserTag = new HashMap<String, UserTag>();

		// userState.logger.info("###### calculateCatalogDimensions:  firstTags.size() = " + userState.catalogFirstTags.size());

		for (int i = 0; i < allTags.size(); i++) {
			UserTag userTag = allTags.get(i);
			ArrayList<String> tagStrings = userTag.tagStrings;
			ArrayList<Integer> tagStrengths = userTag.tagStrengths;
			for (int j = 0; j < tagStrings.size(); j++) {

				String key = tagStrings.get(j);
				int stength = tagStrengths.get(j);

				if (userState.tagNames.contains(key) && classNameToUserTag.get(key) == null && stength == 10) {
					classNameToUserTag.put(key, userTag);
					userState.catalogFirstTags.add(userTag);
					break;
				}

			}

		}
		// userState.logger.info("###### calculateCatalogDimensions:  firstTags.size() = " + userState.catalogFirstTags.size());

		Collections.sort(userState.catalogFirstTags, new UserTagDisplayNameAndCreationTimeComparator());

		// UserTag[] firstTagsArray = new UserTag[firstTags.size()];
		//
		// firstTagsArray = (UserTag[]) firstTags.toArray(firstTagsArray);
		//
		// Arrays.sort(firstTagsArray);
		// userState.logger.info("###### calculateCatalogDimensions:  firstTagsArray.length = " + firstTagsArray.length);
		//
		// for (int i = 0; i < firstTagsArray.length; i++) {
		// firstTags.set(i, firstTagsArray[i]);
		// }

	}

	public void calculateClassExampleTags() {

		userState.catalogClassExampleTags = new ArrayList<UserTag>();

		for (int i = 0; i < userState.taggedExamples.size(); i++) {

			UserTag userTag = userState.taggedExamples.get(i);

			ArrayList<String> tagStrings = userTag.tagStrings;
			ArrayList<Integer> tagStrengths = userTag.tagStrengths;
			int maxStrength = Integer.MIN_VALUE;
			int maxStrengthIndex = 0;
			for (int j = 0; j < tagStrings.size(); j++) {

				int stength = tagStrengths.get(j);

				if (stength > maxStrength) {
					maxStrength = stength;
					maxStrengthIndex = j;
				}

			}

			String tagName = tagStrings.get(maxStrengthIndex);
			userTag.displayName = tagName;
			userTag.tagClassIndex = userState.tagNames.indexOf(tagName);

			if (userTag.tagClassIndex == userState.catalogTagClassIndex)
				userState.catalogClassExampleTags.add(userTag);
		}

		HashMap<String, UserTag> classNameToUserTag = new HashMap<String, UserTag>();

		// userState.logger.info("###### calculateCatalogDimensions:  classExampleTags.size() = " + userState.catalogClassExampleTags.size());

		Collections.sort(userState.catalogClassExampleTags, new UserTagDisplayNameAndCreationTimeComparator());

		// UserTag[] firstTagsArray = new UserTag[firstTags.size()];
		//
		// firstTagsArray = (UserTag[]) firstTags.toArray(firstTagsArray);
		//
		// Arrays.sort(firstTagsArray);
		// userState.logger.info("###### calculateCatalogDimensions:  firstTagsArray.length = " + firstTagsArray.length);
		//
		// for (int i = 0; i < firstTagsArray.length; i++) {
		// firstTags.set(i, firstTagsArray[i]);
		// }

	}

	public int calculateCatalogDimensions(UserState userState) {

		int currentRowWidthInPixels = 0;
		ArrayList<UserTag> taggedExamples = userState.taggedExamples;

		switch (userState.catalogTypeIndex) {

		case UserState.CATALOG_TYPE_ALL_TAG_CLASSES:

			calculateFirstTags();

			// userState.logger.info("###### calculateCatalogDimensions");

			currentRowWidthInPixels += userState.spectraBorderWidth + userState.catalogBorderWidth + userState.spectraBorderWidth;

			for (int i = 0; i < userState.catalogFirstTags.size(); i++) {
				UserTag userTag = userState.catalogFirstTags.get(i);
				double duation = userTag.endTime - userTag.startTime;
				int exampleWidth = Math.max(1, (int) (duation * userState.catalogSpectraNumFramesPerSecond));

				currentRowWidthInPixels += exampleWidth + userState.catalogBorderWidth;

			}
			// userState.logger.info("###### calculateCatalogDimensions:  currentRowWidthInPixels = " + currentRowWidthInPixels);

			break;

		case UserState.CATALOG_TYPE_TAG_CLASS_EXAMPLES:

			calculateClassExampleTags();

			// userState.logger.info("###### calculateCatalogDimensions");

			currentRowWidthInPixels += userState.spectraBorderWidth + userState.catalogBorderWidth + userState.spectraBorderWidth;

			for (int i = 0; i < userState.catalogClassExampleTags.size(); i++) {
				UserTag userTag = userState.catalogClassExampleTags.get(i);
				double duation = userTag.endTime - userTag.startTime;
				int exampleWidth = Math.max(1, (int) (duation * userState.catalogSpectraNumFramesPerSecond));

				currentRowWidthInPixels += exampleWidth + userState.catalogBorderWidth;

			}
			// userState.logger.info("###### calculateCatalogDimensions:  currentRowWidthInPixels = " + currentRowWidthInPixels);

			break;

		}
		return currentRowWidthInPixels;

	}

	public void createCatalog(Logger logger, UserState userState, Graphics graphics, int yOffset) {

		// logger.info("###### createCatalog");

		Font font = new Font("Verdana", Font.PLAIN, 10);

		int textHeight = graphics.getFontMetrics().getHeight();
		// logger.info("###### createCatalog: textHeight = " + textHeight);

		int xOffset = userState.spectraBorderWidth;

		switch (userState.catalogTypeIndex) {

		case UserState.CATALOG_TYPE_ALL_TAG_CLASSES:

			for (int i = 0; i < userState.catalogFirstTags.size(); i++) {

				UserTag userTag = userState.catalogFirstTags.get(i);
				double duation = userTag.endTime - userTag.startTime;

				int windowNumTimeFrames = Math.max(1, (int) (duation * userState.catalogSpectraNumFramesPerSecond));

				double[][] imageSpectraData = null;

				// logger.info("###### createCatalog: userTag.startTime = " + userTag.startTime);
				// logger.info("###### createCatalog: userTag.endTime = " + userTag.endTime);
				// logger.info("###### createCatalog: duation = " + duation);
				// logger.info("###### createCatalog: userState.catalogNumSpectraPerSecond = " + userState.catalogSpectraNumFramesPerSecond);
				// logger.info("###### createCatalog: windowNumTimeFrames = " + windowNumTimeFrames);

				imageSpectraData = getSpectraData(userState, logger, userTag.startTime, userTag.audioFile, windowNumTimeFrames, userState.catalogSpectraNumFrequencyBands,
						userState.catalogSpectraNumFramesPerSecond, userState.spectraDampingFactor, userState.spectraMinimumBandFrequency, userState.spectraMaximumBandFrequency,
						false, null);

				for (int t = 0; t < windowNumTimeFrames; t++) {
					updateDisplayIncremental(graphics, imageSpectraData, t, xOffset, yOffset, userState.catalogSpectraNumFrequencyBands);
				}

				graphics.setFont(font);
				graphics.setColor(Color.WHITE);
				graphics.drawString(userTag.displayName, xOffset + 1, yOffset + textHeight + 1);

				xOffset += windowNumTimeFrames + userState.catalogBorderWidth;

			}
			break;

		case UserState.CATALOG_TYPE_TAG_CLASS_EXAMPLES:

			for (int i = 0; i < userState.catalogClassExampleTags.size(); i++) {

				UserTag userTag = userState.catalogClassExampleTags.get(i);
				double duation = userTag.endTime - userTag.startTime;

				int windowNumTimeFrames = Math.max(1, (int) (duation * userState.catalogSpectraNumFramesPerSecond));

				double[][] imageSpectraData = null;

				// logger.info("###### createCatalog: userTag.startTime = " + userTag.startTime);
				// logger.info("###### createCatalog: userTag.endTime = " + userTag.endTime);
				// logger.info("###### createCatalog: duation = " + duation);
				// logger.info("###### createCatalog: userState.catalogNumSpectraPerSecond = " + userState.catalogSpectraNumFramesPerSecond);
				// logger.info("###### createCatalog: windowNumTimeFrames = " + windowNumTimeFrames);

				imageSpectraData = getSpectraData(userState, logger, userTag.startTime, userTag.audioFile, windowNumTimeFrames, userState.catalogSpectraNumFrequencyBands,
						userState.catalogSpectraNumFramesPerSecond, userState.spectraDampingFactor, userState.spectraMinimumBandFrequency, userState.spectraMaximumBandFrequency,
						false, null);

				for (int t = 0; t < windowNumTimeFrames; t++) {
					updateDisplayIncremental(graphics, imageSpectraData, t, xOffset, yOffset, userState.catalogSpectraNumFrequencyBands);
				}

				graphics.setFont(font);
				graphics.setColor(Color.WHITE);
				graphics.drawString(userTag.displayName + Integer.toString(i + 1), xOffset + 1, yOffset + textHeight + 1);

				xOffset += windowNumTimeFrames + userState.catalogBorderWidth;

			}
			break;

		}

	}

	/*
	 * public BufferedImage createSpectralImageJPEG(double startTime, double endTime, AudioFile audioFile, int windowNumTimeFrames, int numFrequencyBands, int numTimeFramesPerSecond, double
	 * dampingRatio, double minFrequency, double maxFrequency, boolean usePrecomputedSpectra, Object[] intData) {
	 * 
	 * return createSpectralImageJPEG(true, false, SpectraPitchTraceType.MAX_ENERGY, startTime, endTime, audioFile, windowNumTimeFrames, numFrequencyBands, numTimeFramesPerSecond, dampingRatio,
	 * minFrequency, maxFrequency, usePrecomputedSpectra, intData, null);
	 * 
	 * }
	 */
	public BufferedImage createSpectralImageJPEG(boolean showSpectra, boolean showWaveform, SpectraPitchTraceType pitchTraceType, double startTime, double endTime,
			AudioFile audioFile, int windowNumTimeFrames, int numFrequencyBands, double numTimeFramesPerSecond, double dampingRatio, double minFrequency, double maxFrequency,
			boolean usePrecomputedSpectra, Object[] intData, String imageFilePath, SpectraPitchTraceColor pitchTraceColor, int pitchTraceNumSamplePoints, int pitchTraceWidth,
			double pitchTraceMinCorrelation, double pitchTraceEntropyThreshold, double pitchTraceStartFreq, double pitchTraceEndFreq, double spectraPitchTraceMinEnergyThreshold,
			double spectraPitchTraceTolerance, double spectraPitchTraceInverseFreqWeight, double spectraPitchTraceMaxPathLengthPerTransition, int spectraPitchTraceWindowSize, double spectraPitchTraceExtendRangeFactor) {

		/**********************/
		/* Compute Image Size */
		/**********************/

		// Width
		int currentRowWidthInPixels = 0;

		if (userState.showCatalog)
			currentRowWidthInPixels = calculateCatalogDimensions(userState);
		int imageWidth = Math.max(currentRowWidthInPixels, userState.spectraBorderWidth + windowNumTimeFrames + userState.spectraBorderWidth);

		// Height
		int imageHeight = 0;

		if (showSpectra) {
			imageHeight += userState.spectraTopBorderHeight + numFrequencyBands + userState.timeScaleHeight;
		}

		if (showWaveform) {
			imageHeight += numFrequencyBands;
		}

		if (userState.showCatalog)
			imageHeight += userState.catalogSpectraNumFrequencyBands;

		// Open Output Image File
		File file = null;

		if (imageFilePath != null)
			file = new File(imageFilePath);
		else {

			String curDir = System.getProperty("user.dir");
			String fileName = curDir + "/published_resources/" + userState.loginName + ".jpg";
			file = new File(fileName);
		}

		if (file == null) {
			userState.logger.info("createSpectralImageJPEG: Error - Output file == null");
			return null;
		}

		if (file.exists())
			file.delete();

		// userState.logger.info("createSpectralImageJPEG: imageFilePath = " + file.getAbsolutePath());

		/**************/
		/* Init Image */
		/**************/

		int numColorPoints = -1;
		boolean linearColorScale = true;
		initDisplay(numColorPoints, imageWidth, imageHeight, linearColorScale);
		userState.logger.info("createSpectralImageJPEG: 1");
		// Create an image
		BufferedImage bufferedImage = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_RGB);

		Graphics graphics = bufferedImage.getGraphics();
		userState.logger.info("createSpectralImageJPEG: 2");
		graphics.setColor(Color.WHITE);
		graphics.fillRect(0, 0, imageWidth, imageHeight);

		int currentYOffset = 0;
		double[][] imageSpectraData = null;

		/*************************/
		/* Compute Image Padding */
		/*************************/

		// image padding before and after end of file

		double prePadSeconds = 0;
		double postPadSeconds = 0;
		double fileStartTime = startTime;
		double fileEndTime = endTime;
		int numPrePaddedFrames = 0;
		int numPostPaddedFrames = 0;

		if (startTime < 0) {
			// pad beginning of image
			prePadSeconds = 0 - startTime;
			fileStartTime = 0;
			numPrePaddedFrames = (int) (numTimeFramesPerSecond * prePadSeconds);
		}
		if (endTime >= audioFile.durationInSeconds) {
			postPadSeconds = endTime - audioFile.durationInSeconds;
			fileEndTime = audioFile.durationInSeconds;
			numPostPaddedFrames = (int) (postPadSeconds * numTimeFramesPerSecond);

			// if endTime is exactly equal to duration, then this entire function will fail (probably
			// a < vs. <= type issue somewhere) - this resolves this by just forcing one extra frame of post padding
			numPostPaddedFrames += 1;
		}

		int fileWindowNumTimeFrames = windowNumTimeFrames - numPrePaddedFrames - numPostPaddedFrames;

		/******************/
		/* Get Image Data */
		/******************/

		// ///
		// spectra

		if (showSpectra) {
			/*
			 * userState.logger.info("Pre Pad" + "\n\tstart:" + 0 + "\n\tend:" + numPrePaddedFrames + "\n\tyOffset:" + userState.spectraBorderWidth + "\n\theight:" + numFrequencyBands + "\n");
			 */

			// pre-padding
			updateDisplayPadding(graphics, 0, numPrePaddedFrames, userState.spectraBorderWidth, userState.spectraTopBorderHeight, numFrequencyBands);

			// post-padding
			updateDisplayPadding(graphics, windowNumTimeFrames - numPostPaddedFrames, windowNumTimeFrames, userState.spectraBorderWidth, userState.spectraTopBorderHeight,
					numFrequencyBands);

			imageSpectraData = null;
			/*
			 * userState.logger.info("createSpectralImageJPEG: 2.3"); userState.logger.info( "\n\tstartTime:" + startTime + "\n\tendTime:" + endTime + "\n\tfileStartTime:" + fileStartTime +
			 * "\n\tfileEndTime:" + fileEndTime + "\n\tfileWindowNumTimeFrames:" + fileWindowNumTimeFrames + "\n");
			 */
			if (fileWindowNumTimeFrames > 0) { // fringe cases where this is 0 (or maybe less ?) due to padding
				imageSpectraData = getSpectraData(userState, userState.logger, fileStartTime, audioFile, fileWindowNumTimeFrames, numFrequencyBands, numTimeFramesPerSecond,
						dampingRatio, minFrequency, maxFrequency, usePrecomputedSpectra, intData, pitchTraceType, pitchTraceColor, pitchTraceNumSamplePoints, pitchTraceWidth,
						pitchTraceMinCorrelation, pitchTraceEntropyThreshold, pitchTraceStartFreq, pitchTraceEndFreq, spectraPitchTraceMinEnergyThreshold,
						spectraPitchTraceTolerance, spectraPitchTraceInverseFreqWeight, spectraPitchTraceMaxPathLengthPerTransition, spectraPitchTraceWindowSize, spectraPitchTraceExtendRangeFactor);
				userState.logger.info("createSpectralImageJPEG: 2.5");
				for (int t = 0; t < fileWindowNumTimeFrames; t++) {
					updateDisplayIncremental(graphics, imageSpectraData, t, userState.spectraBorderWidth + numPrePaddedFrames, userState.spectraTopBorderHeight, numFrequencyBands);

				}
			} // if (fileWindowNumTimeFrames)
			else {
				userState.logger.info("####################################");
				userState.logger.info("Error!: fileWindowNumTimeFrames <= 0");
				userState.logger.info("####################################");
			}

			userState.logger.info("createSpectralImageJPEG: 3");
			createScales(userState.logger, graphics, windowNumTimeFrames, numFrequencyBands, imageWidth, imageHeight, userState.spectraBorderWidth,
					userState.spectraTopBorderHeight, startTime, endTime, minFrequency, maxFrequency, userState.showTimeScale, userState.showFrequencyScale);

			currentYOffset += userState.spectraTopBorderHeight + numFrequencyBands + userState.spectraTopBorderHeight;
		}

		// ///
		// waveform

		if (showWaveform) {

			if (imageSpectraData == null) {
				imageSpectraData = getSpectraData(userState, userState.logger, fileStartTime, audioFile, fileWindowNumTimeFrames, numFrequencyBands, numTimeFramesPerSecond,
						dampingRatio, minFrequency, maxFrequency, usePrecomputedSpectra, intData, pitchTraceType, pitchTraceColor, pitchTraceNumSamplePoints, pitchTraceWidth,
						pitchTraceMinCorrelation, pitchTraceEntropyThreshold, pitchTraceStartFreq, pitchTraceEndFreq, spectraPitchTraceMinEnergyThreshold,
						spectraPitchTraceTolerance, spectraPitchTraceInverseFreqWeight, spectraPitchTraceMaxPathLengthPerTransition, spectraPitchTraceWindowSize, spectraPitchTraceExtendRangeFactor);
			}

			updateWaveformDisplay(graphics, (int[]) intData[0], userState.spectraBorderWidth + numPrePaddedFrames, currentYOffset, numFrequencyBands, fileWindowNumTimeFrames);

			currentYOffset += numFrequencyBands;
		} // if (showWaveform)

		userState.logger.info("createSpectralImageJPEG: 4");
		// create catalog
		if (userState.showCatalog)
			createCatalog(userState.logger, userState, graphics, currentYOffset);

		Iterator iter = ImageIO.getImageWritersByFormatName("jpeg");
		ImageWriter writer = (ImageWriter) iter.next();
		// instantiate an ImageWriteParam object with
		// default compression options
		ImageWriteParam iwp = writer.getDefaultWriteParam();
		iwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
		// an float between 0.0 and 1.0
		// 1.0 specifies minimum compression and maximum quality
		float compressRatio = 1.0f;
		iwp.setCompressionQuality(compressRatio);

		// Write the output
		IIOImage image = new IIOImage(bufferedImage, null, null);
		userState.logger.info("createSpectralImageJPEG: 5");
		try {
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			ImageOutputStream imageOutputStream = ImageIO.createImageOutputStream(byteArrayOutputStream);
			writer.setOutput(imageOutputStream);
			writer.write(null, image, iwp);
			writer.dispose();
			RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
			randomAccessFile.write(byteArrayOutputStream.toByteArray());
			randomAccessFile.close();
			imageOutputStream.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		userState.logger.info("createSpectralImageJPEG: 6");
		return bufferedImage;
	}

	static String lastMemoryMapFileString = null;
	static TagDiscovery lastTagDiscovery = null;

	public double[][] getSpectraData(UserState userState, Logger logger, double startTime, AudioFile audioFile, int windowNumTimeFrames, int numFrequencyBands,
			double numTimeFramesPerSecond, double dampingRatio, double minFrequency, double maxFrequency, boolean usePrecomputedSpectra, Object[] waveIntData) {

		return getSpectraData(userState, logger, startTime, audioFile, windowNumTimeFrames, numFrequencyBands, numTimeFramesPerSecond, dampingRatio, minFrequency, maxFrequency,
				usePrecomputedSpectra, waveIntData, SpectraPitchTraceType.NONE, SpectraPitchTraceColor.BLACK, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
	}

	public double[][] getSpectraData(UserState userState, Logger logger, double startTime, AudioFile audioFile, int windowNumTimeFrames, int numFrequencyBands,
			double numTimeFramesPerSecond, double dampingRatio, double minFrequency, double maxFrequency, boolean usePrecomputedSpectra, Object[] waveIntData,
			SpectraPitchTraceType pitchTraceType, SpectraPitchTraceColor pitchTraceColor, int pitchTraceNumSamplePoints, int pitchTraceWidth, double pitchTraceMinCorrelation,
			double pitchTraceEntropyThreshold, double pitchTraceStartFreq, double pitchTraceEndFreq, double spectraPitchTraceMinEnergyThreshold, double spectraPitchTraceTolerance,
			double spectraPitchTraceInverseFreqWeight, double spectraPitchTraceMaxPathLengthPerTransition, int spectraPitchTraceWindowSize, double spectraPitchTraceExtendRangeFactor) {

		
		


		// double extendRangeFactor = 2.0;
		double extendRangeFactor = spectraPitchTraceExtendRangeFactor;
		
		// double maxPathLengthPerTransition = 4.0;
		double maxPathLengthPerTransition = spectraPitchTraceMaxPathLengthPerTransition;
		// int windowSize = 13;
		int windowSize = spectraPitchTraceWindowSize;
	
		int averagingWindowSize = 2;
		
		

//		double extendRangeFactor = 1.0;
//		
//		double maxPathLengthPerTransition = 40.0;
//		int windowSize = 3;
//		
//		int averagingWindowSize = 2;
		
		
		
		
		
		
		BandPass bandPass = null;

		// boolean useGPU = false;

		double[][] imageSpectraData = new double[windowNumTimeFrames][numFrequencyBands];

		// System.out.println("useGPU = " + useGPU);

		// if (useGPU) {
		//
		// } else {

		// read bytes from wav file
		// convert bytes to mono samples

		double windowDuration = (double) windowNumTimeFrames / numTimeFramesPerSecond;
		double endTime = startTime + windowDuration;

		int[] audioDataAsInt = null;
		if (waveIntData != null) {
			int padNumFrames = 1;
			audioDataAsInt = BytesToWavData.getWavDataAsInt(audioFile, startTime, endTime, padNumFrames, true);
			waveIntData[0] = audioDataAsInt;
		}

		double scalingFactor = 0.2 * userState.gain;

		logger.info("###### userState.gain = " + userState.gain);

		double baseLineIntensity = Double.NaN;

		int[][] imageBandValues = new int[windowNumTimeFrames][numFrequencyBands];
		// imageSpectraData = new double[windowNumTimeFrames][numFrequencyBands];

		int timeSegmentOffset = (int) (startTime * numTimeFramesPerSecond);

		TagDiscoveryBias tagDiscoveryBias = new TagDiscoveryBias(numFrequencyBands, numTimeFramesPerSecond, dampingRatio, minFrequency, maxFrequency);

		boolean useMemoryMapFile = false;
		boolean loadMemoryMapFile = false;

		String memoryMapFileString = tagDiscoveryBias.getSpectraMemoryMapFilePath();

		boolean allDataSpectraFilePathExists = IO.fileExists(memoryMapFileString);

		if (allDataSpectraFilePathExists) {
			useMemoryMapFile = true;
		}

		TagDiscovery tagDiscovery = null;

		if (useMemoryMapFile && (lastTagDiscovery == null || !lastMemoryMapFileString.equals(memoryMapFileString))) {
			tagDiscovery = new TagDiscovery(tagDiscoveryBias);
			tagDiscovery.setupSpectralDataMemoryMaps();
			lastTagDiscovery = tagDiscovery;
		} else {
			tagDiscovery = lastTagDiscovery;
		}

		lastMemoryMapFileString = memoryMapFileString;

		if (useMemoryMapFile) {

			// logger.info("###### getting memory map spectra for = " + audioFile);
			int audioFileIndex = 0;
			boolean found = false;
			for (Iterator iterator = tagDiscovery.tagDiscoveryNesterAudioFiles.iterator(); iterator.hasNext();) {
				NesterAudioFile nesterAudioFile = (NesterAudioFile) iterator.next();
				if (nesterAudioFile.GetAbsoluteFilePath().equals(audioFile.filePath)) {
					found = true;
					break;
				}
				audioFileIndex++;
			}

			if (!found) {
				logger.info("###### error, file not found in spectra memory map file audioFile = " + audioFile);
				System.exit(1);
			}

			int[] spectraData = tagDiscovery.loadAudioFileSpectraData(audioFileIndex, timeSegmentOffset, windowNumTimeFrames, true);

			// int audioFileNumTimeFrames = spectraData.length / numFrequencyBands;
			int index = 0;
			for (int t = 0; t < windowNumTimeFrames; t++) {
				for (int f = 0; f < numFrequencyBands; f++) {
					// int index = f * audioFileNumTimeFrames + t + timeSegmentOffset;
					imageBandValues[t][f] = spectraData[index++];
				}
			}
		} else {

			// logger.info("###### CreateSpectraImage.getSpectraData:audioFile = " + audioFile);
			// logger.info("###### CreateSpectraImage.getSpectraData:startTime = " + startTime);
			// logger.info("###### CreateSpectraImage.getSpectraData:windowDuration = " + windowDuration);
			// logger.info("###### CreateSpectraImage.getSpectraData:endTime = " + endTime);
			// logger.info("###### CreateSpectraImage.getSpectraData:windowNumTimeFrames = " + windowNumTimeFrames);
			// logger.info("###### CreateSpectraImage.getSpectraData:numTimeFramesPerSecond = " + numTimeFramesPerSecond);

			bandPass = new BandPass();

			bandPass.setDampingRatio(dampingRatio);
			bandPass.setMinBandFrequency(minFrequency);
			bandPass.setMaxBandFrequency(maxFrequency);
			bandPass.setNumBands(numFrequencyBands);
			bandPass.setRawSamplingRate(audioFile.sampleRate);
			bandPass.setNumSegments((int) (windowNumTimeFrames));

			bandPass.initialize();

			int numThreads = 8;
			int[] spectraFeatures = bandPass.calculateBandPassSpectrogram(audioDataAsInt, 0, (int) (windowDuration * audioFile.sampleRate), true, numThreads);

			// logger.info("spectraFeatures.length = " + spectraFeatures.length);

			for (int t = 0; t < windowNumTimeFrames; t++) {
				for (int f = 0; f < numFrequencyBands; f++) {
					int index = f * windowNumTimeFrames + t;
					imageBandValues[t][f] = spectraFeatures[index];
				}
			}
		}

		if (userState.simpleNormalization) {

			long intensitySum = 0;

			intensitySum = 0;

			for (int t = 0; t < windowNumTimeFrames; t++) {

				for (int f = 0; f < numFrequencyBands; f++) {

					int value1 = imageBandValues[t][f];

					intensitySum += value1;

				}
			}

			double averageIntensity = (double) intensitySum / windowNumTimeFrames / numFrequencyBands;

			baseLineIntensity = averageIntensity;

			for (int t = 0; t < windowNumTimeFrames; t++) {
				for (int f = 0; f < numFrequencyBands; f++) {
					imageSpectraData[t][f] = imageBandValues[t][f] / baseLineIntensity * scalingFactor;
				}
			}
		}

		if (userState.frequencyBandNormalization) {

			for (int f = 0; f < numFrequencyBands; f++) {

				long intensitySum = 0;

				for (int t = 0; t < windowNumTimeFrames; t++) {

					int value1 = imageBandValues[t][f];

					intensitySum += value1;

				}

				double averageIntensity = (double) intensitySum / windowNumTimeFrames;

				baseLineIntensity = averageIntensity;

				for (int t = 0; t < windowNumTimeFrames; t++) {
					imageSpectraData[t][f] = imageBandValues[t][f] / baseLineIntensity * scalingFactor;
				}
			}
		}
		
		if (pitchTraceType == SpectraPitchTraceType.MAX_ENERGY) {

			double startFreqForAnalysis = pitchTraceStartFreq;
			double endFreqForAnalysis = pitchTraceEndFreq;
			int thickness = pitchTraceWidth;

			double overallMinValue = Double.POSITIVE_INFINITY;
			double overallMaxValue = Double.NEGATIVE_INFINITY;
			for (int t = 0; t < windowNumTimeFrames; t++) {
				for (int f = 0; f < numFrequencyBands; f++) {
					if (overallMinValue > imageSpectraData[t][f]) {
						overallMinValue = imageSpectraData[t][f];
					}
					if (overallMaxValue < imageSpectraData[t][f]) {
						overallMaxValue = imageSpectraData[t][f];
					}
				}
			}


			int[] maxEnergyBandIndexData = new int[windowNumTimeFrames];
			
			for (int timeFrameIndex = 0; timeFrameIndex < windowNumTimeFrames; timeFrameIndex++) {
				
				double maxValue = Double.NEGATIVE_INFINITY;
				int maxEnergyBandIndex = -1;

				for (int f = 0; f < numFrequencyBands; f++) {
					double value = imageSpectraData[timeFrameIndex][f];
					if (value > maxValue) {
						maxValue = value;
						maxEnergyBandIndex = f;
					}
				}

				if (maxEnergyBandIndex == -1) {
					// all values are NaN, likely from an audio file of all 0's
					maxEnergyBandIndex = 0;
				}

				maxEnergyBandIndexData[timeFrameIndex] = maxEnergyBandIndex;

			}
			
			
			// add lines to to spectra visualization 
			
			double paintValue;
			if (pitchTraceColor == SpectraPitchTraceColor.BLACK) {
				paintValue = overallMinValue;
			} else {
				paintValue = overallMaxValue;
			}

			for (int timeFrameIndex = 0; timeFrameIndex < windowNumTimeFrames; timeFrameIndex++) {

				int maxEnergyBandIndex = maxEnergyBandIndexData[timeFrameIndex];

				int halfThickness = thickness / 2;
				int startPaintIndex1 = Math.max(maxEnergyBandIndex - halfThickness, 0);
				int endPaintIndex1 = Math.min(startPaintIndex1 + thickness, numFrequencyBands);

				for (int f = startPaintIndex1; f < endPaintIndex1; f++) {
					imageSpectraData[timeFrameIndex][f] = paintValue;
				}

			}
			
			
		}


		if (pitchTraceType == SpectraPitchTraceType.FUNDAMENTAL) {

			double startFreqForAnalysis = pitchTraceStartFreq;
			double endFreqForAnalysis = pitchTraceEndFreq;
			int numSamplePoints = pitchTraceNumSamplePoints;
			int thickness = pitchTraceWidth;
			// int thickness = Math.max(1, numFrequencyBands / 64);
//			double minCorrelationThreshold = pitchTraceMinCorrelation;
//			double maxEntropyThreshold = pitchTraceEntropyThreshold;

			// double tolerance = 0.15;
			double tolerance = spectraPitchTraceTolerance;
			// double inverseFreqWeightingPower = 0.75; // Dream Speech
			double inverseFreqWeightingPower = spectraPitchTraceInverseFreqWeight;
			// double tolerance = 0.15;
			// double inverseFreqWeightingPower = 0.0; // TEH comparison

			int fundamentalEnergyDistributionDownSampleFactor = 16;

			// int startHarmonic = 0; // Howel
			int startHarmonic = 1; // Hollywood

			// double minEnergyThreshold = 0.00;
//			double minEnergyThreshold = spectraPitchTraceMinEnergyThreshold;
			double minP = 1e-100;

			double distanceDivisor = 1.0;
			double distancePower = 0.1;

			double freqChangeFactor = Math.exp((Math.log(endFreqForAnalysis) - Math.log(startFreqForAnalysis)) / (numSamplePoints - 1));

			/******************************************/
			/* create harmonic templates for matching */
			/******************************************/

			double[] sampleFreqs = new double[numSamplePoints];
			double[][] harmonicWeightTemplates = new double[numSamplePoints][numFrequencyBands];

			double sampleFreq = startFreqForAnalysis;

			for (int sampleFreqIndex = 0; sampleFreqIndex < numSamplePoints; sampleFreqIndex++) {

				sampleFreq *= freqChangeFactor;
				sampleFreqs[sampleFreqIndex] = sampleFreq;

				for (int bandIndex = 0; bandIndex < numFrequencyBands; bandIndex++) {

					double bandFreq = bandPass.bandFrequencies[bandIndex];

					/* find distance to nearest Harmonic */

					if (false) {

						int freqMultiple = (int) (bandFreq / sampleFreq + 0.5);

						double difference = bandFreq - ((int) (bandFreq / sampleFreq) * sampleFreq);

						double distance = Math.min(sampleFreq - difference, difference);

						double proximityWeight = 1.0 / Math.pow(distance / distanceDivisor, distancePower);

						double freqMultipleWeight = Double.NaN;

						if (freqMultiple > startHarmonic) {
							freqMultipleWeight = 1.0;
						} else {
							freqMultipleWeight = 0.0;
						}

						harmonicWeightTemplates[sampleFreqIndex][bandIndex] = proximityWeight * freqMultipleWeight;
					}

					double freqMultiple = bandFreq / sampleFreq;

					double fractionRemainder = freqMultiple - (int) freqMultiple;

					boolean nearHarmonic = false;
					boolean farHarmonic = false;
					if (fractionRemainder < tolerance || fractionRemainder > (1.0 - tolerance)) {
						nearHarmonic = true;
					}
					if (fractionRemainder < (0.5 + tolerance) && fractionRemainder > (0.5 - tolerance)) {
						farHarmonic = true;
					}

					if (freqMultiple < (1.0 - tolerance)) {
						harmonicWeightTemplates[sampleFreqIndex][bandIndex] = -1.0;
					} else {
						if (nearHarmonic) {
							harmonicWeightTemplates[sampleFreqIndex][bandIndex] = 1.0;
						}
						if (farHarmonic) {
							harmonicWeightTemplates[sampleFreqIndex][bandIndex] = -1.0;
						}
					}
				}

			}

			double[] harmonicEnergies = new double[numSamplePoints];

			double f0 = startFreqForAnalysis;

			boolean showPitchTraceOnly = false;

			double overallMinValue = Double.POSITIVE_INFINITY;
			double overallMaxValue = Double.NEGATIVE_INFINITY;
			for (int t = 0; t < windowNumTimeFrames; t++) {
				for (int f = 0; f < numFrequencyBands; f++) {
					if (overallMinValue > imageSpectraData[t][f]) {
						overallMinValue = imageSpectraData[t][f];
					}
					if (overallMaxValue < imageSpectraData[t][f]) {
						overallMaxValue = imageSpectraData[t][f];
					}
				}
			}

			double[] fundamentalEnergyDistribution = new double[numSamplePoints];

			boolean outputDataToFile = true;

			String traceFileName = null;
			String distributionFileName = null;

			double windowMaxEnergyFrequencySum = 0;
			double windowMaxHarmonicCorrelationSum = 0;
			double windowEntropySum = 0;
			int windowCount = 0;

			double[] pitchData = new double[windowNumTimeFrames];
			double[] correlationData = new double[windowNumTimeFrames];
			double[] entropyData = new double[windowNumTimeFrames];
			double[] energyData = new double[windowNumTimeFrames];
			int[] maxEnergyBandIndexData = new int[windowNumTimeFrames];
			int[] maxEnergySampleIndexData = new int[windowNumTimeFrames];
			
			for (int timeFrameIndex = 0; timeFrameIndex < windowNumTimeFrames; timeFrameIndex++) {

				
				
				// boolean trackMaxEnergyPitch = false;
				// boolean trackFundamentalPitch = true;

				double maxValue = Double.NEGATIVE_INFINITY;
				int maxEnergyBandIndex = -1;
				int maxEnergySampleIndex = -1;
				double maxHarmonicCorrelation = Double.NEGATIVE_INFINITY;


				double maxEnergyFrequency = Double.NaN;
				if (pitchTraceType == SpectraPitchTraceType.FUNDAMENTAL) {
					IO.p("finding fundamental: t = " + timeFrameIndex / numTimeFramesPerSecond);

					/***************************************************************/
					/* use harmonic template to compute fundamental pitch energies */
					/***************************************************************/

					double harmonicsEnergyWeightedSum;
					double harmonicWeight;

					f0 = startFreqForAnalysis;
					for (int sampleFreqIndex = 0; sampleFreqIndex < numSamplePoints; sampleFreqIndex++) {
						harmonicEnergies[sampleFreqIndex] = Utility.correlation(imageSpectraData[timeFrameIndex], harmonicWeightTemplates[sampleFreqIndex], numFrequencyBands)
								/ Math.pow(f0, inverseFreqWeightingPower);
						f0 *= freqChangeFactor;
					}

					maxEnergyBandIndex = -1;
					maxHarmonicCorrelation = Double.NEGATIVE_INFINITY;

					f0 = startFreqForAnalysis;
					for (int candidateFreqIndex = 0; candidateFreqIndex < numSamplePoints; candidateFreqIndex++) {

						if (harmonicEnergies[candidateFreqIndex] > maxHarmonicCorrelation) {
							maxHarmonicCorrelation = harmonicEnergies[candidateFreqIndex];
							maxEnergyFrequency = f0;
							maxEnergyBandIndex = bandPass.getFrequencyIndex(maxEnergyFrequency);
							maxEnergySampleIndex = candidateFreqIndex;
						}

						f0 *= freqChangeFactor;

					}
				}

				if (maxEnergyBandIndex == -1) {
					// all values are NaN, likely from an audio file of all 0's
					maxEnergyBandIndex = 0;
				}

				maxEnergyBandIndexData[timeFrameIndex] = maxEnergyBandIndex;
				maxEnergySampleIndexData[timeFrameIndex] = maxEnergySampleIndex;

				int startIndex = bandPass.getFrequencyIndex(startFreqForAnalysis);
				int endIndex = bandPass.getFrequencyIndex(endFreqForAnalysis * extendRangeFactor );
				int numBandsAnalyzed = endIndex - startIndex + 1;
				double energySum = 0.0;
				//				for (int bandIndex = 0; bandIndex < numFrequencyBands; bandIndex++) {
				for (int bandIndex = startIndex; bandIndex <= endIndex; bandIndex++) {
					energySum += imageSpectraData[timeFrameIndex][bandIndex];
				}

				double entropySum = 0.0;
				//				for (int bandIndex = 0; bandIndex < numFrequencyBands; bandIndex++) {
				for (int bandIndex = startIndex; bandIndex <= endIndex; bandIndex++) {
					double p = imageSpectraData[timeFrameIndex][bandIndex] / energySum;
					if (p < minP)
						p = minP;
					entropySum += p * Math.log(p);
				}

				double entropy = entropySum / numBandsAnalyzed;
				double energy = energySum / numBandsAnalyzed;

				pitchData[timeFrameIndex] = maxEnergyFrequency;
				correlationData[timeFrameIndex] = maxHarmonicCorrelation;
				entropyData[timeFrameIndex] = entropy;
				energyData[timeFrameIndex] = energy;

			}
			
			
			
			
			// sort values for filtering //
			
//			double[] pitchDataSorted = new double[windowNumTimeFrames];
			double[] correlationDataSorted = new double[windowNumTimeFrames];
			double[] entropyDataSorted = new double[windowNumTimeFrames];
			double[] energyDataSorted = new double[windowNumTimeFrames];
//			int[] maxEnergyBandIndexDataSorted = new int[windowNumTimeFrames];
//			int[] maxEnergySampleIndexDataSorted = new int[windowNumTimeFrames];

			
			System.arraycopy(correlationData, 0, correlationDataSorted, 0, windowNumTimeFrames);
			System.arraycopy(entropyData, 0, entropyDataSorted, 0, windowNumTimeFrames);
			System.arraycopy(energyData, 0, energyDataSorted, 0, windowNumTimeFrames);

			Arrays.sort(correlationDataSorted);
			Arrays.sort(entropyDataSorted);
			Arrays.sort(energyDataSorted);
			
			double minCorrelation = pitchTraceMinCorrelation;
			// This was for using 'pitchTraceMinCorrelation' as a percentile selector (0 - 1) to specify 
			// relative values rather than absolute. For repeatability, we'll keep this absolute for the moment. 
//			double minCorrelationThreshold = correlationDataSorted[(int) (minCorrelation * (windowNumTimeFrames - 1))];
			double minCorrelationThreshold = minCorrelation;

			//double maxEntropy = 1.0 - pitchTraceEntropyThreshold;
			//double maxEntropyThreshold = entropyDataSorted[(int) (maxEntropy * (windowNumTimeFrames - 1))];
			double maxEntropyThreshold = pitchTraceEntropyThreshold;
			
			//double minEnergyFraction = spectraPitchTraceMinEnergyThreshold;
			//double minEnergyThreshold = energyDataSorted[(int) (minEnergyFraction * (windowNumTimeFrames - 1))];
			double minEnergyThreshold = spectraPitchTraceMinEnergyThreshold;

			
//			minCorrelationThreshold = -1.0;
////			minCorrelationThreshold = 0.00285;
////			maxEntropyThreshold = -0.0180;
//			maxEntropyThreshold = 0.0;
////			minEnergyThreshold = 0.04;
			

			IO.p("minCorrelationThreshold = " + minCorrelationThreshold);
			IO.p("maxEntropyThreshold = " + maxEntropyThreshold);
			IO.p("minEnergyThreshold = " + minEnergyThreshold);
			
			
			
			
			
			// analyze and pitch traces to provide data for filtering //
			
			// apply sliding window to path length //

			int halfWindowSizeMinusOne = (windowSize - 1) / 2;
			
			int windowSum = 0;
			
			int startTimeFrame = halfWindowSizeMinusOne + 1;
			int endTimeFrame = (windowNumTimeFrames - 1) -  halfWindowSizeMinusOne - 1;
			
			
			double [] pathLengths = new double[windowNumTimeFrames];
			boolean [] useTimeFrame = new boolean[windowNumTimeFrames];

			for (int centerTimeFrameIndex = startTimeFrame; centerTimeFrameIndex <= endTimeFrame; centerTimeFrameIndex++) {
				
				
				int startIndex = centerTimeFrameIndex - halfWindowSizeMinusOne;
				int endIndex = centerTimeFrameIndex + halfWindowSizeMinusOne;
				for (int i = startIndex; i <= endIndex; i++) {
					pathLengths[centerTimeFrameIndex] += Math.abs(pitchData[i] - ((pitchData[i - 1] + pitchData[i + 1]) / 2.0));
				}
				
				
				
				if ((pathLengths[centerTimeFrameIndex] / windowSize < maxPathLengthPerTransition) && //
						(energyData[centerTimeFrameIndex] > minEnergyThreshold) && //
						(entropyData[centerTimeFrameIndex] < maxEntropyThreshold) && //
						(correlationData[centerTimeFrameIndex] > minCorrelationThreshold)) {
					useTimeFrame[centerTimeFrameIndex] = true;
				}
				
			}
			
			
			
			
			
			
			
			
			
			
			if (outputDataToFile) {

				traceFileName = "/data/pitch/" + IO.getFileNameFromPath(audioFile.filePath) + "." + startTime + "-" + endTime + ".trace.csv";
				distributionFileName = "/data/pitch/" + IO.getFileNameFromPath(audioFile.filePath) + "." + startTime + "-" + endTime  + ".dist.csv";
				IO.delete(traceFileName);
				IO.delete(distributionFileName);

				try {
//					String dataString = "DATA\t" + "Time" + "\t" + "EstimatedFundamental" + "\t" + "HarmonicCorrelation" + "\t" + "Entropy" + "\t" + "Energy";
					String dataString = "Time (s)" + "\t" + "Pitch (Hz)";
					IO.appendStringLine(traceFileName, dataString);
					IO.p(dataString);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				try {
					String dataString = "DIST\t" + "EstimatedFundamental" + "\t" + "Count";
					IO.appendStringLine(distributionFileName, dataString);
					IO.p(dataString);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				for (int timeFrameIndex = 0; timeFrameIndex < windowNumTimeFrames; timeFrameIndex++) {
					
					
					
					if (!useTimeFrame[timeFrameIndex]) {
						continue;
					}
					

					double maxEnergyFrequency = pitchData[timeFrameIndex];
					double maxHarmonicCorrelation = correlationData[timeFrameIndex];
					double entropy = entropyData[timeFrameIndex];
					double energy = energyData[timeFrameIndex];

					windowMaxEnergyFrequencySum += maxEnergyFrequency;
					windowMaxHarmonicCorrelationSum += maxHarmonicCorrelation;
					windowEntropySum += entropy;
					windowCount++;

					if ((timeFrameIndex + 1) % averagingWindowSize == 0)
						try {

							if (Double.isNaN(maxEnergyFrequency)) {
								maxEnergyFrequency = startFreqForAnalysis;
								maxHarmonicCorrelation = -1.0;
								entropy = 999999999.0;
								energy = 0;
							}

//							if (outputDataToFile && windowCount > averagingWindowSize / 2) {
								if (outputDataToFile && windowCount > 0) {
//								String dataString = "DATA\t" + (float) (timeFrameIndex + 1) / numTimeFramesPerSecond + "\t" + windowMaxEnergyFrequencySum / windowCount + "\t"
//										+ windowMaxHarmonicCorrelationSum / windowCount + "\t" + windowEntropySum / windowCount + "\t" + energy;
								String dataString = (float) (timeFrameIndex + 1) / numTimeFramesPerSecond + "\t" + windowMaxEnergyFrequencySum / windowCount;
								IO.appendStringLine(traceFileName, dataString);
								IO.p(dataString);
							}

							windowMaxEnergyFrequencySum = 0;
							windowMaxHarmonicCorrelationSum = 0;
							windowEntropySum = 0;
							windowCount  = 0;

						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

				}

				f0 = startFreqForAnalysis;
				for (int i = 0; i < numSamplePoints / fundamentalEnergyDistributionDownSampleFactor; i++) {

					try {
						String dataString = "DIST\t" + f0 + "\t" + fundamentalEnergyDistribution[i];
						IO.appendStringLine(distributionFileName, dataString);
						IO.p(dataString);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					for (int j = 0; j < fundamentalEnergyDistributionDownSampleFactor; j++) {

						f0 *= freqChangeFactor;
					}
				}
			}
			
			
			
			
			
			// add lines to to spectra visualization 
			

			for (int timeFrameIndex = 0; timeFrameIndex < windowNumTimeFrames; timeFrameIndex++) {


				if (useTimeFrame[timeFrameIndex]) {

					double maxEnergyFrequency = pitchData[timeFrameIndex];
					double maxHarmonicCorrelation = correlationData[timeFrameIndex];
					double entropy = entropyData[timeFrameIndex];
					double energy = energyData[timeFrameIndex];
					int maxEnergyBandIndex = maxEnergyBandIndexData[timeFrameIndex];


					int halfThickness = thickness / 2;
					int quarterThickness = thickness / 4;
					int startPaintIndex1 = Math.max(maxEnergyBandIndex - halfThickness, 0);
					int endPaintIndex1 = Math.min(startPaintIndex1 + thickness, numFrequencyBands);
					int startPaintIndex2 = Math.max(maxEnergyBandIndex - quarterThickness, 0);
					int endPaintIndex2 = Math.min(startPaintIndex2 + halfThickness, numFrequencyBands);

					if (showPitchTraceOnly) {
						for (int f = 0; f < numFrequencyBands; f++) {
							if ((f >= startPaintIndex1) && (f < endPaintIndex1)) {
								imageSpectraData[timeFrameIndex][f] = 0;
							} else {
								imageSpectraData[timeFrameIndex][f] = 1;
							}
						}
					} else {


						//							fundamentalEnergyDistribution[maxEnergySampleIndex / fundamentalEnergyDistributionDownSampleFactor] += maxHarmonicCorrelation;
						fundamentalEnergyDistribution[maxEnergySampleIndexData[timeFrameIndex] / fundamentalEnergyDistributionDownSampleFactor] += 1;

						for (int f = startPaintIndex1; f < endPaintIndex1; f++) {
							//imageSpectraData[t][f] = overallMinValue;
							if (pitchTraceColor == SpectraPitchTraceColor.BLACK) {
								imageSpectraData[timeFrameIndex][f] = overallMinValue;
							} else {
								imageSpectraData[timeFrameIndex][f] = overallMaxValue;
							}
						}
						for (int f = startPaintIndex2; f < endPaintIndex2; f++) {
							//imageSpectraData[t][f] = overallMinValue;
							if (pitchTraceColor == SpectraPitchTraceColor.BLACK) {
								imageSpectraData[timeFrameIndex][f] = overallMaxValue;
							} else {
								imageSpectraData[timeFrameIndex][f] = overallMinValue;
							}
						}

					}

				}

			}
			
			
		}

		// }
		return imageSpectraData;
	}

	public int[][] getNesterAudioFileSpectraData(NesterAudioFile nesterAudioFile, NesterADAPTAnalysisBias nesterADAPTAnalysisBias) {

		return getNesterAudioFileSpectraData(nesterAudioFile, (int) nesterADAPTAnalysisBias.numTimeFramesPerSecond, nesterADAPTAnalysisBias.numFrequencyBands,
				nesterADAPTAnalysisBias.dampingRatio, nesterADAPTAnalysisBias.minFrequency, nesterADAPTAnalysisBias.maxFrequency);

	}

	public int[][] getNesterAudioFileSpectraData(NesterAudioFile nesterAudioFile, TagDiscoveryBias nesterTagDiscoveryBias) {

		return getNesterAudioFileSpectraData(nesterAudioFile, (int) nesterTagDiscoveryBias.numTimeFramesPerSecond, nesterTagDiscoveryBias.numFrequencyBands,
				nesterTagDiscoveryBias.dampingRatio, nesterTagDiscoveryBias.minFrequency, nesterTagDiscoveryBias.maxFrequency);

	}

	public int[][] getNesterAudioFileSpectraData(NesterAudioFile nesterAudioFile, int numTimeFramesPerSecond, int numFrequencyBands, double dampingRatio, double minFrequency,
			double maxFrequency) {

		// double endPadDuration = 1.0; // ignore last portion of each audio file !!! is this needed?
		double endPadDuration = 0.0;

		int sampleRate = nesterAudioFile.wavFileMetaData.sampleRate;

		if (sampleRate % numTimeFramesPerSecond != 0) {
			System.err.println("Error!  sampleRate % numTimeFramesPerSecond != 0");
			System.exit(1);
		}
		int numSamplesPerTimeFrame = sampleRate / numTimeFramesPerSecond;

		int targetWindowNumTimeFrames = (int) ((nesterAudioFile.wavFileMetaData.durationInSeconds - endPadDuration) * numTimeFramesPerSecond);

		double startTime = 0.0;

		// read bytes from wav file
		// convert bytes to mono samples

		double windowDuration = (double) targetWindowNumTimeFrames / numTimeFramesPerSecond;
		double endTime = startTime + windowDuration + endPadDuration / 2.0;

		AudioFile audioFile = new AudioFile(nesterAudioFile);

		int[] data = BytesToWavData.getWavDataAsInt(audioFile, startTime, endTime, 0, true);

		if (data == null)
			return null;

		int actualWindowNumTimeFrames = data.length / numSamplesPerTimeFrame;

		BandPass bandPass = new BandPass();

		bandPass.setDampingRatio(dampingRatio);
		bandPass.setMinBandFrequency(minFrequency);
		bandPass.setMaxBandFrequency(maxFrequency);
		bandPass.setNumBands(numFrequencyBands);
		bandPass.setRawSamplingRate(sampleRate);
		bandPass.setNumSegments((int) (actualWindowNumTimeFrames));

		bandPass.initialize();

		int numThreads = ArloSettings.numThreads;
		int[] spectraFeatures = bandPass.calculateBandPassSpectrogram(data, 0, (int) (actualWindowNumTimeFrames * numSamplesPerTimeFrame), true, numThreads);

		data = null;
		// System.gc();

		int[][] imageBandValues = new int[actualWindowNumTimeFrames][numFrequencyBands];
		for (int t = 0; t < actualWindowNumTimeFrames; t++) {
			for (int f = 0; f < numFrequencyBands; f++) {
				int index = f * actualWindowNumTimeFrames + t;
				imageBandValues[t][f] = spectraFeatures[index];
			}
		}

		return imageBandValues;

	}

	public int[][] getNesterAudioFileSpectraData(NesterAudioFile nesterAudioFile, double startTime, double endTime, NesterADAPTAnalysisBias nesterADAPTAnalysisBias) {

		return getNesterAudioFileSpectraData(nesterAudioFile, startTime, endTime, (int) nesterADAPTAnalysisBias.numTimeFramesPerSecond, nesterADAPTAnalysisBias.numFrequencyBands,
				nesterADAPTAnalysisBias.dampingRatio, nesterADAPTAnalysisBias.minFrequency, nesterADAPTAnalysisBias.maxFrequency);

	}

	public static int[][] getNesterAudioFileSpectraData(NesterAudioFile nesterAudioFile, double startTime, double endTime, int numTimeFramesPerSecond, int numFrequencyBands,
			double dampingRatio, double minFrequency, double maxFrequency) {

		int sampleRate = nesterAudioFile.wavFileMetaData.sampleRate;

		double targetDuration = endTime - startTime;

		// System.out.println("startTime = " + startTime);
		// System.out.println("endTime   = " + endTime);
		// System.out.println("durations = " + targetDuration);

		int windowNumTimeFrames = (int) (targetDuration * numTimeFramesPerSecond + 0.5);

		AudioFile audioFile = new AudioFile(nesterAudioFile);

		int numPadFrames = 0; /* !!! why was it set to 1 -- caused error !!! ** */

		int[] data = BytesToWavData.getWavDataAsInt(audioFile, startTime, endTime, numPadFrames, true);

		if (data == null)
			return null;

		BandPass bandPass = new BandPass();

		bandPass.setDampingRatio(dampingRatio);
		bandPass.setMinBandFrequency(minFrequency);
		bandPass.setMaxBandFrequency(maxFrequency);
		bandPass.setNumBands(numFrequencyBands);
		bandPass.setRawSamplingRate(sampleRate);
		bandPass.setNumSegments((int) (windowNumTimeFrames));

		bandPass.initialize();

		int numThreads = ArloSettings.numThreads;
		// int numSamples = (int) (windowNumTimeFrames * (sampleRate / numTimeFramesPerSecond));
		int[] spectraFeatures = bandPass.calculateBandPassSpectrogram(data, 0, data.length, true, numThreads);
		// int[] spectraFeatures = bandPass.calculateBandPassSpectrogram(data, 0, data.length, true, numThreads);

		windowNumTimeFrames = spectraFeatures.length / numFrequencyBands;
		int[][] imageBandValues = new int[windowNumTimeFrames][numFrequencyBands];

		for (int t = 0; t < windowNumTimeFrames; t++) {
			for (int f = 0; f < numFrequencyBands; f++) {
				int index = f * windowNumTimeFrames + t;
				imageBandValues[t][f] = spectraFeatures[index];
			}
		}

		return imageBandValues;

	}
}
