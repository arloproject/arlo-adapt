package arlo;

public class NesterUserSettings {

	int id;

	int user_id;
	String name;
	double windowSizeInSeconds;
	double spectraMinimumBandFrequency;
	double spectraMaximumBandFrequency;
	double spectraDampingFactor;
	int spectraNumFrequencyBands;
	double spectraNumFramesPerSecond;
	boolean showSpectra;
	boolean showWaveform;
	boolean loadAudio;
	int maxNumAudioFilesToList;

	NesterUserSettings(int id, String name, double windowSizeInSeconds, double spectraMinimumBandFrequency, double spectraMaximumBandFrequency, double spectraDampingFactor,
			int spectraNumFrequencyBands, double spectraNumFramesPerSecond, boolean showSpectra, boolean showWaveform, boolean loadAudio, int maxNumAudioFilesToList) {

		this.id = id;
		this.name = name;

		this.windowSizeInSeconds = windowSizeInSeconds;
		this.spectraMinimumBandFrequency = spectraMinimumBandFrequency;
		this.spectraMaximumBandFrequency = spectraMaximumBandFrequency;
		this.spectraDampingFactor = spectraDampingFactor;
		this.spectraNumFrequencyBands = spectraNumFrequencyBands;
		this.spectraNumFramesPerSecond = spectraNumFramesPerSecond;
		this.showSpectra = showSpectra;
		this.showWaveform = showWaveform;
		this.loadAudio = loadAudio;
		this.maxNumAudioFilesToList = maxNumAudioFilesToList;

	}

}
