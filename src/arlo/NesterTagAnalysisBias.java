package arlo;

import java.sql.Timestamp;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NesterTagAnalysisBias extends TagDiscoveryBias {

	public int freqAveragingWindowWidth;
	public int numFramesToAverageNoise;
	public boolean updateTagPositions;

	NesterTagAnalysisBias() {

		// job related
		this.id = 0;
		this.name = "";
		this.user_id = 0;
		// this.creationDate = ;
		this.numToComplete = 0;
		this.numCompleted = 0;
		this.fractionCompleted = 0;
		this.elapsedRealTime = 0;
		this.timeToCompletion = 0;
		this.isRunning = false;
		this.wasStopped = false;
		this.isComplete = false;
		this.wasDeleted = false;

		this.numFrequencyBands = 0;
		this.numTimeFramesPerSecond = 0;
		this.dampingRatio = 0;
		this.minFrequency = 0;
		this.maxFrequency = 0;

		// tag analysis related
		this.freqAveragingWindowWidth = 0;
		this.numFramesToAverageNoise = 0;
		this.updateTagPositions = false;
	}
	
	NesterTagAnalysisBias(

			// job related
			int id, String name, int user_id, Timestamp creationDate, int numToComplete, int numCompleted, double fractionCompleted, double elapsedRealTime, double timeToCompletion,
			boolean isRunning, boolean wasStopped, boolean isComplete, boolean wasDeleted,
			//
			int numFrequencyBands, double numTimeFramesPerSecond, double dampingRatio, double minFrequency, double maxFrequency,
			// tag analysis related
			int freqAveragingWindowWidth, int numFramesToAverageNoise, boolean updateTagPositions) {
		
		// job related
		this.id = id;
		this.name = name;
		this.user_id = user_id;
		this.creationDate = creationDate;
		this.numToComplete = numToComplete;
		this.numCompleted = numCompleted;
		this.fractionCompleted = fractionCompleted;
		this.elapsedRealTime = elapsedRealTime;
		this.timeToCompletion = timeToCompletion;
		this.isRunning = isRunning;
		this.wasStopped = wasStopped;
		this.isComplete = isComplete;
		this.wasDeleted = wasDeleted;

		this.numFrequencyBands = numFrequencyBands;
		this.numTimeFramesPerSecond = numTimeFramesPerSecond;
		this.dampingRatio = dampingRatio;
		this.minFrequency = minFrequency;
		this.maxFrequency = maxFrequency;

		// tag analysis related
		this.freqAveragingWindowWidth = freqAveragingWindowWidth;
		this.numFramesToAverageNoise = numFramesToAverageNoise;
		this.updateTagPositions = updateTagPositions;

	}


	public boolean getNesterTagAnalysisBias(int tagAnalysisBiasID, Connection connection) {

		Logger logger;
		logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

		if (! this.getNesterJob(tagAnalysisBiasID, connection)) {
			logger.log(Level.WARNING, "Failed to retrieve job");
			return false;
		}


		//////////////////////
		// Get Job Parameters
		logger.log(Level.INFO, "Get Job Params");	

		boolean gotParameters = true;
		String value;

		value = this.GetJobParameter("numFrequencyBands");
		if (value != null) {
			this.numFrequencyBands = Integer.parseInt(value);
		} else {
			gotParameters = false;
		}

		value = this.GetJobParameter("numTimeFramesPerSecond");
		if (value != null) {
			this.numTimeFramesPerSecond = Double.parseDouble(value);
		} else {
			gotParameters = false;
		}

		value = this.GetJobParameter("dampingRatio");
		if (value != null) {
			this.dampingRatio = Double.parseDouble(value);
		} else {
			gotParameters = false;
		}

		value = this.GetJobParameter("minFrequency");
		if (value != null) {
			this.minFrequency = Double.parseDouble(value);
		} else {
			gotParameters = false;
		}

		value = this.GetJobParameter("maxFrequency");
		if (value != null) {
			this.maxFrequency = Double.parseDouble(value);
		} else {
			gotParameters = false;
		}
		
		value = this.GetJobParameter("freqAveragingWindowWidth");
		if (value != null) {
			this.freqAveragingWindowWidth = Integer.parseInt(value);
		} else {
			gotParameters = false;
		}
		value = this.GetJobParameter("numFramesToAverageNoise");
		if (value != null) {
			this.numFramesToAverageNoise = Integer.parseInt(value);
		} else {
			gotParameters = false;
		}
		value = this.GetJobParameter("updateTagPositions");
		if (value != null) {
			this.updateTagPositions = Boolean.parseBoolean(value);
		} else {
			gotParameters = false;
		}


		if (gotParameters == false) {
			logger.log(Level.INFO, "Not all Job Parameters were found");
			return false;
		}

		return true;
	}



}
