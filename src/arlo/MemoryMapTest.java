package arlo;

import java.io.*;
import java.nio.channels.*;
import java.nio.*;

class Channel05 {

//	 static String dataPath = "/windows/junk.txt";
//	static String dataPath = "/media/My Book/junk.txt";  //rate = 0.5144694533762058
	 static String dataPath = "/home/dtcheng/junk.txt";
	static boolean createData = false;
	static long numElements = (long) 3e9;
	static int bufferNumElements = (int) 100e6;
	static int numBuffers = (int) (numElements / bufferNumElements);
	static int doubleSize = 8;
	static int elementSize = doubleSize;
	static int bufferSize = bufferNumElements * elementSize;

	static int spectraNumValues = 1;

	public static void main(String[] args) {

		System.out.println("createData        = " + createData);
		System.out.println("numElements       = " + numElements);
		System.out.println("bufferNumElements = " + bufferNumElements);
		System.out.println("numBuffers        = " + numBuffers);

		try {

			if (createData) {

				ByteBuffer buf = ByteBuffer.allocate(bufferSize);

				FileOutputStream fos = new FileOutputStream(dataPath);
				FileChannel fChan = fos.getChannel();
				long bytesWritten = 0;
				long elementIndex = 0;
				for (int i = 0; i < numBuffers; i++) {
					buf.position(0);
					for (int j = 0; j < bufferNumElements; j++) {

						double value = (double) elementIndex / numElements;

						buf.putDouble(value);

						elementIndex++;

					}
					buf.position(0);
					bytesWritten += fChan.write(buf);
				}

				buf.position(0);

				System.out.println("Bytes written: " + bytesWritten);
				System.out.println("File size: " + fChan.size() + "\n");

				// Close stream and channel and
				// make objects eligible for
				// garbage collection
				fos.close();
				fChan.close();
				fos = null;
				fChan = null;
				buf = null;
			}

			// Get a new FileChannel object for reading and writing the existing file
			FileChannel rwCh = new RandomAccessFile(dataPath, "rw").getChannel();

			// Map entire file to memory and close the channel
			long fileSize = rwCh.size();
			System.out.println("fileSize = " + fileSize);

			ByteBuffer[] mapBufs = new ByteBuffer[numBuffers];

			for (int i = 0; i < numBuffers; i++) {
				mapBufs[i] = rwCh.map(FileChannel.MapMode.READ_WRITE, (long) bufferSize * (long) i, bufferSize);
			}

			rwCh.close();

			randomlyProbeData(mapBufs);

			System.exit(0);

			showData(mapBufs, "Map contents");

			// Modify one value in the middle
			// of the map buffer
			mapBufs[0].position(0);
			mapBufs[0].putDouble(99.999);
			showData(mapBufs, "Modified map contents");

			if (false) {
				// Read and display the contents
				// of the file
				// Get new channel for read only
				FileChannel newInCh = new RandomAccessFile(dataPath, "r").getChannel();

				// Allocate a new ByteBuffer
				ByteBuffer newBuf = ByteBuffer.allocate((int) fileSize);

				// Read file data into the new
				// buffer, close the channel, and
				// display the data.
				System.out.println("Bytes read = " + newInCh.read(newBuf));
				newInCh.close();

				// showData(newBuf, "Modified file contents");
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
		}

	}// end main

	// ---------------------------------//
	static void randomlyProbeData(ByteBuffer[] bufs) {

		long startTime = System.currentTimeMillis();
		int spectraCount = 0;
		int valueCount = 0;
		double valueSum = 0.0;
		while (true) {
			int bufferIndex = (int) (Math.random() * numBuffers);
			int bufferElementIndex = (int) (Math.random() * (bufferNumElements - spectraNumValues + 1));
			bufs[bufferIndex].position((int) (bufferElementIndex * elementSize));
			for (int j = 0; j < spectraNumValues; j++) {

				double value = bufs[bufferIndex].getDouble();

				valueSum += value;
				valueCount++;

			}

			spectraCount++;

			System.out.println("spectraCount = " + spectraCount);
			System.out.println("valueSum / count = " + valueSum / valueCount);

			double duration = (System.currentTimeMillis() - startTime) / 1000.0;
			double rate = spectraCount / duration;
			System.out.println("rate = " + rate);
		}

	}

	// ---------------------------------//

	static void showData(ByteBuffer[] bufs, String label) {
		// Displays byte buffer contents

		// Save position
		int[] pos = new int[numBuffers];

		for (int i = 0; i < numBuffers; i++) {
			pos[i] = bufs[i].position();
		}

		// Set position to zero
		System.out.println(label);
		for (int i = 0; i < numBuffers; i++) {
			bufs[i].position(0);
		}

		for (long i = 0; i < numElements; i++) {

			int bufIndex = (int) (i / bufferNumElements);

			double value = bufs[bufIndex].getDouble();

			if (i % 1000000 == 0)
				System.out.println(i + "\t" + value);
		}
		System.out.println();// new line
		// Restore position and return

		for (int i = 0; i < numBuffers; i++) {
			bufs[i].position(pos[i]);
		}

	}// end showBufferData

	// ---------------------------------//

}// end class Channel05 definition