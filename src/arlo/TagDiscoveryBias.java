package arlo;

import java.io.File;
import java.text.Format;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class TagDiscoveryBias extends NesterJob {

	// REPRESENTATION BIAS
	public int numFrequencyBands;
	public double numTimeFramesPerSecond;
	public double dampingRatio;
	public double minFrequency;
	public double maxFrequency;

	TagDiscoveryBias() {

	}

	public TagDiscoveryBias(int numFrequencyBands, double numTimeFramesPerSecond, double dampingRatio, double minFrequency, double maxFrequency) {
		this.numFrequencyBands = numFrequencyBands;
		this.numTimeFramesPerSecond = numTimeFramesPerSecond;
		this.dampingRatio = dampingRatio;
		this.minFrequency = minFrequency;
		this.maxFrequency = maxFrequency;
	}

	public String getSpectraComputationBiasString() {
		
		String dampingRatioString = String.format("%06.4f", dampingRatio);
		
		String string = "numFrequencyBands=" + numFrequencyBands + "numTimeFramesPerSecond=" + numTimeFramesPerSecond + "dampingRatio=" + dampingRatioString + "minFrequency="
				+ minFrequency + "maxFrequency=" + maxFrequency + "user_id=" + this.user_id;
		return string;

	}

	public String getSpectraMemoryMapFilePath() {
		return ArloSettings.bigDataCacheDirectoryPath + File.separator + getSpectraComputationBiasString() + ".mm";
	}

	/** \brief Update a Job in the database with the current fractionCompleted of this job.
	* 
	* @param connection An opened database connection.
	# @return True if success, False on any error
	*/

	public boolean updateFractionCompletedForTagAnalysis(Connection connection) {
		return updateFractionCompletedForTagAnalysis(this.id, this.fractionCompleted, connection);
	}

	/** \brief Update a Job in the database with the current fractionCompleted.
	* 
	* @param tagDiscoveryBiasId Database id of the Job to update
	* @param fractionCompleted Fraction of the Job complete, TODO 0.0 - 100.0 percent, or 0.0 to 1.0.
	* @param connection An opened database connection.
	# @return True if success, False on any error
	*/

	public static boolean updateFractionCompletedForTagAnalysis(int tagDiscoveryBiasId, double fractionCompleted, Connection connection) {
		try {
			Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			ResultSet uprs = stmt.executeQuery("SELECT * FROM tools_jobs where id=" + tagDiscoveryBiasId);
			uprs.next();
			uprs.updateDouble("fractionCompleted", fractionCompleted);
			uprs.updateRow();
			uprs.close();
			stmt.close();
			return true;
		} catch (Exception e) {
			Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
			logger.log(Level.SEVERE, "Exception = " + e);
			return false;
		}
	}


}
