package system;

public class IO {

	public static void println(String string) {
		System.out.println(string);
	}
	public static void println(int value) {
		System.out.println(value);
	}
	public static void println(Object object) {
		System.out.println(object);
	}

	public static void println() {
		System.out.println();
	}

}
