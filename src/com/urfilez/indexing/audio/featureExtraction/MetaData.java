package com.urfilez.indexing.audio.featureExtraction;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author kris@urfilez.com
 */
public class MetaData implements Serializable, Comparable<MetaData> {

    public final static long serialVersionUID = 837495697362L;
    public int trackID = -1;
    public long extractionTime = -1;
    public String audioFilePath = null;
    public String metaFilePath = null;
    public float audioSampleRate = -1;
    public float spectraSampleRate = -1;
    public int framesPerWindow = -1;
    public int numWindows = -1;
    public float[][] bandEnergyMeanDistribution = null;
    public float[][] bandEnergyDeviationDistribution = null;
    public float[][] bandEnergyPathIntegralDistribution = null;
    public float[][] bandEnergyContourDistribution = null;
    public float[][] bandEnergyPitchDistribution = null;
    public float[][] rhythmEnergyDistribution = null;
    public float[][] noteEnergyDistribution = null;
    
    public float[][][] floatFeatures = null;
    
    public short[][] bandEnergyMeanDistributionShort = null;
    public short[][] bandEnergyDeviationDistributionShort = null;
    public short[][] bandEnergyPathIntegralDistributionShort = null;
    public short[][] bandEnergyContourDistributionShort = null;
    public short[][] bandEnergyPitchDistributionShort = null;
    public short[][] rhythmEnergyDistributionShort = null;
    public short[][] noteEnergyDistributionShort = null;
    
    public short[][][] shortFeatures = null;
    
//    public String genre;
//    public String artist;
    public List<Integer> artistIDs;
    public int albumID;
//    public String title;
//    public int genreIndex = -1;
//    public int artistIndex = -1;
//    public int albumIDIndex = -1;
//    public int trackIDIndex = -1;

    @Override
    public String toString() {

        String out = "-------------------------------------------------\nMetadata object:\n";

        out += "albumID: " + this.albumID + "\n";
//        out += "albumIDIndex: " + this.albumIDIndex + "\n";
//        out += "artist: " + this.artist + "\n";
        out += "artistIDs: " + this.artistIDs + "\n";
//        out += "artistIndex: " + this.artistIndex + "\n";
        out += "audioFilePath: " + this.audioFilePath + "\n";
        out += "audioSampleRate: " + this.audioSampleRate + "\n";
        out += "extractionTime: " + this.extractionTime + "\n";
        out += "framesPerWindow: " + this.framesPerWindow + "\n";
//        out += "genre: " + this.genre + "\n";
//        out += "genreIndex: " + this.genreIndex + "\n";
        out += "metaFilePath: " + this.metaFilePath + "\n";
        out += "numWindows: " + this.numWindows + "\n";
        out += "serialVersionUID: " + serialVersionUID + "\n";
        out += "spectraSampleRate: " + this.spectraSampleRate + "\n";
//        out += "title: " + this.title + "\n";
        out += "trackID: " + this.trackID + "\n";
//        out += "trackIDIndex: " + this.trackIDIndex + "\n";

//        out += "bandEnergyContourDistribution:\n";
//        for (int i = 0; i < bandEnergyContourDistribution.length; i++) {
//            out += i + ": ";
//            for (int j = 0; j < bandEnergyContourDistribution[i].length; j++) {
//                out += bandEnergyContourDistribution[i][j] + "\t";
//            }
//            out += "\n";
//        }
//        
//        out += "bandEnergyDeviationDistribution:\n";
//        for (int i = 0; i < bandEnergyDeviationDistribution.length; i++) {
//            out += i + ": ";
//            for (int j = 0; j < bandEnergyDeviationDistribution[i].length; j++) {
//                out += bandEnergyDeviationDistribution[i][j] + "\t";
//            }
//            out += "\n";
//        }
//        
//        out += "bandEnergyMeanDistribution:\n";
//        for (int i = 0; i < bandEnergyMeanDistribution.length; i++) {
//            out += i + ": ";
//            for (int j = 0; j < bandEnergyMeanDistribution[i].length; j++) {
//                out += bandEnergyMeanDistribution[i][j] + "\t";
//            }
//            out += "\n";
//        }
//        
//        out += "bandEnergyPathIntegralDistribution:\n";
//        for (int i = 0; i < bandEnergyPathIntegralDistribution.length; i++) {
//            out += i + ": ";
//            for (int j = 0; j < bandEnergyPathIntegralDistribution[i].length; j++) {
//                out += bandEnergyPathIntegralDistribution[i][j] + "\t";
//            }
//            out += "\n";
//        }
//        
//        out += "bandEnergyPitchDistribution:\n";
//        for (int i = 0; i < bandEnergyPitchDistribution.length; i++) {
//            out += i + ": ";
//            for (int j = 0; j < bandEnergyPitchDistribution[i].length; j++) {
//                out += bandEnergyPitchDistribution[i][j] + "\t";
//            }
//            out += "\n";
//        }
//        
//        out += "noteEnergyDistribution:\n";
//        for (int i = 0; i < noteEnergyDistribution.length; i++) {
//            out += i + ": ";
//            for (int j = 0; j < noteEnergyDistribution[i].length; j++) {
//                out += noteEnergyDistribution[i][j] + "\t";
//            }
//            out += "\n";
//        }
//        
//        out += "rhythmEnergyDistribution:\n";
//        for (int i = 0; i < rhythmEnergyDistribution.length; i++) {
//            out += i + ": ";
//            for (int j = 0; j < rhythmEnergyDistribution[i].length; j++) {
//                out += rhythmEnergyDistribution[i][j] + "\t";
//            }
//            out += "\n";
//        }
        out += "-------------------------------------------------\n";
        return out;
    }

    /** Creates a new instance of BandPassStream */
    public MetaData() {
    }

    public int compareTo(MetaData object) {

//        if (this.genreIndex != other.genreIndex) {
//            if (this.genreIndex < other.genreIndex) {
//                return -1;
//            } else {
//                return 1;
//            }
//        }
//        if (this.artistIndex != other.artistIndex) {
//            if (this.artistIndex < other.artistIndex) {
//                return -1;
//            } else {
//                return 1;
//            }
//        }
//        if (this.albumIDIndex != other.albumIDIndex) {
//            if (this.albumIDIndex < other.albumIDIndex) {
//                return -1;
//            } else {
//                return 1;
//            }
//        }
        return this.trackID - object.trackID;



    }
    
    public void tclean() {
        
        bandEnergyMeanDistribution         = null;
        bandEnergyDeviationDistribution    = null;
        bandEnergyPathIntegralDistribution = null;
        bandEnergyContourDistribution      = null;
        bandEnergyPitchDistribution        = null;
        rhythmEnergyDistribution           = null;
        noteEnergyDistribution             = null;
        
        bandEnergyMeanDistributionShort         = null;
        bandEnergyDeviationDistributionShort    = null;
        bandEnergyPathIntegralDistributionShort = null;
        bandEnergyContourDistributionShort      = null;
        bandEnergyPitchDistributionShort        = null;
        rhythmEnergyDistributionShort           = null;
        noteEnergyDistributionShort             = null;
    }

    public void clean() {
        
    	audioFilePath = null;
        metaFilePath = null;
        
        tclean();
        
        floatFeatures = null;
        shortFeatures = null;
        artistIDs = null;
    }
    
    
    /*
     * get feature matrix
     */
    public float[][] floatTimbreMean()
     { return this.floatFeatures[0]; } 
         
    public short[][] shortTimbreMean()
     { return this.shortFeatures[0]; }
         
    public float[][] floatTimbreCovariance()
     { return this.floatFeatures[1]; } 
         
    public short[][] shortTimbreCovariance()
     { return this.shortFeatures[1]; }
    
    public float[][] floatLoudness()
     { return this.floatFeatures[2]; } 

    public short[][] shortLoudness()
     { return this.shortFeatures[2]; }
         
    public float[][] floatKey()
     { return this.floatFeatures[3]; } 
         
    public short[][] shortKey()
     { return this.shortFeatures[3]; }
    
    
    public float[][] floatRhythm()
     { return this.floatFeatures[4]; } 
         
    public short[][] shortRhythm()
     { return this.shortFeatures[4]; }
    
    
    /* 
     * get float features as matrix where the columns are: [timbreMean timbreCovariance loudness key rhythm]
     */
    public float[][] combinedFloatFeatures ()
     { if (this.floatFeatures != null)
        { int n_feat = 0;
          for (int f=0; f<this.floatFeatures.length; f++)
              n_feat += this.floatFeatures[f][0].length;
          float [][] feat = new float [this.numWindows][n_feat];
          for (int i=0; i<feat.length; i++)
            { int j=0;
              for (int f=0; f<this.floatFeatures.length; f++)
               { for (int k=0; k<this.floatFeatures[f][i].length; k++)
                  { feat[i][j] = this.floatFeatures[f][i][k];
                    j++;
                  }
               }
            }
          return feat;   
        }
       else
         return null;
     }
    
    
    /* 
     * get short features as matrix where the columns are: [timbreMean timbreCovariance loudness key rhythm]
     */
    public short[][] combinedShortFeatures ()
     { if (this.shortFeatures != null)
        { int n_feat = 0;
          for (int f=0; f<this.shortFeatures.length; f++)
              n_feat += this.shortFeatures[f][0].length;
          short [][] feat = new short [this.numWindows][n_feat];
          for (int i=0; i<feat.length; i++)
            { int j=0;
              for (int f=0; f<this.shortFeatures.length; f++)
               { for (int k=0; k<this.shortFeatures[f][i].length; k++)
                  { feat[i][j] = this.shortFeatures[f][i][k];
                    j++;
                  }
               }
            }
          return feat;   
        }
       else
         return null;
     }
    
    
    /*
     * get float timbre features as matrix where the columns are: [timbreMean timbreCovariance]
     */
    public float[][] combinedTimbreFloatFeatures ()
     { if (this.floatFeatures != null)
        { int n_feat = this.floatFeatures[0][0].length + this.floatFeatures[1][0].length;
          float [][] feat = new float [this.numWindows][n_feat];
          for (int i=0; i<feat.length; i++)
            { int j=0;
              for (int f=0; f<2; f++)
               { for (int k=0; k<this.floatFeatures[f][i].length; k++)
                  { feat[i][j] = this.floatFeatures[f][i][k];
                    j++;
                  }
               }
            }
          return feat;   
        }
       else
         return null;
     }
    
    
     /*
     * get float timbre features as matrix where the columns are: [timbreMean timbreCovariance]
     */
    public short[][] combinedTimbreShortFeatures ()
     { if (this.shortFeatures != null)
        { int n_feat = this.shortFeatures[0][0].length + this.shortFeatures[1][0].length;
          short [][] feat = new short [this.numWindows][n_feat];
          for (int i=0; i<feat.length; i++)
            { int j=0;
              for (int f=0; f<2; f++)
               { for (int k=0; k<this.shortFeatures[f][i].length; k++)
                  { feat[i][j] = this.shortFeatures[f][i][k];
                    j++;
                  }
               }
            }
          return feat;   
        }
       else
         return null;
     }
    
         
}