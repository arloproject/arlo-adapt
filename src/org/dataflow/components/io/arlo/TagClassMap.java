/**
 * 
 */
package org.dataflow.components.io.arlo;

import java.util.HashMap;

/**
 * This singleton simply keeps track of the id to class name mapping.
 * @author redman
 */
class TagClassMap {
    /** this map maps tags names to tag id. */
    public transient static final HashMap<String, Integer> tagIndexMap = new HashMap<String, Integer>();
    
    /** this map maps tags names to tag id. */
    public transient static final HashMap<Integer, String> tagClassNameMap = new HashMap<Integer, String>();
    
    /** Keep track of the next available tag index. */
    static private int nextTagIndex = 0;

    /**
     * Get the unique integer id for the tag.
     * @param classname the string name of the class.
     * @return the id of the class, create a new entry if needed.
     */
    static int getClassId(String classname) {
        synchronized (tagIndexMap) {
            if (tagIndexMap.containsKey(classname))
                return tagIndexMap.get(classname);
            else {
                int tid = nextTagIndex;
                tagIndexMap.put(classname, tid);
                tagClassNameMap.put(tid, classname);
                nextTagIndex++;
                return tid;
            }
        }
    }
    /**
     * Given a unique id, return the class name.
     * @param id the string name of the class.
     * @return the classname.
     */
    static String getClassName(int id) {
        synchronized (tagIndexMap) {
            return tagClassNameMap.get(id);
        }
    }

}
