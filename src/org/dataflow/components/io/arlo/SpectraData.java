package org.dataflow.components.io.arlo;

import java.util.HashMap;

import com.google.gson.internal.LinkedTreeMap;

/**
 * The spectra data organized into a hashmap, contains the frequency bands for all the slices.
 */
class SpectraData {
    /** contains the frames, freq bands and params */
    HashMap<String, Object> spectraData;
    
    /** just dump the data. */
    public String toString() {
        return spectraData.toString();
    }
    
    /**
     * the media file ID is an integer (I hope).
     * @return the medial file id.
     */
    public long getMediaFileID() {
        LinkedTreeMap<?, ?> f = (LinkedTreeMap<?, ?>)this.spectraData.get("params");
        return ((Double)f.get("mediaFileId")).longValue();
    }
    
    /**
     * the media file ID is an integer (I hope).
     * @return the medial file id.
     */
    public double getStartTime() {
        LinkedTreeMap<?, ?> ltm = (LinkedTreeMap<?, ?>)this.spectraData.get("params");
        return ((Double)ltm.get("startTime")).doubleValue();
    }
    /**
     * the media file ID is an integer (I hope).
     * @return the medial file id.
     */
    public double getEndTime() {
        LinkedTreeMap<?, ?> ltm = (LinkedTreeMap<?, ?>)this.spectraData.get("params");
        return ((Double)ltm.get("endTime")).doubleValue();
    }
}