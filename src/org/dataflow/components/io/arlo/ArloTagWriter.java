/**
 * 
 */
package org.dataflow.components.io.arlo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import org.apache.http.client.ClientProtocolException;
import org.dataflow.pipes.BufferedDataflowPipe;
import org.meandre.annotations.Component;
import org.meandre.annotations.Component.Licenses;
import org.meandre.annotations.Component.Mode;
import org.meandre.annotations.ComponentInput;
import org.seasr.datatypes.datamining.table.Column;
import org.seasr.datatypes.datamining.table.PredictionTable;

/**
 * Write the machine labeled tags out to the system.
 * @author redman
 */
@Component( name = "Arlo Tag Writer", creator = "Tom Redman",
    baseURL = "meandre://seasr.org/components/foundry/", mode = Mode.compute, rights = Licenses.UofINCSA,
    tags = "ARLO, writer, machine tag",
    description = "This module will write label data to the arlo system. The tags are culled from a prediction table "
        + "generate by a model build against known labeled data")
public class ArloTagWriter extends ArloReader {
    
    /** the trained classifer. */
    @ComponentInput(name = "Predictions Table", description = "This table contains data labeled by a model.")
    public BufferedDataflowPipe<PredictionTable> inputPredictions = new BufferedDataflowPipe<PredictionTable>();

    /**
     * This module is always ready, it requires no inputs, since it is a head, 
     * this method will always return false so it does not continue to run.
     * @see org.dataflow.components.DataflowComponent#isReady()
     */
    @Override
    public boolean isReady() {
        return inputPredictions.hasData();
    }
    
    /** the start column. */
    private int start_column_indx = -1;
    
    /** the end column. */
    private int end_column_indx = -1;
    
    /** the media id column. */
    private int mediaid_column_indx = -1;

    /** the tag classes. */
    private HashMap<String, Integer> tagClassNameToIDMap = new HashMap<String, Integer>();
    
    /**
     * @see org.dataflow.components.AbstractDataflowComponent#doit()
     */
    @Override
    protected void doit() throws Exception {
        boolean writetag = true;
        PredictionTable pt = inputPredictions.pullData();
        
        // first find the columns with the start and end time, presumably their labels 
        // start with "start" and "end"
        for (int indx = 0; indx < pt.getNumColumns(); indx++) {
            Column c = pt.getColumn(indx);
            if (c.getLabel().startsWith(this.START_COLUMN_NAME)) {
                if (start_column_indx == -1)  {
                    start_column_indx = indx;
                } else {
                    log.error("There was more than one column label indicating the start time column : "+
                                    c.getLabel()+" is ignored, "+pt.getColumnLabel(start_column_indx)+" was chosen.");
                }
            } else if (c.getLabel().startsWith(this.END_COLUMN_NAME)) {
                if (end_column_indx == -1) {
                    end_column_indx = indx;
                } else {
                    log.error("There was more than one column label indicating the end time column : "+
                                    c.getLabel()+" is ignored, "+pt.getColumnLabel(end_column_indx)+" was chosen.");
                }
            } else if (c.getLabel().startsWith(this.MEDIAFILEID_COLUMN_NAME)) {
                if (mediaid_column_indx == -1) {
                    mediaid_column_indx = indx;
                } else {
                    log.error("There was more than one column label indicating the media file id : "+
                                    c.getLabel()+" is ignored, "+pt.getColumnLabel(mediaid_column_indx)+" was chosen.");
                }
            }
        }

        // check our results
        if (start_column_indx == -1) {
            log.error("No start column was found.");
            return;
        }
        if (end_column_indx == -1) {
            log.error("No end column was found.");
            return;
        }
        if (mediaid_column_indx == -1) {
            log.error("No media id column was found.");
            return;
        }
        
        /** this class simply implements comparable against a table row allowing to 
         * do a sort quickly and easily.
         * @author redman
         */
        class ComparableTableRowWrapper implements Comparable<ComparableTableRowWrapper> {
            
            /** the index of the row. */
            int index = 0;
            
            /** the table the index ponts to. */
            PredictionTable table = null;
            
            /**
             * Create the table row wrapper.
             * @param i the index.
             * @param t the table.
             */
            ComparableTableRowWrapper(int i, PredictionTable t) {
                this.table = t;
                this.index = i;
            }
            @Override
            public int compareTo(ComparableTableRowWrapper o) {
                if (getMediaId() == o.getMediaId()) {
                    if (getStart() > o.getStart())
                        return 1;
                    else if (getStart() == o.getStart())
                        return 0;
                    else
                        return -1;
                } else if (getMediaId() > o.getMediaId()) {
                    return 1;
                } else
                    return -1;
            }
            
            /**
             * @return the media id at this row.
             */
            int getMediaId() {
                return table.getInt(index, mediaid_column_indx);
            }
            
            /**
             * @return the starting offset of the frame
             */
            double getStart() {
                return table.getDouble(index, start_column_indx);      
            }
            /**
             * @return the starting offset of the frame
             */
            double getEnd() {
                return table.getDouble(index, end_column_indx);
            }
        }
        
        // To perform this take, we need to sort the table on two keys, first the mediafileId (to cluster the same file together),
        // and on time offset so we can tag the longest consecutive portion of audio that is or is not applause. ComparableTableRowWrapper
        // will allow this.
        ArrayList<ComparableTableRowWrapper> roworder = new ArrayList<ComparableTableRowWrapper>();
        for (int i = 0; i < pt.getNumRows() ; i++)
            roworder.add(new ComparableTableRowWrapper(i, pt));
        Collections.sort(roworder);
        
        int lastprediction = -1;
        ComparableTableRowWrapper previous = null;
        double startTime = -1;
        ArrayList<TagData> tags = new ArrayList<TagData>();
        for (ComparableTableRowWrapper trw : roworder) {
            int index = trw.index;
            int prediction = pt.getIntPrediction(index, 0);
            if (lastprediction == -1) {
                lastprediction = prediction;
                startTime = trw.getStart();
            } else if (prediction != lastprediction || trw.getMediaId() != previous.getMediaId()) {
                
                // either different prediction within a file, or a different file.
                TagData td = new TagData();
                td.startTime = startTime;
                td.endTime = previous.getEnd();
                td.mediaFileId = previous.getMediaId();
                td.setClassNameIndex(lastprediction);
                tags.add(td);
                
                // start a new tag.
                startTime = trw.getStart();
                lastprediction = prediction;
            }
            previous = trw;
        }
        
        // add the last tag 
        if (startTime != -1) {
            TagData lastone = new TagData();
            lastone.startTime = startTime;
            lastone.endTime = previous.getEnd();
            lastone.mediaFileId = previous.getMediaId();
            lastone.setClassNameIndex(lastprediction);
            tags.add(lastone);
        }

        // write the tag.
        if (writetag) {
            ArrayList<MachineTaggedData> machtags = new ArrayList<MachineTaggedData>(tags.size());
            for (TagData td : tags) {
                machtags.add(new MachineTaggedData(td, Integer.parseInt(this.tags), 
                    this.getTagClassID(td.getClassName())));
                
            }
            String json = gson.toJson(machtags);
            String posttag = "/api/v0/bulkTagCreate/"+this.getProject()+"/";
            StringBuffer sb = new StringBuffer("Posting a set of machine generated tags.\n"+json+"\n");
            for (TagData td : tags) {
                sb.append(td.mediaFileId+","+td.startTime+","+td.endTime+"\n");
            }
            log.info(sb.toString());

            this.postRecord(posttag, json);
        }
    }

    /**
     * wrapper class for the returened tag class id.
     * @author redman
     */
    static class TagClassId {
        
        /** the tag id. */
        int id;
        
        /** the user. */
        int user;
        
        /** the project id. */
        int project;
        
        /** the name we fetch an ID for. */
        String className;
        
        /** I have no idea what this  is. */
        String displayName;
    }
    
    /**
     * get the tag id from the tag name.
     * @param className the named classification.
     * @return the class id.
     * @throws IOException 
     * @throws ClientProtocolException 
     */
    private int getTagClassID(String className) throws ClientProtocolException, IOException {
        Integer tmp = this.tagClassNameToIDMap.get(className);
        if (tmp == null) {
            String json = this.postRecord("/api/v0/findOrCreateTagClass/"+this.getProject()+"/", "{\"tagClassName\":\""+className+"\"}");
            TagClassId tagidwrapper = gson.fromJson(json, TagClassId.class);
            this.tagClassNameToIDMap.put(className, tagidwrapper.id);
            return tagidwrapper.id;
        } else
            return tmp;
    }

    /**
     * @return the inputPredictions
     */
    public BufferedDataflowPipe<PredictionTable> getInputPredictions() {
        return inputPredictions;
    }

    /**
     * @param inputPredictions the inputPredictions to set
     */
    public void setInputPredictions(BufferedDataflowPipe<PredictionTable> inputPredictions) {
        this.inputPredictions = inputPredictions;
    }
}
