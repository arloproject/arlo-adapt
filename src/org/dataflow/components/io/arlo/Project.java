package org.dataflow.components.io.arlo;

import java.util.ArrayList;

/** this is a class representing the JSON project rep that gets returned. */
class Project {
    /** project id. */
    public int id;
    /** the user account. */
    public int user;
    /** the type. */
    public int type;
    
    /** project name. */
    public String name;
    
    /** list of media files in project. */
    public ArrayList<Integer> mediaFiles;
    
    /** project creation date. */
    public String creationDate;
    
    public String toString() {
        return ""+id+" user:"+user+" type:"+type+" name:"+name+" mediafiles:"+mediaFiles;
    }
}