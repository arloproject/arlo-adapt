package org.dataflow.components.io.arlo;


/**
 * the tag metadata identifies the media file and the time offsets for the frame.
 * @author redman
 */
class TagData {
        
    /** the label used for classification. */
    private String className = "Unknown";

    /** this is the id of this labeled data. */
    int id = -1;
    
    /** the id of the tag set. */
    int tagId = -1;
    
    /** file id. */
    int mediaFileId;
    
    /** file path. */
    String mediaFilePath = null;
    
    /** start time. */
    double startTime = 0.0;
    
    /** end time. */
    double endTime = 0.0;
    
    /** minimum frequency. */
    double minFrequency = 20.0;
    
    /** max frequency. */
    double maxFrequency = 20000.0;
    
    /** I suspect this is a sort of confidence. */
    
    /** just dump the data. */
    public String toString() {
        return mediaFileId+":"+startTime+":"+endTime+":"+className;
    }

    /**
     * @return the className
     */
    public String getClassName() {
        return className;
    }

    /**
     * @param className the className to set
     */
    public void setClassName(String className) {
        this.className = className;
        this.tagId = TagClassMap.getClassId(className);
    }
    
    /**
     * Given the index of the class, set the class name and tag id.
     * @param index the id of the class.
     */
    public void setClassNameIndex(int index) {
        this.tagId = index;
        this.className = TagClassMap.getClassName(index);
    }
}