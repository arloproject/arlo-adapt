package org.dataflow.components.io.arlo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import org.apache.http.client.ClientProtocolException;
import org.meandre.annotations.ComponentProperty;
import org.seasr.datatypes.datamining.table.Column;
import org.seasr.datatypes.datamining.table.ColumnTypes;
import org.seasr.datatypes.datamining.table.ExampleTable;
import org.seasr.datatypes.datamining.table.Table;
import org.seasr.datatypes.datamining.table.basic.BasicTableFactory;
import org.seasr.datatypes.datamining.table.basic.MutableTableImpl;

import com.google.gson.Gson;

/**
 * This is the abstract base class has some useful utilities for fetching data from
 * the arlo system. It must be subclassed by a reader that gets the specific data of 
 * interest.
 * @author redman
 */
abstract public class ArloReader extends HTTPAccessBase {
    /** the tag set also tells use the project. */
    @ComponentProperty(name = "TagSet",
        description = "the id of the tagset to fetch.", defaultValue = "")
    protected String tags = "863";
    
    /** this must be set by first accessing the tag set and getting the project, private so subclasses use accessor. */
    private String project = null;
    
    /** the prject id number. */
    @ComponentProperty(name = "Frequency Bands",
        description = "The number of frequency bands.", defaultValue = "128")
    protected int frequencyBands = 64;
    
    /** the prject id number. */
    @ComponentProperty(name = "Testing Scale",
        description = "The scale for testing.", defaultValue = "-1")
    protected int testScale = -1;
    
    /** the pr0ject id number. */
    @ComponentProperty(name = "Slices Per Example",
        description = "The number of recorded time slices to include in each example. This will define the number of features per row.", 
        defaultValue = "1")
    protected int slices = 1;
    
    /** the pr0ject id number. */
    @ComponentProperty(name = "Slices Per Second",
        description = "The number of recorded time slices to include in each example. This will define the number of features per row.", 
        defaultValue = "2")
    protected int slicesPerSecond = 2;
    
    /** the damping. */
    @ComponentProperty(name = "Damping Factor", description = "The damping factor", defaultValue = "0.5")
    protected double dampingFactor = 0.5;
    
    /** the damping. */
    @ComponentProperty(name = "Min Frequency", description = "The damping factor", defaultValue = "600")
    protected int minFrequency = 600;
    
    /** the damping. */
    @ComponentProperty(name = "Max Frequency", description = "The damping factor", defaultValue = "5000")
    protected int maxFrequency = 5000;
    
    /** used to create the new table. */
    protected BasicTableFactory btf = new BasicTableFactory();
    
    /** the Gson json parser. */
    protected Gson gson = new Gson();
    
    /** the random int generator. */
    protected Random random = new Random(-1);
    
    /** start column name. */
    final protected String START_COLUMN_NAME = "Start";
    
    /** end column name. */
    final protected String END_COLUMN_NAME = "End";
    
    /** metadata column name. */
    final protected String MEDIAFILEID_COLUMN_NAME = "MediaFileID";
    

    /**
     * get the project metadata, including the list of media file ids.
     * @return the project data.
     * @throws IOException 
     * @throws ClientProtocolException 
     */
    protected Project getProjectMetadata() throws ClientProtocolException, IOException {
        // first fetch all the media files in the set.
        String json = this.getRecord("/api/v0/project/"+getProject()+"/");
        return gson.fromJson(json, Project.class);
    }

    /**
     * Get the path, this is used to make sure we are asking for the right stuff during testing.
     * @param instId the project id.
     * @return the path to issue.
     */
    public String getPath(int instId) {
        return "/api/v0/getAudioSpectraData/" + instId + "/?startTime=0.0&endTime=20000.0&dampingFactor="+getDampingFactor()+
        "&minFrequency="+getMinFrequency()+"&maxFrequency="+getMaxFrequency()+"&numTimeFramesPerSecond=" + slicesPerSecond + 
        "&numFrequencyBands=" + frequencyBands;
    }

    /**
     * produce the example table.
     * @param labels the labeled data.
     * @param bands the number of frequency bands.
     * @return the example table.
     */
    protected ExampleTable generateExampleTable(ArrayList<SpectraLabelData> labels, int bands) {
        
        // create a table with a column for each band, and a column for the label.
        MutableTableImpl et = (MutableTableImpl)btf.createTable();
        HashMap<String, Integer> fu = TagClassMap.tagIndexMap;
        StringBuffer sb = new StringBuffer("Class Nominalization for \""+getPath(-1)+"\":");
        for (String key : fu.keySet()) {
            sb.append(' ');
            sb.append(key);
            sb.append('=');
            sb.append(fu.get(key));
        }
        log.info(sb);
        
        // the table will contain for each example, <bands> frequency band measures for each 
        // slice, and <slices> sets of frequencies for every example.
        for (int slice = 0 ; slice < slices ; slice++) {
            String label = "FB-"+slice+"-";
            for (int i = 0; i < bands ; i++) {
                Column c = btf.createColumn(ColumnTypes.DOUBLE);
                c.setLabel(label+i);
                et.addColumn(c);
            }
        }
        Column c = btf.createColumn(ColumnTypes.NOMINAL);
        c.setLabel("Class");
        et.addColumn(c);
        
        Column mfc = btf.createColumn(ColumnTypes.INTEGER);
        mfc.setLabel(this.MEDIAFILEID_COLUMN_NAME);
        et.addColumn(mfc);
        
        Column tidc = btf.createColumn(ColumnTypes.INTEGER);
        tidc.setLabel("TagID");
        et.addColumn(tidc);
        
        Column startc = btf.createColumn(ColumnTypes.DOUBLE);
        startc.setLabel(this.START_COLUMN_NAME);
        et.addColumn(startc);
        
        Column endc = btf.createColumn(ColumnTypes.DOUBLE);
        endc.setLabel(this.END_COLUMN_NAME);
        et.addColumn(endc);

        // compute the number of rows, and add them all at once, otherwise, we
        // will spend lots of time moving data around.
        int totalrows = 0;
        for (SpectraLabelData sld : labels) {
            totalrows += sld.countRows(slices);
        }
        et.addRows(totalrows);
        
        // now add the data to it.
        int where = 0;
        for (SpectraLabelData sld : labels) {
            where = sld.addLabeledData(et, where, slices, this.slicesPerSecond);
        }
                        
        // construct a new column with the nominal integers for the class
        Column c2 = btf.createColumn(ColumnTypes.NOMINAL);
        c2.setLabel("ClassID");
        c2.addRows(totalrows);
        et.addColumn(c2);
        int classidcolumn = et.getNumColumns() - 1;

        for (int i = 0; i < c.getNumRows(); i++) {
            String t = c.getString(i);
            Integer classid = fu.get(t);
            if (classid != null)
                et.setString(Integer.toString(classid), i, classidcolumn);
            else {
                et.setString("-1", i, classidcolumn);
            }
        }

        // set the input and output features
        ExampleTable net = btf.createExampleTable(et);
        int [] innies = new int [bands];
        for (int i = 0; i < bands; i++) 
            innies[i] = i;
        net.setInputFeatures(innies);
        int [] outies = {classidcolumn};
        net.setOutputFeatures(outies);
        return net;
    }
    
    /**
     * Show the example table.
     * @param label the label for the table.
     * @param et the table.
     */
    protected void showTable(String label, Table et) {
       // get start and end column indexes.
        int startcolindx = -1;
        int endcolindx = -1;
        int filecolindx = -1;
        for (int i = 0; i < et.getNumColumns(); i++) {
            if (et.getColumnLabel(i).equals(this.START_COLUMN_NAME))
                startcolindx = i;
            else if (et.getColumnLabel(i).equals(this.END_COLUMN_NAME))
                endcolindx = i;
            else if (et.getColumnLabel(i).equals(this.MEDIAFILEID_COLUMN_NAME))
                filecolindx = i;
        }
        StringBuffer sb = new StringBuffer(label);
        sb.append('\n');
        sb.append("file,start,end");
        sb.append('\n');

        double start = -1;
        double end = -1;
        int file = -1;
        for (int i = 0; i < et.getNumRows(); i++) {
            double nextstart = et.getDouble(i, startcolindx);
            double nextend = et.getDouble(i, endcolindx);
            int nextfile = et.getInt(i, filecolindx);
            if (start == -1) {
                start = nextstart;
                end = nextend;
                file = nextfile;
            } else if (file == nextfile) {
                if (nextend > end)
                    end = nextend;
            } else {
                sb.append(file+","+start+","+end);
                sb.append('\n');
                start = nextstart;
                end = nextend;
                file = nextfile;
            }
        }
        sb.append(file+","+start+","+end);
        sb.append('\n');
        log.info("\n"+sb.toString());        
    }
    
    /**
     * Fetch the tags associated with the provided spectral data vector. The tags
     * are added to the SpectraData object. The arguments to the fetch are :
     * <dl>
     * <dt>mediaFile</dt>
     * <dd>The media file ID to get tagging metadata.</dd>
     * <dt>startTime</dt>
     * <dd>the start time.</dd>
     * <dt>endTime</dt>
     * <dd>the end time in seconds</dd>
     * </dl>
     * @param mfid the media file ID number.
     * @param tags the tags to restrict labeling.
     * @return the json string result.
     * @throws IOException 
     * @throws ClientProtocolException 
     */
    protected TagData[] fetchTags(int mfid, String tags) throws ClientProtocolException, IOException {
        String path = "/api/v0/getTagsInAudioSegment/"+getProject()+"/?mediaFile="+mfid+
                        "&startTime=0&endTime=100000&restrictTagSets="+tags;
        String json = this.getRecord(path);
        return gson.fromJson(json, TagData[].class);
    }

    /**
     * @return the frequencyBands
     */
    public int getFrequencyBands() {
        return frequencyBands;
    }

    /**
     * @param frequencyBands the frequencyBands to set
     */
    public void setFrequencyBands(int frequencyBands) {
        this.frequencyBands = frequencyBands;
    }

    /**
     * @return the slices
     */
    public int getSlices() {
        return slices;
    }

    /**
     * @param slices the slices to set
     */
    public void setSlices(int slices) {
        this.slices = slices;
    }

    /**
     * @return the slicesPerSecond
     */
    public int getSlicesPerSecond() {
        return slicesPerSecond;
    }
    
    /**
     * @param slicesPerSecond the slicesPerSecond to set
     */
    public void setSlicesPerSecond(int slicesPerSecond) {
        this.slicesPerSecond = slicesPerSecond;
    }

    /**
     * @return the dampingFactor
     */
    public double getDampingFactor() {
        return dampingFactor;
    }

    /**
     * @param dampingFactor the dampingFactor to set
     */
    public void setDampingFactor(double dampingFactor) {
        this.dampingFactor = dampingFactor;
    }

    /**
     * @return the minFrequency
     */
    public int getMinFrequency() {
        return minFrequency;
    }

    /**
     * @param minFrequency the minFrequency to set
     */
    public void setMinFrequency(int minFrequency) {
        this.minFrequency = minFrequency;
    }

    /**
     * @return the maxFrequency
     */
    public int getMaxFrequency() {
        return maxFrequency;
    }

    /**
     * @param maxFrequency the maxFrequency to set
     */
    public void setMaxFrequency(int maxFrequency) {
        this.maxFrequency = maxFrequency;
    }

    /**
     * @return the project
     */
    public String getProject() {
        if (this.project == null) {
            String path = "/api/v0/tagSet/"+tags+"/";
            String json;
            try {
                json = this.getRecord(path);
            } catch (IOException e) {
                throw new IllegalArgumentException("Could not access the tag set with id "+tags,e);
            }
            TagSet tsd = gson.fromJson(json, TagSet.class);
            this.project = Integer.toString(tsd.project);
        }
        return this.project;
    }

    /**
     * @return the tags
     */
    public String getTags() {
        return tags;
    }

    /**
     * @param tags the tags to set
     */
    public void setTags(String tags) {
        this.tags = tags;
    }

    /**
     * @return the testScale
     */
    public int getTestScale() {
        return testScale;
    }

    /**
     * @param testScale the testScale to set
     */
    public void setTestScale(int testScale) {
        this.testScale = testScale;
    }
}