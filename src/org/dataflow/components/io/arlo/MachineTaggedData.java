/**
 * 
 */
package org.dataflow.components.io.arlo;

/**
 * This class simply so we can use gson to generate the json representation of this data.
 * @author redman
 */
public class MachineTaggedData {
    
    /** the id of the tag set. */
    int tagSet;
    
    /** the media file id. */
    int mediaFile;
    
    /** in seconds, fraction included. */
    double startTime;
    
    /** the end time in seconds. */
    double endTime;
    
    /** the min frequency. */
    double minFrequency;
    
    /** the max frequency. */
    double maxFrequency;
    
    /** the classification. */
    int tagClass;
    
    /** it's not randomly chosen. */
    boolean randomlyChosen=false;
    
    /** machine tagged is true. */
    boolean machineTagged = true;
    
    /** not user tagged. */
    boolean userTagged=false;
    
    /** we don't recall the strength, but we don't have that. */
    double strength = 1.0;
    
    /**
     * get the data from the tag data.
     * @param td the data to encode.
     * @param tagSetId the tag set id.
     * @param tagClassID the tag class id.
     */
    public MachineTaggedData(TagData td, int tagSetId, int tagClassID) {
        this.tagSet = tagSetId;
        this.mediaFile = td.mediaFileId;
        this.startTime = td.startTime;
        this.endTime = td.endTime;
        this.minFrequency = td.minFrequency;
        this.maxFrequency = td.maxFrequency;
        this.tagClass = tagClassID;
    }

}
