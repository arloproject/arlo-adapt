/**
 * 
 */
package org.dataflow.components.io.arlo;

import java.io.IOException;

import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.dataflow.components.AbstractDataflowComponent;
import org.meandre.annotations.ComponentProperty;

/**
 * This is a base class for anything needing to do HTTP access. It supports and sets 
 * up the HTTP client stuff, manages the user name and password, and url including path
 * if necessary.
 * @author redman
 */
public abstract class HTTPAccessBase extends AbstractDataflowComponent  {
    /** the account name. */
    @ComponentProperty(name = "Account",
        description = "The users account name for the host, or null if one is not required.", defaultValue = "")
    private String account = "";
    
    /** user password. */
    @ComponentProperty(name = "Password",
                    description = "The users password for the host, or null if one is not required.", defaultValue = "")
    protected String password = "";
    
    /** this is the host machine or the base URL for the restful API to support. */
    @ComponentProperty(name = "Host",
                    description = "The URL pointing to the host machine and path.", defaultValue = "")
    protected String host = "";
    
    /** this is the host machine or the base URL for the restful API to support. */
    @ComponentProperty(name = "Port",
                    description = "The http port number", defaultValue = "")
    protected int port = 8080;
    
    /** the HTTP client, commons API. */
    private CloseableHttpClient client = null;
    
    /** the execution context. */
    private HttpClientContext context = null;
    
    /** the host definition. */
    private HttpHost targetHost = null;
    
    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }
    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }
    /**
     * @return the account
     */
    public String getAccount() {
        return account;
    }
    /**
     * @param account the account to set
     */
    public void setAccount(String account) {
        this.account = account;
    }
    /**
     * @return the http client
     */
    protected HttpClient getClient() {
        if (client == null) {            
            this.targetHost = new HttpHost(this.host, this.port, "http");
            CredentialsProvider credsProvider = new BasicCredentialsProvider();
            credsProvider.setCredentials(
                    new AuthScope(targetHost.getHostName(), targetHost.getPort()),
                    new UsernamePasswordCredentials(this.account, this.password));

            // Create AuthCache instance
            AuthCache authCache = new BasicAuthCache();
            // Generate BASIC scheme object and add it to the local auth cache
            BasicScheme basicAuth = new BasicScheme();
            authCache.put(targetHost, basicAuth);

            // Add AuthCache to the execution context
            this.context = HttpClientContext.create();
            context.setCredentialsProvider(credsProvider);
            context.setAuthCache(authCache);
            client = HttpClients.createDefault();
            /*HttpParams params = client.getParams();
            HttpConnectionParams.setConnectionTimeout(params, 0);
            HttpConnectionParams.setSoTimeout(params, 0);*/
        }
        return client;
    }

    /**
     * Construct and issue the request, returning the result as a string.
     * @param path the path to add to the url.
     * @return the data as a string.
     * @throws ClientProtocolException
     * @throws IOException
     */
    protected String getRecord(String path) throws ClientProtocolException, IOException {
        
        // construct the get and issue the request.
        HttpGet httpget = this.getHttpGetter(path);
        HttpResponse response = this.getClient().execute(targetHost, httpget, context);
        String responsebody = EntityUtils.toString(response.getEntity());
        return responsebody;
    }

    /**
     * Construct and issue a put, an exception will be thrown on failure.
     * @param path the path to add to the url.
     * @param content the payload to deliver
     * @return the response text.
     * @throws ClientProtocolException
     * @throws IOException
     */
    protected String postRecord(String path, String content) throws ClientProtocolException, IOException {
        
        // construct the get and issue the request.
        HttpPost httppost = this.getHttpPost(path);
        httppost.setEntity(new StringEntity(content, ContentType.create("application/json")));
        HttpResponse response = this.getClient().execute(targetHost, httppost, context);
        int code = response.getStatusLine().getStatusCode();
        if (code < 200 || code >= 300) {
            String respstring = EntityUtils.toString(response.getEntity());
            throw new IOException ("HTTP Response Error : code = "+code+" Respone = "+respstring);
        } else {
            return EntityUtils.toString(response.getEntity());
        }
    }
    
    /**
     * Construct a getter for the given path appended to the base URL in the host
     * field.
     * @param path the path to add to the host.
     * @return the getter.
     */
    protected HttpPost getHttpPost(String path) {
        if (targetHost == null)
            getClient();
        if (path == null)
            return new HttpPost(targetHost.toURI().toString());
        else
            return new HttpPost(targetHost.toURI().toString()+"/"+path);

    }
    
    /**
     * Construct a getter for the given path appended to the base URL in the host
     * field.
     * @param path the path to add to the host.
     * @return the getter.
     */
    protected HttpGet getHttpGetter(String path) {
        if (targetHost == null)
            getClient();
        if (path == null)
            return new HttpGet(targetHost.toURI().toString());
        else
            return new HttpGet(targetHost.toURI().toString()+"/"+path);

    }
    
    /**
     * @return the host
     */
    public String getHost() {
        return host;
    }
    
    /**
     * @param host the host to set
     */
    public void setHost(String host) {
        this.host = host;
    }
    /**
     * @return the port
     */
    public int getPort() {
        return port;
    }
    /**
     * @param port the port to set
     */
    public void setPort(int port) {
        this.port = port;
    }
}
