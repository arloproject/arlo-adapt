/**
 * 
 */
package org.dataflow.components.io.arlo;

/**
 * just holds info about a tag set for json decoding.
 * @author redman
 */
public class TagSet {
    
    /** tag set id. */
    public int id;
    
    /** the user id. */
    public int user;
    
    /** the name of the set. */
    public String name;
    
    /** the project. */
    public int project;
}
