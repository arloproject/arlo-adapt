/**
 * 
 */
package org.dataflow.components.io.arlo;

import java.util.ArrayList;

import org.dataflow.pipes.BufferedDataflowPipe;
import org.meandre.annotations.Component;
import org.meandre.annotations.Component.Licenses;
import org.meandre.annotations.Component.Mode;
import org.meandre.annotations.ComponentOutput;
import org.seasr.datatypes.datamining.table.ExampleTable;

/**
 * @author redman
 */
@Component( name = "Arlo Label Data Generator", creator = "Tom Redman",
    baseURL = "meandre://seasr.org/components/foundry/", mode = Mode.compute, rights = Licenses.UofINCSA,
    tags = "ARLO, reader",
    description = "Provided with a project id, a tag id, and the number of frequency bands to produce, this module will generate"
        + " an ExampleTable containing examples of the spectral data from the media files within that project, labeled"
        + " using the tag set id provided.")
public class ArloLabelDataReader extends ArloReader {
    
    /** the trained classifer. */
    @ComponentOutput(name = "matches", description = "Returns a table of examples matching the given tag. This table includes "
        + "the media file id and the label is the last column.")
    public BufferedDataflowPipe<ExampleTable> outputInstances = new BufferedDataflowPipe<ExampleTable>();

    /**
     * This module is always ready, it requires no inputs, since it is a head, 
     * this method will always return false so it does not continue to run.
     * @see org.dataflow.components.DataflowComponent#isReady()
     */
    @Override
    public boolean isReady() {
        return false;
    }

    /**
     * @see org.dataflow.components.AbstractDataflowComponent#doit()
     */
    @Override
    protected void doit() throws Exception {
        
        // first fetch all the media files in the set.
        Project result = this.getProjectMetadata();
        ArrayList<SpectraLabelData> labels = new ArrayList<SpectraLabelData>();

        // for each media file, fetch all the tags (labeled data) for that media file.
        for (int instId : result.mediaFiles) {
            TagData[] td = this.fetchTags(instId, tags);
            for (TagData tag : td) {
                tag.setClassName(tag.getClassName());
                String path = "/api/v0/getAudioSpectraData/"+instId+
                                "/?startTime="+tag.startTime+"&endTime="+tag.endTime+
                                "&dampingFactor="+getDampingFactor()+
                                "&minFrequency="+getMinFrequency()+"&maxFrequency="+getMaxFrequency()+
                                "&numTimeFramesPerSecond="+slicesPerSecond+"&numFrequencyBands="+
                                frequencyBands;
                String data = this.getRecord(path);
                SpectraData spectra = null;
                try {
                    spectra = gson.fromJson(data, SpectraData.class);
                    SpectraLabelData tt = new SpectraLabelData(tag, spectra);
                    labels.add(tt);
                } catch (Throwable t) {
                    log.error("Could not parse:\n"+data);
                    continue;
                }
            }
        }
        ExampleTable et = this.generateExampleTable(labels, frequencyBands);
        log.info(frequencyBands+" frequency bands, "+slicesPerSecond+" slices a second, "+slices+" slices per example, produced table with "+et.getNumRows()+" rows and " + et.getNumColumns() + " columns.");
        this.outputInstances.pushData(et);
    }
    
    /**
     * @return the outputInstances
     */
    public BufferedDataflowPipe<ExampleTable> getOutputInstances() {
        return outputInstances;
    }

    /**
     * @param outputInstances the outputInstances to set
     */
    public void setOutputInstances(BufferedDataflowPipe<ExampleTable> outputInstances) {
        this.outputInstances = outputInstances;
    }

    /**
     * @param args
     * @throws Exception anything might happen.
     */
    public static void main(String[] args) throws Exception {
        ArloLabelDataReader ac = new ArloLabelDataReader();
        ac.setHost("live.arloproject.com");
        ac.setPort(81);
        ac.setAccount("~");
        ac.setPassword("~");
        ac.setDampingFactor(0.98);
        ac.setMinFrequency(601);
        ac.setMaxFrequency(6002);
        System.out.println(ac.getPath(111));
        ac.doit();
        System.exit(0);
    }
}
