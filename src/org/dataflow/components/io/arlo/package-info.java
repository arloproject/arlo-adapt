/**
 * Accessors for ARLO data used in the HIPSTAS project. They support a restful API, there
 * is a module to pull a set of record ids based on tags, and another to get the features
 * associated with those ids.
 * @author redman
 */
package org.dataflow.components.io.arlo;