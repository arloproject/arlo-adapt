/**
 * 
 */
package org.dataflow.components.io.arlo;

import java.util.ArrayList;
import java.util.Collections;

import org.seasr.datatypes.datamining.table.basic.MutableTableImpl;

import com.google.gson.internal.LinkedTreeMap;

/**
 * This is the labeled data which is used to directly create labeled instance data
 * in an example table. Each entry in the example table will contain some number of frames
 * within each time window included in the example. 
 * @author redman
 */
class SpectraLabelData {

    /** the frames for each time increment. */
    private double[][] frames;
    
    /** the tag data. */
    private TagData td = null;
    
    /** the start of the data. */
    double start = 0;
    
    /** end of the data. */
    double end = 0;
    
    /**
     * the spectra data.
     * @param td tag meta data.
     * @param sd the spectra data.
     */
    SpectraLabelData(TagData td, SpectraData sd) {
        
        /** the frames need to be sorted, this sorts them. */
        class Frame implements Comparable<Frame> {
            /** the frame offset. */
            double offset;
            
            /** the data points, frequency bands. */
            double[] frequencyBands;
            
            /**
             * the frame containing the offset and the frequency bands.
             * @param o time offset.
             * @param arrayList the frequency band.
             */
            Frame(double o, ArrayList<Double> arrayList) {
                this.offset = o;
                frequencyBands = new double[arrayList.size()];
                for (int i = 0; i < frequencyBands.length; i++)
                    this.frequencyBands[i] = arrayList.get(i);                
            }
            
            @Override
            public int compareTo(Frame o) {
                if (offset > o.offset) 
                    return 1;
                if (offset < o.offset)
                    return -1;
                else
                    return 0;
            }
        }
        
        // the data frames are in a Set, which is useless, they need to be ordered of course since
        // they are consecutive.
        this.td = td;
        LinkedTreeMap<?, ?> ltm = (LinkedTreeMap<?, ?>)sd.spectraData.get("frames");
        int numberFrames = ltm.size();
        frames = new double[numberFrames][];
        this.start = sd.getStartTime();
        this.end = sd.getEndTime();
        // generate an array of Frame objects that are easily sorted.
        ArrayList<Frame> frmarray = new ArrayList<Frame>();
        for (Object key : ltm.keySet()) {
            String off = key.toString();
            @SuppressWarnings("unchecked")
            ArrayList<Double> tmp = ((ArrayList<Double>)ltm.get(key));
            frmarray.add(new Frame(Double.parseDouble(off), tmp));
        }
        Collections.sort(frmarray);
        for (int i = 0; i < numberFrames; i++)
            frames[i] = frmarray.get(i).frequencyBands;
    }
    
    /**
     * add the labeled data to the example table.
     * @param slices the number of slices per row of data.
     * @return The number of rows this set can add to the example table.
     */
    int countRows(int slices) {
        if (frames.length < slices)
            return 0;
        else {
            return (frames.length - slices) + 1;
        }
    }

    /**
     * add the labeled data to the example table.
     * @param et the example table.
     * @param row the row we are populating.
     * @param slices the number of slices per row
     * @param slicesPerSecond the number of slices per second
     * @return where we are when done.
     */
    int addLabeledData(MutableTableImpl et, int row, int slices, int slicesPerSecond) {        
        int column = 0;
        int nframes = countRows(slices);
        final double increment = (1.0 / (double)slicesPerSecond);
        double start = 0.0;
        for (int frame = 0; frame < nframes; frame++) {
            for (int offset = 0; offset < slices ; offset++) {
                double[] bands = frames[frame+offset];
                for (int freq = 0; freq < bands.length; freq++) {
                    et.setDouble(bands[freq], row, column++);
                }
            }
            et.setString(td.getClassName(), row, column++);
            et.setInt(td.mediaFileId, row, column++);
            et.setInt(td.tagId, row, column++);
            double st = td.startTime+start;
            et.setDouble(st, row, column++);
            start += increment;
            double endt = td.startTime+start;
            et.setDouble(endt, row, column++);
            row++;
            column = 0;
        }
        return row;
    }
    
    /**
     * just a nice rep of this object.
     */
    public String toString() {
        return td.getClassName()+"("+td.tagId+")"+":id="+td.mediaFileId+":s="+start+":e="+end+":f="+frames.length+":fs="+frames[0].length;
    }
}
