/**
 * 
 */
package org.dataflow.components.io.arlo;

import java.util.ArrayList;

import org.dataflow.pipes.BufferedDataflowPipe;
import org.meandre.annotations.Component;
import org.meandre.annotations.Component.Licenses;
import org.meandre.annotations.Component.Mode;
import org.meandre.annotations.ComponentOutput;
import org.seasr.datatypes.datamining.table.ExampleTable;

/**
 * Generate an example table with data to classify.
 * @author redman
 */
@Component(name = "Arlo Data Generator", creator = "Tom Redman",
                baseURL = "meandre://seasr.org/components/foundry/", mode = Mode.compute, rights = Licenses.UofINCSA,
                tags = "ARLO, reader",
                description = "Provided with a project id, the number of frequency bands to produce, slices per example and slices per second, this module will generate"
                    + " an ExampleTable containing examples of the spectral data from the media files within that project.")
public class ArloDataReader extends ArloReader {

    
    /** the trained classifer. */
    @ComponentOutput(name = "matches", description = "Returns a table of examples to be labeled. This table includes "
        + "the media file id.")
    public BufferedDataflowPipe<ExampleTable> outputInstances = new BufferedDataflowPipe<ExampleTable>();

    /** the offset into the list of media files. This component will not run continuously, it needs to exit occasionally
     * so when it fills it's output pipe, it can be blocked. */
    private int index = 0;
    
    /** the project data. */
    private Project projdef = null;
    
    /**
     * This module is always ready, it requires no inputs, since it is a head, 
     * this method will always return false so it does not continue to run.
     * @see org.dataflow.components.DataflowComponent#isReady()
     */
    @Override
    public boolean isReady() {
        return projdef != null;
    }

    /**
     * @see org.dataflow.components.AbstractDataflowComponent#doit()
     */
    @Override
    protected void doit() throws Exception {
        // number of records to do at one time
        final int BATCH_SIZE = 5;

        // first fetch all the media files in the set.
        if (projdef == null)
            projdef = this.getProjectMetadata();
        ArrayList<SpectraLabelData> labels = new ArrayList<SpectraLabelData>();

        // for each media file, fetch all the tags (labeled data) for that media file.
        for (int counter = 0; counter < BATCH_SIZE && index < projdef.mediaFiles.size(); counter++) {
            int instId = projdef.mediaFiles.get(index);
            index++;
            TagData td = new TagData();
            td.mediaFileId = instId;
            String path = getPath(instId);
            String data = this.getRecord(path);
            SpectraData spectra = null;
            try {
                spectra = gson.fromJson(data, SpectraData.class);
                SpectraLabelData tt = new SpectraLabelData(td, spectra);
                labels.add(tt);
            } catch (Throwable t) {
                log.error("Could not parse:\n" + data);
                continue;
            }
        }
        ExampleTable et = this.generateExampleTable(labels, frequencyBands);
        this.showTable("Testing Table",et);
        this.outputInstances.pushData(et);
        if (index >= projdef.mediaFiles.size()) 
            projdef = null;
    }

    /**
     * @return the outputInstances
     */
    public BufferedDataflowPipe<ExampleTable> getOutputInstances() {
        return outputInstances;
    }

    /**
     * @param outputInstances the outputInstances to set
     */
    public void setOutputInstances(BufferedDataflowPipe<ExampleTable> outputInstances) {
        this.outputInstances = outputInstances;
    }

    /**
     * @param args
     * @throws Exception anything might happen.
     */
    public static void main(String[] args) throws Exception {
        ArloDataReader ac = new ArloDataReader();
        ac.setHost("live.arloproject.com");
        ac.setPort(81);
        ac.setAccount("~");
        ac.setPassword("~");
        ac.setDampingFactor(0.98);
        ac.setMinFrequency(601);
        ac.setMaxFrequency(6002);
        System.out.println(ac.getPath(111));
        ac.doit();
        System.exit(0);
    }
}
