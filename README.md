ARLO Project - Adapt Backend
============

Adapt is the Java backend for ARLO. This provides most of the heavy 
lifting in ARLO, notably search algorithms and spectra generation. 

### License

ARLO is released under the University of Illinois/NCSA Open Source License. 

See the LICENSE file in the root of this repository for further details. 
